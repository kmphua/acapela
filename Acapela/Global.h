//
//  Global.h
//  eLive
//
//  Created by Kevin on 13/1/3.
//  Copyright (c) 2013年 Kevin Phua. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "acapela.h"

#define SYSTEM_VERSION_EQUAL_TO(v)                  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedSame)
#define SYSTEM_VERSION_GREATER_THAN(v)              ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedDescending)
#define SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN(v)                 ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] == NSOrderedAscending)
#define SYSTEM_VERSION_LESS_THAN_OR_EQUAL_TO(v)     ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedDescending)

#define MAX_LABEL_LENGTH                9
#define MAX_LABEL_LENGTH_CHINESE        3

typedef enum {
    CH_1 = 0,
    CH_2,
    CH_3,
    CH_4,
    CH_5,
    CH_6,
    CH_7,
    CH_8,
    CH_9,
    CH_10,
    CH_11,
    CH_12,
    CH_13,
    CH_14,
    CH_15,
    CH_16,
    CH_17,
    CH_18,
    MT_1,
    MT_2,
    MT_3,
    MT_4,
    MAIN,
    CTRL_RM
} ChannelNum;

enum DelayUnit {
    DELAY_TIME = 0,
    DELAY_METER,
    DELAY_FEET
};

@interface Global : NSObject

+ (Global *)getInstance;
+ (NSString *)getSliderGain:(float)value appendDb:(BOOL)appendDb;
+ (NSString *)getPanString:(float)value;
+ (NSString *)getEqGainString:(int)value;
+ (NSString *)getEqFreqString:(int)value;

+ (NSString *)getDelayString:(int)value type:(int)type temp:(int)temp;
+ (NSString *)getTempString:(int)temp;
+ (int)delayToKnobValue:(int)delay;
+ (int)knobValueToDelay:(int)knobValue;
+ (int)freqToKnobValue:(int)freq;
+ (int)knobValueToFreq:(int)knobValue;
+ (float)tempToKnobValue:(int)temp;

+ (double)getEqGain:(int)index;
+ (double)getEqFreq:(int)index;
+ (double)getEqQ:(int)index;
+ (NSString *)getEqQ1String:(float)value;
+ (NSString *)getEqQ4String:(float)value;
+ (NSString *)getEqQ23String:(float)value;
+ (UIImage *)imageWithView:(UIView *)view scaledToSize:(CGSize)newSize;
+ (double)eqPeak:(double)freq eqFreq:(double)eqFreq eqDb:(double)eqDb eqQ:(double)eqQ;
+ (double)eqLowShelf:(double)freq eqFreq:(double)eqFreq eqDb:(double)eqDb;
+ (double)eqHighShelf:(double)freq eqFreq:(double)eqFreq eqDb:(double)eqDb;
+ (double)eqLPF:(double)freq eqFreq:(double)eqFreq;
+ (double)eqHPF:(double)freq eqFreq:(double)eqFreq;

+ (NSString *)getFreqString:(float)value;
+ (NSString *)getTimeString:(float)value;
+ (NSString *)getRatioString:(float)value;
+ (double)getRatioValue:(float)value;
+ (double)getRatioCompValue:(float)value;

+ (int)getPeqFrequencyCount;
+ (NSArray *)eqPeakData:(double)eqFreq eqDb:(double)eqDb eqQ:(double)eqQ;
+ (NSArray *)eqLowShelfData:(double)eqFreq eqDb:(double)eqDb;
+ (NSArray *)eqHighShelfData:(double)eqFreq eqDb:(double)eqDb;
+ (NSArray *)eqLPFData:(double)eqFreq;
+ (NSArray *)eqHPFData:(double)eqFreq;

+ (NSArray *)dynGateOutput:(double)threshold range:(double)range;
+ (NSArray *)dynExpanderOutput:(double)threshold ratio:(double)ratio;
+ (NSArray *)dynCompressorOutput:(double)threshold ratio:(double)ratio;
+ (NSArray *)dynLimitOutput:(double)threshold;
+ (double)dynGateValue:(double)threshold range:(double)range;
+ (double)dynExpanderValue:(double)threshold ratio:(double)ratio;
+ (double)dynCompressorValue:(double)threshold ratio:(double)ratio;
+ (double)dynLimitValue:(double)threshold;
+ (NSNumber *)dynGateOutputValue:(double)input threshold:(double)threshold range:(double)range;
+ (NSNumber *)dynExpanderOutputValue:(double)input threshold:(double)threshold ratio:(double)ratio;
+ (NSNumber *)dynCompressorOutputValue:(double)input threshold:(double)threshold ratio:(double)ratio;
+ (NSNumber *)dynLimitOutputValue:(double)input threshold:(double)threshold;

+ (NSString *)base64EncodedStringFromString:(NSString *)string;
+ (unsigned char)crc:(unsigned char*)data len:(int)len;
+ (BOOL)checkCrc:(unsigned char*)data len:(int)len pktCrc:(unsigned char)pktCrc;

+ (void)setClearBit:(int *)value bitValue:(int)bitValue bitOrder:(int)bitOrder;
+ (void)setOrder:(int *)value value:(int)order;

+ (NSString *)getCurrentTime;

@end
