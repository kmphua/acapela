//
//  DspRead.h
//  Acapela
//
//  Created by Kevin Phua on 13/8/18.
//  Copyright (c) 2013年 Kevin Phua. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DspRead : NSObject

@property int command;
@property int dspId;

@end
