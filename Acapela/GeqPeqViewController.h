//
//  GeqViewController.h
//  eLive
//
//  Created by Kevin on 12/12/27.
//  Copyright (c) 2012年 Kevin Phua. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CorePlot-CocoaTouch.h"
#import "acapela.h"
#import "JEProgressView.h"

@class GeqPeqViewController;
@protocol PeqPreviewDelegate <NSObject>
@required
- (void)didFinishEditing;
@end

@interface GeqPeqViewController : UIViewController<CPTPlotDataSource, UITextFieldDelegate>
{
    // Label edit
    ChannelType currentChannel;
    ChannelType currentChannelLabel;
}

@property (nonatomic) ChannelType currentChannel;
@property (nonatomic) ChannelType currentChannelLabel;

@property (strong, nonatomic) NSMutableArray *peqImages;
@property (strong, nonatomic) NSMutableArray *eqPreviewImages;

@property (weak, nonatomic) IBOutlet UIImageView *imgBackground;

// Main panel
@property (strong, nonatomic) IBOutlet UIButton *btnFile;
@property (strong, nonatomic) IBOutlet UIButton *btnSelectDown;
@property (strong, nonatomic) IBOutlet UIButton *btnSelectUp;
@property (strong, nonatomic) IBOutlet UIButton *btnSolo;
@property (strong, nonatomic) IBOutlet UIButton *btnOn;
@property (strong, nonatomic) IBOutlet UILabel *lblPage;
@property (weak, nonatomic) IBOutlet UILabel *lblSliderValue;
@property (nonatomic,retain) UISlider *slider;
@property (nonatomic,retain) JEProgressView *meterL;
@property (nonatomic,retain) JEProgressView *meterR;
@property (strong, nonatomic) IBOutlet UIButton *btnMeterPrePost;
@property (strong, nonatomic) IBOutlet UITextField *txtPage;
@property (weak, nonatomic) IBOutlet UIImageView *imgPanelBg;
@property (nonatomic,retain) JEProgressView *meterInL;
@property (nonatomic,retain) JEProgressView *meterInR;
@property (nonatomic,retain) JEProgressView *meterOutL;
@property (nonatomic,retain) JEProgressView *meterOutR;
@property (nonatomic,retain) JEProgressView *meterIn;
@property (nonatomic,retain) JEProgressView *meterOut;

// PEQ panel
@property (weak, nonatomic) IBOutlet UIView *viewPeq;
@property (weak, nonatomic) IBOutlet UIButton *btnPeqOnOff;
@property (weak, nonatomic) IBOutlet UIView *viewPeqFrame;
@property (strong, nonatomic) IBOutlet UIButton *btnBall1;
@property (strong, nonatomic) IBOutlet UIButton *btnBall2;
@property (strong, nonatomic) IBOutlet UIButton *btnBall3;
@property (strong, nonatomic) IBOutlet UIButton *btnBall4;
@property (strong, nonatomic) IBOutlet UIButton *btnPeq1;
@property (strong, nonatomic) IBOutlet UIButton *btnPeq2;
@property (strong, nonatomic) IBOutlet UIButton *btnPeq3;
@property (strong, nonatomic) IBOutlet UIButton *btnPeq4;
@property (strong, nonatomic) IBOutlet UIButton *btnPeqFilterFront;
@property (strong, nonatomic) IBOutlet UIButton *btnPeqFilterBack;
@property (strong, nonatomic) IBOutlet UILabel *lblPeq1ValueG;
@property (strong, nonatomic) IBOutlet UILabel *lblPeq1ValueF;
@property (strong, nonatomic) IBOutlet UILabel *lblPeq1ValueQ;
@property (strong, nonatomic) IBOutlet UILabel *lblPeq2ValueG;
@property (strong, nonatomic) IBOutlet UILabel *lblPeq2ValueF;
@property (strong, nonatomic) IBOutlet UILabel *lblPeq2ValueQ;
@property (strong, nonatomic) IBOutlet UILabel *lblPeq3ValueG;
@property (strong, nonatomic) IBOutlet UILabel *lblPeq3ValueF;
@property (strong, nonatomic) IBOutlet UILabel *lblPeq3ValueQ;
@property (strong, nonatomic) IBOutlet UILabel *lblPeq4ValueG;
@property (strong, nonatomic) IBOutlet UILabel *lblPeq4ValueF;
@property (strong, nonatomic) IBOutlet UILabel *lblPeq4ValueQ;

// GEQ panel
@property (weak, nonatomic) IBOutlet UIView *viewGeq;
@property (weak, nonatomic) IBOutlet UIButton *btnGeqOnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnGeqDraw;
@property (weak, nonatomic) IBOutlet UIView *viewGeqFrame;
@property (weak, nonatomic) IBOutlet UIView *viewGeqDraw;
@property (weak, nonatomic) IBOutlet UILabel *lblGeq;
@property (nonatomic,retain) UISlider *sliderGeq1;
@property (nonatomic,retain) UISlider *sliderGeq2;
@property (nonatomic,retain) UISlider *sliderGeq3;
@property (nonatomic,retain) UISlider *sliderGeq4;
@property (nonatomic,retain) UISlider *sliderGeq5;
@property (nonatomic,retain) UISlider *sliderGeq6;
@property (nonatomic,retain) UISlider *sliderGeq7;
@property (nonatomic,retain) UISlider *sliderGeq8;
@property (nonatomic,retain) UISlider *sliderGeq9;
@property (nonatomic,retain) UISlider *sliderGeq10;
@property (nonatomic,retain) UISlider *sliderGeq11;
@property (nonatomic,retain) UISlider *sliderGeq12;
@property (nonatomic,retain) UISlider *sliderGeq13;
@property (nonatomic,retain) UISlider *sliderGeq14;
@property (nonatomic,retain) UISlider *sliderGeq15;
@property (nonatomic,retain) UISlider *sliderGeq16;
@property (nonatomic,retain) UISlider *sliderGeq17;
@property (nonatomic,retain) UISlider *sliderGeq18;
@property (nonatomic,retain) UISlider *sliderGeq19;
@property (nonatomic,retain) UISlider *sliderGeq20;
@property (nonatomic,retain) UISlider *sliderGeq21;
@property (nonatomic,retain) UISlider *sliderGeq22;
@property (nonatomic,retain) UISlider *sliderGeq23;
@property (nonatomic,retain) UISlider *sliderGeq24;
@property (nonatomic,retain) UISlider *sliderGeq25;
@property (nonatomic,retain) UISlider *sliderGeq26;
@property (nonatomic,retain) UISlider *sliderGeq27;
@property (nonatomic,retain) UISlider *sliderGeq28;
@property (nonatomic,retain) UISlider *sliderGeq29;
@property (nonatomic,retain) UISlider *sliderGeq30;
@property (nonatomic,retain) UISlider *sliderGeq31;

// Slider Meters
@property (nonatomic,retain) JEProgressView *meter1Top;
@property (nonatomic,retain) JEProgressView *meter1Btm;
@property (nonatomic,retain) JEProgressView *meter2Top;
@property (nonatomic,retain) JEProgressView *meter2Btm;
@property (nonatomic,retain) JEProgressView *meter3Top;
@property (nonatomic,retain) JEProgressView *meter3Btm;
@property (nonatomic,retain) JEProgressView *meter4Top;
@property (nonatomic,retain) JEProgressView *meter4Btm;
@property (nonatomic,retain) JEProgressView *meter5Top;
@property (nonatomic,retain) JEProgressView *meter5Btm;
@property (nonatomic,retain) JEProgressView *meter6Top;
@property (nonatomic,retain) JEProgressView *meter6Btm;
@property (nonatomic,retain) JEProgressView *meter7Top;
@property (nonatomic,retain) JEProgressView *meter7Btm;
@property (nonatomic,retain) JEProgressView *meter8Top;
@property (nonatomic,retain) JEProgressView *meter8Btm;
@property (nonatomic,retain) JEProgressView *meter9Top;
@property (nonatomic,retain) JEProgressView *meter9Btm;
@property (nonatomic,retain) JEProgressView *meter10Top;
@property (nonatomic,retain) JEProgressView *meter10Btm;
@property (nonatomic,retain) JEProgressView *meter11Top;
@property (nonatomic,retain) JEProgressView *meter11Btm;
@property (nonatomic,retain) JEProgressView *meter12Top;
@property (nonatomic,retain) JEProgressView *meter12Btm;
@property (nonatomic,retain) JEProgressView *meter13Top;
@property (nonatomic,retain) JEProgressView *meter13Btm;
@property (nonatomic,retain) JEProgressView *meter14Top;
@property (nonatomic,retain) JEProgressView *meter14Btm;
@property (nonatomic,retain) JEProgressView *meter15Top;
@property (nonatomic,retain) JEProgressView *meter15Btm;
@property (nonatomic,retain) JEProgressView *meter16Top;
@property (nonatomic,retain) JEProgressView *meter16Btm;
@property (nonatomic,retain) JEProgressView *meter17Top;
@property (nonatomic,retain) JEProgressView *meter17Btm;
@property (nonatomic,retain) JEProgressView *meter18Top;
@property (nonatomic,retain) JEProgressView *meter18Btm;
@property (nonatomic,retain) JEProgressView *meter19Top;
@property (nonatomic,retain) JEProgressView *meter19Btm;
@property (nonatomic,retain) JEProgressView *meter20Top;
@property (nonatomic,retain) JEProgressView *meter20Btm;
@property (nonatomic,retain) JEProgressView *meter21Top;
@property (nonatomic,retain) JEProgressView *meter21Btm;
@property (nonatomic,retain) JEProgressView *meter22Top;
@property (nonatomic,retain) JEProgressView *meter22Btm;
@property (nonatomic,retain) JEProgressView *meter23Top;
@property (nonatomic,retain) JEProgressView *meter23Btm;
@property (nonatomic,retain) JEProgressView *meter24Top;
@property (nonatomic,retain) JEProgressView *meter24Btm;
@property (nonatomic,retain) JEProgressView *meter25Top;
@property (nonatomic,retain) JEProgressView *meter25Btm;
@property (nonatomic,retain) JEProgressView *meter26Top;
@property (nonatomic,retain) JEProgressView *meter26Btm;
@property (nonatomic,retain) JEProgressView *meter27Top;
@property (nonatomic,retain) JEProgressView *meter27Btm;
@property (nonatomic,retain) JEProgressView *meter28Top;
@property (nonatomic,retain) JEProgressView *meter28Btm;
@property (nonatomic,retain) JEProgressView *meter29Top;
@property (nonatomic,retain) JEProgressView *meter29Btm;
@property (nonatomic,retain) JEProgressView *meter30Top;
@property (nonatomic,retain) JEProgressView *meter30Btm;
@property (nonatomic,retain) JEProgressView *meter31Top;
@property (nonatomic,retain) JEProgressView *meter31Btm;

@property (weak, nonatomic) IBOutlet UIView *viewSelectFilterFront;
@property (weak, nonatomic) IBOutlet UIView *viewSelectFilterBack;

// CorePlot scatter plot view
@property (nonatomic, strong) CPTGraphHostingView *hostView;

// Delegate callback to parent controller
@property (nonatomic, assign) id <PeqPreviewDelegate> delegate;

- (void)updateChannelLabels;
- (void)updatePeqFrame;
- (void)processChannelReply:(EqChannelPagePacket *)eqChannelPkt;
- (void)processMultiReply:(EqMultiPagePacket *)eqMultiPkt;
- (void)processMainReply:(EqMainPagePacket *)eqMainPkt;
- (void)processChannel1_8PageReply:(ChannelCh1_8PagePacket *)eqChannelPkt;
- (void)processChannel9_16PageReply:(ChannelCh9_16PagePacket *)eqChannelPkt;
- (void)processChannel17_18PageReply:(ChannelCh17_18PagePacket *)eqChannelPkt;
- (void)processMulti1_4PageReply:(ChannelMulti1_4PagePacket *)eqMultiPkt;
- (void)processChannelViewPage:(ViewPageChannelPacket *)pkt;
- (void)processUsbAudioViewPage:(ViewPageUsbAudioPacket *)pkt;
- (void)processMultiViewPage:(ViewPageMultiPacket *)pkt;
- (void)processMainViewPage:(ViewPageMainPacket *)pkt;
- (void)processChannelLabel:(AllMeterValueRxPacket *)pkt;
- (void)processMeter:(AllMeterValueRxPacket *)meterValue;
- (void)setSceneFilename:(NSString*)fileName;

@end
