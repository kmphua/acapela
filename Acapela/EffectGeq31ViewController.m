//
//  EffectGeq31ViewController
//  Acapela
//
//  Created by Kevin on 12/12/27.
//  Copyright (c) 2012年 Kevin Phua. All rights reserved.
//

#import "EffectGeq31ViewController.h"
#import "acapela.h"
#import "AppDelegate.h"
#import "ViewController.h"
#import "Global.h"
#import "JEProgressView.h"

#define GEQ_SLIDER_MIN_VALUE    -12.0
#define GEQ_SLIDER_MAX_VALUE    12.0
#define PEQ_FRAME_WIDTH         535.0
#define PEQ_FRAME_HEIGHT        227.0

@implementation EffectGeq31ViewController
{
    ViewController *viewController;
    BOOL geqLock, mainEqLock;
    NSTimer *channelScrollTimer;
    BOOL geqOnOff;
    BOOL geqLinkMode;
}

@synthesize btnGeqLink, btnGeqDraw, btnFile, viewGeqFrameL, viewGeqDrawL, viewGeqFrameR, viewGeqDrawR, imgEqL, imgEqR;
@synthesize sliderLGeq1, sliderLGeq2, sliderLGeq3, sliderLGeq4, sliderLGeq5;
@synthesize sliderLGeq6, sliderLGeq7, sliderLGeq8, sliderLGeq9, sliderLGeq10;
@synthesize sliderLGeq11, sliderLGeq12, sliderLGeq13, sliderLGeq14, sliderLGeq15;
@synthesize sliderLGeq16, sliderLGeq17, sliderLGeq18, sliderLGeq19, sliderLGeq20;
@synthesize sliderLGeq21, sliderLGeq22, sliderLGeq23, sliderLGeq24, sliderLGeq25;
@synthesize sliderLGeq26, sliderLGeq27, sliderLGeq28, sliderLGeq29, sliderLGeq30, sliderLGeq31;
@synthesize sliderRGeq1, sliderRGeq2, sliderRGeq3, sliderRGeq4, sliderRGeq5;
@synthesize sliderRGeq6, sliderRGeq7, sliderRGeq8, sliderRGeq9, sliderRGeq10;
@synthesize sliderRGeq11, sliderRGeq12, sliderRGeq13, sliderRGeq14, sliderRGeq15;
@synthesize sliderRGeq16, sliderRGeq17, sliderRGeq18, sliderRGeq19, sliderRGeq20;
@synthesize sliderRGeq21, sliderRGeq22, sliderRGeq23, sliderRGeq24, sliderRGeq25;
@synthesize sliderRGeq26, sliderRGeq27, sliderRGeq28, sliderRGeq29, sliderRGeq30, sliderRGeq31;
@synthesize meterL1Top, meterL1Btm, meterL2Top, meterL2Btm, meterL3Top, meterL3Btm;
@synthesize meterL4Top, meterL4Btm, meterL5Top, meterL5Btm, meterL6Top, meterL6Btm;
@synthesize meterL7Top, meterL7Btm, meterL8Top, meterL8Btm, meterL9Top, meterL9Btm;
@synthesize meterL10Top, meterL10Btm, meterL11Top, meterL11Btm, meterL12Top, meterL12Btm;
@synthesize meterL13Top, meterL13Btm, meterL14Top, meterL14Btm, meterL15Top, meterL15Btm;
@synthesize meterL16Top, meterL16Btm, meterL17Top, meterL17Btm, meterL18Top, meterL18Btm;
@synthesize meterL19Top, meterL19Btm, meterL20Top, meterL20Btm, meterL21Top, meterL21Btm;
@synthesize meterL22Top, meterL22Btm, meterL23Top, meterL23Btm, meterL24Top, meterL24Btm;
@synthesize meterL25Top, meterL25Btm, meterL26Top, meterL26Btm, meterL27Top, meterL27Btm;
@synthesize meterL28Top, meterL28Btm, meterL29Top, meterL29Btm, meterL30Top, meterL30Btm;
@synthesize meterL31Top, meterL31Btm;
@synthesize meterR1Top, meterR1Btm, meterR2Top, meterR2Btm, meterR3Top, meterR3Btm;
@synthesize meterR4Top, meterR4Btm, meterR5Top, meterR5Btm, meterR6Top, meterR6Btm;
@synthesize meterR7Top, meterR7Btm, meterR8Top, meterR8Btm, meterR9Top, meterR9Btm;
@synthesize meterR10Top, meterR10Btm, meterR11Top, meterR11Btm, meterR12Top, meterR12Btm;
@synthesize meterR13Top, meterR13Btm, meterR14Top, meterR14Btm, meterR15Top, meterR15Btm;
@synthesize meterR16Top, meterR16Btm, meterR17Top, meterR17Btm, meterR18Top, meterR18Btm;
@synthesize meterR19Top, meterR19Btm, meterR20Top, meterR20Btm, meterR21Top, meterR21Btm;
@synthesize meterR22Top, meterR22Btm, meterR23Top, meterR23Btm, meterR24Top, meterR24Btm;
@synthesize meterR25Top, meterR25Btm, meterR26Top, meterR26Btm, meterR27Top, meterR27Btm;
@synthesize meterR28Top, meterR28Btm, meterR29Top, meterR29Btm, meterR30Top, meterR30Btm;
@synthesize meterR31Top, meterR31Btm;

static BOOL initDone = NO;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    
    mainEqLock = NO;
    initDone = YES;
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    viewController = (ViewController *)appDelegate.window.rootViewController;
    
    // Do any additional setup after loading the view from its nib.

    // Add GEQ vertical L sliders
    UIImage *sliderImage = [UIImage imageNamed:@"fader-9.png"];
    UIImage *progressImage = [[UIImage imageNamed:@"bar-4.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0,0,0,0)];
    
    sliderLGeq1 = [[UISlider alloc] initWithFrame:CGRectMake(-94, 100, 230, 12)];
    sliderLGeq1.transform = CGAffineTransformRotate(sliderLGeq1.transform, 270.0/180*M_PI);
    [sliderLGeq1 addTarget:self action:@selector(onSlider1Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderLGeq1 addTarget:self action:@selector(onSlider1End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderLGeq1 addTarget:self action:@selector(onSlider1End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderLGeq1 addTarget:self action:@selector(onSlider1Action:) forControlEvents:UIControlEventValueChanged];
    [sliderLGeq1 setBackgroundColor:[UIColor clearColor]];
    [sliderLGeq1 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq1 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq1 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderLGeq1.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderLGeq1.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderLGeq1.continuous = YES;
    sliderLGeq1.value = 0.0;
    [viewGeqFrameL addSubview:sliderLGeq1];
    
    sliderLGeq2 = [[UISlider alloc] initWithFrame:CGRectMake(-69, 100, 230, 12)];
    sliderLGeq2.transform = CGAffineTransformRotate(sliderLGeq2.transform, 270.0/180*M_PI);
    [sliderLGeq2 addTarget:self action:@selector(onSlider2Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderLGeq2 addTarget:self action:@selector(onSlider2End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderLGeq2 addTarget:self action:@selector(onSlider2End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderLGeq2 addTarget:self action:@selector(onSlider2Action:) forControlEvents:UIControlEventValueChanged];
    [sliderLGeq2 setBackgroundColor:[UIColor clearColor]];
    [sliderLGeq2 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq2 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq2 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderLGeq2.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderLGeq2.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderLGeq2.continuous = YES;
    sliderLGeq2.value = 0.0;
    [viewGeqFrameL addSubview:sliderLGeq2];
    
    sliderLGeq3 = [[UISlider alloc] initWithFrame:CGRectMake(-44, 100, 230, 12)];
    sliderLGeq3.transform = CGAffineTransformRotate(sliderLGeq3.transform, 270.0/180*M_PI);
    [sliderLGeq3 addTarget:self action:@selector(onSlider3Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderLGeq3 addTarget:self action:@selector(onSlider3End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderLGeq3 addTarget:self action:@selector(onSlider3End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderLGeq3 addTarget:self action:@selector(onSlider3Action:) forControlEvents:UIControlEventValueChanged];
    [sliderLGeq3 setBackgroundColor:[UIColor clearColor]];
    [sliderLGeq3 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq3 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq3 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderLGeq3.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderLGeq3.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderLGeq3.continuous = YES;
    sliderLGeq3.value = 0.0;
    [viewGeqFrameL addSubview:sliderLGeq3];
    
    sliderLGeq4 = [[UISlider alloc] initWithFrame:CGRectMake(-19, 100, 230, 12)];
    sliderLGeq4.transform = CGAffineTransformRotate(sliderLGeq4.transform, 270.0/180*M_PI);
    [sliderLGeq4 addTarget:self action:@selector(onSlider4Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderLGeq4 addTarget:self action:@selector(onSlider4End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderLGeq4 addTarget:self action:@selector(onSlider4End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderLGeq4 addTarget:self action:@selector(onSlider4Action:) forControlEvents:UIControlEventValueChanged];
    [sliderLGeq4 setBackgroundColor:[UIColor clearColor]];
    [sliderLGeq4 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq4 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq4 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderLGeq4.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderLGeq4.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderLGeq4.continuous = YES;
    sliderLGeq4.value = 0.0;
    [viewGeqFrameL addSubview:sliderLGeq4];
    
    sliderLGeq5 = [[UISlider alloc] initWithFrame:CGRectMake(6, 100, 230, 12)];
    sliderLGeq5.transform = CGAffineTransformRotate(sliderLGeq5.transform, 270.0/180*M_PI);
    [sliderLGeq5 addTarget:self action:@selector(onSlider5Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderLGeq5 addTarget:self action:@selector(onSlider5End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderLGeq5 addTarget:self action:@selector(onSlider5End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderLGeq5 addTarget:self action:@selector(onSlider5Action:) forControlEvents:UIControlEventValueChanged];
    [sliderLGeq5 setBackgroundColor:[UIColor clearColor]];
    [sliderLGeq5 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq5 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq5 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderLGeq5.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderLGeq5.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderLGeq5.continuous = YES;
    sliderLGeq5.value = 0.0;
    [viewGeqFrameL addSubview:sliderLGeq5];
    
    sliderLGeq6 = [[UISlider alloc] initWithFrame:CGRectMake(31, 100, 230, 12)];
    sliderLGeq6.transform = CGAffineTransformRotate(sliderLGeq6.transform, 270.0/180*M_PI);
    [sliderLGeq6 addTarget:self action:@selector(onSlider6Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderLGeq6 addTarget:self action:@selector(onSlider6End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderLGeq6 addTarget:self action:@selector(onSlider6End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderLGeq6 addTarget:self action:@selector(onSlider6Action:) forControlEvents:UIControlEventValueChanged];
    [sliderLGeq6 setBackgroundColor:[UIColor clearColor]];
    [sliderLGeq6 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq6 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq6 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderLGeq6.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderLGeq6.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderLGeq6.continuous = YES;
    sliderLGeq6.value = 0.0;
    [viewGeqFrameL addSubview:sliderLGeq6];
    
    sliderLGeq7 = [[UISlider alloc] initWithFrame:CGRectMake(56, 100, 230, 12)];
    sliderLGeq7.transform = CGAffineTransformRotate(sliderLGeq7.transform, 270.0/180*M_PI);
    [sliderLGeq7 addTarget:self action:@selector(onSlider7Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderLGeq7 addTarget:self action:@selector(onSlider7End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderLGeq7 addTarget:self action:@selector(onSlider7End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderLGeq7 addTarget:self action:@selector(onSlider7Action:) forControlEvents:UIControlEventValueChanged];
    [sliderLGeq7 setBackgroundColor:[UIColor clearColor]];
    [sliderLGeq7 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq7 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq7 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderLGeq7.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderLGeq7.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderLGeq7.continuous = YES;
    sliderLGeq7.value = 0.0;
    [viewGeqFrameL addSubview:sliderLGeq7];
    
    sliderLGeq8 = [[UISlider alloc] initWithFrame:CGRectMake(81, 100, 230, 12)];
    sliderLGeq8.transform = CGAffineTransformRotate(sliderLGeq8.transform, 270.0/180*M_PI);
    [sliderLGeq8 addTarget:self action:@selector(onSlider8Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderLGeq8 addTarget:self action:@selector(onSlider8End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderLGeq8 addTarget:self action:@selector(onSlider8End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderLGeq8 addTarget:self action:@selector(onSlider8Action:) forControlEvents:UIControlEventValueChanged];
    [sliderLGeq8 setBackgroundColor:[UIColor clearColor]];
    [sliderLGeq8 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq8 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq8 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderLGeq8.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderLGeq8.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderLGeq8.continuous = YES;
    sliderLGeq8.value = 0.0;
    [viewGeqFrameL addSubview:sliderLGeq8];
    
    sliderLGeq9 = [[UISlider alloc] initWithFrame:CGRectMake(106, 100, 230, 12)];
    sliderLGeq9.transform = CGAffineTransformRotate(sliderLGeq9.transform, 270.0/180*M_PI);
    [sliderLGeq9 addTarget:self action:@selector(onSlider9Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderLGeq9 addTarget:self action:@selector(onSlider9End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderLGeq9 addTarget:self action:@selector(onSlider9End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderLGeq9 addTarget:self action:@selector(onSlider9Action:) forControlEvents:UIControlEventValueChanged];
    [sliderLGeq9 setBackgroundColor:[UIColor clearColor]];
    [sliderLGeq9 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq9 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq9 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderLGeq9.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderLGeq9.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderLGeq9.continuous = YES;
    sliderLGeq9.value = 0.0;
    [viewGeqFrameL addSubview:sliderLGeq9];
    
    sliderLGeq10 = [[UISlider alloc] initWithFrame:CGRectMake(131, 100, 230, 12)];
    sliderLGeq10.transform = CGAffineTransformRotate(sliderLGeq10.transform, 270.0/180*M_PI);
    [sliderLGeq10 addTarget:self action:@selector(onSlider10Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderLGeq10 addTarget:self action:@selector(onSlider10End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderLGeq10 addTarget:self action:@selector(onSlider10End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderLGeq10 addTarget:self action:@selector(onSlider10Action:) forControlEvents:UIControlEventValueChanged];
    [sliderLGeq10 setBackgroundColor:[UIColor clearColor]];
    [sliderLGeq10 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq10 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq10 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderLGeq10.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderLGeq10.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderLGeq10.continuous = YES;
    sliderLGeq10.value = 0.0;
    [viewGeqFrameL addSubview:sliderLGeq10];
    
    sliderLGeq11 = [[UISlider alloc] initWithFrame:CGRectMake(156, 100, 230, 12)];
    sliderLGeq11.transform = CGAffineTransformRotate(sliderLGeq11.transform, 270.0/180*M_PI);
    [sliderLGeq11 addTarget:self action:@selector(onSlider11Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderLGeq11 addTarget:self action:@selector(onSlider11End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderLGeq11 addTarget:self action:@selector(onSlider11End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderLGeq11 addTarget:self action:@selector(onSlider11Action:) forControlEvents:UIControlEventValueChanged];
    [sliderLGeq11 setBackgroundColor:[UIColor clearColor]];
    [sliderLGeq11 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq11 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq11 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderLGeq11.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderLGeq11.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderLGeq11.continuous = YES;
    sliderLGeq11.value = 0.0;
    [viewGeqFrameL addSubview:sliderLGeq11];
    
    sliderLGeq12 = [[UISlider alloc] initWithFrame:CGRectMake(181, 100, 230, 12)];
    sliderLGeq12.transform = CGAffineTransformRotate(sliderLGeq12.transform, 270.0/180*M_PI);
    [sliderLGeq12 addTarget:self action:@selector(onSlider12Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderLGeq12 addTarget:self action:@selector(onSlider12End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderLGeq12 addTarget:self action:@selector(onSlider12End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderLGeq12 addTarget:self action:@selector(onSlider12Action:) forControlEvents:UIControlEventValueChanged];
    [sliderLGeq12 setBackgroundColor:[UIColor clearColor]];
    [sliderLGeq12 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq12 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq12 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderLGeq12.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderLGeq12.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderLGeq12.continuous = YES;
    sliderLGeq12.value = 0.0;
    [viewGeqFrameL addSubview:sliderLGeq12];
    
    sliderLGeq13 = [[UISlider alloc] initWithFrame:CGRectMake(206, 100, 230, 12)];
    sliderLGeq13.transform = CGAffineTransformRotate(sliderLGeq13.transform, 270.0/180*M_PI);
    [sliderLGeq13 addTarget:self action:@selector(onSlider13Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderLGeq13 addTarget:self action:@selector(onSlider13End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderLGeq13 addTarget:self action:@selector(onSlider13End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderLGeq13 addTarget:self action:@selector(onSlider13Action:) forControlEvents:UIControlEventValueChanged];
    [sliderLGeq13 setBackgroundColor:[UIColor clearColor]];
    [sliderLGeq13 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq13 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq13 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderLGeq13.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderLGeq13.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderLGeq13.continuous = YES;
    sliderLGeq13.value = 0.0;
    [viewGeqFrameL addSubview:sliderLGeq13];
    
    sliderLGeq14 = [[UISlider alloc] initWithFrame:CGRectMake(231, 100, 230, 12)];
    sliderLGeq14.transform = CGAffineTransformRotate(sliderLGeq14.transform, 270.0/180*M_PI);
    [sliderLGeq14 addTarget:self action:@selector(onSlider14Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderLGeq14 addTarget:self action:@selector(onSlider14End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderLGeq14 addTarget:self action:@selector(onSlider14End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderLGeq14 addTarget:self action:@selector(onSlider14Action:) forControlEvents:UIControlEventValueChanged];
    [sliderLGeq14 setBackgroundColor:[UIColor clearColor]];
    [sliderLGeq14 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq14 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq14 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderLGeq14.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderLGeq14.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderLGeq14.continuous = YES;
    sliderLGeq14.value = 0.0;
    [viewGeqFrameL addSubview:sliderLGeq14];
    
    sliderLGeq15 = [[UISlider alloc] initWithFrame:CGRectMake(256, 100, 230, 12)];
    sliderLGeq15.transform = CGAffineTransformRotate(sliderLGeq15.transform, 270.0/180*M_PI);
    [sliderLGeq15 addTarget:self action:@selector(onSlider15Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderLGeq15 addTarget:self action:@selector(onSlider15End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderLGeq15 addTarget:self action:@selector(onSlider15End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderLGeq15 addTarget:self action:@selector(onSlider15Action:) forControlEvents:UIControlEventValueChanged];
    [sliderLGeq15 setBackgroundColor:[UIColor clearColor]];
    [sliderLGeq15 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq15 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq15 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderLGeq15.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderLGeq15.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderLGeq15.continuous = YES;
    sliderLGeq15.value = 0.0;
    [viewGeqFrameL addSubview:sliderLGeq15];
    
    sliderLGeq16 = [[UISlider alloc] initWithFrame:CGRectMake(281, 100, 230, 12)];
    sliderLGeq16.transform = CGAffineTransformRotate(sliderLGeq16.transform, 270.0/180*M_PI);
    [sliderLGeq16 addTarget:self action:@selector(onSlider16Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderLGeq16 addTarget:self action:@selector(onSlider16End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderLGeq16 addTarget:self action:@selector(onSlider16End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderLGeq16 addTarget:self action:@selector(onSlider16Action:) forControlEvents:UIControlEventValueChanged];
    [sliderLGeq16 setBackgroundColor:[UIColor clearColor]];
    [sliderLGeq16 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq16 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq16 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderLGeq16.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderLGeq16.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderLGeq16.continuous = YES;
    sliderLGeq16.value = 0.0;
    [viewGeqFrameL addSubview:sliderLGeq16];
    
    sliderLGeq17 = [[UISlider alloc] initWithFrame:CGRectMake(306, 100, 230, 12)];
    sliderLGeq17.transform = CGAffineTransformRotate(sliderLGeq17.transform, 270.0/180*M_PI);
    [sliderLGeq17 addTarget:self action:@selector(onSlider17Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderLGeq17 addTarget:self action:@selector(onSlider17End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderLGeq17 addTarget:self action:@selector(onSlider17End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderLGeq17 addTarget:self action:@selector(onSlider17Action:) forControlEvents:UIControlEventValueChanged];
    [sliderLGeq17 setBackgroundColor:[UIColor clearColor]];
    [sliderLGeq17 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq17 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq17 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderLGeq17.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderLGeq17.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderLGeq17.continuous = YES;
    sliderLGeq17.value = 0.0;
    [viewGeqFrameL addSubview:sliderLGeq17];
    
    sliderLGeq18 = [[UISlider alloc] initWithFrame:CGRectMake(331, 100, 230, 12)];
    sliderLGeq18.transform = CGAffineTransformRotate(sliderLGeq18.transform, 270.0/180*M_PI);
    [sliderLGeq18 addTarget:self action:@selector(onSlider18Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderLGeq18 addTarget:self action:@selector(onSlider18End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderLGeq18 addTarget:self action:@selector(onSlider18End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderLGeq18 addTarget:self action:@selector(onSlider18Action:) forControlEvents:UIControlEventValueChanged];
    [sliderLGeq18 setBackgroundColor:[UIColor clearColor]];
    [sliderLGeq18 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq18 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq18 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderLGeq18.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderLGeq18.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderLGeq18.continuous = YES;
    sliderLGeq18.value = 0.0;
    [viewGeqFrameL addSubview:sliderLGeq18];
    
    sliderLGeq19 = [[UISlider alloc] initWithFrame:CGRectMake(356, 100, 230, 12)];
    sliderLGeq19.transform = CGAffineTransformRotate(sliderLGeq19.transform, 270.0/180*M_PI);
    [sliderLGeq19 addTarget:self action:@selector(onSlider19Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderLGeq19 addTarget:self action:@selector(onSlider19End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderLGeq19 addTarget:self action:@selector(onSlider19End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderLGeq19 addTarget:self action:@selector(onSlider19Action:) forControlEvents:UIControlEventValueChanged];
    [sliderLGeq19 setBackgroundColor:[UIColor clearColor]];
    [sliderLGeq19 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq19 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq19 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderLGeq19.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderLGeq19.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderLGeq19.continuous = YES;
    sliderLGeq19.value = 0.0;
    [viewGeqFrameL addSubview:sliderLGeq19];
    
    sliderLGeq20 = [[UISlider alloc] initWithFrame:CGRectMake(381, 100, 230, 12)];
    sliderLGeq20.transform = CGAffineTransformRotate(sliderLGeq20.transform, 270.0/180*M_PI);
    [sliderLGeq20 addTarget:self action:@selector(onSlider20Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderLGeq20 addTarget:self action:@selector(onSlider20End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderLGeq20 addTarget:self action:@selector(onSlider20End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderLGeq20 addTarget:self action:@selector(onSlider20Action:) forControlEvents:UIControlEventValueChanged];
    [sliderLGeq20 setBackgroundColor:[UIColor clearColor]];
    [sliderLGeq20 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq20 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq20 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderLGeq20.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderLGeq20.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderLGeq20.continuous = YES;
    sliderLGeq20.value = 0.0;
    [viewGeqFrameL addSubview:sliderLGeq20];
    
    sliderLGeq21 = [[UISlider alloc] initWithFrame:CGRectMake(406, 100, 230, 12)];
    sliderLGeq21.transform = CGAffineTransformRotate(sliderLGeq21.transform, 270.0/180*M_PI);
    [sliderLGeq21 addTarget:self action:@selector(onSlider21Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderLGeq21 addTarget:self action:@selector(onSlider21End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderLGeq21 addTarget:self action:@selector(onSlider21End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderLGeq21 addTarget:self action:@selector(onSlider21Action:) forControlEvents:UIControlEventValueChanged];
    [sliderLGeq21 setBackgroundColor:[UIColor clearColor]];
    [sliderLGeq21 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq21 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq21 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderLGeq21.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderLGeq21.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderLGeq21.continuous = YES;
    sliderLGeq21.value = 0.0;
    [viewGeqFrameL addSubview:sliderLGeq21];
    
    sliderLGeq22 = [[UISlider alloc] initWithFrame:CGRectMake(431, 100, 230, 12)];
    sliderLGeq22.transform = CGAffineTransformRotate(sliderLGeq22.transform, 270.0/180*M_PI);
    [sliderLGeq22 addTarget:self action:@selector(onSlider22Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderLGeq22 addTarget:self action:@selector(onSlider22End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderLGeq22 addTarget:self action:@selector(onSlider22End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderLGeq22 addTarget:self action:@selector(onSlider22Action:) forControlEvents:UIControlEventValueChanged];
    [sliderLGeq22 setBackgroundColor:[UIColor clearColor]];
    [sliderLGeq22 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq22 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq22 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderLGeq22.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderLGeq22.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderLGeq22.continuous = YES;
    sliderLGeq22.value = 0.0;
    [viewGeqFrameL addSubview:sliderLGeq22];
    
    sliderLGeq23 = [[UISlider alloc] initWithFrame:CGRectMake(456, 100, 230, 12)];
    sliderLGeq23.transform = CGAffineTransformRotate(sliderLGeq23.transform, 270.0/180*M_PI);
    [sliderLGeq23 addTarget:self action:@selector(onSlider23Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderLGeq23 addTarget:self action:@selector(onSlider23End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderLGeq23 addTarget:self action:@selector(onSlider23End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderLGeq23 addTarget:self action:@selector(onSlider23Action:) forControlEvents:UIControlEventValueChanged];
    [sliderLGeq23 setBackgroundColor:[UIColor clearColor]];
    [sliderLGeq23 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq23 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq23 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderLGeq23.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderLGeq23.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderLGeq23.continuous = YES;
    sliderLGeq23.value = 0.0;
    [viewGeqFrameL addSubview:sliderLGeq23];
    
    sliderLGeq24 = [[UISlider alloc] initWithFrame:CGRectMake(481, 100, 230, 12)];
    sliderLGeq24.transform = CGAffineTransformRotate(sliderLGeq24.transform, 270.0/180*M_PI);
    [sliderLGeq24 addTarget:self action:@selector(onSlider24Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderLGeq24 addTarget:self action:@selector(onSlider24End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderLGeq24 addTarget:self action:@selector(onSlider24End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderLGeq24 addTarget:self action:@selector(onSlider24Action:) forControlEvents:UIControlEventValueChanged];
    [sliderLGeq24 setBackgroundColor:[UIColor clearColor]];
    [sliderLGeq24 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq24 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq24 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderLGeq24.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderLGeq24.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderLGeq24.continuous = YES;
    sliderLGeq24.value = 0.0;
    [viewGeqFrameL addSubview:sliderLGeq24];
    
    sliderLGeq25 = [[UISlider alloc] initWithFrame:CGRectMake(506, 100, 230, 12)];
    sliderLGeq25.transform = CGAffineTransformRotate(sliderLGeq25.transform, 270.0/180*M_PI);
    [sliderLGeq25 addTarget:self action:@selector(onSlider25Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderLGeq25 addTarget:self action:@selector(onSlider25End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderLGeq25 addTarget:self action:@selector(onSlider25End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderLGeq25 addTarget:self action:@selector(onSlider25Action:) forControlEvents:UIControlEventValueChanged];
    [sliderLGeq25 setBackgroundColor:[UIColor clearColor]];
    [sliderLGeq25 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq25 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq25 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderLGeq25.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderLGeq25.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderLGeq25.continuous = YES;
    sliderLGeq25.value = 0.0;
    [viewGeqFrameL addSubview:sliderLGeq25];
    
    sliderLGeq26 = [[UISlider alloc] initWithFrame:CGRectMake(531, 100, 230, 12)];
    sliderLGeq26.transform = CGAffineTransformRotate(sliderLGeq26.transform, 270.0/180*M_PI);
    [sliderLGeq26 addTarget:self action:@selector(onSlider26Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderLGeq26 addTarget:self action:@selector(onSlider26End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderLGeq26 addTarget:self action:@selector(onSlider26End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderLGeq26 addTarget:self action:@selector(onSlider26Action:) forControlEvents:UIControlEventValueChanged];
    [sliderLGeq26 setBackgroundColor:[UIColor clearColor]];
    [sliderLGeq26 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq26 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq26 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderLGeq26.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderLGeq26.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderLGeq26.continuous = YES;
    sliderLGeq26.value = 0.0;
    [viewGeqFrameL addSubview:sliderLGeq26];
    
    sliderLGeq27 = [[UISlider alloc] initWithFrame:CGRectMake(556, 100, 230, 12)];
    sliderLGeq27.transform = CGAffineTransformRotate(sliderLGeq27.transform, 270.0/180*M_PI);
    [sliderLGeq27 addTarget:self action:@selector(onSlider27Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderLGeq27 addTarget:self action:@selector(onSlider27End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderLGeq27 addTarget:self action:@selector(onSlider27End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderLGeq27 addTarget:self action:@selector(onSlider27Action:) forControlEvents:UIControlEventValueChanged];
    [sliderLGeq27 setBackgroundColor:[UIColor clearColor]];
    [sliderLGeq27 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq27 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq27 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderLGeq27.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderLGeq27.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderLGeq27.continuous = YES;
    sliderLGeq27.value = 0.0;
    [viewGeqFrameL addSubview:sliderLGeq27];
    
    sliderLGeq28 = [[UISlider alloc] initWithFrame:CGRectMake(581, 100, 230, 12)];
    sliderLGeq28.transform = CGAffineTransformRotate(sliderLGeq28.transform, 270.0/180*M_PI);
    [sliderLGeq28 addTarget:self action:@selector(onSlider28Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderLGeq28 addTarget:self action:@selector(onSlider28End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderLGeq28 addTarget:self action:@selector(onSlider28End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderLGeq28 addTarget:self action:@selector(onSlider28Action:) forControlEvents:UIControlEventValueChanged];
    [sliderLGeq28 setBackgroundColor:[UIColor clearColor]];
    [sliderLGeq28 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq28 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq28 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderLGeq28.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderLGeq28.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderLGeq28.continuous = YES;
    sliderLGeq28.value = 0.0;
    [viewGeqFrameL addSubview:sliderLGeq28];
    
    sliderLGeq29 = [[UISlider alloc] initWithFrame:CGRectMake(606, 100, 230, 12)];
    sliderLGeq29.transform = CGAffineTransformRotate(sliderLGeq29.transform, 270.0/180*M_PI);
    [sliderLGeq29 addTarget:self action:@selector(onSlider29Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderLGeq29 addTarget:self action:@selector(onSlider29End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderLGeq29 addTarget:self action:@selector(onSlider29End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderLGeq29 addTarget:self action:@selector(onSlider29Action:) forControlEvents:UIControlEventValueChanged];
    [sliderLGeq29 setBackgroundColor:[UIColor clearColor]];
    [sliderLGeq29 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq29 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq29 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderLGeq29.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderLGeq29.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderLGeq29.continuous = YES;
    sliderLGeq29.value = 0.0;
    [viewGeqFrameL addSubview:sliderLGeq29];
    
    sliderLGeq30 = [[UISlider alloc] initWithFrame:CGRectMake(631, 100, 230, 12)];
    sliderLGeq30.transform = CGAffineTransformRotate(sliderLGeq30.transform, 270.0/180*M_PI);
    [sliderLGeq30 addTarget:self action:@selector(onSlider30Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderLGeq30 addTarget:self action:@selector(onSlider30End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderLGeq30 addTarget:self action:@selector(onSlider30End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderLGeq30 addTarget:self action:@selector(onSlider30Action:) forControlEvents:UIControlEventValueChanged];
    [sliderLGeq30 setBackgroundColor:[UIColor clearColor]];
    [sliderLGeq30 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq30 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq30 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderLGeq30.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderLGeq30.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderLGeq30.continuous = YES;
    sliderLGeq30.value = 0.0;
    [viewGeqFrameL addSubview:sliderLGeq30];
    
    sliderLGeq31 = [[UISlider alloc] initWithFrame:CGRectMake(656, 100, 230, 12)];
    sliderLGeq31.transform = CGAffineTransformRotate(sliderLGeq31.transform, 270.0/180*M_PI);
    [sliderLGeq31 addTarget:self action:@selector(onSlider31Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderLGeq31 addTarget:self action:@selector(onSlider31End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderLGeq31 addTarget:self action:@selector(onSlider31End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderLGeq31 addTarget:self action:@selector(onSlider31Action:) forControlEvents:UIControlEventValueChanged];
    [sliderLGeq31 setBackgroundColor:[UIColor clearColor]];
    [sliderLGeq31 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq31 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq31 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderLGeq31.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderLGeq31.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderLGeq31.continuous = YES;
    sliderLGeq31.value = 0.0;
    [viewGeqFrameL addSubview:sliderLGeq31];
    
    // Add R sliders
    sliderRGeq1 = [[UISlider alloc] initWithFrame:CGRectMake(-94, 100, 230, 12)];
    sliderRGeq1.transform = CGAffineTransformRotate(sliderRGeq1.transform, 270.0/180*M_PI);
    [sliderRGeq1 addTarget:self action:@selector(onSlider1Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderRGeq1 addTarget:self action:@selector(onSlider1End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderRGeq1 addTarget:self action:@selector(onSlider1End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderRGeq1 addTarget:self action:@selector(onSlider1Action:) forControlEvents:UIControlEventValueChanged];
    [sliderRGeq1 setBackgroundColor:[UIColor clearColor]];
    [sliderRGeq1 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq1 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq1 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderRGeq1.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderRGeq1.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderRGeq1.continuous = YES;
    sliderRGeq1.value = 0.0;
    [viewGeqFrameR addSubview:sliderRGeq1];
    
    sliderRGeq2 = [[UISlider alloc] initWithFrame:CGRectMake(-69, 100, 230, 12)];
    sliderRGeq2.transform = CGAffineTransformRotate(sliderRGeq2.transform, 270.0/180*M_PI);
    [sliderRGeq2 addTarget:self action:@selector(onSlider2Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderRGeq2 addTarget:self action:@selector(onSlider2End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderRGeq2 addTarget:self action:@selector(onSlider2End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderRGeq2 addTarget:self action:@selector(onSlider2Action:) forControlEvents:UIControlEventValueChanged];
    [sliderRGeq2 setBackgroundColor:[UIColor clearColor]];
    [sliderRGeq2 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq2 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq2 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderRGeq2.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderRGeq2.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderRGeq2.continuous = YES;
    sliderRGeq2.value = 0.0;
    [viewGeqFrameR addSubview:sliderRGeq2];
    
    sliderRGeq3 = [[UISlider alloc] initWithFrame:CGRectMake(-44, 100, 230, 12)];
    sliderRGeq3.transform = CGAffineTransformRotate(sliderRGeq3.transform, 270.0/180*M_PI);
    [sliderRGeq3 addTarget:self action:@selector(onSlider3Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderRGeq3 addTarget:self action:@selector(onSlider3End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderRGeq3 addTarget:self action:@selector(onSlider3End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderRGeq3 addTarget:self action:@selector(onSlider3Action:) forControlEvents:UIControlEventValueChanged];
    [sliderRGeq3 setBackgroundColor:[UIColor clearColor]];
    [sliderRGeq3 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq3 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq3 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderRGeq3.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderRGeq3.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderRGeq3.continuous = YES;
    sliderRGeq3.value = 0.0;
    [viewGeqFrameR addSubview:sliderRGeq3];
    
    sliderRGeq4 = [[UISlider alloc] initWithFrame:CGRectMake(-19, 100, 230, 12)];
    sliderRGeq4.transform = CGAffineTransformRotate(sliderRGeq4.transform, 270.0/180*M_PI);
    [sliderRGeq4 addTarget:self action:@selector(onSlider4Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderRGeq4 addTarget:self action:@selector(onSlider4End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderRGeq4 addTarget:self action:@selector(onSlider4End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderRGeq4 addTarget:self action:@selector(onSlider4Action:) forControlEvents:UIControlEventValueChanged];
    [sliderRGeq4 setBackgroundColor:[UIColor clearColor]];
    [sliderRGeq4 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq4 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq4 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderRGeq4.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderRGeq4.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderRGeq4.continuous = YES;
    sliderRGeq4.value = 0.0;
    [viewGeqFrameR addSubview:sliderRGeq4];
    
    sliderRGeq5 = [[UISlider alloc] initWithFrame:CGRectMake(6, 100, 230, 12)];
    sliderRGeq5.transform = CGAffineTransformRotate(sliderRGeq5.transform, 270.0/180*M_PI);
    [sliderRGeq5 addTarget:self action:@selector(onSlider5Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderRGeq5 addTarget:self action:@selector(onSlider5End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderRGeq5 addTarget:self action:@selector(onSlider5End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderRGeq5 addTarget:self action:@selector(onSlider5Action:) forControlEvents:UIControlEventValueChanged];
    [sliderRGeq5 setBackgroundColor:[UIColor clearColor]];
    [sliderRGeq5 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq5 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq5 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderRGeq5.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderRGeq5.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderRGeq5.continuous = YES;
    sliderRGeq5.value = 0.0;
    [viewGeqFrameR addSubview:sliderRGeq5];
    
    sliderRGeq6 = [[UISlider alloc] initWithFrame:CGRectMake(31, 100, 230, 12)];
    sliderRGeq6.transform = CGAffineTransformRotate(sliderRGeq6.transform, 270.0/180*M_PI);
    [sliderRGeq6 addTarget:self action:@selector(onSlider6Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderRGeq6 addTarget:self action:@selector(onSlider6End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderRGeq6 addTarget:self action:@selector(onSlider6End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderRGeq6 addTarget:self action:@selector(onSlider6Action:) forControlEvents:UIControlEventValueChanged];
    [sliderRGeq6 setBackgroundColor:[UIColor clearColor]];
    [sliderRGeq6 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq6 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq6 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderRGeq6.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderRGeq6.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderRGeq6.continuous = YES;
    sliderRGeq6.value = 0.0;
    [viewGeqFrameR addSubview:sliderRGeq6];
    
    sliderRGeq7 = [[UISlider alloc] initWithFrame:CGRectMake(56, 100, 230, 12)];
    sliderRGeq7.transform = CGAffineTransformRotate(sliderRGeq7.transform, 270.0/180*M_PI);
    [sliderRGeq7 addTarget:self action:@selector(onSlider7Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderRGeq7 addTarget:self action:@selector(onSlider7End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderRGeq7 addTarget:self action:@selector(onSlider7End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderRGeq7 addTarget:self action:@selector(onSlider7Action:) forControlEvents:UIControlEventValueChanged];
    [sliderRGeq7 setBackgroundColor:[UIColor clearColor]];
    [sliderRGeq7 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq7 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq7 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderRGeq7.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderRGeq7.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderRGeq7.continuous = YES;
    sliderRGeq7.value = 0.0;
    [viewGeqFrameR addSubview:sliderRGeq7];
    
    sliderRGeq8 = [[UISlider alloc] initWithFrame:CGRectMake(81, 100, 230, 12)];
    sliderRGeq8.transform = CGAffineTransformRotate(sliderRGeq8.transform, 270.0/180*M_PI);
    [sliderRGeq8 addTarget:self action:@selector(onSlider8Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderRGeq8 addTarget:self action:@selector(onSlider8End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderRGeq8 addTarget:self action:@selector(onSlider8End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderRGeq8 addTarget:self action:@selector(onSlider8Action:) forControlEvents:UIControlEventValueChanged];
    [sliderRGeq8 setBackgroundColor:[UIColor clearColor]];
    [sliderRGeq8 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq8 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq8 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderRGeq8.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderRGeq8.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderRGeq8.continuous = YES;
    sliderRGeq8.value = 0.0;
    [viewGeqFrameR addSubview:sliderRGeq8];
    
    sliderRGeq9 = [[UISlider alloc] initWithFrame:CGRectMake(106, 100, 230, 12)];
    sliderRGeq9.transform = CGAffineTransformRotate(sliderRGeq9.transform, 270.0/180*M_PI);
    [sliderRGeq9 addTarget:self action:@selector(onSlider9Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderRGeq9 addTarget:self action:@selector(onSlider9End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderRGeq9 addTarget:self action:@selector(onSlider9End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderRGeq9 addTarget:self action:@selector(onSlider9Action:) forControlEvents:UIControlEventValueChanged];
    [sliderRGeq9 setBackgroundColor:[UIColor clearColor]];
    [sliderRGeq9 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq9 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq9 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderRGeq9.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderRGeq9.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderRGeq9.continuous = YES;
    sliderRGeq9.value = 0.0;
    [viewGeqFrameR addSubview:sliderRGeq9];
    
    sliderRGeq10 = [[UISlider alloc] initWithFrame:CGRectMake(131, 100, 230, 12)];
    sliderRGeq10.transform = CGAffineTransformRotate(sliderRGeq10.transform, 270.0/180*M_PI);
    [sliderRGeq10 addTarget:self action:@selector(onSlider10Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderRGeq10 addTarget:self action:@selector(onSlider10End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderRGeq10 addTarget:self action:@selector(onSlider10End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderRGeq10 addTarget:self action:@selector(onSlider10Action:) forControlEvents:UIControlEventValueChanged];
    [sliderRGeq10 setBackgroundColor:[UIColor clearColor]];
    [sliderRGeq10 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq10 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq10 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderRGeq10.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderRGeq10.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderRGeq10.continuous = YES;
    sliderRGeq10.value = 0.0;
    [viewGeqFrameR addSubview:sliderRGeq10];
    
    sliderRGeq11 = [[UISlider alloc] initWithFrame:CGRectMake(156, 100, 230, 12)];
    sliderRGeq11.transform = CGAffineTransformRotate(sliderRGeq11.transform, 270.0/180*M_PI);
    [sliderRGeq11 addTarget:self action:@selector(onSlider11Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderRGeq11 addTarget:self action:@selector(onSlider11End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderRGeq11 addTarget:self action:@selector(onSlider11End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderRGeq11 addTarget:self action:@selector(onSlider11Action:) forControlEvents:UIControlEventValueChanged];
    [sliderRGeq11 setBackgroundColor:[UIColor clearColor]];
    [sliderRGeq11 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq11 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq11 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderRGeq11.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderRGeq11.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderRGeq11.continuous = YES;
    sliderRGeq11.value = 0.0;
    [viewGeqFrameR addSubview:sliderRGeq11];
    
    sliderRGeq12 = [[UISlider alloc] initWithFrame:CGRectMake(181, 100, 230, 12)];
    sliderRGeq12.transform = CGAffineTransformRotate(sliderRGeq12.transform, 270.0/180*M_PI);
    [sliderRGeq12 addTarget:self action:@selector(onSlider12Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderRGeq12 addTarget:self action:@selector(onSlider12End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderRGeq12 addTarget:self action:@selector(onSlider12End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderRGeq12 addTarget:self action:@selector(onSlider12Action:) forControlEvents:UIControlEventValueChanged];
    [sliderRGeq12 setBackgroundColor:[UIColor clearColor]];
    [sliderRGeq12 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq12 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq12 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderRGeq12.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderRGeq12.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderRGeq12.continuous = YES;
    sliderRGeq12.value = 0.0;
    [viewGeqFrameR addSubview:sliderRGeq12];
    
    sliderRGeq13 = [[UISlider alloc] initWithFrame:CGRectMake(206, 100, 230, 12)];
    sliderRGeq13.transform = CGAffineTransformRotate(sliderRGeq13.transform, 270.0/180*M_PI);
    [sliderRGeq13 addTarget:self action:@selector(onSlider13Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderRGeq13 addTarget:self action:@selector(onSlider13End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderRGeq13 addTarget:self action:@selector(onSlider13End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderRGeq13 addTarget:self action:@selector(onSlider13Action:) forControlEvents:UIControlEventValueChanged];
    [sliderRGeq13 setBackgroundColor:[UIColor clearColor]];
    [sliderRGeq13 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq13 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq13 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderRGeq13.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderRGeq13.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderRGeq13.continuous = YES;
    sliderRGeq13.value = 0.0;
    [viewGeqFrameR addSubview:sliderRGeq13];
    
    sliderRGeq14 = [[UISlider alloc] initWithFrame:CGRectMake(231, 100, 230, 12)];
    sliderRGeq14.transform = CGAffineTransformRotate(sliderRGeq14.transform, 270.0/180*M_PI);
    [sliderRGeq14 addTarget:self action:@selector(onSlider14Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderRGeq14 addTarget:self action:@selector(onSlider14End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderRGeq14 addTarget:self action:@selector(onSlider14End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderRGeq14 addTarget:self action:@selector(onSlider14Action:) forControlEvents:UIControlEventValueChanged];
    [sliderRGeq14 setBackgroundColor:[UIColor clearColor]];
    [sliderRGeq14 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq14 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq14 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderRGeq14.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderRGeq14.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderRGeq14.continuous = YES;
    sliderRGeq14.value = 0.0;
    [viewGeqFrameR addSubview:sliderRGeq14];
    
    sliderRGeq15 = [[UISlider alloc] initWithFrame:CGRectMake(256, 100, 230, 12)];
    sliderRGeq15.transform = CGAffineTransformRotate(sliderRGeq15.transform, 270.0/180*M_PI);
    [sliderRGeq15 addTarget:self action:@selector(onSlider15Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderRGeq15 addTarget:self action:@selector(onSlider15End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderRGeq15 addTarget:self action:@selector(onSlider15End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderRGeq15 addTarget:self action:@selector(onSlider15Action:) forControlEvents:UIControlEventValueChanged];
    [sliderRGeq15 setBackgroundColor:[UIColor clearColor]];
    [sliderRGeq15 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq15 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq15 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderRGeq15.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderRGeq15.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderRGeq15.continuous = YES;
    sliderRGeq15.value = 0.0;
    [viewGeqFrameR addSubview:sliderRGeq15];
    
    sliderRGeq16 = [[UISlider alloc] initWithFrame:CGRectMake(281, 100, 230, 12)];
    sliderRGeq16.transform = CGAffineTransformRotate(sliderRGeq16.transform, 270.0/180*M_PI);
    [sliderRGeq16 addTarget:self action:@selector(onSlider16Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderRGeq16 addTarget:self action:@selector(onSlider16End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderRGeq16 addTarget:self action:@selector(onSlider16End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderRGeq16 addTarget:self action:@selector(onSlider16Action:) forControlEvents:UIControlEventValueChanged];
    [sliderRGeq16 setBackgroundColor:[UIColor clearColor]];
    [sliderRGeq16 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq16 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq16 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderRGeq16.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderRGeq16.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderRGeq16.continuous = YES;
    sliderRGeq16.value = 0.0;
    [viewGeqFrameR addSubview:sliderRGeq16];
    
    sliderRGeq17 = [[UISlider alloc] initWithFrame:CGRectMake(306, 100, 230, 12)];
    sliderRGeq17.transform = CGAffineTransformRotate(sliderRGeq17.transform, 270.0/180*M_PI);
    [sliderRGeq17 addTarget:self action:@selector(onSlider17Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderRGeq17 addTarget:self action:@selector(onSlider17End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderRGeq17 addTarget:self action:@selector(onSlider17End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderRGeq17 addTarget:self action:@selector(onSlider17Action:) forControlEvents:UIControlEventValueChanged];
    [sliderRGeq17 setBackgroundColor:[UIColor clearColor]];
    [sliderRGeq17 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq17 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq17 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderRGeq17.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderRGeq17.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderRGeq17.continuous = YES;
    sliderRGeq17.value = 0.0;
    [viewGeqFrameR addSubview:sliderRGeq17];
    
    sliderRGeq18 = [[UISlider alloc] initWithFrame:CGRectMake(331, 100, 230, 12)];
    sliderRGeq18.transform = CGAffineTransformRotate(sliderRGeq18.transform, 270.0/180*M_PI);
    [sliderRGeq18 addTarget:self action:@selector(onSlider18Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderRGeq18 addTarget:self action:@selector(onSlider18End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderRGeq18 addTarget:self action:@selector(onSlider18End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderRGeq18 addTarget:self action:@selector(onSlider18Action:) forControlEvents:UIControlEventValueChanged];
    [sliderRGeq18 setBackgroundColor:[UIColor clearColor]];
    [sliderRGeq18 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq18 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq18 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderRGeq18.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderRGeq18.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderRGeq18.continuous = YES;
    sliderRGeq18.value = 0.0;
    [viewGeqFrameR addSubview:sliderRGeq18];
    
    sliderRGeq19 = [[UISlider alloc] initWithFrame:CGRectMake(356, 100, 230, 12)];
    sliderRGeq19.transform = CGAffineTransformRotate(sliderRGeq19.transform, 270.0/180*M_PI);
    [sliderRGeq19 addTarget:self action:@selector(onSlider19Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderRGeq19 addTarget:self action:@selector(onSlider19End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderRGeq19 addTarget:self action:@selector(onSlider19End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderRGeq19 addTarget:self action:@selector(onSlider19Action:) forControlEvents:UIControlEventValueChanged];
    [sliderRGeq19 setBackgroundColor:[UIColor clearColor]];
    [sliderRGeq19 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq19 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq19 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderRGeq19.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderRGeq19.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderRGeq19.continuous = YES;
    sliderRGeq19.value = 0.0;
    [viewGeqFrameR addSubview:sliderRGeq19];
    
    sliderRGeq20 = [[UISlider alloc] initWithFrame:CGRectMake(381, 100, 230, 12)];
    sliderRGeq20.transform = CGAffineTransformRotate(sliderRGeq20.transform, 270.0/180*M_PI);
    [sliderRGeq20 addTarget:self action:@selector(onSlider20Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderRGeq20 addTarget:self action:@selector(onSlider20End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderRGeq20 addTarget:self action:@selector(onSlider20End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderRGeq20 addTarget:self action:@selector(onSlider20Action:) forControlEvents:UIControlEventValueChanged];
    [sliderRGeq20 setBackgroundColor:[UIColor clearColor]];
    [sliderRGeq20 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq20 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq20 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderRGeq20.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderRGeq20.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderRGeq20.continuous = YES;
    sliderRGeq20.value = 0.0;
    [viewGeqFrameR addSubview:sliderRGeq20];
    
    sliderRGeq21 = [[UISlider alloc] initWithFrame:CGRectMake(406, 100, 230, 12)];
    sliderRGeq21.transform = CGAffineTransformRotate(sliderRGeq21.transform, 270.0/180*M_PI);
    [sliderRGeq21 addTarget:self action:@selector(onSlider21Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderRGeq21 addTarget:self action:@selector(onSlider21End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderRGeq21 addTarget:self action:@selector(onSlider21End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderRGeq21 addTarget:self action:@selector(onSlider21Action:) forControlEvents:UIControlEventValueChanged];
    [sliderRGeq21 setBackgroundColor:[UIColor clearColor]];
    [sliderRGeq21 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq21 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq21 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderRGeq21.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderRGeq21.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderRGeq21.continuous = YES;
    sliderRGeq21.value = 0.0;
    [viewGeqFrameR addSubview:sliderRGeq21];
    
    sliderRGeq22 = [[UISlider alloc] initWithFrame:CGRectMake(431, 100, 230, 12)];
    sliderRGeq22.transform = CGAffineTransformRotate(sliderRGeq22.transform, 270.0/180*M_PI);
    [sliderRGeq22 addTarget:self action:@selector(onSlider22Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderRGeq22 addTarget:self action:@selector(onSlider22End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderRGeq22 addTarget:self action:@selector(onSlider22End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderRGeq22 addTarget:self action:@selector(onSlider22Action:) forControlEvents:UIControlEventValueChanged];
    [sliderRGeq22 setBackgroundColor:[UIColor clearColor]];
    [sliderRGeq22 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq22 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq22 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderRGeq22.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderRGeq22.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderRGeq22.continuous = YES;
    sliderRGeq22.value = 0.0;
    [viewGeqFrameR addSubview:sliderRGeq22];
    
    sliderRGeq23 = [[UISlider alloc] initWithFrame:CGRectMake(456, 100, 230, 12)];
    sliderRGeq23.transform = CGAffineTransformRotate(sliderRGeq23.transform, 270.0/180*M_PI);
    [sliderRGeq23 addTarget:self action:@selector(onSlider23Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderRGeq23 addTarget:self action:@selector(onSlider23End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderRGeq23 addTarget:self action:@selector(onSlider23End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderRGeq23 addTarget:self action:@selector(onSlider23Action:) forControlEvents:UIControlEventValueChanged];
    [sliderRGeq23 setBackgroundColor:[UIColor clearColor]];
    [sliderRGeq23 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq23 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq23 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderRGeq23.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderRGeq23.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderRGeq23.continuous = YES;
    sliderRGeq23.value = 0.0;
    [viewGeqFrameR addSubview:sliderRGeq23];
    
    sliderRGeq24 = [[UISlider alloc] initWithFrame:CGRectMake(481, 100, 230, 12)];
    sliderRGeq24.transform = CGAffineTransformRotate(sliderRGeq24.transform, 270.0/180*M_PI);
    [sliderRGeq24 addTarget:self action:@selector(onSlider24Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderRGeq24 addTarget:self action:@selector(onSlider24End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderRGeq24 addTarget:self action:@selector(onSlider24End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderRGeq24 addTarget:self action:@selector(onSlider24Action:) forControlEvents:UIControlEventValueChanged];
    [sliderRGeq24 setBackgroundColor:[UIColor clearColor]];
    [sliderRGeq24 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq24 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq24 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderRGeq24.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderRGeq24.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderRGeq24.continuous = YES;
    sliderRGeq24.value = 0.0;
    [viewGeqFrameR addSubview:sliderRGeq24];
    
    sliderRGeq25 = [[UISlider alloc] initWithFrame:CGRectMake(506, 100, 230, 12)];
    sliderRGeq25.transform = CGAffineTransformRotate(sliderRGeq25.transform, 270.0/180*M_PI);
    [sliderRGeq25 addTarget:self action:@selector(onSlider25Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderRGeq25 addTarget:self action:@selector(onSlider25End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderRGeq25 addTarget:self action:@selector(onSlider25End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderRGeq25 addTarget:self action:@selector(onSlider25Action:) forControlEvents:UIControlEventValueChanged];
    [sliderRGeq25 setBackgroundColor:[UIColor clearColor]];
    [sliderRGeq25 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq25 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq25 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderRGeq25.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderRGeq25.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderRGeq25.continuous = YES;
    sliderRGeq25.value = 0.0;
    [viewGeqFrameR addSubview:sliderRGeq25];
    
    sliderRGeq26 = [[UISlider alloc] initWithFrame:CGRectMake(531, 100, 230, 12)];
    sliderRGeq26.transform = CGAffineTransformRotate(sliderRGeq26.transform, 270.0/180*M_PI);
    [sliderRGeq26 addTarget:self action:@selector(onSlider26Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderRGeq26 addTarget:self action:@selector(onSlider26End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderRGeq26 addTarget:self action:@selector(onSlider26End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderRGeq26 addTarget:self action:@selector(onSlider26Action:) forControlEvents:UIControlEventValueChanged];
    [sliderRGeq26 setBackgroundColor:[UIColor clearColor]];
    [sliderRGeq26 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq26 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq26 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderRGeq26.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderRGeq26.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderRGeq26.continuous = YES;
    sliderRGeq26.value = 0.0;
    [viewGeqFrameR addSubview:sliderRGeq26];
    
    sliderRGeq27 = [[UISlider alloc] initWithFrame:CGRectMake(556, 100, 230, 12)];
    sliderRGeq27.transform = CGAffineTransformRotate(sliderRGeq27.transform, 270.0/180*M_PI);
    [sliderRGeq27 addTarget:self action:@selector(onSlider27Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderRGeq27 addTarget:self action:@selector(onSlider27End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderRGeq27 addTarget:self action:@selector(onSlider27End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderRGeq27 addTarget:self action:@selector(onSlider27Action:) forControlEvents:UIControlEventValueChanged];
    [sliderRGeq27 setBackgroundColor:[UIColor clearColor]];
    [sliderRGeq27 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq27 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq27 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderRGeq27.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderRGeq27.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderRGeq27.continuous = YES;
    sliderRGeq27.value = 0.0;
    [viewGeqFrameR addSubview:sliderRGeq27];
    
    sliderRGeq28 = [[UISlider alloc] initWithFrame:CGRectMake(581, 100, 230, 12)];
    sliderRGeq28.transform = CGAffineTransformRotate(sliderRGeq28.transform, 270.0/180*M_PI);
    [sliderRGeq28 addTarget:self action:@selector(onSlider28Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderRGeq28 addTarget:self action:@selector(onSlider28End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderRGeq28 addTarget:self action:@selector(onSlider28End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderRGeq28 addTarget:self action:@selector(onSlider28Action:) forControlEvents:UIControlEventValueChanged];
    [sliderRGeq28 setBackgroundColor:[UIColor clearColor]];
    [sliderRGeq28 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq28 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq28 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderRGeq28.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderRGeq28.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderRGeq28.continuous = YES;
    sliderRGeq28.value = 0.0;
    [viewGeqFrameR addSubview:sliderRGeq28];
    
    sliderRGeq29 = [[UISlider alloc] initWithFrame:CGRectMake(606, 100, 230, 12)];
    sliderRGeq29.transform = CGAffineTransformRotate(sliderRGeq29.transform, 270.0/180*M_PI);
    [sliderRGeq29 addTarget:self action:@selector(onSlider29Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderRGeq29 addTarget:self action:@selector(onSlider29End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderRGeq29 addTarget:self action:@selector(onSlider29End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderRGeq29 addTarget:self action:@selector(onSlider29Action:) forControlEvents:UIControlEventValueChanged];
    [sliderRGeq29 setBackgroundColor:[UIColor clearColor]];
    [sliderRGeq29 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq29 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq29 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderRGeq29.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderRGeq29.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderRGeq29.continuous = YES;
    sliderRGeq29.value = 0.0;
    [viewGeqFrameR addSubview:sliderRGeq29];
    
    sliderRGeq30 = [[UISlider alloc] initWithFrame:CGRectMake(631, 100, 230, 12)];
    sliderRGeq30.transform = CGAffineTransformRotate(sliderRGeq30.transform, 270.0/180*M_PI);
    [sliderRGeq30 addTarget:self action:@selector(onSlider30Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderRGeq30 addTarget:self action:@selector(onSlider30End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderRGeq30 addTarget:self action:@selector(onSlider30End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderRGeq30 addTarget:self action:@selector(onSlider30Action:) forControlEvents:UIControlEventValueChanged];
    [sliderRGeq30 setBackgroundColor:[UIColor clearColor]];
    [sliderRGeq30 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq30 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq30 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderRGeq30.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderRGeq30.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderRGeq30.continuous = YES;
    sliderRGeq30.value = 0.0;
    [viewGeqFrameR addSubview:sliderRGeq30];
    
    sliderRGeq31 = [[UISlider alloc] initWithFrame:CGRectMake(656, 100, 230, 12)];
    sliderRGeq31.transform = CGAffineTransformRotate(sliderRGeq31.transform, 270.0/180*M_PI);
    [sliderRGeq31 addTarget:self action:@selector(onSlider31Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderRGeq31 addTarget:self action:@selector(onSlider31End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderRGeq31 addTarget:self action:@selector(onSlider31End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderRGeq31 addTarget:self action:@selector(onSlider31Action:) forControlEvents:UIControlEventValueChanged];
    [sliderRGeq31 setBackgroundColor:[UIColor clearColor]];
    [sliderRGeq31 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq31 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq31 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderRGeq31.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderRGeq31.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderRGeq31.continuous = YES;
    sliderRGeq31.value = 0.0;
    [viewGeqFrameR addSubview:sliderRGeq31];
    
    // Add L slider meters
    meterL1Top = [[JEProgressView alloc] initWithFrame:CGRectMake(-33, 53, 108, 12)];
    meterL1Top.transform = CGAffineTransformRotate(meterL1Top.transform, 270.0/180*M_PI);
    [meterL1Top setProgressImage:progressImage];
    [meterL1Top setTrackImage:[UIImage alloc]];
    [meterL1Top setProgress:0.0];
    [viewGeqFrameL addSubview:meterL1Top];
    [viewGeqFrameL sendSubviewToBack:meterL1Top];
    
    meterL1Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(-33, 158, 108, 12)];
    meterL1Btm.transform = CGAffineTransformRotate(meterL1Btm.transform, 90.0/180*M_PI);
    [meterL1Btm setProgressImage:progressImage];
    [meterL1Btm setTrackImage:[UIImage alloc]];
    [meterL1Btm setProgress:0.0];
    [viewGeqFrameL addSubview:meterL1Btm];
    [viewGeqFrameL sendSubviewToBack:meterL1Btm];
    
    meterL2Top = [[JEProgressView alloc] initWithFrame:CGRectMake(-8, 53, 108, 12)];
    meterL2Top.transform = CGAffineTransformRotate(meterL2Top.transform, 270.0/180*M_PI);
    [meterL2Top setProgressImage:progressImage];
    [meterL2Top setTrackImage:[UIImage alloc]];
    [meterL2Top setProgress:0.0];
    [viewGeqFrameL addSubview:meterL2Top];
    [viewGeqFrameL sendSubviewToBack:meterL2Top];
    
    meterL2Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(-8, 158, 108, 12)];
    meterL2Btm.transform = CGAffineTransformRotate(meterL2Btm.transform, 90.0/180*M_PI);
    [meterL2Btm setProgressImage:progressImage];
    [meterL2Btm setTrackImage:[UIImage alloc]];
    [meterL2Btm setProgress:0.0];
    [viewGeqFrameL addSubview:meterL2Btm];
    [viewGeqFrameL sendSubviewToBack:meterL2Btm];
    
    meterL3Top = [[JEProgressView alloc] initWithFrame:CGRectMake(17, 53, 108, 12)];
    meterL3Top.transform = CGAffineTransformRotate(meterL3Top.transform, 270.0/180*M_PI);
    [meterL3Top setProgressImage:progressImage];
    [meterL3Top setTrackImage:[UIImage alloc]];
    [meterL3Top setProgress:0.0];
    [viewGeqFrameL addSubview:meterL3Top];
    [viewGeqFrameL sendSubviewToBack:meterL3Top];
    
    meterL3Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(17, 158, 108, 12)];
    meterL3Btm.transform = CGAffineTransformRotate(meterL3Btm.transform, 90.0/180*M_PI);
    [meterL3Btm setProgressImage:progressImage];
    [meterL3Btm setTrackImage:[UIImage alloc]];
    [meterL3Btm setProgress:0.0];
    [viewGeqFrameL addSubview:meterL3Btm];
    [viewGeqFrameL sendSubviewToBack:meterL3Btm];
    
    meterL4Top = [[JEProgressView alloc] initWithFrame:CGRectMake(42, 53, 108, 12)];
    meterL4Top.transform = CGAffineTransformRotate(meterL4Top.transform, 270.0/180*M_PI);
    [meterL4Top setProgressImage:progressImage];
    [meterL4Top setTrackImage:[UIImage alloc]];
    [meterL4Top setProgress:0.0];
    [viewGeqFrameL addSubview:meterL4Top];
    [viewGeqFrameL sendSubviewToBack:meterL4Top];
    
    meterL4Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(42, 158, 108, 12)];
    meterL4Btm.transform = CGAffineTransformRotate(meterL4Btm.transform, 90.0/180*M_PI);
    [meterL4Btm setProgressImage:progressImage];
    [meterL4Btm setTrackImage:[UIImage alloc]];
    [meterL4Btm setProgress:0.0];
    [viewGeqFrameL addSubview:meterL4Btm];
    [viewGeqFrameL sendSubviewToBack:meterL4Btm];
    
    meterL5Top = [[JEProgressView alloc] initWithFrame:CGRectMake(67, 53, 108, 12)];
    meterL5Top.transform = CGAffineTransformRotate(meterL5Top.transform, 270.0/180*M_PI);
    [meterL5Top setProgressImage:progressImage];
    [meterL5Top setTrackImage:[UIImage alloc]];
    [meterL5Top setProgress:0.0];
    [viewGeqFrameL addSubview:meterL5Top];
    [viewGeqFrameL sendSubviewToBack:meterL5Top];
    
    meterL5Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(67, 158, 108, 12)];
    meterL5Btm.transform = CGAffineTransformRotate(meterL5Btm.transform, 90.0/180*M_PI);
    [meterL5Btm setProgressImage:progressImage];
    [meterL5Btm setTrackImage:[UIImage alloc]];
    [meterL5Btm setProgress:0.0];
    [viewGeqFrameL addSubview:meterL5Btm];
    [viewGeqFrameL sendSubviewToBack:meterL5Btm];
    
    meterL6Top = [[JEProgressView alloc] initWithFrame:CGRectMake(92, 53, 108, 12)];
    meterL6Top.transform = CGAffineTransformRotate(meterL6Top.transform, 270.0/180*M_PI);
    [meterL6Top setProgressImage:progressImage];
    [meterL6Top setTrackImage:[UIImage alloc]];
    [meterL6Top setProgress:0.0];
    [viewGeqFrameL addSubview:meterL6Top];
    [viewGeqFrameL sendSubviewToBack:meterL6Top];
    
    meterL6Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(92, 158, 108, 12)];
    meterL6Btm.transform = CGAffineTransformRotate(meterL6Btm.transform, 90.0/180*M_PI);
    [meterL6Btm setProgressImage:progressImage];
    [meterL6Btm setTrackImage:[UIImage alloc]];
    [meterL6Btm setProgress:0.0];
    [viewGeqFrameL addSubview:meterL6Btm];
    [viewGeqFrameL sendSubviewToBack:meterL6Btm];
    
    meterL7Top = [[JEProgressView alloc] initWithFrame:CGRectMake(117, 53, 108, 12)];
    meterL7Top.transform = CGAffineTransformRotate(meterL7Top.transform, 270.0/180*M_PI);
    [meterL7Top setProgressImage:progressImage];
    [meterL7Top setTrackImage:[UIImage alloc]];
    [meterL7Top setProgress:0.0];
    [viewGeqFrameL addSubview:meterL7Top];
    [viewGeqFrameL sendSubviewToBack:meterL7Top];
    
    meterL7Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(117, 158, 108, 12)];
    meterL7Btm.transform = CGAffineTransformRotate(meterL7Btm.transform, 90.0/180*M_PI);
    [meterL7Btm setProgressImage:progressImage];
    [meterL7Btm setTrackImage:[UIImage alloc]];
    [meterL7Btm setProgress:0.0];
    [viewGeqFrameL addSubview:meterL7Btm];
    [viewGeqFrameL sendSubviewToBack:meterL7Btm];
    
    meterL8Top = [[JEProgressView alloc] initWithFrame:CGRectMake(142, 53, 108, 12)];
    meterL8Top.transform = CGAffineTransformRotate(meterL8Top.transform, 270.0/180*M_PI);
    [meterL8Top setProgressImage:progressImage];
    [meterL8Top setTrackImage:[UIImage alloc]];
    [meterL8Top setProgress:0.0];
    [viewGeqFrameL addSubview:meterL8Top];
    [viewGeqFrameL sendSubviewToBack:meterL8Top];
    
    meterL8Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(142, 158, 108, 12)];
    meterL8Btm.transform = CGAffineTransformRotate(meterL8Btm.transform, 90.0/180*M_PI);
    [meterL8Btm setProgressImage:progressImage];
    [meterL8Btm setTrackImage:[UIImage alloc]];
    [meterL8Btm setProgress:0.0];
    [viewGeqFrameL addSubview:meterL8Btm];
    [viewGeqFrameL sendSubviewToBack:meterL8Btm];
    
    meterL9Top = [[JEProgressView alloc] initWithFrame:CGRectMake(167, 53, 108, 12)];
    meterL9Top.transform = CGAffineTransformRotate(meterL9Top.transform, 270.0/180*M_PI);
    [meterL9Top setProgressImage:progressImage];
    [meterL9Top setTrackImage:[UIImage alloc]];
    [meterL9Top setProgress:0.0];
    [viewGeqFrameL addSubview:meterL9Top];
    [viewGeqFrameL sendSubviewToBack:meterL9Top];
    
    meterL9Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(167, 158, 108, 12)];
    meterL9Btm.transform = CGAffineTransformRotate(meterL9Btm.transform, 90.0/180*M_PI);
    [meterL9Btm setProgressImage:progressImage];
    [meterL9Btm setTrackImage:[UIImage alloc]];
    [meterL9Btm setProgress:0.0];
    [viewGeqFrameL addSubview:meterL9Btm];
    [viewGeqFrameL sendSubviewToBack:meterL9Btm];
    
    meterL10Top = [[JEProgressView alloc] initWithFrame:CGRectMake(192, 53, 108, 12)];
    meterL10Top.transform = CGAffineTransformRotate(meterL10Top.transform, 270.0/180*M_PI);
    [meterL10Top setProgressImage:progressImage];
    [meterL10Top setTrackImage:[UIImage alloc]];
    [meterL10Top setProgress:0.0];
    [viewGeqFrameL addSubview:meterL10Top];
    [viewGeqFrameL sendSubviewToBack:meterL10Top];
    
    meterL10Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(192, 158, 108, 12)];
    meterL10Btm.transform = CGAffineTransformRotate(meterL10Btm.transform, 90.0/180*M_PI);
    [meterL10Btm setProgressImage:progressImage];
    [meterL10Btm setTrackImage:[UIImage alloc]];
    [meterL10Btm setProgress:0.0];
    [viewGeqFrameL addSubview:meterL10Btm];
    [viewGeqFrameL sendSubviewToBack:meterL10Btm];
    
    meterL11Top = [[JEProgressView alloc] initWithFrame:CGRectMake(217, 53, 108, 12)];
    meterL11Top.transform = CGAffineTransformRotate(meterL11Top.transform, 270.0/180*M_PI);
    [meterL11Top setProgressImage:progressImage];
    [meterL11Top setTrackImage:[UIImage alloc]];
    [meterL11Top setProgress:0.0];
    [viewGeqFrameL addSubview:meterL11Top];
    [viewGeqFrameL sendSubviewToBack:meterL11Top];
    
    meterL11Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(217, 158, 108, 12)];
    meterL11Btm.transform = CGAffineTransformRotate(meterL11Btm.transform, 90.0/180*M_PI);
    [meterL11Btm setProgressImage:progressImage];
    [meterL11Btm setTrackImage:[UIImage alloc]];
    [meterL11Btm setProgress:0.0];
    [viewGeqFrameL addSubview:meterL11Btm];
    [viewGeqFrameL sendSubviewToBack:meterL11Btm];
    
    meterL12Top = [[JEProgressView alloc] initWithFrame:CGRectMake(242, 53, 108, 12)];
    meterL12Top.transform = CGAffineTransformRotate(meterL12Top.transform, 270.0/180*M_PI);
    [meterL12Top setProgressImage:progressImage];
    [meterL12Top setTrackImage:[UIImage alloc]];
    [meterL12Top setProgress:0.0];
    [viewGeqFrameL addSubview:meterL12Top];
    [viewGeqFrameL sendSubviewToBack:meterL12Top];
    
    meterL12Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(242, 158, 108, 12)];
    meterL12Btm.transform = CGAffineTransformRotate(meterL12Btm.transform, 90.0/180*M_PI);
    [meterL12Btm setProgressImage:progressImage];
    [meterL12Btm setTrackImage:[UIImage alloc]];
    [meterL12Btm setProgress:0.0];
    [viewGeqFrameL addSubview:meterL12Btm];
    [viewGeqFrameL sendSubviewToBack:meterL12Btm];
    
    meterL13Top = [[JEProgressView alloc] initWithFrame:CGRectMake(267, 53, 108, 12)];
    meterL13Top.transform = CGAffineTransformRotate(meterL13Top.transform, 270.0/180*M_PI);
    [meterL13Top setProgressImage:progressImage];
    [meterL13Top setTrackImage:[UIImage alloc]];
    [meterL13Top setProgress:0.0];
    [viewGeqFrameL addSubview:meterL13Top];
    [viewGeqFrameL sendSubviewToBack:meterL13Top];
    
    meterL13Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(267, 158, 108, 12)];
    meterL13Btm.transform = CGAffineTransformRotate(meterL13Btm.transform, 90.0/180*M_PI);
    [meterL13Btm setProgressImage:progressImage];
    [meterL13Btm setTrackImage:[UIImage alloc]];
    [meterL13Btm setProgress:0.0];
    [viewGeqFrameL addSubview:meterL13Btm];
    [viewGeqFrameL sendSubviewToBack:meterL13Btm];
    
    meterL14Top = [[JEProgressView alloc] initWithFrame:CGRectMake(292, 53, 108, 12)];
    meterL14Top.transform = CGAffineTransformRotate(meterL14Top.transform, 270.0/180*M_PI);
    [meterL14Top setProgressImage:progressImage];
    [meterL14Top setTrackImage:[UIImage alloc]];
    [meterL14Top setProgress:0.0];
    [viewGeqFrameL addSubview:meterL14Top];
    [viewGeqFrameL sendSubviewToBack:meterL14Top];
    
    meterL14Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(292, 158, 108, 12)];
    meterL14Btm.transform = CGAffineTransformRotate(meterL14Btm.transform, 90.0/180*M_PI);
    [meterL14Btm setProgressImage:progressImage];
    [meterL14Btm setTrackImage:[UIImage alloc]];
    [meterL14Btm setProgress:0.0];
    [viewGeqFrameL addSubview:meterL14Btm];
    [viewGeqFrameL sendSubviewToBack:meterL14Btm];
    
    meterL15Top = [[JEProgressView alloc] initWithFrame:CGRectMake(317, 53, 108, 12)];
    meterL15Top.transform = CGAffineTransformRotate(meterL15Top.transform, 270.0/180*M_PI);
    [meterL15Top setProgressImage:progressImage];
    [meterL15Top setTrackImage:[UIImage alloc]];
    [meterL15Top setProgress:0.0];
    [viewGeqFrameL addSubview:meterL15Top];
    [viewGeqFrameL sendSubviewToBack:meterL15Top];
    
    meterL15Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(317, 158, 108, 12)];
    meterL15Btm.transform = CGAffineTransformRotate(meterL15Btm.transform, 90.0/180*M_PI);
    [meterL15Btm setProgressImage:progressImage];
    [meterL15Btm setTrackImage:[UIImage alloc]];
    [meterL15Btm setProgress:0.0];
    [viewGeqFrameL addSubview:meterL15Btm];
    [viewGeqFrameL sendSubviewToBack:meterL15Btm];
    
    meterL16Top = [[JEProgressView alloc] initWithFrame:CGRectMake(342, 53, 108, 12)];
    meterL16Top.transform = CGAffineTransformRotate(meterL16Top.transform, 270.0/180*M_PI);
    [meterL16Top setProgressImage:progressImage];
    [meterL16Top setTrackImage:[UIImage alloc]];
    [meterL16Top setProgress:0.0];
    [viewGeqFrameL addSubview:meterL16Top];
    [viewGeqFrameL sendSubviewToBack:meterL16Top];
    
    meterL16Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(342, 158, 108, 12)];
    meterL16Btm.transform = CGAffineTransformRotate(meterL16Btm.transform, 90.0/180*M_PI);
    [meterL16Btm setProgressImage:progressImage];
    [meterL16Btm setTrackImage:[UIImage alloc]];
    [meterL16Btm setProgress:0.0];
    [viewGeqFrameL addSubview:meterL16Btm];
    [viewGeqFrameL sendSubviewToBack:meterL16Btm];
    
    meterL17Top = [[JEProgressView alloc] initWithFrame:CGRectMake(367, 53, 108, 12)];
    meterL17Top.transform = CGAffineTransformRotate(meterL17Top.transform, 270.0/180*M_PI);
    [meterL17Top setProgressImage:progressImage];
    [meterL17Top setTrackImage:[UIImage alloc]];
    [meterL17Top setProgress:0.0];
    [viewGeqFrameL addSubview:meterL17Top];
    [viewGeqFrameL sendSubviewToBack:meterL17Top];
    
    meterL17Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(367, 158, 108, 12)];
    meterL17Btm.transform = CGAffineTransformRotate(meterL17Btm.transform, 90.0/180*M_PI);
    [meterL17Btm setProgressImage:progressImage];
    [meterL17Btm setTrackImage:[UIImage alloc]];
    [meterL17Btm setProgress:0.0];
    [viewGeqFrameL addSubview:meterL17Btm];
    [viewGeqFrameL sendSubviewToBack:meterL17Btm];
    
    meterL18Top = [[JEProgressView alloc] initWithFrame:CGRectMake(392, 53, 108, 12)];
    meterL18Top.transform = CGAffineTransformRotate(meterL18Top.transform, 270.0/180*M_PI);
    [meterL18Top setProgressImage:progressImage];
    [meterL18Top setTrackImage:[UIImage alloc]];
    [meterL18Top setProgress:0.0];
    [viewGeqFrameL addSubview:meterL18Top];
    [viewGeqFrameL sendSubviewToBack:meterL18Top];
    
    meterL18Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(392, 158, 108, 12)];
    meterL18Btm.transform = CGAffineTransformRotate(meterL18Btm.transform, 90.0/180*M_PI);
    [meterL18Btm setProgressImage:progressImage];
    [meterL18Btm setTrackImage:[UIImage alloc]];
    [meterL18Btm setProgress:0.0];
    [viewGeqFrameL addSubview:meterL18Btm];
    [viewGeqFrameL sendSubviewToBack:meterL18Btm];
    
    meterL19Top = [[JEProgressView alloc] initWithFrame:CGRectMake(417, 53, 108, 12)];
    meterL19Top.transform = CGAffineTransformRotate(meterL19Top.transform, 270.0/180*M_PI);
    [meterL19Top setProgressImage:progressImage];
    [meterL19Top setTrackImage:[UIImage alloc]];
    [meterL19Top setProgress:0.0];
    [viewGeqFrameL addSubview:meterL19Top];
    [viewGeqFrameL sendSubviewToBack:meterL19Top];
    
    meterL19Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(417, 158, 108, 12)];
    meterL19Btm.transform = CGAffineTransformRotate(meterL19Btm.transform, 90.0/180*M_PI);
    [meterL19Btm setProgressImage:progressImage];
    [meterL19Btm setTrackImage:[UIImage alloc]];
    [meterL19Btm setProgress:0.0];
    [viewGeqFrameL addSubview:meterL19Btm];
    [viewGeqFrameL sendSubviewToBack:meterL19Btm];
    
    meterL20Top = [[JEProgressView alloc] initWithFrame:CGRectMake(442, 53, 108, 12)];
    meterL20Top.transform = CGAffineTransformRotate(meterL20Top.transform, 270.0/180*M_PI);
    [meterL20Top setProgressImage:progressImage];
    [meterL20Top setTrackImage:[UIImage alloc]];
    [meterL20Top setProgress:0.0];
    [viewGeqFrameL addSubview:meterL20Top];
    [viewGeqFrameL sendSubviewToBack:meterL20Top];
    
    meterL20Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(442, 158, 108, 12)];
    meterL20Btm.transform = CGAffineTransformRotate(meterL20Btm.transform, 90.0/180*M_PI);
    [meterL20Btm setProgressImage:progressImage];
    [meterL20Btm setTrackImage:[UIImage alloc]];
    [meterL20Btm setProgress:0.0];
    [viewGeqFrameL addSubview:meterL20Btm];
    [viewGeqFrameL sendSubviewToBack:meterL20Btm];
    
    meterL21Top = [[JEProgressView alloc] initWithFrame:CGRectMake(467, 53, 108, 12)];
    meterL21Top.transform = CGAffineTransformRotate(meterL21Top.transform, 270.0/180*M_PI);
    [meterL21Top setProgressImage:progressImage];
    [meterL21Top setTrackImage:[UIImage alloc]];
    [meterL21Top setProgress:0.0];
    [viewGeqFrameL addSubview:meterL21Top];
    [viewGeqFrameL sendSubviewToBack:meterL21Top];
    
    meterL21Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(467, 158, 108, 12)];
    meterL21Btm.transform = CGAffineTransformRotate(meterL21Btm.transform, 90.0/180*M_PI);
    [meterL21Btm setProgressImage:progressImage];
    [meterL21Btm setTrackImage:[UIImage alloc]];
    [meterL21Btm setProgress:0.0];
    [viewGeqFrameL addSubview:meterL21Btm];
    [viewGeqFrameL sendSubviewToBack:meterL21Btm];
    
    meterL22Top = [[JEProgressView alloc] initWithFrame:CGRectMake(492, 53, 108, 12)];
    meterL22Top.transform = CGAffineTransformRotate(meterL22Top.transform, 270.0/180*M_PI);
    [meterL22Top setProgressImage:progressImage];
    [meterL22Top setTrackImage:[UIImage alloc]];
    [meterL22Top setProgress:0.0];
    [viewGeqFrameL addSubview:meterL22Top];
    [viewGeqFrameL sendSubviewToBack:meterL22Top];
    
    meterL22Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(492, 158, 108, 12)];
    meterL22Btm.transform = CGAffineTransformRotate(meterL22Btm.transform, 90.0/180*M_PI);
    [meterL22Btm setProgressImage:progressImage];
    [meterL22Btm setTrackImage:[UIImage alloc]];
    [meterL22Btm setProgress:0.0];
    [viewGeqFrameL addSubview:meterL22Btm];
    [viewGeqFrameL sendSubviewToBack:meterL22Btm];
    
    meterL23Top = [[JEProgressView alloc] initWithFrame:CGRectMake(517, 53, 108, 12)];
    meterL23Top.transform = CGAffineTransformRotate(meterL23Top.transform, 270.0/180*M_PI);
    [meterL23Top setProgressImage:progressImage];
    [meterL23Top setTrackImage:[UIImage alloc]];
    [meterL23Top setProgress:0.0];
    [viewGeqFrameL addSubview:meterL23Top];
    [viewGeqFrameL sendSubviewToBack:meterL23Top];
    
    meterL23Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(517, 158, 108, 12)];
    meterL23Btm.transform = CGAffineTransformRotate(meterL23Btm.transform, 90.0/180*M_PI);
    [meterL23Btm setProgressImage:progressImage];
    [meterL23Btm setTrackImage:[UIImage alloc]];
    [meterL23Btm setProgress:0.0];
    [viewGeqFrameL addSubview:meterL23Btm];
    [viewGeqFrameL sendSubviewToBack:meterL23Btm];
    
    meterL24Top = [[JEProgressView alloc] initWithFrame:CGRectMake(542, 53, 108, 12)];
    meterL24Top.transform = CGAffineTransformRotate(meterL24Top.transform, 270.0/180*M_PI);
    [meterL24Top setProgressImage:progressImage];
    [meterL24Top setTrackImage:[UIImage alloc]];
    [meterL24Top setProgress:0.0];
    [viewGeqFrameL addSubview:meterL24Top];
    [viewGeqFrameL sendSubviewToBack:meterL24Top];
    
    meterL24Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(542, 158, 108, 12)];
    meterL24Btm.transform = CGAffineTransformRotate(meterL24Btm.transform, 90.0/180*M_PI);
    [meterL24Btm setProgressImage:progressImage];
    [meterL24Btm setTrackImage:[UIImage alloc]];
    [meterL24Btm setProgress:0.0];
    [viewGeqFrameL addSubview:meterL24Btm];
    [viewGeqFrameL sendSubviewToBack:meterL24Btm];
    
    meterL25Top = [[JEProgressView alloc] initWithFrame:CGRectMake(567, 53, 108, 12)];
    meterL25Top.transform = CGAffineTransformRotate(meterL25Top.transform, 270.0/180*M_PI);
    [meterL25Top setProgressImage:progressImage];
    [meterL25Top setTrackImage:[UIImage alloc]];
    [meterL25Top setProgress:0.0];
    [viewGeqFrameL addSubview:meterL25Top];
    [viewGeqFrameL sendSubviewToBack:meterL25Top];
    
    meterL25Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(567, 158, 108, 12)];
    meterL25Btm.transform = CGAffineTransformRotate(meterL25Btm.transform, 90.0/180*M_PI);
    [meterL25Btm setProgressImage:progressImage];
    [meterL25Btm setTrackImage:[UIImage alloc]];
    [meterL25Btm setProgress:0.0];
    [viewGeqFrameL addSubview:meterL25Btm];
    [viewGeqFrameL sendSubviewToBack:meterL25Btm];
    
    meterL26Top = [[JEProgressView alloc] initWithFrame:CGRectMake(592, 53, 108, 12)];
    meterL26Top.transform = CGAffineTransformRotate(meterL26Top.transform, 270.0/180*M_PI);
    [meterL26Top setProgressImage:progressImage];
    [meterL26Top setTrackImage:[UIImage alloc]];
    [meterL26Top setProgress:0.0];
    [viewGeqFrameL addSubview:meterL26Top];
    [viewGeqFrameL sendSubviewToBack:meterL26Top];
    
    meterL26Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(592, 158, 108, 12)];
    meterL26Btm.transform = CGAffineTransformRotate(meterL26Btm.transform, 90.0/180*M_PI);
    [meterL26Btm setProgressImage:progressImage];
    [meterL26Btm setTrackImage:[UIImage alloc]];
    [meterL26Btm setProgress:0.0];
    [viewGeqFrameL addSubview:meterL26Btm];
    [viewGeqFrameL sendSubviewToBack:meterL26Btm];
    
    meterL27Top = [[JEProgressView alloc] initWithFrame:CGRectMake(617, 53, 108, 12)];
    meterL27Top.transform = CGAffineTransformRotate(meterL27Top.transform, 270.0/180*M_PI);
    [meterL27Top setProgressImage:progressImage];
    [meterL27Top setTrackImage:[UIImage alloc]];
    [meterL27Top setProgress:0.0];
    [viewGeqFrameL addSubview:meterL27Top];
    [viewGeqFrameL sendSubviewToBack:meterL27Top];
    
    meterL27Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(617, 158, 108, 12)];
    meterL27Btm.transform = CGAffineTransformRotate(meterL27Btm.transform, 90.0/180*M_PI);
    [meterL27Btm setProgressImage:progressImage];
    [meterL27Btm setTrackImage:[UIImage alloc]];
    [meterL27Btm setProgress:0.0];
    [viewGeqFrameL addSubview:meterL27Btm];
    [viewGeqFrameL sendSubviewToBack:meterL27Btm];
    
    meterL28Top = [[JEProgressView alloc] initWithFrame:CGRectMake(642, 53, 108, 12)];
    meterL28Top.transform = CGAffineTransformRotate(meterL28Top.transform, 270.0/180*M_PI);
    [meterL28Top setProgressImage:progressImage];
    [meterL28Top setTrackImage:[UIImage alloc]];
    [meterL28Top setProgress:0.0];
    [viewGeqFrameL addSubview:meterL28Top];
    [viewGeqFrameL sendSubviewToBack:meterL28Top];
    
    meterL28Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(642, 158, 108, 12)];
    meterL28Btm.transform = CGAffineTransformRotate(meterL28Btm.transform, 90.0/180*M_PI);
    [meterL28Btm setProgressImage:progressImage];
    [meterL28Btm setTrackImage:[UIImage alloc]];
    [meterL28Btm setProgress:0.0];
    [viewGeqFrameL addSubview:meterL28Btm];
    [viewGeqFrameL sendSubviewToBack:meterL28Btm];
    
    meterL29Top = [[JEProgressView alloc] initWithFrame:CGRectMake(667, 53, 108, 12)];
    meterL29Top.transform = CGAffineTransformRotate(meterL29Top.transform, 270.0/180*M_PI);
    [meterL29Top setProgressImage:progressImage];
    [meterL29Top setTrackImage:[UIImage alloc]];
    [meterL29Top setProgress:0.0];
    [viewGeqFrameL addSubview:meterL29Top];
    [viewGeqFrameL sendSubviewToBack:meterL29Top];
    
    meterL29Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(667, 158, 108, 12)];
    meterL29Btm.transform = CGAffineTransformRotate(meterL29Btm.transform, 90.0/180*M_PI);
    [meterL29Btm setProgressImage:progressImage];
    [meterL29Btm setTrackImage:[UIImage alloc]];
    [meterL29Btm setProgress:0.0];
    [viewGeqFrameL addSubview:meterL29Btm];
    [viewGeqFrameL sendSubviewToBack:meterL29Btm];
    
    meterL30Top = [[JEProgressView alloc] initWithFrame:CGRectMake(692, 53, 108, 12)];
    meterL30Top.transform = CGAffineTransformRotate(meterL30Top.transform, 270.0/180*M_PI);
    [meterL30Top setProgressImage:progressImage];
    [meterL30Top setTrackImage:[UIImage alloc]];
    [meterL30Top setProgress:0.0];
    [viewGeqFrameL addSubview:meterL30Top];
    [viewGeqFrameL sendSubviewToBack:meterL30Top];
    
    meterL30Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(692, 158, 108, 12)];
    meterL30Btm.transform = CGAffineTransformRotate(meterL30Btm.transform, 90.0/180*M_PI);
    [meterL30Btm setProgressImage:progressImage];
    [meterL30Btm setTrackImage:[UIImage alloc]];
    [meterL30Btm setProgress:0.0];
    [viewGeqFrameL addSubview:meterL30Btm];
    [viewGeqFrameL sendSubviewToBack:meterL30Btm];
    
    meterL31Top = [[JEProgressView alloc] initWithFrame:CGRectMake(717, 53, 108, 12)];
    meterL31Top.transform = CGAffineTransformRotate(meterL31Top.transform, 270.0/180*M_PI);
    [meterL31Top setProgressImage:progressImage];
    [meterL31Top setTrackImage:[UIImage alloc]];
    [meterL31Top setProgress:0.0];
    [viewGeqFrameL addSubview:meterL31Top];
    [viewGeqFrameL sendSubviewToBack:meterL31Top];
    
    meterL31Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(717, 158, 108, 12)];
    meterL31Btm.transform = CGAffineTransformRotate(meterL31Btm.transform, 90.0/180*M_PI);
    [meterL31Btm setProgressImage:progressImage];
    [meterL31Btm setTrackImage:[UIImage alloc]];
    [meterL31Btm setProgress:0.0];
    [viewGeqFrameL addSubview:meterL31Btm];
    [viewGeqFrameL sendSubviewToBack:meterL31Btm];
    
    // Add R slider meters
    meterR1Top = [[JEProgressView alloc] initWithFrame:CGRectMake(-33, 53, 108, 12)];
    meterR1Top.transform = CGAffineTransformRotate(meterR1Top.transform, 270.0/180*M_PI);
    [meterR1Top setProgressImage:progressImage];
    [meterR1Top setTrackImage:[UIImage alloc]];
    [meterR1Top setProgress:0.0];
    [viewGeqFrameR addSubview:meterR1Top];
    [viewGeqFrameR sendSubviewToBack:meterR1Top];
    
    meterR1Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(-33, 158, 108, 12)];
    meterR1Btm.transform = CGAffineTransformRotate(meterR1Btm.transform, 90.0/180*M_PI);
    [meterR1Btm setProgressImage:progressImage];
    [meterR1Btm setTrackImage:[UIImage alloc]];
    [meterR1Btm setProgress:0.0];
    [viewGeqFrameR addSubview:meterR1Btm];
    [viewGeqFrameR sendSubviewToBack:meterR1Btm];
    
    meterR2Top = [[JEProgressView alloc] initWithFrame:CGRectMake(-8, 53, 108, 12)];
    meterR2Top.transform = CGAffineTransformRotate(meterR2Top.transform, 270.0/180*M_PI);
    [meterR2Top setProgressImage:progressImage];
    [meterR2Top setTrackImage:[UIImage alloc]];
    [meterR2Top setProgress:0.0];
    [viewGeqFrameR addSubview:meterR2Top];
    [viewGeqFrameR sendSubviewToBack:meterR2Top];
    
    meterR2Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(-8, 158, 108, 12)];
    meterR2Btm.transform = CGAffineTransformRotate(meterR2Btm.transform, 90.0/180*M_PI);
    [meterR2Btm setProgressImage:progressImage];
    [meterR2Btm setTrackImage:[UIImage alloc]];
    [meterR2Btm setProgress:0.0];
    [viewGeqFrameR addSubview:meterR2Btm];
    [viewGeqFrameR sendSubviewToBack:meterR2Btm];
    
    meterR3Top = [[JEProgressView alloc] initWithFrame:CGRectMake(17, 53, 108, 12)];
    meterR3Top.transform = CGAffineTransformRotate(meterR3Top.transform, 270.0/180*M_PI);
    [meterR3Top setProgressImage:progressImage];
    [meterR3Top setTrackImage:[UIImage alloc]];
    [meterR3Top setProgress:0.0];
    [viewGeqFrameR addSubview:meterR3Top];
    [viewGeqFrameR sendSubviewToBack:meterR3Top];
    
    meterR3Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(17, 158, 108, 12)];
    meterR3Btm.transform = CGAffineTransformRotate(meterR3Btm.transform, 90.0/180*M_PI);
    [meterR3Btm setProgressImage:progressImage];
    [meterR3Btm setTrackImage:[UIImage alloc]];
    [meterR3Btm setProgress:0.0];
    [viewGeqFrameR addSubview:meterR3Btm];
    [viewGeqFrameR sendSubviewToBack:meterR3Btm];
    
    meterR4Top = [[JEProgressView alloc] initWithFrame:CGRectMake(42, 53, 108, 12)];
    meterR4Top.transform = CGAffineTransformRotate(meterR4Top.transform, 270.0/180*M_PI);
    [meterR4Top setProgressImage:progressImage];
    [meterR4Top setTrackImage:[UIImage alloc]];
    [meterR4Top setProgress:0.0];
    [viewGeqFrameR addSubview:meterR4Top];
    [viewGeqFrameR sendSubviewToBack:meterR4Top];
    
    meterR4Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(42, 158, 108, 12)];
    meterR4Btm.transform = CGAffineTransformRotate(meterR4Btm.transform, 90.0/180*M_PI);
    [meterR4Btm setProgressImage:progressImage];
    [meterR4Btm setTrackImage:[UIImage alloc]];
    [meterR4Btm setProgress:0.0];
    [viewGeqFrameR addSubview:meterR4Btm];
    [viewGeqFrameR sendSubviewToBack:meterR4Btm];
    
    meterR5Top = [[JEProgressView alloc] initWithFrame:CGRectMake(67, 53, 108, 12)];
    meterR5Top.transform = CGAffineTransformRotate(meterR5Top.transform, 270.0/180*M_PI);
    [meterR5Top setProgressImage:progressImage];
    [meterR5Top setTrackImage:[UIImage alloc]];
    [meterR5Top setProgress:0.0];
    [viewGeqFrameR addSubview:meterR5Top];
    [viewGeqFrameR sendSubviewToBack:meterR5Top];
    
    meterR5Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(67, 158, 108, 12)];
    meterR5Btm.transform = CGAffineTransformRotate(meterR5Btm.transform, 90.0/180*M_PI);
    [meterR5Btm setProgressImage:progressImage];
    [meterR5Btm setTrackImage:[UIImage alloc]];
    [meterR5Btm setProgress:0.0];
    [viewGeqFrameR addSubview:meterR5Btm];
    [viewGeqFrameR sendSubviewToBack:meterR5Btm];
    
    meterR6Top = [[JEProgressView alloc] initWithFrame:CGRectMake(92, 53, 108, 12)];
    meterR6Top.transform = CGAffineTransformRotate(meterR6Top.transform, 270.0/180*M_PI);
    [meterR6Top setProgressImage:progressImage];
    [meterR6Top setTrackImage:[UIImage alloc]];
    [meterR6Top setProgress:0.0];
    [viewGeqFrameR addSubview:meterR6Top];
    [viewGeqFrameR sendSubviewToBack:meterR6Top];
    
    meterR6Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(92, 158, 108, 12)];
    meterR6Btm.transform = CGAffineTransformRotate(meterR6Btm.transform, 90.0/180*M_PI);
    [meterR6Btm setProgressImage:progressImage];
    [meterR6Btm setTrackImage:[UIImage alloc]];
    [meterR6Btm setProgress:0.0];
    [viewGeqFrameR addSubview:meterR6Btm];
    [viewGeqFrameR sendSubviewToBack:meterR6Btm];
    
    meterR7Top = [[JEProgressView alloc] initWithFrame:CGRectMake(117, 53, 108, 12)];
    meterR7Top.transform = CGAffineTransformRotate(meterR7Top.transform, 270.0/180*M_PI);
    [meterR7Top setProgressImage:progressImage];
    [meterR7Top setTrackImage:[UIImage alloc]];
    [meterR7Top setProgress:0.0];
    [viewGeqFrameR addSubview:meterR7Top];
    [viewGeqFrameR sendSubviewToBack:meterR7Top];
    
    meterR7Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(117, 158, 108, 12)];
    meterR7Btm.transform = CGAffineTransformRotate(meterR7Btm.transform, 90.0/180*M_PI);
    [meterR7Btm setProgressImage:progressImage];
    [meterR7Btm setTrackImage:[UIImage alloc]];
    [meterR7Btm setProgress:0.0];
    [viewGeqFrameR addSubview:meterR7Btm];
    [viewGeqFrameR sendSubviewToBack:meterR7Btm];
    
    meterR8Top = [[JEProgressView alloc] initWithFrame:CGRectMake(142, 53, 108, 12)];
    meterR8Top.transform = CGAffineTransformRotate(meterR8Top.transform, 270.0/180*M_PI);
    [meterR8Top setProgressImage:progressImage];
    [meterR8Top setTrackImage:[UIImage alloc]];
    [meterR8Top setProgress:0.0];
    [viewGeqFrameR addSubview:meterR8Top];
    [viewGeqFrameR sendSubviewToBack:meterR8Top];
    
    meterR8Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(142, 158, 108, 12)];
    meterR8Btm.transform = CGAffineTransformRotate(meterR8Btm.transform, 90.0/180*M_PI);
    [meterR8Btm setProgressImage:progressImage];
    [meterR8Btm setTrackImage:[UIImage alloc]];
    [meterR8Btm setProgress:0.0];
    [viewGeqFrameR addSubview:meterR8Btm];
    [viewGeqFrameR sendSubviewToBack:meterR8Btm];
    
    meterR9Top = [[JEProgressView alloc] initWithFrame:CGRectMake(167, 53, 108, 12)];
    meterR9Top.transform = CGAffineTransformRotate(meterR9Top.transform, 270.0/180*M_PI);
    [meterR9Top setProgressImage:progressImage];
    [meterR9Top setTrackImage:[UIImage alloc]];
    [meterR9Top setProgress:0.0];
    [viewGeqFrameR addSubview:meterR9Top];
    [viewGeqFrameR sendSubviewToBack:meterR9Top];
    
    meterR9Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(167, 158, 108, 12)];
    meterR9Btm.transform = CGAffineTransformRotate(meterR9Btm.transform, 90.0/180*M_PI);
    [meterR9Btm setProgressImage:progressImage];
    [meterR9Btm setTrackImage:[UIImage alloc]];
    [meterR9Btm setProgress:0.0];
    [viewGeqFrameR addSubview:meterR9Btm];
    [viewGeqFrameR sendSubviewToBack:meterR9Btm];
    
    meterR10Top = [[JEProgressView alloc] initWithFrame:CGRectMake(192, 53, 108, 12)];
    meterR10Top.transform = CGAffineTransformRotate(meterR10Top.transform, 270.0/180*M_PI);
    [meterR10Top setProgressImage:progressImage];
    [meterR10Top setTrackImage:[UIImage alloc]];
    [meterR10Top setProgress:0.0];
    [viewGeqFrameR addSubview:meterR10Top];
    [viewGeqFrameR sendSubviewToBack:meterR10Top];
    
    meterR10Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(192, 158, 108, 12)];
    meterR10Btm.transform = CGAffineTransformRotate(meterR10Btm.transform, 90.0/180*M_PI);
    [meterR10Btm setProgressImage:progressImage];
    [meterR10Btm setTrackImage:[UIImage alloc]];
    [meterR10Btm setProgress:0.0];
    [viewGeqFrameR addSubview:meterR10Btm];
    [viewGeqFrameR sendSubviewToBack:meterR10Btm];
    
    meterR11Top = [[JEProgressView alloc] initWithFrame:CGRectMake(217, 53, 108, 12)];
    meterR11Top.transform = CGAffineTransformRotate(meterR11Top.transform, 270.0/180*M_PI);
    [meterR11Top setProgressImage:progressImage];
    [meterR11Top setTrackImage:[UIImage alloc]];
    [meterR11Top setProgress:0.0];
    [viewGeqFrameR addSubview:meterR11Top];
    [viewGeqFrameR sendSubviewToBack:meterR11Top];
    
    meterR11Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(217, 158, 108, 12)];
    meterR11Btm.transform = CGAffineTransformRotate(meterR11Btm.transform, 90.0/180*M_PI);
    [meterR11Btm setProgressImage:progressImage];
    [meterR11Btm setTrackImage:[UIImage alloc]];
    [meterR11Btm setProgress:0.0];
    [viewGeqFrameR addSubview:meterR11Btm];
    [viewGeqFrameR sendSubviewToBack:meterR11Btm];
    
    meterR12Top = [[JEProgressView alloc] initWithFrame:CGRectMake(242, 53, 108, 12)];
    meterR12Top.transform = CGAffineTransformRotate(meterR12Top.transform, 270.0/180*M_PI);
    [meterR12Top setProgressImage:progressImage];
    [meterR12Top setTrackImage:[UIImage alloc]];
    [meterR12Top setProgress:0.0];
    [viewGeqFrameR addSubview:meterR12Top];
    [viewGeqFrameR sendSubviewToBack:meterR12Top];
    
    meterR12Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(242, 158, 108, 12)];
    meterR12Btm.transform = CGAffineTransformRotate(meterR12Btm.transform, 90.0/180*M_PI);
    [meterR12Btm setProgressImage:progressImage];
    [meterR12Btm setTrackImage:[UIImage alloc]];
    [meterR12Btm setProgress:0.0];
    [viewGeqFrameR addSubview:meterR12Btm];
    [viewGeqFrameR sendSubviewToBack:meterR12Btm];
    
    meterR13Top = [[JEProgressView alloc] initWithFrame:CGRectMake(267, 53, 108, 12)];
    meterR13Top.transform = CGAffineTransformRotate(meterR13Top.transform, 270.0/180*M_PI);
    [meterR13Top setProgressImage:progressImage];
    [meterR13Top setTrackImage:[UIImage alloc]];
    [meterR13Top setProgress:0.0];
    [viewGeqFrameR addSubview:meterR13Top];
    [viewGeqFrameR sendSubviewToBack:meterR13Top];
    
    meterR13Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(267, 158, 108, 12)];
    meterR13Btm.transform = CGAffineTransformRotate(meterR13Btm.transform, 90.0/180*M_PI);
    [meterR13Btm setProgressImage:progressImage];
    [meterR13Btm setTrackImage:[UIImage alloc]];
    [meterR13Btm setProgress:0.0];
    [viewGeqFrameR addSubview:meterR13Btm];
    [viewGeqFrameR sendSubviewToBack:meterR13Btm];
    
    meterR14Top = [[JEProgressView alloc] initWithFrame:CGRectMake(292, 53, 108, 12)];
    meterR14Top.transform = CGAffineTransformRotate(meterR14Top.transform, 270.0/180*M_PI);
    [meterR14Top setProgressImage:progressImage];
    [meterR14Top setTrackImage:[UIImage alloc]];
    [meterR14Top setProgress:0.0];
    [viewGeqFrameR addSubview:meterR14Top];
    [viewGeqFrameR sendSubviewToBack:meterR14Top];
    
    meterR14Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(292, 158, 108, 12)];
    meterR14Btm.transform = CGAffineTransformRotate(meterR14Btm.transform, 90.0/180*M_PI);
    [meterR14Btm setProgressImage:progressImage];
    [meterR14Btm setTrackImage:[UIImage alloc]];
    [meterR14Btm setProgress:0.0];
    [viewGeqFrameR addSubview:meterR14Btm];
    [viewGeqFrameR sendSubviewToBack:meterR14Btm];
    
    meterR15Top = [[JEProgressView alloc] initWithFrame:CGRectMake(317, 53, 108, 12)];
    meterR15Top.transform = CGAffineTransformRotate(meterR15Top.transform, 270.0/180*M_PI);
    [meterR15Top setProgressImage:progressImage];
    [meterR15Top setTrackImage:[UIImage alloc]];
    [meterR15Top setProgress:0.0];
    [viewGeqFrameR addSubview:meterR15Top];
    [viewGeqFrameR sendSubviewToBack:meterR15Top];
    
    meterR15Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(317, 158, 108, 12)];
    meterR15Btm.transform = CGAffineTransformRotate(meterR15Btm.transform, 90.0/180*M_PI);
    [meterR15Btm setProgressImage:progressImage];
    [meterR15Btm setTrackImage:[UIImage alloc]];
    [meterR15Btm setProgress:0.0];
    [viewGeqFrameR addSubview:meterR15Btm];
    [viewGeqFrameR sendSubviewToBack:meterR15Btm];
    
    meterR16Top = [[JEProgressView alloc] initWithFrame:CGRectMake(342, 53, 108, 12)];
    meterR16Top.transform = CGAffineTransformRotate(meterR16Top.transform, 270.0/180*M_PI);
    [meterR16Top setProgressImage:progressImage];
    [meterR16Top setTrackImage:[UIImage alloc]];
    [meterR16Top setProgress:0.0];
    [viewGeqFrameR addSubview:meterR16Top];
    [viewGeqFrameR sendSubviewToBack:meterR16Top];
    
    meterR16Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(342, 158, 108, 12)];
    meterR16Btm.transform = CGAffineTransformRotate(meterR16Btm.transform, 90.0/180*M_PI);
    [meterR16Btm setProgressImage:progressImage];
    [meterR16Btm setTrackImage:[UIImage alloc]];
    [meterR16Btm setProgress:0.0];
    [viewGeqFrameR addSubview:meterR16Btm];
    [viewGeqFrameR sendSubviewToBack:meterR16Btm];
    
    meterR17Top = [[JEProgressView alloc] initWithFrame:CGRectMake(367, 53, 108, 12)];
    meterR17Top.transform = CGAffineTransformRotate(meterR17Top.transform, 270.0/180*M_PI);
    [meterR17Top setProgressImage:progressImage];
    [meterR17Top setTrackImage:[UIImage alloc]];
    [meterR17Top setProgress:0.0];
    [viewGeqFrameR addSubview:meterR17Top];
    [viewGeqFrameR sendSubviewToBack:meterR17Top];
    
    meterR17Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(367, 158, 108, 12)];
    meterR17Btm.transform = CGAffineTransformRotate(meterR17Btm.transform, 90.0/180*M_PI);
    [meterR17Btm setProgressImage:progressImage];
    [meterR17Btm setTrackImage:[UIImage alloc]];
    [meterR17Btm setProgress:0.0];
    [viewGeqFrameR addSubview:meterR17Btm];
    [viewGeqFrameR sendSubviewToBack:meterR17Btm];
    
    meterR18Top = [[JEProgressView alloc] initWithFrame:CGRectMake(392, 53, 108, 12)];
    meterR18Top.transform = CGAffineTransformRotate(meterR18Top.transform, 270.0/180*M_PI);
    [meterR18Top setProgressImage:progressImage];
    [meterR18Top setTrackImage:[UIImage alloc]];
    [meterR18Top setProgress:0.0];
    [viewGeqFrameR addSubview:meterR18Top];
    [viewGeqFrameR sendSubviewToBack:meterR18Top];
    
    meterR18Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(392, 158, 108, 12)];
    meterR18Btm.transform = CGAffineTransformRotate(meterR18Btm.transform, 90.0/180*M_PI);
    [meterR18Btm setProgressImage:progressImage];
    [meterR18Btm setTrackImage:[UIImage alloc]];
    [meterR18Btm setProgress:0.0];
    [viewGeqFrameR addSubview:meterR18Btm];
    [viewGeqFrameR sendSubviewToBack:meterR18Btm];
    
    meterR19Top = [[JEProgressView alloc] initWithFrame:CGRectMake(417, 53, 108, 12)];
    meterR19Top.transform = CGAffineTransformRotate(meterR19Top.transform, 270.0/180*M_PI);
    [meterR19Top setProgressImage:progressImage];
    [meterR19Top setTrackImage:[UIImage alloc]];
    [meterR19Top setProgress:0.0];
    [viewGeqFrameR addSubview:meterR19Top];
    [viewGeqFrameR sendSubviewToBack:meterR19Top];
    
    meterR19Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(417, 158, 108, 12)];
    meterR19Btm.transform = CGAffineTransformRotate(meterR19Btm.transform, 90.0/180*M_PI);
    [meterR19Btm setProgressImage:progressImage];
    [meterR19Btm setTrackImage:[UIImage alloc]];
    [meterR19Btm setProgress:0.0];
    [viewGeqFrameR addSubview:meterR19Btm];
    [viewGeqFrameR sendSubviewToBack:meterR19Btm];
    
    meterR20Top = [[JEProgressView alloc] initWithFrame:CGRectMake(442, 53, 108, 12)];
    meterR20Top.transform = CGAffineTransformRotate(meterR20Top.transform, 270.0/180*M_PI);
    [meterR20Top setProgressImage:progressImage];
    [meterR20Top setTrackImage:[UIImage alloc]];
    [meterR20Top setProgress:0.0];
    [viewGeqFrameR addSubview:meterR20Top];
    [viewGeqFrameR sendSubviewToBack:meterR20Top];
    
    meterR20Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(442, 158, 108, 12)];
    meterR20Btm.transform = CGAffineTransformRotate(meterR20Btm.transform, 90.0/180*M_PI);
    [meterR20Btm setProgressImage:progressImage];
    [meterR20Btm setTrackImage:[UIImage alloc]];
    [meterR20Btm setProgress:0.0];
    [viewGeqFrameR addSubview:meterR20Btm];
    [viewGeqFrameR sendSubviewToBack:meterR20Btm];
    
    meterR21Top = [[JEProgressView alloc] initWithFrame:CGRectMake(467, 53, 108, 12)];
    meterR21Top.transform = CGAffineTransformRotate(meterR21Top.transform, 270.0/180*M_PI);
    [meterR21Top setProgressImage:progressImage];
    [meterR21Top setTrackImage:[UIImage alloc]];
    [meterR21Top setProgress:0.0];
    [viewGeqFrameR addSubview:meterR21Top];
    [viewGeqFrameR sendSubviewToBack:meterR21Top];
    
    meterR21Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(467, 158, 108, 12)];
    meterR21Btm.transform = CGAffineTransformRotate(meterR21Btm.transform, 90.0/180*M_PI);
    [meterR21Btm setProgressImage:progressImage];
    [meterR21Btm setTrackImage:[UIImage alloc]];
    [meterR21Btm setProgress:0.0];
    [viewGeqFrameR addSubview:meterR21Btm];
    [viewGeqFrameR sendSubviewToBack:meterR21Btm];
    
    meterR22Top = [[JEProgressView alloc] initWithFrame:CGRectMake(492, 53, 108, 12)];
    meterR22Top.transform = CGAffineTransformRotate(meterR22Top.transform, 270.0/180*M_PI);
    [meterR22Top setProgressImage:progressImage];
    [meterR22Top setTrackImage:[UIImage alloc]];
    [meterR22Top setProgress:0.0];
    [viewGeqFrameR addSubview:meterR22Top];
    [viewGeqFrameR sendSubviewToBack:meterR22Top];
    
    meterR22Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(492, 158, 108, 12)];
    meterR22Btm.transform = CGAffineTransformRotate(meterR22Btm.transform, 90.0/180*M_PI);
    [meterR22Btm setProgressImage:progressImage];
    [meterR22Btm setTrackImage:[UIImage alloc]];
    [meterR22Btm setProgress:0.0];
    [viewGeqFrameR addSubview:meterR22Btm];
    [viewGeqFrameR sendSubviewToBack:meterR22Btm];
    
    meterR23Top = [[JEProgressView alloc] initWithFrame:CGRectMake(517, 53, 108, 12)];
    meterR23Top.transform = CGAffineTransformRotate(meterR23Top.transform, 270.0/180*M_PI);
    [meterR23Top setProgressImage:progressImage];
    [meterR23Top setTrackImage:[UIImage alloc]];
    [meterR23Top setProgress:0.0];
    [viewGeqFrameR addSubview:meterR23Top];
    [viewGeqFrameR sendSubviewToBack:meterR23Top];
    
    meterR23Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(517, 158, 108, 12)];
    meterR23Btm.transform = CGAffineTransformRotate(meterR23Btm.transform, 90.0/180*M_PI);
    [meterR23Btm setProgressImage:progressImage];
    [meterR23Btm setTrackImage:[UIImage alloc]];
    [meterR23Btm setProgress:0.0];
    [viewGeqFrameR addSubview:meterR23Btm];
    [viewGeqFrameR sendSubviewToBack:meterR23Btm];
    
    meterR24Top = [[JEProgressView alloc] initWithFrame:CGRectMake(542, 53, 108, 12)];
    meterR24Top.transform = CGAffineTransformRotate(meterR24Top.transform, 270.0/180*M_PI);
    [meterR24Top setProgressImage:progressImage];
    [meterR24Top setTrackImage:[UIImage alloc]];
    [meterR24Top setProgress:0.0];
    [viewGeqFrameR addSubview:meterR24Top];
    [viewGeqFrameR sendSubviewToBack:meterR24Top];
    
    meterR24Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(542, 158, 108, 12)];
    meterR24Btm.transform = CGAffineTransformRotate(meterR24Btm.transform, 90.0/180*M_PI);
    [meterR24Btm setProgressImage:progressImage];
    [meterR24Btm setTrackImage:[UIImage alloc]];
    [meterR24Btm setProgress:0.0];
    [viewGeqFrameR addSubview:meterR24Btm];
    [viewGeqFrameR sendSubviewToBack:meterR24Btm];
    
    meterR25Top = [[JEProgressView alloc] initWithFrame:CGRectMake(567, 53, 108, 12)];
    meterR25Top.transform = CGAffineTransformRotate(meterR25Top.transform, 270.0/180*M_PI);
    [meterR25Top setProgressImage:progressImage];
    [meterR25Top setTrackImage:[UIImage alloc]];
    [meterR25Top setProgress:0.0];
    [viewGeqFrameR addSubview:meterR25Top];
    [viewGeqFrameR sendSubviewToBack:meterR25Top];
    
    meterR25Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(567, 158, 108, 12)];
    meterR25Btm.transform = CGAffineTransformRotate(meterR25Btm.transform, 90.0/180*M_PI);
    [meterR25Btm setProgressImage:progressImage];
    [meterR25Btm setTrackImage:[UIImage alloc]];
    [meterR25Btm setProgress:0.0];
    [viewGeqFrameR addSubview:meterR25Btm];
    [viewGeqFrameR sendSubviewToBack:meterR25Btm];
    
    meterR26Top = [[JEProgressView alloc] initWithFrame:CGRectMake(592, 53, 108, 12)];
    meterR26Top.transform = CGAffineTransformRotate(meterR26Top.transform, 270.0/180*M_PI);
    [meterR26Top setProgressImage:progressImage];
    [meterR26Top setTrackImage:[UIImage alloc]];
    [meterR26Top setProgress:0.0];
    [viewGeqFrameR addSubview:meterR26Top];
    [viewGeqFrameR sendSubviewToBack:meterR26Top];
    
    meterR26Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(592, 158, 108, 12)];
    meterR26Btm.transform = CGAffineTransformRotate(meterR26Btm.transform, 90.0/180*M_PI);
    [meterR26Btm setProgressImage:progressImage];
    [meterR26Btm setTrackImage:[UIImage alloc]];
    [meterR26Btm setProgress:0.0];
    [viewGeqFrameR addSubview:meterR26Btm];
    [viewGeqFrameR sendSubviewToBack:meterR26Btm];
    
    meterR27Top = [[JEProgressView alloc] initWithFrame:CGRectMake(617, 53, 108, 12)];
    meterR27Top.transform = CGAffineTransformRotate(meterR27Top.transform, 270.0/180*M_PI);
    [meterR27Top setProgressImage:progressImage];
    [meterR27Top setTrackImage:[UIImage alloc]];
    [meterR27Top setProgress:0.0];
    [viewGeqFrameR addSubview:meterR27Top];
    [viewGeqFrameR sendSubviewToBack:meterR27Top];
    
    meterR27Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(617, 158, 108, 12)];
    meterR27Btm.transform = CGAffineTransformRotate(meterR27Btm.transform, 90.0/180*M_PI);
    [meterR27Btm setProgressImage:progressImage];
    [meterR27Btm setTrackImage:[UIImage alloc]];
    [meterR27Btm setProgress:0.0];
    [viewGeqFrameR addSubview:meterR27Btm];
    [viewGeqFrameR sendSubviewToBack:meterR27Btm];
    
    meterR28Top = [[JEProgressView alloc] initWithFrame:CGRectMake(642, 53, 108, 12)];
    meterR28Top.transform = CGAffineTransformRotate(meterR28Top.transform, 270.0/180*M_PI);
    [meterR28Top setProgressImage:progressImage];
    [meterR28Top setTrackImage:[UIImage alloc]];
    [meterR28Top setProgress:0.0];
    [viewGeqFrameR addSubview:meterR28Top];
    [viewGeqFrameR sendSubviewToBack:meterR28Top];
    
    meterR28Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(642, 158, 108, 12)];
    meterR28Btm.transform = CGAffineTransformRotate(meterR28Btm.transform, 90.0/180*M_PI);
    [meterR28Btm setProgressImage:progressImage];
    [meterR28Btm setTrackImage:[UIImage alloc]];
    [meterR28Btm setProgress:0.0];
    [viewGeqFrameR addSubview:meterR28Btm];
    [viewGeqFrameR sendSubviewToBack:meterR28Btm];
    
    meterR29Top = [[JEProgressView alloc] initWithFrame:CGRectMake(667, 53, 108, 12)];
    meterR29Top.transform = CGAffineTransformRotate(meterR29Top.transform, 270.0/180*M_PI);
    [meterR29Top setProgressImage:progressImage];
    [meterR29Top setTrackImage:[UIImage alloc]];
    [meterR29Top setProgress:0.0];
    [viewGeqFrameR addSubview:meterR29Top];
    [viewGeqFrameR sendSubviewToBack:meterR29Top];
    
    meterR29Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(667, 158, 108, 12)];
    meterR29Btm.transform = CGAffineTransformRotate(meterR29Btm.transform, 90.0/180*M_PI);
    [meterR29Btm setProgressImage:progressImage];
    [meterR29Btm setTrackImage:[UIImage alloc]];
    [meterR29Btm setProgress:0.0];
    [viewGeqFrameR addSubview:meterR29Btm];
    [viewGeqFrameR sendSubviewToBack:meterR29Btm];
    
    meterR30Top = [[JEProgressView alloc] initWithFrame:CGRectMake(692, 53, 108, 12)];
    meterR30Top.transform = CGAffineTransformRotate(meterR30Top.transform, 270.0/180*M_PI);
    [meterR30Top setProgressImage:progressImage];
    [meterR30Top setTrackImage:[UIImage alloc]];
    [meterR30Top setProgress:0.0];
    [viewGeqFrameR addSubview:meterR30Top];
    [viewGeqFrameR sendSubviewToBack:meterR30Top];
    
    meterR30Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(692, 158, 108, 12)];
    meterR30Btm.transform = CGAffineTransformRotate(meterR30Btm.transform, 90.0/180*M_PI);
    [meterR30Btm setProgressImage:progressImage];
    [meterR30Btm setTrackImage:[UIImage alloc]];
    [meterR30Btm setProgress:0.0];
    [viewGeqFrameR addSubview:meterR30Btm];
    [viewGeqFrameR sendSubviewToBack:meterR30Btm];
    
    meterR31Top = [[JEProgressView alloc] initWithFrame:CGRectMake(717, 53, 108, 12)];
    meterR31Top.transform = CGAffineTransformRotate(meterR31Top.transform, 270.0/180*M_PI);
    [meterR31Top setProgressImage:progressImage];
    [meterR31Top setTrackImage:[UIImage alloc]];
    [meterR31Top setProgress:0.0];
    [viewGeqFrameR addSubview:meterR31Top];
    [viewGeqFrameR sendSubviewToBack:meterR31Top];
    
    meterR31Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(717, 158, 108, 12)];
    meterR31Btm.transform = CGAffineTransformRotate(meterR31Btm.transform, 90.0/180*M_PI);
    [meterR31Btm setProgressImage:progressImage];
    [meterR31Btm setTrackImage:[UIImage alloc]];
    [meterR31Btm setProgress:0.0];
    [viewGeqFrameR addSubview:meterR31Btm];
    [viewGeqFrameR sendSubviewToBack:meterR31Btm];
    
    [self.view sendSubviewToBack:self.viewGeqDrawL];
    [self.view sendSubviewToBack:self.viewGeqDrawR];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
    NSLog(@"GeqPeqViewController didReceiveMemoryWarning");
}

- (void)processReply:(EffectPagePacket *)pkt
{
    BOOL needsUpdate = NO;
     
    // Don't update if user is moving sliders
    if (geqLock) {
        return;
    }
     
    // Update GEQ sliders
    int geqL1Value = pkt->l_geq31_20_hz_db;
    int geqL2Value = pkt->l_geq31_25_hz_db;
    int geqL3Value = pkt->l_geq31_31_5_hz_db;
    int geqL4Value = pkt->l_geq31_40_hz_db;
    int geqL5Value = pkt->l_geq31_50_hz_db;
    int geqL6Value = pkt->l_geq31_63_hz_db;
    int geqL7Value = pkt->l_geq31_80_hz_db;
    int geqL8Value = pkt->l_geq31_100_hz_db;
    int geqL9Value = pkt->l_geq31_125_hz_db;
    int geqL10Value = pkt->l_geq31_160_hz_db;
    int geqL11Value = pkt->l_geq31_200_hz_db;
    int geqL12Value = pkt->l_geq31_250_hz_db;
    int geqL13Value = pkt->l_geq31_315_hz_db;
    int geqL14Value = pkt->l_geq31_400_hz_db;
    int geqL15Value = pkt->l_geq31_500_hz_db;
    int geqL16Value = pkt->l_geq31_630_hz_db;
    int geqL17Value = pkt->l_geq31_800_hz_db;
    int geqL18Value = pkt->l_geq31_1_khz_db;
    int geqL19Value = pkt->l_geq31_1_25_khz_db;
    int geqL20Value = pkt->l_geq31_1_6_khz_db;
    int geqL21Value = pkt->l_geq31_2_khz_db;
    int geqL22Value = pkt->l_geq31_2_5_khz_db;
    int geqL23Value = pkt->l_geq31_3_15_khz_db;
    int geqL24Value = pkt->l_geq31_4_khz_db;
    int geqL25Value = pkt->l_geq31_5_khz_db;
    int geqL26Value = pkt->l_geq31_6_3_khz_db;
    int geqL27Value = pkt->l_geq31_8_khz_db;
    int geqL28Value = pkt->l_geq31_10_khz_db;
    int geqL29Value = pkt->l_geq31_12_5_khz_db;
    int geqL30Value = pkt->l_geq31_16_khz_db;
    int geqL31Value = pkt->l_geq31_20_khz_db;
    
    if (sliderLGeq1.value != geqL1Value && ![viewController sendCommandExists:CMD_L_GEQ_20HZ_DB dspId:DSP_8]) {
        sliderLGeq1.value = geqL1Value;
        if (sliderLGeq1.value > 0.0) {
            meterL1Top.progress = sliderLGeq1.value/GEQ_SLIDER_MAX_VALUE;
            meterL1Btm.progress = 0.0;
        } else if (sliderLGeq1.value < 0.0) {
            meterL1Btm.progress = sliderLGeq1.value/GEQ_SLIDER_MIN_VALUE;
            meterL1Top.progress = 0.0;
        } else {
            meterL1Top.progress = 0.0;
            meterL1Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderLGeq2.value != geqL2Value && ![viewController sendCommandExists:CMD_L_GEQ_25HZ_DB dspId:DSP_8]) {
        sliderLGeq2.value = geqL2Value;
        if (sliderLGeq2.value > 0.0) {
            meterL2Top.progress = sliderLGeq2.value/GEQ_SLIDER_MAX_VALUE;
            meterL2Btm.progress = 0.0;
        } else if (sliderLGeq2.value < 0.0) {
            meterL2Btm.progress = sliderLGeq2.value/GEQ_SLIDER_MIN_VALUE;
            meterL2Top.progress = 0.0;
        } else {
            meterL2Top.progress = 0.0;
            meterL2Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderLGeq3.value != geqL3Value && ![viewController sendCommandExists:CMD_L_GEQ_31_5HZ_DB dspId:DSP_8]) {
        sliderLGeq3.value = geqL3Value;
        if (sliderLGeq3.value > 0.0) {
            meterL3Top.progress = sliderLGeq3.value/GEQ_SLIDER_MAX_VALUE;
            meterL3Btm.progress = 0.0;
        } else if (sliderLGeq3.value < 0.0) {
            meterL3Btm.progress = sliderLGeq3.value/GEQ_SLIDER_MIN_VALUE;
            meterL3Top.progress = 0.0;
        } else {
            meterL3Top.progress = 0.0;
            meterL3Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderLGeq4.value != geqL4Value && ![viewController sendCommandExists:CMD_L_GEQ_40HZ_DB dspId:DSP_8]) {
        sliderLGeq4.value = geqL4Value;
        if (sliderLGeq4.value > 0.0) {
            meterL4Top.progress = sliderLGeq4.value/GEQ_SLIDER_MAX_VALUE;
            meterL4Btm.progress = 0.0;
        } else if (sliderLGeq4.value < 0.0) {
            meterL4Btm.progress = sliderLGeq4.value/GEQ_SLIDER_MIN_VALUE;
            meterL4Top.progress = 0.0;
        } else {
            meterL4Top.progress = 0.0;
            meterL4Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderLGeq5.value != geqL5Value && ![viewController sendCommandExists:CMD_L_GEQ_50HZ_DB dspId:DSP_8]) {
        sliderLGeq5.value = geqL5Value;
        if (sliderLGeq5.value > 0.0) {
            meterL5Top.progress = sliderLGeq5.value/GEQ_SLIDER_MAX_VALUE;
            meterL5Btm.progress = 0.0;
        } else if (sliderLGeq5.value < 0.0) {
            meterL5Btm.progress = sliderLGeq5.value/GEQ_SLIDER_MIN_VALUE;
            meterL5Top.progress = 0.0;
        } else {
            meterL5Top.progress = 0.0;
            meterL5Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderLGeq6.value != geqL6Value && ![viewController sendCommandExists:CMD_L_GEQ_63HZ_DB dspId:DSP_8]) {
        sliderLGeq6.value = geqL6Value;
        if (sliderLGeq6.value > 0.0) {
            meterL6Top.progress = sliderLGeq6.value/GEQ_SLIDER_MAX_VALUE;
            meterL6Btm.progress = 0.0;
        } else if (sliderLGeq6.value < 0.0) {
            meterL6Btm.progress = sliderLGeq6.value/GEQ_SLIDER_MIN_VALUE;
            meterL6Top.progress = 0.0;
        } else {
            meterL6Top.progress = 0.0;
            meterL6Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderLGeq7.value != geqL7Value && ![viewController sendCommandExists:CMD_L_GEQ_80HZ_DB dspId:DSP_8]) {
        sliderLGeq7.value = geqL7Value;
        if (sliderLGeq7.value > 0.0) {
            meterL7Top.progress = sliderLGeq7.value/GEQ_SLIDER_MAX_VALUE;
            meterL7Btm.progress = 0.0;
        } else if (sliderLGeq7.value < 0.0) {
            meterL7Btm.progress = sliderLGeq7.value/GEQ_SLIDER_MIN_VALUE;
            meterL7Top.progress = 0.0;
        } else {
            meterL7Top.progress = 0.0;
            meterL7Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderLGeq8.value != geqL8Value && ![viewController sendCommandExists:CMD_L_GEQ_100HZ_DB dspId:DSP_8]) {
        sliderLGeq8.value = geqL8Value;
        if (sliderLGeq8.value > 0.0) {
            meterL8Top.progress = sliderLGeq8.value/GEQ_SLIDER_MAX_VALUE;
            meterL8Btm.progress = 0.0;
        } else if (sliderLGeq8.value < 0.0) {
            meterL8Btm.progress = sliderLGeq8.value/GEQ_SLIDER_MIN_VALUE;
            meterL8Top.progress = 0.0;
        } else {
            meterL8Top.progress = 0.0;
            meterL8Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderLGeq9.value != geqL9Value && ![viewController sendCommandExists:CMD_L_GEQ_125HZ_DB dspId:DSP_8]) {
        sliderLGeq9.value = geqL9Value;
        if (sliderLGeq9.value > 0.0) {
            meterL9Top.progress = sliderLGeq9.value/GEQ_SLIDER_MAX_VALUE;
            meterL9Btm.progress = 0.0;
        } else if (sliderLGeq9.value < 0.0) {
            meterL9Btm.progress = sliderLGeq9.value/GEQ_SLIDER_MIN_VALUE;
            meterL9Top.progress = 0.0;
        } else {
            meterL9Top.progress = 0.0;
            meterL9Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderLGeq10.value != geqL10Value && ![viewController sendCommandExists:CMD_L_GEQ_160HZ_DB dspId:DSP_8]) {
        sliderLGeq10.value = geqL10Value;
        if (sliderLGeq10.value > 0.0) {
            meterL10Top.progress = sliderLGeq10.value/GEQ_SLIDER_MAX_VALUE;
            meterL10Btm.progress = 0.0;
        } else if (sliderLGeq10.value < 0.0) {
            meterL10Btm.progress = sliderLGeq10.value/GEQ_SLIDER_MIN_VALUE;
            meterL10Top.progress = 0.0;
        } else {
            meterL10Top.progress = 0.0;
            meterL10Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderLGeq11.value != geqL11Value && ![viewController sendCommandExists:CMD_L_GEQ_200HZ_DB dspId:DSP_8]) {
        sliderLGeq11.value = geqL11Value;
        if (sliderLGeq11.value > 0.0) {
            meterL11Top.progress = sliderLGeq11.value/GEQ_SLIDER_MAX_VALUE;
            meterL11Btm.progress = 0.0;
        } else if (sliderLGeq11.value < 0.0) {
            meterL11Btm.progress = sliderLGeq11.value/GEQ_SLIDER_MIN_VALUE;
            meterL1Top.progress = 0.0;
        } else {
            meterL11Top.progress = 0.0;
            meterL11Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderLGeq12.value != geqL12Value && ![viewController sendCommandExists:CMD_L_GEQ_250HZ_DB dspId:DSP_8]) {
        sliderLGeq12.value = geqL12Value;
        if (sliderLGeq12.value > 0.0) {
            meterL12Top.progress = sliderLGeq12.value/GEQ_SLIDER_MAX_VALUE;
            meterL12Btm.progress = 0.0;
        } else if (sliderLGeq12.value < 0.0) {
            meterL12Btm.progress = sliderLGeq12.value/GEQ_SLIDER_MIN_VALUE;
            meterL12Top.progress = 0.0;
        } else {
            meterL12Top.progress = 0.0;
            meterL12Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderLGeq13.value != geqL13Value && ![viewController sendCommandExists:CMD_L_GEQ_315HZ_DB dspId:DSP_8]) {
        sliderLGeq13.value = geqL13Value;
        if (sliderLGeq13.value > 0.0) {
            meterL13Top.progress = sliderLGeq13.value/GEQ_SLIDER_MAX_VALUE;
            meterL13Btm.progress = 0.0;
        } else if (sliderLGeq13.value < 0.0) {
            meterL13Btm.progress = sliderLGeq13.value/GEQ_SLIDER_MIN_VALUE;
            meterL13Top.progress = 0.0;
        } else {
            meterL13Top.progress = 0.0;
            meterL13Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderLGeq14.value != geqL14Value && ![viewController sendCommandExists:CMD_L_GEQ_400HZ_DB dspId:DSP_8]) {
        sliderLGeq14.value = geqL14Value;
        if (sliderLGeq14.value > 0.0) {
            meterL14Top.progress = sliderLGeq14.value/GEQ_SLIDER_MAX_VALUE;
            meterL14Btm.progress = 0.0;
        } else if (sliderLGeq14.value < 0.0) {
            meterL14Btm.progress = sliderLGeq14.value/GEQ_SLIDER_MIN_VALUE;
            meterL14Top.progress = 0.0;
        } else {
            meterL14Top.progress = 0.0;
            meterL14Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderLGeq15.value != geqL15Value && ![viewController sendCommandExists:CMD_L_GEQ_500HZ_DB dspId:DSP_8]) {
        sliderLGeq15.value = geqL15Value;
        if (sliderLGeq15.value > 0.0) {
            meterL15Top.progress = sliderLGeq15.value/GEQ_SLIDER_MAX_VALUE;
            meterL15Btm.progress = 0.0;
        } else if (sliderLGeq15.value < 0.0) {
            meterL15Btm.progress = sliderLGeq15.value/GEQ_SLIDER_MIN_VALUE;
            meterL15Top.progress = 0.0;
        } else {
            meterL15Top.progress = 0.0;
            meterL15Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderLGeq16.value != geqL16Value && ![viewController sendCommandExists:CMD_L_GEQ_630HZ_DB dspId:DSP_8]) {
        sliderLGeq16.value = geqL16Value;
        if (sliderLGeq16.value > 0.0) {
            meterL16Top.progress = sliderLGeq16.value/GEQ_SLIDER_MAX_VALUE;
            meterL16Btm.progress = 0.0;
        } else if (sliderLGeq16.value < 0.0) {
            meterL16Btm.progress = sliderLGeq16.value/GEQ_SLIDER_MIN_VALUE;
            meterL16Top.progress = 0.0;
        } else {
            meterL16Top.progress = 0.0;
            meterL16Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderLGeq17.value != geqL17Value && ![viewController sendCommandExists:CMD_L_GEQ_800HZ_DB dspId:DSP_8]) {
        sliderLGeq17.value = geqL17Value;
        if (sliderLGeq17.value > 0.0) {
            meterL17Top.progress = sliderLGeq17.value/GEQ_SLIDER_MAX_VALUE;
            meterL17Btm.progress = 0.0;
        } else if (sliderLGeq17.value < 0.0) {
            meterL17Btm.progress = sliderLGeq17.value/GEQ_SLIDER_MIN_VALUE;
            meterL17Top.progress = 0.0;
        } else {
            meterL17Top.progress = 0.0;
            meterL17Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderLGeq18.value != geqL18Value && ![viewController sendCommandExists:CMD_L_GEQ_1KHZ_DB dspId:DSP_8]) {
        sliderLGeq18.value = geqL18Value;
        if (sliderLGeq18.value > 0.0) {
            meterL18Top.progress = sliderLGeq18.value/GEQ_SLIDER_MAX_VALUE;
            meterL18Btm.progress = 0.0;
        } else if (sliderLGeq18.value < 0.0) {
            meterL18Btm.progress = sliderLGeq18.value/GEQ_SLIDER_MIN_VALUE;
            meterL18Top.progress = 0.0;
        } else {
            meterL18Top.progress = 0.0;
            meterL18Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderLGeq19.value != geqL19Value && ![viewController sendCommandExists:CMD_L_GEQ_1_25KHZ_DB dspId:DSP_8]) {
        sliderLGeq19.value = geqL19Value;
        if (sliderLGeq19.value > 0.0) {
            meterL19Top.progress = sliderLGeq19.value/GEQ_SLIDER_MAX_VALUE;
            meterL19Btm.progress = 0.0;
        } else if (sliderLGeq19.value < 0.0) {
            meterL19Btm.progress = sliderLGeq19.value/GEQ_SLIDER_MIN_VALUE;
            meterL19Top.progress = 0.0;
        } else {
            meterL19Top.progress = 0.0;
            meterL19Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderLGeq20.value != geqL20Value && ![viewController sendCommandExists:CMD_L_GEQ_1_6KHZ_DB dspId:DSP_8]) {
        sliderLGeq20.value = geqL20Value;
        if (sliderLGeq20.value > 0.0) {
            meterL20Top.progress = sliderLGeq20.value/GEQ_SLIDER_MAX_VALUE;
            meterL20Btm.progress = 0.0;
        } else if (sliderLGeq20.value < 0.0) {
            meterL20Btm.progress = sliderLGeq20.value/GEQ_SLIDER_MIN_VALUE;
            meterL20Top.progress = 0.0;
        } else {
            meterL20Top.progress = 0.0;
            meterL20Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderLGeq21.value != geqL21Value && ![viewController sendCommandExists:CMD_L_GEQ_2KHZ_DB dspId:DSP_8]) {
        sliderLGeq21.value = geqL21Value;
        if (sliderLGeq21.value > 0.0) {
            meterL21Top.progress = sliderLGeq21.value/GEQ_SLIDER_MAX_VALUE;
            meterL21Btm.progress = 0.0;
        } else if (sliderLGeq21.value < 0.0) {
            meterL21Btm.progress = sliderLGeq21.value/GEQ_SLIDER_MIN_VALUE;
            meterL21Top.progress = 0.0;
        } else {
            meterL21Top.progress = 0.0;
            meterL21Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderLGeq22.value != geqL22Value && ![viewController sendCommandExists:CMD_L_GEQ_2_5KHZ_DB dspId:DSP_8]) {
        sliderLGeq22.value = geqL22Value;
        if (sliderLGeq22.value > 0.0) {
            meterL22Top.progress = sliderLGeq22.value/GEQ_SLIDER_MAX_VALUE;
            meterL22Btm.progress = 0.0;
        } else if (sliderLGeq22.value < 0.0) {
            meterL22Btm.progress = sliderLGeq22.value/GEQ_SLIDER_MIN_VALUE;
            meterL22Top.progress = 0.0;
        } else {
            meterL22Top.progress = 0.0;
            meterL22Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderLGeq23.value != geqL23Value && ![viewController sendCommandExists:CMD_L_GEQ_3_15KHZ_DB dspId:DSP_8]) {
        sliderLGeq23.value = geqL23Value;
        if (sliderLGeq23.value > 0.0) {
            meterL23Top.progress = sliderLGeq23.value/GEQ_SLIDER_MAX_VALUE;
            meterL23Btm.progress = 0.0;
        } else if (sliderLGeq23.value < 0.0) {
            meterL23Btm.progress = sliderLGeq23.value/GEQ_SLIDER_MIN_VALUE;
            meterL23Top.progress = 0.0;
        } else {
            meterL23Top.progress = 0.0;
            meterL23Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderLGeq24.value != geqL24Value && ![viewController sendCommandExists:CMD_L_GEQ_4KHZ_DB dspId:DSP_8]) {
        sliderLGeq24.value = geqL24Value;
        if (sliderLGeq24.value > 0.0) {
            meterL24Top.progress = sliderLGeq24.value/GEQ_SLIDER_MAX_VALUE;
            meterL24Btm.progress = 0.0;
        } else if (sliderLGeq24.value < 0.0) {
            meterL24Btm.progress = sliderLGeq24.value/GEQ_SLIDER_MIN_VALUE;
            meterL24Top.progress = 0.0;
        } else {
            meterL24Top.progress = 0.0;
            meterL24Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderLGeq25.value != geqL25Value && ![viewController sendCommandExists:CMD_L_GEQ_5KHZ_DB dspId:DSP_8]) {
        sliderLGeq25.value = geqL25Value;
        if (sliderLGeq25.value > 0.0) {
            meterL25Top.progress = sliderLGeq25.value/GEQ_SLIDER_MAX_VALUE;
            meterL25Btm.progress = 0.0;
        } else if (sliderLGeq25.value < 0.0) {
            meterL25Btm.progress = sliderLGeq25.value/GEQ_SLIDER_MIN_VALUE;
            meterL25Top.progress = 0.0;
        } else {
            meterL25Top.progress = 0.0;
            meterL25Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderLGeq26.value != geqL26Value && ![viewController sendCommandExists:CMD_L_GEQ_6_3KHZ_DB dspId:DSP_8]) {
        sliderLGeq26.value = geqL26Value;
        if (sliderLGeq26.value > 0.0) {
            meterL26Top.progress = sliderLGeq26.value/GEQ_SLIDER_MAX_VALUE;
            meterL26Btm.progress = 0.0;
        } else if (sliderLGeq26.value < 0.0) {
            meterL26Btm.progress = sliderLGeq26.value/GEQ_SLIDER_MIN_VALUE;
            meterL26Top.progress = 0.0;
        } else {
            meterL26Top.progress = 0.0;
            meterL26Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderLGeq27.value != geqL27Value && ![viewController sendCommandExists:CMD_L_GEQ_8KHZ_DB dspId:DSP_8]) {
        sliderLGeq27.value = geqL27Value;
        if (sliderLGeq27.value > 0.0) {
            meterL27Top.progress = sliderLGeq27.value/GEQ_SLIDER_MAX_VALUE;
            meterL27Btm.progress = 0.0;
        } else if (sliderLGeq27.value < 0.0) {
            meterL27Btm.progress = sliderLGeq27.value/GEQ_SLIDER_MIN_VALUE;
            meterL27Top.progress = 0.0;
        } else {
            meterL27Top.progress = 0.0;
            meterL27Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderLGeq28.value != geqL28Value && ![viewController sendCommandExists:CMD_L_GEQ_10KHZ_DB dspId:DSP_8]) {
        sliderLGeq28.value = geqL28Value;
        if (sliderLGeq28.value > 0.0) {
            meterL28Top.progress = sliderLGeq28.value/GEQ_SLIDER_MAX_VALUE;
            meterL28Btm.progress = 0.0;
        } else if (sliderLGeq28.value < 0.0) {
            meterL28Btm.progress = sliderLGeq28.value/GEQ_SLIDER_MIN_VALUE;
            meterL28Top.progress = 0.0;
        } else {
            meterL28Top.progress = 0.0;
            meterL28Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderLGeq29.value != geqL29Value && ![viewController sendCommandExists:CMD_L_GEQ_12_5KHZ_DB dspId:DSP_8]) {
        sliderLGeq29.value = geqL29Value;
        if (sliderLGeq29.value > 0.0) {
            meterL29Top.progress = sliderLGeq29.value/GEQ_SLIDER_MAX_VALUE;
            meterL29Btm.progress = 0.0;
        } else if (sliderLGeq29.value < 0.0) {
            meterL29Btm.progress = sliderLGeq29.value/GEQ_SLIDER_MIN_VALUE;
            meterL29Top.progress = 0.0;
        } else {
            meterL29Top.progress = 0.0;
            meterL29Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderLGeq30.value != geqL30Value && ![viewController sendCommandExists:CMD_L_GEQ_16KHZ_DB dspId:DSP_8]) {
        sliderLGeq30.value = geqL30Value;
        if (sliderLGeq30.value > 0.0) {
            meterL30Top.progress = sliderLGeq30.value/GEQ_SLIDER_MAX_VALUE;
            meterL30Btm.progress = 0.0;
        } else if (sliderLGeq30.value < 0.0) {
            meterL30Btm.progress = sliderLGeq30.value/GEQ_SLIDER_MIN_VALUE;
            meterL30Top.progress = 0.0;
        } else {
            meterL30Top.progress = 0.0;
            meterL30Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderLGeq31.value != geqL31Value && ![viewController sendCommandExists:CMD_L_GEQ_20KHZ_DB dspId:DSP_8]) {
        sliderLGeq31.value = geqL31Value;
        if (sliderLGeq31.value > 0.0) {
            meterL31Top.progress = sliderLGeq31.value/GEQ_SLIDER_MAX_VALUE;
            meterL31Btm.progress = 0.0;
        } else if (sliderLGeq31.value < 0.0) {
            meterL31Btm.progress = sliderLGeq31.value/GEQ_SLIDER_MIN_VALUE;
            meterL31Top.progress = 0.0;
        } else {
            meterL31Top.progress = 0.0;
            meterL31Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    
    int geqR1Value = pkt->r_geq31_20_hz_db;
    int geqR2Value = pkt->r_geq31_25_hz_db;
    int geqR3Value = pkt->r_geq31_31_5_hz_db;
    int geqR4Value = pkt->r_geq31_40_hz_db;
    int geqR5Value = pkt->r_geq31_50_hz_db;
    int geqR6Value = pkt->r_geq31_63_hz_db;
    int geqR7Value = pkt->r_geq31_80_hz_db;
    int geqR8Value = pkt->r_geq31_100_hz_db;
    int geqR9Value = pkt->r_geq31_125_hz_db;
    int geqR10Value = pkt->r_geq31_160_hz_db;
    int geqR11Value = pkt->r_geq31_200_hz_db;
    int geqR12Value = pkt->r_geq31_250_hz_db;
    int geqR13Value = pkt->r_geq31_315_hz_db;
    int geqR14Value = pkt->r_geq31_400_hz_db;
    int geqR15Value = pkt->r_geq31_500_hz_db;
    int geqR16Value = pkt->r_geq31_630_hz_db;
    int geqR17Value = pkt->r_geq31_800_hz_db;
    int geqR18Value = pkt->r_geq31_1_khz_db;
    int geqR19Value = pkt->r_geq31_1_25_khz_db;
    int geqR20Value = pkt->r_geq31_1_6_khz_db;
    int geqR21Value = pkt->r_geq31_2_khz_db;
    int geqR22Value = pkt->r_geq31_2_5_khz_db;
    int geqR23Value = pkt->r_geq31_3_15_khz_db;
    int geqR24Value = pkt->r_geq31_4_khz_db;
    int geqR25Value = pkt->r_geq31_5_khz_db;
    int geqR26Value = pkt->r_geq31_6_3_khz_db;
    int geqR27Value = pkt->r_geq31_8_khz_db;
    int geqR28Value = pkt->r_geq31_10_khz_db;
    int geqR29Value = pkt->r_geq31_12_5_khz_db;
    int geqR30Value = pkt->r_geq31_16_khz_db;
    int geqR31Value = pkt->r_geq31_20_khz_db;
    
    if (sliderRGeq1.value != geqR1Value && ![viewController sendCommandExists:CMD_R_GEQ_20HZ_DB dspId:DSP_8]) {
        sliderRGeq1.value = geqR1Value;
        if (sliderRGeq1.value > 0.0) {
            meterR1Top.progress = sliderRGeq1.value/GEQ_SLIDER_MAX_VALUE;
            meterR1Btm.progress = 0.0;
        } else if (sliderRGeq1.value < 0.0) {
            meterR1Btm.progress = sliderRGeq1.value/GEQ_SLIDER_MIN_VALUE;
            meterR1Top.progress = 0.0;
        } else {
            meterR1Top.progress = 0.0;
            meterR1Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderRGeq2.value != geqR2Value && ![viewController sendCommandExists:CMD_R_GEQ_25HZ_DB dspId:DSP_8]) {
        sliderRGeq2.value = geqR2Value;
        if (sliderRGeq2.value > 0.0) {
            meterR2Top.progress = sliderRGeq2.value/GEQ_SLIDER_MAX_VALUE;
            meterR2Btm.progress = 0.0;
        } else if (sliderRGeq2.value < 0.0) {
            meterR2Btm.progress = sliderRGeq2.value/GEQ_SLIDER_MIN_VALUE;
            meterR2Top.progress = 0.0;
        } else {
            meterR2Top.progress = 0.0;
            meterR2Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderRGeq3.value != geqR3Value && ![viewController sendCommandExists:CMD_R_GEQ_31_5HZ_DB dspId:DSP_8]) {
        sliderRGeq3.value = geqR3Value;
        if (sliderRGeq3.value > 0.0) {
            meterR3Top.progress = sliderRGeq3.value/GEQ_SLIDER_MAX_VALUE;
            meterR3Btm.progress = 0.0;
        } else if (sliderRGeq3.value < 0.0) {
            meterR3Btm.progress = sliderRGeq3.value/GEQ_SLIDER_MIN_VALUE;
            meterR3Top.progress = 0.0;
        } else {
            meterR3Top.progress = 0.0;
            meterR3Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderRGeq4.value != geqR4Value && ![viewController sendCommandExists:CMD_R_GEQ_40HZ_DB dspId:DSP_8]) {
        sliderRGeq4.value = geqR4Value;
        if (sliderRGeq4.value > 0.0) {
            meterR4Top.progress = sliderRGeq4.value/GEQ_SLIDER_MAX_VALUE;
            meterR4Btm.progress = 0.0;
        } else if (sliderRGeq4.value < 0.0) {
            meterR4Btm.progress = sliderRGeq4.value/GEQ_SLIDER_MIN_VALUE;
            meterR4Top.progress = 0.0;
        } else {
            meterR4Top.progress = 0.0;
            meterR4Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderRGeq5.value != geqR5Value && ![viewController sendCommandExists:CMD_R_GEQ_50HZ_DB dspId:DSP_8]) {
        sliderRGeq5.value = geqR5Value;
        if (sliderRGeq5.value > 0.0) {
            meterR5Top.progress = sliderRGeq5.value/GEQ_SLIDER_MAX_VALUE;
            meterR5Btm.progress = 0.0;
        } else if (sliderRGeq5.value < 0.0) {
            meterR5Btm.progress = sliderRGeq5.value/GEQ_SLIDER_MIN_VALUE;
            meterR5Top.progress = 0.0;
        } else {
            meterR5Top.progress = 0.0;
            meterR5Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderRGeq6.value != geqR6Value && ![viewController sendCommandExists:CMD_R_GEQ_63HZ_DB dspId:DSP_8]) {
        sliderRGeq6.value = geqR6Value;
        if (sliderRGeq6.value > 0.0) {
            meterR6Top.progress = sliderRGeq6.value/GEQ_SLIDER_MAX_VALUE;
            meterR6Btm.progress = 0.0;
        } else if (sliderRGeq6.value < 0.0) {
            meterR6Btm.progress = sliderRGeq6.value/GEQ_SLIDER_MIN_VALUE;
            meterR6Top.progress = 0.0;
        } else {
            meterR6Top.progress = 0.0;
            meterR6Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderRGeq7.value != geqR7Value && ![viewController sendCommandExists:CMD_R_GEQ_80HZ_DB dspId:DSP_8]) {
        sliderRGeq7.value = geqR7Value;
        if (sliderRGeq7.value > 0.0) {
            meterR7Top.progress = sliderRGeq7.value/GEQ_SLIDER_MAX_VALUE;
            meterR7Btm.progress = 0.0;
        } else if (sliderRGeq7.value < 0.0) {
            meterR7Btm.progress = sliderRGeq7.value/GEQ_SLIDER_MIN_VALUE;
            meterR7Top.progress = 0.0;
        } else {
            meterR7Top.progress = 0.0;
            meterR7Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderRGeq8.value != geqR8Value && ![viewController sendCommandExists:CMD_R_GEQ_100HZ_DB dspId:DSP_8]) {
        sliderRGeq8.value = geqR8Value;
        if (sliderRGeq8.value > 0.0) {
            meterR8Top.progress = sliderRGeq8.value/GEQ_SLIDER_MAX_VALUE;
            meterR8Btm.progress = 0.0;
        } else if (sliderRGeq8.value < 0.0) {
            meterR8Btm.progress = sliderRGeq8.value/GEQ_SLIDER_MIN_VALUE;
            meterR8Top.progress = 0.0;
        } else {
            meterR8Top.progress = 0.0;
            meterR8Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderRGeq9.value != geqR9Value && ![viewController sendCommandExists:CMD_R_GEQ_125HZ_DB dspId:DSP_8]) {
        sliderRGeq9.value = geqR9Value;
        if (sliderRGeq9.value > 0.0) {
            meterR9Top.progress = sliderRGeq9.value/GEQ_SLIDER_MAX_VALUE;
            meterR9Btm.progress = 0.0;
        } else if (sliderRGeq9.value < 0.0) {
            meterR9Btm.progress = sliderRGeq9.value/GEQ_SLIDER_MIN_VALUE;
            meterR9Top.progress = 0.0;
        } else {
            meterR9Top.progress = 0.0;
            meterR9Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderRGeq10.value != geqR10Value && ![viewController sendCommandExists:CMD_R_GEQ_160HZ_DB dspId:DSP_8]) {
        sliderRGeq10.value = geqR10Value;
        if (sliderRGeq10.value > 0.0) {
            meterR10Top.progress = sliderRGeq10.value/GEQ_SLIDER_MAX_VALUE;
            meterR10Btm.progress = 0.0;
        } else if (sliderRGeq10.value < 0.0) {
            meterR10Btm.progress = sliderRGeq10.value/GEQ_SLIDER_MIN_VALUE;
            meterR10Top.progress = 0.0;
        } else {
            meterR10Top.progress = 0.0;
            meterR10Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderRGeq11.value != geqR11Value && ![viewController sendCommandExists:CMD_R_GEQ_200HZ_DB dspId:DSP_8]) {
        sliderRGeq11.value = geqR11Value;
        if (sliderRGeq11.value > 0.0) {
            meterR11Top.progress = sliderRGeq11.value/GEQ_SLIDER_MAX_VALUE;
            meterR11Btm.progress = 0.0;
        } else if (sliderRGeq11.value < 0.0) {
            meterR11Btm.progress = sliderRGeq11.value/GEQ_SLIDER_MIN_VALUE;
            meterR1Top.progress = 0.0;
        } else {
            meterR11Top.progress = 0.0;
            meterR11Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderRGeq12.value != geqR12Value && ![viewController sendCommandExists:CMD_R_GEQ_250HZ_DB dspId:DSP_8]) {
        sliderRGeq12.value = geqR12Value;
        if (sliderRGeq12.value > 0.0) {
            meterR12Top.progress = sliderRGeq12.value/GEQ_SLIDER_MAX_VALUE;
            meterR12Btm.progress = 0.0;
        } else if (sliderRGeq12.value < 0.0) {
            meterR12Btm.progress = sliderRGeq12.value/GEQ_SLIDER_MIN_VALUE;
            meterR12Top.progress = 0.0;
        } else {
            meterR12Top.progress = 0.0;
            meterR12Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderRGeq13.value != geqR13Value && ![viewController sendCommandExists:CMD_R_GEQ_315HZ_DB dspId:DSP_8]) {
        sliderRGeq13.value = geqR13Value;
        if (sliderRGeq13.value > 0.0) {
            meterR13Top.progress = sliderRGeq13.value/GEQ_SLIDER_MAX_VALUE;
            meterR13Btm.progress = 0.0;
        } else if (sliderRGeq13.value < 0.0) {
            meterR13Btm.progress = sliderRGeq13.value/GEQ_SLIDER_MIN_VALUE;
            meterR13Top.progress = 0.0;
        } else {
            meterR13Top.progress = 0.0;
            meterR13Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderRGeq14.value != geqR14Value && ![viewController sendCommandExists:CMD_R_GEQ_400HZ_DB dspId:DSP_8]) {
        sliderRGeq14.value = geqR14Value;
        if (sliderRGeq14.value > 0.0) {
            meterR14Top.progress = sliderRGeq14.value/GEQ_SLIDER_MAX_VALUE;
            meterR14Btm.progress = 0.0;
        } else if (sliderRGeq14.value < 0.0) {
            meterR14Btm.progress = sliderRGeq14.value/GEQ_SLIDER_MIN_VALUE;
            meterR14Top.progress = 0.0;
        } else {
            meterR14Top.progress = 0.0;
            meterR14Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderRGeq15.value != geqR15Value && ![viewController sendCommandExists:CMD_R_GEQ_500HZ_DB dspId:DSP_8]) {
        sliderRGeq15.value = geqR15Value;
        if (sliderRGeq15.value > 0.0) {
            meterR15Top.progress = sliderRGeq15.value/GEQ_SLIDER_MAX_VALUE;
            meterR15Btm.progress = 0.0;
        } else if (sliderRGeq15.value < 0.0) {
            meterR15Btm.progress = sliderRGeq15.value/GEQ_SLIDER_MIN_VALUE;
            meterR15Top.progress = 0.0;
        } else {
            meterR15Top.progress = 0.0;
            meterR15Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderRGeq16.value != geqR16Value && ![viewController sendCommandExists:CMD_R_GEQ_630HZ_DB dspId:DSP_8]) {
        sliderRGeq16.value = geqR16Value;
        if (sliderRGeq16.value > 0.0) {
            meterR16Top.progress = sliderRGeq16.value/GEQ_SLIDER_MAX_VALUE;
            meterR16Btm.progress = 0.0;
        } else if (sliderRGeq16.value < 0.0) {
            meterR16Btm.progress = sliderRGeq16.value/GEQ_SLIDER_MIN_VALUE;
            meterR16Top.progress = 0.0;
        } else {
            meterR16Top.progress = 0.0;
            meterR16Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderRGeq17.value != geqR17Value && ![viewController sendCommandExists:CMD_R_GEQ_800HZ_DB dspId:DSP_8]) {
        sliderRGeq17.value = geqR17Value;
        if (sliderRGeq17.value > 0.0) {
            meterR17Top.progress = sliderRGeq17.value/GEQ_SLIDER_MAX_VALUE;
            meterR17Btm.progress = 0.0;
        } else if (sliderRGeq17.value < 0.0) {
            meterR17Btm.progress = sliderRGeq17.value/GEQ_SLIDER_MIN_VALUE;
            meterR17Top.progress = 0.0;
        } else {
            meterR17Top.progress = 0.0;
            meterR17Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderRGeq18.value != geqR18Value && ![viewController sendCommandExists:CMD_R_GEQ_1KHZ_DB dspId:DSP_8]) {
        sliderRGeq18.value = geqR18Value;
        if (sliderRGeq18.value > 0.0) {
            meterR18Top.progress = sliderRGeq18.value/GEQ_SLIDER_MAX_VALUE;
            meterR18Btm.progress = 0.0;
        } else if (sliderRGeq18.value < 0.0) {
            meterR18Btm.progress = sliderRGeq18.value/GEQ_SLIDER_MIN_VALUE;
            meterR18Top.progress = 0.0;
        } else {
            meterR18Top.progress = 0.0;
            meterR18Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderRGeq19.value != geqR19Value && ![viewController sendCommandExists:CMD_R_GEQ_1_25KHZ_DB dspId:DSP_8]) {
        sliderRGeq19.value = geqR19Value;
        if (sliderRGeq19.value > 0.0) {
            meterR19Top.progress = sliderRGeq19.value/GEQ_SLIDER_MAX_VALUE;
            meterR19Btm.progress = 0.0;
        } else if (sliderRGeq19.value < 0.0) {
            meterR19Btm.progress = sliderRGeq19.value/GEQ_SLIDER_MIN_VALUE;
            meterR19Top.progress = 0.0;
        } else {
            meterR19Top.progress = 0.0;
            meterR19Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderRGeq20.value != geqR20Value && ![viewController sendCommandExists:CMD_R_GEQ_1_6KHZ_DB dspId:DSP_8]) {
        sliderRGeq20.value = geqR20Value;
        if (sliderRGeq20.value > 0.0) {
            meterR20Top.progress = sliderRGeq20.value/GEQ_SLIDER_MAX_VALUE;
            meterR20Btm.progress = 0.0;
        } else if (sliderRGeq20.value < 0.0) {
            meterR20Btm.progress = sliderRGeq20.value/GEQ_SLIDER_MIN_VALUE;
            meterR20Top.progress = 0.0;
        } else {
            meterR20Top.progress = 0.0;
            meterR20Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderRGeq21.value != geqR21Value && ![viewController sendCommandExists:CMD_R_GEQ_2KHZ_DB dspId:DSP_8]) {
        sliderRGeq21.value = geqR21Value;
        if (sliderRGeq21.value > 0.0) {
            meterR21Top.progress = sliderRGeq21.value/GEQ_SLIDER_MAX_VALUE;
            meterR21Btm.progress = 0.0;
        } else if (sliderRGeq21.value < 0.0) {
            meterR21Btm.progress = sliderRGeq21.value/GEQ_SLIDER_MIN_VALUE;
            meterR21Top.progress = 0.0;
        } else {
            meterR21Top.progress = 0.0;
            meterR21Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderRGeq22.value != geqR22Value && ![viewController sendCommandExists:CMD_R_GEQ_2_5KHZ_DB dspId:DSP_8]) {
        sliderRGeq22.value = geqR22Value;
        if (sliderRGeq22.value > 0.0) {
            meterR22Top.progress = sliderRGeq22.value/GEQ_SLIDER_MAX_VALUE;
            meterR22Btm.progress = 0.0;
        } else if (sliderRGeq22.value < 0.0) {
            meterR22Btm.progress = sliderRGeq22.value/GEQ_SLIDER_MIN_VALUE;
            meterR22Top.progress = 0.0;
        } else {
            meterR22Top.progress = 0.0;
            meterR22Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderRGeq23.value != geqR23Value && ![viewController sendCommandExists:CMD_R_GEQ_3_15KHZ_DB dspId:DSP_8]) {
        sliderRGeq23.value = geqR23Value;
        if (sliderRGeq23.value > 0.0) {
            meterR23Top.progress = sliderRGeq23.value/GEQ_SLIDER_MAX_VALUE;
            meterR23Btm.progress = 0.0;
        } else if (sliderRGeq23.value < 0.0) {
            meterR23Btm.progress = sliderRGeq23.value/GEQ_SLIDER_MIN_VALUE;
            meterR23Top.progress = 0.0;
        } else {
            meterR23Top.progress = 0.0;
            meterR23Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderRGeq24.value != geqR24Value && ![viewController sendCommandExists:CMD_R_GEQ_4KHZ_DB dspId:DSP_8]) {
        sliderRGeq24.value = geqR24Value;
        if (sliderRGeq24.value > 0.0) {
            meterR24Top.progress = sliderRGeq24.value/GEQ_SLIDER_MAX_VALUE;
            meterR24Btm.progress = 0.0;
        } else if (sliderRGeq24.value < 0.0) {
            meterR24Btm.progress = sliderRGeq24.value/GEQ_SLIDER_MIN_VALUE;
            meterR24Top.progress = 0.0;
        } else {
            meterR24Top.progress = 0.0;
            meterR24Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderRGeq25.value != geqR25Value && ![viewController sendCommandExists:CMD_R_GEQ_5KHZ_DB dspId:DSP_8]) {
        sliderRGeq25.value = geqR25Value;
        if (sliderRGeq25.value > 0.0) {
            meterR25Top.progress = sliderRGeq25.value/GEQ_SLIDER_MAX_VALUE;
            meterR25Btm.progress = 0.0;
        } else if (sliderRGeq25.value < 0.0) {
            meterR25Btm.progress = sliderRGeq25.value/GEQ_SLIDER_MIN_VALUE;
            meterR25Top.progress = 0.0;
        } else {
            meterR25Top.progress = 0.0;
            meterR25Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderRGeq26.value != geqR26Value && ![viewController sendCommandExists:CMD_R_GEQ_6_3KHZ_DB dspId:DSP_8]) {
        sliderRGeq26.value = geqR26Value;
        if (sliderRGeq26.value > 0.0) {
            meterR26Top.progress = sliderRGeq26.value/GEQ_SLIDER_MAX_VALUE;
            meterR26Btm.progress = 0.0;
        } else if (sliderRGeq26.value < 0.0) {
            meterR26Btm.progress = sliderRGeq26.value/GEQ_SLIDER_MIN_VALUE;
            meterR26Top.progress = 0.0;
        } else {
            meterR26Top.progress = 0.0;
            meterR26Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderRGeq27.value != geqR27Value && ![viewController sendCommandExists:CMD_R_GEQ_8KHZ_DB dspId:DSP_8]) {
        sliderRGeq27.value = geqR27Value;
        if (sliderRGeq27.value > 0.0) {
            meterR27Top.progress = sliderRGeq27.value/GEQ_SLIDER_MAX_VALUE;
            meterR27Btm.progress = 0.0;
        } else if (sliderRGeq27.value < 0.0) {
            meterR27Btm.progress = sliderRGeq27.value/GEQ_SLIDER_MIN_VALUE;
            meterR27Top.progress = 0.0;
        } else {
            meterR27Top.progress = 0.0;
            meterR27Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderRGeq28.value != geqR28Value && ![viewController sendCommandExists:CMD_R_GEQ_10KHZ_DB dspId:DSP_8]) {
        sliderRGeq28.value = geqR28Value;
        if (sliderRGeq28.value > 0.0) {
            meterR28Top.progress = sliderRGeq28.value/GEQ_SLIDER_MAX_VALUE;
            meterR28Btm.progress = 0.0;
        } else if (sliderRGeq28.value < 0.0) {
            meterR28Btm.progress = sliderRGeq28.value/GEQ_SLIDER_MIN_VALUE;
            meterR28Top.progress = 0.0;
        } else {
            meterR28Top.progress = 0.0;
            meterR28Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderRGeq29.value != geqR29Value && ![viewController sendCommandExists:CMD_R_GEQ_12_5KHZ_DB dspId:DSP_8]) {
        sliderRGeq29.value = geqR29Value;
        if (sliderRGeq29.value > 0.0) {
            meterR29Top.progress = sliderRGeq29.value/GEQ_SLIDER_MAX_VALUE;
            meterR29Btm.progress = 0.0;
        } else if (sliderRGeq29.value < 0.0) {
            meterR29Btm.progress = sliderRGeq29.value/GEQ_SLIDER_MIN_VALUE;
            meterR29Top.progress = 0.0;
        } else {
            meterR29Top.progress = 0.0;
            meterR29Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderRGeq30.value != geqR30Value && ![viewController sendCommandExists:CMD_R_GEQ_16KHZ_DB dspId:DSP_8]) {
        sliderRGeq30.value = geqR30Value;
        if (sliderRGeq30.value > 0.0) {
            meterR30Top.progress = sliderRGeq30.value/GEQ_SLIDER_MAX_VALUE;
            meterR30Btm.progress = 0.0;
        } else if (sliderRGeq30.value < 0.0) {
            meterR30Btm.progress = sliderRGeq30.value/GEQ_SLIDER_MIN_VALUE;
            meterR30Top.progress = 0.0;
        } else {
            meterR30Top.progress = 0.0;
            meterR30Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderRGeq31.value != geqR31Value && ![viewController sendCommandExists:CMD_R_GEQ_20KHZ_DB dspId:DSP_8]) {
        sliderRGeq31.value = geqR31Value;
        if (sliderRGeq31.value > 0.0) {
            meterR31Top.progress = sliderRGeq31.value/GEQ_SLIDER_MAX_VALUE;
            meterR31Btm.progress = 0.0;
        } else if (sliderRGeq31.value < 0.0) {
            meterR31Btm.progress = sliderRGeq31.value/GEQ_SLIDER_MIN_VALUE;
            meterR31Top.progress = 0.0;
        } else {
            meterR31Top.progress = 0.0;
            meterR31Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }

    if (needsUpdate) {
        [self updatePreviewImage];
    }
}

- (void)setGeqSoloOnOff:(BOOL)value {
    if (value) {
        
    } else {
        
    }
}

- (void)setGeqOnOff:(BOOL)value {
    geqOnOff = value;
    [self updateGeqMeters];
}

/*
- (void)setLinkMode:(BOOL)value {
    geqLinkMode = value;
    btnGeqLink.selected = value;
}
*/

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

// Convert touch point to sliderL value
- (float)pointToSliderValue:(CGPoint)point
{
    return (12.0 - (12.0/108.0) * point.y);
}

- (void)touchesBegan:(NSSet*)touches withEvent:(UIEvent*)event
{
    /*
     UITouch *touch = [touches anyObject];
     if (!viewGeq.isHidden && btnGeqDraw.selected) {
     CGPoint startPoint = [touch locationInView:self.viewGeqDraw];
     NSLog(@"Start point = (%.f, %.f)", startPoint.x, startPoint.y);
     }
     */
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    geqLock = YES;
    
    UITouch *touch = [touches anyObject];
    
    // Handle GEQ draw
    if (btnGeqDraw.selected) {
        CGPoint touchPoint = [touch locationInView:self.viewGeqDrawL];
        //NSLog(@"Touch point L = (%.f, %.f)", touchPoint.x, touchPoint.y);
        
        if (CGRectContainsPoint(sliderLGeq1.frame, touchPoint)) {
            sliderLGeq1.value = [self pointToSliderValue:touchPoint];
            [self onSlider1Action:sliderLGeq1];
        } else if (CGRectContainsPoint(sliderLGeq2.frame, touchPoint)) {
            sliderLGeq2.value = [self pointToSliderValue:touchPoint];
            [self onSlider2Action:sliderLGeq2];
        } else if (CGRectContainsPoint(sliderLGeq3.frame, touchPoint)) {
            sliderLGeq3.value = [self pointToSliderValue:touchPoint];
            [self onSlider3Action:sliderLGeq3];
        } else if (CGRectContainsPoint(sliderLGeq4.frame, touchPoint)) {
            sliderLGeq4.value = [self pointToSliderValue:touchPoint];
            [self onSlider4Action:sliderLGeq4];
        } else if (CGRectContainsPoint(sliderLGeq5.frame, touchPoint)) {
            sliderLGeq5.value = [self pointToSliderValue:touchPoint];
            [self onSlider5Action:sliderLGeq5];
        } else if (CGRectContainsPoint(sliderLGeq6.frame, touchPoint)) {
            sliderLGeq6.value = [self pointToSliderValue:touchPoint];
            [self onSlider6Action:sliderLGeq6];
        } else if (CGRectContainsPoint(sliderLGeq7.frame, touchPoint)) {
            sliderLGeq7.value = [self pointToSliderValue:touchPoint];
            [self onSlider7Action:sliderLGeq7];
        } else if (CGRectContainsPoint(sliderLGeq8.frame, touchPoint)) {
            sliderLGeq8.value = [self pointToSliderValue:touchPoint];
            [self onSlider8Action:sliderLGeq8];
        } else if (CGRectContainsPoint(sliderLGeq9.frame, touchPoint)) {
            sliderLGeq9.value = [self pointToSliderValue:touchPoint];
            [self onSlider9Action:sliderLGeq9];
        } else if (CGRectContainsPoint(sliderLGeq10.frame, touchPoint)) {
            sliderLGeq10.value = [self pointToSliderValue:touchPoint];
            [self onSlider10Action:sliderLGeq10];
        } else if (CGRectContainsPoint(sliderLGeq11.frame, touchPoint)) {
            sliderLGeq11.value = [self pointToSliderValue:touchPoint];
            [self onSlider11Action:sliderLGeq11];
        } else if (CGRectContainsPoint(sliderLGeq12.frame, touchPoint)) {
            sliderLGeq12.value = [self pointToSliderValue:touchPoint];
            [self onSlider12Action:sliderLGeq12];
        } else if (CGRectContainsPoint(sliderLGeq13.frame, touchPoint)) {
            sliderLGeq13.value = [self pointToSliderValue:touchPoint];
            [self onSlider13Action:sliderLGeq13];
        } else if (CGRectContainsPoint(sliderLGeq14.frame, touchPoint)) {
            sliderLGeq14.value = [self pointToSliderValue:touchPoint];
            [self onSlider14Action:sliderLGeq14];
        } else if (CGRectContainsPoint(sliderLGeq15.frame, touchPoint)) {
            sliderLGeq15.value = [self pointToSliderValue:touchPoint];
            [self onSlider15Action:sliderLGeq15];
        } else if (CGRectContainsPoint(sliderLGeq16.frame, touchPoint)) {
            sliderLGeq16.value = [self pointToSliderValue:touchPoint];
            [self onSlider16Action:sliderLGeq16];
        } else if (CGRectContainsPoint(sliderLGeq17.frame, touchPoint)) {
            sliderLGeq17.value = [self pointToSliderValue:touchPoint];
            [self onSlider17Action:sliderLGeq17];
        } else if (CGRectContainsPoint(sliderLGeq18.frame, touchPoint)) {
            sliderLGeq18.value = [self pointToSliderValue:touchPoint];
            [self onSlider18Action:sliderLGeq18];
        } else if (CGRectContainsPoint(sliderLGeq19.frame, touchPoint)) {
            sliderLGeq19.value = [self pointToSliderValue:touchPoint];
            [self onSlider19Action:sliderLGeq19];
        } else if (CGRectContainsPoint(sliderLGeq20.frame, touchPoint)) {
            sliderLGeq20.value = [self pointToSliderValue:touchPoint];
            [self onSlider20Action:sliderLGeq20];
        } else if (CGRectContainsPoint(sliderLGeq21.frame, touchPoint)) {
            sliderLGeq21.value = [self pointToSliderValue:touchPoint];
            [self onSlider21Action:sliderLGeq21];
        } else if (CGRectContainsPoint(sliderLGeq22.frame, touchPoint)) {
            sliderLGeq22.value = [self pointToSliderValue:touchPoint];
            [self onSlider22Action:sliderLGeq22];
        } else if (CGRectContainsPoint(sliderLGeq23.frame, touchPoint)) {
            sliderLGeq23.value = [self pointToSliderValue:touchPoint];
            [self onSlider23Action:sliderLGeq23];
        } else if (CGRectContainsPoint(sliderLGeq24.frame, touchPoint)) {
            sliderLGeq24.value = [self pointToSliderValue:touchPoint];
            [self onSlider24Action:sliderLGeq24];
        } else if (CGRectContainsPoint(sliderLGeq25.frame, touchPoint)) {
            sliderLGeq25.value = [self pointToSliderValue:touchPoint];
            [self onSlider25Action:sliderLGeq25];
        } else if (CGRectContainsPoint(sliderLGeq26.frame, touchPoint)) {
            sliderLGeq26.value = [self pointToSliderValue:touchPoint];
            [self onSlider26Action:sliderLGeq26];
        } else if (CGRectContainsPoint(sliderLGeq27.frame, touchPoint)) {
            sliderLGeq27.value = [self pointToSliderValue:touchPoint];
            [self onSlider27Action:sliderLGeq27];
        } else if (CGRectContainsPoint(sliderLGeq28.frame, touchPoint)) {
            sliderLGeq28.value = [self pointToSliderValue:touchPoint];
            [self onSlider28Action:sliderLGeq28];
        } else if (CGRectContainsPoint(sliderLGeq29.frame, touchPoint)) {
            sliderLGeq29.value = [self pointToSliderValue:touchPoint];
            [self onSlider29Action:sliderLGeq29];
        } else if (CGRectContainsPoint(sliderLGeq30.frame, touchPoint)) {
            sliderLGeq30.value = [self pointToSliderValue:touchPoint];
            [self onSlider30Action:sliderLGeq30];
        } else if (CGRectContainsPoint(sliderLGeq31.frame, touchPoint)) {
            sliderLGeq31.value = [self pointToSliderValue:touchPoint];
            [self onSlider31Action:sliderLGeq31];
        }
        
        touchPoint = [touch locationInView:self.viewGeqDrawR];
        //NSLog(@"Touch point R = (%.f, %.f)", touchPoint.x, touchPoint.y);
        
        if (CGRectContainsPoint(sliderRGeq1.frame, touchPoint)) {
            sliderRGeq1.value = [self pointToSliderValue:touchPoint];
            [self onSlider1Action:sliderRGeq1];
        } else if (CGRectContainsPoint(sliderRGeq2.frame, touchPoint)) {
            sliderRGeq2.value = [self pointToSliderValue:touchPoint];
            [self onSlider2Action:sliderRGeq2];
        } else if (CGRectContainsPoint(sliderRGeq3.frame, touchPoint)) {
            sliderRGeq3.value = [self pointToSliderValue:touchPoint];
            [self onSlider3Action:sliderRGeq3];
        } else if (CGRectContainsPoint(sliderRGeq4.frame, touchPoint)) {
            sliderRGeq4.value = [self pointToSliderValue:touchPoint];
            [self onSlider4Action:sliderRGeq4];
        } else if (CGRectContainsPoint(sliderRGeq5.frame, touchPoint)) {
            sliderRGeq5.value = [self pointToSliderValue:touchPoint];
            [self onSlider5Action:sliderRGeq5];
        } else if (CGRectContainsPoint(sliderRGeq6.frame, touchPoint)) {
            sliderRGeq6.value = [self pointToSliderValue:touchPoint];
            [self onSlider6Action:sliderRGeq6];
        } else if (CGRectContainsPoint(sliderRGeq7.frame, touchPoint)) {
            sliderRGeq7.value = [self pointToSliderValue:touchPoint];
            [self onSlider7Action:sliderRGeq7];
        } else if (CGRectContainsPoint(sliderRGeq8.frame, touchPoint)) {
            sliderRGeq8.value = [self pointToSliderValue:touchPoint];
            [self onSlider8Action:sliderRGeq8];
        } else if (CGRectContainsPoint(sliderRGeq9.frame, touchPoint)) {
            sliderRGeq9.value = [self pointToSliderValue:touchPoint];
            [self onSlider9Action:sliderRGeq9];
        } else if (CGRectContainsPoint(sliderRGeq10.frame, touchPoint)) {
            sliderRGeq10.value = [self pointToSliderValue:touchPoint];
            [self onSlider10Action:sliderRGeq10];
        } else if (CGRectContainsPoint(sliderRGeq11.frame, touchPoint)) {
            sliderRGeq11.value = [self pointToSliderValue:touchPoint];
            [self onSlider11Action:sliderRGeq11];
        } else if (CGRectContainsPoint(sliderRGeq12.frame, touchPoint)) {
            sliderRGeq12.value = [self pointToSliderValue:touchPoint];
            [self onSlider12Action:sliderRGeq12];
        } else if (CGRectContainsPoint(sliderRGeq13.frame, touchPoint)) {
            sliderRGeq13.value = [self pointToSliderValue:touchPoint];
            [self onSlider13Action:sliderRGeq13];
        } else if (CGRectContainsPoint(sliderRGeq14.frame, touchPoint)) {
            sliderRGeq14.value = [self pointToSliderValue:touchPoint];
            [self onSlider14Action:sliderRGeq14];
        } else if (CGRectContainsPoint(sliderRGeq15.frame, touchPoint)) {
            sliderRGeq15.value = [self pointToSliderValue:touchPoint];
            [self onSlider15Action:sliderRGeq15];
        } else if (CGRectContainsPoint(sliderRGeq16.frame, touchPoint)) {
            sliderRGeq16.value = [self pointToSliderValue:touchPoint];
            [self onSlider16Action:sliderRGeq16];
        } else if (CGRectContainsPoint(sliderRGeq17.frame, touchPoint)) {
            sliderRGeq17.value = [self pointToSliderValue:touchPoint];
            [self onSlider17Action:sliderRGeq17];
        } else if (CGRectContainsPoint(sliderRGeq18.frame, touchPoint)) {
            sliderRGeq18.value = [self pointToSliderValue:touchPoint];
            [self onSlider18Action:sliderRGeq18];
        } else if (CGRectContainsPoint(sliderRGeq19.frame, touchPoint)) {
            sliderRGeq19.value = [self pointToSliderValue:touchPoint];
            [self onSlider19Action:sliderRGeq19];
        } else if (CGRectContainsPoint(sliderRGeq20.frame, touchPoint)) {
            sliderRGeq20.value = [self pointToSliderValue:touchPoint];
            [self onSlider20Action:sliderRGeq20];
        } else if (CGRectContainsPoint(sliderRGeq21.frame, touchPoint)) {
            sliderRGeq21.value = [self pointToSliderValue:touchPoint];
            [self onSlider21Action:sliderRGeq21];
        } else if (CGRectContainsPoint(sliderRGeq22.frame, touchPoint)) {
            sliderRGeq22.value = [self pointToSliderValue:touchPoint];
            [self onSlider22Action:sliderRGeq22];
        } else if (CGRectContainsPoint(sliderRGeq23.frame, touchPoint)) {
            sliderRGeq23.value = [self pointToSliderValue:touchPoint];
            [self onSlider23Action:sliderRGeq23];
        } else if (CGRectContainsPoint(sliderRGeq24.frame, touchPoint)) {
            sliderRGeq24.value = [self pointToSliderValue:touchPoint];
            [self onSlider24Action:sliderRGeq24];
        } else if (CGRectContainsPoint(sliderRGeq25.frame, touchPoint)) {
            sliderRGeq25.value = [self pointToSliderValue:touchPoint];
            [self onSlider25Action:sliderRGeq25];
        } else if (CGRectContainsPoint(sliderRGeq26.frame, touchPoint)) {
            sliderRGeq26.value = [self pointToSliderValue:touchPoint];
            [self onSlider26Action:sliderRGeq26];
        } else if (CGRectContainsPoint(sliderRGeq27.frame, touchPoint)) {
            sliderRGeq27.value = [self pointToSliderValue:touchPoint];
            [self onSlider27Action:sliderRGeq27];
        } else if (CGRectContainsPoint(sliderRGeq28.frame, touchPoint)) {
            sliderRGeq28.value = [self pointToSliderValue:touchPoint];
            [self onSlider28Action:sliderRGeq28];
        } else if (CGRectContainsPoint(sliderRGeq29.frame, touchPoint)) {
            sliderRGeq29.value = [self pointToSliderValue:touchPoint];
            [self onSlider29Action:sliderRGeq29];
        } else if (CGRectContainsPoint(sliderRGeq30.frame, touchPoint)) {
            sliderRGeq30.value = [self pointToSliderValue:touchPoint];
            [self onSlider30Action:sliderRGeq30];
        } else if (CGRectContainsPoint(sliderRGeq31.frame, touchPoint)) {
            sliderRGeq31.value = [self pointToSliderValue:touchPoint];
            [self onSlider31Action:sliderRGeq31];
        }
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    /*
     UITouch *touch = [touches anyObject];
     if (!viewGeq.isHidden && btnGeqDraw.selected) {
     CGPoint endPoint = [touch locationInView:self.viewGeqDraw];
     NSLog(@"End point = (%.f, %.f)", endPoint.x, endPoint.y);
     }
     */
    geqLock = NO;
}

// Update image preview
- (void)updatePreviewImage
{
    if (!initDone) {
        NSLog(@"updatePreviewImage: Init not done!");
        return;
    }
    
    UIImage *finalImage;
    
    // GEQ L
    UIGraphicsBeginImageContext(CGSizeMake(780, 212));

    [viewGeqFrameL.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    finalImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    if (finalImage != nil) {
        imgEqL = finalImage;
    } else {
        NSLog(@"updatePreviewImage: NULL preview image!");
    }
    
    // GEQ R
    UIGraphicsBeginImageContext(CGSizeMake(780, 212));
    
    [viewGeqFrameR.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    finalImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    if (finalImage != nil) {
        imgEqR = finalImage;
    } else {
        NSLog(@"updatePreviewImage: NULL preview image!");
    }
    
    // Update delegate image
    [self.delegate didFinishEditingGeq31];
}

#pragma mark -
#pragma mark Main event handler methods

- (IBAction)onBtnFile:(id)sender {
    [viewController setFileSceneMode:SCENE_MODE_EFFECT_1 sceneChannel:0];
    [viewController.view bringSubviewToFront:viewController.viewScenes];
    [viewController.viewScenes setHidden:NO];
}

- (IBAction)onBtnBack:(id)sender {
    [self.view setHidden:YES];
}

- (IBAction)onBtnReset:(id)sender {
    geqLock = YES;
    
    sliderLGeq1.value = 0; sliderLGeq2.value = 0; sliderLGeq3.value = 0;
    sliderLGeq4.value = 0; sliderLGeq5.value = 0; sliderLGeq6.value = 0;
    sliderLGeq7.value = 0; sliderLGeq8.value = 0; sliderLGeq9.value = 0;
    sliderLGeq10.value = 0; sliderLGeq11.value = 0; sliderLGeq12.value = 0;
    sliderLGeq13.value = 0; sliderLGeq14.value = 0; sliderLGeq15.value = 0;
    sliderLGeq16.value = 0; sliderLGeq17.value = 0; sliderLGeq18.value = 0;
    sliderLGeq19.value = 0; sliderLGeq20.value = 0; sliderLGeq21.value = 0;
    sliderLGeq22.value = 0; sliderLGeq23.value = 0; sliderLGeq24.value = 0;
    sliderLGeq25.value = 0; sliderLGeq26.value = 0; sliderLGeq27.value = 0;
    sliderLGeq28.value = 0; sliderLGeq29.value = 0; sliderLGeq30.value = 0;
    sliderLGeq31.value = 0;
    
    [self onSlider1Action:sliderLGeq1];     [self onSlider2Action:sliderLGeq2];
    [self onSlider3Action:sliderLGeq3];     [self onSlider4Action:sliderLGeq4];
    [self onSlider5Action:sliderLGeq5];     [self onSlider6Action:sliderLGeq6];
    [self onSlider7Action:sliderLGeq7];     [self onSlider8Action:sliderLGeq8];
    [self onSlider9Action:sliderLGeq9];     [self onSlider10Action:sliderLGeq10];
    [self onSlider11Action:sliderLGeq11];     [self onSlider12Action:sliderLGeq12];
    [self onSlider13Action:sliderLGeq13];     [self onSlider14Action:sliderLGeq14];
    [self onSlider15Action:sliderLGeq15];     [self onSlider16Action:sliderLGeq16];
    [self onSlider17Action:sliderLGeq17];     [self onSlider18Action:sliderLGeq18];
    [self onSlider19Action:sliderLGeq19];     [self onSlider20Action:sliderLGeq20];
    [self onSlider21Action:sliderLGeq21];     [self onSlider22Action:sliderLGeq22];
    [self onSlider23Action:sliderLGeq23];     [self onSlider24Action:sliderLGeq24];
    [self onSlider25Action:sliderLGeq25];     [self onSlider26Action:sliderLGeq26];
    [self onSlider27Action:sliderLGeq27];     [self onSlider28Action:sliderLGeq28];
    [self onSlider29Action:sliderLGeq29];     [self onSlider30Action:sliderLGeq30];
    [self onSlider31Action:sliderLGeq31];
    
    sliderRGeq1.value = 0; sliderRGeq2.value = 0; sliderRGeq3.value = 0;
    sliderRGeq4.value = 0; sliderRGeq5.value = 0; sliderRGeq6.value = 0;
    sliderRGeq7.value = 0; sliderRGeq8.value = 0; sliderRGeq9.value = 0;
    sliderRGeq10.value = 0; sliderRGeq11.value = 0; sliderRGeq12.value = 0;
    sliderRGeq13.value = 0; sliderRGeq14.value = 0; sliderRGeq15.value = 0;
    sliderRGeq16.value = 0; sliderRGeq17.value = 0; sliderRGeq18.value = 0;
    sliderRGeq19.value = 0; sliderRGeq20.value = 0; sliderRGeq21.value = 0;
    sliderRGeq22.value = 0; sliderRGeq23.value = 0; sliderRGeq24.value = 0;
    sliderRGeq25.value = 0; sliderRGeq26.value = 0; sliderRGeq27.value = 0;
    sliderRGeq28.value = 0; sliderRGeq29.value = 0; sliderRGeq30.value = 0;
    sliderRGeq31.value = 0;
    
    [self onSlider1Action:sliderRGeq1];     [self onSlider2Action:sliderRGeq2];
    [self onSlider3Action:sliderRGeq3];     [self onSlider4Action:sliderRGeq4];
    [self onSlider5Action:sliderRGeq5];     [self onSlider6Action:sliderRGeq6];
    [self onSlider7Action:sliderRGeq7];     [self onSlider8Action:sliderRGeq8];
    [self onSlider9Action:sliderRGeq9];     [self onSlider10Action:sliderRGeq10];
    [self onSlider11Action:sliderRGeq11];     [self onSlider12Action:sliderRGeq12];
    [self onSlider13Action:sliderRGeq13];     [self onSlider14Action:sliderRGeq14];
    [self onSlider15Action:sliderRGeq15];     [self onSlider16Action:sliderRGeq16];
    [self onSlider17Action:sliderRGeq17];     [self onSlider18Action:sliderRGeq18];
    [self onSlider19Action:sliderRGeq19];     [self onSlider20Action:sliderRGeq20];
    [self onSlider21Action:sliderRGeq21];     [self onSlider22Action:sliderRGeq22];
    [self onSlider23Action:sliderRGeq23];     [self onSlider24Action:sliderRGeq24];
    [self onSlider25Action:sliderRGeq25];     [self onSlider26Action:sliderRGeq26];
    [self onSlider27Action:sliderRGeq27];     [self onSlider28Action:sliderRGeq28];
    [self onSlider29Action:sliderRGeq29];     [self onSlider30Action:sliderRGeq30];
    [self onSlider31Action:sliderRGeq31];

    [self updatePreviewImage];
    
    geqLock = NO;
}

- (IBAction)onBtnDraw:(id)sender {
    btnGeqDraw.selected = !btnGeqDraw.selected;
    if (btnGeqDraw.selected) {
        [self.view bringSubviewToFront:self.viewGeqDrawL];
        [self.view bringSubviewToFront:self.viewGeqDrawR];
    } else {
        [self.view sendSubviewToBack:self.viewGeqDrawL];
        [self.view sendSubviewToBack:self.viewGeqDrawR];
    }
}

#pragma mark -
#pragma mark GEQ event handler methods

- (IBAction)onBtnGeqLink:(id)sender {
    btnGeqLink.selected = !btnGeqLink.selected;
    
    // Sync right channel to left
    if (btnGeqLink.selected) {
        sliderRGeq1.value = sliderLGeq1.value;
        sliderRGeq2.value = sliderLGeq2.value;
        sliderRGeq3.value = sliderLGeq3.value;
        sliderRGeq4.value = sliderLGeq4.value;
        sliderRGeq5.value = sliderLGeq5.value;
        sliderRGeq6.value = sliderLGeq6.value;
        sliderRGeq7.value = sliderLGeq7.value;
        sliderRGeq8.value = sliderLGeq8.value;
        sliderRGeq9.value = sliderLGeq9.value;
        sliderRGeq10.value = sliderLGeq10.value;
        sliderRGeq11.value = sliderLGeq11.value;
        sliderRGeq12.value = sliderLGeq12.value;
        sliderRGeq13.value = sliderLGeq13.value;
        sliderRGeq14.value = sliderLGeq14.value;
        sliderRGeq15.value = sliderLGeq15.value;
        sliderRGeq16.value = sliderLGeq16.value;
        sliderRGeq17.value = sliderLGeq17.value;
        sliderRGeq18.value = sliderLGeq18.value;
        sliderRGeq19.value = sliderLGeq19.value;
        sliderRGeq20.value = sliderLGeq20.value;
        sliderRGeq21.value = sliderLGeq21.value;
        sliderRGeq22.value = sliderLGeq22.value;
        sliderRGeq23.value = sliderLGeq23.value;
        sliderRGeq24.value = sliderLGeq24.value;
        sliderRGeq25.value = sliderLGeq25.value;
        sliderRGeq26.value = sliderLGeq26.value;
        sliderRGeq27.value = sliderLGeq27.value;
        sliderRGeq28.value = sliderLGeq28.value;
        sliderRGeq29.value = sliderLGeq29.value;
        sliderRGeq30.value = sliderLGeq30.value;
        sliderRGeq31.value = sliderLGeq31.value;
        [self onSlider1Action:sliderRGeq1];
        [self onSlider2Action:sliderRGeq2];
        [self onSlider3Action:sliderRGeq3];
        [self onSlider4Action:sliderRGeq4];
        [self onSlider5Action:sliderRGeq5];
        [self onSlider6Action:sliderRGeq6];
        [self onSlider7Action:sliderRGeq7];
        [self onSlider8Action:sliderRGeq8];
        [self onSlider9Action:sliderRGeq9];
        [self onSlider10Action:sliderRGeq10];
        [self onSlider11Action:sliderRGeq11];
        [self onSlider12Action:sliderRGeq12];
        [self onSlider13Action:sliderRGeq13];
        [self onSlider14Action:sliderRGeq14];
        [self onSlider15Action:sliderRGeq15];
        [self onSlider16Action:sliderRGeq16];
        [self onSlider17Action:sliderRGeq17];
        [self onSlider18Action:sliderRGeq18];
        [self onSlider19Action:sliderRGeq19];
        [self onSlider20Action:sliderRGeq20];
        [self onSlider21Action:sliderRGeq21];
        [self onSlider22Action:sliderRGeq22];
        [self onSlider23Action:sliderRGeq23];
        [self onSlider24Action:sliderRGeq24];
        [self onSlider25Action:sliderRGeq25];
        [self onSlider26Action:sliderRGeq26];
        [self onSlider27Action:sliderRGeq27];
        [self onSlider28Action:sliderRGeq28];
        [self onSlider29Action:sliderRGeq29];
        [self onSlider30Action:sliderRGeq30];
        [self onSlider31Action:sliderRGeq31];
    }
    
    geqLinkMode = btnGeqLink.selected;
    [viewController sendData2:SET_GEQ_LINK_MODE value1:0 value2:(int)btnGeqLink.selected];
}

- (void)updateGeqMeters {
    // Update meterL images
    UIImage *progressImage;
    if (geqOnOff) {
        progressImage = [[UIImage imageNamed:@"bar-2.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0,0,0,0)];
    } else {
        progressImage = [[UIImage imageNamed:@"bar-4.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0,0,0,0)];
    }
    
    [meterL1Top setProgressImage:progressImage];
    [meterL1Btm setProgressImage:progressImage];
    [meterL2Top setProgressImage:progressImage];
    [meterL2Btm setProgressImage:progressImage];
    [meterL3Top setProgressImage:progressImage];
    [meterL3Btm setProgressImage:progressImage];
    [meterL4Top setProgressImage:progressImage];
    [meterL4Btm setProgressImage:progressImage];
    [meterL5Top setProgressImage:progressImage];
    [meterL5Btm setProgressImage:progressImage];
    [meterL6Top setProgressImage:progressImage];
    [meterL6Btm setProgressImage:progressImage];
    [meterL7Top setProgressImage:progressImage];
    [meterL7Btm setProgressImage:progressImage];
    [meterL8Top setProgressImage:progressImage];
    [meterL8Btm setProgressImage:progressImage];
    [meterL9Top setProgressImage:progressImage];
    [meterL9Btm setProgressImage:progressImage];
    [meterL10Top setProgressImage:progressImage];
    [meterL10Btm setProgressImage:progressImage];
    [meterL11Top setProgressImage:progressImage];
    [meterL11Btm setProgressImage:progressImage];
    [meterL12Top setProgressImage:progressImage];
    [meterL12Btm setProgressImage:progressImage];
    [meterL13Top setProgressImage:progressImage];
    [meterL13Btm setProgressImage:progressImage];
    [meterL14Top setProgressImage:progressImage];
    [meterL14Btm setProgressImage:progressImage];
    [meterL15Top setProgressImage:progressImage];
    [meterL15Btm setProgressImage:progressImage];
    [meterL16Top setProgressImage:progressImage];
    [meterL16Btm setProgressImage:progressImage];
    [meterL17Top setProgressImage:progressImage];
    [meterL17Btm setProgressImage:progressImage];
    [meterL18Top setProgressImage:progressImage];
    [meterL18Btm setProgressImage:progressImage];
    [meterL19Top setProgressImage:progressImage];
    [meterL19Btm setProgressImage:progressImage];
    [meterL20Top setProgressImage:progressImage];
    [meterL20Btm setProgressImage:progressImage];
    [meterL21Top setProgressImage:progressImage];
    [meterL21Btm setProgressImage:progressImage];
    [meterL22Top setProgressImage:progressImage];
    [meterL22Btm setProgressImage:progressImage];
    [meterL23Top setProgressImage:progressImage];
    [meterL23Btm setProgressImage:progressImage];
    [meterL24Top setProgressImage:progressImage];
    [meterL24Btm setProgressImage:progressImage];
    [meterL25Top setProgressImage:progressImage];
    [meterL25Btm setProgressImage:progressImage];
    [meterL26Top setProgressImage:progressImage];
    [meterL26Btm setProgressImage:progressImage];
    [meterL27Top setProgressImage:progressImage];
    [meterL27Btm setProgressImage:progressImage];
    [meterL28Top setProgressImage:progressImage];
    [meterL28Btm setProgressImage:progressImage];
    [meterL29Top setProgressImage:progressImage];
    [meterL29Btm setProgressImage:progressImage];
    [meterL30Top setProgressImage:progressImage];
    [meterL30Btm setProgressImage:progressImage];
    [meterL31Top setProgressImage:progressImage];
    [meterL31Btm setProgressImage:progressImage];
    
    [meterR1Top setProgressImage:progressImage];
    [meterR1Btm setProgressImage:progressImage];
    [meterR2Top setProgressImage:progressImage];
    [meterR2Btm setProgressImage:progressImage];
    [meterR3Top setProgressImage:progressImage];
    [meterR3Btm setProgressImage:progressImage];
    [meterR4Top setProgressImage:progressImage];
    [meterR4Btm setProgressImage:progressImage];
    [meterR5Top setProgressImage:progressImage];
    [meterR5Btm setProgressImage:progressImage];
    [meterR6Top setProgressImage:progressImage];
    [meterR6Btm setProgressImage:progressImage];
    [meterR7Top setProgressImage:progressImage];
    [meterR7Btm setProgressImage:progressImage];
    [meterR8Top setProgressImage:progressImage];
    [meterR8Btm setProgressImage:progressImage];
    [meterR9Top setProgressImage:progressImage];
    [meterR9Btm setProgressImage:progressImage];
    [meterR10Top setProgressImage:progressImage];
    [meterR10Btm setProgressImage:progressImage];
    [meterR11Top setProgressImage:progressImage];
    [meterR11Btm setProgressImage:progressImage];
    [meterR12Top setProgressImage:progressImage];
    [meterR12Btm setProgressImage:progressImage];
    [meterR13Top setProgressImage:progressImage];
    [meterR13Btm setProgressImage:progressImage];
    [meterR14Top setProgressImage:progressImage];
    [meterR14Btm setProgressImage:progressImage];
    [meterR15Top setProgressImage:progressImage];
    [meterR15Btm setProgressImage:progressImage];
    [meterR16Top setProgressImage:progressImage];
    [meterR16Btm setProgressImage:progressImage];
    [meterR17Top setProgressImage:progressImage];
    [meterR17Btm setProgressImage:progressImage];
    [meterR18Top setProgressImage:progressImage];
    [meterR18Btm setProgressImage:progressImage];
    [meterR19Top setProgressImage:progressImage];
    [meterR19Btm setProgressImage:progressImage];
    [meterR20Top setProgressImage:progressImage];
    [meterR20Btm setProgressImage:progressImage];
    [meterR21Top setProgressImage:progressImage];
    [meterR21Btm setProgressImage:progressImage];
    [meterR22Top setProgressImage:progressImage];
    [meterR22Btm setProgressImage:progressImage];
    [meterR23Top setProgressImage:progressImage];
    [meterR23Btm setProgressImage:progressImage];
    [meterR24Top setProgressImage:progressImage];
    [meterR24Btm setProgressImage:progressImage];
    [meterR25Top setProgressImage:progressImage];
    [meterR25Btm setProgressImage:progressImage];
    [meterR26Top setProgressImage:progressImage];
    [meterR26Btm setProgressImage:progressImage];
    [meterR27Top setProgressImage:progressImage];
    [meterR27Btm setProgressImage:progressImage];
    [meterR28Top setProgressImage:progressImage];
    [meterR28Btm setProgressImage:progressImage];
    [meterR29Top setProgressImage:progressImage];
    [meterR29Btm setProgressImage:progressImage];
    [meterR30Top setProgressImage:progressImage];
    [meterR30Btm setProgressImage:progressImage];
    [meterR31Top setProgressImage:progressImage];
    [meterR31Btm setProgressImage:progressImage];
    
    [self updatePreviewImage];
}

#pragma mark -
#pragma mark Slider event handler methods

- (IBAction)onSlider1Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider1End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider1Action:(id) sender {
    if (sender == sliderLGeq1) {
        if (sliderLGeq1.value > 0.0) {
            meterL1Top.progress = sliderLGeq1.value/GEQ_SLIDER_MAX_VALUE;
            meterL1Btm.progress = 0.0;
        } else if (sliderLGeq1.value < 0.0) {
            meterL1Btm.progress = sliderLGeq1.value/GEQ_SLIDER_MIN_VALUE;
            meterL1Top.progress = 0.0;
        } else {
            meterL1Top.progress = 0.0;
            meterL1Btm.progress = 0.0;
        }
        [viewController sendData:CMD_L_GEQ_20HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderLGeq1.value)];

        // Handle link mode
        if (geqLinkMode) {
            sliderRGeq1.value = sliderLGeq1.value;
            if (sliderRGeq1.value > 0.0) {
                meterR1Top.progress = sliderRGeq1.value/GEQ_SLIDER_MAX_VALUE;
                meterR1Btm.progress = 0.0;
            } else if (sliderRGeq1.value < 0.0) {
                meterR1Btm.progress = sliderRGeq1.value/GEQ_SLIDER_MIN_VALUE;
                meterR1Top.progress = 0.0;
            } else {
                meterR1Top.progress = 0.0;
                meterR1Btm.progress = 0.0;
            }
            [viewController sendData:CMD_R_GEQ_20HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderRGeq1.value)];
        }
        [self updatePreviewImage];
    } else if (sender == sliderRGeq1) {
        if (sliderRGeq1.value > 0.0) {
            meterR1Top.progress = sliderRGeq1.value/GEQ_SLIDER_MAX_VALUE;
            meterR1Btm.progress = 0.0;
        } else if (sliderRGeq1.value < 0.0) {
            meterR1Btm.progress = sliderRGeq1.value/GEQ_SLIDER_MIN_VALUE;
            meterR1Top.progress = 0.0;
        } else {
            meterR1Top.progress = 0.0;
            meterR1Btm.progress = 0.0;
        }
        [viewController sendData:CMD_R_GEQ_20HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderRGeq1.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderLGeq1.value = sliderRGeq1.value;
            if (sliderLGeq1.value > 0.0) {
                meterL1Top.progress = sliderLGeq1.value/GEQ_SLIDER_MAX_VALUE;
                meterL1Btm.progress = 0.0;
            } else if (sliderLGeq1.value < 0.0) {
                meterL1Btm.progress = sliderLGeq1.value/GEQ_SLIDER_MIN_VALUE;
                meterL1Top.progress = 0.0;
            } else {
                meterL1Top.progress = 0.0;
                meterL1Btm.progress = 0.0;
            }
            [viewController sendData:CMD_L_GEQ_20HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderLGeq1.value)];
        }
        [self updatePreviewImage];
    }
}

- (IBAction)onSlider2Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider2End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider2Action:(id) sender {
    if (sender == sliderLGeq2) {
        if (sliderLGeq2.value > 0.0) {
            meterL2Top.progress = sliderLGeq2.value/GEQ_SLIDER_MAX_VALUE;
            meterL2Btm.progress = 0.0;
        } else if (sliderLGeq2.value < 0.0) {
            meterL2Btm.progress = sliderLGeq2.value/GEQ_SLIDER_MIN_VALUE;
            meterL2Top.progress = 0.0;
        } else {
            meterL2Top.progress = 0.0;
            meterL2Btm.progress = 0.0;
        }
        [viewController sendData:CMD_L_GEQ_25HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderLGeq2.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderRGeq2.value = sliderLGeq2.value;
            if (sliderRGeq2.value > 0.0) {
                meterR2Top.progress = sliderRGeq2.value/GEQ_SLIDER_MAX_VALUE;
                meterR2Btm.progress = 0.0;
            } else if (sliderRGeq2.value < 0.0) {
                meterR2Btm.progress = sliderRGeq2.value/GEQ_SLIDER_MIN_VALUE;
                meterR2Top.progress = 0.0;
            } else {
                meterR2Top.progress = 0.0;
                meterR2Btm.progress = 0.0;
            }
            [viewController sendData:CMD_R_GEQ_25HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderRGeq2.value)];
        }
        [self updatePreviewImage];
    } else if (sender == sliderRGeq2) {
        if (sliderRGeq2.value > 0.0) {
            meterR2Top.progress = sliderRGeq2.value/GEQ_SLIDER_MAX_VALUE;
            meterR2Btm.progress = 0.0;
        } else if (sliderRGeq2.value < 0.0) {
            meterR2Btm.progress = sliderRGeq2.value/GEQ_SLIDER_MIN_VALUE;
            meterR2Top.progress = 0.0;
        } else {
            meterR2Top.progress = 0.0;
            meterR2Btm.progress = 0.0;
        }
        [viewController sendData:CMD_R_GEQ_25HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderRGeq2.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderLGeq2.value = sliderRGeq2.value;
            if (sliderLGeq2.value > 0.0) {
                meterL2Top.progress = sliderLGeq2.value/GEQ_SLIDER_MAX_VALUE;
                meterL2Btm.progress = 0.0;
            } else if (sliderLGeq2.value < 0.0) {
                meterL2Btm.progress = sliderLGeq2.value/GEQ_SLIDER_MIN_VALUE;
                meterL2Top.progress = 0.0;
            } else {
                meterL2Top.progress = 0.0;
                meterL2Btm.progress = 0.0;
            }
            [viewController sendData:CMD_L_GEQ_25HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderLGeq2.value)];
        }
        [self updatePreviewImage];
    }
}

- (IBAction)onSlider3Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider3End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider3Action:(id) sender {
    if (sender == sliderLGeq3) {
        if (sliderLGeq3.value > 0.0) {
            meterL3Top.progress = sliderLGeq3.value/GEQ_SLIDER_MAX_VALUE;
            meterL3Btm.progress = 0.0;
        } else if (sliderLGeq3.value < 0.0) {
            meterL3Btm.progress = sliderLGeq3.value/GEQ_SLIDER_MIN_VALUE;
            meterL3Top.progress = 0.0;
        } else {
            meterL3Top.progress = 0.0;
            meterL3Btm.progress = 0.0;
        }
        [viewController sendData:CMD_L_GEQ_31_5HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderLGeq3.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderRGeq3.value = sliderLGeq3.value;
            if (sliderRGeq3.value > 0.0) {
                meterR3Top.progress = sliderRGeq3.value/GEQ_SLIDER_MAX_VALUE;
                meterR3Btm.progress = 0.0;
            } else if (sliderRGeq3.value < 0.0) {
                meterR3Btm.progress = sliderRGeq3.value/GEQ_SLIDER_MIN_VALUE;
                meterR3Top.progress = 0.0;
            } else {
                meterR3Top.progress = 0.0;
                meterR3Btm.progress = 0.0;
            }
            [viewController sendData:CMD_R_GEQ_31_5HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderRGeq3.value)];
        }
        [self updatePreviewImage];
    } else if (sender == sliderRGeq3) {
        if (sliderRGeq3.value > 0.0) {
            meterR3Top.progress = sliderRGeq3.value/GEQ_SLIDER_MAX_VALUE;
            meterR3Btm.progress = 0.0;
        } else if (sliderRGeq3.value < 0.0) {
            meterR3Btm.progress = sliderRGeq3.value/GEQ_SLIDER_MIN_VALUE;
            meterR3Top.progress = 0.0;
        } else {
            meterR3Top.progress = 0.0;
            meterR3Btm.progress = 0.0;
        }
        [viewController sendData:CMD_R_GEQ_31_5HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderRGeq3.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderLGeq3.value = sliderRGeq3.value;
            if (sliderLGeq3.value > 0.0) {
                meterL3Top.progress = sliderLGeq3.value/GEQ_SLIDER_MAX_VALUE;
                meterL3Btm.progress = 0.0;
            } else if (sliderLGeq3.value < 0.0) {
                meterL3Btm.progress = sliderLGeq3.value/GEQ_SLIDER_MIN_VALUE;
                meterL3Top.progress = 0.0;
            } else {
                meterL3Top.progress = 0.0;
                meterL3Btm.progress = 0.0;
            }
            [viewController sendData:CMD_L_GEQ_31_5HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderLGeq3.value)];
        }
        [self updatePreviewImage];
    }
}

- (IBAction)onSlider4Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider4End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider4Action:(id) sender {
    if (sender == sliderLGeq4) {
        if (sliderLGeq4.value > 0.0) {
            meterL4Top.progress = sliderLGeq4.value/GEQ_SLIDER_MAX_VALUE;
            meterL4Btm.progress = 0.0;
        } else if (sliderLGeq4.value < 0.0) {
            meterL4Btm.progress = sliderLGeq4.value/GEQ_SLIDER_MIN_VALUE;
            meterL4Top.progress = 0.0;
        } else {
            meterL4Top.progress = 0.0;
            meterL4Btm.progress = 0.0;
        }
        [viewController sendData:CMD_L_GEQ_40HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderLGeq4.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderRGeq4.value = sliderLGeq4.value;
            if (sliderRGeq4.value > 0.0) {
                meterR4Top.progress = sliderRGeq4.value/GEQ_SLIDER_MAX_VALUE;
                meterR4Btm.progress = 0.0;
            } else if (sliderRGeq4.value < 0.0) {
                meterR4Btm.progress = sliderRGeq4.value/GEQ_SLIDER_MIN_VALUE;
                meterR4Top.progress = 0.0;
            } else {
                meterR4Top.progress = 0.0;
                meterR4Btm.progress = 0.0;
            }
            [viewController sendData:CMD_R_GEQ_40HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderRGeq4.value)];
        }
        [self updatePreviewImage];
    } else if (sender == sliderRGeq4) {
        if (sliderRGeq4.value > 0.0) {
            meterR4Top.progress = sliderRGeq4.value/GEQ_SLIDER_MAX_VALUE;
            meterR4Btm.progress = 0.0;
        } else if (sliderRGeq4.value < 0.0) {
            meterR4Btm.progress = sliderRGeq4.value/GEQ_SLIDER_MIN_VALUE;
            meterR4Top.progress = 0.0;
        } else {
            meterR4Top.progress = 0.0;
            meterR4Btm.progress = 0.0;
        }
        [viewController sendData:CMD_R_GEQ_40HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderRGeq4.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderLGeq4.value = sliderRGeq4.value;
            if (sliderLGeq4.value > 0.0) {
                meterL4Top.progress = sliderLGeq4.value/GEQ_SLIDER_MAX_VALUE;
                meterL4Btm.progress = 0.0;
            } else if (sliderLGeq4.value < 0.0) {
                meterL4Btm.progress = sliderLGeq4.value/GEQ_SLIDER_MIN_VALUE;
                meterL4Top.progress = 0.0;
            } else {
                meterL4Top.progress = 0.0;
                meterL4Btm.progress = 0.0;
            }
            [viewController sendData:CMD_L_GEQ_40HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderLGeq4.value)];
        }
        [self updatePreviewImage];
    }
}

- (IBAction)onSlider5Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider5End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider5Action:(id) sender {
    if (sender == sliderLGeq5) {
        if (sliderLGeq5.value > 0.0) {
            meterL5Top.progress = sliderLGeq5.value/GEQ_SLIDER_MAX_VALUE;
            meterL5Btm.progress = 0.0;
        } else if (sliderLGeq5.value < 0.0) {
            meterL5Btm.progress = sliderLGeq5.value/GEQ_SLIDER_MIN_VALUE;
            meterL5Top.progress = 0.0;
        } else {
            meterL5Top.progress = 0.0;
            meterL5Btm.progress = 0.0;
        }
        [viewController sendData:CMD_L_GEQ_50HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderLGeq5.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderRGeq5.value = sliderLGeq5.value;
            if (sliderRGeq5.value > 0.0) {
                meterR5Top.progress = sliderRGeq5.value/GEQ_SLIDER_MAX_VALUE;
                meterR5Btm.progress = 0.0;
            } else if (sliderRGeq5.value < 0.0) {
                meterR5Btm.progress = sliderRGeq5.value/GEQ_SLIDER_MIN_VALUE;
                meterR5Top.progress = 0.0;
            } else {
                meterR5Top.progress = 0.0;
                meterR5Btm.progress = 0.0;
            }
            [viewController sendData:CMD_R_GEQ_50HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderRGeq5.value)];
        }
        [self updatePreviewImage];
    } else if (sender == sliderRGeq5) {
        if (sliderRGeq5.value > 0.0) {
            meterR5Top.progress = sliderRGeq5.value/GEQ_SLIDER_MAX_VALUE;
            meterR5Btm.progress = 0.0;
        } else if (sliderRGeq5.value < 0.0) {
            meterR5Btm.progress = sliderRGeq5.value/GEQ_SLIDER_MIN_VALUE;
            meterR5Top.progress = 0.0;
        } else {
            meterR5Top.progress = 0.0;
            meterR5Btm.progress = 0.0;
        }
        [viewController sendData:CMD_R_GEQ_50HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderRGeq5.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderLGeq5.value = sliderRGeq5.value;
            if (sliderLGeq5.value > 0.0) {
                meterL5Top.progress = sliderLGeq5.value/GEQ_SLIDER_MAX_VALUE;
                meterL5Btm.progress = 0.0;
            } else if (sliderLGeq5.value < 0.0) {
                meterL5Btm.progress = sliderLGeq5.value/GEQ_SLIDER_MIN_VALUE;
                meterL5Top.progress = 0.0;
            } else {
                meterL5Top.progress = 0.0;
                meterL5Btm.progress = 0.0;
            }
            [viewController sendData:CMD_L_GEQ_50HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderLGeq5.value)];
        }
        [self updatePreviewImage];
    }
}

- (IBAction)onSlider6Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider6End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider6Action:(id) sender {
    if (sender == sliderLGeq6) {
        if (sliderLGeq6.value > 0.0) {
            meterL6Top.progress = sliderLGeq6.value/GEQ_SLIDER_MAX_VALUE;
            meterL6Btm.progress = 0.0;
        } else if (sliderLGeq6.value < 0.0) {
            meterL6Btm.progress = sliderLGeq6.value/GEQ_SLIDER_MIN_VALUE;
            meterL6Top.progress = 0.0;
        } else {
            meterL6Top.progress = 0.0;
            meterL6Btm.progress = 0.0;
        }
        [viewController sendData:CMD_L_GEQ_63HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderLGeq6.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderRGeq6.value = sliderLGeq6.value;
            if (sliderRGeq6.value > 0.0) {
                meterR6Top.progress = sliderRGeq6.value/GEQ_SLIDER_MAX_VALUE;
                meterR6Btm.progress = 0.0;
            } else if (sliderRGeq6.value < 0.0) {
                meterR6Btm.progress = sliderRGeq6.value/GEQ_SLIDER_MIN_VALUE;
                meterR6Top.progress = 0.0;
            } else {
                meterR6Top.progress = 0.0;
                meterR6Btm.progress = 0.0;
            }
            [viewController sendData:CMD_R_GEQ_63HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderRGeq6.value)];
        }
        [self updatePreviewImage];
    } else if (sender == sliderRGeq6) {
        if (sliderRGeq6.value > 0.0) {
            meterR6Top.progress = sliderRGeq6.value/GEQ_SLIDER_MAX_VALUE;
            meterR6Btm.progress = 0.0;
        } else if (sliderRGeq6.value < 0.0) {
            meterR6Btm.progress = sliderRGeq6.value/GEQ_SLIDER_MIN_VALUE;
            meterR6Top.progress = 0.0;
        } else {
            meterR6Top.progress = 0.0;
            meterR6Btm.progress = 0.0;
        }
        [viewController sendData:CMD_R_GEQ_63HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderRGeq6.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderLGeq6.value = sliderRGeq6.value;
            if (sliderLGeq6.value > 0.0) {
                meterL6Top.progress = sliderLGeq6.value/GEQ_SLIDER_MAX_VALUE;
                meterL6Btm.progress = 0.0;
            } else if (sliderLGeq6.value < 0.0) {
                meterL6Btm.progress = sliderLGeq6.value/GEQ_SLIDER_MIN_VALUE;
                meterL6Top.progress = 0.0;
            } else {
                meterL6Top.progress = 0.0;
                meterL6Btm.progress = 0.0;
            }
            [viewController sendData:CMD_L_GEQ_63HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderLGeq6.value)];
        }
        [self updatePreviewImage];
    }
}

- (IBAction)onSlider7Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider7End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider7Action:(id) sender {
    if (sender == sliderLGeq7) {
        if (sliderLGeq7.value > 0.0) {
            meterL7Top.progress = sliderLGeq7.value/GEQ_SLIDER_MAX_VALUE;
            meterL7Btm.progress = 0.0;
        } else if (sliderLGeq7.value < 0.0) {
            meterL7Btm.progress = sliderLGeq7.value/GEQ_SLIDER_MIN_VALUE;
            meterL7Top.progress = 0.0;
        } else {
            meterL7Top.progress = 0.0;
            meterL7Btm.progress = 0.0;
        }
        [viewController sendData:CMD_L_GEQ_80HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderLGeq7.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderRGeq7.value = sliderLGeq7.value;
            if (sliderRGeq7.value > 0.0) {
                meterR7Top.progress = sliderRGeq7.value/GEQ_SLIDER_MAX_VALUE;
                meterR7Btm.progress = 0.0;
            } else if (sliderRGeq7.value < 0.0) {
                meterR7Btm.progress = sliderRGeq7.value/GEQ_SLIDER_MIN_VALUE;
                meterR7Top.progress = 0.0;
            } else {
                meterR7Top.progress = 0.0;
                meterR7Btm.progress = 0.0;
            }
            [viewController sendData:CMD_R_GEQ_80HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderRGeq7.value)];
        }
        [self updatePreviewImage];

    } else if (sender == sliderRGeq7) {
        if (sliderRGeq7.value > 0.0) {
            meterR7Top.progress = sliderRGeq7.value/GEQ_SLIDER_MAX_VALUE;
            meterR7Btm.progress = 0.0;
        } else if (sliderRGeq7.value < 0.0) {
            meterR7Btm.progress = sliderRGeq7.value/GEQ_SLIDER_MIN_VALUE;
            meterR7Top.progress = 0.0;
        } else {
            meterR7Top.progress = 0.0;
            meterR7Btm.progress = 0.0;
        }
        [viewController sendData:CMD_R_GEQ_80HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderRGeq7.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderLGeq7.value = sliderRGeq7.value;
            if (sliderLGeq7.value > 0.0) {
                meterL7Top.progress = sliderLGeq7.value/GEQ_SLIDER_MAX_VALUE;
                meterL7Btm.progress = 0.0;
            } else if (sliderLGeq7.value < 0.0) {
                meterL7Btm.progress = sliderLGeq7.value/GEQ_SLIDER_MIN_VALUE;
                meterL7Top.progress = 0.0;
            } else {
                meterL7Top.progress = 0.0;
                meterL7Btm.progress = 0.0;
            }
            [viewController sendData:CMD_L_GEQ_80HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderLGeq7.value)];
        }
        [self updatePreviewImage];
    }
}

- (IBAction)onSlider8Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider8End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider8Action:(id) sender {
    if (sender == sliderLGeq8) {
        if (sliderLGeq8.value > 0.0) {
            meterL8Top.progress = sliderLGeq8.value/GEQ_SLIDER_MAX_VALUE;
            meterL8Btm.progress = 0.0;
        } else if (sliderLGeq8.value < 0.0) {
            meterL8Btm.progress = sliderLGeq8.value/GEQ_SLIDER_MIN_VALUE;
            meterL8Top.progress = 0.0;
        } else {
            meterL8Top.progress = 0.0;
            meterL8Btm.progress = 0.0;
        }
        [viewController sendData:CMD_L_GEQ_100HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderLGeq8.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderRGeq8.value = sliderLGeq8.value;
            if (sliderRGeq8.value > 0.0) {
                meterR8Top.progress = sliderRGeq8.value/GEQ_SLIDER_MAX_VALUE;
                meterR8Btm.progress = 0.0;
            } else if (sliderRGeq8.value < 0.0) {
                meterR8Btm.progress = sliderRGeq8.value/GEQ_SLIDER_MIN_VALUE;
                meterR8Top.progress = 0.0;
            } else {
                meterR8Top.progress = 0.0;
                meterR8Btm.progress = 0.0;
            }
            [viewController sendData:CMD_R_GEQ_100HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderRGeq8.value)];
        }
        [self updatePreviewImage];
    } else if (sender == sliderRGeq8) {
        if (sliderRGeq8.value > 0.0) {
            meterR8Top.progress = sliderRGeq8.value/GEQ_SLIDER_MAX_VALUE;
            meterR8Btm.progress = 0.0;
        } else if (sliderRGeq8.value < 0.0) {
            meterR8Btm.progress = sliderRGeq8.value/GEQ_SLIDER_MIN_VALUE;
            meterR8Top.progress = 0.0;
        } else {
            meterR8Top.progress = 0.0;
            meterR8Btm.progress = 0.0;
        }
        [viewController sendData:CMD_R_GEQ_100HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderRGeq8.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderLGeq8.value = sliderRGeq8.value;
            if (sliderLGeq8.value > 0.0) {
                meterL8Top.progress = sliderLGeq8.value/GEQ_SLIDER_MAX_VALUE;
                meterL8Btm.progress = 0.0;
            } else if (sliderLGeq8.value < 0.0) {
                meterL8Btm.progress = sliderLGeq8.value/GEQ_SLIDER_MIN_VALUE;
                meterL8Top.progress = 0.0;
            } else {
                meterL8Top.progress = 0.0;
                meterL8Btm.progress = 0.0;
            }
            [viewController sendData:CMD_L_GEQ_100HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderLGeq8.value)];
        }
        [self updatePreviewImage];
    }
}

- (IBAction)onSlider9Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider9End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider9Action:(id) sender {
    if (sender == sliderLGeq9) {
        if (sliderLGeq9.value > 0.0) {
            meterL9Top.progress = sliderLGeq9.value/GEQ_SLIDER_MAX_VALUE;
            meterL9Btm.progress = 0.0;
        } else if (sliderLGeq9.value < 0.0) {
            meterL9Btm.progress = sliderLGeq9.value/GEQ_SLIDER_MIN_VALUE;
            meterL9Top.progress = 0.0;
        } else {
            meterL9Top.progress = 0.0;
            meterL9Btm.progress = 0.0;
        }
        [viewController sendData:CMD_L_GEQ_125HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderLGeq9.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderRGeq9.value = sliderLGeq9.value;
            if (sliderRGeq9.value > 0.0) {
                meterR9Top.progress = sliderRGeq9.value/GEQ_SLIDER_MAX_VALUE;
                meterR9Btm.progress = 0.0;
            } else if (sliderRGeq9.value < 0.0) {
                meterR9Btm.progress = sliderRGeq9.value/GEQ_SLIDER_MIN_VALUE;
                meterR9Top.progress = 0.0;
            } else {
                meterR9Top.progress = 0.0;
                meterR9Btm.progress = 0.0;
            }
            [viewController sendData:CMD_R_GEQ_125HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderRGeq9.value)];
        }
        [self updatePreviewImage];
    } else if (sender == sliderRGeq9) {
        if (sliderRGeq9.value > 0.0) {
            meterR9Top.progress = sliderRGeq9.value/GEQ_SLIDER_MAX_VALUE;
            meterR9Btm.progress = 0.0;
        } else if (sliderRGeq9.value < 0.0) {
            meterR9Btm.progress = sliderRGeq9.value/GEQ_SLIDER_MIN_VALUE;
            meterR9Top.progress = 0.0;
        } else {
            meterR9Top.progress = 0.0;
            meterR9Btm.progress = 0.0;
        }
        [viewController sendData:CMD_R_GEQ_125HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderRGeq9.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderLGeq9.value = sliderRGeq9.value;
            if (sliderLGeq9.value > 0.0) {
                meterL9Top.progress = sliderLGeq9.value/GEQ_SLIDER_MAX_VALUE;
                meterL9Btm.progress = 0.0;
            } else if (sliderLGeq9.value < 0.0) {
                meterL9Btm.progress = sliderLGeq9.value/GEQ_SLIDER_MIN_VALUE;
                meterL9Top.progress = 0.0;
            } else {
                meterL9Top.progress = 0.0;
                meterL9Btm.progress = 0.0;
            }
            [viewController sendData:CMD_L_GEQ_125HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderLGeq9.value)];
        }
        [self updatePreviewImage];
    }
}

- (IBAction)onSlider10Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider10End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider10Action:(id) sender {
    if (sender == sliderLGeq10) {
        if (sliderLGeq10.value > 0.0) {
            meterL10Top.progress = sliderLGeq10.value/GEQ_SLIDER_MAX_VALUE;
            meterL10Btm.progress = 0.0;
        } else if (sliderLGeq10.value < 0.0) {
            meterL10Btm.progress = sliderLGeq10.value/GEQ_SLIDER_MIN_VALUE;
            meterL10Top.progress = 0.0;
        } else {
            meterL10Top.progress = 0.0;
            meterL10Btm.progress = 0.0;
        }
        [viewController sendData:CMD_L_GEQ_160HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderLGeq10.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderRGeq10.value = sliderLGeq10.value;
            if (sliderRGeq10.value > 0.0) {
                meterR10Top.progress = sliderRGeq10.value/GEQ_SLIDER_MAX_VALUE;
                meterR10Btm.progress = 0.0;
            } else if (sliderRGeq10.value < 0.0) {
                meterR10Btm.progress = sliderRGeq10.value/GEQ_SLIDER_MIN_VALUE;
                meterR10Top.progress = 0.0;
            } else {
                meterR10Top.progress = 0.0;
                meterR10Btm.progress = 0.0;
            }
            [viewController sendData:CMD_R_GEQ_160HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderRGeq10.value)];
        }
        [self updatePreviewImage];
    } else if (sender == sliderRGeq10) {
        if (sliderRGeq10.value > 0.0) {
            meterR10Top.progress = sliderRGeq10.value/GEQ_SLIDER_MAX_VALUE;
            meterR10Btm.progress = 0.0;
        } else if (sliderRGeq10.value < 0.0) {
            meterR10Btm.progress = sliderRGeq10.value/GEQ_SLIDER_MIN_VALUE;
            meterR10Top.progress = 0.0;
        } else {
            meterR10Top.progress = 0.0;
            meterR10Btm.progress = 0.0;
        }
        [viewController sendData:CMD_R_GEQ_160HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderRGeq10.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderLGeq10.value = sliderRGeq10.value;
            if (sliderLGeq10.value > 0.0) {
                meterL10Top.progress = sliderLGeq10.value/GEQ_SLIDER_MAX_VALUE;
                meterL10Btm.progress = 0.0;
            } else if (sliderLGeq10.value < 0.0) {
                meterL10Btm.progress = sliderLGeq10.value/GEQ_SLIDER_MIN_VALUE;
                meterL10Top.progress = 0.0;
            } else {
                meterL10Top.progress = 0.0;
                meterL10Btm.progress = 0.0;
            }
            [viewController sendData:CMD_L_GEQ_160HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderLGeq10.value)];
        }
        [self updatePreviewImage];
    }
}

- (IBAction)onSlider11Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider11End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider11Action:(id) sender {
    if (sender == sliderLGeq11) {
        if (sliderLGeq11.value > 0.0) {
            meterL11Top.progress = sliderLGeq11.value/GEQ_SLIDER_MAX_VALUE;
            meterL11Btm.progress = 0.0;
        } else if (sliderLGeq11.value < 0.0) {
            meterL11Btm.progress = sliderLGeq11.value/GEQ_SLIDER_MIN_VALUE;
            meterL11Top.progress = 0.0;
        } else {
            meterL11Top.progress = 0.0;
            meterL11Btm.progress = 0.0;
        }
        [viewController sendData:CMD_L_GEQ_200HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderLGeq11.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderRGeq11.value = sliderLGeq11.value;
            if (sliderRGeq11.value > 0.0) {
                meterR11Top.progress = sliderRGeq11.value/GEQ_SLIDER_MAX_VALUE;
                meterR11Btm.progress = 0.0;
            } else if (sliderRGeq11.value < 0.0) {
                meterR11Btm.progress = sliderRGeq11.value/GEQ_SLIDER_MIN_VALUE;
                meterR11Top.progress = 0.0;
            } else {
                meterR11Top.progress = 0.0;
                meterR11Btm.progress = 0.0;
            }
            [viewController sendData:CMD_R_GEQ_200HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderRGeq11.value)];
        }
        [self updatePreviewImage];
    } else if (sender == sliderRGeq11) {
        if (sliderRGeq11.value > 0.0) {
            meterR11Top.progress = sliderRGeq11.value/GEQ_SLIDER_MAX_VALUE;
            meterR11Btm.progress = 0.0;
        } else if (sliderRGeq11.value < 0.0) {
            meterR11Btm.progress = sliderRGeq11.value/GEQ_SLIDER_MIN_VALUE;
            meterR11Top.progress = 0.0;
        } else {
            meterR11Top.progress = 0.0;
            meterR11Btm.progress = 0.0;
        }
        [viewController sendData:CMD_R_GEQ_200HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderRGeq11.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderLGeq11.value = sliderRGeq11.value;
            if (sliderLGeq11.value > 0.0) {
                meterL11Top.progress = sliderLGeq11.value/GEQ_SLIDER_MAX_VALUE;
                meterL11Btm.progress = 0.0;
            } else if (sliderLGeq11.value < 0.0) {
                meterL11Btm.progress = sliderLGeq11.value/GEQ_SLIDER_MIN_VALUE;
                meterL11Top.progress = 0.0;
            } else {
                meterL11Top.progress = 0.0;
                meterL11Btm.progress = 0.0;
            }
            [viewController sendData:CMD_L_GEQ_200HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderLGeq11.value)];
        }
        [self updatePreviewImage];
    }
}

- (IBAction)onSlider12Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider12End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider12Action:(id) sender {
    if (sender == sliderLGeq12) {
        if (sliderLGeq12.value > 0.0) {
            meterL12Top.progress = sliderLGeq12.value/GEQ_SLIDER_MAX_VALUE;
            meterL12Btm.progress = 0.0;
        } else if (sliderLGeq12.value < 0.0) {
            meterL12Btm.progress = sliderLGeq12.value/GEQ_SLIDER_MIN_VALUE;
            meterL12Top.progress = 0.0;
        } else {
            meterL12Top.progress = 0.0;
            meterL12Btm.progress = 0.0;
        }
        [viewController sendData:CMD_L_GEQ_250HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderLGeq12.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderRGeq12.value = sliderLGeq12.value;
            if (sliderRGeq12.value > 0.0) {
                meterR12Top.progress = sliderRGeq12.value/GEQ_SLIDER_MAX_VALUE;
                meterR12Btm.progress = 0.0;
            } else if (sliderRGeq12.value < 0.0) {
                meterR12Btm.progress = sliderRGeq12.value/GEQ_SLIDER_MIN_VALUE;
                meterR12Top.progress = 0.0;
            } else {
                meterR12Top.progress = 0.0;
                meterR12Btm.progress = 0.0;
            }
            [viewController sendData:CMD_R_GEQ_250HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderRGeq12.value)];
        }
        [self updatePreviewImage];

    } else if (sender == sliderRGeq12) {
        if (sliderRGeq12.value > 0.0) {
            meterR12Top.progress = sliderRGeq12.value/GEQ_SLIDER_MAX_VALUE;
            meterR12Btm.progress = 0.0;
        } else if (sliderRGeq12.value < 0.0) {
            meterR12Btm.progress = sliderRGeq12.value/GEQ_SLIDER_MIN_VALUE;
            meterR12Top.progress = 0.0;
        } else {
            meterR12Top.progress = 0.0;
            meterR12Btm.progress = 0.0;
        }
        [viewController sendData:CMD_R_GEQ_250HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderRGeq12.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderLGeq12.value = sliderRGeq12.value;
            if (sliderLGeq12.value > 0.0) {
                meterL12Top.progress = sliderLGeq12.value/GEQ_SLIDER_MAX_VALUE;
                meterL12Btm.progress = 0.0;
            } else if (sliderLGeq12.value < 0.0) {
                meterL12Btm.progress = sliderLGeq12.value/GEQ_SLIDER_MIN_VALUE;
                meterL12Top.progress = 0.0;
            } else {
                meterL12Top.progress = 0.0;
                meterL12Btm.progress = 0.0;
            }
            [viewController sendData:CMD_L_GEQ_250HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderLGeq12.value)];
        }
        [self updatePreviewImage];
    }
}

- (IBAction)onSlider13Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider13End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider13Action:(id) sender {
    if (sender == sliderLGeq13) {
        if (sliderLGeq13.value > 0.0) {
            meterL13Top.progress = sliderLGeq13.value/GEQ_SLIDER_MAX_VALUE;
            meterL13Btm.progress = 0.0;
        } else if (sliderLGeq13.value < 0.0) {
            meterL13Btm.progress = sliderLGeq13.value/GEQ_SLIDER_MIN_VALUE;
            meterL13Top.progress = 0.0;
        } else {
            meterL13Top.progress = 0.0;
            meterL13Btm.progress = 0.0;
        }
        [viewController sendData:CMD_L_GEQ_315HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderLGeq13.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderRGeq13.value = sliderLGeq13.value;
            if (sliderRGeq13.value > 0.0) {
                meterR13Top.progress = sliderRGeq13.value/GEQ_SLIDER_MAX_VALUE;
                meterR13Btm.progress = 0.0;
            } else if (sliderRGeq13.value < 0.0) {
                meterR13Btm.progress = sliderRGeq13.value/GEQ_SLIDER_MIN_VALUE;
                meterR13Top.progress = 0.0;
            } else {
                meterR13Top.progress = 0.0;
                meterR13Btm.progress = 0.0;
            }
            [viewController sendData:CMD_R_GEQ_315HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderRGeq13.value)];
        }
        [self updatePreviewImage];
    } else if (sender == sliderRGeq13) {
        if (sliderRGeq13.value > 0.0) {
            meterR13Top.progress = sliderRGeq13.value/GEQ_SLIDER_MAX_VALUE;
            meterR13Btm.progress = 0.0;
        } else if (sliderRGeq13.value < 0.0) {
            meterR13Btm.progress = sliderRGeq13.value/GEQ_SLIDER_MIN_VALUE;
            meterR13Top.progress = 0.0;
        } else {
            meterR13Top.progress = 0.0;
            meterR13Btm.progress = 0.0;
        }
        [viewController sendData:CMD_R_GEQ_315HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderRGeq13.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderLGeq13.value = sliderRGeq13.value;
            if (sliderLGeq13.value > 0.0) {
                meterL13Top.progress = sliderLGeq13.value/GEQ_SLIDER_MAX_VALUE;
                meterL13Btm.progress = 0.0;
            } else if (sliderLGeq13.value < 0.0) {
                meterL13Btm.progress = sliderLGeq13.value/GEQ_SLIDER_MIN_VALUE;
                meterL13Top.progress = 0.0;
            } else {
                meterL13Top.progress = 0.0;
                meterL13Btm.progress = 0.0;
            }
            [viewController sendData:CMD_L_GEQ_315HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderLGeq13.value)];
        }
        [self updatePreviewImage];
    }
}

- (IBAction)onSlider14Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider14End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider14Action:(id) sender {
    if (sender == sliderLGeq14) {
        if (sliderLGeq14.value > 0.0) {
            meterL14Top.progress = sliderLGeq14.value/GEQ_SLIDER_MAX_VALUE;
            meterL14Btm.progress = 0.0;
        } else if (sliderLGeq14.value < 0.0) {
            meterL14Btm.progress = sliderLGeq14.value/GEQ_SLIDER_MIN_VALUE;
            meterL14Top.progress = 0.0;
        } else {
            meterL14Top.progress = 0.0;
            meterL14Btm.progress = 0.0;
        }
        [viewController sendData:CMD_L_GEQ_400HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderLGeq14.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderRGeq14.value = sliderLGeq14.value;
            if (sliderRGeq14.value > 0.0) {
                meterR14Top.progress = sliderRGeq14.value/GEQ_SLIDER_MAX_VALUE;
                meterR14Btm.progress = 0.0;
            } else if (sliderRGeq14.value < 0.0) {
                meterR14Btm.progress = sliderRGeq14.value/GEQ_SLIDER_MIN_VALUE;
                meterR14Top.progress = 0.0;
            } else {
                meterR14Top.progress = 0.0;
                meterR14Btm.progress = 0.0;
            }
            [viewController sendData:CMD_R_GEQ_400HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderRGeq14.value)];
        }
        [self updatePreviewImage];
    } else if (sender == sliderRGeq14) {
        if (sliderRGeq14.value > 0.0) {
            meterR14Top.progress = sliderRGeq14.value/GEQ_SLIDER_MAX_VALUE;
            meterR14Btm.progress = 0.0;
        } else if (sliderRGeq14.value < 0.0) {
            meterR14Btm.progress = sliderRGeq14.value/GEQ_SLIDER_MIN_VALUE;
            meterR14Top.progress = 0.0;
        } else {
            meterR14Top.progress = 0.0;
            meterR14Btm.progress = 0.0;
        }
        [viewController sendData:CMD_R_GEQ_400HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderRGeq14.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderLGeq14.value = sliderRGeq14.value;
            if (sliderLGeq14.value > 0.0) {
                meterL14Top.progress = sliderLGeq14.value/GEQ_SLIDER_MAX_VALUE;
                meterL14Btm.progress = 0.0;
            } else if (sliderLGeq14.value < 0.0) {
                meterL14Btm.progress = sliderLGeq14.value/GEQ_SLIDER_MIN_VALUE;
                meterL14Top.progress = 0.0;
            } else {
                meterL14Top.progress = 0.0;
                meterL14Btm.progress = 0.0;
            }
            [viewController sendData:CMD_L_GEQ_400HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderLGeq14.value)];
        }
        [self updatePreviewImage];
    }
}

- (IBAction)onSlider15Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider15End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider15Action:(id) sender {
    if (sender == sliderLGeq15) {
        if (sliderLGeq15.value > 0.0) {
            meterL15Top.progress = sliderLGeq15.value/GEQ_SLIDER_MAX_VALUE;
            meterL15Btm.progress = 0.0;
        } else if (sliderLGeq15.value < 0.0) {
            meterL15Btm.progress = sliderLGeq15.value/GEQ_SLIDER_MIN_VALUE;
            meterL15Top.progress = 0.0;
        } else {
            meterL15Top.progress = 0.0;
            meterL15Btm.progress = 0.0;
        }
        [viewController sendData:CMD_L_GEQ_500HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderLGeq15.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderRGeq15.value = sliderLGeq15.value;
            if (sliderRGeq15.value > 0.0) {
                meterR15Top.progress = sliderRGeq15.value/GEQ_SLIDER_MAX_VALUE;
                meterR15Btm.progress = 0.0;
            } else if (sliderRGeq15.value < 0.0) {
                meterR15Btm.progress = sliderRGeq15.value/GEQ_SLIDER_MIN_VALUE;
                meterR15Top.progress = 0.0;
            } else {
                meterR15Top.progress = 0.0;
                meterR15Btm.progress = 0.0;
            }
            [viewController sendData:CMD_R_GEQ_500HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderRGeq15.value)];
        }
        [self updatePreviewImage];
    } else if (sender == sliderRGeq15) {
        if (sliderRGeq15.value > 0.0) {
            meterR15Top.progress = sliderRGeq15.value/GEQ_SLIDER_MAX_VALUE;
            meterR15Btm.progress = 0.0;
        } else if (sliderRGeq15.value < 0.0) {
            meterR15Btm.progress = sliderRGeq15.value/GEQ_SLIDER_MIN_VALUE;
            meterR15Top.progress = 0.0;
        } else {
            meterR15Top.progress = 0.0;
            meterR15Btm.progress = 0.0;
        }
        [viewController sendData:CMD_R_GEQ_500HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderRGeq15.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderLGeq15.value = sliderRGeq15.value;
            if (sliderLGeq15.value > 0.0) {
                meterL15Top.progress = sliderLGeq15.value/GEQ_SLIDER_MAX_VALUE;
                meterL15Btm.progress = 0.0;
            } else if (sliderLGeq15.value < 0.0) {
                meterL15Btm.progress = sliderLGeq15.value/GEQ_SLIDER_MIN_VALUE;
                meterL15Top.progress = 0.0;
            } else {
                meterL15Top.progress = 0.0;
                meterL15Btm.progress = 0.0;
            }
            [viewController sendData:CMD_L_GEQ_500HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderLGeq15.value)];
        }
        [self updatePreviewImage];
    }
}

- (IBAction)onSlider16Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider16End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider16Action:(id) sender {
    if (sender == sliderLGeq16) {
        if (sliderLGeq16.value > 0.0) {
            meterL16Top.progress = sliderLGeq16.value/GEQ_SLIDER_MAX_VALUE;
            meterL16Btm.progress = 0.0;
        } else if (sliderLGeq16.value < 0.0) {
            meterL16Btm.progress = sliderLGeq16.value/GEQ_SLIDER_MIN_VALUE;
            meterL16Top.progress = 0.0;
        } else {
            meterL16Top.progress = 0.0;
            meterL16Btm.progress = 0.0;
        }
        [viewController sendData:CMD_L_GEQ_630HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderLGeq16.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderRGeq16.value = sliderLGeq16.value;
            if (sliderRGeq16.value > 0.0) {
                meterR16Top.progress = sliderRGeq16.value/GEQ_SLIDER_MAX_VALUE;
                meterR16Btm.progress = 0.0;
            } else if (sliderRGeq16.value < 0.0) {
                meterR16Btm.progress = sliderRGeq16.value/GEQ_SLIDER_MIN_VALUE;
                meterR16Top.progress = 0.0;
            } else {
                meterR16Top.progress = 0.0;
                meterR16Btm.progress = 0.0;
            }
            [viewController sendData:CMD_R_GEQ_630HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderRGeq16.value)];
        }
        [self updatePreviewImage];
    } else if (sender == sliderRGeq16) {
        if (sliderRGeq16.value > 0.0) {
            meterR16Top.progress = sliderRGeq16.value/GEQ_SLIDER_MAX_VALUE;
            meterR16Btm.progress = 0.0;
        } else if (sliderRGeq16.value < 0.0) {
            meterR16Btm.progress = sliderRGeq16.value/GEQ_SLIDER_MIN_VALUE;
            meterR16Top.progress = 0.0;
        } else {
            meterR16Top.progress = 0.0;
            meterR16Btm.progress = 0.0;
        }
        [viewController sendData:CMD_R_GEQ_630HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderRGeq16.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderLGeq16.value = sliderRGeq16.value;
            if (sliderLGeq16.value > 0.0) {
                meterL16Top.progress = sliderLGeq16.value/GEQ_SLIDER_MAX_VALUE;
                meterL16Btm.progress = 0.0;
            } else if (sliderLGeq16.value < 0.0) {
                meterL16Btm.progress = sliderLGeq16.value/GEQ_SLIDER_MIN_VALUE;
                meterL16Top.progress = 0.0;
            } else {
                meterL16Top.progress = 0.0;
                meterL16Btm.progress = 0.0;
            }
            [viewController sendData:CMD_L_GEQ_630HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderLGeq16.value)];
        }
        [self updatePreviewImage];
    }
}

- (IBAction)onSlider17Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider17End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider17Action:(id) sender {
    if (sender == sliderLGeq17) {
        if (sliderLGeq17.value > 0.0) {
            meterL17Top.progress = sliderLGeq17.value/GEQ_SLIDER_MAX_VALUE;
            meterL17Btm.progress = 0.0;
        } else if (sliderLGeq17.value < 0.0) {
            meterL17Btm.progress = sliderLGeq17.value/GEQ_SLIDER_MIN_VALUE;
            meterL17Top.progress = 0.0;
        } else {
            meterL17Top.progress = 0.0;
            meterL17Btm.progress = 0.0;
        }
        [viewController sendData:CMD_L_GEQ_800HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderLGeq17.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderRGeq17.value = sliderLGeq17.value;
            if (sliderRGeq17.value > 0.0) {
                meterR17Top.progress = sliderRGeq17.value/GEQ_SLIDER_MAX_VALUE;
                meterR17Btm.progress = 0.0;
            } else if (sliderRGeq17.value < 0.0) {
                meterR17Btm.progress = sliderRGeq17.value/GEQ_SLIDER_MIN_VALUE;
                meterR17Top.progress = 0.0;
            } else {
                meterR17Top.progress = 0.0;
                meterR17Btm.progress = 0.0;
            }
            [viewController sendData:CMD_R_GEQ_800HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderRGeq17.value)];
        }
        [self updatePreviewImage];

    } else if (sender == sliderRGeq17) {
        if (sliderRGeq17.value > 0.0) {
            meterR17Top.progress = sliderRGeq17.value/GEQ_SLIDER_MAX_VALUE;
            meterR17Btm.progress = 0.0;
        } else if (sliderRGeq17.value < 0.0) {
            meterR17Btm.progress = sliderRGeq17.value/GEQ_SLIDER_MIN_VALUE;
            meterR17Top.progress = 0.0;
        } else {
            meterR17Top.progress = 0.0;
            meterR17Btm.progress = 0.0;
        }
        [viewController sendData:CMD_R_GEQ_800HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderRGeq17.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderLGeq17.value = sliderRGeq17.value;
            if (sliderLGeq17.value > 0.0) {
                meterL17Top.progress = sliderLGeq17.value/GEQ_SLIDER_MAX_VALUE;
                meterL17Btm.progress = 0.0;
            } else if (sliderLGeq17.value < 0.0) {
                meterL17Btm.progress = sliderLGeq17.value/GEQ_SLIDER_MIN_VALUE;
                meterL17Top.progress = 0.0;
            } else {
                meterL17Top.progress = 0.0;
                meterL17Btm.progress = 0.0;
            }
            [viewController sendData:CMD_L_GEQ_800HZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderLGeq17.value)];
        }
        [self updatePreviewImage];
    }
}

- (IBAction)onSlider18Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider18End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider18Action:(id) sender {
    if (sender == sliderLGeq18) {
        if (sliderLGeq18.value > 0.0) {
            meterL18Top.progress = sliderLGeq18.value/GEQ_SLIDER_MAX_VALUE;
            meterL18Btm.progress = 0.0;
        } else if (sliderLGeq18.value < 0.0) {
            meterL18Btm.progress = sliderLGeq18.value/GEQ_SLIDER_MIN_VALUE;
            meterL18Top.progress = 0.0;
        } else {
            meterL18Top.progress = 0.0;
            meterL18Btm.progress = 0.0;
        }
        [viewController sendData:CMD_L_GEQ_1KHZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderLGeq18.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderRGeq18.value = sliderLGeq18.value;
            if (sliderRGeq18.value > 0.0) {
                meterR18Top.progress = sliderRGeq18.value/GEQ_SLIDER_MAX_VALUE;
                meterR18Btm.progress = 0.0;
            } else if (sliderRGeq18.value < 0.0) {
                meterR18Btm.progress = sliderRGeq18.value/GEQ_SLIDER_MIN_VALUE;
                meterR18Top.progress = 0.0;
            } else {
                meterR18Top.progress = 0.0;
                meterR18Btm.progress = 0.0;
            }
            [viewController sendData:CMD_R_GEQ_1KHZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderRGeq18.value)];
        }
        [self updatePreviewImage];
    } else if (sender == sliderRGeq18) {
        if (sliderRGeq18.value > 0.0) {
            meterR18Top.progress = sliderRGeq18.value/GEQ_SLIDER_MAX_VALUE;
            meterR18Btm.progress = 0.0;
        } else if (sliderRGeq18.value < 0.0) {
            meterR18Btm.progress = sliderRGeq18.value/GEQ_SLIDER_MIN_VALUE;
            meterR18Top.progress = 0.0;
        } else {
            meterR18Top.progress = 0.0;
            meterR18Btm.progress = 0.0;
        }
        [viewController sendData:CMD_R_GEQ_1KHZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderRGeq18.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderLGeq18.value = sliderRGeq18.value;
            if (sliderLGeq18.value > 0.0) {
                meterL18Top.progress = sliderLGeq18.value/GEQ_SLIDER_MAX_VALUE;
                meterL18Btm.progress = 0.0;
            } else if (sliderLGeq18.value < 0.0) {
                meterL18Btm.progress = sliderLGeq18.value/GEQ_SLIDER_MIN_VALUE;
                meterL18Top.progress = 0.0;
            } else {
                meterL18Top.progress = 0.0;
                meterL18Btm.progress = 0.0;
            }
            [viewController sendData:CMD_L_GEQ_1KHZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderLGeq18.value)];
        }
        [self updatePreviewImage];
    }
}

- (IBAction)onSlider19Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider19End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider19Action:(id) sender {
    if (sender == sliderLGeq19) {
        if (sliderLGeq19.value > 0.0) {
            meterL19Top.progress = sliderLGeq19.value/GEQ_SLIDER_MAX_VALUE;
            meterL19Btm.progress = 0.0;
        } else if (sliderLGeq19.value < 0.0) {
            meterL19Btm.progress = sliderLGeq19.value/GEQ_SLIDER_MIN_VALUE;
            meterL19Top.progress = 0.0;
        } else {
            meterL19Top.progress = 0.0;
            meterL19Btm.progress = 0.0;
        }
        [viewController sendData:CMD_L_GEQ_1_25KHZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderLGeq19.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderRGeq19.value = sliderLGeq19.value;
            if (sliderRGeq19.value > 0.0) {
                meterR19Top.progress = sliderRGeq19.value/GEQ_SLIDER_MAX_VALUE;
                meterR19Btm.progress = 0.0;
            } else if (sliderRGeq19.value < 0.0) {
                meterR19Btm.progress = sliderRGeq19.value/GEQ_SLIDER_MIN_VALUE;
                meterR19Top.progress = 0.0;
            } else {
                meterR19Top.progress = 0.0;
                meterR19Btm.progress = 0.0;
            }
            [viewController sendData:CMD_R_GEQ_1_25KHZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderRGeq19.value)];
        }
        [self updatePreviewImage];
    } else if (sender == sliderRGeq19) {
        if (sliderRGeq19.value > 0.0) {
            meterR19Top.progress = sliderRGeq19.value/GEQ_SLIDER_MAX_VALUE;
            meterR19Btm.progress = 0.0;
        } else if (sliderRGeq19.value < 0.0) {
            meterR19Btm.progress = sliderRGeq19.value/GEQ_SLIDER_MIN_VALUE;
            meterR19Top.progress = 0.0;
        } else {
            meterR19Top.progress = 0.0;
            meterR19Btm.progress = 0.0;
        }
        [viewController sendData:CMD_R_GEQ_1_25KHZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderRGeq19.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderLGeq19.value = sliderRGeq19.value;
            if (sliderLGeq19.value > 0.0) {
                meterL19Top.progress = sliderLGeq19.value/GEQ_SLIDER_MAX_VALUE;
                meterL19Btm.progress = 0.0;
            } else if (sliderLGeq19.value < 0.0) {
                meterL19Btm.progress = sliderLGeq19.value/GEQ_SLIDER_MIN_VALUE;
                meterL19Top.progress = 0.0;
            } else {
                meterL19Top.progress = 0.0;
                meterL19Btm.progress = 0.0;
            }
            [viewController sendData:CMD_L_GEQ_1_25KHZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderLGeq19.value)];
        }
        [self updatePreviewImage];
    }
}

- (IBAction)onSlider20Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider20End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider20Action:(id) sender {
    if (sender == sliderLGeq20) {
        if (sliderLGeq20.value > 0.0) {
            meterL20Top.progress = sliderLGeq20.value/GEQ_SLIDER_MAX_VALUE;
            meterL20Btm.progress = 0.0;
        } else if (sliderLGeq20.value < 0.0) {
            meterL20Btm.progress = sliderLGeq20.value/GEQ_SLIDER_MIN_VALUE;
            meterL20Top.progress = 0.0;
        } else {
            meterL20Top.progress = 0.0;
            meterL20Btm.progress = 0.0;
        }
        [viewController sendData:CMD_L_GEQ_1_6KHZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderLGeq20.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderRGeq20.value = sliderLGeq20.value;
            if (sliderRGeq20.value > 0.0) {
                meterR20Top.progress = sliderRGeq20.value/GEQ_SLIDER_MAX_VALUE;
                meterR20Btm.progress = 0.0;
            } else if (sliderRGeq20.value < 0.0) {
                meterR20Btm.progress = sliderRGeq20.value/GEQ_SLIDER_MIN_VALUE;
                meterR20Top.progress = 0.0;
            } else {
                meterR20Top.progress = 0.0;
                meterR20Btm.progress = 0.0;
            }
            [viewController sendData:CMD_R_GEQ_1_6KHZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderRGeq20.value)];
        }
        [self updatePreviewImage];
    } else if (sender == sliderRGeq20) {
        if (sliderRGeq20.value > 0.0) {
            meterR20Top.progress = sliderRGeq20.value/GEQ_SLIDER_MAX_VALUE;
            meterR20Btm.progress = 0.0;
        } else if (sliderRGeq20.value < 0.0) {
            meterR20Btm.progress = sliderRGeq20.value/GEQ_SLIDER_MIN_VALUE;
            meterR20Top.progress = 0.0;
        } else {
            meterR20Top.progress = 0.0;
            meterR20Btm.progress = 0.0;
        }
        [viewController sendData:CMD_R_GEQ_1_6KHZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderRGeq20.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderLGeq20.value = sliderRGeq20.value;
            if (sliderLGeq20.value > 0.0) {
                meterL20Top.progress = sliderLGeq20.value/GEQ_SLIDER_MAX_VALUE;
                meterL20Btm.progress = 0.0;
            } else if (sliderLGeq20.value < 0.0) {
                meterL20Btm.progress = sliderLGeq20.value/GEQ_SLIDER_MIN_VALUE;
                meterL20Top.progress = 0.0;
            } else {
                meterL20Top.progress = 0.0;
                meterL20Btm.progress = 0.0;
            }
            [viewController sendData:CMD_L_GEQ_1_6KHZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderLGeq20.value)];
        }
        [self updatePreviewImage];
    }
}

- (IBAction)onSlider21Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider21End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider21Action:(id) sender {
    if (sender == sliderLGeq21) {
        if (sliderLGeq21.value > 0.0) {
            meterL21Top.progress = sliderLGeq21.value/GEQ_SLIDER_MAX_VALUE;
            meterL21Btm.progress = 0.0;
        } else if (sliderLGeq21.value < 0.0) {
            meterL21Btm.progress = sliderLGeq21.value/GEQ_SLIDER_MIN_VALUE;
            meterL21Top.progress = 0.0;
        } else {
            meterL21Top.progress = 0.0;
            meterL21Btm.progress = 0.0;
        }
        [viewController sendData:CMD_L_GEQ_2KHZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderLGeq21.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderRGeq21.value = sliderLGeq21.value;
            if (sliderRGeq21.value > 0.0) {
                meterR21Top.progress = sliderRGeq21.value/GEQ_SLIDER_MAX_VALUE;
                meterR21Btm.progress = 0.0;
            } else if (sliderRGeq21.value < 0.0) {
                meterR21Btm.progress = sliderRGeq21.value/GEQ_SLIDER_MIN_VALUE;
                meterR21Top.progress = 0.0;
            } else {
                meterR21Top.progress = 0.0;
                meterR21Btm.progress = 0.0;
            }
            [viewController sendData:CMD_R_GEQ_2KHZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderRGeq21.value)];
        }
        [self updatePreviewImage];
    } else if (sender == sliderRGeq21) {
        if (sliderRGeq21.value > 0.0) {
            meterR21Top.progress = sliderRGeq21.value/GEQ_SLIDER_MAX_VALUE;
            meterR21Btm.progress = 0.0;
        } else if (sliderRGeq21.value < 0.0) {
            meterR21Btm.progress = sliderRGeq21.value/GEQ_SLIDER_MIN_VALUE;
            meterR21Top.progress = 0.0;
        } else {
            meterR21Top.progress = 0.0;
            meterR21Btm.progress = 0.0;
        }
        [viewController sendData:CMD_R_GEQ_2KHZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderRGeq21.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderLGeq21.value = sliderRGeq21.value;
            if (sliderLGeq21.value > 0.0) {
                meterL21Top.progress = sliderLGeq21.value/GEQ_SLIDER_MAX_VALUE;
                meterL21Btm.progress = 0.0;
            } else if (sliderLGeq21.value < 0.0) {
                meterL21Btm.progress = sliderLGeq21.value/GEQ_SLIDER_MIN_VALUE;
                meterL21Top.progress = 0.0;
            } else {
                meterL21Top.progress = 0.0;
                meterL21Btm.progress = 0.0;
            }
            [viewController sendData:CMD_L_GEQ_2KHZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderLGeq21.value)];
        }
        [self updatePreviewImage];
    }
}

- (IBAction)onSlider22Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider22End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider22Action:(id) sender {
    if (sender == sliderLGeq22) {
        if (sliderLGeq22.value > 0.0) {
            meterL22Top.progress = sliderLGeq22.value/GEQ_SLIDER_MAX_VALUE;
            meterL22Btm.progress = 0.0;
        } else if (sliderLGeq22.value < 0.0) {
            meterL22Btm.progress = sliderLGeq22.value/GEQ_SLIDER_MIN_VALUE;
            meterL22Top.progress = 0.0;
        } else {
            meterL22Top.progress = 0.0;
            meterL22Btm.progress = 0.0;
        }
        [viewController sendData:CMD_L_GEQ_2_5KHZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderLGeq22.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderRGeq22.value = sliderLGeq22.value;
            if (sliderRGeq22.value > 0.0) {
                meterR22Top.progress = sliderRGeq22.value/GEQ_SLIDER_MAX_VALUE;
                meterR22Btm.progress = 0.0;
            } else if (sliderRGeq22.value < 0.0) {
                meterR22Btm.progress = sliderRGeq22.value/GEQ_SLIDER_MIN_VALUE;
                meterR22Top.progress = 0.0;
            } else {
                meterR22Top.progress = 0.0;
                meterR22Btm.progress = 0.0;
            }
            [viewController sendData:CMD_R_GEQ_2_5KHZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderRGeq22.value)];
        }
        [self updatePreviewImage];
    } else if (sender == sliderRGeq22) {
        if (sliderRGeq22.value > 0.0) {
            meterR22Top.progress = sliderRGeq22.value/GEQ_SLIDER_MAX_VALUE;
            meterR22Btm.progress = 0.0;
        } else if (sliderRGeq22.value < 0.0) {
            meterR22Btm.progress = sliderRGeq22.value/GEQ_SLIDER_MIN_VALUE;
            meterR22Top.progress = 0.0;
        } else {
            meterR22Top.progress = 0.0;
            meterR22Btm.progress = 0.0;
        }
        [viewController sendData:CMD_R_GEQ_2_5KHZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderRGeq22.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderLGeq22.value = sliderRGeq22.value;
            if (sliderLGeq22.value > 0.0) {
                meterL22Top.progress = sliderLGeq22.value/GEQ_SLIDER_MAX_VALUE;
                meterL22Btm.progress = 0.0;
            } else if (sliderLGeq22.value < 0.0) {
                meterL22Btm.progress = sliderLGeq22.value/GEQ_SLIDER_MIN_VALUE;
                meterL22Top.progress = 0.0;
            } else {
                meterL22Top.progress = 0.0;
                meterL22Btm.progress = 0.0;
            }
            [viewController sendData:CMD_L_GEQ_2_5KHZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderLGeq22.value)];
        }
        [self updatePreviewImage];
    }
}

- (IBAction)onSlider23Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider23End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider23Action:(id) sender {
    if (sender == sliderLGeq23) {
        if (sliderLGeq23.value > 0.0) {
            meterL23Top.progress = sliderLGeq23.value/GEQ_SLIDER_MAX_VALUE;
            meterL23Btm.progress = 0.0;
        } else if (sliderLGeq23.value < 0.0) {
            meterL23Btm.progress = sliderLGeq23.value/GEQ_SLIDER_MIN_VALUE;
            meterL23Top.progress = 0.0;
        } else {
            meterL23Top.progress = 0.0;
            meterL23Btm.progress = 0.0;
        }
        [viewController sendData:CMD_L_GEQ_3_15KHZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderLGeq23.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderRGeq23.value = sliderLGeq23.value;
            if (sliderRGeq23.value > 0.0) {
                meterR23Top.progress = sliderRGeq23.value/GEQ_SLIDER_MAX_VALUE;
                meterR23Btm.progress = 0.0;
            } else if (sliderRGeq23.value < 0.0) {
                meterR23Btm.progress = sliderRGeq23.value/GEQ_SLIDER_MIN_VALUE;
                meterR23Top.progress = 0.0;
            } else {
                meterR23Top.progress = 0.0;
                meterR23Btm.progress = 0.0;
            }
            [viewController sendData:CMD_R_GEQ_3_15KHZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderRGeq23.value)];
        }
        [self updatePreviewImage];
    } else if (sender == sliderRGeq23) {
        if (sliderRGeq23.value > 0.0) {
            meterR23Top.progress = sliderRGeq23.value/GEQ_SLIDER_MAX_VALUE;
            meterR23Btm.progress = 0.0;
        } else if (sliderRGeq23.value < 0.0) {
            meterR23Btm.progress = sliderRGeq23.value/GEQ_SLIDER_MIN_VALUE;
            meterR23Top.progress = 0.0;
        } else {
            meterR23Top.progress = 0.0;
            meterR23Btm.progress = 0.0;
        }
        [viewController sendData:CMD_R_GEQ_3_15KHZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderRGeq23.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderLGeq23.value = sliderRGeq23.value;
            if (sliderLGeq23.value > 0.0) {
                meterL23Top.progress = sliderLGeq23.value/GEQ_SLIDER_MAX_VALUE;
                meterL23Btm.progress = 0.0;
            } else if (sliderLGeq23.value < 0.0) {
                meterL23Btm.progress = sliderLGeq23.value/GEQ_SLIDER_MIN_VALUE;
                meterL23Top.progress = 0.0;
            } else {
                meterL23Top.progress = 0.0;
                meterL23Btm.progress = 0.0;
            }
            [viewController sendData:CMD_L_GEQ_3_15KHZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderLGeq23.value)];
        }
        [self updatePreviewImage];
    }
}

- (IBAction)onSlider24Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider24End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider24Action:(id) sender {
    if (sender == sliderLGeq24) {
        if (sliderLGeq24.value > 0.0) {
            meterL24Top.progress = sliderLGeq24.value/GEQ_SLIDER_MAX_VALUE;
            meterL24Btm.progress = 0.0;
        } else if (sliderLGeq24.value < 0.0) {
            meterL24Btm.progress = sliderLGeq24.value/GEQ_SLIDER_MIN_VALUE;
            meterL24Top.progress = 0.0;
        } else {
            meterL24Top.progress = 0.0;
            meterL24Btm.progress = 0.0;
        }
        [viewController sendData:CMD_L_GEQ_4KHZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderLGeq24.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderRGeq24.value = sliderLGeq24.value;
            if (sliderRGeq24.value > 0.0) {
                meterR24Top.progress = sliderRGeq24.value/GEQ_SLIDER_MAX_VALUE;
                meterR24Btm.progress = 0.0;
            } else if (sliderRGeq24.value < 0.0) {
                meterR24Btm.progress = sliderRGeq24.value/GEQ_SLIDER_MIN_VALUE;
                meterR24Top.progress = 0.0;
            } else {
                meterR24Top.progress = 0.0;
                meterR24Btm.progress = 0.0;
            }
            [viewController sendData:CMD_R_GEQ_4KHZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderRGeq24.value)];
        }
        [self updatePreviewImage];
    } else if (sender == sliderRGeq24) {
        if (sliderRGeq24.value > 0.0) {
            meterR24Top.progress = sliderRGeq24.value/GEQ_SLIDER_MAX_VALUE;
            meterR24Btm.progress = 0.0;
        } else if (sliderRGeq24.value < 0.0) {
            meterR24Btm.progress = sliderRGeq24.value/GEQ_SLIDER_MIN_VALUE;
            meterR24Top.progress = 0.0;
        } else {
            meterR24Top.progress = 0.0;
            meterR24Btm.progress = 0.0;
        }
        [viewController sendData:CMD_R_GEQ_4KHZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderRGeq24.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderLGeq24.value = sliderRGeq24.value;
            if (sliderLGeq24.value > 0.0) {
                meterL24Top.progress = sliderLGeq24.value/GEQ_SLIDER_MAX_VALUE;
                meterL24Btm.progress = 0.0;
            } else if (sliderLGeq24.value < 0.0) {
                meterL24Btm.progress = sliderLGeq24.value/GEQ_SLIDER_MIN_VALUE;
                meterL24Top.progress = 0.0;
            } else {
                meterL24Top.progress = 0.0;
                meterL24Btm.progress = 0.0;
            }
            [viewController sendData:CMD_L_GEQ_4KHZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderLGeq24.value)];
        }
        [self updatePreviewImage];
    }
}

- (IBAction)onSlider25Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider25End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider25Action:(id) sender {
    if (sender == sliderLGeq25) {
        if (sliderLGeq25.value > 0.0) {
            meterL25Top.progress = sliderLGeq25.value/GEQ_SLIDER_MAX_VALUE;
            meterL25Btm.progress = 0.0;
        } else if (sliderLGeq25.value < 0.0) {
            meterL25Btm.progress = sliderLGeq25.value/GEQ_SLIDER_MIN_VALUE;
            meterL25Top.progress = 0.0;
        } else {
            meterL25Top.progress = 0.0;
            meterL25Btm.progress = 0.0;
        }
        [viewController sendData:CMD_L_GEQ_5KHZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderLGeq25.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderRGeq25.value = sliderLGeq25.value;
            if (sliderRGeq25.value > 0.0) {
                meterR25Top.progress = sliderRGeq25.value/GEQ_SLIDER_MAX_VALUE;
                meterR25Btm.progress = 0.0;
            } else if (sliderRGeq25.value < 0.0) {
                meterR25Btm.progress = sliderRGeq25.value/GEQ_SLIDER_MIN_VALUE;
                meterR25Top.progress = 0.0;
            } else {
                meterR25Top.progress = 0.0;
                meterR25Btm.progress = 0.0;
            }
            [viewController sendData:CMD_R_GEQ_5KHZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderRGeq25.value)];
        }
        [self updatePreviewImage];
    } else if (sender == sliderRGeq25) {
        if (sliderRGeq25.value > 0.0) {
            meterR25Top.progress = sliderRGeq25.value/GEQ_SLIDER_MAX_VALUE;
            meterR25Btm.progress = 0.0;
        } else if (sliderRGeq25.value < 0.0) {
            meterR25Btm.progress = sliderRGeq25.value/GEQ_SLIDER_MIN_VALUE;
            meterR25Top.progress = 0.0;
        } else {
            meterR25Top.progress = 0.0;
            meterR25Btm.progress = 0.0;
        }
        [viewController sendData:CMD_R_GEQ_5KHZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderRGeq25.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderLGeq25.value = sliderRGeq25.value;
            if (sliderLGeq25.value > 0.0) {
                meterL25Top.progress = sliderLGeq25.value/GEQ_SLIDER_MAX_VALUE;
                meterL25Btm.progress = 0.0;
            } else if (sliderLGeq25.value < 0.0) {
                meterL25Btm.progress = sliderLGeq25.value/GEQ_SLIDER_MIN_VALUE;
                meterL25Top.progress = 0.0;
            } else {
                meterL25Top.progress = 0.0;
                meterL25Btm.progress = 0.0;
            }
            [viewController sendData:CMD_L_GEQ_5KHZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderLGeq25.value)];
        }
        [self updatePreviewImage];
    }
}

- (IBAction)onSlider26Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider26End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider26Action:(id) sender {
    if (sender == sliderLGeq26) {
        if (sliderLGeq26.value > 0.0) {
            meterL26Top.progress = sliderLGeq26.value/GEQ_SLIDER_MAX_VALUE;
            meterL26Btm.progress = 0.0;
        } else if (sliderLGeq26.value < 0.0) {
            meterL26Btm.progress = sliderLGeq26.value/GEQ_SLIDER_MIN_VALUE;
            meterL26Top.progress = 0.0;
        } else {
            meterL26Top.progress = 0.0;
            meterL26Btm.progress = 0.0;
        }
        [viewController sendData:CMD_L_GEQ_6_3KHZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderLGeq26.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderRGeq26.value = sliderLGeq26.value;
            if (sliderRGeq26.value > 0.0) {
                meterR26Top.progress = sliderRGeq26.value/GEQ_SLIDER_MAX_VALUE;
                meterR26Btm.progress = 0.0;
            } else if (sliderRGeq26.value < 0.0) {
                meterR26Btm.progress = sliderRGeq26.value/GEQ_SLIDER_MIN_VALUE;
                meterR26Top.progress = 0.0;
            } else {
                meterR26Top.progress = 0.0;
                meterR26Btm.progress = 0.0;
            }
            [viewController sendData:CMD_R_GEQ_6_3KHZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderRGeq26.value)];
        }
        [self updatePreviewImage];
    } else if (sender == sliderRGeq26) {
        if (sliderRGeq26.value > 0.0) {
            meterR26Top.progress = sliderRGeq26.value/GEQ_SLIDER_MAX_VALUE;
            meterR26Btm.progress = 0.0;
        } else if (sliderRGeq26.value < 0.0) {
            meterR26Btm.progress = sliderRGeq26.value/GEQ_SLIDER_MIN_VALUE;
            meterR26Top.progress = 0.0;
        } else {
            meterR26Top.progress = 0.0;
            meterR26Btm.progress = 0.0;
        }
        [viewController sendData:CMD_R_GEQ_6_3KHZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderRGeq26.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderLGeq26.value = sliderRGeq26.value;
            if (sliderLGeq26.value > 0.0) {
                meterL26Top.progress = sliderLGeq26.value/GEQ_SLIDER_MAX_VALUE;
                meterL26Btm.progress = 0.0;
            } else if (sliderLGeq26.value < 0.0) {
                meterL26Btm.progress = sliderLGeq26.value/GEQ_SLIDER_MIN_VALUE;
                meterL26Top.progress = 0.0;
            } else {
                meterL26Top.progress = 0.0;
                meterL26Btm.progress = 0.0;
            }
            [viewController sendData:CMD_L_GEQ_6_3KHZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderLGeq26.value)];
        }
        [self updatePreviewImage];
    }
}

- (IBAction)onSlider27Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider27End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider27Action:(id) sender {
    if (sender == sliderLGeq27) {
        if (sliderLGeq27.value > 0.0) {
            meterL27Top.progress = sliderLGeq27.value/GEQ_SLIDER_MAX_VALUE;
            meterL27Btm.progress = 0.0;
        } else if (sliderLGeq27.value < 0.0) {
            meterL27Btm.progress = sliderLGeq27.value/GEQ_SLIDER_MIN_VALUE;
            meterL27Top.progress = 0.0;
        } else {
            meterL27Top.progress = 0.0;
            meterL27Btm.progress = 0.0;
        }
        [viewController sendData:CMD_L_GEQ_8KHZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderLGeq27.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderRGeq27.value = sliderLGeq27.value;
            if (sliderRGeq27.value > 0.0) {
                meterR27Top.progress = sliderRGeq27.value/GEQ_SLIDER_MAX_VALUE;
                meterR27Btm.progress = 0.0;
            } else if (sliderRGeq27.value < 0.0) {
                meterR27Btm.progress = sliderRGeq27.value/GEQ_SLIDER_MIN_VALUE;
                meterR27Top.progress = 0.0;
            } else {
                meterR27Top.progress = 0.0;
                meterR27Btm.progress = 0.0;
            }
            [viewController sendData:CMD_R_GEQ_8KHZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderRGeq27.value)];
        }
        [self updatePreviewImage];
    } else if (sender == sliderRGeq27) {
        if (sliderRGeq27.value > 0.0) {
            meterR27Top.progress = sliderRGeq27.value/GEQ_SLIDER_MAX_VALUE;
            meterR27Btm.progress = 0.0;
        } else if (sliderRGeq27.value < 0.0) {
            meterR27Btm.progress = sliderRGeq27.value/GEQ_SLIDER_MIN_VALUE;
            meterR27Top.progress = 0.0;
        } else {
            meterR27Top.progress = 0.0;
            meterR27Btm.progress = 0.0;
        }
        [viewController sendData:CMD_R_GEQ_8KHZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderRGeq27.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderLGeq27.value = sliderRGeq27.value;
            if (sliderLGeq27.value > 0.0) {
                meterL27Top.progress = sliderLGeq27.value/GEQ_SLIDER_MAX_VALUE;
                meterL27Btm.progress = 0.0;
            } else if (sliderLGeq27.value < 0.0) {
                meterL27Btm.progress = sliderLGeq27.value/GEQ_SLIDER_MIN_VALUE;
                meterL27Top.progress = 0.0;
            } else {
                meterL27Top.progress = 0.0;
                meterL27Btm.progress = 0.0;
            }
            [viewController sendData:CMD_L_GEQ_8KHZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderLGeq27.value)];
        }
        [self updatePreviewImage];
    }
}

- (IBAction)onSlider28Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider28End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider28Action:(id) sender {
    if (sender == sliderLGeq28) {
        if (sliderLGeq28.value > 0.0) {
            meterL28Top.progress = sliderLGeq28.value/GEQ_SLIDER_MAX_VALUE;
            meterL28Btm.progress = 0.0;
        } else if (sliderLGeq28.value < 0.0) {
            meterL28Btm.progress = sliderLGeq28.value/GEQ_SLIDER_MIN_VALUE;
            meterL28Top.progress = 0.0;
        } else {
            meterL28Top.progress = 0.0;
            meterL28Btm.progress = 0.0;
        }
        [viewController sendData:CMD_L_GEQ_10KHZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderLGeq28.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderRGeq28.value = sliderLGeq28.value;
            if (sliderRGeq28.value > 0.0) {
                meterR28Top.progress = sliderRGeq28.value/GEQ_SLIDER_MAX_VALUE;
                meterR28Btm.progress = 0.0;
            } else if (sliderRGeq28.value < 0.0) {
                meterR28Btm.progress = sliderRGeq28.value/GEQ_SLIDER_MIN_VALUE;
                meterR28Top.progress = 0.0;
            } else {
                meterR28Top.progress = 0.0;
                meterR28Btm.progress = 0.0;
            }
            [viewController sendData:CMD_R_GEQ_10KHZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderRGeq28.value)];
        }
        [self updatePreviewImage];
    } else if (sender == sliderRGeq28) {
        if (sliderRGeq28.value > 0.0) {
            meterR28Top.progress = sliderRGeq28.value/GEQ_SLIDER_MAX_VALUE;
            meterR28Btm.progress = 0.0;
        } else if (sliderRGeq28.value < 0.0) {
            meterR28Btm.progress = sliderRGeq28.value/GEQ_SLIDER_MIN_VALUE;
            meterR28Top.progress = 0.0;
        } else {
            meterR28Top.progress = 0.0;
            meterR28Btm.progress = 0.0;
        }
        [viewController sendData:CMD_R_GEQ_10KHZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderRGeq28.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderLGeq28.value = sliderRGeq28.value;
            if (sliderLGeq28.value > 0.0) {
                meterL28Top.progress = sliderLGeq28.value/GEQ_SLIDER_MAX_VALUE;
                meterL28Btm.progress = 0.0;
            } else if (sliderLGeq28.value < 0.0) {
                meterL28Btm.progress = sliderLGeq28.value/GEQ_SLIDER_MIN_VALUE;
                meterL28Top.progress = 0.0;
            } else {
                meterL28Top.progress = 0.0;
                meterL28Btm.progress = 0.0;
            }
            [viewController sendData:CMD_L_GEQ_10KHZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderLGeq28.value)];
        }
        [self updatePreviewImage];
    }
}

- (IBAction)onSlider29Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider29End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider29Action:(id) sender {
    if (sender == sliderLGeq29) {
        if (sliderLGeq29.value > 0.0) {
            meterL29Top.progress = sliderLGeq29.value/GEQ_SLIDER_MAX_VALUE;
            meterL29Btm.progress = 0.0;
        } else if (sliderLGeq29.value < 0.0) {
            meterL29Btm.progress = sliderLGeq29.value/GEQ_SLIDER_MIN_VALUE;
            meterL29Top.progress = 0.0;
        } else {
            meterL29Top.progress = 0.0;
            meterL29Btm.progress = 0.0;
        }
        [viewController sendData:CMD_L_GEQ_12_5KHZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderLGeq29.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderRGeq29.value = sliderLGeq29.value;
            if (sliderRGeq29.value > 0.0) {
                meterR29Top.progress = sliderRGeq29.value/GEQ_SLIDER_MAX_VALUE;
                meterR29Btm.progress = 0.0;
            } else if (sliderRGeq29.value < 0.0) {
                meterR29Btm.progress = sliderRGeq29.value/GEQ_SLIDER_MIN_VALUE;
                meterR29Top.progress = 0.0;
            } else {
                meterR29Top.progress = 0.0;
                meterR29Btm.progress = 0.0;
            }
            [viewController sendData:CMD_R_GEQ_12_5KHZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderRGeq29.value)];
        }
        [self updatePreviewImage];
    } else if (sender == sliderRGeq29) {
        if (sliderRGeq29.value > 0.0) {
            meterR29Top.progress = sliderRGeq29.value/GEQ_SLIDER_MAX_VALUE;
            meterR29Btm.progress = 0.0;
        } else if (sliderRGeq29.value < 0.0) {
            meterR29Btm.progress = sliderRGeq29.value/GEQ_SLIDER_MIN_VALUE;
            meterR29Top.progress = 0.0;
        } else {
            meterR29Top.progress = 0.0;
            meterR29Btm.progress = 0.0;
        }
        [viewController sendData:CMD_R_GEQ_12_5KHZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderRGeq29.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderLGeq29.value = sliderRGeq29.value;
            if (sliderLGeq29.value > 0.0) {
                meterL29Top.progress = sliderLGeq29.value/GEQ_SLIDER_MAX_VALUE;
                meterL29Btm.progress = 0.0;
            } else if (sliderLGeq29.value < 0.0) {
                meterL29Btm.progress = sliderLGeq29.value/GEQ_SLIDER_MIN_VALUE;
                meterL29Top.progress = 0.0;
            } else {
                meterL29Top.progress = 0.0;
                meterL29Btm.progress = 0.0;
            }
            [viewController sendData:CMD_L_GEQ_12_5KHZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderLGeq29.value)];
        }
        [self updatePreviewImage];
    }
}

- (IBAction)onSlider30Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider30End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider30Action:(id) sender {
    if (sender == sliderLGeq30) {
        if (sliderLGeq30.value > 0.0) {
            meterL30Top.progress = sliderLGeq30.value/GEQ_SLIDER_MAX_VALUE;
            meterL30Btm.progress = 0.0;
        } else if (sliderLGeq30.value < 0.0) {
            meterL30Btm.progress = sliderLGeq30.value/GEQ_SLIDER_MIN_VALUE;
            meterL30Top.progress = 0.0;
        } else {
            meterL30Top.progress = 0.0;
            meterL30Btm.progress = 0.0;
        }
        [viewController sendData:CMD_L_GEQ_16KHZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderLGeq30.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderRGeq30.value = sliderLGeq30.value;
            if (sliderRGeq30.value > 0.0) {
                meterR30Top.progress = sliderRGeq30.value/GEQ_SLIDER_MAX_VALUE;
                meterR30Btm.progress = 0.0;
            } else if (sliderRGeq30.value < 0.0) {
                meterR30Btm.progress = sliderRGeq30.value/GEQ_SLIDER_MIN_VALUE;
                meterR30Top.progress = 0.0;
            } else {
                meterR30Top.progress = 0.0;
                meterR30Btm.progress = 0.0;
            }
            [viewController sendData:CMD_R_GEQ_16KHZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderRGeq30.value)];
        }
        [self updatePreviewImage];
    } else if (sender == sliderRGeq30) {
        if (sliderRGeq30.value > 0.0) {
            meterR30Top.progress = sliderRGeq30.value/GEQ_SLIDER_MAX_VALUE;
            meterR30Btm.progress = 0.0;
        } else if (sliderRGeq30.value < 0.0) {
            meterR30Btm.progress = sliderRGeq30.value/GEQ_SLIDER_MIN_VALUE;
            meterR30Top.progress = 0.0;
        } else {
            meterR30Top.progress = 0.0;
            meterR30Btm.progress = 0.0;
        }
        [viewController sendData:CMD_R_GEQ_16KHZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderRGeq30.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderLGeq30.value = sliderRGeq30.value;
            if (sliderLGeq30.value > 0.0) {
                meterL30Top.progress = sliderLGeq30.value/GEQ_SLIDER_MAX_VALUE;
                meterL30Btm.progress = 0.0;
            } else if (sliderLGeq30.value < 0.0) {
                meterL30Btm.progress = sliderLGeq30.value/GEQ_SLIDER_MIN_VALUE;
                meterL30Top.progress = 0.0;
            } else {
                meterL30Top.progress = 0.0;
                meterL30Btm.progress = 0.0;
            }
            [viewController sendData:CMD_L_GEQ_16KHZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderLGeq30.value)];
        }
        [self updatePreviewImage];
    }
}

- (IBAction)onSlider31Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider31End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider31Action:(id) sender {
    if (sender == sliderLGeq31) {
        if (sliderLGeq31.value > 0.0) {
            meterL31Top.progress = sliderLGeq31.value/GEQ_SLIDER_MAX_VALUE;
            meterL31Btm.progress = 0.0;
        } else if (sliderLGeq31.value < 0.0) {
            meterL31Btm.progress = sliderLGeq31.value/GEQ_SLIDER_MIN_VALUE;
            meterL31Top.progress = 0.0;
        } else {
            meterL31Top.progress = 0.0;
            meterL31Btm.progress = 0.0;
        }
        [viewController sendData:CMD_L_GEQ_20KHZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderLGeq31.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderRGeq31.value = sliderLGeq31.value;
            if (sliderRGeq31.value > 0.0) {
                meterR31Top.progress = sliderRGeq31.value/GEQ_SLIDER_MAX_VALUE;
                meterR31Btm.progress = 0.0;
            } else if (sliderRGeq31.value < 0.0) {
                meterR31Btm.progress = sliderRGeq31.value/GEQ_SLIDER_MIN_VALUE;
                meterR31Top.progress = 0.0;
            } else {
                meterR31Top.progress = 0.0;
                meterR31Btm.progress = 0.0;
            }
            [viewController sendData:CMD_R_GEQ_20KHZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderRGeq31.value)];
        }
        [self updatePreviewImage];
    } else if (sender == sliderRGeq31) {
        if (sliderRGeq31.value > 0.0) {
            meterR31Top.progress = sliderRGeq31.value/GEQ_SLIDER_MAX_VALUE;
            meterR31Btm.progress = 0.0;
        } else if (sliderRGeq31.value < 0.0) {
            meterR31Btm.progress = sliderRGeq31.value/GEQ_SLIDER_MIN_VALUE;
            meterR31Top.progress = 0.0;
        } else {
            meterR31Top.progress = 0.0;
            meterR31Btm.progress = 0.0;
        }
        [viewController sendData:CMD_R_GEQ_20KHZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderRGeq31.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderLGeq31.value = sliderRGeq31.value;
            if (sliderLGeq31.value > 0.0) {
                meterL31Top.progress = sliderLGeq31.value/GEQ_SLIDER_MAX_VALUE;
                meterL31Btm.progress = 0.0;
            } else if (sliderLGeq31.value < 0.0) {
                meterL31Btm.progress = sliderLGeq31.value/GEQ_SLIDER_MIN_VALUE;
                meterL31Top.progress = 0.0;
            } else {
                meterL31Top.progress = 0.0;
                meterL31Btm.progress = 0.0;
            }
            [viewController sendData:CMD_L_GEQ_20KHZ_DB commandType:DSP_WRITE dspId:DSP_8 value:(int)(sliderLGeq31.value)];
        }
        [self updatePreviewImage];
    }
}

- (void)reset {
    [self onBtnReset:nil];
}

@end
