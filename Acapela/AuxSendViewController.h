//
//  AuxSendViewController.h
//  eLive
//
//  Created by Kevin on 13/1/3.
//  Copyright (c) 2013年 Kevin Phua. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MHRotaryKnob.h"
#import "DACircularProgressView.h"
#import "acapela.h"
#import "JEProgressView.h"

enum AuxChannel {
    AUX_1 = 0,
    AUX_2,
    AUX_3,
    AUX_4,
    AUX_MAX
};

@interface AuxSendViewController : UIViewController<UITextFieldDelegate>
{
    enum AuxChannel currentAux;
    enum AuxChannel currentAuxLabel;
}

@property (nonatomic) enum AuxChannel currentAux;
@property (nonatomic) enum AuxChannel currentAuxLabel;

// Aux sends panel
@property (strong, nonatomic) IBOutlet UIButton *btnAuxSendPrePostAll;
@property (strong, nonatomic) IBOutlet UIView *viewAuxSend1;
@property (weak, nonatomic) IBOutlet UIButton *btnAuxSendPrePost1;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobAuxSend1;
@property (nonatomic, retain) DACircularProgressView *cpvAuxSend1;
@property (weak, nonatomic) IBOutlet UILabel *lblAuxSend1;
@property (weak, nonatomic) IBOutlet UILabel *lblAuxSendValue1;
@property (nonatomic,retain) JEProgressView *meterAuxSend1;
@property (strong, nonatomic) IBOutlet UIView *viewAuxSend2;
@property (weak, nonatomic) IBOutlet UIButton *btnAuxSendPrePost2;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobAuxSend2;
@property (nonatomic, retain) DACircularProgressView *cpvAuxSend2;
@property (weak, nonatomic) IBOutlet UILabel *lblAuxSend2;
@property (weak, nonatomic) IBOutlet UILabel *lblAuxSendValue2;
@property (nonatomic,retain) JEProgressView *meterAuxSend2;
@property (strong, nonatomic) IBOutlet UIView *viewAuxSend3;
@property (weak, nonatomic) IBOutlet UIButton *btnAuxSendPrePost3;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobAuxSend3;
@property (nonatomic, retain) DACircularProgressView *cpvAuxSend3;
@property (weak, nonatomic) IBOutlet UILabel *lblAuxSend3;
@property (weak, nonatomic) IBOutlet UILabel *lblAuxSendValue3;
@property (nonatomic,retain) JEProgressView *meterAuxSend3;
@property (strong, nonatomic) IBOutlet UIView *viewAuxSend4;
@property (weak, nonatomic) IBOutlet UIButton *btnAuxSendPrePost4;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobAuxSend4;
@property (nonatomic, retain) DACircularProgressView *cpvAuxSend4;
@property (weak, nonatomic) IBOutlet UILabel *lblAuxSend4;
@property (weak, nonatomic) IBOutlet UILabel *lblAuxSendValue4;
@property (nonatomic,retain) JEProgressView *meterAuxSend4;
@property (strong, nonatomic) IBOutlet UIView *viewAuxSend5;
@property (weak, nonatomic) IBOutlet UIButton *btnAuxSendPrePost5;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobAuxSend5;
@property (nonatomic, retain) DACircularProgressView *cpvAuxSend5;
@property (weak, nonatomic) IBOutlet UILabel *lblAuxSend5;
@property (weak, nonatomic) IBOutlet UILabel *lblAuxSendValue5;
@property (nonatomic,retain) JEProgressView *meterAuxSend5;
@property (strong, nonatomic) IBOutlet UIView *viewAuxSend6;
@property (weak, nonatomic) IBOutlet UIButton *btnAuxSendPrePost6;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobAuxSend6;
@property (nonatomic, retain) DACircularProgressView *cpvAuxSend6;
@property (weak, nonatomic) IBOutlet UILabel *lblAuxSend6;
@property (weak, nonatomic) IBOutlet UILabel *lblAuxSendValue6;
@property (nonatomic,retain) JEProgressView *meterAuxSend6;
@property (strong, nonatomic) IBOutlet UIView *viewAuxSend7;
@property (weak, nonatomic) IBOutlet UIButton *btnAuxSendPrePost7;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobAuxSend7;
@property (nonatomic, retain) DACircularProgressView *cpvAuxSend7;
@property (weak, nonatomic) IBOutlet UILabel *lblAuxSend7;
@property (weak, nonatomic) IBOutlet UILabel *lblAuxSendValue7;
@property (nonatomic,retain) JEProgressView *meterAuxSend7;
@property (strong, nonatomic) IBOutlet UIView *viewAuxSend8;
@property (weak, nonatomic) IBOutlet UIButton *btnAuxSendPrePost8;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobAuxSend8;
@property (nonatomic, retain) DACircularProgressView *cpvAuxSend8;
@property (weak, nonatomic) IBOutlet UILabel *lblAuxSend8;
@property (weak, nonatomic) IBOutlet UILabel *lblAuxSendValue8;
@property (nonatomic,retain) JEProgressView *meterAuxSend8;
@property (strong, nonatomic) IBOutlet UIView *viewAuxSend9;
@property (weak, nonatomic) IBOutlet UIButton *btnAuxSendPrePost9;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobAuxSend9;
@property (nonatomic, retain) DACircularProgressView *cpvAuxSend9;
@property (weak, nonatomic) IBOutlet UILabel *lblAuxSend9;
@property (weak, nonatomic) IBOutlet UILabel *lblAuxSendValue9;
@property (nonatomic,retain) JEProgressView *meterAuxSend9;
@property (strong, nonatomic) IBOutlet UIView *viewAuxSend10;
@property (weak, nonatomic) IBOutlet UIButton *btnAuxSendPrePost10;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobAuxSend10;
@property (nonatomic, retain) DACircularProgressView *cpvAuxSend10;
@property (weak, nonatomic) IBOutlet UILabel *lblAuxSend10;
@property (weak, nonatomic) IBOutlet UILabel *lblAuxSendValue10;
@property (nonatomic,retain) JEProgressView *meterAuxSend10;
@property (strong, nonatomic) IBOutlet UIView *viewAuxSend11;
@property (weak, nonatomic) IBOutlet UIButton *btnAuxSendPrePost11;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobAuxSend11;
@property (nonatomic, retain) DACircularProgressView *cpvAuxSend11;
@property (weak, nonatomic) IBOutlet UILabel *lblAuxSend11;
@property (weak, nonatomic) IBOutlet UILabel *lblAuxSendValue11;
@property (nonatomic,retain) JEProgressView *meterAuxSend11;
@property (strong, nonatomic) IBOutlet UIView *viewAuxSend12;
@property (weak, nonatomic) IBOutlet UIButton *btnAuxSendPrePost12;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobAuxSend12;
@property (nonatomic, retain) DACircularProgressView *cpvAuxSend12;
@property (weak, nonatomic) IBOutlet UILabel *lblAuxSend12;
@property (weak, nonatomic) IBOutlet UILabel *lblAuxSendValue12;
@property (nonatomic,retain) JEProgressView *meterAuxSend12;
@property (strong, nonatomic) IBOutlet UIView *viewAuxSend13;
@property (weak, nonatomic) IBOutlet UIButton *btnAuxSendPrePost13;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobAuxSend13;
@property (nonatomic, retain) DACircularProgressView *cpvAuxSend13;
@property (weak, nonatomic) IBOutlet UILabel *lblAuxSend13;
@property (weak, nonatomic) IBOutlet UILabel *lblAuxSendValue13;
@property (nonatomic,retain) JEProgressView *meterAuxSend13;
@property (strong, nonatomic) IBOutlet UIView *viewAuxSend14;
@property (weak, nonatomic) IBOutlet UIButton *btnAuxSendPrePost14;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobAuxSend14;
@property (nonatomic, retain) DACircularProgressView *cpvAuxSend14;
@property (weak, nonatomic) IBOutlet UILabel *lblAuxSend14;
@property (weak, nonatomic) IBOutlet UILabel *lblAuxSendValue14;
@property (nonatomic,retain) JEProgressView *meterAuxSend14;
@property (strong, nonatomic) IBOutlet UIView *viewAuxSend15;
@property (weak, nonatomic) IBOutlet UIButton *btnAuxSendPrePost15;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobAuxSend15;
@property (nonatomic, retain) DACircularProgressView *cpvAuxSend15;
@property (weak, nonatomic) IBOutlet UILabel *lblAuxSend15;
@property (weak, nonatomic) IBOutlet UILabel *lblAuxSendValue15;
@property (nonatomic,retain) JEProgressView *meterAuxSend15;
@property (strong, nonatomic) IBOutlet UIView *viewAuxSend16;
@property (weak, nonatomic) IBOutlet UIButton *btnAuxSendPrePost16;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobAuxSend16;
@property (nonatomic, retain) DACircularProgressView *cpvAuxSend16;
@property (weak, nonatomic) IBOutlet UILabel *lblAuxSend16;
@property (weak, nonatomic) IBOutlet UILabel *lblAuxSendValue16;
@property (nonatomic,retain) JEProgressView *meterAuxSend16;
@property (strong, nonatomic) IBOutlet UIView *viewAuxSend17;
@property (weak, nonatomic) IBOutlet UIButton *btnAuxSendPrePost17;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobAuxSend17;
@property (nonatomic, retain) DACircularProgressView *cpvAuxSend17;
@property (weak, nonatomic) IBOutlet UILabel *lblAuxSend17;
@property (weak, nonatomic) IBOutlet UILabel *lblAuxSendValue17;
@property (nonatomic,retain) JEProgressView *meterAuxSend17;
@property (strong, nonatomic) IBOutlet UIView *viewAuxSend18;
@property (weak, nonatomic) IBOutlet UIButton *btnAuxSendPrePost18;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobAuxSend18;
@property (nonatomic, retain) DACircularProgressView *cpvAuxSend18;
@property (weak, nonatomic) IBOutlet UILabel *lblAuxSend18;
@property (weak, nonatomic) IBOutlet UILabel *lblAuxSendValue18;
@property (nonatomic,retain) JEProgressView *meterAuxSend18;
@property (strong, nonatomic) IBOutlet UIButton *btnAuxSendSolo;
@property (strong, nonatomic) IBOutlet UIButton *btnAuxSendOn;
@property (strong, nonatomic) IBOutlet UIButton *btnAuxSelectDown;
@property (strong, nonatomic) IBOutlet UIButton *btnAuxSelectUp;
@property (weak, nonatomic) IBOutlet UIButton *btnAuxMeter;
@property (weak, nonatomic) IBOutlet UITextField *txtCurrentAux;
@property (strong, nonatomic) IBOutlet UILabel *lblCurrentAux;
@property (nonatomic,retain) UISlider *sliderAuxSendMain;
@property (nonatomic,retain) JEProgressView *meterAuxSendMain;
@property (weak, nonatomic) IBOutlet UILabel *lblSliderAuxMainValue;
@property (strong, nonatomic) IBOutlet UIButton *btnAuxMulti1;
@property (strong, nonatomic) IBOutlet UIButton *btnAuxMulti2;
@property (strong, nonatomic) IBOutlet UIButton *btnAuxMulti3;
@property (strong, nonatomic) IBOutlet UIButton *btnAuxMulti4;

- (IBAction)onBtnAuxSendPrePostAll:(id)sender;
- (IBAction)onBtnAuxSend1:(id)sender;
- (IBAction)onBtnAuxSend2:(id)sender;
- (IBAction)onBtnAuxSend3:(id)sender;
- (IBAction)onBtnAuxSend4:(id)sender;
- (IBAction)onBtnAuxSend5:(id)sender;
- (IBAction)onBtnAuxSend6:(id)sender;
- (IBAction)onBtnAuxSend7:(id)sender;
- (IBAction)onBtnAuxSend8:(id)sender;
- (IBAction)onBtnAuxSend9:(id)sender;
- (IBAction)onBtnAuxSend10:(id)sender;
- (IBAction)onBtnAuxSend11:(id)sender;
- (IBAction)onBtnAuxSend12:(id)sender;
- (IBAction)onBtnAuxSend13:(id)sender;
- (IBAction)onBtnAuxSend14:(id)sender;
- (IBAction)onBtnAuxSend15:(id)sender;
- (IBAction)onBtnAuxSend16:(id)sender;
- (IBAction)onBtnAuxSendSolo:(id)sender;
- (IBAction)onBtnAuxSendOn:(id)sender;
- (IBAction)onBtnAuxSelectDown:(id)sender;
- (IBAction)onBtnAuxSelectUp:(id)sender;
- (IBAction)onBtnAuxMeter:(id)sender;

- (void)processReply:(AuxPagePacket *)pkt;
- (void)processMeter:(AllMeterValueRxPacket *)meterValue;
- (void)updateCurrentAuxLabel;

@end
