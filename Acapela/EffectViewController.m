//
//  EffectViewController.m
//  eLive
//
//  Created by Kevin on 13/1/3.
//  Copyright (c) 2013年 Kevin Phua. All rights reserved.
//

#import "EffectViewController.h"
#import "AppDelegate.h"
#import "ViewController.h"
#import "Global.h"

#define LFO_FREQ_MAX                100
#define LFO_FREQ_MIN                1
#define LFO_FREQ_DEFAULT            100

#define LFO_PHASE_MAX               180
#define LFO_PHASE_MIN               0
#define LFO_PHASE_DEFAULT           90

#define DEPTH_MAX                   100
#define DEPTH_MIN                   0
#define DEPTH_DEFAULT               50

#define PRE_DELAY_MAX               100
#define PRE_DELAY_MIN               0
#define PRE_DELAY_DEFAULT           10

#define LPF_FREQ_MAX                60  // 20kHz
#define LPF_FREQ_MIN                0  // 20Hz
#define LPF_FREQ_DEFAULT            50  // 6.3kHz

#define HPF_FREQ_MAX                60  // 20kHz
#define HPF_FREQ_MIN                0   // 20Hz
#define HPF_FREQ_DEFAULT            50  // 6.3kHz

#define REVERB_EARLY_OUT_MAX        100
#define REVERB_EARLY_OUT_MIN        0
#define REVERB_EARLY_OUT_DEFAULT    100
#define REVERB_TIME_MAX             200
#define REVERB_TIME_MIN             1
#define REVERB_TIME_DEFAULT         56
#define REVERB_PRE_DELAY_MAX        200
#define REVERB_PRE_DELAY_MIN        0
#define REVERB_PRE_DELAY_DEFAULT    40
#define REVERB_HIRATIO_MAX          100
#define REVERB_HIRATIO_MIN          0
#define REVERB_HIRATIO_DEFAULT      65
#define REVERB_DENSITY_MAX          100
#define REVERB_DENSITY_MIN          0
#define REVERB_DENSITY_DEFAULT      90
#define REVERB_LEVEL_MAX            100
#define REVERB_LEVEL_MIN            0
#define REVERB_LEVEL_DEFAULT        100
#define REVERB_GATE_THRESHOLD_MAX   70      // -70dB
#define REVERB_GATE_THRESHOLD_MIN   0
#define REVERB_GATE_THRESHOLD_DEFAULT   20  // -20dB
#define REVERB_GATE_HOLD_MAX        39
#define REVERB_GATE_HOLD_MIN        0
#define REVERB_GATE_HOLD_DEFAULT    10

#define ECHO_DELAY_MAX              680
#define ECHO_DELAY_MIN              1
#define ECHO_DELAY_DEFAULT          20
#define ECHO_DELAY_FB_MAX           99
#define ECHO_DELAY_FB_MIN           0
#define ECHO_DELAY_FB_DEFAULT       50

#define CHORUS_PHASE_MAX            180
#define CHORUS_PHASE_MIN            0
#define CHORUS_PHASE_DEFAULT        0
#define CHORUS_DEPTH_MAX            100
#define CHORUS_DEPTH_MIN            0
#define CHORUS_DEPTH_DEFAULT        100
#define CHORUS_PRE_DELAY_MAX        100
#define CHORUS_PRE_DELAY_MIN        0
#define CHORUS_PRE_DELAY_DEFAULT    10
#define CHORUS_LFO_FREQ_MAX         2000
#define CHORUS_LFO_FREQ_MIN         1
#define CHORUS_LFO_FREQ_DEFAULT     30

#define PHASER_STAGENO_MAX          3
#define PHASER_STAGENO_MIN          0
#define PHASER_STAGENO_DEFAULT      2

#define VIBRATO_FREQ_MAX            60  // 20kHz
#define VIBRATO_FREQ_MIN            0   // 20Hz
#define VIBRATO_FREQ_DEFAULT        34  // 1kHz

#define AUTOPAN_WAY_MAX             2
#define AUTOPAN_WAY_MIN             0
#define AUTOPAN_WAY_DEFAULT         1

#define GEQ31_VIEW_TAG              101
#define GEQ15_VIEW_TAG              102

@interface EffectViewController ()

@end

@implementation EffectViewController
{
    ViewController* viewController;
    EFFECT_NUM effectNum;
    EFFECT_TYPE effectType;
    REVERB_GROUP reverbGroup;
    REVERB_TYPE reverbRoom;
    REVERB_TYPE reverbHall;
    REVERB_TYPE reverbPlate;
    EFFECT_IN   effectInL, effectInR;
    int         effect1InL, effect1InR, effect2InL, effect2InR;
    int         effect1OutL, effect1OutR, effect2OutL, effect2OutR;
    int         effect1Program, effect2Program;

    // Tap delay led flashing timer
    int         _tapDelay;
    BOOL        _ledOnOff;
    NSTimer     *_tapDelayTimer;
}

@synthesize imgBackground,btnEffectInL,btnEffectInR,btnEffect,btnReverbType;
@synthesize sliderEffectDryWet,lblEffectDryWet,btnFile;
@synthesize viewEffectInL,btnEffectInLCh1,btnEffectInLCh2,btnEffectInLCh3,btnEffectInLCh4;
@synthesize btnEffectInLCh5,btnEffectInLCh6,btnEffectInLCh7,btnEffectInLCh8,btnEffectInLCh9;
@synthesize btnEffectInLCh10,btnEffectInLCh11,btnEffectInLCh12,btnEffectInLCh13,btnEffectInLCh14;
@synthesize btnEffectInLCh15,btnEffectInLCh16,btnEffectInLCh17,btnEffectInLCh18;
@synthesize btnEffectInLGp1,btnEffectInLGp2,btnEffectInLGp3,btnEffectInLGp4;
@synthesize btnEffectInLAux1,btnEffectInLAux2,btnEffectInLAux3,btnEffectInLAux4,btnEffectInLNull;

@synthesize viewEffectInR,btnEffectInRCh1,btnEffectInRCh2,btnEffectInRCh3,btnEffectInRCh4;
@synthesize btnEffectInRCh5,btnEffectInRCh6,btnEffectInRCh7,btnEffectInRCh8,btnEffectInRCh9;
@synthesize btnEffectInRCh10,btnEffectInRCh11,btnEffectInRCh12,btnEffectInRCh13,btnEffectInRCh14;
@synthesize btnEffectInRCh15,btnEffectInRCh16,btnEffectInRCh17,btnEffectInRCh18;
@synthesize btnEffectInRGp1,btnEffectInRGp2,btnEffectInRGp3,btnEffectInRGp4;
@synthesize btnEffectInRAux1,btnEffectInRAux2,btnEffectInRAux3,btnEffectInRAux4,btnEffectInRNull;
@synthesize viewEffect1,btnEffect1ReverbRoom,btnEffect1ReverbHall,btnEffect1ReverbPlate;
@synthesize btnEffect1Echo,btnEffect1TapDelay,btnEffect1Chorus,btnEffect1Flanger,btnEffect1Phaser;
@synthesize btnEffect1Tremolo,btnEffect1Vibrato,btnEffect1AutoPan,btnEffect1Geq31;
@synthesize viewEffect2,btnEffect2Echo,btnEffect2TapDelay,btnEffect2Chorus,btnEffect2Flanger,btnEffect2Phaser;
@synthesize btnEffect2Tremolo,btnEffect2Vibrato,btnEffect2AutoPan,btnEffect2Geq15;
@synthesize btnEffectOutLMulti1Status,btnEffectOutLMulti3Status;
@synthesize btnEffectOutLMainLStatus,btnEffectOutLGp1Status,btnEffectOutLGp3Status;
@synthesize btnEffectOutRMulti2Status,btnEffectOutRMulti4Status;
@synthesize btnEffectOutRMainRStatus,btnEffectOutRGp2Status,btnEffectOutRGp4Status;
@synthesize viewReverbType,btnReverbType1,btnReverbType2,btnReverbType3,btnReverbType4;
@synthesize btnReverbType5,btnReverbType6,btnReverbType7,btnReverbType8;
@synthesize meterInL,meterInR,meterOutL,meterOutR;
@synthesize viewReverb,knobHpf,cpvHpf,lblHpf,knobLpf,cpvLpf,lblLpf;
@synthesize knobPreDelay,cpvPreDelay,lblPreDelay,knobEarlyOut,cpvEarlyOut,lblEarlyOut;
@synthesize knobReverbTime,cpvReverbTime,lblReverbTime,knobLevel,cpvLevel,lblLevel;
@synthesize knobHiRatio,cpvHiRatio,lblHiRatio,knobDensity,cpvDensity,lblDensity;
@synthesize knobThreshold,cpvThreshold,lblThreshold,knobHold,cpvHold,lblHold,btnGate;
@synthesize viewEcho,knobEchoTimeL,cpvEchoTimeL,lblEchoTimeL,knobEchoTimeR,cpvEchoTimeR,lblEchoTimeR;
@synthesize knobEchoFb1,cpvEchoFb1,lblEchoFb1,knobEchoFb2,cpvEchoFb2,lblEchoFb2;
@synthesize knobEchoHpf,cpvEchoHpf,lblEchoHpf,knobEchoLpf,cpvEchoLpf,lblEchoLpf;
@synthesize viewTapDelay,knobTapFb,cpvTapFb,lblTapFb,knobTapHpf,cpvTapHpf,lblTapHpf;
@synthesize knobTapLpf,cpvTapLpf,lblTapLpf,lblTapDelay,btnTap,imgTapLed;
@synthesize viewChorus,knobChorusPreDelay,cpvChorusPreDelay,lblChorusPreDelay;
@synthesize knobChorusDepth,cpvChorusDepth,lblChorusDepth,knobChorusLpf,cpvChorusLpf,lblChorusLpf;
@synthesize knobChorusLfoPhase,cpvChorusLfoPhase,lblChorusLfoPhase;
@synthesize knobChorusLfoFreq,cpvChorusLfoFreq,lblChorusLfoFreq,btnChorusLfoType;
@synthesize viewFlanger,knobFlangerPreDelay,cpvFlangerPreDelay,lblFlangerPreDelay;
@synthesize knobFlangerDepth,cpvFlangerDepth,lblFlangerDepth,knobFlangerLpf,cpvFlangerLpf,lblFlangerLpf;
@synthesize knobFlangerFb,cpvFlangerFb,lblFlangerFb,knobFlangerLfoPhase,cpvFlangerLfoPhase,lblFlangerLfoPhase;
@synthesize knobFlangerLfoFreq,cpvFlangerLfoFreq,lblFlangerLfoFreq,btnFlangerLfoType;
@synthesize viewPhaser,knobPhaserStageNo,cpvPhaserStageNo,lblPhaserStageNo;
@synthesize knobPhaserFreq,cpvPhaserFreq,lblPhaserFreq,knobPhaserDepth,cpvPhaserDepth,lblPhaserDepth;
@synthesize knobPhaserLfoFreq,cpvPhaserLfoFreq,lblPhaserLfoFreq,btnPhaserLfoType;
@synthesize viewTremolo,knobTremoloDepth,cpvTremoloDepth,lblTremoloDepth;
@synthesize knobTremoloLfoFreq,cpvTremoloLfoFreq,lblTremoloLfoFreq,btnTremoloLfoType;
@synthesize viewVibrato,knobVibratoFreq,cpvVibratoFreq,lblVibratoFreq;
@synthesize knobVibratoDepth,cpvVibratoDepth,lblVibratoDepth;
@synthesize knobVibratoLfoFreq,cpvVibratoLfoFreq,lblVibratoLfoFreq,btnVibratoLfoType;
@synthesize viewAutoPan,knobAutoPanWay,cpvAutoPanWay,lblAutoPanWay,knobAutoPanDepth;
@synthesize cpvAutoPanDepth,lblAutoPanDepth,knobAutoPanLfoFreq,cpvAutoPanLfoFreq;
@synthesize lblAutoPanLfoFreq,btnAutoPanLfoType;
@synthesize viewGeq31, viewGeq15;
@synthesize effectGeq31Controller, imgGeq31LPreview, imgGeq31RPreview;
@synthesize effectGeq15Controller, imgGeq15LPreview, imgGeq15RPreview;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    viewController = (ViewController *)appDelegate.window.rootViewController;
    
    // Do any additional setup after loading the view from its nib.
    [btnEffect.titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
    [btnEffect.titleLabel setNumberOfLines:0];
    [btnEffect.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [btnReverbType.titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
    [btnReverbType.titleLabel setNumberOfLines:0];
    [btnReverbType.titleLabel setTextAlignment:NSTextAlignmentCenter];
    
    // Init button states
    btnEffectInLNull.selected = YES;
    btnEffectInRNull.selected = YES;
    btnEffect1ReverbRoom.selected = YES;
    btnReverbType1.selected = YES;
    
    // Init values
    effectType = EFFECT_REVERB;
    reverbGroup = REVERB_ROOM;
    reverbRoom = ROOM_LARGE;
    reverbHall = HALL_LARGE;
    reverbPlate = PLATE_LARGE;
    effectInL = effectInR = EFFECT_IN_NULL;
    [viewReverb setHidden:NO];
    
    // Add slider meters
    UIImage *progressImage = [[UIImage imageNamed:@"meter-6.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0,0,0,0)];
    
    meterInL = [[JEProgressView alloc] initWithFrame:CGRectMake(561, 150, 140, 8)];
    meterInL.transform = CGAffineTransformRotate(meterInL.transform, 270.0/180*M_PI);
    [meterInL setTintColor:[UIColor clearColor]];
    [meterInL setProgressImage:progressImage];
    [meterInL setTrackImage:[UIImage alloc]];
    [meterInL setProgress:0.0];
    [self.view addSubview:meterInL];

    meterInR = [[JEProgressView alloc] initWithFrame:CGRectMake(591, 150, 140, 8)];
    meterInR.transform = CGAffineTransformRotate(meterInR.transform, 270.0/180*M_PI);
    [meterInR setTintColor:[UIColor clearColor]];
    [meterInR setProgressImage:progressImage];
    [meterInR setTrackImage:[UIImage alloc]];
    [meterInR setProgress:0.0];
    [self.view addSubview:meterInR];

    meterOutL = [[JEProgressView alloc] initWithFrame:CGRectMake(624, 150, 140, 8)];
    meterOutL.transform = CGAffineTransformRotate(meterOutL.transform, 270.0/180*M_PI);
    [meterOutL setTintColor:[UIColor clearColor]];
    [meterOutL setProgressImage:progressImage];
    [meterOutL setTrackImage:[UIImage alloc]];
    [meterOutL setProgress:0.0];
    [self.view addSubview:meterOutL];

    meterOutR = [[JEProgressView alloc] initWithFrame:CGRectMake(654, 150, 140, 8)];
    meterOutR.transform = CGAffineTransformRotate(meterOutR.transform, 270.0/180*M_PI);
    [meterOutR setTintColor:[UIColor clearColor]];
    [meterOutR setProgressImage:progressImage];
    [meterOutR setTrackImage:[UIImage alloc]];
    [meterOutR setProgress:0.0];
    [self.view addSubview:meterOutR];
    
    // Reverb view init
    knobHpf.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobHpf.scalingFactor = 2.0f;
	knobHpf.maximumValue = HPF_FREQ_MAX;
	knobHpf.minimumValue = HPF_FREQ_MIN;
	knobHpf.value = HPF_FREQ_DEFAULT;
	knobHpf.defaultValue = knobHpf.value;
	knobHpf.resetsToDefault = YES;
	knobHpf.backgroundColor = [UIColor clearColor];
	[knobHpf setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobHpf.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobHpf addTarget:self action:@selector(knobHpfDidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvHpf = [[DACircularProgressView alloc] initWithFrame:CGRectMake(31.0f, 47.0f, 63.0f, 63.0f)];
    [viewReverb addSubview:cpvHpf];
    [viewReverb bringSubviewToFront:knobHpf];
    
    knobLpf.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobLpf.scalingFactor = 2.0f;
	knobLpf.maximumValue = LPF_FREQ_MAX;
	knobLpf.minimumValue = LPF_FREQ_MIN;
	knobLpf.value = LPF_FREQ_DEFAULT;
	knobLpf.defaultValue = knobLpf.value;
	knobLpf.resetsToDefault = YES;
	knobLpf.backgroundColor = [UIColor clearColor];
	[knobLpf setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobLpf.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobLpf addTarget:self action:@selector(knobLpfDidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvLpf = [[DACircularProgressView alloc] initWithFrame:CGRectMake(171.0f, 47.0f, 63.0f, 63.0f)];
    [viewReverb addSubview:cpvLpf];
    [viewReverb bringSubviewToFront:knobLpf];
    
    knobPreDelay.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobPreDelay.scalingFactor = 2.0f;
	knobPreDelay.maximumValue = REVERB_PRE_DELAY_MAX;
	knobPreDelay.minimumValue = REVERB_PRE_DELAY_MIN;
	knobPreDelay.value = REVERB_PRE_DELAY_DEFAULT;
	knobPreDelay.defaultValue = knobPreDelay.value;
	knobPreDelay.resetsToDefault = YES;
	knobPreDelay.backgroundColor = [UIColor clearColor];
	[knobPreDelay setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobPreDelay.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobPreDelay addTarget:self action:@selector(knobPreDelayDidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvPreDelay = [[DACircularProgressView alloc] initWithFrame:CGRectMake(310.0f, 47.0f, 63.0f, 63.0f)];
    [viewReverb addSubview:cpvPreDelay];
    [viewReverb bringSubviewToFront:knobPreDelay];

    knobEarlyOut.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobEarlyOut.scalingFactor = 2.0f;
	knobEarlyOut.maximumValue = REVERB_EARLY_OUT_MAX;
	knobEarlyOut.minimumValue = REVERB_EARLY_OUT_MIN;
	knobEarlyOut.value = REVERB_EARLY_OUT_DEFAULT;
	knobEarlyOut.defaultValue = knobEarlyOut.value;
	knobEarlyOut.resetsToDefault = YES;
	knobEarlyOut.backgroundColor = [UIColor clearColor];
	[knobEarlyOut setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobEarlyOut.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobEarlyOut addTarget:self action:@selector(knobEarlyOutDidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvEarlyOut = [[DACircularProgressView alloc] initWithFrame:CGRectMake(447.0f, 47.0f, 63.0f, 63.0f)];
    [viewReverb addSubview:cpvEarlyOut];
    [viewReverb bringSubviewToFront:knobEarlyOut];

    knobReverbTime.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobReverbTime.scalingFactor = 2.0f;
	knobReverbTime.maximumValue = REVERB_TIME_MAX;
	knobReverbTime.minimumValue = REVERB_TIME_MIN;
	knobReverbTime.value = REVERB_TIME_DEFAULT;
	knobReverbTime.defaultValue = knobReverbTime.value;
	knobReverbTime.resetsToDefault = YES;
	knobReverbTime.backgroundColor = [UIColor clearColor];
	[knobReverbTime setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobReverbTime.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobReverbTime addTarget:self action:@selector(knobReverbTimeDidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvReverbTime = [[DACircularProgressView alloc] initWithFrame:CGRectMake(587.0f, 47.0f, 63.0f, 63.0f)];
    [viewReverb addSubview:cpvReverbTime];
    [viewReverb bringSubviewToFront:knobReverbTime];
    
    knobLevel.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobLevel.scalingFactor = 2.0f;
	knobLevel.maximumValue = REVERB_LEVEL_MAX;
	knobLevel.minimumValue = REVERB_LEVEL_MIN;
	knobLevel.value = REVERB_LEVEL_DEFAULT;
	knobLevel.defaultValue = knobLevel.value;
	knobLevel.resetsToDefault = YES;
	knobLevel.backgroundColor = [UIColor clearColor];
	[knobLevel setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobLevel.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobLevel addTarget:self action:@selector(knobLevelDidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvLevel = [[DACircularProgressView alloc] initWithFrame:CGRectMake(31.0f, 156.0f, 63.0f, 63.0f)];
    [viewReverb addSubview:cpvLevel];
    [viewReverb bringSubviewToFront:knobLevel];

    knobHiRatio.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobHiRatio.scalingFactor = 2.0f;
	knobHiRatio.maximumValue = REVERB_HIRATIO_MAX;
	knobHiRatio.minimumValue = REVERB_HIRATIO_MIN;
	knobHiRatio.value = REVERB_HIRATIO_DEFAULT;
	knobHiRatio.defaultValue = knobHiRatio.value;
	knobHiRatio.resetsToDefault = YES;
	knobHiRatio.backgroundColor = [UIColor clearColor];
	[knobHiRatio setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobHiRatio.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobHiRatio addTarget:self action:@selector(knobHiRatioDidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvHiRatio = [[DACircularProgressView alloc] initWithFrame:CGRectMake(171.0f, 156.0f, 63.0f, 63.0f)];
    [viewReverb addSubview:cpvHiRatio];
    [viewReverb bringSubviewToFront:knobHiRatio];

    knobDensity.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobDensity.scalingFactor = 2.0f;
	knobDensity.maximumValue = REVERB_DENSITY_MAX;
	knobDensity.minimumValue = REVERB_DENSITY_MIN;
	knobDensity.value = REVERB_DENSITY_DEFAULT;
	knobDensity.defaultValue = knobDensity.value;
	knobDensity.resetsToDefault = YES;
	knobDensity.backgroundColor = [UIColor clearColor];
	[knobDensity setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobDensity.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobDensity addTarget:self action:@selector(knobDensityDidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvDensity = [[DACircularProgressView alloc] initWithFrame:CGRectMake(310.0f, 156.0f, 63.0f, 63.0f)];
    [viewReverb addSubview:cpvDensity];
    [viewReverb bringSubviewToFront:knobDensity];

    knobThreshold.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobThreshold.scalingFactor = 2.0f;
	knobThreshold.maximumValue = REVERB_GATE_THRESHOLD_MAX;
	knobThreshold.minimumValue = REVERB_GATE_THRESHOLD_MIN;
	knobThreshold.value = REVERB_GATE_THRESHOLD_DEFAULT;
	knobThreshold.defaultValue = knobThreshold.value;
	knobThreshold.resetsToDefault = YES;
	knobThreshold.backgroundColor = [UIColor clearColor];
	[knobThreshold setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobThreshold.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobThreshold addTarget:self action:@selector(knobThresholdDidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvThreshold = [[DACircularProgressView alloc] initWithFrame:CGRectMake(447.0f, 156.0f, 63.0f, 63.0f)];
    [viewReverb addSubview:cpvThreshold];
    [viewReverb bringSubviewToFront:knobThreshold];

    knobHold.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobHold.scalingFactor = 2.0f;
	knobHold.maximumValue = REVERB_GATE_HOLD_MAX;
	knobHold.minimumValue = REVERB_GATE_HOLD_MIN;
	knobHold.value = REVERB_GATE_HOLD_DEFAULT;
	knobHold.defaultValue = knobHold.value;
	knobHold.resetsToDefault = YES;
	knobHold.backgroundColor = [UIColor clearColor];
	[knobHold setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobHold.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobHold addTarget:self action:@selector(knobHoldDidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvHold = [[DACircularProgressView alloc] initWithFrame:CGRectMake(587.0f, 156.0f, 63.0f, 63.0f)];
    [viewReverb addSubview:cpvHold];
    [viewReverb bringSubviewToFront:knobHold];
    
    // Echo view init
    knobEchoTimeL.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobEchoTimeL.scalingFactor = 2.0f;
	knobEchoTimeL.maximumValue = ECHO_DELAY_MAX;
	knobEchoTimeL.minimumValue = ECHO_DELAY_MIN;
	knobEchoTimeL.value = ECHO_DELAY_DEFAULT;
	knobEchoTimeL.defaultValue = knobEchoTimeL.value;
	knobEchoTimeL.resetsToDefault = YES;
	knobEchoTimeL.backgroundColor = [UIColor clearColor];
	[knobEchoTimeL setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobEchoTimeL.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobEchoTimeL addTarget:self action:@selector(knobEchoTimeLDidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvEchoTimeL = [[DACircularProgressView alloc] initWithFrame:CGRectMake(95.0f, 48.0f, 63.0f, 63.0f)];
    [viewEcho addSubview:cpvEchoTimeL];
    [viewEcho bringSubviewToFront:knobEchoTimeL];

    knobEchoTimeR.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobEchoTimeR.scalingFactor = 2.0f;
	knobEchoTimeR.maximumValue = ECHO_DELAY_MAX;
	knobEchoTimeR.minimumValue = ECHO_DELAY_MIN;
	knobEchoTimeR.value = ECHO_DELAY_DEFAULT;
	knobEchoTimeR.defaultValue = knobEchoTimeR.value;
	knobEchoTimeR.resetsToDefault = YES;
	knobEchoTimeR.backgroundColor = [UIColor clearColor];
	[knobEchoTimeR setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobEchoTimeR.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobEchoTimeR addTarget:self action:@selector(knobEchoTimeRDidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvEchoTimeR = [[DACircularProgressView alloc] initWithFrame:CGRectMake(235.0f, 48.0f, 63.0f, 63.0f)];
    [viewEcho addSubview:cpvEchoTimeR];
    [viewEcho bringSubviewToFront:knobEchoTimeR];
    
    knobEchoFb1.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobEchoFb1.scalingFactor = 2.0f;
	knobEchoFb1.maximumValue = ECHO_DELAY_FB_MAX;
	knobEchoFb1.minimumValue = ECHO_DELAY_FB_MIN;
	knobEchoFb1.value = ECHO_DELAY_FB_DEFAULT;
	knobEchoFb1.defaultValue = knobEchoFb1.value;
	knobEchoFb1.resetsToDefault = YES;
	knobEchoFb1.backgroundColor = [UIColor clearColor];
	[knobEchoFb1 setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobEchoFb1.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobEchoFb1 addTarget:self action:@selector(knobEchoFb1DidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvEchoFb1 = [[DACircularProgressView alloc] initWithFrame:CGRectMake(95.0f, 157.0f, 63.0f, 63.0f)];
    [viewEcho addSubview:cpvEchoFb1];
    [viewEcho bringSubviewToFront:knobEchoFb1];

    knobEchoFb2.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobEchoFb2.scalingFactor = 2.0f;
	knobEchoFb2.maximumValue = ECHO_DELAY_FB_MAX;
	knobEchoFb2.minimumValue = ECHO_DELAY_FB_MIN;
	knobEchoFb2.value = ECHO_DELAY_FB_DEFAULT;
	knobEchoFb2.defaultValue = knobEchoFb2.value;
	knobEchoFb2.resetsToDefault = YES;
	knobEchoFb2.backgroundColor = [UIColor clearColor];
	[knobEchoFb2 setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobEchoFb2.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobEchoFb2 addTarget:self action:@selector(knobEchoFb2DidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvEchoFb2 = [[DACircularProgressView alloc] initWithFrame:CGRectMake(235.0f, 157.0f, 63.0f, 63.0f)];
    [viewEcho addSubview:cpvEchoFb2];
    [viewEcho bringSubviewToFront:knobEchoFb2];
    
    knobEchoHpf.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobEchoHpf.scalingFactor = 2.0f;
	knobEchoHpf.maximumValue = HPF_FREQ_MAX;
	knobEchoHpf.minimumValue = HPF_FREQ_MIN;
	knobEchoHpf.value = HPF_FREQ_DEFAULT;
	knobEchoHpf.defaultValue = knobEchoHpf.value;
	knobEchoHpf.resetsToDefault = YES;
	knobEchoHpf.backgroundColor = [UIColor clearColor];
	[knobEchoHpf setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobEchoHpf.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobEchoHpf addTarget:self action:@selector(knobEchoHpfDidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvEchoHpf = [[DACircularProgressView alloc] initWithFrame:CGRectMake(375.0f, 157.0f, 63.0f, 63.0f)];
    [viewEcho addSubview:cpvEchoHpf];
    [viewEcho bringSubviewToFront:knobEchoHpf];

    knobEchoLpf.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobEchoLpf.scalingFactor = 2.0f;
	knobEchoLpf.maximumValue = LPF_FREQ_MAX;
	knobEchoLpf.minimumValue = LPF_FREQ_MIN;
	knobEchoLpf.value = LPF_FREQ_DEFAULT;
	knobEchoLpf.defaultValue = knobEchoLpf.value;
	knobEchoLpf.resetsToDefault = YES;
	knobEchoLpf.backgroundColor = [UIColor clearColor];
	[knobEchoLpf setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobEchoLpf.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobEchoLpf addTarget:self action:@selector(knobEchoLpfDidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvEchoLpf = [[DACircularProgressView alloc] initWithFrame:CGRectMake(515.0f, 157.0f, 63.0f, 63.0f)];
    [viewEcho addSubview:cpvEchoLpf];
    [viewEcho bringSubviewToFront:knobEchoLpf];
    
    // Tap delay view init
    knobTapFb.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobTapFb.scalingFactor = 2.0f;
	knobTapFb.maximumValue = ECHO_DELAY_FB_MAX;
	knobTapFb.minimumValue = ECHO_DELAY_FB_MIN;
	knobTapFb.value = ECHO_DELAY_FB_DEFAULT;
	knobTapFb.defaultValue = knobTapFb.value;
	knobTapFb.resetsToDefault = YES;
	knobTapFb.backgroundColor = [UIColor clearColor];
	[knobTapFb setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobTapFb.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobTapFb addTarget:self action:@selector(knobTapFbDidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvTapFb = [[DACircularProgressView alloc] initWithFrame:CGRectMake(83.0f, 97.0f, 63.0f, 63.0f)];
    [viewTapDelay addSubview:cpvTapFb];
    [viewTapDelay bringSubviewToFront:knobTapFb];
    
    knobTapHpf.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobTapHpf.scalingFactor = 2.0f;
	knobTapHpf.maximumValue = HPF_FREQ_MAX;
	knobTapHpf.minimumValue = HPF_FREQ_MIN;
	knobTapHpf.value = HPF_FREQ_DEFAULT;
	knobTapHpf.defaultValue = knobTapHpf.value;
	knobTapHpf.resetsToDefault = YES;
	knobTapHpf.backgroundColor = [UIColor clearColor];
	[knobTapHpf setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobTapHpf.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobTapHpf addTarget:self action:@selector(knobTapHpfDidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvTapHpf = [[DACircularProgressView alloc] initWithFrame:CGRectMake(223.0f, 97.0f, 63.0f, 63.0f)];
    [viewTapDelay addSubview:cpvTapHpf];
    [viewTapDelay bringSubviewToFront:knobTapHpf];
    
    knobTapLpf.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobTapLpf.scalingFactor = 2.0f;
	knobTapLpf.maximumValue = LPF_FREQ_MAX;
	knobTapLpf.minimumValue = LPF_FREQ_MIN;
	knobTapLpf.value = LPF_FREQ_DEFAULT;
	knobTapLpf.defaultValue = knobTapLpf.value;
	knobTapLpf.resetsToDefault = YES;
	knobTapLpf.backgroundColor = [UIColor clearColor];
	[knobTapLpf setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobTapLpf.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobTapLpf addTarget:self action:@selector(knobTapLpfDidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvTapLpf = [[DACircularProgressView alloc] initWithFrame:CGRectMake(363.0f, 97.0f, 63.0f, 63.0f)];
    [viewTapDelay addSubview:cpvTapLpf];
    [viewTapDelay bringSubviewToFront:knobTapLpf];
    
    // Chorus view init
    knobChorusPreDelay.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobChorusPreDelay.scalingFactor = 2.0f;
	knobChorusPreDelay.maximumValue = CHORUS_PRE_DELAY_MAX;
	knobChorusPreDelay.minimumValue = CHORUS_PRE_DELAY_MIN;
	knobChorusPreDelay.value = CHORUS_PRE_DELAY_DEFAULT;
	knobChorusPreDelay.defaultValue = knobChorusPreDelay.value;
	knobChorusPreDelay.resetsToDefault = YES;
	knobChorusPreDelay.backgroundColor = [UIColor clearColor];
	[knobChorusPreDelay setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobChorusPreDelay.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobChorusPreDelay addTarget:self action:@selector(knobChorusPreDelayDidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvChorusPreDelay = [[DACircularProgressView alloc] initWithFrame:CGRectMake(169.0f, 47.0f, 63.0f, 63.0f)];
    [viewChorus addSubview:cpvChorusPreDelay];
    [viewChorus bringSubviewToFront:knobChorusPreDelay];

    knobChorusDepth.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobChorusDepth.scalingFactor = 2.0f;
	knobChorusDepth.maximumValue = CHORUS_DEPTH_MAX;
	knobChorusDepth.minimumValue = CHORUS_DEPTH_MIN;
	knobChorusDepth.value = CHORUS_DEPTH_DEFAULT;
	knobChorusDepth.defaultValue = knobChorusDepth.value;
	knobChorusDepth.resetsToDefault = YES;
	knobChorusDepth.backgroundColor = [UIColor clearColor];
	[knobChorusDepth setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobChorusDepth.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobChorusDepth addTarget:self action:@selector(knobChorusDepthDidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvChorusDepth = [[DACircularProgressView alloc] initWithFrame:CGRectMake(310.0f, 47.0f, 63.0f, 63.0f)];
    [viewChorus addSubview:cpvChorusDepth];
    [viewChorus bringSubviewToFront:knobChorusDepth];

    knobChorusLpf.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobChorusLpf.scalingFactor = 2.0f;
	knobChorusLpf.maximumValue = LPF_FREQ_MAX;
	knobChorusLpf.minimumValue = LPF_FREQ_MIN;
	knobChorusLpf.value = LPF_FREQ_DEFAULT;
	knobChorusLpf.defaultValue = knobChorusLpf.value;
	knobChorusLpf.resetsToDefault = YES;
	knobChorusLpf.backgroundColor = [UIColor clearColor];
	[knobChorusLpf setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobChorusLpf.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobChorusLpf addTarget:self action:@selector(knobChorusLpfDidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvChorusLpf = [[DACircularProgressView alloc] initWithFrame:CGRectMake(447.0f, 47.0f, 63.0f, 63.0f)];
    [viewChorus addSubview:cpvChorusLpf];
    [viewChorus bringSubviewToFront:knobChorusLpf];
    
    knobChorusLfoPhase.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobChorusLfoPhase.scalingFactor = 2.0f;
	knobChorusLfoPhase.maximumValue = CHORUS_PHASE_MAX;
	knobChorusLfoPhase.minimumValue = CHORUS_PHASE_MIN;
	knobChorusLfoPhase.value = CHORUS_PHASE_DEFAULT;
	knobChorusLfoPhase.defaultValue = knobChorusLfoPhase.value;
	knobChorusLfoPhase.resetsToDefault = YES;
	knobChorusLfoPhase.backgroundColor = [UIColor clearColor];
	[knobChorusLfoPhase setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobChorusLfoPhase.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobChorusLfoPhase addTarget:self action:@selector(knobChorusLfoPhaseDidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvChorusLfoPhase = [[DACircularProgressView alloc] initWithFrame:CGRectMake(169.0f, 159.0f, 63.0f, 63.0f)];
    [viewChorus addSubview:cpvChorusLfoPhase];
    [viewChorus bringSubviewToFront:knobChorusLfoPhase];

    knobChorusLfoFreq.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobChorusLfoFreq.scalingFactor = 2.0f;
	knobChorusLfoFreq.maximumValue = CHORUS_LFO_FREQ_MAX;
	knobChorusLfoFreq.minimumValue = CHORUS_LFO_FREQ_MIN;
	knobChorusLfoFreq.value = CHORUS_LFO_FREQ_DEFAULT;
	knobChorusLfoFreq.defaultValue = knobChorusLfoFreq.value;
	knobChorusLfoFreq.resetsToDefault = YES;
	knobChorusLfoFreq.backgroundColor = [UIColor clearColor];
	[knobChorusLfoFreq setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobChorusLfoFreq.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobChorusLfoFreq addTarget:self action:@selector(knobChorusLfoFreqDidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvChorusLfoFreq = [[DACircularProgressView alloc] initWithFrame:CGRectMake(310.0f, 159.0f, 63.0f, 63.0f)];
    [viewChorus addSubview:cpvChorusLfoFreq];
    [viewChorus bringSubviewToFront:knobChorusLfoFreq];
    
    // Flanger view init
    knobFlangerPreDelay.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobFlangerPreDelay.scalingFactor = 2.0f;
	knobFlangerPreDelay.maximumValue = CHORUS_PRE_DELAY_MAX;
	knobFlangerPreDelay.minimumValue = CHORUS_PRE_DELAY_MIN;
	knobFlangerPreDelay.value = CHORUS_PRE_DELAY_DEFAULT;
	knobFlangerPreDelay.defaultValue = knobFlangerPreDelay.value;
	knobFlangerPreDelay.resetsToDefault = YES;
	knobFlangerPreDelay.backgroundColor = [UIColor clearColor];
	[knobFlangerPreDelay setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobFlangerPreDelay.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobFlangerPreDelay addTarget:self action:@selector(knobFlangerPreDelayDidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvFlangerPreDelay = [[DACircularProgressView alloc] initWithFrame:CGRectMake(94.0f, 48.0f, 63.0f, 63.0f)];
    [viewFlanger addSubview:cpvFlangerPreDelay];
    [viewFlanger bringSubviewToFront:knobFlangerPreDelay];
    
    knobFlangerDepth.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobFlangerDepth.scalingFactor = 2.0f;
	knobFlangerDepth.maximumValue = CHORUS_DEPTH_MAX;
	knobFlangerDepth.minimumValue = CHORUS_DEPTH_MIN;
	knobFlangerDepth.value = CHORUS_DEPTH_DEFAULT;
	knobFlangerDepth.defaultValue = knobFlangerDepth.value;
	knobFlangerDepth.resetsToDefault = YES;
	knobFlangerDepth.backgroundColor = [UIColor clearColor];
	[knobFlangerDepth setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobFlangerDepth.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobFlangerDepth addTarget:self action:@selector(knobFlangerDepthDidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvFlangerDepth = [[DACircularProgressView alloc] initWithFrame:CGRectMake(234.0f, 48.0f, 63.0f, 63.0f)];
    [viewFlanger addSubview:cpvFlangerDepth];
    [viewFlanger bringSubviewToFront:knobFlangerDepth];
    
    knobFlangerLpf.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobFlangerLpf.scalingFactor = 2.0f;
	knobFlangerLpf.maximumValue = LPF_FREQ_MAX;
	knobFlangerLpf.minimumValue = LPF_FREQ_MIN;
	knobFlangerLpf.value = LPF_FREQ_DEFAULT;
	knobFlangerLpf.defaultValue = knobFlangerLpf.value;
	knobFlangerLpf.resetsToDefault = YES;
	knobFlangerLpf.backgroundColor = [UIColor clearColor];
	[knobFlangerLpf setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobFlangerLpf.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobFlangerLpf addTarget:self action:@selector(knobFlangerLpfDidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvFlangerLpf = [[DACircularProgressView alloc] initWithFrame:CGRectMake(374.0f, 48.0f, 63.0f, 63.0f)];
    [viewFlanger addSubview:cpvFlangerLpf];
    [viewFlanger bringSubviewToFront:knobFlangerLpf];

    knobFlangerFb.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobFlangerFb.scalingFactor = 2.0f;
	knobFlangerFb.maximumValue = CHORUS_DEPTH_MAX;
	knobFlangerFb.minimumValue = CHORUS_DEPTH_MIN;
	knobFlangerFb.value = CHORUS_DEPTH_DEFAULT;
	knobFlangerFb.defaultValue = knobFlangerFb.value;
	knobFlangerFb.resetsToDefault = YES;
	knobFlangerFb.backgroundColor = [UIColor clearColor];
	[knobFlangerFb setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobFlangerFb.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobFlangerFb addTarget:self action:@selector(knobFlangerFbDidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvFlangerFb = [[DACircularProgressView alloc] initWithFrame:CGRectMake(514.0f, 48.0f, 63.0f, 63.0f)];
    [viewFlanger addSubview:cpvFlangerFb];
    [viewFlanger bringSubviewToFront:knobFlangerFb];
    
    knobFlangerLfoPhase.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobFlangerLfoPhase.scalingFactor = 2.0f;
	knobFlangerLfoPhase.maximumValue = CHORUS_PHASE_MAX;
	knobFlangerLfoPhase.minimumValue = CHORUS_PHASE_MIN;
	knobFlangerLfoPhase.value = CHORUS_PHASE_DEFAULT;
	knobFlangerLfoPhase.defaultValue = knobFlangerLfoPhase.value;
	knobFlangerLfoPhase.resetsToDefault = YES;
	knobFlangerLfoPhase.backgroundColor = [UIColor clearColor];
	[knobFlangerLfoPhase setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobFlangerLfoPhase.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobFlangerLfoPhase addTarget:self action:@selector(knobFlangerLfoPhaseDidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvFlangerLfoPhase = [[DACircularProgressView alloc] initWithFrame:CGRectMake(94.0, 159.0f, 63.0f, 63.0f)];
    [viewFlanger addSubview:cpvFlangerLfoPhase];
    [viewFlanger bringSubviewToFront:knobFlangerLfoPhase];
    
    knobFlangerLfoFreq.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobFlangerLfoFreq.scalingFactor = 2.0f;
	knobFlangerLfoFreq.maximumValue = CHORUS_LFO_FREQ_MAX;
	knobFlangerLfoFreq.minimumValue = CHORUS_LFO_FREQ_MIN;
	knobFlangerLfoFreq.value = CHORUS_LFO_FREQ_DEFAULT;
	knobFlangerLfoFreq.defaultValue = knobFlangerLfoFreq.value;
	knobFlangerLfoFreq.resetsToDefault = YES;
	knobFlangerLfoFreq.backgroundColor = [UIColor clearColor];
	[knobFlangerLfoFreq setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobFlangerLfoFreq.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobFlangerLfoFreq addTarget:self action:@selector(knobFlangerLfoFreqDidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvFlangerLfoFreq = [[DACircularProgressView alloc] initWithFrame:CGRectMake(234.0f, 159.0f, 63.0f, 63.0f)];
    [viewFlanger addSubview:cpvFlangerLfoFreq];
    [viewFlanger bringSubviewToFront:knobFlangerLfoFreq];
    
    // Phaser view init
    knobPhaserStageNo.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobPhaserStageNo.scalingFactor = 2.0f;
	knobPhaserStageNo.maximumValue = PHASER_STAGENO_MAX;
	knobPhaserStageNo.minimumValue = PHASER_STAGENO_MIN;
	knobPhaserStageNo.value = PHASER_STAGENO_DEFAULT;
	knobPhaserStageNo.defaultValue = knobPhaserStageNo.value;
	knobPhaserStageNo.resetsToDefault = YES;
	knobPhaserStageNo.backgroundColor = [UIColor clearColor];
	[knobPhaserStageNo setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobPhaserStageNo.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobPhaserStageNo addTarget:self action:@selector(knobPhaserStageNoDidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvPhaserStageNo = [[DACircularProgressView alloc] initWithFrame:CGRectMake(169.0f, 47.0f, 63.0f, 63.0f)];
    [viewPhaser addSubview:cpvPhaserStageNo];
    [viewPhaser bringSubviewToFront:knobPhaserStageNo];
    
    knobPhaserFreq.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobPhaserFreq.scalingFactor = 2.0f;
	knobPhaserFreq.maximumValue = HPF_FREQ_MAX;
	knobPhaserFreq.minimumValue = HPF_FREQ_MIN;
	knobPhaserFreq.value = HPF_FREQ_DEFAULT;
	knobPhaserFreq.defaultValue = knobPhaserFreq.value;
	knobPhaserFreq.resetsToDefault = YES;
	knobPhaserFreq.backgroundColor = [UIColor clearColor];
	[knobPhaserFreq setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobPhaserFreq.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobPhaserFreq addTarget:self action:@selector(knobPhaserFreqDidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvPhaserFreq = [[DACircularProgressView alloc] initWithFrame:CGRectMake(310.0f, 47.0f, 63.0f, 63.0f)];
    [viewPhaser addSubview:cpvPhaserFreq];
    [viewPhaser bringSubviewToFront:knobPhaserFreq];
    
    knobPhaserDepth.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobPhaserDepth.scalingFactor = 2.0f;
	knobPhaserDepth.maximumValue = CHORUS_DEPTH_MAX;
	knobPhaserDepth.minimumValue = CHORUS_DEPTH_MIN;
	knobPhaserDepth.value = CHORUS_DEPTH_DEFAULT;
	knobPhaserDepth.defaultValue = knobPhaserDepth.value;
	knobPhaserDepth.resetsToDefault = YES;
	knobPhaserDepth.backgroundColor = [UIColor clearColor];
	[knobPhaserDepth setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobPhaserDepth.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobPhaserDepth addTarget:self action:@selector(knobPhaserDepthDidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvPhaserDepth = [[DACircularProgressView alloc] initWithFrame:CGRectMake(447.0f, 47.0f, 63.0f, 63.0f)];
    [viewPhaser addSubview:cpvPhaserDepth];
    [viewPhaser bringSubviewToFront:knobPhaserDepth];
    
    knobPhaserLfoFreq.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobPhaserLfoFreq.scalingFactor = 2.0f;
	knobPhaserLfoFreq.maximumValue = CHORUS_LFO_FREQ_MAX;
	knobPhaserLfoFreq.minimumValue = CHORUS_LFO_FREQ_MIN;
	knobPhaserLfoFreq.value = CHORUS_LFO_FREQ_DEFAULT;
	knobPhaserLfoFreq.defaultValue = knobPhaserLfoFreq.value;
	knobPhaserLfoFreq.resetsToDefault = YES;
	knobPhaserLfoFreq.backgroundColor = [UIColor clearColor];
	[knobPhaserLfoFreq setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobPhaserLfoFreq.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobPhaserLfoFreq addTarget:self action:@selector(knobPhaserLfoFreqDidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvPhaserLfoFreq = [[DACircularProgressView alloc] initWithFrame:CGRectMake(169.0f, 159.0f, 63.0f, 63.0f)];
    [viewPhaser addSubview:cpvPhaserLfoFreq];
    [viewPhaser bringSubviewToFront:knobPhaserLfoFreq];
    
    // Tremolo view init    
    knobTremoloDepth.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobTremoloDepth.scalingFactor = 2.0f;
	knobTremoloDepth.maximumValue = CHORUS_DEPTH_MAX;
	knobTremoloDepth.minimumValue = CHORUS_DEPTH_MIN;
	knobTremoloDepth.value = CHORUS_DEPTH_DEFAULT;
	knobTremoloDepth.defaultValue = knobTremoloDepth.value;
	knobTremoloDepth.resetsToDefault = YES;
	knobTremoloDepth.backgroundColor = [UIColor clearColor];
	[knobTremoloDepth setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobTremoloDepth.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobTremoloDepth addTarget:self action:@selector(knobTremoloDepthDidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvTremoloDepth = [[DACircularProgressView alloc] initWithFrame:CGRectMake(174.0f, 108.0f, 63.0f, 63.0f)];
    [viewTremolo addSubview:cpvTremoloDepth];
    [viewTremolo bringSubviewToFront:knobTremoloDepth];
    
    knobTremoloLfoFreq.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobTremoloLfoFreq.scalingFactor = 2.0f;
	knobTremoloLfoFreq.maximumValue = CHORUS_LFO_FREQ_MAX;
	knobTremoloLfoFreq.minimumValue = CHORUS_LFO_FREQ_MIN;
	knobTremoloLfoFreq.value = CHORUS_LFO_FREQ_DEFAULT;
	knobTremoloLfoFreq.defaultValue = knobTremoloLfoFreq.value;
	knobTremoloLfoFreq.resetsToDefault = YES;
	knobTremoloLfoFreq.backgroundColor = [UIColor clearColor];
	[knobTremoloLfoFreq setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobTremoloLfoFreq.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobTremoloLfoFreq addTarget:self action:@selector(knobTremoloLfoFreqDidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvTremoloLfoFreq = [[DACircularProgressView alloc] initWithFrame:CGRectMake(314.0f, 108.0f, 63.0f, 63.0f)];
    [viewTremolo addSubview:cpvTremoloLfoFreq];
    [viewTremolo bringSubviewToFront:knobTremoloLfoFreq];
    
    // Vibrato view init
    knobVibratoFreq.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobVibratoFreq.scalingFactor = 2.0f;
	knobVibratoFreq.maximumValue = VIBRATO_FREQ_MAX;
	knobVibratoFreq.minimumValue = VIBRATO_FREQ_MIN;
	knobVibratoFreq.value = VIBRATO_FREQ_DEFAULT;
	knobVibratoFreq.defaultValue = knobVibratoFreq.value;
	knobVibratoFreq.resetsToDefault = YES;
	knobVibratoFreq.backgroundColor = [UIColor clearColor];
	[knobVibratoFreq setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobVibratoFreq.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobVibratoFreq addTarget:self action:@selector(knobVibratoFreqDidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvVibratoFreq = [[DACircularProgressView alloc] initWithFrame:CGRectMake(99.0f, 108.0f, 63.0f, 63.0f)];
    [viewVibrato addSubview:cpvVibratoFreq];
    [viewVibrato bringSubviewToFront:knobVibratoFreq];

    knobVibratoDepth.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobVibratoDepth.scalingFactor = 2.0f;
	knobVibratoDepth.maximumValue = CHORUS_DEPTH_MAX;
	knobVibratoDepth.minimumValue = CHORUS_DEPTH_MIN;
	knobVibratoDepth.value = CHORUS_DEPTH_DEFAULT;
	knobVibratoDepth.defaultValue = knobVibratoDepth.value;
	knobVibratoDepth.resetsToDefault = YES;
	knobVibratoDepth.backgroundColor = [UIColor clearColor];
	[knobVibratoDepth setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobVibratoDepth.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobVibratoDepth addTarget:self action:@selector(knobVibratoDepthDidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvVibratoDepth = [[DACircularProgressView alloc] initWithFrame:CGRectMake(239.0f, 108.0f, 63.0f, 63.0f)];
    [viewVibrato addSubview:cpvVibratoDepth];
    [viewVibrato bringSubviewToFront:knobVibratoDepth];
    
    knobVibratoLfoFreq.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobVibratoLfoFreq.scalingFactor = 2.0f;
	knobVibratoLfoFreq.maximumValue = CHORUS_LFO_FREQ_MAX;
	knobVibratoLfoFreq.minimumValue = CHORUS_LFO_FREQ_MIN;
	knobVibratoLfoFreq.value = CHORUS_LFO_FREQ_DEFAULT;
	knobVibratoLfoFreq.defaultValue = knobVibratoLfoFreq.value;
	knobVibratoLfoFreq.resetsToDefault = YES;
	knobVibratoLfoFreq.backgroundColor = [UIColor clearColor];
	[knobVibratoLfoFreq setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobVibratoLfoFreq.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobVibratoLfoFreq addTarget:self action:@selector(knobVibratoLfoFreqDidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvVibratoLfoFreq = [[DACircularProgressView alloc] initWithFrame:CGRectMake(379.0f, 108.0f, 63.0f, 63.0f)];
    [viewVibrato addSubview:cpvVibratoLfoFreq];
    [viewVibrato bringSubviewToFront:knobVibratoLfoFreq];
    
    // AutoPan view init
    knobAutoPanWay.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobAutoPanWay.scalingFactor = 2.0f;
	knobAutoPanWay.maximumValue = AUTOPAN_WAY_MAX;
	knobAutoPanWay.minimumValue = AUTOPAN_WAY_MIN;
	knobAutoPanWay.value = AUTOPAN_WAY_DEFAULT;
	knobAutoPanWay.defaultValue = knobAutoPanWay.value;
	knobAutoPanWay.resetsToDefault = YES;
	knobAutoPanWay.backgroundColor = [UIColor clearColor];
	[knobAutoPanWay setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobAutoPanWay.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobAutoPanWay addTarget:self action:@selector(knobAutoPanWayDidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvAutoPanWay = [[DACircularProgressView alloc] initWithFrame:CGRectMake(99.0f, 108.0f, 63.0f, 63.0f)];
    [viewAutoPan addSubview:cpvAutoPanWay];
    [viewAutoPan bringSubviewToFront:knobAutoPanWay];
    
    knobAutoPanDepth.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobAutoPanDepth.scalingFactor = 2.0f;
	knobAutoPanDepth.maximumValue = CHORUS_DEPTH_MAX;
	knobAutoPanDepth.minimumValue = CHORUS_DEPTH_MIN;
	knobAutoPanDepth.value = CHORUS_DEPTH_DEFAULT;
	knobAutoPanDepth.defaultValue = knobAutoPanDepth.value;
	knobAutoPanDepth.resetsToDefault = YES;
	knobAutoPanDepth.backgroundColor = [UIColor clearColor];
	[knobAutoPanDepth setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobAutoPanDepth.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobAutoPanDepth addTarget:self action:@selector(knobAutoPanDepthDidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvAutoPanDepth = [[DACircularProgressView alloc] initWithFrame:CGRectMake(239.0f, 108.0f, 63.0f, 63.0f)];
    [viewAutoPan addSubview:cpvAutoPanDepth];
    [viewAutoPan bringSubviewToFront:knobAutoPanDepth];
    
    knobAutoPanLfoFreq.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobAutoPanLfoFreq.scalingFactor = 2.0f;
	knobAutoPanLfoFreq.maximumValue = CHORUS_LFO_FREQ_MAX;
	knobAutoPanLfoFreq.minimumValue = CHORUS_LFO_FREQ_MIN;
	knobAutoPanLfoFreq.value = CHORUS_LFO_FREQ_DEFAULT;
	knobAutoPanLfoFreq.defaultValue = knobAutoPanLfoFreq.value;
	knobAutoPanLfoFreq.resetsToDefault = YES;
	knobAutoPanLfoFreq.backgroundColor = [UIColor clearColor];
	[knobAutoPanLfoFreq setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobAutoPanLfoFreq.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobAutoPanLfoFreq addTarget:self action:@selector(knobAutoPanLfoFreqDidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvAutoPanLfoFreq = [[DACircularProgressView alloc] initWithFrame:CGRectMake(379.0f, 108.0f, 63.0f, 63.0f)];
    [viewAutoPan addSubview:cpvAutoPanLfoFreq];
    [viewAutoPan bringSubviewToFront:knobAutoPanLfoFreq];
    
    [self.view bringSubviewToFront:viewEffectInL];
    [self.view bringSubviewToFront:viewEffectInR];
    [self.view bringSubviewToFront:viewEffect1];
    [self.view bringSubviewToFront:viewEffect2];
}

- (void)viewDidAppear:(BOOL)animated
{
    // Init GEQ31 & GEQ15 controllers
    if (effectGeq31Controller == nil) {
        effectGeq31Controller = (EffectGeq31ViewController *)[[EffectGeq31ViewController alloc] initWithNibName:@"EffectGeq31View" bundle:nil];
        [effectGeq31Controller.view setFrame:CGRectMake(0, 153, 925, 615)];
        [viewController.view addSubview:effectGeq31Controller.view];
        [effectGeq31Controller setDelegate:self];
        [effectGeq31Controller.view setHidden:YES];
    }
    
    if (effectGeq15Controller == nil) {
        effectGeq15Controller = (EffectGeq15ViewController *)[[EffectGeq15ViewController alloc] initWithNibName:@"EffectGeq15View" bundle:nil];
        [effectGeq15Controller.view setFrame:CGRectMake(0, 153, 925, 615)];
        [viewController.view addSubview:effectGeq15Controller.view];
        [effectGeq15Controller setDelegate:self];
        [effectGeq15Controller.view setHidden:YES];
    }    
}

- (void)touchesBegan:(NSSet*)touches withEvent:(UIEvent*)event
{
    // Detect touches for GEQ31/GEQ15 views
    UITouch *touch = [touches anyObject];
    CGPoint touch_point = [touch locationInView:self.view];
    
    if (![viewGeq31 pointInside:touch_point withEvent:event] && !viewGeq31.hidden) {
        if (effectGeq31Controller == nil) {
            effectGeq31Controller = (EffectGeq31ViewController *)[[EffectGeq31ViewController alloc] initWithNibName:@"EffectGeq31View" bundle:nil];
            [effectGeq31Controller.view setFrame:CGRectMake(0, 153, 925, 615)];
            [viewController.view addSubview:effectGeq31Controller.view];
            [effectGeq31Controller setDelegate:self];
        } else {
            effectGeq31Controller.view.hidden = NO;
        }
    } else if (![viewGeq15 pointInside:touch_point withEvent:event] && !viewGeq15.hidden) {
        if (effectGeq15Controller == nil) {
            effectGeq15Controller = (EffectGeq15ViewController *)[[EffectGeq15ViewController alloc] initWithNibName:@"EffectGeq15View" bundle:nil];
            [effectGeq15Controller.view setFrame:CGRectMake(0, 153, 925, 615)];
            [viewController.view addSubview:effectGeq15Controller.view];
            [effectGeq15Controller setDelegate:self];
        } else {
            effectGeq15Controller.view.hidden = NO;
        }
    }
    
    // Close menu views if not touched
    if (![viewEffect1 pointInside:touch_point withEvent:event] && !viewEffect1.hidden) {
        [viewEffect1 setHidden:YES];
    }
    if (![viewEffect2 pointInside:touch_point withEvent:event] && !viewEffect2.hidden) {
        [viewEffect2 setHidden:YES];
    }
    if (![viewEffectInL pointInside:touch_point withEvent:event] && !viewEffectInL.hidden) {
        [viewEffectInL setHidden:YES];
    }
    if (![viewEffectInR pointInside:touch_point withEvent:event] && !viewEffectInR.hidden) {
        [viewEffectInR setHidden:YES];
    }
    if (![viewReverbType pointInside:touch_point withEvent:event] && !viewReverbType.hidden) {
        [viewReverbType setHidden:YES];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark UI update methods

- (void)processReply:(EffectPagePacket *)pkt {
    
    effect1InL = pkt->effect_1_1_in_source;
    effect1InR = pkt->effect_1_2_in_source;
    effect2InL = pkt->effect_2_1_in_source;
    effect2InR = pkt->effect_2_2_in_source;
    effect1OutL = pkt->effect_1_1_out_source;
    effect1OutR = pkt->effect_1_2_out_source;
    effect2OutL = pkt->effect_2_1_out_source;
    effect2OutR = pkt->effect_2_2_out_source;
    
    if (effectNum == EFX_1) {
        meterInL.progress = ((float)pkt->effect_1_1_in_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
        meterInR.progress = ((float)pkt->effect_1_2_in_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
        meterOutL.progress = ((float)pkt->effect_1_1_out_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
        meterOutR.progress = ((float)pkt->effect_1_2_out_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
        if (![viewController sendCommandExists:CMD_EFFECT_1_DRY_WET_MIX dspId:DSP_5]) {
            sliderEffectDryWet.value = pkt->effect_1_dry_wet_mix;
            [self onSliderEffectDryWet:nil];
        }
        
        if (![viewController sendCommandExists:CMD_EFFECT_1_1_IN_SOURCE dspId:DSP_5]) {
            switch (pkt->effect_1_1_in_source) {
                case EFFECT_IN_NULL:
                    [self onBtnEffectInLNull:nil];
                    break;
                case EFFECT_IN_CH_1:
                    [self onBtnEffectInLCh1:nil];
                    break;
                case EFFECT_IN_CH_2:
                    [self onBtnEffectInLCh2:nil];
                    break;
                case EFFECT_IN_CH_3:
                    [self onBtnEffectInLCh3:nil];
                    break;
                case EFFECT_IN_CH_4:
                    [self onBtnEffectInLCh4:nil];
                    break;
                case EFFECT_IN_CH_5:
                    [self onBtnEffectInLCh5:nil];
                    break;
                case EFFECT_IN_CH_6:
                    [self onBtnEffectInLCh6:nil];
                    break;
                case EFFECT_IN_CH_7:
                    [self onBtnEffectInLCh7:nil];
                    break;
                case EFFECT_IN_CH_8:
                    [self onBtnEffectInLCh8:nil];
                    break;
                case EFFECT_IN_CH_9:
                    [self onBtnEffectInLCh9:nil];
                    break;
                case EFFECT_IN_CH_10:
                    [self onBtnEffectInLCh10:nil];
                    break;
                case EFFECT_IN_CH_11:
                    [self onBtnEffectInLCh11:nil];
                    break;
                case EFFECT_IN_CH_12:
                    [self onBtnEffectInLCh12:nil];
                    break;
                case EFFECT_IN_CH_13:
                    [self onBtnEffectInLCh13:nil];
                    break;
                case EFFECT_IN_CH_14:
                    [self onBtnEffectInLCh14:nil];
                    break;
                case EFFECT_IN_CH_15:
                    [self onBtnEffectInLCh15:nil];
                    break;
                case EFFECT_IN_CH_16:
                    [self onBtnEffectInLCh16:nil];
                    break;
                case EFFECT_IN_GP_1:
                    [self onBtnEffectInLGp1:nil];
                    break;
                case EFFECT_IN_GP_2:
                    [self onBtnEffectInLGp2:nil];
                    break;
                case EFFECT_IN_GP_3:
                    [self onBtnEffectInLGp3:nil];
                    break;
                case EFFECT_IN_GP_4:
                    [self onBtnEffectInLGp4:nil];
                    break;
                case EFFECT_IN_AUX_1:
                    [self onBtnEffectInLAux1:nil];
                    break;
                case EFFECT_IN_AUX_2:
                    [self onBtnEffectInLAux2:nil];
                    break;
                case EFFECT_IN_AUX_3:
                    [self onBtnEffectInLAux3:nil];
                    break;
                case EFFECT_IN_AUX_4:
                    [self onBtnEffectInLAux4:nil];
                    break;
                case EFFECT_IN_CH_17:
                    [self onBtnEffectInLCh17:nil];
                    break;
                case EFFECT_IN_CH_18:
                    [self onBtnEffectInLCh18:nil];
                    break;
                default:
                    break;
            }
        }
        
        if (![viewController sendCommandExists:CMD_EFFECT_1_2_IN_SOURCE dspId:DSP_5]) {
            switch (pkt->effect_1_2_in_source) {
                case EFFECT_IN_NULL:
                    [self onBtnEffectInRNull:nil];
                    break;
                case EFFECT_IN_CH_1:
                    [self onBtnEffectInRCh1:nil];
                    break;
                case EFFECT_IN_CH_2:
                    [self onBtnEffectInRCh2:nil];
                    break;
                case EFFECT_IN_CH_3:
                    [self onBtnEffectInRCh3:nil];
                    break;
                case EFFECT_IN_CH_4:
                    [self onBtnEffectInRCh4:nil];
                    break;
                case EFFECT_IN_CH_5:
                    [self onBtnEffectInRCh5:nil];
                    break;
                case EFFECT_IN_CH_6:
                    [self onBtnEffectInRCh6:nil];
                    break;
                case EFFECT_IN_CH_7:
                    [self onBtnEffectInRCh7:nil];
                    break;
                case EFFECT_IN_CH_8:
                    [self onBtnEffectInRCh8:nil];
                    break;
                case EFFECT_IN_CH_9:
                    [self onBtnEffectInRCh9:nil];
                    break;
                case EFFECT_IN_CH_10:
                    [self onBtnEffectInRCh10:nil];
                    break;
                case EFFECT_IN_CH_11:
                    [self onBtnEffectInRCh11:nil];
                    break;
                case EFFECT_IN_CH_12:
                    [self onBtnEffectInRCh12:nil];
                    break;
                case EFFECT_IN_CH_13:
                    [self onBtnEffectInRCh13:nil];
                    break;
                case EFFECT_IN_CH_14:
                    [self onBtnEffectInRCh14:nil];
                    break;
                case EFFECT_IN_CH_15:
                    [self onBtnEffectInRCh15:nil];
                    break;
                case EFFECT_IN_CH_16:
                    [self onBtnEffectInRCh16:nil];
                    break;
                case EFFECT_IN_GP_1:
                    [self onBtnEffectInRGp1:nil];
                    break;
                case EFFECT_IN_GP_2:
                    [self onBtnEffectInRGp2:nil];
                    break;
                case EFFECT_IN_GP_3:
                    [self onBtnEffectInRGp3:nil];
                    break;
                case EFFECT_IN_GP_4:
                    [self onBtnEffectInRGp4:nil];
                    break;
                case EFFECT_IN_AUX_1:
                    [self onBtnEffectInRAux1:nil];
                    break;
                case EFFECT_IN_AUX_2:
                    [self onBtnEffectInRAux2:nil];
                    break;
                case EFFECT_IN_AUX_3:
                    [self onBtnEffectInRAux3:nil];
                    break;
                case EFFECT_IN_AUX_4:
                    [self onBtnEffectInRAux4:nil];
                    break;
                case EFFECT_IN_CH_17:
                    [self onBtnEffectInRCh17:nil];
                    break;
                case EFFECT_IN_CH_18:
                    [self onBtnEffectInRCh18:nil];
                    break;
                default:
                    break;
            }
        }
        
        if (![viewController sendCommandExists:CMD_EFFECT_1_1_OUT_SOURCE dspId:DSP_5]) {
            btnEffectOutLGp1Status.selected = pkt->effect_1_1_out_source & 0x01;
            btnEffectOutLGp3Status.selected = (pkt->effect_1_1_out_source & 0x02) >> 1;
            btnEffectOutLMulti1Status.selected = (pkt->effect_1_1_out_source & 0x10) >> 4;
            btnEffectOutLMulti3Status.selected = (pkt->effect_1_1_out_source & 0x20) >> 5;
            btnEffectOutLMainLStatus.selected = (pkt->effect_1_1_out_source & 0x0100) >> 8;
        }
        if (![viewController sendCommandExists:CMD_EFFECT_1_2_OUT_SOURCE dspId:DSP_5]) {
            btnEffectOutRGp2Status.selected = pkt->effect_1_2_out_source & 0x01;
            btnEffectOutRGp4Status.selected = (pkt->effect_1_2_out_source & 0x02) >> 1;
            btnEffectOutRMulti2Status.selected = (pkt->effect_1_2_out_source & 0x10) >> 4;
            btnEffectOutRMulti4Status.selected = (pkt->effect_1_2_out_source & 0x20) >> 5;
            btnEffectOutRMainRStatus.selected = (pkt->effect_1_2_out_source & 0x0100) >> 8;
        }
        
        switch (pkt->effect_1_program) {
            case EFFECT_REVERB:
                {
                    if ([viewController sendCommandExists:CMD_EFFECT_1_REVERB_TYPE dspId:DSP_8]) {
                        NSLog(@"Reverb command exists!");
                        break;
                    }
                    [btnReverbType setHidden:NO];
                    switch (pkt->effect_1_reverb_type) {
                        case ROOM_LARGE:
                            [self onBtnReverbType1:nil];
                            [self onBtnEffect1ReverbRoom:nil];
                            reverbGroup = REVERB_ROOM;
                            reverbRoom = pkt->effect_1_reverb_type;
                            break;
                        case ROOM_MEDIUM:
                            [self onBtnReverbType2:nil];
                            [self onBtnEffect1ReverbRoom:nil];
                            reverbGroup = REVERB_ROOM;
                            reverbRoom = pkt->effect_1_reverb_type;
                            break;
                        case ROOM_SMALL:
                            [self onBtnReverbType3:nil];
                            [self onBtnEffect1ReverbRoom:nil];
                            reverbGroup = REVERB_ROOM;
                            reverbRoom = pkt->effect_1_reverb_type;
                            break;
                        case ROOM_LIVE:
                            [self onBtnReverbType4:nil];
                            [self onBtnEffect1ReverbRoom:nil];
                            reverbGroup = REVERB_ROOM;
                            reverbRoom = pkt->effect_1_reverb_type;
                            break;
                        case ROOM_BRIGHT:
                            [self onBtnReverbType5:nil];
                            [self onBtnEffect1ReverbRoom:nil];
                            reverbGroup = REVERB_ROOM;
                            reverbRoom = pkt->effect_1_reverb_type;
                            break;
                        case ROOM_WOOD:
                            [self onBtnReverbType6:nil];
                            [self onBtnEffect1ReverbRoom:nil];
                            reverbGroup = REVERB_ROOM;
                            reverbRoom = pkt->effect_1_reverb_type;
                            break;
                        case ROOM_HEAVY:
                            [self onBtnReverbType7:nil];
                            [self onBtnEffect1ReverbRoom:nil];
                            reverbGroup = REVERB_ROOM;
                            reverbRoom = pkt->effect_1_reverb_type;
                            break;
                        case ROOM_OPERA:
                            [self onBtnReverbType8:nil];
                            [self onBtnEffect1ReverbRoom:nil];
                            reverbGroup = REVERB_ROOM;
                            reverbRoom = pkt->effect_1_reverb_type;
                            break;
                        case HALL_LARGE:
                            [self onBtnReverbType1:nil];
                            [self onBtnEffect1ReverbHall:nil];
                            reverbGroup = REVERB_HALL;
                            reverbHall = pkt->effect_1_reverb_type;
                            break;
                        case HALL_MEDIUM:
                            [self onBtnReverbType2:nil];
                            [self onBtnEffect1ReverbHall:nil];
                            reverbGroup = REVERB_HALL;
                            reverbHall = pkt->effect_1_reverb_type;
                            break;
                        case HALL_SMALL:
                            [self onBtnReverbType3:nil];
                            [self onBtnEffect1ReverbHall:nil];
                            reverbGroup = REVERB_HALL;
                            reverbHall = pkt->effect_1_reverb_type;
                            break;
                        case HALL_CONCERT:
                            [self onBtnReverbType4:nil];
                            [self onBtnEffect1ReverbHall:nil];
                            reverbGroup = REVERB_HALL;
                            reverbHall = pkt->effect_1_reverb_type;
                            break;
                        case HALL_DARK:
                            [self onBtnReverbType5:nil];
                            [self onBtnEffect1ReverbHall:nil];
                            reverbGroup = REVERB_HALL;
                            reverbHall = pkt->effect_1_reverb_type;
                            break;
                        case HALL_WONDER:
                            [self onBtnReverbType6:nil];
                            [self onBtnEffect1ReverbHall:nil];
                            reverbGroup = REVERB_HALL;
                            reverbHall = pkt->effect_1_reverb_type;
                            break;
                        case HALL_JAZZ:
                            [self onBtnReverbType7:nil];
                            [self onBtnEffect1ReverbHall:nil];
                            reverbGroup = REVERB_HALL;
                            reverbHall = pkt->effect_1_reverb_type;
                            break;
                        case HALL_VOCAL:
                            [self onBtnReverbType8:nil];
                            [self onBtnEffect1ReverbHall:nil];
                            reverbGroup = REVERB_HALL;
                            reverbHall = pkt->effect_1_reverb_type;
                            break;                        
                        case PLATE_LARGE:
                            [self onBtnReverbType1:nil];
                            [self onBtnEffect1ReverbPlate:nil];
                            reverbGroup = REVERB_PLATE;
                            reverbPlate = pkt->effect_1_reverb_type;
                            break;
                        case PLATE_MEDIUM:
                            [self onBtnReverbType2:nil];
                            [self onBtnEffect1ReverbPlate:nil];
                            reverbGroup = REVERB_PLATE;
                            reverbPlate = pkt->effect_1_reverb_type;
                            break;
                        case PLATE_SMALL:
                            [self onBtnReverbType3:nil];
                            [self onBtnEffect1ReverbPlate:nil];
                            reverbGroup = REVERB_PLATE;
                            reverbPlate = pkt->effect_1_reverb_type;
                            break;
                        case PLATE_FLAT:
                            [self onBtnReverbType4:nil];
                            [self onBtnEffect1ReverbPlate:nil];
                            reverbGroup = REVERB_PLATE;
                            reverbPlate = pkt->effect_1_reverb_type;
                            break;
                        case PLATE_LIGHT:
                            [self onBtnReverbType5:nil];
                            [self onBtnEffect1ReverbPlate:nil];
                            reverbGroup = REVERB_PLATE;
                            reverbPlate = pkt->effect_1_reverb_type;
                            break;
                        case PLATE_THIN:
                            [self onBtnReverbType6:nil];
                            [self onBtnEffect1ReverbPlate:nil];
                            reverbGroup = REVERB_PLATE;
                            reverbPlate = pkt->effect_1_reverb_type;
                            break;
                        case PLATE_PERC:
                            [self onBtnReverbType7:nil];
                            [self onBtnEffect1ReverbPlate:nil];
                            reverbGroup = REVERB_PLATE;
                            reverbPlate = pkt->effect_1_reverb_type;
                            break;
                        case PLATE_INDUSTRIAL:
                            [self onBtnReverbType8:nil];
                            [self onBtnEffect1ReverbPlate:nil];
                            reverbGroup = REVERB_PLATE;
                            reverbPlate = pkt->effect_1_reverb_type;
                            break;
                        default:
                            break;
                    }
                    
                    if (![viewController sendCommandExists:CMD_EFFECT_1_REVERB_HPF_FREQ dspId:DSP_8]) {
                        knobHpf.value = [Global freqToKnobValue:pkt->effect_1_reverb_hpf_freq];
                        [self knobHpfDidChange:nil];
                    }
                    if (![viewController sendCommandExists:CMD_EFFECT_1_REVERB_LPF_FREQ dspId:DSP_8]) {
                        knobLpf.value = [Global freqToKnobValue:pkt->effect_1_reverb_lpf_freq];
                        [self knobLpfDidChange:nil];
                    }
                    if (![viewController sendCommandExists:CMD_EFFECT_1_REVERB_PRE_DELAY dspId:DSP_8]) {
                        knobPreDelay.value = pkt->effect_1_pre_delay;
                        [self knobPreDelayDidChange:nil];
                    }
                    if (![viewController sendCommandExists:CMD_EFFECT_1_REVERB_EARLY_DELAY_OUT dspId:DSP_8]) {
                        knobEarlyOut.value = pkt->effect_1_reverb_early_delay_out;
                        [self knobEarlyOutDidChange:nil];
                    }
                    if (![viewController sendCommandExists:CMD_EFFECT_1_REVERB_TIME dspId:DSP_8]) {
                        knobReverbTime.value = pkt->effect_1_reverb_time;
                        [self knobReverbTimeDidChange:nil];
                    }
                    if (![viewController sendCommandExists:CMD_EFFECT_1_REVERB_LEVEL dspId:DSP_8]) {
                        knobLevel.value = pkt->effect_1_reverb_level;
                        [self knobLevelDidChange:nil];
                    }
                    if (![viewController sendCommandExists:CMD_EFFECT_1_REVERB_HI_RATIO dspId:DSP_8]) {
                        knobHiRatio.value = pkt->effect_1_reverb_hi_ratio;
                        [self knobHiRatioDidChange:nil];
                    }
                    if (![viewController sendCommandExists:CMD_EFFECT_1_REVERB_DENSITY dspId:DSP_8]) {
                        knobDensity.value = pkt->effect_1_reverb_density;
                        [self knobDensityDidChange:nil];
                    }
                    if (![viewController sendCommandExists:CMD_EFFECT_1_REVERB_GATE_THRESHOLD dspId:DSP_8]) {
                        knobThreshold.value = pkt->effect_1_reverb_gate_threshold + REVERB_GATE_THRESHOLD_MAX;
                        [self knobThresholdDidChange:nil];
                    }
                    if (![viewController sendCommandExists:CMD_EFFECT_1_REVERB_GATE_HOLD_TIME dspId:DSP_8]) {
                        knobHold.value = pkt->effect_1_reverb_gate_hold_time;
                        [self knobHoldDidChange:nil];
                    }
                    btnGate.selected = pkt->effect_1_reverb_gate;
                }
                break;
            case EFFECT_ECHO_DELAY:
                if ([viewController sendCommandExists:CMD_EFFECT_1_PROGRAM dspId:DSP_8]) {
                    NSLog(@"Echo command exists!");
                    break;
                }
                [btnReverbType setHidden:YES];
                [self onBtnEffect1Echo:nil];
                if (![viewController sendCommandExists:CMD_EFFECT_1_ECHO_DELAY_1_TIME dspId:DSP_8]) {
                    knobEchoTimeL.value = pkt->effect_1_echo_delay1_time;
                    [self knobEchoTimeLDidChange:nil];
                }
                if (![viewController sendCommandExists:CMD_EFFECT_1_ECHO_DELAY_2_TIME dspId:DSP_8]) {
                    knobEchoTimeR.value = pkt->effect_1_echo_delay2_time;
                    [self knobEchoTimeRDidChange:nil];
                }
                if (![viewController sendCommandExists:CMD_EFFECT_1_ECHO_DELAY_FB_1 dspId:DSP_8]) {
                    knobEchoFb1.value = pkt->effect_1_echo_delay_fb1;
                    [self knobEchoFb1DidChange:nil];
                }
                if (![viewController sendCommandExists:CMD_EFFECT_1_ECHO_DELAY_FB_2 dspId:DSP_8]) {
                    knobEchoFb2.value = pkt->effect_1_echo_delay_fb2;
                    [self knobEchoFb2DidChange:nil];
                }
                if (![viewController sendCommandExists:CMD_EFFECT_1_ECHO_DELAY_FB_HPF dspId:DSP_8]) {
                    knobEchoHpf.value = [Global freqToKnobValue:pkt->effect_1_echo_delay_fb_hpf];
                    [self knobEchoHpfDidChange:nil];
                }
                if (![viewController sendCommandExists:CMD_EFFECT_1_ECHO_DELAY_FB_LPF dspId:DSP_8]) {
                    knobEchoLpf.value = [Global freqToKnobValue:pkt->effect_1_echo_delay_fb_lpf];
                    [self knobEchoLpfDidChange:nil];
                }
                break;
            case EFFECT_TAP_DELAY:
                if ([viewController sendCommandExists:CMD_EFFECT_1_PROGRAM dspId:DSP_8]) {
                    NSLog(@"Tap delay command exists!");
                    break;
                }
                [btnReverbType setHidden:YES];
                [self onBtnEffect1TapDelay:nil];
                if (![viewController sendCommandExists:CMD_EFFECT_1_TAP_DELAY_FB dspId:DSP_8]) {
                    knobTapFb.value = pkt->effect_1_tap_delay_fb;
                    [self knobTapFbDidChange:nil];
                }
                if (![viewController sendCommandExists:CMD_EFFECT_1_TAP_DELAY_FB_HPF dspId:DSP_8]) {
                    knobTapHpf.value = [Global freqToKnobValue:pkt->effect_1_tap_delay_fb_hpf];
                    [self knobTapHpfDidChange:nil];
                }
                if (![viewController sendCommandExists:CMD_EFFECT_1_TAP_DELAY_FB_LPF dspId:DSP_8]) {
                    knobTapLpf.value = [Global freqToKnobValue:pkt->effect_1_tap_delay_fb_lpf];
                    [self knobTapLpfDidChange:nil];
                }
                btnTap.selected = pkt->effect_1_tap_key;
                NSLog(@"Efx 1 tap delay = %d", pkt->effect_1_delay_time);
                if (pkt->effect_1_delay_time > 0) {
                    int curTapDelay = pkt->effect_1_delay_time / 10;
                    if (curTapDelay != _tapDelay) {
                        _tapDelay = curTapDelay;
                        [lblTapDelay setText:[NSString stringWithFormat:@"%dmS", _tapDelay]];
                        //[viewController sendData:CMD_EFFECT_1_TAP_DELAY_TIME commandType:DSP_WRITE dspId:DSP_8 value:_tapDelay*10];
                        if (_tapDelayTimer != nil) {
                            [_tapDelayTimer invalidate];
                            _tapDelayTimer = nil;
                        }
                        _tapDelayTimer = [NSTimer timerWithTimeInterval:(float)(_tapDelay/1000.0f) target:self selector:@selector(updateTapDelayLed) userInfo:nil repeats:YES];
                        [[NSRunLoop mainRunLoop] addTimer:_tapDelayTimer forMode:NSRunLoopCommonModes];
                    }
                }
                break;
            case EFFECT_CHORUS:
                if ([viewController sendCommandExists:CMD_EFFECT_1_PROGRAM dspId:DSP_8]) {
                    NSLog(@"Chorus command exists!");
                    break;
                }
                [btnReverbType setHidden:YES];
                [self onBtnEffect1Chorus:nil];
                if (![viewController sendCommandExists:CMD_EFFECT_1_CHORUS_PRE_DELAY dspId:DSP_8]) {
                    knobChorusPreDelay.value = pkt->effect_1_chorus_pre_delay;
                    [self knobChorusPreDelayDidChange:nil];
                }
                if (![viewController sendCommandExists:CMD_EFFECT_1_CHORUS_DEPTH dspId:DSP_8]) {
                    knobChorusDepth.value = pkt->effect_1_chorus_depth;
                    [self knobChorusDepthDidChange:nil];
                }
                if (![viewController sendCommandExists:CMD_EFFECT_1_CHORUS_LPF_FREQ dspId:DSP_8]) {
                    knobChorusLpf.value = [Global freqToKnobValue:pkt->effect_1_chorus_lpf_freq];
                    [self knobChorusLpfDidChange:nil];
                }
                if (![viewController sendCommandExists:CMD_EFFECT_1_CHORUS_LFO_PHASE dspId:DSP_8]) {
                    knobChorusLfoPhase.value = pkt->effect_1_chorus_lfo_phase;
                    [self knobChorusLfoPhaseDidChange:nil];
                }
                if (![viewController sendCommandExists:CMD_EFFECT_1_CHORUS_LFO_FREQ dspId:DSP_8]) {
                    knobChorusLfoFreq.value = pkt->effect_1_chorus_lfo_freq;
                    [self knobChorusLfoFreqDidChange:nil];
                }
                if (![viewController sendCommandExists:CMD_EFFECT_1_CHORUS_LFO_TYPE dspId:DSP_8]) {
                    if (pkt->effect_1_chorus_lfo_type == 1) {
                        btnChorusLfoType.selected = NO;
                    } else if (pkt->effect_1_chorus_lfo_type == 2) {
                        btnChorusLfoType.selected = YES;
                    }
                }
                break;
            case EFFECT_FLANGER:
                if ([viewController sendCommandExists:CMD_EFFECT_1_PROGRAM dspId:DSP_8]) {
                    NSLog(@"Flanger command exists!");
                    break;
                }
                [btnReverbType setHidden:YES];
                [self onBtnEffect1Flanger:nil];
                if (![viewController sendCommandExists:CMD_EFFECT_1_FLANGER_PRE_DELAY dspId:DSP_8]) {
                    knobFlangerPreDelay.value = pkt->effect_1_flanger_pre_delay;
                    [self knobFlangerPreDelayDidChange:nil];
                }
                if (![viewController sendCommandExists:CMD_EFFECT_1_FLANGER_LPF_FREQ dspId:DSP_8]) {
                    knobFlangerLpf.value = [Global freqToKnobValue:pkt->effect_1_flanger_lpf_freq];
                    [self knobFlangerLpfDidChange:nil];
                }
                if (![viewController sendCommandExists:CMD_EFFECT_1_FLANGER_DEPTH dspId:DSP_8]) {
                    knobFlangerDepth.value = pkt->effect_1_flanger_depth;
                    [self knobFlangerDepthDidChange:nil];
                }
                if (![viewController sendCommandExists:CMD_EFFECT_1_FLANGER_FB dspId:DSP_8]) {
                    knobFlangerFb.value = pkt->effect_1_flanger_fb;
                    [self knobFlangerFbDidChange:nil];
                }
                if (![viewController sendCommandExists:CMD_EFFECT_1_FLANGER_LFO_PHASE dspId:DSP_8]) {
                    knobFlangerLfoPhase.value = pkt->effect_1_flanger_lfo_phase;
                    [self knobFlangerLfoPhaseDidChange:nil];
                }
                if (![viewController sendCommandExists:CMD_EFFECT_1_FLANGER_LFO_FREQ dspId:DSP_8]) {
                    knobFlangerLfoFreq.value = pkt->effect_1_flanger_lfo_freq;
                    [self knobFlangerLfoFreqDidChange:nil];
                }
                if (![viewController sendCommandExists:CMD_EFFECT_1_FLANGER_LFO_TYPE dspId:DSP_8]) {
                    if (pkt->effect_1_flanger_lfo_type == 1) {
                        btnFlangerLfoType.selected = NO;
                    } else if (pkt->effect_1_flanger_lfo_type == 2) {
                        btnFlangerLfoType.selected = YES;
                    }
                }
                break;
            case EFFECT_PHASER:
                if ([viewController sendCommandExists:CMD_EFFECT_1_PROGRAM dspId:DSP_8]) {
                    NSLog(@"Phaser command exists!");
                    break;
                }
                [btnReverbType setHidden:YES];
                [self onBtnEffect1Phaser:nil];
                if (![viewController sendCommandExists:CMD_EFFECT_1_PHASER_STAGE_NO dspId:DSP_8]) {
                    knobPhaserStageNo.value = pkt->effect_1_phaser_stage_no;
                    [self knobPhaserStageNoDidChange:nil];
                }
                if (![viewController sendCommandExists:CMD_EFFECT_1_PHASER_FREQ dspId:DSP_8]) {
                    knobPhaserFreq.value = [Global freqToKnobValue:pkt->effect_1_phaser_freq];
                    [self knobPhaserFreqDidChange:nil];
                }
                if (![viewController sendCommandExists:CMD_EFFECT_1_PHASER_DEPTH dspId:DSP_8]) {
                    knobPhaserDepth.value = pkt->effect_1_phaser_depth;
                    [self knobPhaserDepthDidChange:nil];
                }
                if (![viewController sendCommandExists:CMD_EFFECT_1_PHASER_LFO_FREQ dspId:DSP_8]) {
                    knobPhaserLfoFreq.value = pkt->effect_1_phaser_lfo_freq;
                    [self knobPhaserLfoFreqDidChange:nil];
                }
                if (![viewController sendCommandExists:CMD_EFFECT_1_PHASER_LFO_TYPE dspId:DSP_8]) {
                    if (pkt->effect_1_phaser_lfo_type == 1) {
                        btnPhaserLfoType.selected = NO;
                    } else if (pkt->effect_1_phaser_lfo_type == 2) {
                        btnPhaserLfoType.selected = YES;
                    }
                }
                break;
            case EFFECT_VIBRATO:
                if ([viewController sendCommandExists:CMD_EFFECT_1_PROGRAM dspId:DSP_8]) {
                    NSLog(@"Vibrato command exists!");
                    break;
                }
                [btnReverbType setHidden:YES];
                [self onBtnEffect1Vibrato:nil];
                if (![viewController sendCommandExists:CMD_EFFECT_1_VIBRATO_FREQ dspId:DSP_8]) {
                    knobVibratoFreq.value = [Global freqToKnobValue:pkt->effect_1_vibrato_freq];
                    [self knobVibratoFreqDidChange:nil];
                }
                if (![viewController sendCommandExists:CMD_EFFECT_1_VIBRATO_DEPTH dspId:DSP_8]) {
                    knobVibratoDepth.value = pkt->effect_1_vibrato_depth;
                    [self knobVibratoDepthDidChange:nil];
                }
                if (![viewController sendCommandExists:CMD_EFFECT_1_VIBRATO_LFO_FREQ dspId:DSP_8]) {
                    knobVibratoLfoFreq.value = pkt->effect_1_vibrato_lfo_freq;
                    [self knobVibratoLfoFreqDidChange:nil];
                }
                if (![viewController sendCommandExists:CMD_EFFECT_1_VIBRATO_LFO_TYPE dspId:DSP_8]) {
                    if (pkt->effect_1_vibrato_lfo_type == 1) {
                        btnVibratoLfoType.selected = NO;
                    } else if (pkt->effect_1_vibrato_lfo_type == 2) {
                        btnVibratoLfoType.selected = YES;
                    }
                }
                break;
            case EFFECT_TREMOLO:
                if ([viewController sendCommandExists:CMD_EFFECT_1_PROGRAM dspId:DSP_8]) {
                    NSLog(@"Tremolo command exists!");
                    break;
                }
                [btnReverbType setHidden:YES];
                [self onBtnEffect1Tremolo:nil];
                if (![viewController sendCommandExists:CMD_EFFECT_1_TREMOLO_DEPTH dspId:DSP_8]) {
                    knobTremoloDepth.value = pkt->effect_1_tremolo_depth;
                    [self knobTremoloDepthDidChange:nil];
                }
                if (![viewController sendCommandExists:CMD_EFFECT_1_TREMOLO_LFO_FREQ dspId:DSP_8]) {
                    knobTremoloLfoFreq.value = pkt->effect_1_tremolo_lfo_freq;
                    [self knobTremoloLfoFreqDidChange:nil];
                }
                if (![viewController sendCommandExists:CMD_EFFECT_1_TREMOLO_LFO_TYPE dspId:DSP_8]) {
                    if (pkt->effect_1_tremolo_lfo_type == 1) {
                        btnTremoloLfoType.selected = NO;
                    } else if (pkt->effect_1_tremolo_lfo_type == 2) {
                        btnTremoloLfoType.selected = YES;
                    }
                }
                break;
            case EFFECT_AUTO_PAN:
                if ([viewController sendCommandExists:CMD_EFFECT_1_PROGRAM dspId:DSP_8]) {
                    NSLog(@"Auto pan command exists!");
                    break;
                }
                [btnReverbType setHidden:YES];
                [self onBtnEffect1AutoPan:nil];
                if (![viewController sendCommandExists:CMD_EFFECT_1_AUTOPAN_DEPTH dspId:DSP_8]) {
                    knobAutoPanDepth.value = pkt->effect_1_autopan_depth;
                    [self knobAutoPanDepthDidChange:nil];
                }
                if (![viewController sendCommandExists:CMD_EFFECT_1_AUTOPAN_LFO_FREQ dspId:DSP_8]) {
                    knobAutoPanLfoFreq.value = pkt->effect_1_autopan_lfo_freq;
                    [self knobAutoPanLfoFreqDidChange:nil];
                }
                if (![viewController sendCommandExists:CMD_EFFECT_1_AUTOPAN_WAY dspId:DSP_8]) {
                    knobAutoPanWay.value = pkt->effect_1_autopan_way;
                    [self knobAutoPanWayDidChange:nil];
                }
                if (![viewController sendCommandExists:CMD_EFFECT_1_AUTOPAN_LFO_TYPE dspId:DSP_8]) {
                    if (pkt->effect_1_autopan_lfo_type == 1) {
                        btnAutoPanLfoType.selected = NO;
                    } else if (pkt->effect_1_autopan_lfo_type == 2) {
                        btnAutoPanLfoType.selected = YES;
                    }
                }
                break;
            case EFFECT_GEQ_31:
                if ([viewController sendCommandExists:CMD_EFFECT_1_PROGRAM dspId:DSP_8]) {
                    NSLog(@"Geq 31 command exists!");
                    break;
                }
                [btnReverbType setHidden:YES];
                [self onBtnEffect1Geq31:nil];
                if (effectGeq31Controller != nil) {
                    [effectGeq31Controller processReply:pkt];
                }
                break;
            default:
                break;
        }
    } else if (effectNum == EFX_2) {
        meterInL.progress = ((float)pkt->effect_2_1_in_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
        meterInR.progress = ((float)pkt->effect_2_2_in_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
        meterOutL.progress = ((float)pkt->effect_2_1_out_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
        meterOutR.progress = ((float)pkt->effect_2_2_out_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
        if (![viewController sendCommandExists:CMD_EFFECT_2_DRY_WET_MIX dspId:DSP_5]) {
            sliderEffectDryWet.value = pkt->effect_2_dry_wet_mix;
            [self onSliderEffectDryWet:nil];
        }

        if (![viewController sendCommandExists:CMD_EFFECT_2_1_IN_SOURCE dspId:DSP_5]) {
            switch (pkt->effect_2_1_in_source) {
                case EFFECT_IN_NULL:
                    [self onBtnEffectInLNull:nil];
                    break;
                case EFFECT_IN_CH_1:
                    [self onBtnEffectInLCh1:nil];
                    break;
                case EFFECT_IN_CH_2:
                    [self onBtnEffectInLCh2:nil];
                    break;
                case EFFECT_IN_CH_3:
                    [self onBtnEffectInLCh3:nil];
                    break;
                case EFFECT_IN_CH_4:
                    [self onBtnEffectInLCh4:nil];
                    break;
                case EFFECT_IN_CH_5:
                    [self onBtnEffectInLCh5:nil];
                    break;
                case EFFECT_IN_CH_6:
                    [self onBtnEffectInLCh6:nil];
                    break;
                case EFFECT_IN_CH_7:
                    [self onBtnEffectInLCh7:nil];
                    break;
                case EFFECT_IN_CH_8:
                    [self onBtnEffectInLCh8:nil];
                    break;
                case EFFECT_IN_CH_9:
                    [self onBtnEffectInLCh9:nil];
                    break;
                case EFFECT_IN_CH_10:
                    [self onBtnEffectInLCh10:nil];
                    break;
                case EFFECT_IN_CH_11:
                    [self onBtnEffectInLCh11:nil];
                    break;
                case EFFECT_IN_CH_12:
                    [self onBtnEffectInLCh12:nil];
                    break;
                case EFFECT_IN_CH_13:
                    [self onBtnEffectInLCh13:nil];
                    break;
                case EFFECT_IN_CH_14:
                    [self onBtnEffectInLCh14:nil];
                    break;
                case EFFECT_IN_CH_15:
                    [self onBtnEffectInLCh15:nil];
                    break;
                case EFFECT_IN_CH_16:
                    [self onBtnEffectInLCh16:nil];
                    break;
                case EFFECT_IN_GP_1:
                    [self onBtnEffectInLGp1:nil];
                    break;
                case EFFECT_IN_GP_2:
                    [self onBtnEffectInLGp2:nil];
                    break;
                case EFFECT_IN_GP_3:
                    [self onBtnEffectInLGp3:nil];
                    break;
                case EFFECT_IN_GP_4:
                    [self onBtnEffectInLGp4:nil];
                    break;
                case EFFECT_IN_AUX_1:
                    [self onBtnEffectInLAux1:nil];
                    break;
                case EFFECT_IN_AUX_2:
                    [self onBtnEffectInLAux2:nil];
                    break;
                case EFFECT_IN_AUX_3:
                    [self onBtnEffectInLAux3:nil];
                    break;
                case EFFECT_IN_AUX_4:
                    [self onBtnEffectInLAux4:nil];
                    break;
                case EFFECT_IN_CH_17:
                    [self onBtnEffectInLCh17:nil];
                    break;
                case EFFECT_IN_CH_18:
                    [self onBtnEffectInLCh18:nil];
                    break;
                default:
                    break;
            }
        }
        
        if (![viewController sendCommandExists:CMD_EFFECT_2_2_IN_SOURCE dspId:DSP_5]) {
            switch (pkt->effect_2_2_in_source) {
                case EFFECT_IN_NULL:
                    [self onBtnEffectInRNull:nil];
                    break;
                case EFFECT_IN_CH_1:
                    [self onBtnEffectInRCh1:nil];
                    break;
                case EFFECT_IN_CH_2:
                    [self onBtnEffectInRCh2:nil];
                    break;
                case EFFECT_IN_CH_3:
                    [self onBtnEffectInRCh3:nil];
                    break;
                case EFFECT_IN_CH_4:
                    [self onBtnEffectInRCh4:nil];
                    break;
                case EFFECT_IN_CH_5:
                    [self onBtnEffectInRCh5:nil];
                    break;
                case EFFECT_IN_CH_6:
                    [self onBtnEffectInRCh6:nil];
                    break;
                case EFFECT_IN_CH_7:
                    [self onBtnEffectInRCh7:nil];
                    break;
                case EFFECT_IN_CH_8:
                    [self onBtnEffectInRCh8:nil];
                    break;
                case EFFECT_IN_CH_9:
                    [self onBtnEffectInRCh9:nil];
                    break;
                case EFFECT_IN_CH_10:
                    [self onBtnEffectInRCh10:nil];
                    break;
                case EFFECT_IN_CH_11:
                    [self onBtnEffectInRCh11:nil];
                    break;
                case EFFECT_IN_CH_12:
                    [self onBtnEffectInRCh12:nil];
                    break;
                case EFFECT_IN_CH_13:
                    [self onBtnEffectInRCh13:nil];
                    break;
                case EFFECT_IN_CH_14:
                    [self onBtnEffectInRCh14:nil];
                    break;
                case EFFECT_IN_CH_15:
                    [self onBtnEffectInRCh15:nil];
                    break;
                case EFFECT_IN_CH_16:
                    [self onBtnEffectInRCh16:nil];
                    break;
                case EFFECT_IN_GP_1:
                    [self onBtnEffectInRGp1:nil];
                    break;
                case EFFECT_IN_GP_2:
                    [self onBtnEffectInRGp2:nil];
                    break;
                case EFFECT_IN_GP_3:
                    [self onBtnEffectInRGp3:nil];
                    break;
                case EFFECT_IN_GP_4:
                    [self onBtnEffectInRGp4:nil];
                    break;
                case EFFECT_IN_AUX_1:
                    [self onBtnEffectInRAux1:nil];
                    break;
                case EFFECT_IN_AUX_2:
                    [self onBtnEffectInRAux2:nil];
                    break;
                case EFFECT_IN_AUX_3:
                    [self onBtnEffectInRAux3:nil];
                    break;
                case EFFECT_IN_AUX_4:
                    [self onBtnEffectInRAux4:nil];
                    break;
                case EFFECT_IN_CH_17:
                    [self onBtnEffectInRCh17:nil];
                    break;
                case EFFECT_IN_CH_18:
                    [self onBtnEffectInRCh18:nil];
                    break;
                default:
                    break;
            }
        }
        
        if (![viewController sendCommandExists:CMD_EFFECT_2_1_OUT_SOURCE dspId:DSP_5]) {
            btnEffectOutLGp1Status.selected = pkt->effect_2_1_out_source & 0x01;
            btnEffectOutLGp3Status.selected = (pkt->effect_2_1_out_source & 0x02) >> 1;
            btnEffectOutLMulti1Status.selected = (pkt->effect_2_1_out_source & 0x10) >> 4;
            btnEffectOutLMulti3Status.selected = (pkt->effect_2_1_out_source & 0x20) >> 5;
            btnEffectOutLMainLStatus.selected = (pkt->effect_2_1_out_source & 0x0100) >> 8;
        }
        if (![viewController sendCommandExists:CMD_EFFECT_2_2_OUT_SOURCE dspId:DSP_5]) {
            btnEffectOutRGp2Status.selected = pkt->effect_2_2_out_source & 0x01;
            btnEffectOutRGp4Status.selected = (pkt->effect_2_2_out_source & 0x02) >> 1;
            btnEffectOutRMulti2Status.selected = (pkt->effect_2_2_out_source & 0x10) >> 4;
            btnEffectOutRMulti4Status.selected = (pkt->effect_2_2_out_source & 0x20) >> 5;
            btnEffectOutRMainRStatus.selected = (pkt->effect_2_2_out_source & 0x0100) >> 8;
        }
        
        [btnReverbType setHidden:YES];
        
        switch (pkt->effect_2_program) {
            case EFFECT_ECHO_DELAY:
                if ([viewController sendCommandExists:CMD_EFFECT_2_PROGRAM dspId:DSP_5]) {
                    NSLog(@"Echo command exists!");
                    break;
                }
                [self onBtnEffect2Echo:nil];
                if (![viewController sendCommandExists:CMD_EFFECT_2_ECHO_DELAY_1_TIME dspId:DSP_5]) {
                    knobEchoTimeL.value = pkt->effect_2_echo_delay1_time;
                    [self knobEchoTimeLDidChange:nil];
                }
                if (![viewController sendCommandExists:CMD_EFFECT_2_ECHO_DELAY_2_TIME dspId:DSP_5]) {
                    knobEchoTimeR.value = pkt->effect_2_echo_delay2_time;
                    [self knobEchoTimeRDidChange:nil];
                }
                if (![viewController sendCommandExists:CMD_EFFECT_2_ECHO_DELAY_FB_1 dspId:DSP_5]) {
                    knobEchoFb1.value = pkt->effect_2_echo_delay_fb1;
                    [self knobEchoFb1DidChange:nil];
                }
                if (![viewController sendCommandExists:CMD_EFFECT_2_ECHO_DELAY_FB_2 dspId:DSP_5]) {
                    knobEchoFb2.value = pkt->effect_2_echo_delay_fb2;
                    [self knobEchoFb2DidChange:nil];
                }
                if (![viewController sendCommandExists:CMD_EFFECT_2_ECHO_DELAY_FB_HPF dspId:DSP_5]) {
                    knobEchoHpf.value = [Global freqToKnobValue:pkt->effect_2_echo_delay_fb_hpf];
                    [self knobEchoHpfDidChange:nil];
                }
                if (![viewController sendCommandExists:CMD_EFFECT_2_ECHO_DELAY_FB_LPF dspId:DSP_5]) {
                    knobEchoLpf.value = [Global freqToKnobValue:pkt->effect_2_echo_delay_fb_lpf];
                    [self knobEchoLpfDidChange:nil];
                }
                break;
            case EFFECT_TAP_DELAY:
                if ([viewController sendCommandExists:CMD_EFFECT_2_PROGRAM dspId:DSP_5]) {
                    NSLog(@"Tap delay command exists!");
                    break;
                }
                [self onBtnEffect2TapDelay:nil];
                knobTapFb.value = pkt->effect_2_tap_delay_fb;
                if (![viewController sendCommandExists:CMD_EFFECT_2_TAP_DELAY_FB_HPF dspId:DSP_5]) {
                    [self knobTapFbDidChange:nil];
                    knobTapHpf.value = [Global freqToKnobValue:pkt->effect_2_tap_delay_fb_hpf];
                }
                if (![viewController sendCommandExists:CMD_EFFECT_2_TAP_DELAY_FB_LPF dspId:DSP_5]) {
                    [self knobTapHpfDidChange:nil];
                    knobTapLpf.value = [Global freqToKnobValue:pkt->effect_2_tap_delay_fb_lpf];
                }
                if (![viewController sendCommandExists:CMD_EFFECT_2_TAP_KEY dspId:DSP_5]) {
                    [self knobTapLpfDidChange:nil];
                    btnTap.selected = pkt->effect_2_tap_key;
                }
                NSLog(@"Efx 2 tap delay = %d", pkt->effect_2_delay_time);
                if (pkt->effect_2_delay_time > 0) {
                    int curTapDelay = pkt->effect_2_delay_time / 10;
                    if (curTapDelay != _tapDelay) {
                        _tapDelay = curTapDelay;
                        [lblTapDelay setText:[NSString stringWithFormat:@"%dmS", _tapDelay]];
                        //[viewController sendData:CMD_EFFECT_2_TAP_DELAY_TIME commandType:DSP_WRITE dspId:DSP_5 value:_tapDelay*10];
                        if (_tapDelayTimer != nil) {
                            [_tapDelayTimer invalidate];
                            _tapDelayTimer = nil;
                        }
                        _tapDelayTimer = [NSTimer timerWithTimeInterval:(float)(_tapDelay/1000.0f) target:self selector:@selector(updateTapDelayLed) userInfo:nil repeats:YES];
                        [[NSRunLoop mainRunLoop] addTimer:_tapDelayTimer forMode:NSRunLoopCommonModes];
                    }
                }
                break;
            case EFFECT_CHORUS:
                if ([viewController sendCommandExists:CMD_EFFECT_2_PROGRAM dspId:DSP_5]) {
                    NSLog(@"Chorus command exists!");
                    break;
                }
                [self onBtnEffect2Chorus:nil];
                if (![viewController sendCommandExists:CMD_EFFECT_2_CHORUS_DEPTH dspId:DSP_5]) {
                    knobChorusPreDelay.value = pkt->effect_2_chorus_pre_delay;
                    [self knobChorusPreDelayDidChange:nil];
                }
                if (![viewController sendCommandExists:CMD_EFFECT_2_CHORUS_LPF_FREQ dspId:DSP_5]) {
                    knobChorusDepth.value = pkt->effect_2_chorus_depth;
                    [self knobChorusDepthDidChange:nil];
                }
                if (![viewController sendCommandExists:CMD_EFFECT_2_CHORUS_LFO_PHASE dspId:DSP_5]) {
                    knobChorusLpf.value = [Global freqToKnobValue:pkt->effect_2_chorus_lpf_freq];
                    [self knobChorusLpfDidChange:nil];
                }
                if (![viewController sendCommandExists:CMD_EFFECT_2_CHORUS_LFO_PHASE dspId:DSP_5]) {
                    knobChorusLfoPhase.value = pkt->effect_2_chorus_lfo_phase;
                    [self knobChorusLfoPhaseDidChange:nil];
                }
                if (![viewController sendCommandExists:CMD_EFFECT_2_CHORUS_LFO_FREQ dspId:DSP_5]) {
                    knobChorusLfoFreq.value = pkt->effect_2_chorus_lfo_freq;
                    [self knobChorusLfoFreqDidChange:nil];
                }
                if (![viewController sendCommandExists:CMD_EFFECT_2_CHORUS_LFO_TYPE dspId:DSP_5]) {
                    if (pkt->effect_2_chorus_lfo_type == 1) {
                        btnChorusLfoType.selected = NO;
                    } else if (pkt->effect_2_chorus_lfo_type == 2) {
                        btnChorusLfoType.selected = YES;
                    }
                }
                break;
            case EFFECT_FLANGER:
                if ([viewController sendCommandExists:CMD_EFFECT_2_PROGRAM dspId:DSP_5]) {
                    NSLog(@"Flanger command exists!");
                    break;
                }
                [self onBtnEffect2Flanger:nil];
                if (![viewController sendCommandExists:CMD_EFFECT_2_FLANGER_PRE_DELAY dspId:DSP_5]) {
                    knobFlangerPreDelay.value = pkt->effect_2_flanger_pre_delay;
                    [self knobFlangerPreDelayDidChange:nil];
                }
                if (![viewController sendCommandExists:CMD_EFFECT_2_FLANGER_LPF_FREQ dspId:DSP_5]) {
                    knobFlangerLpf.value = [Global freqToKnobValue:pkt->effect_2_flanger_lpf_freq];
                    [self knobFlangerLpfDidChange:nil];
                }
                if (![viewController sendCommandExists:CMD_EFFECT_2_FLANGER_DEPTH dspId:DSP_5]) {
                    knobFlangerDepth.value = pkt->effect_2_flanger_depth;
                    [self knobFlangerDepthDidChange:nil];
                }
                if (![viewController sendCommandExists:CMD_EFFECT_2_FLANGER_FB dspId:DSP_5]) {
                    knobFlangerFb.value = pkt->effect_2_flanger_fb;
                    [self knobFlangerFbDidChange:nil];
                }
                if (![viewController sendCommandExists:CMD_EFFECT_2_FLANGER_LFO_PHASE dspId:DSP_5]) {
                    knobFlangerLfoPhase.value = pkt->effect_2_flanger_lfo_phase;
                    [self knobFlangerLfoPhaseDidChange:nil];
                }
                if (![viewController sendCommandExists:CMD_EFFECT_2_FLANGER_LFO_FREQ dspId:DSP_5]) {
                    knobFlangerLfoFreq.value = pkt->effect_2_flanger_lfo_freq;
                    [self knobFlangerLfoFreqDidChange:nil];
                }
                if (![viewController sendCommandExists:CMD_EFFECT_2_FLANGER_LFO_TYPE dspId:DSP_5]) {
                    if (pkt->effect_2_flanger_lfo_type == 1) {
                        btnFlangerLfoType.selected = NO;
                    } else if (pkt->effect_2_flanger_lfo_type == 2) {
                        btnFlangerLfoType.selected = YES;
                    }
                }
                break;
            case EFFECT_PHASER:
                if ([viewController sendCommandExists:CMD_EFFECT_2_PROGRAM dspId:DSP_5]) {
                    NSLog(@"Phaser command exists!");
                    break;
                }
                [self onBtnEffect2Phaser:nil];
                if (![viewController sendCommandExists:CMD_EFFECT_2_PHASER_STAGE_NO dspId:DSP_5]) {
                    knobPhaserStageNo.value = pkt->effect_2_phaser_stage_no;
                    [self knobPhaserStageNoDidChange:nil];
                }
                if (![viewController sendCommandExists:CMD_EFFECT_2_PHASER_FREQ dspId:DSP_5]) {
                    knobPhaserFreq.value = [Global freqToKnobValue:pkt->effect_2_phaser_freq];
                    [self knobPhaserFreqDidChange:nil];
                }
                if (![viewController sendCommandExists:CMD_EFFECT_2_PHASER_DEPTH dspId:DSP_5]) {
                    knobPhaserDepth.value = pkt->effect_2_phaser_depth;
                    [self knobPhaserDepthDidChange:nil];
                }
                if (![viewController sendCommandExists:CMD_EFFECT_2_PHASER_LFO_FREQ dspId:DSP_5]) {
                    knobPhaserLfoFreq.value = pkt->effect_2_phaser_lfo_freq;
                    [self knobPhaserLfoFreqDidChange:nil];
                }
                if (![viewController sendCommandExists:CMD_EFFECT_2_PHASER_LFO_TYPE dspId:DSP_5]) {
                    if (pkt->effect_2_phaser_lfo_type == 1) {
                        btnPhaserLfoType.selected = NO;
                    } else if (pkt->effect_2_phaser_lfo_type == 2) {
                        btnPhaserLfoType.selected = YES;
                    }
                }
                break;
            case EFFECT_VIBRATO:
                if ([viewController sendCommandExists:CMD_EFFECT_2_PROGRAM dspId:DSP_5]) {
                    NSLog(@"Vibrato command exists!");
                    break;
                }
                [self onBtnEffect2Vibrato:nil];
                if (![viewController sendCommandExists:CMD_EFFECT_2_VIBRATO_FREQ dspId:DSP_5]) {
                    knobVibratoFreq.value = [Global freqToKnobValue:pkt->effect_2_vibrato_freq];
                    [self knobVibratoFreqDidChange:nil];
                }
                if (![viewController sendCommandExists:CMD_EFFECT_2_VIBRATO_DEPTH dspId:DSP_5]) {
                    knobVibratoDepth.value = pkt->effect_2_vibrato_depth;
                    [self knobVibratoDepthDidChange:nil];
                }
                if (![viewController sendCommandExists:CMD_EFFECT_2_VIBRATO_LFO_FREQ dspId:DSP_5]) {
                    knobVibratoLfoFreq.value = pkt->effect_2_vibrato_lfo_freq;
                    [self knobVibratoLfoFreqDidChange:nil];
                }
                if (![viewController sendCommandExists:CMD_EFFECT_2_VIBRATO_LFO_TYPE dspId:DSP_5]) {
                    if (pkt->effect_2_vibrato_lfo_type == 1) {
                        btnVibratoLfoType.selected = NO;
                    } else if (pkt->effect_2_vibrato_lfo_type == 2) {
                        btnVibratoLfoType.selected = YES;
                    }
                }
                break;
            case EFFECT_TREMOLO:
                if ([viewController sendCommandExists:CMD_EFFECT_2_PROGRAM dspId:DSP_5]) {
                    NSLog(@"Tremolo command exists!");
                    break;
                }
                [self onBtnEffect2Tremolo:nil];
                if (![viewController sendCommandExists:CMD_EFFECT_2_TREMOLO_DEPTH dspId:DSP_5]) {
                    knobTremoloDepth.value = pkt->effect_2_tremolo_depth;
                    [self knobTremoloDepthDidChange:nil];
                }
                if (![viewController sendCommandExists:CMD_EFFECT_2_TREMOLO_LFO_FREQ dspId:DSP_5]) {
                    knobTremoloLfoFreq.value = pkt->effect_2_tremolo_lfo_freq;
                    [self knobTremoloLfoFreqDidChange:nil];
                }
                if (![viewController sendCommandExists:CMD_EFFECT_2_TREMOLO_LFO_TYPE dspId:DSP_5]) {
                    if (pkt->effect_2_tremolo_lfo_type == 1) {
                        btnTremoloLfoType.selected = NO;
                    } else if (pkt->effect_2_tremolo_lfo_type == 2) {
                        btnTremoloLfoType.selected = YES;
                    }
                }
                break;
            case EFFECT_AUTO_PAN:
                if ([viewController sendCommandExists:CMD_EFFECT_2_PROGRAM dspId:DSP_5]) {
                    NSLog(@"Auto pan command exists!");
                    break;
                }
                [self onBtnEffect2AutoPan:nil];
                if (![viewController sendCommandExists:CMD_EFFECT_2_AUTOPAN_DEPTH dspId:DSP_5]) {
                    knobAutoPanDepth.value = pkt->effect_2_autopan_depth;
                    [self knobAutoPanDepthDidChange:nil];
                }
                if (![viewController sendCommandExists:CMD_EFFECT_2_AUTOPAN_LFO_FREQ dspId:DSP_5]) {
                    knobAutoPanLfoFreq.value = pkt->effect_2_autopan_lfo_freq;
                    [self knobAutoPanLfoFreqDidChange:nil];
                }
                if (![viewController sendCommandExists:CMD_EFFECT_2_AUTOPAN_WAY dspId:DSP_5]) {
                    knobAutoPanWay.value = pkt->effect_2_autopan_way;
                    [self knobAutoPanWayDidChange:nil];
                }
                if (![viewController sendCommandExists:CMD_EFFECT_2_AUTOPAN_LFO_TYPE dspId:DSP_5]) {
                    if (pkt->effect_2_autopan_lfo_type == 1) {
                        btnAutoPanLfoType.selected = NO;
                    } else if (pkt->effect_2_autopan_lfo_type == 2) {
                        btnAutoPanLfoType.selected = YES;
                    }
                }
                break;
            case EFFECT_GEQ_15:
                if ([viewController sendCommandExists:CMD_EFFECT_2_PROGRAM dspId:DSP_5]) {
                    NSLog(@"Geq 15 command exists!");
                    break;
                }
                [self onBtnEffect2Geq15:nil];
                if (effectGeq15Controller != nil) {
                    [effectGeq15Controller processReply:pkt];
                }
                break;
            default:
                break;
        }
    }
}

- (void)clearEffectInLButtons {
    btnEffectInLCh1.selected = NO; btnEffectInLCh2.selected = NO;
    btnEffectInLCh3.selected = NO; btnEffectInLCh4.selected = NO;
    btnEffectInLCh5.selected = NO; btnEffectInLCh6.selected = NO;
    btnEffectInLCh7.selected = NO; btnEffectInLCh8.selected = NO;
    btnEffectInLCh9.selected = NO; btnEffectInLCh10.selected = NO;
    btnEffectInLCh11.selected = NO; btnEffectInLCh12.selected = NO;
    btnEffectInLCh13.selected = NO; btnEffectInLCh14.selected = NO;
    btnEffectInLCh15.selected = NO; btnEffectInLCh16.selected = NO;
    btnEffectInLCh17.selected = NO; btnEffectInLCh18.selected = NO;
    btnEffectInLGp1.selected = NO; btnEffectInLGp2.selected = NO;
    btnEffectInLGp3.selected = NO; btnEffectInLGp4.selected = NO;
    btnEffectInLAux1.selected = NO; btnEffectInLAux2.selected = NO;
    btnEffectInLAux3.selected = NO; btnEffectInLAux4.selected = NO;
    btnEffectInLNull.selected = NO;
}

- (void)clearEffectInRButtons {
    btnEffectInRCh1.selected = NO; btnEffectInRCh2.selected = NO;
    btnEffectInRCh3.selected = NO; btnEffectInRCh4.selected = NO;
    btnEffectInRCh5.selected = NO; btnEffectInRCh6.selected = NO;
    btnEffectInRCh7.selected = NO; btnEffectInRCh8.selected = NO;
    btnEffectInRCh9.selected = NO; btnEffectInRCh10.selected = NO;
    btnEffectInRCh11.selected = NO; btnEffectInRCh12.selected = NO;
    btnEffectInRCh13.selected = NO; btnEffectInRCh14.selected = NO;
    btnEffectInRCh15.selected = NO; btnEffectInRCh16.selected = NO;
    btnEffectInRCh17.selected = NO; btnEffectInRCh18.selected = NO;
    btnEffectInRGp1.selected = NO; btnEffectInRGp2.selected = NO;
    btnEffectInRGp3.selected = NO; btnEffectInRGp4.selected = NO;
    btnEffectInRAux1.selected = NO; btnEffectInRAux2.selected = NO;
    btnEffectInRAux3.selected = NO; btnEffectInRAux4.selected = NO;
    btnEffectInRNull.selected = NO;
}

- (void)hideBottomViews {
    [viewReverb setHidden:YES];
    [viewEcho setHidden:YES];
    [viewTapDelay setHidden:YES];
    [viewChorus setHidden:YES];
    [viewFlanger setHidden:YES];
    [viewPhaser setHidden:YES];
    [viewTremolo setHidden:YES];
    [viewVibrato setHidden:YES];
    [viewAutoPan setHidden:YES];
    [viewGeq31 setHidden:YES];
    [viewGeq15 setHidden:YES];
}

- (void)clearEffect1Buttons {
    btnEffect1ReverbRoom.selected = NO;
    btnEffect1ReverbHall.selected = NO;
    btnEffect1ReverbPlate.selected = NO;
    btnEffect1Echo.selected = NO;
    btnEffect1TapDelay.selected = NO;
    btnEffect1Chorus.selected = NO;
    btnEffect1Flanger.selected = NO;
    btnEffect1Phaser.selected = NO;
    btnEffect1Tremolo.selected = NO;
    btnEffect1Vibrato.selected = NO;
    btnEffect1AutoPan.selected = NO;
    btnEffect1Geq31.selected = NO;
}

- (void)clearEffect2Buttons {
    btnEffect2Echo.selected = NO;
    btnEffect2TapDelay.selected = NO;
    btnEffect2Chorus.selected = NO;
    btnEffect2Flanger.selected = NO;
    btnEffect2Phaser.selected = NO;
    btnEffect2Tremolo.selected = NO;
    btnEffect2Vibrato.selected = NO;
    btnEffect2AutoPan.selected = NO;
    btnEffect2Geq15.selected = NO;
}

- (void)clearReverbTypeButtons {
    btnReverbType1.selected = NO;
    btnReverbType2.selected = NO;
    btnReverbType3.selected = NO;
    btnReverbType4.selected = NO;
    btnReverbType5.selected = NO;
    btnReverbType6.selected = NO;
    btnReverbType7.selected = NO;
    btnReverbType8.selected = NO;
}

- (void)updateTapDelayLed {
    _ledOnOff = !_ledOnOff;
    if (_ledOnOff) {
        // Blink LED
        CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"opacity"];
        [animation setFromValue:[NSNumber numberWithFloat:0.0]];
        [animation setToValue:[NSNumber numberWithFloat:1.0]];
        [animation setDuration:0.05f];
        [animation setTimingFunction:[CAMediaTimingFunction
                                      functionWithName:kCAMediaTimingFunctionLinear]];
        [animation setAutoreverses:YES];
        [animation setRepeatCount:1];
        [[imgTapLed layer] addAnimation:animation forKey:@"opacity"];
    } else {
        [imgTapLed setAlpha:0.0f];
    }
}

#pragma mark -
#pragma mark Public methods

- (void)clearDialogViews {
    [viewEffectInL setHidden:YES];
    [viewEffectInR setHidden:YES];
    [viewEffect1 setHidden:YES];
    [viewEffect2 setHidden:YES];
}

- (void)setEffectNum:(EFFECT_NUM)num {
    effectNum = num;
    [self clearDialogViews];
    if (effectNum == EFX_1) {
        [self onBtnEffect1ReverbRoom:nil];
    } else {
        [self onBtnEffect2Echo:nil];
    }
}

- (void)setEffectSolo:(BOOL)value {
    if (effectGeq31Controller != nil && effectNum == EFX_1) {
        [effectGeq31Controller setGeqSoloOnOff:value];
    } else if (effectGeq15Controller != nil && effectNum == EFX_2) {
        [effectGeq15Controller setGeqSoloOnOff:value];
    }
}

- (void)setEffectOnOff:(BOOL)value {
    if (effectGeq31Controller != nil && effectNum == EFX_1) {
        [effectGeq31Controller setGeqOnOff:value];
    } else if (effectGeq15Controller != nil && effectNum == EFX_2) {
        [effectGeq15Controller setGeqOnOff:value];
    }
}

/*
- (void)setLinkMode:(int)linkMode effect:(EFFECT_NUM)effect {
    if (effect == EFX_1) {
        if (effectGeq31Controller != nil && effectNum == EFX_1) {
            [effectGeq31Controller setLinkMode:linkMode];
        }
    } else if (effect == EFX_2) {
        if (effectGeq15Controller != nil && effectNum == EFX_2) {
            [effectGeq15Controller setLinkMode:linkMode];
        }
    }
}
*/

#pragma mark -
#pragma mark Effect panel event handler methods

- (IBAction)onBtnFile:(id)sender {
    if (effectNum == EFX_1) {
        [viewController setFileSceneMode:SCENE_MODE_EFFECT_1 sceneChannel:0];
    } else {
        [viewController setFileSceneMode:SCENE_MODE_EFFECT_2 sceneChannel:0];
    }
    [viewController.view bringSubviewToFront:viewController.viewScenes];
    [viewController.viewScenes setHidden:NO];
}

- (IBAction)onBtnReset:(id)sender {
    switch (effectType) {
        case EFFECT_REVERB:
            knobHpf.value = HPF_FREQ_DEFAULT;
            knobLpf.value = LPF_FREQ_DEFAULT;
            knobPreDelay.value = PRE_DELAY_DEFAULT;
            knobEarlyOut.value = REVERB_EARLY_OUT_DEFAULT;
            knobReverbTime.value = REVERB_TIME_DEFAULT;
            knobLevel.value = REVERB_LEVEL_DEFAULT;
            knobHiRatio.value = REVERB_HIRATIO_DEFAULT;
            knobDensity.value = REVERB_DENSITY_DEFAULT;
            knobThreshold.value = REVERB_GATE_THRESHOLD_DEFAULT;
            knobHold.value = REVERB_GATE_HOLD_DEFAULT;
            btnGate.selected = NO;
            [self knobHpfDidChange:self];
            [self knobLpfDidChange:self];
            [self knobPreDelayDidChange:self];
            [self knobEarlyOutDidChange:self];
            [self knobReverbTimeDidChange:self];
            [self knobLevelDidChange:self];
            [self knobHiRatioDidChange:self];
            [self knobDensityDidChange:self];
            [self knobThresholdDidChange:self];
            [self knobHoldDidChange:self];
            [viewController sendData:CMD_EFFECT_1_REVERB_GATE commandType:DSP_WRITE dspId:DSP_8 value:btnGate.selected];
            break;
        case EFFECT_ECHO_DELAY:
            knobEchoTimeL.value = ECHO_DELAY_DEFAULT;
            knobEchoTimeR.value = ECHO_DELAY_DEFAULT;
            knobEchoFb1.value = ECHO_DELAY_FB_DEFAULT;
            knobEchoFb2.value = ECHO_DELAY_FB_DEFAULT;
            knobEchoHpf.value = HPF_FREQ_DEFAULT;
            knobEchoLpf.value = LPF_FREQ_DEFAULT;
            [self knobEchoTimeLDidChange:self];
            [self knobEchoTimeRDidChange:self];
            [self knobEchoFb1DidChange:self];
            [self knobEchoFb2DidChange:self];
            [self knobEchoHpfDidChange:self];
            [self knobEchoLpfDidChange:self];
            break;
        case EFFECT_TAP_DELAY:
            knobTapFb.value = ECHO_DELAY_FB_DEFAULT;
            knobTapHpf.value = HPF_FREQ_DEFAULT;
            knobTapLpf.value = LPF_FREQ_DEFAULT;
            [self knobTapFbDidChange:self];
            [self knobTapHpfDidChange:self];
            [self knobTapLpfDidChange:self];
            break;
        case EFFECT_CHORUS:
            knobChorusDepth.value = CHORUS_DEPTH_DEFAULT;
            knobChorusPreDelay.value = CHORUS_PRE_DELAY_DEFAULT;
            knobChorusLpf.value = LPF_FREQ_DEFAULT;
            knobChorusLfoPhase.value = CHORUS_PHASE_DEFAULT;
            knobChorusLfoFreq.value = CHORUS_LFO_FREQ_DEFAULT;
            [self knobChorusDepthDidChange:self];
            [self knobChorusPreDelayDidChange:self];
            [self knobChorusLpfDidChange:self];
            [self knobChorusLfoPhaseDidChange:self];
            [self knobChorusLfoFreqDidChange:self];
            break;
        case EFFECT_FLANGER:
            knobFlangerDepth.value = CHORUS_DEPTH_DEFAULT;
            knobFlangerPreDelay.value = CHORUS_PRE_DELAY_DEFAULT;
            knobFlangerLpf.value = LPF_FREQ_DEFAULT;
            knobFlangerLfoPhase.value = CHORUS_PHASE_DEFAULT;
            knobFlangerLfoFreq.value = CHORUS_LFO_FREQ_DEFAULT;
            knobFlangerFb.value = CHORUS_DEPTH_DEFAULT;
            [self knobFlangerDepthDidChange:self];
            [self knobFlangerPreDelayDidChange:self];
            [self knobFlangerLpfDidChange:self];
            [self knobFlangerLfoPhaseDidChange:self];
            [self knobFlangerLfoFreqDidChange:self];
            [self knobFlangerFbDidChange:self];
            break;
        case EFFECT_PHASER:
            knobPhaserDepth.value = CHORUS_DEPTH_DEFAULT;
            knobPhaserFreq.value = HPF_FREQ_DEFAULT;
            knobPhaserLfoFreq.value = CHORUS_PHASE_DEFAULT;
            knobPhaserStageNo.value = PHASER_STAGENO_DEFAULT;
            [self knobPhaserDepthDidChange:self];
            [self knobPhaserFreqDidChange:self];
            [self knobPhaserLfoFreqDidChange:self];
            [self knobPhaserStageNoDidChange:self];
            break;
        case EFFECT_VIBRATO:
            knobVibratoDepth.value = CHORUS_DEPTH_DEFAULT;
            knobVibratoFreq.value = HPF_FREQ_DEFAULT;
            knobVibratoLfoFreq.value = CHORUS_LFO_FREQ_DEFAULT;
            [self knobVibratoDepthDidChange:self];
            [self knobVibratoFreqDidChange:self];
            [self knobVibratoLfoFreqDidChange:self];
            break;
        case EFFECT_TREMOLO:
            knobTremoloDepth.value = CHORUS_DEPTH_DEFAULT;
            knobTremoloLfoFreq.value = CHORUS_LFO_FREQ_DEFAULT;
            [self knobTremoloDepthDidChange:self];
            [self knobTremoloLfoFreqDidChange:self];
            break;
        case EFFECT_AUTO_PAN:
            knobAutoPanDepth.value = CHORUS_DEPTH_DEFAULT;
            knobAutoPanLfoFreq.value = CHORUS_LFO_FREQ_DEFAULT;
            knobAutoPanWay.value = AUTOPAN_WAY_DEFAULT;
            [self knobAutoPanDepthDidChange:self];
            [self knobAutoPanLfoFreqDidChange:self];
            [self knobAutoPanWayDidChange:self];
            break;
        case EFFECT_GEQ_31:
            if (effectNum == EFX_1) {
                [effectGeq31Controller reset];
            } else {
                [effectGeq15Controller reset];
            }
            break;
        default:
            break;
    }
}

- (IBAction)onBtnEffectInL:(id)sender {
    [viewEffect1 setHidden:YES];
    [viewEffect2 setHidden:YES];
    [viewReverbType setHidden:YES];
    [viewEffectInR setHidden:YES];
    [viewEffectInL setHidden:!viewEffectInL.isHidden];
}

- (IBAction)onBtnEffectInR:(id)sender {
    [viewEffect1 setHidden:YES];
    [viewEffect2 setHidden:YES];
    [viewReverbType setHidden:YES];
    [viewEffectInL setHidden:YES];
    [viewEffectInR setHidden:!viewEffectInR.isHidden];
}

- (IBAction)onBtnEffect:(id)sender {
    if (effectNum == EFX_1) {
        [viewEffect2 setHidden:YES];
        [viewReverbType setHidden:YES];
        [viewEffectInL setHidden:YES];
        [viewEffectInR setHidden:YES];
        [viewEffect1 setHidden:!viewEffect1.isHidden];
    } else {
        [viewEffect1 setHidden:YES];
        [viewReverbType setHidden:YES];
        [viewEffectInL setHidden:YES];
        [viewEffectInR setHidden:YES];
        [viewEffect2 setHidden:!viewEffect2.isHidden];
    }
}

- (IBAction)onBtnReverbType:(id)sender {
    [viewEffect1 setHidden:YES];
    [viewEffect2 setHidden:YES];
    [viewEffectInL setHidden:YES];
    [viewEffectInR setHidden:YES];
    [viewReverbType setHidden:!viewReverbType.isHidden];
}

- (IBAction)onSliderEffectDryWet:(id)sender {
    int value = (int)(sliderEffectDryWet.value);
    [lblEffectDryWet setText:[NSString stringWithFormat:@"%d%%", value]];
    if (sender) {
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_DRY_WET_MIX commandType:DSP_WRITE dspId:DSP_5 value:sliderEffectDryWet.value];
        } else {
            [viewController sendData:CMD_EFFECT_2_DRY_WET_MIX commandType:DSP_WRITE dspId:DSP_5 value:sliderEffectDryWet.value];
        }
    }
}

#pragma mark -
#pragma mark Effect In L event handler methods

- (IBAction)onBtnEffectInLCh1:(id)sender {
    [self clearEffectInLButtons];
    btnEffectInLCh1.selected = YES;
    [btnEffectInL setTitle:@"CH 1" forState:UIControlStateNormal];
    effectInL = EFFECT_IN_CH_1;
    if (sender) {
        [viewEffectInL setHidden:YES];
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_1_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInL];
        } else {
            [viewController sendData:CMD_EFFECT_2_1_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInL];
        }
    }
}

- (IBAction)onBtnEffectInLCh2:(id)sender {
    [self clearEffectInLButtons];
    btnEffectInLCh2.selected = YES;
    [btnEffectInL setTitle:@"CH 2" forState:UIControlStateNormal];
    effectInL = EFFECT_IN_CH_2;
    if (sender) {
        [viewEffectInL setHidden:YES];
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_1_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInL];
        } else {
            [viewController sendData:CMD_EFFECT_2_1_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInL];
        }
    }
}

- (IBAction)onBtnEffectInLCh3:(id)sender {
    [self clearEffectInLButtons];
    btnEffectInLCh3.selected = YES;
    [btnEffectInL setTitle:@"CH 3" forState:UIControlStateNormal];
    effectInL = EFFECT_IN_CH_3;
    if (sender) {
        [viewEffectInL setHidden:YES];
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_1_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInL];
        } else {
            [viewController sendData:CMD_EFFECT_2_1_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInL];
        }
    }
}

- (IBAction)onBtnEffectInLCh4:(id)sender {
    [self clearEffectInLButtons];
    btnEffectInLCh4.selected = YES;
    [btnEffectInL setTitle:@"CH 4" forState:UIControlStateNormal];
    effectInL = EFFECT_IN_CH_4;
    if (sender) {
        [viewEffectInL setHidden:YES];
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_1_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInL];
        } else {
            [viewController sendData:CMD_EFFECT_2_1_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInL];
        }
    }
}

- (IBAction)onBtnEffectInLCh5:(id)sender {
    [self clearEffectInLButtons];
    btnEffectInLCh5.selected = YES;
    [btnEffectInL setTitle:@"CH 5" forState:UIControlStateNormal];
    effectInL = EFFECT_IN_CH_5;
    if (sender) {
        [viewEffectInL setHidden:YES];
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_1_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInL];
        } else {
            [viewController sendData:CMD_EFFECT_2_1_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInL];
        }
    }
}

- (IBAction)onBtnEffectInLCh6:(id)sender {
    [self clearEffectInLButtons];
    btnEffectInLCh6.selected = YES;
    [btnEffectInL setTitle:@"CH 6" forState:UIControlStateNormal];
    effectInL = EFFECT_IN_CH_6;
    if (sender) {
        [viewEffectInL setHidden:YES];
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_1_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInL];
        } else {
            [viewController sendData:CMD_EFFECT_2_1_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInL];
        }
    }
}

- (IBAction)onBtnEffectInLCh7:(id)sender {
    [self clearEffectInLButtons];
    btnEffectInLCh7.selected = YES;
    [btnEffectInL setTitle:@"CH 7" forState:UIControlStateNormal];
    effectInL = EFFECT_IN_CH_7;
    if (sender) {
        [viewEffectInL setHidden:YES];
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_1_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInL];
        } else {
            [viewController sendData:CMD_EFFECT_2_1_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInL];
        }
    }
}

- (IBAction)onBtnEffectInLCh8:(id)sender {
    [self clearEffectInLButtons];
    btnEffectInLCh8.selected = YES;
    [btnEffectInL setTitle:@"CH 8" forState:UIControlStateNormal];
    effectInL = EFFECT_IN_CH_8;
    if (sender) {
        [viewEffectInL setHidden:YES];
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_1_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInL];
        } else {
            [viewController sendData:CMD_EFFECT_2_1_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInL];
        }
    }
}

- (IBAction)onBtnEffectInLCh9:(id)sender {
    [self clearEffectInLButtons];
    btnEffectInLCh9.selected = YES;
    [btnEffectInL setTitle:@"CH 9" forState:UIControlStateNormal];
    effectInL = EFFECT_IN_CH_9;
    if (sender) {
        [viewEffectInL setHidden:YES];
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_1_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInL];
        } else {
            [viewController sendData:CMD_EFFECT_2_1_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInL];
        }
    }
}

- (IBAction)onBtnEffectInLCh10:(id)sender {
    [self clearEffectInLButtons];
    btnEffectInLCh10.selected = YES;
    [btnEffectInL setTitle:@"CH 10" forState:UIControlStateNormal];
    effectInL = EFFECT_IN_CH_10;
    if (sender) {
        [viewEffectInL setHidden:YES];
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_1_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInL];
        } else {
            [viewController sendData:CMD_EFFECT_2_1_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInL];
        }
    }
}

- (IBAction)onBtnEffectInLCh11:(id)sender {
    [self clearEffectInLButtons];
    btnEffectInLCh11.selected = YES;
    [btnEffectInL setTitle:@"CH 11" forState:UIControlStateNormal];
    effectInL = EFFECT_IN_CH_11;
    if (sender) {
        [viewEffectInL setHidden:YES];
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_1_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInL];
        } else {
            [viewController sendData:CMD_EFFECT_2_1_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInL];
        }
    }
}

- (IBAction)onBtnEffectInLCh12:(id)sender {
    [self clearEffectInLButtons];
    btnEffectInLCh12.selected = YES;
    [btnEffectInL setTitle:@"CH 12" forState:UIControlStateNormal];
    effectInL = EFFECT_IN_CH_12;
    if (sender) {
        [viewEffectInL setHidden:YES];
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_1_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInL];
        } else {
            [viewController sendData:CMD_EFFECT_2_1_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInL];
        }
    }
}

- (IBAction)onBtnEffectInLCh13:(id)sender {
    [self clearEffectInLButtons];
    btnEffectInLCh13.selected = YES;
    [btnEffectInL setTitle:@"CH 13" forState:UIControlStateNormal];
    effectInL = EFFECT_IN_CH_13;
    if (sender) {
        [viewEffectInL setHidden:YES];
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_1_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInL];
        } else {
            [viewController sendData:CMD_EFFECT_2_1_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInL];
        }
    }
}

- (IBAction)onBtnEffectInLCh14:(id)sender {
    [self clearEffectInLButtons];
    btnEffectInLCh14.selected = YES;
    [btnEffectInL setTitle:@"CH 14" forState:UIControlStateNormal];
    effectInL = EFFECT_IN_CH_14;
    if (sender) {
        [viewEffectInL setHidden:YES];
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_1_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInL];
        } else {
            [viewController sendData:CMD_EFFECT_2_1_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInL];
        }
    }
}

- (IBAction)onBtnEffectInLCh15:(id)sender {
    [self clearEffectInLButtons];
    btnEffectInLCh15.selected = YES;
    [btnEffectInL setTitle:@"CH 15" forState:UIControlStateNormal];
    effectInL = EFFECT_IN_CH_15;
    if (sender) {
        [viewEffectInL setHidden:YES];
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_1_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInL];
        } else {
            [viewController sendData:CMD_EFFECT_2_1_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInL];
        }
    }
}

- (IBAction)onBtnEffectInLCh16:(id)sender {
    [self clearEffectInLButtons];
    btnEffectInLCh16.selected = YES;
    [btnEffectInL setTitle:@"CH 16" forState:UIControlStateNormal];
    effectInL = EFFECT_IN_CH_16;
    if (sender) {
        [viewEffectInL setHidden:YES];
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_1_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInL];
        } else {
            [viewController sendData:CMD_EFFECT_2_1_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInL];
        }
    }
}

- (IBAction)onBtnEffectInLCh17:(id)sender {
    [self clearEffectInLButtons];
    btnEffectInLCh17.selected = YES;
    [btnEffectInL setTitle:@"CH 17" forState:UIControlStateNormal];
    effectInL = EFFECT_IN_CH_17;
    if (sender) {
        [viewEffectInL setHidden:YES];
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_1_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInL];
        } else {
            [viewController sendData:CMD_EFFECT_2_1_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInL];
        }
    }
}

- (IBAction)onBtnEffectInLCh18:(id)sender {
    [self clearEffectInLButtons];
    btnEffectInLCh18.selected = YES;
    [btnEffectInL setTitle:@"CH 18" forState:UIControlStateNormal];
    effectInL = EFFECT_IN_CH_18;
    if (sender) {
        [viewEffectInL setHidden:YES];
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_1_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInL];
        } else {
            [viewController sendData:CMD_EFFECT_2_1_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInL];
        }
    }
}

- (IBAction)onBtnEffectInLGp1:(id)sender {
    [self clearEffectInLButtons];
    btnEffectInLGp1.selected = YES;
    [btnEffectInL setTitle:@"GP 1" forState:UIControlStateNormal];
    effectInL = EFFECT_IN_GP_1;
    if (sender) {
        [viewEffectInL setHidden:YES];
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_1_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInL];
        } else {
            [viewController sendData:CMD_EFFECT_2_1_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInL];
        }
    }
}

- (IBAction)onBtnEffectInLGp2:(id)sender {
    [self clearEffectInLButtons];
    btnEffectInLGp2.selected = YES;
    [btnEffectInL setTitle:@"GP 2" forState:UIControlStateNormal];
    effectInL = EFFECT_IN_GP_2;
    if (sender) {
        [viewEffectInL setHidden:YES];
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_1_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInL];
        } else {
            [viewController sendData:CMD_EFFECT_2_1_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInL];
        }
    }
}

- (IBAction)onBtnEffectInLGp3:(id)sender {
    [self clearEffectInLButtons];
    btnEffectInLGp3.selected = YES;
    [btnEffectInL setTitle:@"GP 3" forState:UIControlStateNormal];
    effectInL = EFFECT_IN_GP_3;
    if (sender) {
        [viewEffectInL setHidden:YES];
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_1_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInL];
        } else {
            [viewController sendData:CMD_EFFECT_2_1_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInL];
        }
    }
}

- (IBAction)onBtnEffectInLGp4:(id)sender {
    [self clearEffectInLButtons];
    btnEffectInLGp4.selected = YES;
    [btnEffectInL setTitle:@"GP 4" forState:UIControlStateNormal];
    effectInL = EFFECT_IN_GP_4;
    if (sender) {
        [viewEffectInL setHidden:YES];
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_1_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInL];
        } else {
            [viewController sendData:CMD_EFFECT_2_1_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInL];
        }
    }
}

- (IBAction)onBtnEffectInLAux1:(id)sender {
    [self clearEffectInLButtons];
    btnEffectInLAux1.selected = YES;
    [btnEffectInL setTitle:@"AUX 1" forState:UIControlStateNormal];
    effectInL = EFFECT_IN_AUX_1;
    if (sender) {
        [viewEffectInL setHidden:YES];
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_1_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInL];
        } else {
            [viewController sendData:CMD_EFFECT_2_1_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInL];
        }
    }
}

- (IBAction)onBtnEffectInLAux2:(id)sender {
    [self clearEffectInLButtons];
    btnEffectInLAux2.selected = YES;
    [btnEffectInL setTitle:@"AUX 2" forState:UIControlStateNormal];
    effectInL = EFFECT_IN_AUX_2;
    if (sender) {
        [viewEffectInL setHidden:YES];
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_1_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInL];
        } else {
            [viewController sendData:CMD_EFFECT_2_1_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInL];
        }
    }
}

- (IBAction)onBtnEffectInLAux3:(id)sender {
    [self clearEffectInLButtons];
    btnEffectInLAux3.selected = YES;
    [btnEffectInL setTitle:@"AUX 3" forState:UIControlStateNormal];
    effectInL = EFFECT_IN_AUX_3;
    if (sender) {
        [viewEffectInL setHidden:YES];
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_1_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInL];
        } else {
            [viewController sendData:CMD_EFFECT_2_1_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInL];
        }
    }
}

- (IBAction)onBtnEffectInLAux4:(id)sender {
    [self clearEffectInLButtons];
    btnEffectInLAux4.selected = YES;
    [btnEffectInL setTitle:@"AUX 4" forState:UIControlStateNormal];
    effectInL = EFFECT_IN_AUX_4;
    if (sender) {
        [viewEffectInL setHidden:YES];
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_1_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInL];
        } else {
            [viewController sendData:CMD_EFFECT_2_1_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInL];
        }
    }
}

- (IBAction)onBtnEffectInLNull:(id)sender {
    [self clearEffectInLButtons];
    btnEffectInLNull.selected = !btnEffectInLNull.selected;
    [btnEffectInL setTitle:@"NULL" forState:UIControlStateNormal];
    effectInL = EFFECT_IN_NULL;
    if (sender) {
        [viewEffectInL setHidden:YES];
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_1_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInL];
        } else {
            [viewController sendData:CMD_EFFECT_2_1_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInL];
        }
    }
}

#pragma mark -
#pragma mark Effect In R event handler methods

- (IBAction)onBtnEffectInRCh1:(id)sender {
    [self clearEffectInRButtons];
    btnEffectInRCh1.selected = YES;
    [btnEffectInR setTitle:@"CH 1" forState:UIControlStateNormal];
    effectInR = EFFECT_IN_CH_1;
    if (sender) {
        [viewEffectInR setHidden:YES];
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_2_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInR];
        } else {
            [viewController sendData:CMD_EFFECT_2_2_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInR];
        }
    }
}

- (IBAction)onBtnEffectInRCh2:(id)sender {
    [self clearEffectInRButtons];
    btnEffectInRCh2.selected = YES;
    [btnEffectInR setTitle:@"CH 2" forState:UIControlStateNormal];
    effectInR = EFFECT_IN_CH_2;
    if (sender) {
        [viewEffectInR setHidden:YES];
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_2_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInR];
        } else {
            [viewController sendData:CMD_EFFECT_2_2_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInR];
        }
    }
}

- (IBAction)onBtnEffectInRCh3:(id)sender {
    [self clearEffectInRButtons];
    btnEffectInRCh3.selected = YES;
    [btnEffectInR setTitle:@"CH 3" forState:UIControlStateNormal];
    effectInR = EFFECT_IN_CH_3;
    if (sender) {
        [viewEffectInR setHidden:YES];
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_2_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInR];
        } else {
            [viewController sendData:CMD_EFFECT_2_2_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInR];
        }
    }
}

- (IBAction)onBtnEffectInRCh4:(id)sender {
    [self clearEffectInRButtons];
    btnEffectInRCh4.selected = YES;
    [btnEffectInR setTitle:@"CH 4" forState:UIControlStateNormal];
    effectInR = EFFECT_IN_CH_4;
    if (sender) {
        [viewEffectInR setHidden:YES];
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_2_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInR];
        } else {
            [viewController sendData:CMD_EFFECT_2_2_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInR];
        }
    }
}

- (IBAction)onBtnEffectInRCh5:(id)sender {
    [self clearEffectInRButtons];
    btnEffectInRCh5.selected = YES;
    [btnEffectInR setTitle:@"CH 5" forState:UIControlStateNormal];
    effectInR = EFFECT_IN_CH_5;
    if (sender) {
        [viewEffectInR setHidden:YES];
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_2_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInR];
        } else {
            [viewController sendData:CMD_EFFECT_2_2_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInR];
        }
    }
}

- (IBAction)onBtnEffectInRCh6:(id)sender {
    [self clearEffectInRButtons];
    btnEffectInRCh6.selected = YES;
    [btnEffectInR setTitle:@"CH 6" forState:UIControlStateNormal];
    effectInR = EFFECT_IN_CH_6;
    if (sender) {
        [viewEffectInR setHidden:YES];
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_2_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInR];
        } else {
            [viewController sendData:CMD_EFFECT_2_2_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInR];
        }
    }
}

- (IBAction)onBtnEffectInRCh7:(id)sender {
    [self clearEffectInRButtons];
    btnEffectInRCh7.selected = YES;
    [btnEffectInR setTitle:@"CH 7" forState:UIControlStateNormal];
    effectInR = EFFECT_IN_CH_7;
    if (sender) {
        [viewEffectInR setHidden:YES];
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_2_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInR];
        } else {
            [viewController sendData:CMD_EFFECT_2_2_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInR];
        }
    }
}

- (IBAction)onBtnEffectInRCh8:(id)sender {
    [self clearEffectInRButtons];
    btnEffectInRCh8.selected = YES;
    [btnEffectInR setTitle:@"CH 8" forState:UIControlStateNormal];
    effectInR = EFFECT_IN_CH_8;
    if (sender) {
        [viewEffectInR setHidden:YES];
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_2_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInR];
        } else {
            [viewController sendData:CMD_EFFECT_2_2_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInR];
        }
    }
}

- (IBAction)onBtnEffectInRCh9:(id)sender {
    [self clearEffectInRButtons];
    btnEffectInRCh9.selected = YES;
    [btnEffectInR setTitle:@"CH 9" forState:UIControlStateNormal];
    effectInR = EFFECT_IN_CH_9;
    if (sender) {
        [viewEffectInR setHidden:YES];
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_2_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInR];
        } else {
            [viewController sendData:CMD_EFFECT_2_2_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInR];
        }
    }
}

- (IBAction)onBtnEffectInRCh10:(id)sender {
    [self clearEffectInRButtons];
    btnEffectInRCh10.selected = YES;
    [btnEffectInR setTitle:@"CH 10" forState:UIControlStateNormal];
    effectInR = EFFECT_IN_CH_10;
    if (sender) {
        [viewEffectInR setHidden:YES];
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_2_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInR];
        } else {
            [viewController sendData:CMD_EFFECT_2_2_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInR];
        }
    }
}

- (IBAction)onBtnEffectInRCh11:(id)sender {
    [self clearEffectInRButtons];
    btnEffectInRCh11.selected = YES;
    [btnEffectInR setTitle:@"CH 11" forState:UIControlStateNormal];
    effectInR = EFFECT_IN_CH_11;
    if (sender) {
        [viewEffectInR setHidden:YES];
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_2_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInR];
        } else {
            [viewController sendData:CMD_EFFECT_2_2_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInR];
        }
    }
}

- (IBAction)onBtnEffectInRCh12:(id)sender {
    [self clearEffectInRButtons];
    btnEffectInRCh12.selected = YES;
    [btnEffectInR setTitle:@"CH 12" forState:UIControlStateNormal];
    effectInR = EFFECT_IN_CH_12;
    if (sender) {
        [viewEffectInR setHidden:YES];
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_2_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInR];
        } else {
            [viewController sendData:CMD_EFFECT_2_2_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInR];
        }
    }
}

- (IBAction)onBtnEffectInRCh13:(id)sender {
    [self clearEffectInRButtons];
    btnEffectInRCh13.selected = YES;
    [btnEffectInR setTitle:@"CH 13" forState:UIControlStateNormal];
    effectInR = EFFECT_IN_CH_13;
    if (sender) {
        [viewEffectInR setHidden:YES];
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_2_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInR];
        } else {
            [viewController sendData:CMD_EFFECT_2_2_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInR];
        }
    }
}

- (IBAction)onBtnEffectInRCh14:(id)sender {
    [self clearEffectInRButtons];
    btnEffectInRCh14.selected = YES;
    [btnEffectInR setTitle:@"CH 14" forState:UIControlStateNormal];
    effectInR = EFFECT_IN_CH_14;
    if (sender) {
        [viewEffectInR setHidden:YES];
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_2_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInR];
        } else {
            [viewController sendData:CMD_EFFECT_2_2_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInR];
        }
    }
}

- (IBAction)onBtnEffectInRCh15:(id)sender {
    [self clearEffectInRButtons];
    btnEffectInRCh15.selected = YES;
    [btnEffectInR setTitle:@"CH 15" forState:UIControlStateNormal];
    effectInR = EFFECT_IN_CH_15;
    if (sender) {
        [viewEffectInR setHidden:YES];
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_2_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInR];
        } else {
            [viewController sendData:CMD_EFFECT_2_2_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInR];
        }
    }
}

- (IBAction)onBtnEffectInRCh16:(id)sender {
    [self clearEffectInRButtons];
    btnEffectInRCh16.selected = YES;
    [btnEffectInR setTitle:@"CH 16" forState:UIControlStateNormal];
    effectInR = EFFECT_IN_CH_16;
    if (sender) {
        [viewEffectInR setHidden:YES];
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_2_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInR];
        } else {
            [viewController sendData:CMD_EFFECT_2_2_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInR];
        }
    }
}

- (IBAction)onBtnEffectInRCh17:(id)sender {
    [self clearEffectInRButtons];
    btnEffectInRCh17.selected = YES;
    [btnEffectInR setTitle:@"CH 17" forState:UIControlStateNormal];
    effectInR = EFFECT_IN_CH_17;
    if (sender) {
        [viewEffectInR setHidden:YES];
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_2_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInR];
        } else {
            [viewController sendData:CMD_EFFECT_2_2_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInR];
        }
    }
}

- (IBAction)onBtnEffectInRCh18:(id)sender {
    [self clearEffectInRButtons];
    btnEffectInRCh18.selected = YES;
    [btnEffectInR setTitle:@"CH 18" forState:UIControlStateNormal];
    effectInR = EFFECT_IN_CH_18;
    if (sender) {
        [viewEffectInR setHidden:YES];
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_2_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInR];
        } else {
            [viewController sendData:CMD_EFFECT_2_2_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInR];
        }
    }
}

- (IBAction)onBtnEffectInRGp1:(id)sender {
    [self clearEffectInRButtons];
    btnEffectInRGp1.selected = YES;
    [btnEffectInR setTitle:@"GP 1" forState:UIControlStateNormal];
    effectInR = EFFECT_IN_GP_1;
    if (sender) {
        [viewEffectInR setHidden:YES];
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_2_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInR];
        } else {
            [viewController sendData:CMD_EFFECT_2_2_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInR];
        }
    }
}

- (IBAction)onBtnEffectInRGp2:(id)sender {
    [self clearEffectInRButtons];
    btnEffectInRGp2.selected = YES;
    [btnEffectInR setTitle:@"GP 2" forState:UIControlStateNormal];
    effectInR = EFFECT_IN_GP_2;
    if (sender) {
        [viewEffectInR setHidden:YES];
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_2_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInR];
        } else {
            [viewController sendData:CMD_EFFECT_2_2_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInR];
        }
    }
}

- (IBAction)onBtnEffectInRGp3:(id)sender {
    [self clearEffectInRButtons];
    btnEffectInRGp3.selected = YES;
    [btnEffectInR setTitle:@"GP 3" forState:UIControlStateNormal];
    effectInR = EFFECT_IN_GP_3;
    if (sender) {
        [viewEffectInR setHidden:YES];
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_2_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInR];
        } else {
            [viewController sendData:CMD_EFFECT_2_2_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInR];
        }
    }
}

- (IBAction)onBtnEffectInRGp4:(id)sender {
    [self clearEffectInRButtons];
    btnEffectInRGp4.selected = YES;
    [btnEffectInR setTitle:@"GP 4" forState:UIControlStateNormal];
    effectInR = EFFECT_IN_GP_4;
    if (sender) {
        [viewEffectInR setHidden:YES];
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_2_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInR];
        } else {
            [viewController sendData:CMD_EFFECT_2_2_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInR];
        }
    }
}

- (IBAction)onBtnEffectInRAux1:(id)sender {
    [self clearEffectInRButtons];
    btnEffectInRAux1.selected = YES;
    [btnEffectInR setTitle:@"AUX 1" forState:UIControlStateNormal];
    effectInR = EFFECT_IN_AUX_1;
    if (sender) {
        [viewEffectInR setHidden:YES];
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_2_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInR];
        } else {
            [viewController sendData:CMD_EFFECT_2_2_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInR];
        }
    }
}

- (IBAction)onBtnEffectInRAux2:(id)sender {
    [self clearEffectInRButtons];
    btnEffectInRAux2.selected = !btnEffectInRAux2.selected;
    [btnEffectInR setTitle:@"AUX 2" forState:UIControlStateNormal];
    effectInR = EFFECT_IN_AUX_2;
    if (sender) {
        [viewEffectInR setHidden:YES];
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_2_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInR];
        } else {
            [viewController sendData:CMD_EFFECT_2_2_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInR];
        }
    }
}

- (IBAction)onBtnEffectInRAux3:(id)sender {
    [self clearEffectInRButtons];
    btnEffectInRAux3.selected = YES;
    [btnEffectInR setTitle:@"AUX 3" forState:UIControlStateNormal];
    effectInR = EFFECT_IN_AUX_3;
    if (sender) {
        [viewEffectInR setHidden:YES];
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_2_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInR];
        } else {
            [viewController sendData:CMD_EFFECT_2_2_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInR];
        }
    }
}

- (IBAction)onBtnEffectInRAux4:(id)sender {
    [self clearEffectInRButtons];
    btnEffectInRAux4.selected = !btnEffectInRAux4.selected;
    [btnEffectInR setTitle:@"AUX 4" forState:UIControlStateNormal];
    effectInR = EFFECT_IN_AUX_4;
    if (sender) {
        [viewEffectInR setHidden:YES];
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_2_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInR];
        } else {
            [viewController sendData:CMD_EFFECT_2_2_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInR];
        }
    }
}

- (IBAction)onBtnEffectInRNull:(id)sender {
    [self clearEffectInRButtons];
    btnEffectInRNull.selected = !btnEffectInRNull.selected;
    [btnEffectInR setTitle:@"NULL" forState:UIControlStateNormal];
    effectInR = EFFECT_IN_NULL;
    if (sender) {
        [viewEffectInR setHidden:YES];
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_2_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInR];
        } else {
            [viewController sendData:CMD_EFFECT_2_2_IN_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effectInR];
        }
    }
}

- (IBAction)onBtnEffectOutLMulti1:(id)sender {
    btnEffectOutLMulti1Status.selected = !btnEffectOutLMulti1Status.selected;
    if (sender) {
        if (effectNum == EFX_1) {
            [Global setClearBit:&effect1OutL bitValue:btnEffectOutLMulti1Status.selected bitOrder:EFFECT_OUT_L_MULTI_1];
            [viewController sendData:CMD_EFFECT_1_1_OUT_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effect1OutL];
        } else {
            [Global setClearBit:&effect2OutL bitValue:btnEffectOutLMulti1Status.selected bitOrder:EFFECT_OUT_L_MULTI_1];
            [viewController sendData:CMD_EFFECT_2_1_OUT_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effect2OutL];
        }
    }
}

- (IBAction)onBtnEffectOutRMulti2:(id)sender {
    btnEffectOutRMulti2Status.selected = !btnEffectOutRMulti2Status.selected;
    if (sender) {
        if (effectNum == EFX_1) {
            [Global setClearBit:&effect1OutR bitValue:btnEffectOutRMulti2Status.selected bitOrder:EFFECT_OUT_R_MULTI_2];
            [viewController sendData:CMD_EFFECT_1_2_OUT_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effect1OutR];
        } else {
            [Global setClearBit:&effect2OutR bitValue:btnEffectOutRMulti2Status.selected bitOrder:EFFECT_OUT_R_MULTI_2];
            [viewController sendData:CMD_EFFECT_2_2_OUT_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effect2OutR];
        }
    }
}

- (IBAction)onBtnEffectOutLMulti3:(id)sender {
    btnEffectOutLMulti3Status.selected = !btnEffectOutLMulti3Status.selected;
    if (sender) {
        if (effectNum == EFX_1) {
            [Global setClearBit:&effect1OutL bitValue:btnEffectOutLMulti3Status.selected bitOrder:EFFECT_OUT_L_MULTI_3];
            [viewController sendData:CMD_EFFECT_1_1_OUT_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effect1OutL];
        } else {
            [Global setClearBit:&effect2OutL bitValue:btnEffectOutLMulti3Status.selected bitOrder:EFFECT_OUT_L_MULTI_3];
            [viewController sendData:CMD_EFFECT_2_1_OUT_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effect2OutL];
        }
    }
}

- (IBAction)onBtnEffectOutRMulti4:(id)sender {
    btnEffectOutRMulti4Status.selected = !btnEffectOutRMulti4Status.selected;
    if (sender) {
        if (effectNum == EFX_1) {
            [Global setClearBit:&effect1OutR bitValue:btnEffectOutRMulti4Status.selected bitOrder:EFFECT_OUT_R_MULTI_4];
            [viewController sendData:CMD_EFFECT_1_2_OUT_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effect1OutR];
        } else {
            [Global setClearBit:&effect2OutR bitValue:btnEffectOutRMulti4Status.selected bitOrder:EFFECT_OUT_R_MULTI_4];
            [viewController sendData:CMD_EFFECT_2_2_OUT_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effect2OutR];
        }
    }
}

- (IBAction)onBtnEffectOutLGp1:(id)sender {
    btnEffectOutLGp1Status.selected = !btnEffectOutLGp1Status.selected;
    if (sender) {
        if (effectNum == EFX_1) {
            [Global setClearBit:&effect1OutL bitValue:btnEffectOutLGp1Status.selected bitOrder:EFFECT_OUT_L_GP_1];
            [viewController sendData:CMD_EFFECT_1_1_OUT_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effect1OutL];
        } else {
            [Global setClearBit:&effect2OutL bitValue:btnEffectOutLGp1Status.selected bitOrder:EFFECT_OUT_L_GP_1];
            [viewController sendData:CMD_EFFECT_2_1_OUT_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effect2OutL];
        }
    }
}

- (IBAction)onBtnEffectOutRGp2:(id)sender {
    btnEffectOutRGp2Status.selected = !btnEffectOutRGp2Status.selected;
    if (sender) {
        if (effectNum == EFX_1) {
            [Global setClearBit:&effect1OutR bitValue:btnEffectOutRGp2Status.selected bitOrder:EFFECT_OUT_R_GP_2];
            [viewController sendData:CMD_EFFECT_1_2_OUT_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effect1OutR];
        } else {
            [Global setClearBit:&effect2OutR bitValue:btnEffectOutRGp2Status.selected bitOrder:EFFECT_OUT_R_GP_2];
            [viewController sendData:CMD_EFFECT_2_2_OUT_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effect2OutR];
        }
    }
}

- (IBAction)onBtnEffectOutLGp3:(id)sender {
    btnEffectOutLGp3Status.selected = !btnEffectOutLGp3Status.selected;
    if (sender) {
        if (effectNum == EFX_1) {
            [Global setClearBit:&effect1OutL bitValue:btnEffectOutLGp3Status.selected bitOrder:EFFECT_OUT_L_GP_3];
            [viewController sendData:CMD_EFFECT_1_1_OUT_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effect1OutL];
        } else {
            [Global setClearBit:&effect2OutL bitValue:btnEffectOutLGp3Status.selected bitOrder:EFFECT_OUT_L_GP_3];
            [viewController sendData:CMD_EFFECT_2_1_OUT_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effect2OutL];
        }
    }
}

- (IBAction)onBtnEffectOutRGp4:(id)sender {
    btnEffectOutRGp4Status.selected = !btnEffectOutRGp4Status.selected;
    if (sender) {
        if (effectNum == EFX_1) {
            [Global setClearBit:&effect1OutR bitValue:btnEffectOutRGp4Status.selected bitOrder:EFFECT_OUT_R_GP_4];
            [viewController sendData:CMD_EFFECT_1_2_OUT_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effect1OutR];
        } else {
            [Global setClearBit:&effect2OutR bitValue:btnEffectOutRGp4Status.selected bitOrder:EFFECT_OUT_R_GP_4];
            [viewController sendData:CMD_EFFECT_2_2_OUT_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effect2OutR];
        }
    }
}

- (IBAction)onBtnEffectOutLMainL:(id)sender {
    btnEffectOutLMainLStatus.selected = !btnEffectOutLMainLStatus.selected;
    if (sender) {
        if (effectNum == EFX_1) {
            [Global setClearBit:&effect1OutL bitValue:btnEffectOutLMainLStatus.selected bitOrder:EFFECT_OUT_L_MAIN_L];
            [viewController sendData:CMD_EFFECT_1_1_OUT_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effect1OutL];
        } else {
            [Global setClearBit:&effect2OutL bitValue:btnEffectOutLMainLStatus.selected bitOrder:EFFECT_OUT_L_MAIN_L];
            [viewController sendData:CMD_EFFECT_2_1_OUT_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effect2OutL];
        }
    }
}

- (IBAction)onBtnEffectOutRMainR:(id)sender {
    btnEffectOutRMainRStatus.selected = !btnEffectOutRMainRStatus.selected;
    if (sender) {
        if (effectNum == EFX_1) {
            [Global setClearBit:&effect1OutR bitValue:btnEffectOutRMainRStatus.selected bitOrder:EFFECT_OUT_R_MAIN_R];
            [viewController sendData:CMD_EFFECT_1_2_OUT_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effect1OutR];
        } else {
            [Global setClearBit:&effect2OutR bitValue:btnEffectOutRMainRStatus.selected bitOrder:EFFECT_OUT_R_MAIN_R];
            [viewController sendData:CMD_EFFECT_2_2_OUT_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:effect2OutR];
        }
    }
}

- (IBAction)onBtnEffect1ReverbRoom:(id)sender {
    [btnReverbType setHidden:NO];
    [imgBackground setImage:[UIImage imageNamed:@"basemap-9.png"]];
    [self hideBottomViews];
    [viewReverb setHidden:NO];
    [self clearEffect1Buttons];
    btnEffect1ReverbRoom.selected = !btnEffect1ReverbRoom.selected;
    [btnEffect setTitle:@"Reverb\nRoom" forState:UIControlStateNormal];
    effectType = EFFECT_REVERB;
    reverbGroup = REVERB_ROOM;
    [btnReverbType1 setTitle:@"Room Large" forState:UIControlStateNormal];
    [btnReverbType2 setTitle:@"Room Medium" forState:UIControlStateNormal];
    [btnReverbType3 setTitle:@"Room Small" forState:UIControlStateNormal];
    [btnReverbType4 setTitle:@"Room Live" forState:UIControlStateNormal];
    [btnReverbType5 setTitle:@"Room Bright" forState:UIControlStateNormal];
    [btnReverbType6 setTitle:@"Room Wood" forState:UIControlStateNormal];
    [btnReverbType7 setTitle:@"Room Heavy" forState:UIControlStateNormal];
    [btnReverbType8 setTitle:@"Room Opera" forState:UIControlStateNormal];    
    if (sender) {
        [viewEffect1 setHidden:YES];
        [viewController sendData:CMD_EFFECT_1_PROGRAM commandType:DSP_WRITE dspId:DSP_8 value:effectType];
        [viewController sendData:CMD_EFFECT_1_REVERB_TYPE commandType:DSP_WRITE dspId:DSP_8 value:reverbRoom];
    }
}

- (IBAction)onBtnEffect1ReverbHall:(id)sender {
    [btnReverbType setHidden:NO];
    [imgBackground setImage:[UIImage imageNamed:@"basemap-9.png"]];
    [self hideBottomViews];
    [viewReverb setHidden:NO];
    [self clearEffect1Buttons];
    btnEffect1ReverbHall.selected = !btnEffect1ReverbHall.selected;
    [btnEffect setTitle:@"Reverb\nHall" forState:UIControlStateNormal];
    effectType = EFFECT_REVERB;
    reverbGroup = REVERB_HALL;
    [btnReverbType1 setTitle:@"Hall Large" forState:UIControlStateNormal];
    [btnReverbType2 setTitle:@"Hall Medium" forState:UIControlStateNormal];
    [btnReverbType3 setTitle:@"Hall Small" forState:UIControlStateNormal];
    [btnReverbType4 setTitle:@"Hall Concert" forState:UIControlStateNormal];
    [btnReverbType5 setTitle:@"Hall Dark" forState:UIControlStateNormal];
    [btnReverbType6 setTitle:@"Hall Wonder" forState:UIControlStateNormal];
    [btnReverbType7 setTitle:@"Hall Jazz" forState:UIControlStateNormal];
    [btnReverbType8 setTitle:@"Hall Vocal" forState:UIControlStateNormal];
    if (sender) {
        [viewEffect1 setHidden:YES];
        [viewController sendData:CMD_EFFECT_1_PROGRAM commandType:DSP_WRITE dspId:DSP_8 value:effectType];
        [viewController sendData:CMD_EFFECT_1_REVERB_TYPE commandType:DSP_WRITE dspId:DSP_8 value:reverbHall];
    }
}

- (IBAction)onBtnEffect1ReverbPlate:(id)sender {
    [btnReverbType setHidden:NO];
    [self hideBottomViews];
    [viewReverb setHidden:NO];
    [self clearEffect1Buttons];
    btnEffect1ReverbPlate.selected = !btnEffect1ReverbPlate.selected;
    [btnEffect setTitle:@"Reverb\nPlate" forState:UIControlStateNormal];
    effectType = EFFECT_REVERB;
    reverbGroup = REVERB_PLATE;
    [btnReverbType1 setTitle:@"Plate Large" forState:UIControlStateNormal];
    [btnReverbType2 setTitle:@"Plate Medium" forState:UIControlStateNormal];
    [btnReverbType3 setTitle:@"Plate Small" forState:UIControlStateNormal];
    [btnReverbType4 setTitle:@"Plate Flat" forState:UIControlStateNormal];
    [btnReverbType5 setTitle:@"Plate Light" forState:UIControlStateNormal];
    [btnReverbType6 setTitle:@"Plate Thin" forState:UIControlStateNormal];
    [btnReverbType7 setTitle:@"Plate Perc" forState:UIControlStateNormal];
    [btnReverbType8 setTitle:@"Plate Industrial" forState:UIControlStateNormal];
    if (sender) {
        [viewEffect1 setHidden:YES];
        [viewController sendData:CMD_EFFECT_1_PROGRAM commandType:DSP_WRITE dspId:DSP_8 value:effectType];
        [viewController sendData:CMD_EFFECT_1_REVERB_TYPE commandType:DSP_WRITE dspId:DSP_8 value:reverbPlate];
    }
}

- (IBAction)onBtnEffect1Echo:(id)sender {
    [btnReverbType setHidden:YES];
    [imgBackground setImage:[UIImage imageNamed:@"basemap-9.png"]];
    [self hideBottomViews];
    [viewEcho setHidden:NO];
    [self clearEffect1Buttons];
    btnEffect1Echo.selected = !btnEffect1Echo.selected;
    [btnEffect setTitle:@"Echo" forState:UIControlStateNormal];
    effectType = EFFECT_ECHO_DELAY;
    if (sender) {
        [viewEffect1 setHidden:YES];
        [viewController sendData:CMD_EFFECT_1_PROGRAM commandType:DSP_WRITE dspId:DSP_8 value:effectType];
    }
}

- (IBAction)onBtnEffect1TapDelay:(id)sender {
    [btnReverbType setHidden:YES];
    [imgBackground setImage:[UIImage imageNamed:@"basemap-9.png"]];
    [self hideBottomViews];
    [viewTapDelay setHidden:NO];
    [self clearEffect1Buttons];
    btnEffect1TapDelay.selected = !btnEffect1TapDelay.selected;
    [btnEffect setTitle:@"Tap\nDelay" forState:UIControlStateNormal];
    effectType = EFFECT_TAP_DELAY;
    if (sender) {
        [viewEffect1 setHidden:YES];
        [viewController sendData:CMD_EFFECT_1_PROGRAM commandType:DSP_WRITE dspId:DSP_8 value:effectType];
    }
}

- (IBAction)onBtnEffect1Chorus:(id)sender {
    [btnReverbType setHidden:YES];
    [imgBackground setImage:[UIImage imageNamed:@"basemap-9.png"]];
    [self hideBottomViews];
    [viewChorus setHidden:NO];
    [self clearEffect1Buttons];
    btnEffect1Chorus.selected = !btnEffect1Chorus.selected;
    [btnEffect setTitle:@"Chorus" forState:UIControlStateNormal];
    effectType = EFFECT_CHORUS;
    if (sender) {
        [viewEffect1 setHidden:YES];
        [viewController sendData:CMD_EFFECT_1_PROGRAM commandType:DSP_WRITE dspId:DSP_8 value:effectType];
    }
}

- (IBAction)onBtnEffect1Flanger:(id)sender {
    [btnReverbType setHidden:YES];
    [imgBackground setImage:[UIImage imageNamed:@"basemap-9.png"]];
    [self hideBottomViews];
    [viewFlanger setHidden:NO];
    [self clearEffect1Buttons];
    btnEffect1Flanger.selected = !btnEffect1Flanger.selected;
    [btnEffect setTitle:@"Flanger" forState:UIControlStateNormal];
    effectType = EFFECT_FLANGER;
    if (sender) {
        [viewEffect1 setHidden:YES];
        [viewController sendData:CMD_EFFECT_1_PROGRAM commandType:DSP_WRITE dspId:DSP_8 value:effectType];
    }
}

- (IBAction)onBtnEffect1Phaser:(id)sender {
    [btnReverbType setHidden:YES];
    [imgBackground setImage:[UIImage imageNamed:@"basemap-9.png"]];
    [self hideBottomViews];
    [viewPhaser setHidden:NO];
    [self clearEffect1Buttons];
    btnEffect1Phaser.selected = !btnEffect1Phaser.selected;
    [btnEffect setTitle:@"Phaser" forState:UIControlStateNormal];
    effectType = EFFECT_PHASER;
    if (sender) {
        [viewEffect1 setHidden:YES];
        [viewController sendData:CMD_EFFECT_1_PROGRAM commandType:DSP_WRITE dspId:DSP_8 value:effectType];
    }
}

- (IBAction)onBtnEffect1Tremolo:(id)sender {
    [btnReverbType setHidden:YES];
    [imgBackground setImage:[UIImage imageNamed:@"basemap-9.png"]];
    [self hideBottomViews];
    [viewTremolo setHidden:NO];
    [self clearEffect1Buttons];
    btnEffect1Tremolo.selected = !btnEffect1Tremolo.selected;
    [btnEffect setTitle:@"Tremolo" forState:UIControlStateNormal];
    effectType = EFFECT_TREMOLO;
    if (sender) {
        [viewEffect1 setHidden:YES];
        [viewController sendData:CMD_EFFECT_1_PROGRAM commandType:DSP_WRITE dspId:DSP_8 value:effectType];
    }
}

- (IBAction)onBtnEffect1Vibrato:(id)sender {
    [btnReverbType setHidden:YES];
    [imgBackground setImage:[UIImage imageNamed:@"basemap-9.png"]];
    [self hideBottomViews];
    [viewVibrato setHidden:NO];
    [self clearEffect1Buttons];
    btnEffect1Vibrato.selected = !btnEffect1Vibrato.selected;
    [btnEffect setTitle:@"Vibrato" forState:UIControlStateNormal];
    effectType = EFFECT_VIBRATO;
    if (sender) {
        [viewEffect1 setHidden:YES];
        [viewController sendData:CMD_EFFECT_1_PROGRAM commandType:DSP_WRITE dspId:DSP_8 value:effectType];
    }
}

- (IBAction)onBtnEffect1AutoPan:(id)sender {
    [btnReverbType setHidden:YES];
    [imgBackground setImage:[UIImage imageNamed:@"basemap-9.png"]];
    [self hideBottomViews];
    [viewAutoPan setHidden:NO];
    [self clearEffect1Buttons];
    btnEffect1AutoPan.selected = !btnEffect1AutoPan.selected;
    [btnEffect setTitle:@"Auto\nPan" forState:UIControlStateNormal];
    effectType = EFFECT_AUTO_PAN;
    if (sender) {
        [viewEffect1 setHidden:YES];
        [viewController sendData:CMD_EFFECT_1_PROGRAM commandType:DSP_WRITE dspId:DSP_8 value:effectType];
    }
}

- (IBAction)onBtnEffect1Geq31:(id)sender {
    [btnReverbType setHidden:YES];
    [imgBackground setImage:[UIImage imageNamed:@"basemap-10.png"]];
    [self hideBottomViews];
    [viewGeq31 setHidden:NO];
    [self clearEffect1Buttons];
    btnEffect1Geq31.selected = !btnEffect1Geq31.selected;
    [btnEffect setTitle:@"GEQ31" forState:UIControlStateNormal];
    effectType = EFFECT_GEQ_31;
    if (sender) {
        [viewEffect1 setHidden:YES];
        [viewController sendData:CMD_EFFECT_1_PROGRAM commandType:DSP_WRITE dspId:DSP_8 value:effectType];
    }
}

- (IBAction)onBtnEffect2Echo:(id)sender {
    [btnReverbType setHidden:YES];
    [imgBackground setImage:[UIImage imageNamed:@"basemap-9.png"]];
    [self hideBottomViews];
    [viewEcho setHidden:NO];
    [self clearEffect2Buttons];
    btnEffect2Echo.selected = !btnEffect2Echo.selected;
    [btnEffect setTitle:@"Echo" forState:UIControlStateNormal];
    effectType = EFFECT_ECHO_DELAY;
    if (sender) {
        [viewEffect2 setHidden:YES];
        [viewController sendData:CMD_EFFECT_2_PROGRAM commandType:DSP_WRITE dspId:DSP_5 value:effectType];
    }
}

- (IBAction)onBtnEffect2TapDelay:(id)sender {
    [btnReverbType setHidden:YES];
    [imgBackground setImage:[UIImage imageNamed:@"basemap-9.png"]];
    [self hideBottomViews];
    [viewTapDelay setHidden:NO];
    [self clearEffect2Buttons];
    btnEffect2TapDelay.selected = !btnEffect2TapDelay.selected;
    [btnEffect setTitle:@"Tap\nDelay" forState:UIControlStateNormal];
    effectType = EFFECT_TAP_DELAY;
    if (sender) {
        [viewEffect2 setHidden:YES];
        [viewController sendData:CMD_EFFECT_2_PROGRAM commandType:DSP_WRITE dspId:DSP_5 value:effectType];
    }
}

- (IBAction)onBtnEffect2Chorus:(id)sender {
    [btnReverbType setHidden:YES];
    [imgBackground setImage:[UIImage imageNamed:@"basemap-9.png"]];
    [self hideBottomViews];
    [viewChorus setHidden:NO];
    [self clearEffect2Buttons];
    btnEffect2Chorus.selected = !btnEffect2Chorus.selected;
    [btnEffect setTitle:@"Chorus" forState:UIControlStateNormal];
    effectType = EFFECT_CHORUS;
    if (sender) {
        [viewEffect2 setHidden:YES];
        [viewController sendData:CMD_EFFECT_2_PROGRAM commandType:DSP_WRITE dspId:DSP_5 value:effectType];
    }
}

- (IBAction)onBtnEffect2Flanger:(id)sender {
    [btnReverbType setHidden:YES];
    [imgBackground setImage:[UIImage imageNamed:@"basemap-9.png"]];
    [self hideBottomViews];
    [viewFlanger setHidden:NO];
    [self clearEffect2Buttons];
    btnEffect2Flanger.selected = !btnEffect2Flanger.selected;
    [btnEffect setTitle:@"Flanger" forState:UIControlStateNormal];
    effectType = EFFECT_FLANGER;
    if (sender) {
        [viewEffect2 setHidden:YES];
        [viewController sendData:CMD_EFFECT_2_PROGRAM commandType:DSP_WRITE dspId:DSP_5 value:effectType];
    }
}

- (IBAction)onBtnEffect2Phaser:(id)sender {
    [btnReverbType setHidden:YES];
    [imgBackground setImage:[UIImage imageNamed:@"basemap-9.png"]];
    [self hideBottomViews];
    [viewPhaser setHidden:NO];
    [self clearEffect2Buttons];
    btnEffect2Phaser.selected = !btnEffect2Phaser.selected;
    [btnEffect setTitle:@"Phaser" forState:UIControlStateNormal];
    effectType = EFFECT_PHASER;
    if (sender) {
        [viewEffect2 setHidden:YES];
        [viewController sendData:CMD_EFFECT_2_PROGRAM commandType:DSP_WRITE dspId:DSP_5 value:effectType];
    }
}

- (IBAction)onBtnEffect2Tremolo:(id)sender {
    [btnReverbType setHidden:YES];
    [imgBackground setImage:[UIImage imageNamed:@"basemap-9.png"]];
    [self hideBottomViews];
    [viewTremolo setHidden:NO];
    [self clearEffect2Buttons];
    btnEffect2Tremolo.selected = !btnEffect2Tremolo.selected;
    [btnEffect setTitle:@"Tremolo" forState:UIControlStateNormal];
    effectType = EFFECT_TREMOLO;
    if (sender) {
        [viewEffect2 setHidden:YES];
        [viewController sendData:CMD_EFFECT_2_PROGRAM commandType:DSP_WRITE dspId:DSP_5 value:effectType];
    }
}

- (IBAction)onBtnEffect2Vibrato:(id)sender {
    [btnReverbType setHidden:YES];
    [imgBackground setImage:[UIImage imageNamed:@"basemap-9.png"]];
    [self hideBottomViews];
    [viewVibrato setHidden:NO];
    [self clearEffect2Buttons];
    btnEffect2Vibrato.selected = !btnEffect2Vibrato.selected;
    [btnEffect setTitle:@"Vibrato" forState:UIControlStateNormal];
    effectType = EFFECT_VIBRATO;
    if (sender) {
        [viewEffect2 setHidden:YES];
        [viewController sendData:CMD_EFFECT_2_PROGRAM commandType:DSP_WRITE dspId:DSP_5 value:effectType];
    }
}

- (IBAction)onBtnEffect2AutoPan:(id)sender {
    [btnReverbType setHidden:YES];
    [imgBackground setImage:[UIImage imageNamed:@"basemap-9.png"]];
    [self hideBottomViews];
    [viewAutoPan setHidden:NO];
    [self clearEffect2Buttons];
    btnEffect2AutoPan.selected = !btnEffect2AutoPan.selected;
    [btnEffect setTitle:@"Auto\nPan" forState:UIControlStateNormal];
    effectType = EFFECT_AUTO_PAN;
    if (sender) {
        [viewEffect2 setHidden:YES];
        [viewController sendData:CMD_EFFECT_2_PROGRAM commandType:DSP_WRITE dspId:DSP_5 value:effectType];
    }
}

- (IBAction)onBtnEffect2Geq15:(id)sender {
    [btnReverbType setHidden:YES];
    [imgBackground setImage:[UIImage imageNamed:@"basemap-12.png"]];
    [self hideBottomViews];
    [viewGeq15 setHidden:NO];
    [self clearEffect2Buttons];
    btnEffect2Geq15.selected = !btnEffect2Geq15.selected;
    [btnEffect setTitle:@"GEQ15" forState:UIControlStateNormal];
    effectType = EFFECT_GEQ_15;
    if (sender) {
        [viewEffect2 setHidden:YES];
        [viewController sendData:CMD_EFFECT_2_PROGRAM commandType:DSP_WRITE dspId:DSP_5 value:effectType];
    }
}

- (IBAction)onBtnReverbType1:(id)sender {
    [self clearReverbTypeButtons];
    btnReverbType1.selected = !btnReverbType1.selected;
    switch (reverbGroup) {
        case REVERB_ROOM:
            [btnReverbType setTitle:@"Room\nLarge" forState:UIControlStateNormal];
            reverbRoom = ROOM_LARGE;
            if (sender) {
                [viewReverbType setHidden:YES];
                [viewController sendData:CMD_EFFECT_1_REVERB_TYPE commandType:DSP_WRITE dspId:DSP_8 value:reverbRoom];
            }
            break;
        case REVERB_HALL:
            [btnReverbType setTitle:@"Hall\nLarge" forState:UIControlStateNormal];
            reverbHall = HALL_LARGE;
            if (sender) {
                [viewReverbType setHidden:YES];
                [viewController sendData:CMD_EFFECT_1_REVERB_TYPE commandType:DSP_WRITE dspId:DSP_8 value:reverbHall];
            }
            break;
        case REVERB_PLATE:
            [btnReverbType setTitle:@"Plate\nLarge" forState:UIControlStateNormal];
            reverbPlate = PLATE_LARGE;
            if (sender) {
                [viewReverbType setHidden:YES];
                [viewController sendData:CMD_EFFECT_1_REVERB_TYPE commandType:DSP_WRITE dspId:DSP_8 value:reverbPlate];
            }
            break;
    }
}

- (IBAction)onBtnReverbType2:(id)sender {
    [self clearReverbTypeButtons];
    btnReverbType2.selected = !btnReverbType2.selected;
    switch (reverbGroup) {
        case REVERB_ROOM:
            [btnReverbType setTitle:@"Room\nMedium" forState:UIControlStateNormal];
            reverbRoom = ROOM_MEDIUM;
            if (sender) {
                [viewReverbType setHidden:YES];
                [viewController sendData:CMD_EFFECT_1_REVERB_TYPE commandType:DSP_WRITE dspId:DSP_8 value:reverbRoom];
            }
            break;
        case REVERB_HALL:
            [btnReverbType setTitle:@"Hall\nMedium" forState:UIControlStateNormal];
            reverbHall = HALL_MEDIUM;
            if (sender) {
                [viewReverbType setHidden:YES];
                [viewController sendData:CMD_EFFECT_1_REVERB_TYPE commandType:DSP_WRITE dspId:DSP_8 value:reverbHall];
            }
            break;
        case REVERB_PLATE:
            [btnReverbType setTitle:@"Plate\nMedium" forState:UIControlStateNormal];
            reverbPlate = PLATE_MEDIUM;
            if (sender) {
                [viewReverbType setHidden:YES];
                [viewController sendData:CMD_EFFECT_1_REVERB_TYPE commandType:DSP_WRITE dspId:DSP_8 value:reverbPlate];
            }
            break;
    }
}

- (IBAction)onBtnReverbType3:(id)sender {
    [self clearReverbTypeButtons];
    btnReverbType3.selected = !btnReverbType3.selected;
    switch (reverbGroup) {
        case REVERB_ROOM:
            [btnReverbType setTitle:@"Room\nSmall" forState:UIControlStateNormal];
            reverbRoom = ROOM_SMALL;
            if (sender) {
                [viewReverbType setHidden:YES];
                [viewController sendData:CMD_EFFECT_1_REVERB_TYPE commandType:DSP_WRITE dspId:DSP_8 value:reverbRoom];
            }
            break;
        case REVERB_HALL:
            [btnReverbType setTitle:@"Hall\nSmall" forState:UIControlStateNormal];
            reverbHall = HALL_SMALL;
            if (sender) {
                [viewReverbType setHidden:YES];
                [viewController sendData:CMD_EFFECT_1_REVERB_TYPE commandType:DSP_WRITE dspId:DSP_8 value:reverbHall];
            }
            break;
        case REVERB_PLATE:
            [btnReverbType setTitle:@"Plate\nSmall" forState:UIControlStateNormal];
            reverbPlate = PLATE_SMALL;
            if (sender) {
                [viewReverbType setHidden:YES];
                [viewController sendData:CMD_EFFECT_1_REVERB_TYPE commandType:DSP_WRITE dspId:DSP_8 value:reverbPlate];
            }
            break;
    }
}

- (IBAction)onBtnReverbType4:(id)sender {
    [self clearReverbTypeButtons];
    btnReverbType4.selected = !btnReverbType4.selected;
    switch (reverbGroup) {
        case REVERB_ROOM:
            [btnReverbType setTitle:@"Room\nLive" forState:UIControlStateNormal];
            reverbRoom = ROOM_LIVE;
            if (sender) {
                [viewReverbType setHidden:YES];
                [viewController sendData:CMD_EFFECT_1_REVERB_TYPE commandType:DSP_WRITE dspId:DSP_8 value:reverbRoom];
            }
            break;
        case REVERB_HALL:
            [btnReverbType setTitle:@"Hall\nConcert" forState:UIControlStateNormal];
            reverbHall = HALL_CONCERT;
            if (sender) {
                [viewReverbType setHidden:YES];
                [viewController sendData:CMD_EFFECT_1_REVERB_TYPE commandType:DSP_WRITE dspId:DSP_8 value:reverbHall];
            }
            break;
        case REVERB_PLATE:
            [btnReverbType setTitle:@"Plate\nFlat" forState:UIControlStateNormal];
            reverbPlate = PLATE_FLAT;
            if (sender) {
                [viewReverbType setHidden:YES];
                [viewController sendData:CMD_EFFECT_1_REVERB_TYPE commandType:DSP_WRITE dspId:DSP_8 value:reverbPlate];
            }
            break;
    }
}

- (IBAction)onBtnReverbType5:(id)sender {
    [self clearReverbTypeButtons];
    btnReverbType5.selected = !btnReverbType5.selected;
    switch (reverbGroup) {
        case REVERB_ROOM:
            [btnReverbType setTitle:@"Room\nBright" forState:UIControlStateNormal];
            reverbRoom = ROOM_BRIGHT;
            if (sender) {
                [viewReverbType setHidden:YES];
                [viewController sendData:CMD_EFFECT_1_REVERB_TYPE commandType:DSP_WRITE dspId:DSP_8 value:reverbRoom];
            }
            break;
        case REVERB_HALL:
            [btnReverbType setTitle:@"Hall\nDark" forState:UIControlStateNormal];
            reverbHall = HALL_DARK;
            if (sender) {
                [viewReverbType setHidden:YES];
                [viewController sendData:CMD_EFFECT_1_REVERB_TYPE commandType:DSP_WRITE dspId:DSP_8 value:reverbHall];
            }
            break;
        case REVERB_PLATE:
            [btnReverbType setTitle:@"Plate\nLight" forState:UIControlStateNormal];
            reverbPlate = PLATE_LIGHT;
            if (sender) {
                [viewReverbType setHidden:YES];
                [viewController sendData:CMD_EFFECT_1_REVERB_TYPE commandType:DSP_WRITE dspId:DSP_8 value:reverbPlate];
            }
            break;
    }
}

- (IBAction)onBtnReverbType6:(id)sender {
    [self clearReverbTypeButtons];
    btnReverbType6.selected = !btnReverbType6.selected;
    switch (reverbGroup) {
        case REVERB_ROOM:
            [btnReverbType setTitle:@"Room\nWood" forState:UIControlStateNormal];
            reverbRoom = ROOM_WOOD;
            if (sender) {
                [viewReverbType setHidden:YES];
                [viewController sendData:CMD_EFFECT_1_REVERB_TYPE commandType:DSP_WRITE dspId:DSP_8 value:reverbRoom];
            }
            break;
        case REVERB_HALL:
            [btnReverbType setTitle:@"Hall\nWonder" forState:UIControlStateNormal];
            reverbHall = HALL_WONDER;
            if (sender) {
                [viewReverbType setHidden:YES];
                [viewController sendData:CMD_EFFECT_1_REVERB_TYPE commandType:DSP_WRITE dspId:DSP_8 value:reverbHall];
            }
            break;
        case REVERB_PLATE:
            [btnReverbType setTitle:@"Plate\nThin" forState:UIControlStateNormal];
            reverbPlate = PLATE_THIN;
            if (sender) {
                [viewReverbType setHidden:YES];
                [viewController sendData:CMD_EFFECT_1_REVERB_TYPE commandType:DSP_WRITE dspId:DSP_8 value:reverbPlate];
            }
            break;
    }
}

- (IBAction)onBtnReverbType7:(id)sender {
    [self clearReverbTypeButtons];
    btnReverbType7.selected = !btnReverbType7.selected;
    switch (reverbGroup) {
        case REVERB_ROOM:
            [btnReverbType setTitle:@"Room\nHeavy" forState:UIControlStateNormal];
            reverbRoom = ROOM_HEAVY;
            if (sender) {
                [viewReverbType setHidden:YES];
                [viewController sendData:CMD_EFFECT_1_REVERB_TYPE commandType:DSP_WRITE dspId:DSP_8 value:reverbRoom];
            }
            break;
        case REVERB_HALL:
            [btnReverbType setTitle:@"Hall\nJazz" forState:UIControlStateNormal];
            reverbHall = HALL_JAZZ;
            if (sender) {
                [viewReverbType setHidden:YES];
                [viewController sendData:CMD_EFFECT_1_REVERB_TYPE commandType:DSP_WRITE dspId:DSP_8 value:reverbHall];
            }
            break;
        case REVERB_PLATE:
            [btnReverbType setTitle:@"Plate\nPerc" forState:UIControlStateNormal];
            reverbPlate = PLATE_PERC;
            if (sender) {
                [viewReverbType setHidden:YES];
                [viewController sendData:CMD_EFFECT_1_REVERB_TYPE commandType:DSP_WRITE dspId:DSP_8 value:reverbPlate];
            }
            break;
    }
}

- (IBAction)onBtnReverbType8:(id)sender {
    [self clearReverbTypeButtons];
    btnReverbType8.selected = !btnReverbType8.selected;
    switch (reverbGroup) {
        case REVERB_ROOM:
            [btnReverbType setTitle:@"Room\nOpera" forState:UIControlStateNormal];
            reverbRoom = ROOM_OPERA;
            if (sender) {
                [viewReverbType setHidden:YES];
                [viewController sendData:CMD_EFFECT_1_REVERB_TYPE commandType:DSP_WRITE dspId:DSP_8 value:reverbRoom];
            }
            break;
        case REVERB_HALL:
            [btnReverbType setTitle:@"Hall\nVocal" forState:UIControlStateNormal];
            reverbHall = HALL_VOCAL;
            if (sender) {
                [viewReverbType setHidden:YES];
                [viewController sendData:CMD_EFFECT_1_REVERB_TYPE commandType:DSP_WRITE dspId:DSP_8 value:reverbHall];
            }
            break;
        case REVERB_PLATE:
            [btnReverbType setTitle:@"Plate\nIndustrial" forState:UIControlStateNormal];
            reverbPlate = PLATE_INDUSTRIAL;
            if (sender) {
                [viewReverbType setHidden:YES];
                [viewController sendData:CMD_EFFECT_1_REVERB_TYPE commandType:DSP_WRITE dspId:DSP_8 value:reverbPlate];
            }
            break;
    }
}

#pragma mark -
#pragma mark Reverb view event handler methods

- (IBAction)knobHpfDidChange:(id)sender
{
	lblHpf.text = [Global getFreqString:knobHpf.value];
    cpvHpf.progress = knobHpf.value/HPF_FREQ_MAX;
    int knobValue = [Global knobValueToFreq:knobHpf.value];
    if (sender) {
        [viewController sendData:CMD_EFFECT_1_REVERB_HPF_FREQ commandType:DSP_WRITE dspId:DSP_8 value:knobValue];
    }
}

- (IBAction)knobLpfDidChange:(id)sender
{
	lblLpf.text = [Global getFreqString:knobLpf.value];
    cpvLpf.progress = knobLpf.value/LPF_FREQ_MAX;
    int knobValue = [Global knobValueToFreq:knobLpf.value];
    if (sender) {
        [viewController sendData:CMD_EFFECT_1_REVERB_LPF_FREQ commandType:DSP_WRITE dspId:DSP_8 value:knobValue];
    }
}

- (IBAction)knobPreDelayDidChange:(id)sender
{
	lblPreDelay.text = [NSString stringWithFormat:@"%d mS", (int)knobPreDelay.value];
    cpvPreDelay.progress = knobPreDelay.value/REVERB_PRE_DELAY_MAX;
    if (sender) {
        [viewController sendData:CMD_EFFECT_1_REVERB_PRE_DELAY commandType:DSP_WRITE dspId:DSP_8 value:knobPreDelay.value];
    }
}

- (IBAction)knobEarlyOutDidChange:(id)sender
{
	lblEarlyOut.text = [NSString stringWithFormat:@"%d%%", (int)knobEarlyOut.value];    
    cpvEarlyOut.progress = knobEarlyOut.value/REVERB_EARLY_OUT_MAX;
    if (sender) {
        [viewController sendData:CMD_EFFECT_1_REVERB_EARLY_DELAY_OUT commandType:DSP_WRITE dspId:DSP_8 value:knobEarlyOut.value];
    }
}

- (IBAction)knobReverbTimeDidChange:(id)sender
{
    int reverbTime = knobReverbTime.value * 50;
    if (reverbTime < 1000) {
        lblReverbTime.text = [NSString stringWithFormat:@"%d mS", reverbTime];
    } else {
        lblReverbTime.text = [NSString stringWithFormat:@"%.2f S", (float)(reverbTime / 1000.0)];
    }
    cpvReverbTime.progress = knobReverbTime.value/REVERB_TIME_MAX;
    if (sender) {
        [viewController sendData:CMD_EFFECT_1_REVERB_TIME commandType:DSP_WRITE dspId:DSP_8 value:knobReverbTime.value];
    }
}

- (IBAction)knobLevelDidChange:(id)sender
{
	lblLevel.text = [NSString stringWithFormat:@"%d%%", (int)knobLevel.value];
    cpvLevel.progress = knobLevel.value/REVERB_LEVEL_MAX;
    if (sender) {
        [viewController sendData:CMD_EFFECT_1_REVERB_LEVEL commandType:DSP_WRITE dspId:DSP_8 value:knobLevel.value];
    }
}

- (IBAction)knobHiRatioDidChange:(id)sender
{
	lblHiRatio.text = [NSString stringWithFormat:@"%d%%", (int)knobHiRatio.value];
    cpvHiRatio.progress = knobHiRatio.value/REVERB_HIRATIO_MAX;
    if (sender) {
        [viewController sendData:CMD_EFFECT_1_REVERB_HI_RATIO commandType:DSP_WRITE dspId:DSP_8 value:knobHiRatio.value];
    }
}

- (IBAction)knobDensityDidChange:(id)sender
{
	lblDensity.text = [NSString stringWithFormat:@"%d%%", (int)knobDensity.value];
    cpvDensity.progress = knobDensity.value/REVERB_DENSITY_MAX;
    if (sender) {
        [viewController sendData:CMD_EFFECT_1_REVERB_DENSITY commandType:DSP_WRITE dspId:DSP_8 value:knobDensity.value];
    }
}

- (IBAction)knobThresholdDidChange:(id)sender
{
	lblThreshold.text = [NSString stringWithFormat:@"%d dB", (int)knobThreshold.value-REVERB_GATE_THRESHOLD_MAX];
    cpvThreshold.progress = knobThreshold.value/REVERB_GATE_THRESHOLD_MAX;
    if (sender) {
        [viewController sendData:CMD_EFFECT_1_REVERB_GATE_THRESHOLD commandType:DSP_WRITE dspId:DSP_8 value:(int)knobThreshold.value-REVERB_GATE_THRESHOLD_MAX];
    }
}

- (IBAction)knobHoldDidChange:(id)sender
{
    lblHold.text = [Global getTimeString:knobHold.value];
    cpvHold.progress = knobHold.value/REVERB_GATE_HOLD_MAX;
    if (sender) {
        [viewController sendData:CMD_EFFECT_1_REVERB_GATE_HOLD_TIME commandType:DSP_WRITE dspId:DSP_8 value:knobHold.value];
    }
}

- (IBAction)onBtnGate:(id)sender
{
    btnGate.selected = !btnGate.selected;
    [viewController sendData:CMD_EFFECT_1_REVERB_GATE commandType:DSP_WRITE dspId:DSP_8 value:btnGate.selected];
}

#pragma mark -
#pragma mark Echo view event handler methods

- (IBAction)knobEchoTimeLDidChange:(id)sender
{
	lblEchoTimeL.text = [NSString stringWithFormat:@"%dmS", (int)knobEchoTimeL.value];
    cpvEchoTimeL.progress = knobEchoTimeL.value/ECHO_DELAY_MAX;
    if (sender) {
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_ECHO_DELAY_1_TIME commandType:DSP_WRITE dspId:DSP_8 value:knobEchoTimeL.value];
        } else {
            [viewController sendData:CMD_EFFECT_2_ECHO_DELAY_1_TIME commandType:DSP_WRITE dspId:DSP_5 value:knobEchoTimeL.value];
        }
    }
}

- (IBAction)knobEchoTimeRDidChange:(id)sender
{
	lblEchoTimeR.text = [NSString stringWithFormat:@"%dmS", (int)knobEchoTimeR.value];
    cpvEchoTimeR.progress = knobEchoTimeR.value/ECHO_DELAY_MAX;
    if (sender) {
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_ECHO_DELAY_2_TIME commandType:DSP_WRITE dspId:DSP_8 value:knobEchoTimeR.value];
        } else {
            [viewController sendData:CMD_EFFECT_2_ECHO_DELAY_2_TIME commandType:DSP_WRITE dspId:DSP_5 value:knobEchoTimeR.value];
        }
    }
}

- (IBAction)knobEchoFb1DidChange:(id)sender
{
	lblEchoFb1.text = [NSString stringWithFormat:@"%d%%", (int)knobEchoFb1.value];
    cpvEchoFb1.progress = knobEchoFb1.value/ECHO_DELAY_FB_MAX;
    if (sender) {
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_ECHO_DELAY_FB_1 commandType:DSP_WRITE dspId:DSP_8 value:knobEchoFb1.value];
        } else {
            [viewController sendData:CMD_EFFECT_2_ECHO_DELAY_FB_1 commandType:DSP_WRITE dspId:DSP_5 value:knobEchoFb1.value];
        }
    }
}

- (IBAction)knobEchoFb2DidChange:(id)sender
{
	lblEchoFb2.text = [NSString stringWithFormat:@"%d%%", (int)knobEchoFb2.value];
    cpvEchoFb2.progress = knobEchoFb2.value/ECHO_DELAY_FB_MAX;
    if (sender) {
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_ECHO_DELAY_FB_2 commandType:DSP_WRITE dspId:DSP_8 value:knobEchoFb2.value];
        } else {
            [viewController sendData:CMD_EFFECT_2_ECHO_DELAY_FB_2 commandType:DSP_WRITE dspId:DSP_5 value:knobEchoFb2.value];
        }
    }
}

- (IBAction)knobEchoHpfDidChange:(id)sender
{
	lblEchoHpf.text = [Global getFreqString:knobEchoHpf.value];
    cpvEchoHpf.progress = knobEchoHpf.value/HPF_FREQ_MAX;
    int knobValue = [Global knobValueToFreq:knobEchoHpf.value];
    if (sender) {
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_ECHO_DELAY_FB_HPF commandType:DSP_WRITE dspId:DSP_8 value:knobValue];
        } else {
            [viewController sendData:CMD_EFFECT_2_ECHO_DELAY_FB_HPF commandType:DSP_WRITE dspId:DSP_5 value:knobValue];
        }
    }
}

- (IBAction)knobEchoLpfDidChange:(id)sender
{
	lblEchoLpf.text = [Global getFreqString:knobEchoLpf.value];
    cpvEchoLpf.progress = knobEchoLpf.value/LPF_FREQ_MAX;
    int knobValue = [Global knobValueToFreq:knobEchoLpf.value];
    if (sender) {
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_ECHO_DELAY_FB_LPF commandType:DSP_WRITE dspId:DSP_8 value:knobValue];
        } else {
            [viewController sendData:CMD_EFFECT_2_ECHO_DELAY_FB_LPF commandType:DSP_WRITE dspId:DSP_5 value:knobValue];
        }
    }
}

#pragma mark -
#pragma mark Tap delay view event handler methods

- (IBAction)knobTapFbDidChange:(id)sender
{
	lblTapFb.text = [NSString stringWithFormat:@"%d%%", (int)knobTapFb.value];
    cpvTapFb.progress = knobTapFb.value/ECHO_DELAY_FB_MAX;
    if (sender) {
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_TAP_DELAY_FB commandType:DSP_WRITE dspId:DSP_8 value:knobTapFb.value];
        } else {
            [viewController sendData:CMD_EFFECT_2_TAP_DELAY_FB commandType:DSP_WRITE dspId:DSP_5 value:knobTapFb.value];
        }
    }
}

- (IBAction)knobTapHpfDidChange:(id)sender
{
	lblTapHpf.text = [Global getFreqString:knobTapHpf.value];
    cpvTapHpf.progress = knobTapHpf.value/HPF_FREQ_MAX;
    int knobValue = [Global knobValueToFreq:knobTapHpf.value];
    if (sender) {
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_TAP_DELAY_FB_HPF commandType:DSP_WRITE dspId:DSP_8 value:knobValue];
        } else {
            [viewController sendData:CMD_EFFECT_2_TAP_DELAY_FB_HPF commandType:DSP_WRITE dspId:DSP_5 value:knobValue];
        }
    }
}

- (IBAction)knobTapLpfDidChange:(id)sender
{
	lblTapLpf.text = [Global getFreqString:knobTapLpf.value];
    cpvTapLpf.progress = knobTapLpf.value/LPF_FREQ_MAX;
    int knobValue = [Global knobValueToFreq:knobTapLpf.value];
    if (sender) {
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_TAP_DELAY_FB_LPF commandType:DSP_WRITE dspId:DSP_8 value:knobValue];
        } else {
            [viewController sendData:CMD_EFFECT_2_TAP_DELAY_FB_LPF commandType:DSP_WRITE dspId:DSP_5 value:knobValue];
        }
    }
}

- (IBAction)onBtnTap:(id)sender
{
    btnTap.selected = !btnTap.selected;
    if (effectNum == EFX_1) {
        [viewController sendData:CMD_EFFECT_1_TAP_KEY commandType:DSP_WRITE dspId:DSP_8 value:btnTap.selected];
    } else {
        [viewController sendData:CMD_EFFECT_2_TAP_KEY commandType:DSP_WRITE dspId:DSP_5 value:btnTap.selected];
    }
}

#pragma mark -
#pragma mark Chorus view event handler methods

- (IBAction)knobChorusPreDelayDidChange:(id)sender
{
	lblChorusPreDelay.text = [NSString stringWithFormat:@"%.1fms", (float)knobChorusPreDelay.value/10.0];
    cpvChorusPreDelay.progress = knobChorusPreDelay.value/CHORUS_PRE_DELAY_MAX;
    if (sender) {
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_CHORUS_PRE_DELAY commandType:DSP_WRITE dspId:DSP_8 value:knobChorusPreDelay.value];
        } else {
            [viewController sendData:CMD_EFFECT_2_CHORUS_PRE_DELAY commandType:DSP_WRITE dspId:DSP_5 value:knobChorusPreDelay.value];
        }
    }
}

- (IBAction)knobChorusDepthDidChange:(id)sender
{
	lblChorusDepth.text = [NSString stringWithFormat:@"%d%%", (int)knobChorusDepth.value];
    cpvChorusDepth.progress = knobChorusDepth.value/CHORUS_DEPTH_MAX;
    if (sender) {
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_CHORUS_DEPTH commandType:DSP_WRITE dspId:DSP_8 value:knobChorusDepth.value];
        } else {
            [viewController sendData:CMD_EFFECT_2_CHORUS_DEPTH commandType:DSP_WRITE dspId:DSP_5 value:knobChorusDepth.value];
        }
    }
}

- (IBAction)knobChorusLpfDidChange:(id)sender
{
	lblChorusLpf.text = [Global getFreqString:knobChorusLpf.value];
    cpvChorusLpf.progress = knobChorusLpf.value/LPF_FREQ_MAX;
    int knobValue = [Global knobValueToFreq:knobChorusLpf.value];
    if (sender) {
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_CHORUS_LPF_FREQ commandType:DSP_WRITE dspId:DSP_8 value:knobValue];
        } else {
            [viewController sendData:CMD_EFFECT_2_CHORUS_LPF_FREQ commandType:DSP_WRITE dspId:DSP_5 value:knobValue];
        }
    }
}

- (IBAction)knobChorusLfoPhaseDidChange:(id)sender
{
	lblChorusLfoPhase.text = [NSString stringWithFormat:@"%d", (int)knobChorusLfoPhase.value];
    cpvChorusLfoPhase.progress = knobChorusLfoPhase.value/CHORUS_PHASE_MAX;
    if (sender) {
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_CHORUS_LFO_PHASE commandType:DSP_WRITE dspId:DSP_8 value:knobChorusLfoPhase.value];
        } else {
            [viewController sendData:CMD_EFFECT_2_CHORUS_LFO_PHASE commandType:DSP_WRITE dspId:DSP_5 value:knobChorusLfoPhase.value];
        }
    }
}

- (IBAction)knobChorusLfoFreqDidChange:(id)sender
{
	lblChorusLfoFreq.text = [NSString stringWithFormat:@"%.2f Hz", knobChorusLfoFreq.value * 0.01f];
    cpvChorusLfoFreq.progress = knobChorusLfoFreq.value/CHORUS_LFO_FREQ_MAX;
    if (sender) {
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_CHORUS_LFO_FREQ commandType:DSP_WRITE dspId:DSP_8 value:knobChorusLfoFreq.value];
        } else {
            [viewController sendData:CMD_EFFECT_2_CHORUS_LFO_FREQ commandType:DSP_WRITE dspId:DSP_5 value:knobChorusLfoFreq.value];
        }
    }
}

- (IBAction)onBtnLfoType:(id)sender
{
    btnChorusLfoType.selected = !btnChorusLfoType.selected;
    int lfoType = btnChorusLfoType.selected ? 2 : 1;
    if (effectNum == EFX_1) {        
        [viewController sendData:CMD_EFFECT_1_CHORUS_LFO_TYPE commandType:DSP_WRITE dspId:DSP_8 value:lfoType];
    } else {
        [viewController sendData:CMD_EFFECT_2_CHORUS_LFO_TYPE commandType:DSP_WRITE dspId:DSP_5 value:lfoType];
    }
}

#pragma mark -
#pragma mark Flanger view event handler methods

- (IBAction)knobFlangerPreDelayDidChange:(id)sender
{
	lblFlangerPreDelay.text = [NSString stringWithFormat:@"%.1fms", (float)knobFlangerPreDelay.value/10.0];
    cpvFlangerPreDelay.progress = knobFlangerPreDelay.value/CHORUS_PRE_DELAY_MAX;
    if (sender) {
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_FLANGER_PRE_DELAY commandType:DSP_WRITE dspId:DSP_8 value:knobFlangerPreDelay.value];
        } else {
            [viewController sendData:CMD_EFFECT_2_FLANGER_PRE_DELAY commandType:DSP_WRITE dspId:DSP_5 value:knobFlangerPreDelay.value];
        }
    }
}

- (IBAction)knobFlangerDepthDidChange:(id)sender
{
	lblFlangerDepth.text = [NSString stringWithFormat:@"%d%%", (int)knobFlangerDepth.value];
    cpvFlangerDepth.progress = knobFlangerDepth.value/CHORUS_DEPTH_MAX;
    if (sender) {
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_FLANGER_DEPTH commandType:DSP_WRITE dspId:DSP_8 value:knobFlangerDepth.value];
        } else {
            [viewController sendData:CMD_EFFECT_2_FLANGER_DEPTH commandType:DSP_WRITE dspId:DSP_5 value:knobFlangerDepth.value];
        }
    }
}

- (IBAction)knobFlangerLpfDidChange:(id)sender
{
	lblFlangerLpf.text = [Global getFreqString:knobFlangerLpf.value];
    cpvFlangerLpf.progress = knobFlangerLpf.value/LPF_FREQ_MAX;
    int knobValue = [Global knobValueToFreq:knobFlangerLpf.value];
    if (sender) {
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_FLANGER_LPF_FREQ commandType:DSP_WRITE dspId:DSP_8 value:knobValue];
        } else {
            [viewController sendData:CMD_EFFECT_2_FLANGER_LPF_FREQ commandType:DSP_WRITE dspId:DSP_5 value:knobValue];
        }
    }
}

- (IBAction)knobFlangerFbDidChange:(id)sender
{
	lblFlangerFb.text = [NSString stringWithFormat:@"%d%%", (int)knobFlangerFb.value];
    cpvFlangerFb.progress = knobFlangerFb.value/ECHO_DELAY_FB_MAX;
    if (sender) {
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_FLANGER_FB commandType:DSP_WRITE dspId:DSP_8 value:knobFlangerFb.value];
        } else {
            [viewController sendData:CMD_EFFECT_2_FLANGER_FB commandType:DSP_WRITE dspId:DSP_5 value:knobFlangerFb.value];
        }
    }
}

- (IBAction)knobFlangerLfoPhaseDidChange:(id)sender
{
	lblFlangerLfoPhase.text = [NSString stringWithFormat:@"%d", (int)knobFlangerLfoPhase.value];
    cpvFlangerLfoPhase.progress = knobFlangerLfoPhase.value/CHORUS_PHASE_MAX;
    if (sender) {
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_FLANGER_LFO_PHASE commandType:DSP_WRITE dspId:DSP_8 value:knobFlangerLfoPhase.value];
        } else {
            [viewController sendData:CMD_EFFECT_2_FLANGER_LFO_PHASE commandType:DSP_WRITE dspId:DSP_5 value:knobFlangerLfoPhase.value];
        }
    }
}

- (IBAction)knobFlangerLfoFreqDidChange:(id)sender
{
	lblFlangerLfoFreq.text = [NSString stringWithFormat:@"%.2f Hz", knobFlangerLfoFreq.value * 0.01f];
    cpvFlangerLfoFreq.progress = knobFlangerLfoFreq.value/CHORUS_LFO_FREQ_MAX;
    if (sender) {
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_FLANGER_LFO_FREQ commandType:DSP_WRITE dspId:DSP_8 value:knobFlangerLfoFreq.value];
        } else {
            [viewController sendData:CMD_EFFECT_2_FLANGER_LFO_FREQ commandType:DSP_WRITE dspId:DSP_5 value:knobFlangerLfoFreq.value];
        }
    }
}

- (IBAction)onBtnFlangerLfoType:(id)sender
{
    btnFlangerLfoType.selected = !btnFlangerLfoType.selected;
    int lfoType = btnFlangerLfoType.selected ? 2 : 1;
    if (effectNum == EFX_1) {
        [viewController sendData:CMD_EFFECT_1_FLANGER_LFO_TYPE commandType:DSP_WRITE dspId:DSP_8 value:lfoType];
    } else {
        [viewController sendData:CMD_EFFECT_2_FLANGER_LFO_TYPE commandType:DSP_WRITE dspId:DSP_5 value:lfoType];
    }
}

#pragma mark -
#pragma mark Phaser view event handler methods

- (IBAction)knobPhaserStageNoDidChange:(id)sender
{
    switch ((int)knobPhaserStageNo.value) {
        case 0:
            lblPhaserStageNo.text = @"2 stage";
            break;
        case 1:
            lblPhaserStageNo.text = @"4 stage";
            break;
        case 2:
            lblPhaserStageNo.text = @"6 stage";
            break;
        case 3:
            lblPhaserStageNo.text = @"8 stage";
            break;
        default:
            break;
    }
    cpvPhaserStageNo.progress = knobPhaserStageNo.value/PHASER_STAGENO_MAX;
    if (sender) {
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_PHASER_STAGE_NO commandType:DSP_WRITE dspId:DSP_8 value:knobPhaserStageNo.value];
        } else {
            [viewController sendData:CMD_EFFECT_2_PHASER_STAGE_NO commandType:DSP_WRITE dspId:DSP_5 value:knobPhaserStageNo.value];
        }
    }
}

- (IBAction)knobPhaserFreqDidChange:(id)sender
{
	lblPhaserFreq.text = [Global getFreqString:knobPhaserFreq.value];
    cpvPhaserFreq.progress = knobPhaserFreq.value/LPF_FREQ_MAX;
    int knobValue = [Global knobValueToFreq:knobPhaserFreq.value];
    if (sender) {
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_PHASER_FREQ commandType:DSP_WRITE dspId:DSP_8 value:knobValue];
        } else {
            [viewController sendData:CMD_EFFECT_2_PHASER_FREQ commandType:DSP_WRITE dspId:DSP_5 value:knobValue];
        }
    }
}

- (IBAction)knobPhaserDepthDidChange:(id)sender
{
	lblPhaserDepth.text = [NSString stringWithFormat:@"%d%%", (int)knobPhaserDepth.value];
    cpvPhaserDepth.progress = knobPhaserDepth.value/CHORUS_DEPTH_MAX;
    if (sender) {
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_PHASER_DEPTH commandType:DSP_WRITE dspId:DSP_8 value:knobPhaserDepth.value];
        } else {
            [viewController sendData:CMD_EFFECT_2_PHASER_DEPTH commandType:DSP_WRITE dspId:DSP_5 value:knobPhaserDepth.value];
        }
    }
}

- (IBAction)knobPhaserLfoFreqDidChange:(id)sender
{
	lblPhaserLfoFreq.text = [NSString stringWithFormat:@"%.2f Hz", knobPhaserLfoFreq.value * 0.01f];
    cpvPhaserLfoFreq.progress = knobPhaserLfoFreq.value/CHORUS_LFO_FREQ_MAX;
    if (sender) {
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_PHASER_LFO_FREQ commandType:DSP_WRITE dspId:DSP_8 value:knobPhaserLfoFreq.value];
        } else {
            [viewController sendData:CMD_EFFECT_2_PHASER_LFO_FREQ commandType:DSP_WRITE dspId:DSP_5 value:knobPhaserLfoFreq.value];
        }
    }
}

- (IBAction)onBtnPhaserLfoType:(id)sender
{
    btnPhaserLfoType.selected = !btnPhaserLfoType.selected;
    int lfoType = btnPhaserLfoType.selected ? 2 : 1;
    if (effectNum == EFX_1) {
        [viewController sendData:CMD_EFFECT_1_PHASER_LFO_TYPE commandType:DSP_WRITE dspId:DSP_8 value:lfoType];
    } else {
        [viewController sendData:CMD_EFFECT_2_PHASER_LFO_TYPE commandType:DSP_WRITE dspId:DSP_5 value:lfoType];
    }
}

#pragma mark -
#pragma mark Tremolo view event handler methods

- (IBAction)knobTremoloDepthDidChange:(id)sender
{
	lblTremoloDepth.text = [NSString stringWithFormat:@"%d%%", (int)knobTremoloDepth.value];
    cpvTremoloDepth.progress = knobTremoloDepth.value/CHORUS_DEPTH_MAX;
    if (sender) {
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_TREMOLO_DEPTH commandType:DSP_WRITE dspId:DSP_8 value:knobTremoloDepth.value];
        } else {
            [viewController sendData:CMD_EFFECT_2_TREMOLO_DEPTH commandType:DSP_WRITE dspId:DSP_5 value:knobTremoloDepth.value];
        }
    }
}

- (IBAction)knobTremoloLfoFreqDidChange:(id)sender
{
	lblTremoloLfoFreq.text = [NSString stringWithFormat:@"%.2f Hz", knobTremoloLfoFreq.value * 0.01f];;
    cpvTremoloLfoFreq.progress = knobTremoloLfoFreq.value/CHORUS_LFO_FREQ_MAX;
    if (sender) {
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_TREMOLO_LFO_FREQ commandType:DSP_WRITE dspId:DSP_8 value:knobTremoloLfoFreq.value];
        } else {
            [viewController sendData:CMD_EFFECT_2_TREMOLO_LFO_FREQ commandType:DSP_WRITE dspId:DSP_5 value:knobTremoloLfoFreq.value];
        }
    }
}

- (IBAction)onBtnTremoloLfoType:(id)sender
{
    btnTremoloLfoType.selected = !btnTremoloLfoType.selected;
    int lfoType = btnTremoloLfoType.selected ? 2 : 1;
    if (sender) {
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_TREMOLO_LFO_TYPE commandType:DSP_WRITE dspId:DSP_8 value:lfoType];
        } else {
            [viewController sendData:CMD_EFFECT_2_TREMOLO_LFO_TYPE commandType:DSP_WRITE dspId:DSP_5 value:lfoType];
        }
    }
}

#pragma mark -
#pragma mark Vibrato view event handler methods

- (IBAction)knobVibratoFreqDidChange:(id)sender
{
    lblVibratoFreq.text = [Global getFreqString:knobVibratoFreq.value];
    cpvVibratoFreq.progress = knobVibratoFreq.value/VIBRATO_FREQ_MAX;
    int knobValue = [Global knobValueToFreq:knobVibratoFreq.value];
    if (sender) {
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_VIBRATO_FREQ commandType:DSP_WRITE dspId:DSP_8 value:knobValue];
        } else {
            [viewController sendData:CMD_EFFECT_2_VIBRATO_FREQ commandType:DSP_WRITE dspId:DSP_5 value:knobValue];
        }
    }
}

- (IBAction)knobVibratoDepthDidChange:(id)sender
{
	lblVibratoDepth.text = [NSString stringWithFormat:@"%d%%", (int)knobVibratoDepth.value];
    cpvVibratoDepth.progress = knobVibratoDepth.value/CHORUS_DEPTH_MAX;
    if (sender) {
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_VIBRATO_DEPTH commandType:DSP_WRITE dspId:DSP_8 value:knobVibratoDepth.value];
        } else {
            [viewController sendData:CMD_EFFECT_2_VIBRATO_DEPTH commandType:DSP_WRITE dspId:DSP_5 value:knobVibratoDepth.value];
        }
    }
}

- (IBAction)knobVibratoLfoFreqDidChange:(id)sender
{
	lblVibratoLfoFreq.text = [NSString stringWithFormat:@"%.2f Hz", knobVibratoLfoFreq.value * 0.01f];
    cpvVibratoLfoFreq.progress = knobVibratoLfoFreq.value/CHORUS_LFO_FREQ_MAX;
    if (sender) {
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_VIBRATO_LFO_FREQ commandType:DSP_WRITE dspId:DSP_8 value:knobVibratoLfoFreq.value];
        } else {
            [viewController sendData:CMD_EFFECT_2_VIBRATO_LFO_FREQ commandType:DSP_WRITE dspId:DSP_5 value:knobVibratoLfoFreq.value];
        }
    }
}

- (IBAction)onBtnVibratoLfoType:(id)sender
{
    btnVibratoLfoType.selected = !btnVibratoLfoType.selected;
    int lfoType = btnVibratoLfoType.selected ? 2 : 1;
    if (effectNum == EFX_1) {
        [viewController sendData:CMD_EFFECT_1_VIBRATO_LFO_TYPE commandType:DSP_WRITE dspId:DSP_8 value:lfoType];
    } else {
        [viewController sendData:CMD_EFFECT_2_VIBRATO_LFO_TYPE commandType:DSP_WRITE dspId:DSP_5 value:lfoType];
    }
}

#pragma mark -
#pragma mark Auto Pan view event handler methods

- (IBAction)knobAutoPanWayDidChange:(id)sender
{
    switch ((int)knobAutoPanWay.value) {
        case 0:
            lblAutoPanWay.text = @"L<->R";
            break;
        case 1:
            lblAutoPanWay.text = @"L->R";
            break;
        case 2:
            lblAutoPanWay.text = @"R->L";
            break;
        default:
            break;
    }
    cpvAutoPanWay.progress = knobAutoPanWay.value/AUTOPAN_WAY_MAX;
    if (sender) {
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_AUTOPAN_WAY commandType:DSP_WRITE dspId:DSP_8 value:knobAutoPanWay.value];
        } else {
            [viewController sendData:CMD_EFFECT_2_AUTOPAN_WAY commandType:DSP_WRITE dspId:DSP_5 value:knobAutoPanWay.value];
        }
    }
}

- (IBAction)knobAutoPanDepthDidChange:(id)sender
{
	lblAutoPanDepth.text = [NSString stringWithFormat:@"%d%%", (int)knobAutoPanDepth.value];
    cpvAutoPanDepth.progress = knobAutoPanDepth.value/CHORUS_DEPTH_MAX;
    if (sender) {
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_AUTOPAN_DEPTH commandType:DSP_WRITE dspId:DSP_8 value:knobAutoPanDepth.value];
        } else {
            [viewController sendData:CMD_EFFECT_2_AUTOPAN_DEPTH commandType:DSP_WRITE dspId:DSP_5 value:knobAutoPanDepth.value];
        }
    }
}

- (IBAction)knobAutoPanLfoFreqDidChange:(id)sender
{
	lblAutoPanLfoFreq.text = [NSString stringWithFormat:@"%.2f Hz", knobAutoPanLfoFreq.value * 0.01f];
    cpvAutoPanLfoFreq.progress = knobAutoPanLfoFreq.value/CHORUS_LFO_FREQ_MAX;
    if (sender) {
        if (effectNum == EFX_1) {
            [viewController sendData:CMD_EFFECT_1_AUTOPAN_LFO_FREQ commandType:DSP_WRITE dspId:DSP_8 value:knobAutoPanLfoFreq.value];
        } else {
            [viewController sendData:CMD_EFFECT_2_AUTOPAN_LFO_FREQ commandType:DSP_WRITE dspId:DSP_5 value:knobAutoPanLfoFreq.value];
        }
    }
}

- (IBAction)onBtnAutoPanLfoType:(id)sender
{
    btnAutoPanLfoType.selected = !btnAutoPanLfoType.selected;
    int lfoType = btnAutoPanLfoType.selected ? 2 : 1;
    if (effectNum == EFX_1) {
        [viewController sendData:CMD_EFFECT_1_AUTOPAN_LFO_TYPE commandType:DSP_WRITE dspId:DSP_8 value:lfoType];
    } else {
        [viewController sendData:CMD_EFFECT_2_AUTOPAN_LFO_TYPE commandType:DSP_WRITE dspId:DSP_5 value:lfoType];
    }
}

#pragma mark -
#pragma mark Callback from Geq31ViewController & Geq15ViewController

- (void)didFinishEditingGeq31 {
    if (effectGeq31Controller == nil) {
        return;
    }
    
    UIImage *image = effectGeq31Controller.imgEqL;
    if (image != nil) {
        [imgGeq31LPreview setImage:image];
    }
    image = effectGeq31Controller.imgEqR;
    if (image != nil) {
        [imgGeq31RPreview setImage:image];
    }
}

- (void)didFinishEditingGeq15 {
    if (effectGeq15Controller == nil) {
        return;
    }
    
    UIImage *image = effectGeq15Controller.imgEqL;
    if (image != nil) {
        [imgGeq15LPreview setImage:image];
    }
    image = effectGeq15Controller.imgEqR;
    if (image != nil) {
        [imgGeq15RPreview setImage:image];
    }
}

- (void)setSceneFilename:(NSString*)fileName {
    [btnFile setTitle:fileName forState:UIControlStateNormal];
}

@end
