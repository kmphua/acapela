//
//  GroupSendViewController.h
//  eLive
//
//  Created by Kevin on 13/1/3.
//  Copyright (c) 2013年 Kevin Phua. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "acapela.h"
#import "JEProgressView.h"

enum GroupChannel {
    GROUP_1 = 0,
    GROUP_2,
    GROUP_3,
    GROUP_4,
    GROUP_MAX
};

@interface GroupSendViewController : UIViewController<UITextFieldDelegate>
{
    enum GroupChannel currentGroup;
    enum GroupChannel currentGroupLabel;
}

@property (nonatomic) enum GroupChannel currentGroup;
@property (nonatomic) enum GroupChannel currentGroupLabel;

// Group sends panel
@property (strong, nonatomic) IBOutlet UIButton *btnGroupSendPrePostAll;
@property (weak, nonatomic) IBOutlet UIButton *btnGroupSend1;
@property (weak, nonatomic) IBOutlet UIButton *btnGroupSend2;
@property (weak, nonatomic) IBOutlet UIButton *btnGroupSend3;
@property (weak, nonatomic) IBOutlet UIButton *btnGroupSend4;
@property (weak, nonatomic) IBOutlet UIButton *btnGroupSend5;
@property (weak, nonatomic) IBOutlet UIButton *btnGroupSend6;
@property (weak, nonatomic) IBOutlet UIButton *btnGroupSend7;
@property (weak, nonatomic) IBOutlet UIButton *btnGroupSend8;
@property (weak, nonatomic) IBOutlet UIButton *btnGroupSend9;
@property (weak, nonatomic) IBOutlet UIButton *btnGroupSend10;
@property (weak, nonatomic) IBOutlet UIButton *btnGroupSend11;
@property (weak, nonatomic) IBOutlet UIButton *btnGroupSend12;
@property (weak, nonatomic) IBOutlet UIButton *btnGroupSend13;
@property (weak, nonatomic) IBOutlet UIButton *btnGroupSend14;
@property (weak, nonatomic) IBOutlet UIButton *btnGroupSend15;
@property (weak, nonatomic) IBOutlet UIButton *btnGroupSend16;
@property (weak, nonatomic) IBOutlet UIButton *btnGroupSend17;
@property (weak, nonatomic) IBOutlet UIButton *btnGroupSend18;
@property (weak, nonatomic) IBOutlet UILabel *lblGroupSend1;
@property (weak, nonatomic) IBOutlet UILabel *lblGroupSend2;
@property (weak, nonatomic) IBOutlet UILabel *lblGroupSend3;
@property (weak, nonatomic) IBOutlet UILabel *lblGroupSend4;
@property (weak, nonatomic) IBOutlet UILabel *lblGroupSend5;
@property (weak, nonatomic) IBOutlet UILabel *lblGroupSend6;
@property (weak, nonatomic) IBOutlet UILabel *lblGroupSend7;
@property (weak, nonatomic) IBOutlet UILabel *lblGroupSend8;
@property (weak, nonatomic) IBOutlet UILabel *lblGroupSend9;
@property (weak, nonatomic) IBOutlet UILabel *lblGroupSend10;
@property (weak, nonatomic) IBOutlet UILabel *lblGroupSend11;
@property (weak, nonatomic) IBOutlet UILabel *lblGroupSend12;
@property (weak, nonatomic) IBOutlet UILabel *lblGroupSend13;
@property (weak, nonatomic) IBOutlet UILabel *lblGroupSend14;
@property (weak, nonatomic) IBOutlet UILabel *lblGroupSend15;
@property (weak, nonatomic) IBOutlet UILabel *lblGroupSend16;
@property (weak, nonatomic) IBOutlet UILabel *lblGroupSend17;
@property (weak, nonatomic) IBOutlet UILabel *lblGroupSend18;
@property (strong, nonatomic) IBOutlet UIButton *btnGroupSendSolo;
@property (strong, nonatomic) IBOutlet UIButton *btnGroupSendOn;
@property (strong, nonatomic) IBOutlet UIButton *btnGroupSelectDown;
@property (strong, nonatomic) IBOutlet UIButton *btnGroupSelectUp;
@property (weak, nonatomic) IBOutlet UIButton *btnGroupMeter;
@property (strong, nonatomic) IBOutlet UITextField *txtCurrentGroup;
@property (strong, nonatomic) IBOutlet UILabel *lblCurrentGroup;
@property (nonatomic,retain) UISlider *sliderGroupSendMain;
@property (nonatomic,retain) JEProgressView *meterGroupSendMain;
@property (weak, nonatomic) IBOutlet UILabel *lblSliderGroupMainValue;
@property (strong, nonatomic) IBOutlet UIButton *btnGroupMulti1;
@property (strong, nonatomic) IBOutlet UIButton *btnGroupMulti2;
@property (strong, nonatomic) IBOutlet UIButton *btnGroupMulti3;
@property (strong, nonatomic) IBOutlet UIButton *btnGroupMulti4;
@property (strong, nonatomic) IBOutlet UIButton *btnGroupEfx1OnOff;
@property (strong, nonatomic) IBOutlet UIButton *btnGroupEfx2OnOff;
@property (strong, nonatomic) IBOutlet UIButton *btnGroupToMain;
@property (nonatomic,retain) JEProgressView *meterGroupSend1;
@property (nonatomic,retain) JEProgressView *meterGroupSend2;
@property (nonatomic,retain) JEProgressView *meterGroupSend3;
@property (nonatomic,retain) JEProgressView *meterGroupSend4;
@property (nonatomic,retain) JEProgressView *meterGroupSend5;
@property (nonatomic,retain) JEProgressView *meterGroupSend6;
@property (nonatomic,retain) JEProgressView *meterGroupSend7;
@property (nonatomic,retain) JEProgressView *meterGroupSend8;
@property (nonatomic,retain) JEProgressView *meterGroupSend9;
@property (nonatomic,retain) JEProgressView *meterGroupSend10;
@property (nonatomic,retain) JEProgressView *meterGroupSend11;
@property (nonatomic,retain) JEProgressView *meterGroupSend12;
@property (nonatomic,retain) JEProgressView *meterGroupSend13;
@property (nonatomic,retain) JEProgressView *meterGroupSend14;
@property (nonatomic,retain) JEProgressView *meterGroupSend15;
@property (nonatomic,retain) JEProgressView *meterGroupSend16;
@property (nonatomic,retain) JEProgressView *meterGroupSend17;
@property (nonatomic,retain) JEProgressView *meterGroupSend18;

// PAN sliders
@property (nonatomic,retain) UISlider *sliderPan;
@property (nonatomic,retain) JEProgressView *meterPan;
@property (weak, nonatomic) IBOutlet UILabel *lblPanValue;
@property (weak, nonatomic) IBOutlet UIImageView *imgPan;

- (IBAction)onBtnGroupSendPrePostAll:(id)sender;
- (IBAction)onBtnGroupSend1:(id)sender;
- (IBAction)onBtnGroupSend2:(id)sender;
- (IBAction)onBtnGroupSend3:(id)sender;
- (IBAction)onBtnGroupSend4:(id)sender;
- (IBAction)onBtnGroupSend5:(id)sender;
- (IBAction)onBtnGroupSend6:(id)sender;
- (IBAction)onBtnGroupSend7:(id)sender;
- (IBAction)onBtnGroupSend8:(id)sender;
- (IBAction)onBtnGroupSend9:(id)sender;
- (IBAction)onBtnGroupSend10:(id)sender;
- (IBAction)onBtnGroupSend11:(id)sender;
- (IBAction)onBtnGroupSend12:(id)sender;
- (IBAction)onBtnGroupSend13:(id)sender;
- (IBAction)onBtnGroupSend14:(id)sender;
- (IBAction)onBtnGroupSend15:(id)sender;
- (IBAction)onBtnGroupSend16:(id)sender;
- (IBAction)onBtnGroupSend17:(id)sender;
- (IBAction)onBtnGroupSend18:(id)sender;
- (IBAction)onBtnGroupSolo:(id)sender;
- (IBAction)onBtnGroupOn:(id)sender;
- (IBAction)onBtnGroupSelectDown:(id)sender;
- (IBAction)onBtnGroupSelectUp:(id)sender;
- (IBAction)onBtnGroupMeter:(id)sender;
- (IBAction)onBtnGroupMulti1:(id)sender;
- (IBAction)onBtnGroupMulti2:(id)sender;
- (IBAction)onBtnGroupMulti3:(id)sender;
- (IBAction)onBtnGroupMulti4:(id)sender;
- (IBAction)onBtnGroupEfx1OnOff:(id)sender;
- (IBAction)onBtnGroupEfx2OnOff:(id)sender;
- (IBAction)onBtnGroupToMain:(id)sender;

- (void)processReply:(GpPagePacket *)pkt;
- (void)processMeter:(AllMeterValueRxPacket *)pkt;
- (void)updateCurrentGroupLabel;

@end
