//
//  GeqViewController.m
//  Acapela
//
//  Created by Kevin on 12/12/27.
//  Copyright (c) 2012年 Kevin Phua. All rights reserved.
//

#import "GeqPeqViewController.h"
#import "acapela.h"
#import "AppDelegate.h"
#import "ViewController.h"
#import "Global.h"
#import "JEProgressView.h"

@interface GeqPeqViewController ()

@end

#define GEQ_SLIDER_MIN_VALUE    -12.0
#define GEQ_SLIDER_MAX_VALUE    12.0
#define PEQ_FRAME_WIDTH         546.0
#define PEQ_FRAME_HEIGHT        172.0

// GFQ indexes
#define PEQ_DEFAULT_G           36
#define PEQ_DEFAULT_Q           16
#define PEQ_1_DEFAULT_F         49
#define PEQ_2_DEFAULT_F         119
#define PEQ_3_DEFAULT_F         183
#define PEQ_4_DEFAULT_F         203

#define PEQ_14_Q_MAX_VALUE      41
#define PEQ_23_Q_MAX_VALUE      39

#define PEQ_1_LOW_SHELF         40
#define PEQ_1_HPF               41
#define PEQ_4_HIGH_SHELF        40
#define PEQ_4_LPF               41

#define MAIN_EQ_PEQ              0
#define MAIN_EQ_GEQ              1

typedef enum
{
    EQ_PEAK = 0,
    EQ_LOW_SHELF,
    EQ_LOW_CUT,
    EQ_HIGH_SHELF,
    EQ_HIGH_CUT
} PEQ_EQ_FILTER;

@implementation GeqPeqViewController
{
    ViewController *viewController;
    NSMutableArray *peqData;
    NSMutableArray *peqData1;
    NSMutableArray *peqData2;
    NSMutableArray *peqData3;
    NSMutableArray *peqData4;
    PEQ_EQ_FILTER peq1Filter[CHANNEL_MAX];
    PEQ_EQ_FILTER peq4Filter[CHANNEL_MAX];
    int peq1G[CHANNEL_MAX], peq1F[CHANNEL_MAX], peq1Q[CHANNEL_MAX], lastPeq1Q[CHANNEL_MAX];
    int peq2G[CHANNEL_MAX], peq2F[CHANNEL_MAX], peq2Q[CHANNEL_MAX];
    int peq3G[CHANNEL_MAX], peq3F[CHANNEL_MAX], peq3Q[CHANNEL_MAX];
    int peq4G[CHANNEL_MAX], peq4F[CHANNEL_MAX], peq4Q[CHANNEL_MAX], lastPeq4Q[CHANNEL_MAX];
    BOOL peq1Enabled[CHANNEL_MAX], peq2Enabled[CHANNEL_MAX], peq3Enabled[CHANNEL_MAX], peq4Enabled[CHANNEL_MAX];
    BOOL peqEnabled[CHANNEL_MAX];
    BOOL sliderLock;
    int chMeterPrePost, multiMeterPrePost;
    BOOL mainEqLock;
    BOOL geqLock;
    BOOL peqLock;
    NSTimer *channelScrollTimer;
    int currentBall;        // Current ball selected
    int mainEqType;
    
    int _ch1Ctrl, _ch2Ctrl, _ch3Ctrl, _ch4Ctrl;
    int _ch5Ctrl, _ch6Ctrl, _ch7Ctrl, _ch8Ctrl;
    int _ch9Ctrl, _ch10Ctrl, _ch11Ctrl, _ch12Ctrl;
    int _ch13Ctrl, _ch14Ctrl, _ch15Ctrl, _ch16Ctrl;
    int _ch17Ctrl, _ch18Ctrl, _multi1Ctrl, _multi2Ctrl;
    int _multi3Ctrl, _multi4Ctrl, _mainCtrl;
    int _soloCtrl, _chOnOff, _chMeterPrePost;
    int _ch17Setting, _ch18Setting, _multiMeter, _mainMeter;
}

@synthesize currentChannel, currentChannelLabel;
@synthesize peqImages, eqPreviewImages;
@synthesize imgBackground;
@synthesize btnFile, btnSelectDown, btnSelectUp, btnSolo, btnOn;
@synthesize lblPage, lblSliderValue, slider, meterL, meterR, btnMeterPrePost, txtPage, imgPanelBg;
@synthesize meterInL, meterInR, meterOutL, meterOutR, meterIn, meterOut;
@synthesize viewPeq, btnPeqOnOff, viewPeqFrame;

@synthesize viewGeq,btnGeqOnOff,btnGeqDraw,viewGeqFrame,viewGeqDraw,lblGeq,sliderGeq1,sliderGeq2,sliderGeq3,sliderGeq4,sliderGeq5,sliderGeq6,sliderGeq7,sliderGeq8;
@synthesize sliderGeq9,sliderGeq10,sliderGeq11,sliderGeq12,sliderGeq13,sliderGeq14,sliderGeq15,sliderGeq16;
@synthesize sliderGeq17,sliderGeq18,sliderGeq19,sliderGeq20,sliderGeq21,sliderGeq22,sliderGeq23,sliderGeq24;
@synthesize sliderGeq25,sliderGeq26,sliderGeq27,sliderGeq28,sliderGeq29,sliderGeq30,sliderGeq31;
@synthesize meter1Top, meter1Btm, meter2Top, meter2Btm, meter3Top, meter3Btm;
@synthesize meter4Top, meter4Btm, meter5Top, meter5Btm, meter6Top, meter6Btm;
@synthesize meter7Top, meter7Btm, meter8Top, meter8Btm, meter9Top, meter9Btm;
@synthesize meter10Top, meter10Btm, meter11Top, meter11Btm, meter12Top, meter12Btm;
@synthesize meter13Top, meter13Btm, meter14Top, meter14Btm, meter15Top, meter15Btm;
@synthesize meter16Top, meter16Btm, meter17Top, meter17Btm, meter18Top, meter18Btm;
@synthesize meter19Top, meter19Btm, meter20Top, meter20Btm, meter21Top, meter21Btm;
@synthesize meter22Top, meter22Btm, meter23Top, meter23Btm, meter24Top, meter24Btm;
@synthesize meter25Top, meter25Btm, meter26Top, meter26Btm, meter27Top, meter27Btm;
@synthesize meter28Top, meter28Btm, meter29Top, meter29Btm, meter30Top, meter30Btm;
@synthesize meter31Top, meter31Btm;

@synthesize btnBall1, btnBall2, btnBall3, btnBall4;
@synthesize btnPeq1, btnPeq2, btnPeq3, btnPeq4, btnPeqFilterFront, btnPeqFilterBack;
@synthesize lblPeq1ValueG, lblPeq1ValueF, lblPeq1ValueQ;
@synthesize lblPeq2ValueG, lblPeq2ValueF, lblPeq2ValueQ;
@synthesize lblPeq3ValueG, lblPeq3ValueF, lblPeq3ValueQ;
@synthesize lblPeq4ValueG, lblPeq4ValueF, lblPeq4ValueQ;
@synthesize viewSelectFilterFront, viewSelectFilterBack;
@synthesize hostView, delegate;

static BOOL initDone = NO;

- (void)resetGeqDataDefault
{
    geqLock = YES;
    
    sliderGeq1.value = 0; sliderGeq2.value = 0; sliderGeq3.value = 0;
    sliderGeq4.value = 0; sliderGeq5.value = 0; sliderGeq6.value = 0;
    sliderGeq7.value = 0; sliderGeq8.value = 0; sliderGeq9.value = 0;
    sliderGeq10.value = 0; sliderGeq11.value = 0; sliderGeq12.value = 0;
    sliderGeq13.value = 0; sliderGeq14.value = 0; sliderGeq15.value = 0;
    sliderGeq16.value = 0; sliderGeq17.value = 0; sliderGeq18.value = 0;
    sliderGeq19.value = 0; sliderGeq20.value = 0; sliderGeq21.value = 0;
    sliderGeq22.value = 0; sliderGeq23.value = 0; sliderGeq24.value = 0;
    sliderGeq25.value = 0; sliderGeq26.value = 0; sliderGeq27.value = 0;
    sliderGeq28.value = 0; sliderGeq29.value = 0; sliderGeq30.value = 0;
    sliderGeq31.value = 0;
    
    [self onSlider1Action:sliderGeq1];     [self onSlider2Action:sliderGeq2];
    [self onSlider3Action:sliderGeq3];     [self onSlider4Action:sliderGeq4];
    [self onSlider5Action:sliderGeq5];     [self onSlider6Action:sliderGeq6];
    [self onSlider7Action:sliderGeq7];     [self onSlider8Action:sliderGeq8];
    [self onSlider9Action:sliderGeq9];     [self onSlider10Action:sliderGeq10];
    [self onSlider11Action:sliderGeq11];     [self onSlider12Action:sliderGeq12];
    [self onSlider13Action:sliderGeq13];     [self onSlider14Action:sliderGeq14];
    [self onSlider15Action:sliderGeq15];     [self onSlider16Action:sliderGeq16];
    [self onSlider17Action:sliderGeq17];     [self onSlider18Action:sliderGeq18];
    [self onSlider19Action:sliderGeq19];     [self onSlider20Action:sliderGeq20];
    [self onSlider21Action:sliderGeq21];     [self onSlider22Action:sliderGeq22];
    [self onSlider23Action:sliderGeq23];     [self onSlider24Action:sliderGeq24];
    [self onSlider25Action:sliderGeq25];     [self onSlider26Action:sliderGeq26];
    [self onSlider27Action:sliderGeq27];     [self onSlider28Action:sliderGeq28];
    [self onSlider29Action:sliderGeq29];     [self onSlider30Action:sliderGeq30];
    [self onSlider31Action:sliderGeq31];
    
    [self updatePreviewImage];
    
    geqLock = NO;
}

- (void)resetPeqDataDefault
{
    [self onBtnFilter1Peak:self];
    [self onBtnFilter2Peak:self];
    
    peq1G[currentChannel] = peq2G[currentChannel] = peq3G[currentChannel] = peq4G[currentChannel] = PEQ_DEFAULT_G;
    peq1F[currentChannel] = PEQ_1_DEFAULT_F; peq2F[currentChannel] = PEQ_2_DEFAULT_F; peq3F[currentChannel] = PEQ_3_DEFAULT_F; peq4F[currentChannel] = PEQ_4_DEFAULT_F;
    peq1Q[currentChannel] = peq2Q[currentChannel] = peq3Q[currentChannel] = peq4Q[currentChannel] = PEQ_DEFAULT_Q;
    //peq1Enabled[currentChannel] = peq2Enabled[currentChannel] = peq3Enabled[currentChannel] = peq4Enabled[currentChannel] = NO;
    [self updatePeqFrame];
    
    // Send reset data to device
    // Send data
    switch (currentChannel) {
        case CHANNEL_CH_1:
            [viewController sendData:CMD_CH1_EQ1_DB commandType:DSP_WRITE dspId:DSP_1 value:peq1G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH1_EQ1_FREQ commandType:DSP_WRITE dspId:DSP_1 value:peq1F[currentChannel]];
            [viewController sendData:CMD_CH1_EQ2_DB commandType:DSP_WRITE dspId:DSP_1 value:peq2G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH1_EQ2_FREQ commandType:DSP_WRITE dspId:DSP_1 value:peq2F[currentChannel]];
            [viewController sendData:CMD_CH1_EQ3_DB commandType:DSP_WRITE dspId:DSP_1 value:peq3G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH1_EQ3_FREQ commandType:DSP_WRITE dspId:DSP_1 value:peq3F[currentChannel]];
            [viewController sendData:CMD_CH1_EQ4_DB commandType:DSP_WRITE dspId:DSP_1 value:peq4G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH1_EQ4_FREQ commandType:DSP_WRITE dspId:DSP_1 value:peq4F[currentChannel]];
            break;
        case CHANNEL_CH_2:
            [viewController sendData:CMD_CH2_EQ1_DB commandType:DSP_WRITE dspId:DSP_1 value:peq1G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH2_EQ1_FREQ commandType:DSP_WRITE dspId:DSP_1 value:peq1F[currentChannel]];
            [viewController sendData:CMD_CH2_EQ2_DB commandType:DSP_WRITE dspId:DSP_1 value:peq2G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH2_EQ2_FREQ commandType:DSP_WRITE dspId:DSP_1 value:peq2F[currentChannel]];
            [viewController sendData:CMD_CH2_EQ3_DB commandType:DSP_WRITE dspId:DSP_1 value:peq3G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH2_EQ3_FREQ commandType:DSP_WRITE dspId:DSP_1 value:peq3F[currentChannel]];
            [viewController sendData:CMD_CH2_EQ4_DB commandType:DSP_WRITE dspId:DSP_1 value:peq4G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH2_EQ4_FREQ commandType:DSP_WRITE dspId:DSP_1 value:peq4F[currentChannel]];
            break;
        case CHANNEL_CH_3:
            [viewController sendData:CMD_CH3_EQ1_DB commandType:DSP_WRITE dspId:DSP_1 value:peq1G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH3_EQ1_FREQ commandType:DSP_WRITE dspId:DSP_1 value:peq1F[currentChannel]];
            [viewController sendData:CMD_CH3_EQ2_DB commandType:DSP_WRITE dspId:DSP_1 value:peq2G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH3_EQ2_FREQ commandType:DSP_WRITE dspId:DSP_1 value:peq2F[currentChannel]];
            [viewController sendData:CMD_CH3_EQ3_DB commandType:DSP_WRITE dspId:DSP_1 value:peq3G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH3_EQ3_FREQ commandType:DSP_WRITE dspId:DSP_1 value:peq3F[currentChannel]];
            [viewController sendData:CMD_CH3_EQ4_DB commandType:DSP_WRITE dspId:DSP_1 value:peq4G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH3_EQ4_FREQ commandType:DSP_WRITE dspId:DSP_1 value:peq4F[currentChannel]];
            break;
        case CHANNEL_CH_4:
            [viewController sendData:CMD_CH4_EQ1_DB commandType:DSP_WRITE dspId:DSP_1 value:peq1G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH4_EQ1_FREQ commandType:DSP_WRITE dspId:DSP_1 value:peq1F[currentChannel]];
            [viewController sendData:CMD_CH4_EQ2_DB commandType:DSP_WRITE dspId:DSP_1 value:peq2G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH4_EQ2_FREQ commandType:DSP_WRITE dspId:DSP_1 value:peq2F[currentChannel]];
            [viewController sendData:CMD_CH4_EQ3_DB commandType:DSP_WRITE dspId:DSP_1 value:peq3G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH4_EQ3_FREQ commandType:DSP_WRITE dspId:DSP_1 value:peq3F[currentChannel]];
            [viewController sendData:CMD_CH4_EQ4_DB commandType:DSP_WRITE dspId:DSP_1 value:peq4G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH4_EQ4_FREQ commandType:DSP_WRITE dspId:DSP_1 value:peq4F[currentChannel]];
            break;
        case CHANNEL_CH_5:
            [viewController sendData:CMD_CH1_EQ1_DB commandType:DSP_WRITE dspId:DSP_2 value:peq1G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH1_EQ1_FREQ commandType:DSP_WRITE dspId:DSP_2 value:peq1F[currentChannel]];
            [viewController sendData:CMD_CH1_EQ2_DB commandType:DSP_WRITE dspId:DSP_2 value:peq2G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH1_EQ2_FREQ commandType:DSP_WRITE dspId:DSP_2 value:peq2F[currentChannel]];
            [viewController sendData:CMD_CH1_EQ3_DB commandType:DSP_WRITE dspId:DSP_2 value:peq3G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH1_EQ3_FREQ commandType:DSP_WRITE dspId:DSP_2 value:peq3F[currentChannel]];
            [viewController sendData:CMD_CH1_EQ4_DB commandType:DSP_WRITE dspId:DSP_2 value:peq4G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH1_EQ4_FREQ commandType:DSP_WRITE dspId:DSP_2 value:peq4F[currentChannel]];
            break;
        case CHANNEL_CH_6:
            [viewController sendData:CMD_CH2_EQ1_DB commandType:DSP_WRITE dspId:DSP_2 value:peq1G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH2_EQ1_FREQ commandType:DSP_WRITE dspId:DSP_2 value:peq1F[currentChannel]];
            [viewController sendData:CMD_CH2_EQ2_DB commandType:DSP_WRITE dspId:DSP_2 value:peq2G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH2_EQ2_FREQ commandType:DSP_WRITE dspId:DSP_2 value:peq2F[currentChannel]];
            [viewController sendData:CMD_CH2_EQ3_DB commandType:DSP_WRITE dspId:DSP_2 value:peq3G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH2_EQ3_FREQ commandType:DSP_WRITE dspId:DSP_2 value:peq3F[currentChannel]];
            [viewController sendData:CMD_CH2_EQ4_DB commandType:DSP_WRITE dspId:DSP_2 value:peq4G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH2_EQ4_FREQ commandType:DSP_WRITE dspId:DSP_2 value:peq4F[currentChannel]];
            break;
        case CHANNEL_CH_7:
            [viewController sendData:CMD_CH3_EQ1_DB commandType:DSP_WRITE dspId:DSP_2 value:peq1G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH3_EQ1_FREQ commandType:DSP_WRITE dspId:DSP_2 value:peq1F[currentChannel]];
            [viewController sendData:CMD_CH3_EQ2_DB commandType:DSP_WRITE dspId:DSP_2 value:peq2G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH3_EQ2_FREQ commandType:DSP_WRITE dspId:DSP_2 value:peq2F[currentChannel]];
            [viewController sendData:CMD_CH3_EQ3_DB commandType:DSP_WRITE dspId:DSP_2 value:peq3G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH3_EQ3_FREQ commandType:DSP_WRITE dspId:DSP_2 value:peq3F[currentChannel]];
            [viewController sendData:CMD_CH3_EQ4_DB commandType:DSP_WRITE dspId:DSP_2 value:peq4G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH3_EQ4_FREQ commandType:DSP_WRITE dspId:DSP_2 value:peq4F[currentChannel]];
            break;
        case CHANNEL_CH_8:
            [viewController sendData:CMD_CH4_EQ1_DB commandType:DSP_WRITE dspId:DSP_2 value:peq1G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH4_EQ1_FREQ commandType:DSP_WRITE dspId:DSP_2 value:peq1F[currentChannel]];
            [viewController sendData:CMD_CH4_EQ2_DB commandType:DSP_WRITE dspId:DSP_2 value:peq2G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH4_EQ2_FREQ commandType:DSP_WRITE dspId:DSP_2 value:peq2F[currentChannel]];
            [viewController sendData:CMD_CH4_EQ3_DB commandType:DSP_WRITE dspId:DSP_2 value:peq3G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH4_EQ3_FREQ commandType:DSP_WRITE dspId:DSP_2 value:peq3F[currentChannel]];
            [viewController sendData:CMD_CH4_EQ4_DB commandType:DSP_WRITE dspId:DSP_2 value:peq4G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH4_EQ4_FREQ commandType:DSP_WRITE dspId:DSP_2 value:peq4F[currentChannel]];
            break;
        case CHANNEL_CH_9:
            [viewController sendData:CMD_CH1_EQ1_DB commandType:DSP_WRITE dspId:DSP_3 value:peq1G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH1_EQ1_FREQ commandType:DSP_WRITE dspId:DSP_3 value:peq1F[currentChannel]];
            [viewController sendData:CMD_CH1_EQ2_DB commandType:DSP_WRITE dspId:DSP_3 value:peq2G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH1_EQ2_FREQ commandType:DSP_WRITE dspId:DSP_3 value:peq2F[currentChannel]];
            [viewController sendData:CMD_CH1_EQ3_DB commandType:DSP_WRITE dspId:DSP_3 value:peq3G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH1_EQ3_FREQ commandType:DSP_WRITE dspId:DSP_3 value:peq3F[currentChannel]];
            [viewController sendData:CMD_CH1_EQ4_DB commandType:DSP_WRITE dspId:DSP_3 value:peq4G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH1_EQ4_FREQ commandType:DSP_WRITE dspId:DSP_3 value:peq4F[currentChannel]];
            break;
        case CHANNEL_CH_10:
            [viewController sendData:CMD_CH2_EQ1_DB commandType:DSP_WRITE dspId:DSP_3 value:peq1G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH2_EQ1_FREQ commandType:DSP_WRITE dspId:DSP_3 value:peq1F[currentChannel]];
            [viewController sendData:CMD_CH2_EQ2_DB commandType:DSP_WRITE dspId:DSP_3 value:peq2G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH2_EQ2_FREQ commandType:DSP_WRITE dspId:DSP_3 value:peq2F[currentChannel]];
            [viewController sendData:CMD_CH2_EQ3_DB commandType:DSP_WRITE dspId:DSP_3 value:peq3G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH2_EQ3_FREQ commandType:DSP_WRITE dspId:DSP_3 value:peq3F[currentChannel]];
            [viewController sendData:CMD_CH2_EQ4_DB commandType:DSP_WRITE dspId:DSP_3 value:peq4G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH2_EQ4_FREQ commandType:DSP_WRITE dspId:DSP_3 value:peq4F[currentChannel]];
            break;
        case CHANNEL_CH_11:
            [viewController sendData:CMD_CH3_EQ1_DB commandType:DSP_WRITE dspId:DSP_3 value:peq1G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH3_EQ1_FREQ commandType:DSP_WRITE dspId:DSP_3 value:peq1F[currentChannel]];
            [viewController sendData:CMD_CH3_EQ2_DB commandType:DSP_WRITE dspId:DSP_3 value:peq2G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH3_EQ2_FREQ commandType:DSP_WRITE dspId:DSP_3 value:peq2F[currentChannel]];
            [viewController sendData:CMD_CH3_EQ3_DB commandType:DSP_WRITE dspId:DSP_3 value:peq3G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH3_EQ3_FREQ commandType:DSP_WRITE dspId:DSP_3 value:peq3F[currentChannel]];
            [viewController sendData:CMD_CH3_EQ4_DB commandType:DSP_WRITE dspId:DSP_3 value:peq4G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH3_EQ4_FREQ commandType:DSP_WRITE dspId:DSP_3 value:peq4F[currentChannel]];
            break;
        case CHANNEL_CH_12:
            [viewController sendData:CMD_CH4_EQ1_DB commandType:DSP_WRITE dspId:DSP_3 value:peq1G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH4_EQ1_FREQ commandType:DSP_WRITE dspId:DSP_3 value:peq1F[currentChannel]];
            [viewController sendData:CMD_CH4_EQ2_DB commandType:DSP_WRITE dspId:DSP_3 value:peq2G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH4_EQ2_FREQ commandType:DSP_WRITE dspId:DSP_3 value:peq2F[currentChannel]];
            [viewController sendData:CMD_CH4_EQ3_DB commandType:DSP_WRITE dspId:DSP_3 value:peq3G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH4_EQ3_FREQ commandType:DSP_WRITE dspId:DSP_3 value:peq3F[currentChannel]];
            [viewController sendData:CMD_CH4_EQ4_DB commandType:DSP_WRITE dspId:DSP_3 value:peq4G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH4_EQ4_FREQ commandType:DSP_WRITE dspId:DSP_3 value:peq4F[currentChannel]];
            break;
        case CHANNEL_CH_13:
            [viewController sendData:CMD_CH1_EQ1_DB commandType:DSP_WRITE dspId:DSP_4 value:peq1G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH1_EQ1_FREQ commandType:DSP_WRITE dspId:DSP_4 value:peq1F[currentChannel]];
            [viewController sendData:CMD_CH1_EQ2_DB commandType:DSP_WRITE dspId:DSP_4 value:peq2G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH1_EQ2_FREQ commandType:DSP_WRITE dspId:DSP_4 value:peq2F[currentChannel]];
            [viewController sendData:CMD_CH1_EQ3_DB commandType:DSP_WRITE dspId:DSP_4 value:peq3G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH1_EQ3_FREQ commandType:DSP_WRITE dspId:DSP_4 value:peq3F[currentChannel]];
            [viewController sendData:CMD_CH1_EQ4_DB commandType:DSP_WRITE dspId:DSP_4 value:peq4G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH1_EQ4_FREQ commandType:DSP_WRITE dspId:DSP_4 value:peq4F[currentChannel]];
            break;
        case CHANNEL_CH_14:
            [viewController sendData:CMD_CH2_EQ1_DB commandType:DSP_WRITE dspId:DSP_4 value:peq1G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH2_EQ1_FREQ commandType:DSP_WRITE dspId:DSP_4 value:peq1F[currentChannel]];
            [viewController sendData:CMD_CH2_EQ2_DB commandType:DSP_WRITE dspId:DSP_4 value:peq2G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH2_EQ2_FREQ commandType:DSP_WRITE dspId:DSP_4 value:peq2F[currentChannel]];
            [viewController sendData:CMD_CH2_EQ3_DB commandType:DSP_WRITE dspId:DSP_4 value:peq3G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH2_EQ3_FREQ commandType:DSP_WRITE dspId:DSP_4 value:peq3F[currentChannel]];
            [viewController sendData:CMD_CH2_EQ4_DB commandType:DSP_WRITE dspId:DSP_4 value:peq4G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH2_EQ4_FREQ commandType:DSP_WRITE dspId:DSP_4 value:peq4F[currentChannel]];
            break;
        case CHANNEL_CH_15:
            [viewController sendData:CMD_CH3_EQ1_DB commandType:DSP_WRITE dspId:DSP_4 value:peq1G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH3_EQ1_FREQ commandType:DSP_WRITE dspId:DSP_4 value:peq1F[currentChannel]];
            [viewController sendData:CMD_CH3_EQ2_DB commandType:DSP_WRITE dspId:DSP_4 value:peq2G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH3_EQ2_FREQ commandType:DSP_WRITE dspId:DSP_4 value:peq2F[currentChannel]];
            [viewController sendData:CMD_CH3_EQ3_DB commandType:DSP_WRITE dspId:DSP_4 value:peq3G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH3_EQ3_FREQ commandType:DSP_WRITE dspId:DSP_4 value:peq3F[currentChannel]];
            [viewController sendData:CMD_CH3_EQ4_DB commandType:DSP_WRITE dspId:DSP_4 value:peq4G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH3_EQ4_FREQ commandType:DSP_WRITE dspId:DSP_4 value:peq4F[currentChannel]];
            break;
        case CHANNEL_CH_16:
            [viewController sendData:CMD_CH4_EQ1_DB commandType:DSP_WRITE dspId:DSP_4 value:peq1G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH4_EQ1_FREQ commandType:DSP_WRITE dspId:DSP_4 value:peq1F[currentChannel]];
            [viewController sendData:CMD_CH4_EQ2_DB commandType:DSP_WRITE dspId:DSP_4 value:peq2G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH4_EQ2_FREQ commandType:DSP_WRITE dspId:DSP_4 value:peq2F[currentChannel]];
            [viewController sendData:CMD_CH4_EQ3_DB commandType:DSP_WRITE dspId:DSP_4 value:peq3G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH4_EQ3_FREQ commandType:DSP_WRITE dspId:DSP_4 value:peq3F[currentChannel]];
            [viewController sendData:CMD_CH4_EQ4_DB commandType:DSP_WRITE dspId:DSP_4 value:peq4G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH4_EQ4_FREQ commandType:DSP_WRITE dspId:DSP_4 value:peq4F[currentChannel]];
            break;
        case CHANNEL_CH_17:
            [viewController sendData:CMD_CH17_EQ1_DB commandType:DSP_WRITE dspId:DSP_6 value:peq1G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH17_EQ1_FREQ commandType:DSP_WRITE dspId:DSP_6 value:peq1F[currentChannel]];
            [viewController sendData:CMD_CH17_EQ2_DB commandType:DSP_WRITE dspId:DSP_6 value:peq2G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH17_EQ2_FREQ commandType:DSP_WRITE dspId:DSP_6 value:peq2F[currentChannel]];
            [viewController sendData:CMD_CH17_EQ3_DB commandType:DSP_WRITE dspId:DSP_6 value:peq3G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH17_EQ3_FREQ commandType:DSP_WRITE dspId:DSP_6 value:peq3F[currentChannel]];
            [viewController sendData:CMD_CH17_EQ4_DB commandType:DSP_WRITE dspId:DSP_6 value:peq4G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH17_EQ4_FREQ commandType:DSP_WRITE dspId:DSP_6 value:peq4F[currentChannel]];
            break;
        case CHANNEL_CH_18:
            [viewController sendData:CMD_CH18_EQ1_DB commandType:DSP_WRITE dspId:DSP_6 value:peq1G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH18_EQ1_FREQ commandType:DSP_WRITE dspId:DSP_6 value:peq1F[currentChannel]];
            [viewController sendData:CMD_CH18_EQ2_DB commandType:DSP_WRITE dspId:DSP_6 value:peq2G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH18_EQ2_FREQ commandType:DSP_WRITE dspId:DSP_6 value:peq2F[currentChannel]];
            [viewController sendData:CMD_CH18_EQ3_DB commandType:DSP_WRITE dspId:DSP_6 value:peq3G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH18_EQ3_FREQ commandType:DSP_WRITE dspId:DSP_6 value:peq3F[currentChannel]];
            [viewController sendData:CMD_CH18_EQ4_DB commandType:DSP_WRITE dspId:DSP_6 value:peq4G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH18_EQ4_FREQ commandType:DSP_WRITE dspId:DSP_6 value:peq4F[currentChannel]];
            break;
        case CHANNEL_MULTI_1:
            [viewController sendData:CMD_MULTI1_EQ1_DB commandType:DSP_WRITE dspId:DSP_7 value:peq1G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_MULTI1_EQ1_FREQ commandType:DSP_WRITE dspId:DSP_7 value:peq1F[currentChannel]];
            [viewController sendData:CMD_MULTI1_EQ2_DB commandType:DSP_WRITE dspId:DSP_7 value:peq2G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_MULTI1_EQ2_FREQ commandType:DSP_WRITE dspId:DSP_7 value:peq2F[currentChannel]];
            [viewController sendData:CMD_MULTI1_EQ3_DB commandType:DSP_WRITE dspId:DSP_7 value:peq3G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_MULTI1_EQ3_FREQ commandType:DSP_WRITE dspId:DSP_7 value:peq3F[currentChannel]];
            [viewController sendData:CMD_MULTI1_EQ4_DB commandType:DSP_WRITE dspId:DSP_7 value:peq4G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_MULTI1_EQ4_FREQ commandType:DSP_WRITE dspId:DSP_7 value:peq4F[currentChannel]];
            break;
        case CHANNEL_MULTI_2:
            [viewController sendData:CMD_MULTI2_EQ1_DB commandType:DSP_WRITE dspId:DSP_7 value:peq1G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_MULTI2_EQ1_FREQ commandType:DSP_WRITE dspId:DSP_7 value:peq1F[currentChannel]];
            [viewController sendData:CMD_MULTI2_EQ2_DB commandType:DSP_WRITE dspId:DSP_7 value:peq2G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_MULTI2_EQ2_FREQ commandType:DSP_WRITE dspId:DSP_7 value:peq2F[currentChannel]];
            [viewController sendData:CMD_MULTI2_EQ3_DB commandType:DSP_WRITE dspId:DSP_7 value:peq3G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_MULTI2_EQ3_FREQ commandType:DSP_WRITE dspId:DSP_7 value:peq3F[currentChannel]];
            [viewController sendData:CMD_MULTI2_EQ4_DB commandType:DSP_WRITE dspId:DSP_7 value:peq4G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_MULTI2_EQ4_FREQ commandType:DSP_WRITE dspId:DSP_7 value:peq4F[currentChannel]];
            break;
        case CHANNEL_MULTI_3:
            [viewController sendData:CMD_MULTI3_EQ1_DB commandType:DSP_WRITE dspId:DSP_7 value:peq1G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_MULTI3_EQ1_FREQ commandType:DSP_WRITE dspId:DSP_7 value:peq1F[currentChannel]];
            [viewController sendData:CMD_MULTI3_EQ2_DB commandType:DSP_WRITE dspId:DSP_7 value:peq2G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_MULTI3_EQ2_FREQ commandType:DSP_WRITE dspId:DSP_7 value:peq2F[currentChannel]];
            [viewController sendData:CMD_MULTI3_EQ3_DB commandType:DSP_WRITE dspId:DSP_7 value:peq3G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_MULTI3_EQ3_FREQ commandType:DSP_WRITE dspId:DSP_7 value:peq3F[currentChannel]];
            [viewController sendData:CMD_MULTI3_EQ4_DB commandType:DSP_WRITE dspId:DSP_7 value:peq4G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_MULTI3_EQ4_FREQ commandType:DSP_WRITE dspId:DSP_7 value:peq4F[currentChannel]];
            break;
        case CHANNEL_MULTI_4:
            [viewController sendData:CMD_MULTI4_EQ1_DB commandType:DSP_WRITE dspId:DSP_7 value:peq1G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_MULTI4_EQ1_FREQ commandType:DSP_WRITE dspId:DSP_7 value:peq1F[currentChannel]];
            [viewController sendData:CMD_MULTI4_EQ2_DB commandType:DSP_WRITE dspId:DSP_7 value:peq2G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_MULTI4_EQ2_FREQ commandType:DSP_WRITE dspId:DSP_7 value:peq2F[currentChannel]];
            [viewController sendData:CMD_MULTI4_EQ3_DB commandType:DSP_WRITE dspId:DSP_7 value:peq3G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_MULTI4_EQ3_FREQ commandType:DSP_WRITE dspId:DSP_7 value:peq3F[currentChannel]];
            [viewController sendData:CMD_MULTI4_EQ4_DB commandType:DSP_WRITE dspId:DSP_7 value:peq4G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_MULTI4_EQ4_FREQ commandType:DSP_WRITE dspId:DSP_7 value:peq4F[currentChannel]];
            break;
        case CHANNEL_MAIN:
            [viewController sendData:CMD_MAIN_LR_EQ1_DB commandType:DSP_WRITE dspId:DSP_6 value:peq1G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_MAIN_LR_EQ1_FREQ commandType:DSP_WRITE dspId:DSP_6 value:peq1F[currentChannel]];
            [viewController sendData:CMD_MAIN_LR_EQ2_DB commandType:DSP_WRITE dspId:DSP_6 value:peq2G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_MAIN_LR_EQ2_FREQ commandType:DSP_WRITE dspId:DSP_6 value:peq2F[currentChannel]];
            [viewController sendData:CMD_MAIN_LR_EQ3_DB commandType:DSP_WRITE dspId:DSP_6 value:peq3G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_MAIN_LR_EQ3_FREQ commandType:DSP_WRITE dspId:DSP_6 value:peq3F[currentChannel]];
            [viewController sendData:CMD_MAIN_LR_EQ4_DB commandType:DSP_WRITE dspId:DSP_6 value:peq4G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_MAIN_LR_EQ4_FREQ commandType:DSP_WRITE dspId:DSP_6 value:peq4F[currentChannel]];
            break;
        default:
            break;
    }
}

- (void)resetPeqDataFlatten
{
    peq1G[currentChannel] = peq2G[currentChannel] = peq3G[currentChannel] = peq4G[currentChannel] = PEQ_DEFAULT_G;
    
    peqData1 = [NSMutableArray arrayWithArray:[Global eqPeakData:[Global getEqFreq:peq1F[currentChannel]] eqDb:[Global getEqGain:peq1G[currentChannel]] eqQ:[Global getEqQ:peq1Q[currentChannel]]]];
    peqData2 = [NSMutableArray arrayWithArray:[Global eqPeakData:[Global getEqFreq:peq2F[currentChannel]] eqDb:[Global getEqGain:peq2G[currentChannel]] eqQ:[Global getEqQ:peq2Q[currentChannel]]]];
    peqData3 = [NSMutableArray arrayWithArray:[Global eqPeakData:[Global getEqFreq:peq3F[currentChannel]] eqDb:[Global getEqGain:peq3G[currentChannel]] eqQ:[Global getEqQ:peq3Q[currentChannel]]]];
    peqData4 = [NSMutableArray arrayWithArray:[Global eqPeakData:[Global getEqFreq:peq4F[currentChannel]] eqDb:[Global getEqGain:peq4G[currentChannel]] eqQ:[Global getEqQ:peq4Q[currentChannel]]]];
    [self updatePeqFrame];
}

- (void)calculatePeqData
{
    int count = [Global getPeqFrequencyCount];
    
    [peqData removeAllObjects];
    
    for (int index=0; index<count; index++) {
        NSNumber *gain1 = (NSNumber *)[peqData1 objectAtIndex:index];
        NSNumber *gain2 = (NSNumber *)[peqData2 objectAtIndex:index];
        NSNumber *gain3 = (NSNumber *)[peqData3 objectAtIndex:index];
        NSNumber *gain4 = (NSNumber *)[peqData4 objectAtIndex:index];
        
        NSNumber *sumGain = [NSNumber numberWithDouble:gain1.doubleValue + gain2.doubleValue + gain3.doubleValue + gain4.doubleValue];
    
        [peqData addObject:sumGain];
    }

    // Refresh graph
    [self.hostView.hostedGraph reloadData];
}

- (void)calculatePeqForChannel:(int)channel
{
    int count = [Global getPeqFrequencyCount];
    
    [peqData removeAllObjects];
    
    for (int index=0; index<count; index++) {
        NSNumber *gain1 = (NSNumber *)[peqData1 objectAtIndex:index];
        NSNumber *gain2 = (NSNumber *)[peqData2 objectAtIndex:index];
        NSNumber *gain3 = (NSNumber *)[peqData3 objectAtIndex:index];
        NSNumber *gain4 = (NSNumber *)[peqData4 objectAtIndex:index];
        
        NSNumber *sumGain = [NSNumber numberWithDouble:gain1.doubleValue + gain2.doubleValue + gain3.doubleValue + gain4.doubleValue];
        
        [peqData addObject:sumGain];
    }
    
    // Refresh graph
    [self.hostView.hostedGraph reloadData];
    
    // Refresh preview image
    if (initDone) {
        [self updatePreviewImageForChannel:channel];
    }
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        currentChannel = CHANNEL_CH_1;
        peqData = [[NSMutableArray alloc] init];
        peqData1 = [[NSMutableArray alloc] init];
        peqData2 = [[NSMutableArray alloc] init];
        peqData3 = [[NSMutableArray alloc] init];
        peqData4 = [[NSMutableArray alloc] init];
        peqImages = [[NSMutableArray alloc] initWithCapacity:CHANNEL_MAX];
        eqPreviewImages = [[NSMutableArray alloc] initWithCapacity:CHANNEL_MAX];
        
        // Create blank image
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(80, 80), NO, 0.0);
        UIImage *blankImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        for (int i=0; i<CHANNEL_MAX; i++) {
            peq1Filter[i] = EQ_PEAK;
            peq4Filter[i] = EQ_PEAK;
            peq1G[i] = peq2G[i] = peq3G[i] = peq4G[i] = PEQ_DEFAULT_G;
            peq1F[i] = PEQ_1_DEFAULT_F; peq2F[i] = PEQ_2_DEFAULT_F;
            peq3F[i] = PEQ_3_DEFAULT_F; peq4F[i] = PEQ_4_DEFAULT_F;
            peq1Q[i] = peq2Q[i] = peq3Q[i] = peq4Q[i] = PEQ_DEFAULT_Q;
            lastPeq1Q[i] = lastPeq4Q[i] = PEQ_DEFAULT_Q;
            peq1Enabled[i] = peq2Enabled[i] = peq3Enabled[i] = peq4Enabled[i] = NO;
            [peqImages addObject:[UIImage imageNamed:@"pan-2.png"]];
            [eqPreviewImages addObject:blankImage];
            peqEnabled[i] = NO;
        }
        
        // Init data points
        peqData1 = [NSMutableArray arrayWithArray:[Global eqPeakData:[Global getEqFreq:peq1F[currentChannel]]
                                                                eqDb:[Global getEqGain:peq1G[currentChannel]] eqQ:[Global getEqQ:peq1Q[currentChannel]]]];
        peqData2 = [NSMutableArray arrayWithArray:[Global eqPeakData:[Global getEqFreq:peq2F[currentChannel]]
                                                                eqDb:[Global getEqGain:peq2G[currentChannel]] eqQ:[Global getEqQ:peq2Q[currentChannel]]]];
        peqData3 = [NSMutableArray arrayWithArray:[Global eqPeakData:[Global getEqFreq:peq3F[currentChannel]]
                                                                eqDb:[Global getEqGain:peq3G[currentChannel]] eqQ:[Global getEqQ:peq3Q[currentChannel]]]];
        peqData4 = [NSMutableArray arrayWithArray:[Global eqPeakData:[Global getEqFreq:peq4F[currentChannel]]
                                                                eqDb:[Global getEqGain:peq4G[currentChannel]] eqQ:[Global getEqQ:peq4Q[currentChannel]]]];
        [self calculatePeqData];
    }
    
    mainEqLock = NO;
    initDone = YES;
    currentBall = 0;
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    viewController = (ViewController *)appDelegate.window.rootViewController;
    
    // Do any additional setup after loading the view from its nib.    
    txtPage.delegate = self;
    
    // Init variables
    lblPeq1ValueG.text = [Global getEqGainString:peq1G[currentChannel]];
    lblPeq2ValueG.text = [Global getEqGainString:peq2G[currentChannel]];
    lblPeq3ValueG.text = [Global getEqGainString:peq3G[currentChannel]];
    lblPeq4ValueG.text = [Global getEqGainString:peq4G[currentChannel]];
    lblPeq1ValueF.text = [Global getEqFreqString:peq1F[currentChannel]];
    lblPeq2ValueF.text = [Global getEqFreqString:peq2F[currentChannel]];
    lblPeq3ValueF.text = [Global getEqFreqString:peq3F[currentChannel]];
    lblPeq4ValueF.text = [Global getEqFreqString:peq4F[currentChannel]];
    lblPeq1ValueQ.text = [Global getEqQ1String:peq1Q[currentChannel]];
    lblPeq2ValueQ.text = [Global getEqQ23String:peq2Q[currentChannel]];
    lblPeq3ValueQ.text = [Global getEqQ23String:peq3Q[currentChannel]];
    lblPeq4ValueQ.text = [Global getEqQ4String:peq4Q[currentChannel]];
    
    // Customize buttons
    [btnMeterPrePost.titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
    [btnMeterPrePost.titleLabel setNumberOfLines:0];
    [btnMeterPrePost.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [btnMeterPrePost setTitle:@"METER INPUT" forState:UIControlStateNormal];
    [btnMeterPrePost setBackgroundImage:[UIImage imageNamed:@"button-3.png"] forState:UIControlStateNormal];
    [btnMeterPrePost setTitle:@"METER POST" forState:UIControlStateSelected];
    [btnMeterPrePost setBackgroundImage:[UIImage imageNamed:@"button-5.png"] forState:UIControlStateSelected];
    
    // Add main slider and meters
    UIImage *faderMainImage = [UIImage imageNamed:@"fader-3.png"];
    UIImage *progressImage = [[UIImage imageNamed:@"meter-1.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0,0,0,0)];
    UIImage *progressImage2 = [[UIImage imageNamed:@"meter-23.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0,0,0,0)];
    
    slider = [[UISlider alloc] initWithFrame:CGRectMake(843, 295, 301, 180)];
    slider.transform = CGAffineTransformRotate(slider.transform, 270.0/180*M_PI);
    [slider addTarget:self action:@selector(onSliderBegin:) forControlEvents:UIControlEventTouchDown];
    [slider addTarget:self action:@selector(onSliderEnd:) forControlEvents:UIControlEventTouchUpInside];
    [slider addTarget:self action:@selector(onSliderEnd:) forControlEvents:UIControlEventTouchUpOutside];
    [slider addTarget:self action:@selector(onSliderAction:) forControlEvents:UIControlEventValueChanged];
    [slider setBackgroundColor:[UIColor clearColor]];
    [slider setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [slider setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [slider setThumbImage:faderMainImage forState:UIControlStateNormal];
    slider.minimumValue = 0.0;
    slider.maximumValue = FADER_MAX_VALUE;
    slider.continuous = YES;
    slider.value = 0.0;
    [self.view addSubview:slider];
        
    meterL = [[JEProgressView alloc] initWithFrame:CGRectMake(797, 372, 275, 4)];
    meterL.transform = CGAffineTransformRotate(meterL.transform, 270.0/180*M_PI);
    [meterL setProgressImage:progressImage];
    [meterL setTrackImage:[UIImage alloc]];
    [meterL setProgress:0.0];
    [self.view addSubview:meterL];
    
    meterR = [[JEProgressView alloc] initWithFrame:CGRectMake(807, 372, 275, 4)];
    meterR.transform = CGAffineTransformRotate(meterR.transform, 270.0/180*M_PI);
    [meterR setProgressImage:progressImage];
    [meterR setTrackImage:[UIImage alloc]];
    [meterR setProgress:0.0];
    [self.view addSubview:meterR];
    
    meterIn = [[JEProgressView alloc] initWithFrame:CGRectMake(652, 220, 226, 12)];
    meterIn.transform = CGAffineTransformRotate(meterIn.transform, 270.0/180*M_PI);
    [meterIn setProgressImage:progressImage2];
    [meterIn setTrackImage:[UIImage alloc]];
    [meterIn setProgress:0.0];
    [self.view addSubview:meterIn];
    
    meterOut = [[JEProgressView alloc] initWithFrame:CGRectMake(712, 220, 226, 12)];
    meterOut.transform = CGAffineTransformRotate(meterOut.transform, 270.0/180*M_PI);
    [meterOut setProgressImage:progressImage2];
    [meterOut setTrackImage:[UIImage alloc]];
    [meterOut setProgress:0.0];
    [self.view addSubview:meterOut];
    
    meterInL = [[JEProgressView alloc] initWithFrame:CGRectMake(595, 220, 226, 12)];
    meterInL.transform = CGAffineTransformRotate(meterInL.transform, 270.0/180*M_PI);
    [meterInL setProgressImage:progressImage2];
    [meterInL setTrackImage:[UIImage alloc]];
    [meterInL setProgress:0.0];
    [self.view addSubview:meterInL];
    
    meterInR = [[JEProgressView alloc] initWithFrame:CGRectMake(655, 220, 226, 12)];
    meterInR.transform = CGAffineTransformRotate(meterInR.transform, 270.0/180*M_PI);
    [meterInR setProgressImage:progressImage2];
    [meterInR setTrackImage:[UIImage alloc]];
    [meterInR setProgress:0.0];
    [self.view addSubview:meterInR];
    
    meterOutL = [[JEProgressView alloc] initWithFrame:CGRectMake(690, 220, 226, 12)];
    meterOutL.transform = CGAffineTransformRotate(meterOutL.transform, 270.0/180*M_PI);
    [meterOutL setProgressImage:progressImage2];
    [meterOutL setTrackImage:[UIImage alloc]];
    [meterOutL setProgress:0.0];
    [self.view addSubview:meterOutL];
    
    meterOutR = [[JEProgressView alloc] initWithFrame:CGRectMake(750, 220, 226, 12)];
    meterOutR.transform = CGAffineTransformRotate(meterOutR.transform, 270.0/180*M_PI);
    [meterOutR setProgressImage:progressImage2];
    [meterOutR setTrackImage:[UIImage alloc]];
    [meterOutR setProgress:0.0];
    [self.view addSubview:meterOutR];
    
    // Add gesture recognizer for PEQ Q value
    UIPinchGestureRecognizer *pinchGesture = [[UIPinchGestureRecognizer alloc] initWithTarget:self action:@selector(handlePinch:)];
    [self.viewPeqFrame addGestureRecognizer:pinchGesture];
    
    // Add GEQ sliders
    UIImage *sliderImage = [UIImage imageNamed:@"fader-9.png"];
    
    sliderGeq1 = [[UISlider alloc] initWithFrame:CGRectMake(-205, 221, 453, 12)];
    sliderGeq1.transform = CGAffineTransformRotate(sliderGeq1.transform, 270.0/180*M_PI);
    [sliderGeq1 addTarget:self action:@selector(onSlider1Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderGeq1 addTarget:self action:@selector(onSlider1End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderGeq1 addTarget:self action:@selector(onSlider1End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderGeq1 addTarget:self action:@selector(onSlider1Action:) forControlEvents:UIControlEventValueChanged];
    [sliderGeq1 setBackgroundColor:[UIColor clearColor]];
    [sliderGeq1 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGeq1 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGeq1 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderGeq1.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderGeq1.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderGeq1.continuous = YES;
    sliderGeq1.value = 0.0;
    [viewGeqFrame addSubview:sliderGeq1];
    
    sliderGeq2 = [[UISlider alloc] initWithFrame:CGRectMake(-180, 221, 453, 12)];
    sliderGeq2.transform = CGAffineTransformRotate(sliderGeq2.transform, 270.0/180*M_PI);
    [sliderGeq2 addTarget:self action:@selector(onSlider2Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderGeq2 addTarget:self action:@selector(onSlider2End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderGeq2 addTarget:self action:@selector(onSlider2End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderGeq2 addTarget:self action:@selector(onSlider2Action:) forControlEvents:UIControlEventValueChanged];
    [sliderGeq2 setBackgroundColor:[UIColor clearColor]];
    [sliderGeq2 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGeq2 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGeq2 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderGeq2.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderGeq2.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderGeq2.continuous = YES;
    sliderGeq2.value = 0.0;
    [viewGeqFrame addSubview:sliderGeq2];
    
    sliderGeq3 = [[UISlider alloc] initWithFrame:CGRectMake(-155, 221, 453, 12)];
    sliderGeq3.transform = CGAffineTransformRotate(sliderGeq3.transform, 270.0/180*M_PI);
    [sliderGeq3 addTarget:self action:@selector(onSlider3Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderGeq3 addTarget:self action:@selector(onSlider3End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderGeq3 addTarget:self action:@selector(onSlider3End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderGeq3 addTarget:self action:@selector(onSlider3Action:) forControlEvents:UIControlEventValueChanged];
    [sliderGeq3 setBackgroundColor:[UIColor clearColor]];
    [sliderGeq3 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGeq3 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGeq3 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderGeq3.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderGeq3.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderGeq3.continuous = YES;
    sliderGeq3.value = 0.0;
    [viewGeqFrame addSubview:sliderGeq3];
    
    sliderGeq4 = [[UISlider alloc] initWithFrame:CGRectMake(-130, 221, 453, 12)];
    sliderGeq4.transform = CGAffineTransformRotate(sliderGeq4.transform, 270.0/180*M_PI);
    [sliderGeq4 addTarget:self action:@selector(onSlider4Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderGeq4 addTarget:self action:@selector(onSlider4End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderGeq4 addTarget:self action:@selector(onSlider4End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderGeq4 addTarget:self action:@selector(onSlider4Action:) forControlEvents:UIControlEventValueChanged];
    [sliderGeq4 setBackgroundColor:[UIColor clearColor]];
    [sliderGeq4 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGeq4 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGeq4 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderGeq4.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderGeq4.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderGeq4.continuous = YES;
    sliderGeq4.value = 0.0;
    [viewGeqFrame addSubview:sliderGeq4];
    
    sliderGeq5 = [[UISlider alloc] initWithFrame:CGRectMake(-105, 221, 453, 12)];
    sliderGeq5.transform = CGAffineTransformRotate(sliderGeq5.transform, 270.0/180*M_PI);
    [sliderGeq5 addTarget:self action:@selector(onSlider5Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderGeq5 addTarget:self action:@selector(onSlider5End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderGeq5 addTarget:self action:@selector(onSlider5End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderGeq5 addTarget:self action:@selector(onSlider5Action:) forControlEvents:UIControlEventValueChanged];
    [sliderGeq5 setBackgroundColor:[UIColor clearColor]];
    [sliderGeq5 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGeq5 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGeq5 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderGeq5.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderGeq5.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderGeq5.continuous = YES;
    sliderGeq5.value = 0.0;
    [viewGeqFrame addSubview:sliderGeq5];
    
    sliderGeq6 = [[UISlider alloc] initWithFrame:CGRectMake(-80, 221, 453, 12)];
    sliderGeq6.transform = CGAffineTransformRotate(sliderGeq6.transform, 270.0/180*M_PI);
    [sliderGeq6 addTarget:self action:@selector(onSlider6Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderGeq6 addTarget:self action:@selector(onSlider6End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderGeq6 addTarget:self action:@selector(onSlider6End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderGeq6 addTarget:self action:@selector(onSlider6Action:) forControlEvents:UIControlEventValueChanged];
    [sliderGeq6 setBackgroundColor:[UIColor clearColor]];
    [sliderGeq6 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGeq6 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGeq6 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderGeq6.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderGeq6.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderGeq6.continuous = YES;
    sliderGeq6.value = 0.0;
    [viewGeqFrame addSubview:sliderGeq6];
    
    sliderGeq7 = [[UISlider alloc] initWithFrame:CGRectMake(-55, 221, 453, 12)];
    sliderGeq7.transform = CGAffineTransformRotate(sliderGeq7.transform, 270.0/180*M_PI);
    [sliderGeq7 addTarget:self action:@selector(onSlider7Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderGeq7 addTarget:self action:@selector(onSlider7End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderGeq7 addTarget:self action:@selector(onSlider7End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderGeq7 addTarget:self action:@selector(onSlider7Action:) forControlEvents:UIControlEventValueChanged];
    [sliderGeq7 setBackgroundColor:[UIColor clearColor]];
    [sliderGeq7 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGeq7 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGeq7 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderGeq7.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderGeq7.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderGeq7.continuous = YES;
    sliderGeq7.value = 0.0;
    [viewGeqFrame addSubview:sliderGeq7];
    
    sliderGeq8 = [[UISlider alloc] initWithFrame:CGRectMake(-30, 221, 453, 12)];
    sliderGeq8.transform = CGAffineTransformRotate(sliderGeq8.transform, 270.0/180*M_PI);
    [sliderGeq8 addTarget:self action:@selector(onSlider8Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderGeq8 addTarget:self action:@selector(onSlider8End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderGeq8 addTarget:self action:@selector(onSlider8End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderGeq8 addTarget:self action:@selector(onSlider8Action:) forControlEvents:UIControlEventValueChanged];
    [sliderGeq8 setBackgroundColor:[UIColor clearColor]];
    [sliderGeq8 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGeq8 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGeq8 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderGeq8.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderGeq8.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderGeq8.continuous = YES;
    sliderGeq8.value = 0.0;
    [viewGeqFrame addSubview:sliderGeq8];
    
    sliderGeq9 = [[UISlider alloc] initWithFrame:CGRectMake(-5, 221, 453, 12)];
    sliderGeq9.transform = CGAffineTransformRotate(sliderGeq9.transform, 270.0/180*M_PI);
    [sliderGeq9 addTarget:self action:@selector(onSlider9Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderGeq9 addTarget:self action:@selector(onSlider9End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderGeq9 addTarget:self action:@selector(onSlider9End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderGeq9 addTarget:self action:@selector(onSlider9Action:) forControlEvents:UIControlEventValueChanged];
    [sliderGeq9 setBackgroundColor:[UIColor clearColor]];
    [sliderGeq9 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGeq9 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGeq9 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderGeq9.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderGeq9.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderGeq9.continuous = YES;
    sliderGeq9.value = 0.0;
    [viewGeqFrame addSubview:sliderGeq9];
    
    sliderGeq10 = [[UISlider alloc] initWithFrame:CGRectMake(20, 221, 453, 12)];
    sliderGeq10.transform = CGAffineTransformRotate(sliderGeq10.transform, 270.0/180*M_PI);
    [sliderGeq10 addTarget:self action:@selector(onSlider10Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderGeq10 addTarget:self action:@selector(onSlider10End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderGeq10 addTarget:self action:@selector(onSlider10End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderGeq10 addTarget:self action:@selector(onSlider10Action:) forControlEvents:UIControlEventValueChanged];
    [sliderGeq10 setBackgroundColor:[UIColor clearColor]];
    [sliderGeq10 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGeq10 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGeq10 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderGeq10.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderGeq10.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderGeq10.continuous = YES;
    sliderGeq10.value = 0.0;
    [viewGeqFrame addSubview:sliderGeq10];
    
    sliderGeq11 = [[UISlider alloc] initWithFrame:CGRectMake(45, 221, 453, 12)];
    sliderGeq11.transform = CGAffineTransformRotate(sliderGeq11.transform, 270.0/180*M_PI);
    [sliderGeq11 addTarget:self action:@selector(onSlider11Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderGeq11 addTarget:self action:@selector(onSlider11End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderGeq11 addTarget:self action:@selector(onSlider11End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderGeq11 addTarget:self action:@selector(onSlider11Action:) forControlEvents:UIControlEventValueChanged];
    [sliderGeq11 setBackgroundColor:[UIColor clearColor]];
    [sliderGeq11 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGeq11 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGeq11 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderGeq11.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderGeq11.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderGeq11.continuous = YES;
    sliderGeq11.value = 0.0;
    [viewGeqFrame addSubview:sliderGeq11];
    
    sliderGeq12 = [[UISlider alloc] initWithFrame:CGRectMake(70, 221, 453, 12)];
    sliderGeq12.transform = CGAffineTransformRotate(sliderGeq12.transform, 270.0/180*M_PI);
    [sliderGeq12 addTarget:self action:@selector(onSlider12Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderGeq12 addTarget:self action:@selector(onSlider12End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderGeq12 addTarget:self action:@selector(onSlider12End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderGeq12 addTarget:self action:@selector(onSlider12Action:) forControlEvents:UIControlEventValueChanged];
    [sliderGeq12 setBackgroundColor:[UIColor clearColor]];
    [sliderGeq12 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGeq12 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGeq12 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderGeq12.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderGeq12.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderGeq12.continuous = YES;
    sliderGeq12.value = 0.0;
    [viewGeqFrame addSubview:sliderGeq12];
    
    sliderGeq13 = [[UISlider alloc] initWithFrame:CGRectMake(95, 221, 453, 12)];
    sliderGeq13.transform = CGAffineTransformRotate(sliderGeq13.transform, 270.0/180*M_PI);
    [sliderGeq13 addTarget:self action:@selector(onSlider13Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderGeq13 addTarget:self action:@selector(onSlider13End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderGeq13 addTarget:self action:@selector(onSlider13End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderGeq13 addTarget:self action:@selector(onSlider13Action:) forControlEvents:UIControlEventValueChanged];
    [sliderGeq13 setBackgroundColor:[UIColor clearColor]];
    [sliderGeq13 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGeq13 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGeq13 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderGeq13.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderGeq13.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderGeq13.continuous = YES;
    sliderGeq13.value = 0.0;
    [viewGeqFrame addSubview:sliderGeq13];
    
    sliderGeq14 = [[UISlider alloc] initWithFrame:CGRectMake(120, 221, 453, 12)];
    sliderGeq14.transform = CGAffineTransformRotate(sliderGeq14.transform, 270.0/180*M_PI);
    [sliderGeq14 addTarget:self action:@selector(onSlider14Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderGeq14 addTarget:self action:@selector(onSlider14End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderGeq14 addTarget:self action:@selector(onSlider14End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderGeq14 addTarget:self action:@selector(onSlider14Action:) forControlEvents:UIControlEventValueChanged];
    [sliderGeq14 setBackgroundColor:[UIColor clearColor]];
    [sliderGeq14 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGeq14 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGeq14 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderGeq14.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderGeq14.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderGeq14.continuous = YES;
    sliderGeq14.value = 0.0;
    [viewGeqFrame addSubview:sliderGeq14];
    
    sliderGeq15 = [[UISlider alloc] initWithFrame:CGRectMake(145, 221, 453, 12)];
    sliderGeq15.transform = CGAffineTransformRotate(sliderGeq15.transform, 270.0/180*M_PI);
    [sliderGeq15 addTarget:self action:@selector(onSlider15Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderGeq15 addTarget:self action:@selector(onSlider15End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderGeq15 addTarget:self action:@selector(onSlider15End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderGeq15 addTarget:self action:@selector(onSlider15Action:) forControlEvents:UIControlEventValueChanged];
    [sliderGeq15 setBackgroundColor:[UIColor clearColor]];
    [sliderGeq15 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGeq15 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGeq15 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderGeq15.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderGeq15.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderGeq15.continuous = YES;
    sliderGeq15.value = 0.0;
    [viewGeqFrame addSubview:sliderGeq15];
    
    sliderGeq16 = [[UISlider alloc] initWithFrame:CGRectMake(170, 221, 453, 12)];
    sliderGeq16.transform = CGAffineTransformRotate(sliderGeq16.transform, 270.0/180*M_PI);
    [sliderGeq16 addTarget:self action:@selector(onSlider16Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderGeq16 addTarget:self action:@selector(onSlider16End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderGeq16 addTarget:self action:@selector(onSlider16End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderGeq16 addTarget:self action:@selector(onSlider16Action:) forControlEvents:UIControlEventValueChanged];
    [sliderGeq16 setBackgroundColor:[UIColor clearColor]];
    [sliderGeq16 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGeq16 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGeq16 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderGeq16.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderGeq16.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderGeq16.continuous = YES;
    sliderGeq16.value = 0.0;
    [viewGeqFrame addSubview:sliderGeq16];
    
    sliderGeq17 = [[UISlider alloc] initWithFrame:CGRectMake(195, 221, 453, 12)];
    sliderGeq17.transform = CGAffineTransformRotate(sliderGeq17.transform, 270.0/180*M_PI);
    [sliderGeq17 addTarget:self action:@selector(onSlider17Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderGeq17 addTarget:self action:@selector(onSlider17End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderGeq17 addTarget:self action:@selector(onSlider17End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderGeq17 addTarget:self action:@selector(onSlider17Action:) forControlEvents:UIControlEventValueChanged];
    [sliderGeq17 setBackgroundColor:[UIColor clearColor]];
    [sliderGeq17 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGeq17 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGeq17 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderGeq17.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderGeq17.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderGeq17.continuous = YES;
    sliderGeq17.value = 0.0;
    [viewGeqFrame addSubview:sliderGeq17];
    
    sliderGeq18 = [[UISlider alloc] initWithFrame:CGRectMake(220, 221, 453, 12)];
    sliderGeq18.transform = CGAffineTransformRotate(sliderGeq18.transform, 270.0/180*M_PI);
    [sliderGeq18 addTarget:self action:@selector(onSlider18Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderGeq18 addTarget:self action:@selector(onSlider18End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderGeq18 addTarget:self action:@selector(onSlider18End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderGeq18 addTarget:self action:@selector(onSlider18Action:) forControlEvents:UIControlEventValueChanged];
    [sliderGeq18 setBackgroundColor:[UIColor clearColor]];
    [sliderGeq18 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGeq18 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGeq18 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderGeq18.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderGeq18.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderGeq18.continuous = YES;
    sliderGeq18.value = 0.0;
    [viewGeqFrame addSubview:sliderGeq18];
    
    sliderGeq19 = [[UISlider alloc] initWithFrame:CGRectMake(245, 221, 453, 12)];
    sliderGeq19.transform = CGAffineTransformRotate(sliderGeq19.transform, 270.0/180*M_PI);
    [sliderGeq19 addTarget:self action:@selector(onSlider19Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderGeq19 addTarget:self action:@selector(onSlider19End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderGeq19 addTarget:self action:@selector(onSlider19End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderGeq19 addTarget:self action:@selector(onSlider19Action:) forControlEvents:UIControlEventValueChanged];
    [sliderGeq19 setBackgroundColor:[UIColor clearColor]];
    [sliderGeq19 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGeq19 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGeq19 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderGeq19.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderGeq19.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderGeq19.continuous = YES;
    sliderGeq19.value = 0.0;
    [viewGeqFrame addSubview:sliderGeq19];
    
    sliderGeq20 = [[UISlider alloc] initWithFrame:CGRectMake(270, 221, 453, 12)];
    sliderGeq20.transform = CGAffineTransformRotate(sliderGeq20.transform, 270.0/180*M_PI);
    [sliderGeq20 addTarget:self action:@selector(onSlider20Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderGeq20 addTarget:self action:@selector(onSlider20End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderGeq20 addTarget:self action:@selector(onSlider20End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderGeq20 addTarget:self action:@selector(onSlider20Action:) forControlEvents:UIControlEventValueChanged];
    [sliderGeq20 setBackgroundColor:[UIColor clearColor]];
    [sliderGeq20 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGeq20 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGeq20 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderGeq20.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderGeq20.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderGeq20.continuous = YES;
    sliderGeq20.value = 0.0;
    [viewGeqFrame addSubview:sliderGeq20];
    
    sliderGeq21 = [[UISlider alloc] initWithFrame:CGRectMake(295, 221, 453, 12)];
    sliderGeq21.transform = CGAffineTransformRotate(sliderGeq21.transform, 270.0/180*M_PI);
    [sliderGeq21 addTarget:self action:@selector(onSlider21Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderGeq21 addTarget:self action:@selector(onSlider21End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderGeq21 addTarget:self action:@selector(onSlider21End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderGeq21 addTarget:self action:@selector(onSlider21Action:) forControlEvents:UIControlEventValueChanged];
    [sliderGeq21 setBackgroundColor:[UIColor clearColor]];
    [sliderGeq21 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGeq21 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGeq21 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderGeq21.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderGeq21.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderGeq21.continuous = YES;
    sliderGeq21.value = 0.0;
    [viewGeqFrame addSubview:sliderGeq21];
    
    sliderGeq22 = [[UISlider alloc] initWithFrame:CGRectMake(320, 221, 453, 12)];
    sliderGeq22.transform = CGAffineTransformRotate(sliderGeq22.transform, 270.0/180*M_PI);
    [sliderGeq22 addTarget:self action:@selector(onSlider22Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderGeq22 addTarget:self action:@selector(onSlider22End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderGeq22 addTarget:self action:@selector(onSlider22End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderGeq22 addTarget:self action:@selector(onSlider22Action:) forControlEvents:UIControlEventValueChanged];
    [sliderGeq22 setBackgroundColor:[UIColor clearColor]];
    [sliderGeq22 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGeq22 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGeq22 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderGeq22.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderGeq22.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderGeq22.continuous = YES;
    sliderGeq22.value = 0.0;
    [viewGeqFrame addSubview:sliderGeq22];
    
    sliderGeq23 = [[UISlider alloc] initWithFrame:CGRectMake(345, 221, 453, 12)];
    sliderGeq23.transform = CGAffineTransformRotate(sliderGeq23.transform, 270.0/180*M_PI);
    [sliderGeq23 addTarget:self action:@selector(onSlider23Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderGeq23 addTarget:self action:@selector(onSlider23End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderGeq23 addTarget:self action:@selector(onSlider23End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderGeq23 addTarget:self action:@selector(onSlider23Action:) forControlEvents:UIControlEventValueChanged];
    [sliderGeq23 setBackgroundColor:[UIColor clearColor]];
    [sliderGeq23 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGeq23 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGeq23 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderGeq23.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderGeq23.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderGeq23.continuous = YES;
    sliderGeq23.value = 0.0;
    [viewGeqFrame addSubview:sliderGeq23];
    
    sliderGeq24 = [[UISlider alloc] initWithFrame:CGRectMake(370, 221, 453, 12)];
    sliderGeq24.transform = CGAffineTransformRotate(sliderGeq24.transform, 270.0/180*M_PI);
    [sliderGeq24 addTarget:self action:@selector(onSlider24Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderGeq24 addTarget:self action:@selector(onSlider24End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderGeq24 addTarget:self action:@selector(onSlider24End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderGeq24 addTarget:self action:@selector(onSlider24Action:) forControlEvents:UIControlEventValueChanged];
    [sliderGeq24 setBackgroundColor:[UIColor clearColor]];
    [sliderGeq24 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGeq24 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGeq24 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderGeq24.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderGeq24.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderGeq24.continuous = YES;
    sliderGeq24.value = 0.0;
    [viewGeqFrame addSubview:sliderGeq24];
    
    sliderGeq25 = [[UISlider alloc] initWithFrame:CGRectMake(395, 221, 453, 12)];
    sliderGeq25.transform = CGAffineTransformRotate(sliderGeq25.transform, 270.0/180*M_PI);
    [sliderGeq25 addTarget:self action:@selector(onSlider25Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderGeq25 addTarget:self action:@selector(onSlider25End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderGeq25 addTarget:self action:@selector(onSlider25End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderGeq25 addTarget:self action:@selector(onSlider25Action:) forControlEvents:UIControlEventValueChanged];
    [sliderGeq25 setBackgroundColor:[UIColor clearColor]];
    [sliderGeq25 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGeq25 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGeq25 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderGeq25.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderGeq25.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderGeq25.continuous = YES;
    sliderGeq25.value = 0.0;
    [viewGeqFrame addSubview:sliderGeq25];
    
    sliderGeq26 = [[UISlider alloc] initWithFrame:CGRectMake(420, 221, 453, 12)];
    sliderGeq26.transform = CGAffineTransformRotate(sliderGeq26.transform, 270.0/180*M_PI);
    [sliderGeq26 addTarget:self action:@selector(onSlider26Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderGeq26 addTarget:self action:@selector(onSlider26End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderGeq26 addTarget:self action:@selector(onSlider26End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderGeq26 addTarget:self action:@selector(onSlider26Action:) forControlEvents:UIControlEventValueChanged];
    [sliderGeq26 setBackgroundColor:[UIColor clearColor]];
    [sliderGeq26 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGeq26 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGeq26 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderGeq26.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderGeq26.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderGeq26.continuous = YES;
    sliderGeq26.value = 0.0;
    [viewGeqFrame addSubview:sliderGeq26];
    
    sliderGeq27 = [[UISlider alloc] initWithFrame:CGRectMake(445, 221, 453, 12)];
    sliderGeq27.transform = CGAffineTransformRotate(sliderGeq27.transform, 270.0/180*M_PI);
    [sliderGeq27 addTarget:self action:@selector(onSlider27Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderGeq27 addTarget:self action:@selector(onSlider27End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderGeq27 addTarget:self action:@selector(onSlider27End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderGeq27 addTarget:self action:@selector(onSlider27Action:) forControlEvents:UIControlEventValueChanged];
    [sliderGeq27 setBackgroundColor:[UIColor clearColor]];
    [sliderGeq27 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGeq27 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGeq27 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderGeq27.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderGeq27.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderGeq27.continuous = YES;
    sliderGeq27.value = 0.0;
    [viewGeqFrame addSubview:sliderGeq27];
    
    sliderGeq28 = [[UISlider alloc] initWithFrame:CGRectMake(470, 221, 453, 12)];
    sliderGeq28.transform = CGAffineTransformRotate(sliderGeq28.transform, 270.0/180*M_PI);
    [sliderGeq28 addTarget:self action:@selector(onSlider28Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderGeq28 addTarget:self action:@selector(onSlider28End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderGeq28 addTarget:self action:@selector(onSlider28End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderGeq28 addTarget:self action:@selector(onSlider28Action:) forControlEvents:UIControlEventValueChanged];
    [sliderGeq28 setBackgroundColor:[UIColor clearColor]];
    [sliderGeq28 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGeq28 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGeq28 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderGeq28.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderGeq28.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderGeq28.continuous = YES;
    sliderGeq28.value = 0.0;
    [viewGeqFrame addSubview:sliderGeq28];
    
    sliderGeq29 = [[UISlider alloc] initWithFrame:CGRectMake(495, 221, 453, 12)];
    sliderGeq29.transform = CGAffineTransformRotate(sliderGeq29.transform, 270.0/180*M_PI);
    [sliderGeq29 addTarget:self action:@selector(onSlider29Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderGeq29 addTarget:self action:@selector(onSlider29End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderGeq29 addTarget:self action:@selector(onSlider29End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderGeq29 addTarget:self action:@selector(onSlider29Action:) forControlEvents:UIControlEventValueChanged];
    [sliderGeq29 setBackgroundColor:[UIColor clearColor]];
    [sliderGeq29 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGeq29 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGeq29 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderGeq29.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderGeq29.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderGeq29.continuous = YES;
    sliderGeq29.value = 0.0;
    [viewGeqFrame addSubview:sliderGeq29];
    
    sliderGeq30 = [[UISlider alloc] initWithFrame:CGRectMake(520, 221, 453, 12)];
    sliderGeq30.transform = CGAffineTransformRotate(sliderGeq30.transform, 270.0/180*M_PI);
    [sliderGeq30 addTarget:self action:@selector(onSlider30Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderGeq30 addTarget:self action:@selector(onSlider30End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderGeq30 addTarget:self action:@selector(onSlider30End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderGeq30 addTarget:self action:@selector(onSlider30Action:) forControlEvents:UIControlEventValueChanged];
    [sliderGeq30 setBackgroundColor:[UIColor clearColor]];
    [sliderGeq30 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGeq30 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGeq30 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderGeq30.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderGeq30.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderGeq30.continuous = YES;
    sliderGeq30.value = 0.0;
    [viewGeqFrame addSubview:sliderGeq30];
    
    sliderGeq31 = [[UISlider alloc] initWithFrame:CGRectMake(545, 221, 453, 12)];
    sliderGeq31.transform = CGAffineTransformRotate(sliderGeq31.transform, 270.0/180*M_PI);
    [sliderGeq31 addTarget:self action:@selector(onSlider31Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderGeq31 addTarget:self action:@selector(onSlider31End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderGeq31 addTarget:self action:@selector(onSlider31End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderGeq31 addTarget:self action:@selector(onSlider31Action:) forControlEvents:UIControlEventValueChanged];
    [sliderGeq31 setBackgroundColor:[UIColor clearColor]];
    [sliderGeq31 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGeq31 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGeq31 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderGeq31.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderGeq31.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderGeq31.continuous = YES;
    sliderGeq31.value = 0.0;
    [viewGeqFrame addSubview:sliderGeq31];
    
    // Add slider meters
    UIImage *sliderProgressImage = [[UIImage imageNamed:@"bar-4.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0,0,0,0)];

    meter1Top = [[JEProgressView alloc] initWithFrame:CGRectMake(-91, 112, 226, 12)];
    meter1Top.transform = CGAffineTransformRotate(meter1Top.transform, 270.0/180*M_PI);
    [meter1Top setProgressImage:sliderProgressImage];
    [meter1Top setTrackImage:[UIImage alloc]];
    [meter1Top setProgress:0.0];
    [viewGeqFrame addSubview:meter1Top];
    [viewGeqFrame sendSubviewToBack:meter1Top];
    
    meter1Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(-91, 338, 226, 12)];
    meter1Btm.transform = CGAffineTransformRotate(meter1Btm.transform, 90.0/180*M_PI);
    [meter1Btm setProgressImage:sliderProgressImage];
    [meter1Btm setTrackImage:[UIImage alloc]];
    [meter1Btm setProgress:0.0];
    [viewGeqFrame addSubview:meter1Btm];
    [viewGeqFrame sendSubviewToBack:meter1Btm];
    
    meter2Top = [[JEProgressView alloc] initWithFrame:CGRectMake(-66, 112, 226, 12)];
    meter2Top.transform = CGAffineTransformRotate(meter2Top.transform, 270.0/180*M_PI);
    [meter2Top setProgressImage:sliderProgressImage];
    [meter2Top setTrackImage:[UIImage alloc]];
    [meter2Top setProgress:0.0];
    [viewGeqFrame addSubview:meter2Top];
    [viewGeqFrame sendSubviewToBack:meter2Top];
    
    meter2Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(-66, 338, 226, 12)];
    meter2Btm.transform = CGAffineTransformRotate(meter2Btm.transform, 90.0/180*M_PI);
    [meter2Btm setProgressImage:sliderProgressImage];
    [meter2Btm setTrackImage:[UIImage alloc]];
    [meter2Btm setProgress:0.0];
    [viewGeqFrame addSubview:meter2Btm];
    [viewGeqFrame sendSubviewToBack:meter2Btm];
    
    meter3Top = [[JEProgressView alloc] initWithFrame:CGRectMake(-41, 112, 226, 12)];
    meter3Top.transform = CGAffineTransformRotate(meter3Top.transform, 270.0/180*M_PI);
    [meter3Top setProgressImage:sliderProgressImage];
    [meter3Top setTrackImage:[UIImage alloc]];
    [meter3Top setProgress:0.0];
    [viewGeqFrame addSubview:meter3Top];
    [viewGeqFrame sendSubviewToBack:meter3Top];
    
    meter3Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(-41, 338, 226, 12)];
    meter3Btm.transform = CGAffineTransformRotate(meter3Btm.transform, 90.0/180*M_PI);
    [meter3Btm setProgressImage:sliderProgressImage];
    [meter3Btm setTrackImage:[UIImage alloc]];
    [meter3Btm setProgress:0.0];
    [viewGeqFrame addSubview:meter3Btm];
    [viewGeqFrame sendSubviewToBack:meter3Btm];
    
    meter4Top = [[JEProgressView alloc] initWithFrame:CGRectMake(-16, 112, 226, 12)];
    meter4Top.transform = CGAffineTransformRotate(meter4Top.transform, 270.0/180*M_PI);
    [meter4Top setProgressImage:sliderProgressImage];
    [meter4Top setTrackImage:[UIImage alloc]];
    [meter4Top setProgress:0.0];
    [viewGeqFrame addSubview:meter4Top];
    [viewGeqFrame sendSubviewToBack:meter4Top];
    
    meter4Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(-16, 338, 226, 12)];
    meter4Btm.transform = CGAffineTransformRotate(meter4Btm.transform, 90.0/180*M_PI);
    [meter4Btm setProgressImage:sliderProgressImage];
    [meter4Btm setTrackImage:[UIImage alloc]];
    [meter4Btm setProgress:0.0];
    [viewGeqFrame addSubview:meter4Btm];
    [viewGeqFrame sendSubviewToBack:meter4Btm];
    
    meter5Top = [[JEProgressView alloc] initWithFrame:CGRectMake(9, 112, 226, 12)];
    meter5Top.transform = CGAffineTransformRotate(meter5Top.transform, 270.0/180*M_PI);
    [meter5Top setProgressImage:sliderProgressImage];
    [meter5Top setTrackImage:[UIImage alloc]];
    [meter5Top setProgress:0.0];
    [viewGeqFrame addSubview:meter5Top];
    [viewGeqFrame sendSubviewToBack:meter5Top];
    
    meter5Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(9, 338, 226, 12)];
    meter5Btm.transform = CGAffineTransformRotate(meter5Btm.transform, 90.0/180*M_PI);
    [meter5Btm setProgressImage:sliderProgressImage];
    [meter5Btm setTrackImage:[UIImage alloc]];
    [meter5Btm setProgress:0.0];
    [viewGeqFrame addSubview:meter5Btm];
    [viewGeqFrame sendSubviewToBack:meter5Btm];
    
    meter6Top = [[JEProgressView alloc] initWithFrame:CGRectMake(34, 112, 226, 12)];
    meter6Top.transform = CGAffineTransformRotate(meter6Top.transform, 270.0/180*M_PI);
    [meter6Top setProgressImage:sliderProgressImage];
    [meter6Top setTrackImage:[UIImage alloc]];
    [meter6Top setProgress:0.0];
    [viewGeqFrame addSubview:meter6Top];
    [viewGeqFrame sendSubviewToBack:meter6Top];
    
    meter6Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(34, 338, 226, 12)];
    meter6Btm.transform = CGAffineTransformRotate(meter6Btm.transform, 90.0/180*M_PI);
    [meter6Btm setProgressImage:sliderProgressImage];
    [meter6Btm setTrackImage:[UIImage alloc]];
    [meter6Btm setProgress:0.0];
    [viewGeqFrame addSubview:meter6Btm];
    [viewGeqFrame sendSubviewToBack:meter6Btm];
    
    meter7Top = [[JEProgressView alloc] initWithFrame:CGRectMake(59, 112, 226, 12)];
    meter7Top.transform = CGAffineTransformRotate(meter7Top.transform, 270.0/180*M_PI);
    [meter7Top setProgressImage:sliderProgressImage];
    [meter7Top setTrackImage:[UIImage alloc]];
    [meter7Top setProgress:0.0];
    [viewGeqFrame addSubview:meter7Top];
    [viewGeqFrame sendSubviewToBack:meter7Top];
    
    meter7Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(59, 338, 226, 12)];
    meter7Btm.transform = CGAffineTransformRotate(meter7Btm.transform, 90.0/180*M_PI);
    [meter7Btm setProgressImage:sliderProgressImage];
    [meter7Btm setTrackImage:[UIImage alloc]];
    [meter7Btm setProgress:0.0];
    [viewGeqFrame addSubview:meter7Btm];
    [viewGeqFrame sendSubviewToBack:meter7Btm];
    
    meter8Top = [[JEProgressView alloc] initWithFrame:CGRectMake(84, 112, 226, 12)];
    meter8Top.transform = CGAffineTransformRotate(meter8Top.transform, 270.0/180*M_PI);
    [meter8Top setProgressImage:sliderProgressImage];
    [meter8Top setTrackImage:[UIImage alloc]];
    [meter8Top setProgress:0.0];
    [viewGeqFrame addSubview:meter8Top];
    [viewGeqFrame sendSubviewToBack:meter8Top];
    
    meter8Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(84, 338, 226, 12)];
    meter8Btm.transform = CGAffineTransformRotate(meter8Btm.transform, 90.0/180*M_PI);
    [meter8Btm setProgressImage:sliderProgressImage];
    [meter8Btm setTrackImage:[UIImage alloc]];
    [meter8Btm setProgress:0.0];
    [viewGeqFrame addSubview:meter8Btm];
    [viewGeqFrame sendSubviewToBack:meter8Btm];
    
    meter9Top = [[JEProgressView alloc] initWithFrame:CGRectMake(109, 112, 226, 12)];
    meter9Top.transform = CGAffineTransformRotate(meter9Top.transform, 270.0/180*M_PI);
    [meter9Top setProgressImage:sliderProgressImage];
    [meter9Top setTrackImage:[UIImage alloc]];
    [meter9Top setProgress:0.0];
    [viewGeqFrame addSubview:meter9Top];
    [viewGeqFrame sendSubviewToBack:meter9Top];
    
    meter9Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(109, 338, 226, 12)];
    meter9Btm.transform = CGAffineTransformRotate(meter9Btm.transform, 90.0/180*M_PI);
    [meter9Btm setProgressImage:sliderProgressImage];
    [meter9Btm setTrackImage:[UIImage alloc]];
    [meter9Btm setProgress:0.0];
    [viewGeqFrame addSubview:meter9Btm];
    [viewGeqFrame sendSubviewToBack:meter9Btm];
    
    meter10Top = [[JEProgressView alloc] initWithFrame:CGRectMake(134, 112, 226, 12)];
    meter10Top.transform = CGAffineTransformRotate(meter10Top.transform, 270.0/180*M_PI);
    [meter10Top setProgressImage:sliderProgressImage];
    [meter10Top setTrackImage:[UIImage alloc]];
    [meter10Top setProgress:0.0];
    [viewGeqFrame addSubview:meter10Top];
    [viewGeqFrame sendSubviewToBack:meter10Top];
    
    meter10Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(134, 338, 226, 12)];
    meter10Btm.transform = CGAffineTransformRotate(meter10Btm.transform, 90.0/180*M_PI);
    [meter10Btm setProgressImage:sliderProgressImage];
    [meter10Btm setTrackImage:[UIImage alloc]];
    [meter10Btm setProgress:0.0];
    [viewGeqFrame addSubview:meter10Btm];
    [viewGeqFrame sendSubviewToBack:meter10Btm];
    
    meter11Top = [[JEProgressView alloc] initWithFrame:CGRectMake(159, 112, 226, 12)];
    meter11Top.transform = CGAffineTransformRotate(meter11Top.transform, 270.0/180*M_PI);
    [meter11Top setProgressImage:sliderProgressImage];
    [meter11Top setTrackImage:[UIImage alloc]];
    [meter11Top setProgress:0.0];
    [viewGeqFrame addSubview:meter11Top];
    [viewGeqFrame sendSubviewToBack:meter11Top];
    
    meter11Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(159, 338, 226, 12)];
    meter11Btm.transform = CGAffineTransformRotate(meter11Btm.transform, 90.0/180*M_PI);
    [meter11Btm setProgressImage:sliderProgressImage];
    [meter11Btm setTrackImage:[UIImage alloc]];
    [meter11Btm setProgress:0.0];
    [viewGeqFrame addSubview:meter11Btm];
    [viewGeqFrame sendSubviewToBack:meter11Btm];
    
    meter12Top = [[JEProgressView alloc] initWithFrame:CGRectMake(184, 112, 226, 12)];
    meter12Top.transform = CGAffineTransformRotate(meter12Top.transform, 270.0/180*M_PI);
    [meter12Top setProgressImage:sliderProgressImage];
    [meter12Top setTrackImage:[UIImage alloc]];
    [meter12Top setProgress:0.0];
    [viewGeqFrame addSubview:meter12Top];
    [viewGeqFrame sendSubviewToBack:meter12Top];
    
    meter12Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(184, 338, 226, 12)];
    meter12Btm.transform = CGAffineTransformRotate(meter12Btm.transform, 90.0/180*M_PI);
    [meter12Btm setProgressImage:sliderProgressImage];
    [meter12Btm setTrackImage:[UIImage alloc]];
    [meter12Btm setProgress:0.0];
    [viewGeqFrame addSubview:meter12Btm];
    [viewGeqFrame sendSubviewToBack:meter12Btm];
    
    meter13Top = [[JEProgressView alloc] initWithFrame:CGRectMake(209, 112, 226, 12)];
    meter13Top.transform = CGAffineTransformRotate(meter13Top.transform, 270.0/180*M_PI);
    [meter13Top setProgressImage:sliderProgressImage];
    [meter13Top setTrackImage:[UIImage alloc]];
    [meter13Top setProgress:0.0];
    [viewGeqFrame addSubview:meter13Top];
    [viewGeqFrame sendSubviewToBack:meter13Top];
    
    meter13Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(209, 338, 226, 12)];
    meter13Btm.transform = CGAffineTransformRotate(meter13Btm.transform, 90.0/180*M_PI);
    [meter13Btm setProgressImage:sliderProgressImage];
    [meter13Btm setTrackImage:[UIImage alloc]];
    [meter13Btm setProgress:0.0];
    [viewGeqFrame addSubview:meter13Btm];
    [viewGeqFrame sendSubviewToBack:meter13Btm];
    
    meter14Top = [[JEProgressView alloc] initWithFrame:CGRectMake(234, 112, 226, 12)];
    meter14Top.transform = CGAffineTransformRotate(meter14Top.transform, 270.0/180*M_PI);
    [meter14Top setProgressImage:sliderProgressImage];
    [meter14Top setTrackImage:[UIImage alloc]];
    [meter14Top setProgress:0.0];
    [viewGeqFrame addSubview:meter14Top];
    [viewGeqFrame sendSubviewToBack:meter14Top];
    
    meter14Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(234, 338, 226, 12)];
    meter14Btm.transform = CGAffineTransformRotate(meter14Btm.transform, 90.0/180*M_PI);
    [meter14Btm setProgressImage:sliderProgressImage];
    [meter14Btm setTrackImage:[UIImage alloc]];
    [meter14Btm setProgress:0.0];
    [viewGeqFrame addSubview:meter14Btm];
    [viewGeqFrame sendSubviewToBack:meter14Btm];
    
    meter15Top = [[JEProgressView alloc] initWithFrame:CGRectMake(259, 112, 226, 12)];
    meter15Top.transform = CGAffineTransformRotate(meter15Top.transform, 270.0/180*M_PI);
    [meter15Top setProgressImage:sliderProgressImage];
    [meter15Top setTrackImage:[UIImage alloc]];
    [meter15Top setProgress:0.0];
    [viewGeqFrame addSubview:meter15Top];
    [viewGeqFrame sendSubviewToBack:meter15Top];
    
    meter15Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(259, 338, 226, 12)];
    meter15Btm.transform = CGAffineTransformRotate(meter15Btm.transform, 90.0/180*M_PI);
    [meter15Btm setProgressImage:sliderProgressImage];
    [meter15Btm setTrackImage:[UIImage alloc]];
    [meter15Btm setProgress:0.0];
    [viewGeqFrame addSubview:meter15Btm];
    [viewGeqFrame sendSubviewToBack:meter15Btm];
    
    meter16Top = [[JEProgressView alloc] initWithFrame:CGRectMake(284, 112, 226, 12)];
    meter16Top.transform = CGAffineTransformRotate(meter16Top.transform, 270.0/180*M_PI);
    [meter16Top setProgressImage:sliderProgressImage];
    [meter16Top setTrackImage:[UIImage alloc]];
    [meter16Top setProgress:0.0];
    [viewGeqFrame addSubview:meter16Top];
    [viewGeqFrame sendSubviewToBack:meter16Top];
    
    meter16Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(284, 338, 226, 12)];
    meter16Btm.transform = CGAffineTransformRotate(meter16Btm.transform, 90.0/180*M_PI);
    [meter16Btm setProgressImage:sliderProgressImage];
    [meter16Btm setTrackImage:[UIImage alloc]];
    [meter16Btm setProgress:0.0];
    [viewGeqFrame addSubview:meter16Btm];
    [viewGeqFrame sendSubviewToBack:meter16Btm];
    
    meter17Top = [[JEProgressView alloc] initWithFrame:CGRectMake(309, 112, 226, 12)];
    meter17Top.transform = CGAffineTransformRotate(meter17Top.transform, 270.0/180*M_PI);
    [meter17Top setProgressImage:sliderProgressImage];
    [meter17Top setTrackImage:[UIImage alloc]];
    [meter17Top setProgress:0.0];
    [viewGeqFrame addSubview:meter17Top];
    [viewGeqFrame sendSubviewToBack:meter17Top];
    
    meter17Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(309, 338, 226, 12)];
    meter17Btm.transform = CGAffineTransformRotate(meter17Btm.transform, 90.0/180*M_PI);
    [meter17Btm setProgressImage:sliderProgressImage];
    [meter17Btm setTrackImage:[UIImage alloc]];
    [meter17Btm setProgress:0.0];
    [viewGeqFrame addSubview:meter17Btm];
    [viewGeqFrame sendSubviewToBack:meter17Btm];
    
    meter18Top = [[JEProgressView alloc] initWithFrame:CGRectMake(334, 112, 226, 12)];
    meter18Top.transform = CGAffineTransformRotate(meter18Top.transform, 270.0/180*M_PI);
    [meter18Top setProgressImage:sliderProgressImage];
    [meter18Top setTrackImage:[UIImage alloc]];
    [meter18Top setProgress:0.0];
    [viewGeqFrame addSubview:meter18Top];
    [viewGeqFrame sendSubviewToBack:meter18Top];
    
    meter18Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(334, 338, 226, 12)];
    meter18Btm.transform = CGAffineTransformRotate(meter18Btm.transform, 90.0/180*M_PI);
    [meter18Btm setProgressImage:sliderProgressImage];
    [meter18Btm setTrackImage:[UIImage alloc]];
    [meter18Btm setProgress:0.0];
    [viewGeqFrame addSubview:meter18Btm];
    [viewGeqFrame sendSubviewToBack:meter18Btm];
    
    meter19Top = [[JEProgressView alloc] initWithFrame:CGRectMake(359, 112, 226, 12)];
    meter19Top.transform = CGAffineTransformRotate(meter19Top.transform, 270.0/180*M_PI);
    [meter19Top setProgressImage:sliderProgressImage];
    [meter19Top setTrackImage:[UIImage alloc]];
    [meter19Top setProgress:0.0];
    [viewGeqFrame addSubview:meter19Top];
    [viewGeqFrame sendSubviewToBack:meter19Top];
    
    meter19Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(359, 338, 226, 12)];
    meter19Btm.transform = CGAffineTransformRotate(meter19Btm.transform, 90.0/180*M_PI);
    [meter19Btm setProgressImage:sliderProgressImage];
    [meter19Btm setTrackImage:[UIImage alloc]];
    [meter19Btm setProgress:0.0];
    [viewGeqFrame addSubview:meter19Btm];
    [viewGeqFrame sendSubviewToBack:meter19Btm];
    
    meter20Top = [[JEProgressView alloc] initWithFrame:CGRectMake(384, 112, 226, 12)];
    meter20Top.transform = CGAffineTransformRotate(meter20Top.transform, 270.0/180*M_PI);
    [meter20Top setProgressImage:sliderProgressImage];
    [meter20Top setTrackImage:[UIImage alloc]];
    [meter20Top setProgress:0.0];
    [viewGeqFrame addSubview:meter20Top];
    [viewGeqFrame sendSubviewToBack:meter20Top];
    
    meter20Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(384, 338, 226, 12)];
    meter20Btm.transform = CGAffineTransformRotate(meter20Btm.transform, 90.0/180*M_PI);
    [meter20Btm setProgressImage:sliderProgressImage];
    [meter20Btm setTrackImage:[UIImage alloc]];
    [meter20Btm setProgress:0.0];
    [viewGeqFrame addSubview:meter20Btm];
    [viewGeqFrame sendSubviewToBack:meter20Btm];
    
    meter21Top = [[JEProgressView alloc] initWithFrame:CGRectMake(409, 112, 226, 12)];
    meter21Top.transform = CGAffineTransformRotate(meter21Top.transform, 270.0/180*M_PI);
    [meter21Top setProgressImage:sliderProgressImage];
    [meter21Top setTrackImage:[UIImage alloc]];
    [meter21Top setProgress:0.0];
    [viewGeqFrame addSubview:meter21Top];
    [viewGeqFrame sendSubviewToBack:meter21Top];
    
    meter21Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(409, 338, 226, 12)];
    meter21Btm.transform = CGAffineTransformRotate(meter21Btm.transform, 90.0/180*M_PI);
    [meter21Btm setProgressImage:sliderProgressImage];
    [meter21Btm setTrackImage:[UIImage alloc]];
    [meter21Btm setProgress:0.0];
    [viewGeqFrame addSubview:meter21Btm];
    [viewGeqFrame sendSubviewToBack:meter21Btm];
    
    meter22Top = [[JEProgressView alloc] initWithFrame:CGRectMake(434, 112, 226, 12)];
    meter22Top.transform = CGAffineTransformRotate(meter22Top.transform, 270.0/180*M_PI);
    [meter22Top setProgressImage:sliderProgressImage];
    [meter22Top setTrackImage:[UIImage alloc]];
    [meter22Top setProgress:0.0];
    [viewGeqFrame addSubview:meter22Top];
    [viewGeqFrame sendSubviewToBack:meter22Top];
    
    meter22Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(434, 338, 226, 12)];
    meter22Btm.transform = CGAffineTransformRotate(meter22Btm.transform, 90.0/180*M_PI);
    [meter22Btm setProgressImage:sliderProgressImage];
    [meter22Btm setTrackImage:[UIImage alloc]];
    [meter22Btm setProgress:0.0];
    [viewGeqFrame addSubview:meter22Btm];
    [viewGeqFrame sendSubviewToBack:meter22Btm];
    
    meter23Top = [[JEProgressView alloc] initWithFrame:CGRectMake(459, 112, 226, 12)];
    meter23Top.transform = CGAffineTransformRotate(meter23Top.transform, 270.0/180*M_PI);
    [meter23Top setProgressImage:sliderProgressImage];
    [meter23Top setTrackImage:[UIImage alloc]];
    [meter23Top setProgress:0.0];
    [viewGeqFrame addSubview:meter23Top];
    [viewGeqFrame sendSubviewToBack:meter23Top];
    
    meter23Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(459, 338, 226, 12)];
    meter23Btm.transform = CGAffineTransformRotate(meter23Btm.transform, 90.0/180*M_PI);
    [meter23Btm setProgressImage:sliderProgressImage];
    [meter23Btm setTrackImage:[UIImage alloc]];
    [meter23Btm setProgress:0.0];
    [viewGeqFrame addSubview:meter23Btm];
    [viewGeqFrame sendSubviewToBack:meter23Btm];
    
    meter24Top = [[JEProgressView alloc] initWithFrame:CGRectMake(484, 112, 226, 12)];
    meter24Top.transform = CGAffineTransformRotate(meter24Top.transform, 270.0/180*M_PI);
    [meter24Top setProgressImage:sliderProgressImage];
    [meter24Top setTrackImage:[UIImage alloc]];
    [meter24Top setProgress:0.0];
    [viewGeqFrame addSubview:meter24Top];
    [viewGeqFrame sendSubviewToBack:meter24Top];
    
    meter24Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(484, 338, 226, 12)];
    meter24Btm.transform = CGAffineTransformRotate(meter24Btm.transform, 90.0/180*M_PI);
    [meter24Btm setProgressImage:sliderProgressImage];
    [meter24Btm setTrackImage:[UIImage alloc]];
    [meter24Btm setProgress:0.0];
    [viewGeqFrame addSubview:meter24Btm];
    [viewGeqFrame sendSubviewToBack:meter24Btm];
    
    meter25Top = [[JEProgressView alloc] initWithFrame:CGRectMake(509, 112, 226, 12)];
    meter25Top.transform = CGAffineTransformRotate(meter25Top.transform, 270.0/180*M_PI);
    [meter25Top setProgressImage:sliderProgressImage];
    [meter25Top setTrackImage:[UIImage alloc]];
    [meter25Top setProgress:0.0];
    [viewGeqFrame addSubview:meter25Top];
    [viewGeqFrame sendSubviewToBack:meter25Top];
    
    meter25Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(509, 338, 226, 12)];
    meter25Btm.transform = CGAffineTransformRotate(meter25Btm.transform, 90.0/180*M_PI);
    [meter25Btm setProgressImage:sliderProgressImage];
    [meter25Btm setTrackImage:[UIImage alloc]];
    [meter25Btm setProgress:0.0];
    [viewGeqFrame addSubview:meter25Btm];
    [viewGeqFrame sendSubviewToBack:meter25Btm];
    
    meter26Top = [[JEProgressView alloc] initWithFrame:CGRectMake(534, 112, 226, 12)];
    meter26Top.transform = CGAffineTransformRotate(meter26Top.transform, 270.0/180*M_PI);
    [meter26Top setProgressImage:sliderProgressImage];
    [meter26Top setTrackImage:[UIImage alloc]];
    [meter26Top setProgress:0.0];
    [viewGeqFrame addSubview:meter26Top];
    [viewGeqFrame sendSubviewToBack:meter26Top];
    
    meter26Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(534, 338, 226, 12)];
    meter26Btm.transform = CGAffineTransformRotate(meter26Btm.transform, 90.0/180*M_PI);
    [meter26Btm setProgressImage:sliderProgressImage];
    [meter26Btm setTrackImage:[UIImage alloc]];
    [meter26Btm setProgress:0.0];
    [viewGeqFrame addSubview:meter26Btm];
    [viewGeqFrame sendSubviewToBack:meter26Btm];
    
    meter27Top = [[JEProgressView alloc] initWithFrame:CGRectMake(559, 112, 226, 12)];
    meter27Top.transform = CGAffineTransformRotate(meter27Top.transform, 270.0/180*M_PI);
    [meter27Top setProgressImage:sliderProgressImage];
    [meter27Top setTrackImage:[UIImage alloc]];
    [meter27Top setProgress:0.0];
    [viewGeqFrame addSubview:meter27Top];
    [viewGeqFrame sendSubviewToBack:meter27Top];
    
    meter27Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(559, 338, 226, 12)];
    meter27Btm.transform = CGAffineTransformRotate(meter27Btm.transform, 90.0/180*M_PI);
    [meter27Btm setProgressImage:sliderProgressImage];
    [meter27Btm setTrackImage:[UIImage alloc]];
    [meter27Btm setProgress:0.0];
    [viewGeqFrame addSubview:meter27Btm];
    [viewGeqFrame sendSubviewToBack:meter27Btm];
    
    meter28Top = [[JEProgressView alloc] initWithFrame:CGRectMake(584, 112, 226, 12)];
    meter28Top.transform = CGAffineTransformRotate(meter28Top.transform, 270.0/180*M_PI);
    [meter28Top setProgressImage:sliderProgressImage];
    [meter28Top setTrackImage:[UIImage alloc]];
    [meter28Top setProgress:0.0];
    [viewGeqFrame addSubview:meter28Top];
    [viewGeqFrame sendSubviewToBack:meter28Top];
    
    meter28Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(584, 338, 226, 12)];
    meter28Btm.transform = CGAffineTransformRotate(meter28Btm.transform, 90.0/180*M_PI);
    [meter28Btm setProgressImage:sliderProgressImage];
    [meter28Btm setTrackImage:[UIImage alloc]];
    [meter28Btm setProgress:0.0];
    [viewGeqFrame addSubview:meter28Btm];
    [viewGeqFrame sendSubviewToBack:meter28Btm];
    
    meter29Top = [[JEProgressView alloc] initWithFrame:CGRectMake(609, 112, 226, 12)];
    meter29Top.transform = CGAffineTransformRotate(meter29Top.transform, 270.0/180*M_PI);
    [meter29Top setProgressImage:sliderProgressImage];
    [meter29Top setTrackImage:[UIImage alloc]];
    [meter29Top setProgress:0.0];
    [viewGeqFrame addSubview:meter29Top];
    [viewGeqFrame sendSubviewToBack:meter29Top];
    
    meter29Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(609, 338, 226, 12)];
    meter29Btm.transform = CGAffineTransformRotate(meter29Btm.transform, 90.0/180*M_PI);
    [meter29Btm setProgressImage:sliderProgressImage];
    [meter29Btm setTrackImage:[UIImage alloc]];
    [meter29Btm setProgress:0.0];
    [viewGeqFrame addSubview:meter29Btm];
    [viewGeqFrame sendSubviewToBack:meter29Btm];
    
    meter30Top = [[JEProgressView alloc] initWithFrame:CGRectMake(634, 112, 226, 12)];
    meter30Top.transform = CGAffineTransformRotate(meter30Top.transform, 270.0/180*M_PI);
    [meter30Top setProgressImage:sliderProgressImage];
    [meter30Top setTrackImage:[UIImage alloc]];
    [meter30Top setProgress:0.0];
    [viewGeqFrame addSubview:meter30Top];
    [viewGeqFrame sendSubviewToBack:meter30Top];
    
    meter30Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(634, 338, 226, 12)];
    meter30Btm.transform = CGAffineTransformRotate(meter30Btm.transform, 90.0/180*M_PI);
    [meter30Btm setProgressImage:sliderProgressImage];
    [meter30Btm setTrackImage:[UIImage alloc]];
    [meter30Btm setProgress:0.0];
    [viewGeqFrame addSubview:meter30Btm];
    [viewGeqFrame sendSubviewToBack:meter30Btm];
    
    meter31Top = [[JEProgressView alloc] initWithFrame:CGRectMake(659, 112, 226, 12)];
    meter31Top.transform = CGAffineTransformRotate(meter31Top.transform, 270.0/180*M_PI);
    [meter31Top setProgressImage:sliderProgressImage];
    [meter31Top setTrackImage:[UIImage alloc]];
    [meter31Top setProgress:0.0];
    [viewGeqFrame addSubview:meter31Top];
    [viewGeqFrame sendSubviewToBack:meter31Top];
    
    meter31Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(659, 338, 226, 12)];
    meter31Btm.transform = CGAffineTransformRotate(meter31Btm.transform, 90.0/180*M_PI);
    [meter31Btm setProgressImage:sliderProgressImage];
    [meter31Btm setTrackImage:[UIImage alloc]];
    [meter31Btm setProgress:0.0];
    [viewGeqFrame addSubview:meter31Btm];
    [viewGeqFrame sendSubviewToBack:meter31Btm];
    
    mainEqType = MAIN_EQ_GEQ;
    [self updateGeqMeters];
    [self updatePreviewImageForChannel:MAIN];
    [self.viewGeq sendSubviewToBack:self.viewGeqDraw];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
    NSLog(@"GeqPeqViewController didReceiveMemoryWarning");
}

- (void)updatePeqFrame
{
    // Update PEQ button positions
    btnBall1.center = CGPointMake([self peqFreqToX:peq1F[currentChannel]], [self peqGainToY:peq1G[currentChannel]]);
    btnBall2.center = CGPointMake([self peqFreqToX:peq2F[currentChannel]], [self peqGainToY:peq2G[currentChannel]]);
    btnBall3.center = CGPointMake([self peqFreqToX:peq3F[currentChannel]], [self peqGainToY:peq3G[currentChannel]]);
    btnBall4.center = CGPointMake([self peqFreqToX:peq4F[currentChannel]], [self peqGainToY:peq4G[currentChannel]]);
    
    btnPeqOnOff.selected = peqEnabled[currentChannel];
    btnPeq1.selected = peq1Enabled[currentChannel];
    btnPeq2.selected = peq2Enabled[currentChannel];
    btnPeq3.selected = peq3Enabled[currentChannel];
    btnPeq4.selected = peq4Enabled[currentChannel];
    btnBall1.hidden = !btnPeq1.selected;
    btnBall2.hidden = !btnPeq2.selected;
    btnBall3.hidden = !btnPeq3.selected;
    btnBall4.hidden = !btnPeq4.selected;
    [self updatePeqPlot:btnPeqOnOff.selected];
    [self resetBalls];
    
    // Update PEQ labels
    lblPeq1ValueG.text = [Global getEqGainString:peq1G[currentChannel]];
    lblPeq1ValueF.text = [Global getEqFreqString:peq1F[currentChannel]];
    lblPeq1ValueQ.text = [Global getEqQ1String:peq1Q[currentChannel]];
    lblPeq2ValueG.text = [Global getEqGainString:peq2G[currentChannel]];
    lblPeq2ValueF.text = [Global getEqFreqString:peq2F[currentChannel]];
    lblPeq2ValueQ.text = [Global getEqQ23String:peq2Q[currentChannel]];
    lblPeq3ValueG.text = [Global getEqGainString:peq3G[currentChannel]];
    lblPeq3ValueF.text = [Global getEqFreqString:peq3F[currentChannel]];
    lblPeq3ValueQ.text = [Global getEqQ23String:peq3Q[currentChannel]];
    lblPeq4ValueG.text = [Global getEqGainString:peq4G[currentChannel]];
    lblPeq4ValueF.text = [Global getEqFreqString:peq4F[currentChannel]];
    lblPeq4ValueQ.text = [Global getEqQ4String:peq4Q[currentChannel]];
    
    // Calculate data
    if (peq1Enabled[currentChannel]) {
        if (peq1Filter[currentChannel] == EQ_PEAK) {
            peqData1 = [NSMutableArray arrayWithArray:[Global eqPeakData:[Global getEqFreq:peq1F[currentChannel]]
                                                                    eqDb:[Global getEqGain:peq1G[currentChannel]]
                                                                     eqQ:[Global getEqQ:peq1Q[currentChannel]]]];
        } else if (peq1Filter[currentChannel] == EQ_LOW_SHELF) {
            peqData1 = [NSMutableArray arrayWithArray:[Global eqLowShelfData:[Global getEqFreq:peq1F[currentChannel]] eqDb:[Global getEqGain:peq1G[currentChannel]]]];
        } else {
            peqData1 = [NSMutableArray arrayWithArray:[Global eqHPFData:[Global getEqFreq:peq1F[currentChannel]]]];
        }
    } else {
        peqData1 = [NSMutableArray arrayWithArray:[Global eqPeakData:[Global getEqFreq:PEQ_1_DEFAULT_F]
                                                                eqDb:[Global getEqGain:PEQ_DEFAULT_G]
                                                                 eqQ:[Global getEqQ:PEQ_DEFAULT_Q]]];
    }

    if (peq2Enabled[currentChannel]) {
        peqData2 = [NSMutableArray arrayWithArray:[Global eqPeakData:[Global getEqFreq:peq2F[currentChannel]]
                                                                eqDb:[Global getEqGain:peq2G[currentChannel]]
                                                                 eqQ:[Global getEqQ:peq2Q[currentChannel]]]];
    } else {
        peqData2 = [NSMutableArray arrayWithArray:[Global eqPeakData:[Global getEqFreq:PEQ_2_DEFAULT_F]
                                                                eqDb:[Global getEqGain:PEQ_DEFAULT_G]
                                                                 eqQ:[Global getEqQ:PEQ_DEFAULT_Q]]];
    }

    if (peq3Enabled[currentChannel]) {
        peqData3 = [NSMutableArray arrayWithArray:[Global eqPeakData:[Global getEqFreq:peq3F[currentChannel]]
                                                                eqDb:[Global getEqGain:peq3G[currentChannel]]
                                                                 eqQ:[Global getEqQ:peq3Q[currentChannel]]]];
    } else {
        peqData3 = [NSMutableArray arrayWithArray:[Global eqPeakData:[Global getEqFreq:PEQ_3_DEFAULT_F]
                                                                eqDb:[Global getEqGain:PEQ_DEFAULT_G]
                                                                 eqQ:[Global getEqQ:PEQ_DEFAULT_Q]]];
    }

    if (peq4Enabled[currentChannel]) {
        if (peq4Filter[currentChannel] == EQ_PEAK) {
            peqData4 = [NSMutableArray arrayWithArray:[Global eqPeakData:[Global getEqFreq:peq4F[currentChannel]]
                                                                    eqDb:[Global getEqGain:peq4G[currentChannel]]
                                                                     eqQ:[Global getEqQ:peq4Q[currentChannel]]]];
        } else if (peq4Filter[currentChannel] == EQ_HIGH_SHELF) {
            peqData4 = [NSMutableArray arrayWithArray:[Global eqHighShelfData:[Global getEqFreq:peq4F[currentChannel]] eqDb:[Global getEqGain:peq4G[currentChannel]]]];
        } else {
            peqData4 = [NSMutableArray arrayWithArray:[Global eqLPFData:[Global getEqFreq:peq4F[currentChannel]]]];
        }
    } else {
        peqData4 = [NSMutableArray arrayWithArray:[Global eqPeakData:[Global getEqFreq:PEQ_4_DEFAULT_F]
                                                                eqDb:[Global getEqGain:PEQ_DEFAULT_G]
                                                                 eqQ:[Global getEqQ:PEQ_DEFAULT_Q]]];
    }
    
    // Redraw graph
    [self calculatePeqData];
}

- (void)updatePeqForChannel:(int)channel
{
    // Calculate data
    if (peq1Enabled[channel]) {
        if (peq1Filter[channel] == EQ_PEAK) {
            peqData1 = [NSMutableArray arrayWithArray:[Global eqPeakData:[Global getEqFreq:peq1F[channel]]
                                                                    eqDb:[Global getEqGain:peq1G[channel]]
                                                                     eqQ:[Global getEqQ:peq1Q[channel]]]];
        } else if (peq1Filter[channel] == EQ_LOW_SHELF) {
            peqData1 = [NSMutableArray arrayWithArray:[Global eqLowShelfData:[Global getEqFreq:peq1F[channel]] eqDb:[Global getEqGain:peq1G[channel]]]];
        } else {
            peqData1 = [NSMutableArray arrayWithArray:[Global eqHPFData:[Global getEqFreq:peq1F[channel]]]];
        }
    } else {
        peqData1 = [NSMutableArray arrayWithArray:[Global eqPeakData:[Global getEqFreq:PEQ_1_DEFAULT_F]
                                                                eqDb:[Global getEqGain:PEQ_DEFAULT_G]
                                                                 eqQ:[Global getEqQ:PEQ_DEFAULT_Q]]];
    }
    
    if (peq2Enabled[channel]) {
        peqData2 = [NSMutableArray arrayWithArray:[Global eqPeakData:[Global getEqFreq:peq2F[channel]]
                                                                eqDb:[Global getEqGain:peq2G[channel]]
                                                                 eqQ:[Global getEqQ:peq2Q[channel]]]];
    } else {
        peqData2 = [NSMutableArray arrayWithArray:[Global eqPeakData:[Global getEqFreq:PEQ_2_DEFAULT_F]
                                                                eqDb:[Global getEqGain:PEQ_DEFAULT_G]
                                                                 eqQ:[Global getEqQ:PEQ_DEFAULT_Q]]];
    }
    
    if (peq3Enabled[channel]) {
        peqData3 = [NSMutableArray arrayWithArray:[Global eqPeakData:[Global getEqFreq:peq3F[channel]]
                                                                eqDb:[Global getEqGain:peq3G[channel]]
                                                                 eqQ:[Global getEqQ:peq3Q[channel]]]];
    } else {
        peqData3 = [NSMutableArray arrayWithArray:[Global eqPeakData:[Global getEqFreq:PEQ_3_DEFAULT_F]
                                                                eqDb:[Global getEqGain:PEQ_DEFAULT_G]
                                                                 eqQ:[Global getEqQ:PEQ_DEFAULT_Q]]];
    }
    
    if (peq4Enabled[channel]) {
        if (peq4Filter[channel] == EQ_PEAK) {
            peqData4 = [NSMutableArray arrayWithArray:[Global eqPeakData:[Global getEqFreq:peq4F[channel]]
                                                                    eqDb:[Global getEqGain:peq4G[channel]]
                                                                     eqQ:[Global getEqQ:peq4Q[channel]]]];
        } else if (peq4Filter[channel] == EQ_HIGH_SHELF) {
            peqData4 = [NSMutableArray arrayWithArray:[Global eqHighShelfData:[Global getEqFreq:peq4F[channel]] eqDb:[Global getEqGain:peq4G[channel]]]];
        } else {
            peqData4 = [NSMutableArray arrayWithArray:[Global eqLPFData:[Global getEqFreq:peq4F[channel]]]];
        }
    } else {
        peqData4 = [NSMutableArray arrayWithArray:[Global eqPeakData:[Global getEqFreq:PEQ_4_DEFAULT_F]
                                                                eqDb:[Global getEqGain:PEQ_DEFAULT_G]
                                                                 eqQ:[Global getEqQ:PEQ_DEFAULT_Q]]];
    }
    
    // Redraw graph
    [self calculatePeqForChannel:channel];
}

- (void)updatePeq1Filter
{
    if (peq1Q[currentChannel] == PEQ_1_LOW_SHELF) {
        lblPeq1ValueG.hidden = NO;
        lblPeq1ValueQ.hidden = YES;
        [btnPeqFilterFront setImage:[UIImage imageNamed:@"frame-12.png"] forState:UIControlStateNormal];
        peq1Filter[currentChannel] = EQ_LOW_SHELF;
    } else if (peq1Q[currentChannel] == PEQ_1_HPF) {
        lblPeq1ValueG.hidden = YES;
        lblPeq1ValueQ.hidden = YES;
        [btnPeqFilterFront setImage:[UIImage imageNamed:@"frame-13.png"] forState:UIControlStateNormal];
        peq1Filter[currentChannel] = EQ_LOW_CUT;
    } else {
        lblPeq1ValueG.hidden = NO;
        lblPeq1ValueQ.hidden = NO;
        [btnPeqFilterFront setImage:[UIImage imageNamed:@"frame-11.png"] forState:UIControlStateNormal];
        peq1Filter[currentChannel] = EQ_PEAK;
        lastPeq1Q[currentChannel] = peq1Q[currentChannel];
    }
}

- (void)updatePeq4Filter
{
    if (peq4Q[currentChannel] == PEQ_4_HIGH_SHELF) {
        lblPeq4ValueG.hidden = NO;
        lblPeq4ValueQ.hidden = YES;
        [btnPeqFilterBack setImage:[UIImage imageNamed:@"frame-15.png"] forState:UIControlStateNormal];
        peq4Filter[currentChannel] = EQ_HIGH_SHELF;
    } else if (peq4Q[currentChannel] == PEQ_4_LPF) {
        lblPeq4ValueG.hidden = YES;
        lblPeq4ValueQ.hidden = YES;
        [btnPeqFilterBack setImage:[UIImage imageNamed:@"frame-14.png"] forState:UIControlStateNormal];
        peq4Filter[currentChannel] = EQ_HIGH_CUT;
    } else {
        lblPeq4ValueG.hidden = NO;
        lblPeq4ValueQ.hidden = NO;
        [btnPeqFilterBack setImage:[UIImage imageNamed:@"frame-11.png"] forState:UIControlStateNormal];
        peq4Filter[currentChannel] = EQ_PEAK;
        lastPeq4Q[currentChannel] = peq4Q[currentChannel];
    }
}

- (void)processChannelReply:(EqChannelPagePacket *)pkt
{
    BOOL valueChanged = NO;
    
    if ([self.view isHidden]) {
        NSLog(@"processChannelReply: view is hidden!");
        return;
    }
    
    if (peqLock) {
        NSLog(@"processChannelReply: peqLock");
        return;
    }
    
    _ch1Ctrl = pkt->ch_1_ctrl;
    _ch2Ctrl = pkt->ch_2_ctrl;
    _ch3Ctrl = pkt->ch_3_ctrl;
    _ch4Ctrl = pkt->ch_4_ctrl;
    _ch5Ctrl = pkt->ch_5_ctrl;
    _ch6Ctrl = pkt->ch_6_ctrl;
    _ch7Ctrl = pkt->ch_7_ctrl;
    _ch8Ctrl = pkt->ch_8_ctrl;
    _ch9Ctrl = pkt->ch_9_ctrl;
    _ch10Ctrl = pkt->ch_10_ctrl;
    _ch11Ctrl = pkt->ch_11_ctrl;
    _ch12Ctrl = pkt->ch_12_ctrl;
    _ch13Ctrl = pkt->ch_13_ctrl;
    _ch14Ctrl = pkt->ch_14_ctrl;
    _ch15Ctrl = pkt->ch_15_ctrl;
    _ch16Ctrl = pkt->ch_16_ctrl;
    _ch17Ctrl = pkt->ch_17_ctrl;
    _ch18Ctrl = pkt->ch_18_ctrl;
    _ch17Setting = pkt->ch_17_audio_setting;
    _ch18Setting = pkt->ch_18_audio_setting;
    _soloCtrl = pkt->solo_ctrl;
    _chOnOff = pkt->ch_on_off;
    _chMeterPrePost = pkt->ch_meter_pre_post;
    
    switch (currentChannel) {
        case CHANNEL_CH_1:
        {
            if (peq1G[currentChannel] != pkt->ch_1_eq_1_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH1_EQ1_DB dspId:DSP_1]) {
                peq1G[currentChannel] = pkt->ch_1_eq_1_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq1F[currentChannel] != pkt->ch_1_eq_1_freq && ![viewController sendCommandExists:CMD_CH1_EQ1_FREQ dspId:DSP_1]) {
                peq1F[currentChannel] = pkt->ch_1_eq_1_freq;
                valueChanged = YES;
            }
            if (peq1Q[currentChannel] != pkt->ch_1_eq_1_q && ![viewController sendCommandExists:CMD_CH1_EQ1_Q dspId:DSP_1]) {
                peq1Q[currentChannel] = pkt->ch_1_eq_1_q;
                [self updatePeq1Filter];
                valueChanged = YES;
            }
            if (peq2G[currentChannel] != pkt->ch_1_eq_2_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH1_EQ2_DB dspId:DSP_1]) {
                peq2G[currentChannel] = pkt->ch_1_eq_2_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq2F[currentChannel] != pkt->ch_1_eq_2_freq && ![viewController sendCommandExists:CMD_CH1_EQ2_FREQ dspId:DSP_1]) {
                peq2F[currentChannel] = pkt->ch_1_eq_2_freq;
                valueChanged = YES;
            }
            if (peq2Q[currentChannel] != pkt->ch_1_eq_2_q && ![viewController sendCommandExists:CMD_CH1_EQ2_Q dspId:DSP_1]) {
                peq2Q[currentChannel] = pkt->ch_1_eq_2_q;
                valueChanged = YES;
            }
            if (peq3G[currentChannel] != pkt->ch_1_eq_3_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH1_EQ3_DB dspId:DSP_1]) {
                peq3G[currentChannel] = pkt->ch_1_eq_3_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq3F[currentChannel] != pkt->ch_1_eq_3_freq && ![viewController sendCommandExists:CMD_CH1_EQ3_FREQ dspId:DSP_1]) {
                peq3F[currentChannel] = pkt->ch_1_eq_3_freq;
                valueChanged = YES;
            }
            if (peq3Q[currentChannel] != pkt->ch_1_eq_3_q && ![viewController sendCommandExists:CMD_CH1_EQ3_Q dspId:DSP_1]) {
                peq3Q[currentChannel] = pkt->ch_1_eq_3_q;
                valueChanged = YES;
            }
            if (peq4G[currentChannel] != pkt->ch_1_eq_4_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH1_EQ4_DB dspId:DSP_1]) {
                peq4G[currentChannel] = pkt->ch_1_eq_4_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq4F[currentChannel] != pkt->ch_1_eq_4_freq && ![viewController sendCommandExists:CMD_CH1_EQ4_FREQ dspId:DSP_1]) {
                peq4F[currentChannel] = pkt->ch_1_eq_4_freq;
                valueChanged = YES;
            }
            if (peq4Q[currentChannel] != pkt->ch_1_eq_4_q && ![viewController sendCommandExists:CMD_CH1_EQ4_Q dspId:DSP_1]) {
                peq4Q[currentChannel] = pkt->ch_1_eq_4_q;
                [self updatePeq4Filter];
                valueChanged = YES;
            }
            meterIn.progress = ((float)pkt->ch_1_meter_eq_in/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterOut.progress = ((float)pkt->ch_1_meter_eq_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            if (!sliderLock) {
                slider.value = pkt->ch_1_fader;
                [self onSliderAction:nil];
            }
            BOOL sliderOn = pkt->ch_on_off & 0x01;
            if (sliderOn != btnOn.selected && ![viewController sendCommandExists:CMD_CH_ON_OFF dspId:DSP_5]) {
                btnOn.selected = sliderOn;
            }
            BOOL sliderSolo = pkt->solo_ctrl & 0x01;
            if (sliderSolo != btnSolo.selected && ![viewController sendCommandExists:CMD_SOLO_CTRL dspId:DSP_5]) {
                btnSolo.selected = sliderSolo;
            }
            BOOL sliderSoloSafe = pkt->solo_safe_ctrl & 0x01;
            if (sliderSoloSafe) {
                [btnSolo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
                [btnSolo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
            } else {
                [btnSolo setTitle:@"SOLO" forState:UIControlStateNormal];
                [btnSolo setTitle:@"SOLO" forState:UIControlStateSelected];
            }
            BOOL sliderMeter = pkt->ch_meter_pre_post & 0x01;
            if (sliderMeter != btnMeterPrePost.selected && ![viewController sendCommandExists:CMD_CH_METER dspId:DSP_5]) {
                btnMeterPrePost.selected = sliderMeter;
            }
            int peqEnableValue = pkt->ch_1_ctrl;
            if (btnPeq1.selected != ((peqEnableValue & 0x08) >> 3) && ![viewController sendCommandExists:CMD_CH1_CTRL dspId:DSP_1]) {
                btnPeq1.selected = peq1Enabled[currentChannel] = (peqEnableValue & 0x08) >> 3;
                valueChanged = YES;
            }
            if (btnPeq2.selected != ((peqEnableValue & 0x10) >> 4) && ![viewController sendCommandExists:CMD_CH1_CTRL dspId:DSP_1]) {
                btnPeq2.selected = peq2Enabled[currentChannel] = (peqEnableValue & 0x10) >> 4;
                valueChanged = YES;
            }
            if (btnPeq3.selected != ((peqEnableValue & 0x20) >> 5) && ![viewController sendCommandExists:CMD_CH1_CTRL dspId:DSP_1]) {
                btnPeq3.selected = peq3Enabled[currentChannel] = (peqEnableValue & 0x20) >> 5;
                valueChanged = YES;
            }
            if (btnPeq4.selected != ((peqEnableValue & 0x40) >> 6) && ![viewController sendCommandExists:CMD_CH1_CTRL dspId:DSP_1]) {
                btnPeq4.selected = peq4Enabled[currentChannel] = (peqEnableValue & 0x40) >> 6;
                valueChanged = YES;
            }
        }
            break;
        case CHANNEL_CH_2:
        {
            if (peq1G[currentChannel] != pkt->ch_2_eq_1_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH2_EQ1_DB dspId:DSP_1]) {
                peq1G[currentChannel] = pkt->ch_2_eq_1_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq1F[currentChannel] != pkt->ch_2_eq_1_freq && ![viewController sendCommandExists:CMD_CH2_EQ1_FREQ dspId:DSP_1]) {
                peq1F[currentChannel] = pkt->ch_2_eq_1_freq;
                valueChanged = YES;
            }
            if (peq1Q[currentChannel] != pkt->ch_2_eq_1_q && ![viewController sendCommandExists:CMD_CH2_EQ1_Q dspId:DSP_1]) {
                peq1Q[currentChannel] = pkt->ch_2_eq_1_q;
                [self updatePeq1Filter];
                valueChanged = YES;
            }
            if (peq2G[currentChannel] != pkt->ch_2_eq_2_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH2_EQ2_DB dspId:DSP_1]) {
                peq2G[currentChannel] = pkt->ch_2_eq_2_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq2F[currentChannel] != pkt->ch_2_eq_2_freq && ![viewController sendCommandExists:CMD_CH2_EQ1_FREQ dspId:DSP_1]) {
                peq2F[currentChannel] = pkt->ch_2_eq_2_freq;
                valueChanged = YES;
            }
            if (peq2Q[currentChannel] != pkt->ch_2_eq_2_q && ![viewController sendCommandExists:CMD_CH2_EQ2_Q dspId:DSP_1]) {
                peq2Q[currentChannel] = pkt->ch_2_eq_2_q;
                valueChanged = YES;
            }
            if (peq3G[currentChannel] != pkt->ch_2_eq_3_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH2_EQ3_DB dspId:DSP_1]) {
                peq3G[currentChannel] = pkt->ch_2_eq_3_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq3F[currentChannel] != pkt->ch_2_eq_3_freq && ![viewController sendCommandExists:CMD_CH2_EQ3_FREQ dspId:DSP_1]) {
                peq3F[currentChannel] = pkt->ch_2_eq_3_freq;
                valueChanged = YES;
            }
            if (peq3Q[currentChannel] != pkt->ch_2_eq_3_q && ![viewController sendCommandExists:CMD_CH2_EQ3_Q dspId:DSP_1]) {
                peq3Q[currentChannel] = pkt->ch_2_eq_3_q;
                valueChanged = YES;
            }
            if (peq4G[currentChannel] != pkt->ch_2_eq_4_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH2_EQ4_DB dspId:DSP_1]) {
                peq4G[currentChannel] = pkt->ch_2_eq_4_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq4F[currentChannel] != pkt->ch_2_eq_4_freq && ![viewController sendCommandExists:CMD_CH2_EQ4_FREQ dspId:DSP_1]) {
                peq4F[currentChannel] = pkt->ch_2_eq_4_freq;
                valueChanged = YES;
            }
            if (peq4Q[currentChannel] != pkt->ch_2_eq_4_q && ![viewController sendCommandExists:CMD_CH2_EQ4_Q dspId:DSP_1]) {
                peq4Q[currentChannel] = pkt->ch_2_eq_4_q;
                [self updatePeq4Filter];
                valueChanged = YES;
            }
            meterIn.progress = ((float)pkt->ch_2_meter_eq_in/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterOut.progress = ((float)pkt->ch_2_meter_eq_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            if (!sliderLock) {
                slider.value = pkt->ch_2_fader;
                [self onSliderAction:nil];
            }
            BOOL sliderOn = (pkt->ch_on_off & 0x02) >> 1;
            if (sliderOn != btnOn.selected && ![viewController sendCommandExists:CMD_CH_ON_OFF dspId:DSP_5]) {
                btnOn.selected = sliderOn;
            }
            BOOL sliderSolo = (pkt->solo_ctrl & 0x02) >> 1;
            if (sliderSolo != btnSolo.selected && ![viewController sendCommandExists:CMD_SOLO_CTRL dspId:DSP_5]) {
                btnSolo.selected = sliderSolo;
            }
            BOOL sliderSoloSafe = (pkt->solo_safe_ctrl & 0x02) >> 1;
            if (sliderSoloSafe) {
                [btnSolo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
                [btnSolo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
            } else {
                [btnSolo setTitle:@"SOLO" forState:UIControlStateNormal];
                [btnSolo setTitle:@"SOLO" forState:UIControlStateSelected];
            }
            BOOL sliderMeter = (pkt->ch_meter_pre_post & 0x02) >> 1;
            if (sliderMeter != btnMeterPrePost.selected && ![viewController sendCommandExists:CMD_CH_METER dspId:DSP_5]) {
                btnMeterPrePost.selected = sliderMeter;
            }
            int peqEnableValue = pkt->ch_2_ctrl;
            if (btnPeq1.selected != ((peqEnableValue & 0x08) >> 3) && ![viewController sendCommandExists:CMD_CH2_CTRL dspId:DSP_1]) {
                btnPeq1.selected = peq1Enabled[currentChannel] = (peqEnableValue & 0x08) >> 3;
                valueChanged = YES;
            }
            if (btnPeq2.selected != ((peqEnableValue & 0x10) >> 4) && ![viewController sendCommandExists:CMD_CH2_CTRL dspId:DSP_1]) {
                btnPeq2.selected = peq2Enabled[currentChannel] = (peqEnableValue & 0x10) >> 4;
                valueChanged = YES;
            }
            if (btnPeq3.selected != ((peqEnableValue & 0x20) >> 5) && ![viewController sendCommandExists:CMD_CH2_CTRL dspId:DSP_1]) {
                btnPeq3.selected = peq3Enabled[currentChannel] = (peqEnableValue & 0x20) >> 5;
                valueChanged = YES;
            }
            if (btnPeq4.selected != ((peqEnableValue & 0x40) >> 6) && ![viewController sendCommandExists:CMD_CH2_CTRL dspId:DSP_1]) {
                btnPeq4.selected = peq4Enabled[currentChannel] = (peqEnableValue & 0x40) >> 6;
                valueChanged = YES;
            }
        }
            break;
        case CHANNEL_CH_3:
        {
            if (peq1G[currentChannel] != pkt->ch_3_eq_1_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH3_EQ1_DB dspId:DSP_1]) {
                peq1G[currentChannel] = pkt->ch_3_eq_1_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq1F[currentChannel] != pkt->ch_3_eq_1_freq && ![viewController sendCommandExists:CMD_CH3_EQ1_FREQ dspId:DSP_1]) {
                peq1F[currentChannel] = pkt->ch_3_eq_1_freq;
                valueChanged = YES;
            }
            if (peq1Q[currentChannel] != pkt->ch_3_eq_1_q && ![viewController sendCommandExists:CMD_CH3_EQ1_Q dspId:DSP_1]) {
                peq1Q[currentChannel] = pkt->ch_3_eq_1_q;
                [self updatePeq1Filter];
                valueChanged = YES;
            }
            if (peq2G[currentChannel] != pkt->ch_3_eq_2_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH3_EQ2_DB dspId:DSP_1]) {
                peq2G[currentChannel] = pkt->ch_3_eq_2_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq2F[currentChannel] != pkt->ch_3_eq_2_freq && ![viewController sendCommandExists:CMD_CH3_EQ2_FREQ dspId:DSP_1]) {
                peq2F[currentChannel] = pkt->ch_3_eq_2_freq;
                valueChanged = YES;
            }
            if (peq2Q[currentChannel] != pkt->ch_3_eq_2_q && ![viewController sendCommandExists:CMD_CH3_EQ2_Q dspId:DSP_1]) {
                peq2Q[currentChannel] = pkt->ch_3_eq_2_q;
                valueChanged = YES;
            }
            if (peq3G[currentChannel] != pkt->ch_3_eq_3_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH3_EQ3_DB dspId:DSP_1]) {
                peq3G[currentChannel] = pkt->ch_3_eq_3_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq3F[currentChannel] != pkt->ch_3_eq_3_freq && ![viewController sendCommandExists:CMD_CH3_EQ3_FREQ dspId:DSP_1]) {
                peq3F[currentChannel] = pkt->ch_3_eq_3_freq;
                valueChanged = YES;
            }
            if (peq3Q[currentChannel] != pkt->ch_3_eq_3_q && ![viewController sendCommandExists:CMD_CH3_EQ3_Q dspId:DSP_1]) {
                peq3Q[currentChannel] = pkt->ch_3_eq_3_q;
                valueChanged = YES;
            }
            if (peq4G[currentChannel] != pkt->ch_3_eq_4_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH3_EQ4_DB dspId:DSP_1]) {
                peq4G[currentChannel] = pkt->ch_3_eq_4_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq4F[currentChannel] != pkt->ch_3_eq_4_freq && ![viewController sendCommandExists:CMD_CH3_EQ4_FREQ dspId:DSP_1]) {
                peq4F[currentChannel] = pkt->ch_3_eq_4_freq;
                valueChanged = YES;
            }
            if (peq4Q[currentChannel] != pkt->ch_3_eq_4_q && ![viewController sendCommandExists:CMD_CH3_EQ4_Q dspId:DSP_1]) {
                peq4Q[currentChannel] = pkt->ch_3_eq_4_q;
                [self updatePeq4Filter];
                valueChanged = YES;
            }
            meterIn.progress = ((float)pkt->ch_3_meter_eq_in/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterOut.progress = ((float)pkt->ch_3_meter_eq_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            if (!sliderLock) {
                slider.value = pkt->ch_3_fader;
                [self onSliderAction:nil];
            }
            BOOL sliderOn = (pkt->ch_on_off & 0x04) >> 2;
            if (sliderOn != btnOn.selected && ![viewController sendCommandExists:CMD_CH_ON_OFF dspId:DSP_5]) {
                btnOn.selected = sliderOn;
            }
            BOOL sliderSolo = (pkt->solo_ctrl & 0x04) >> 2;
            if (sliderSolo != btnSolo.selected && ![viewController sendCommandExists:CMD_SOLO_CTRL dspId:DSP_5]) {
                btnSolo.selected = sliderSolo;
            }
            BOOL sliderSoloSafe = (pkt->solo_safe_ctrl & 0x04) >> 2;
            if (sliderSoloSafe) {
                [btnSolo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
                [btnSolo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
            } else {
                [btnSolo setTitle:@"SOLO" forState:UIControlStateNormal];
                [btnSolo setTitle:@"SOLO" forState:UIControlStateSelected];
            }
            BOOL sliderMeter = (pkt->ch_meter_pre_post & 0x04) >> 2;
            if (sliderMeter != btnMeterPrePost.selected && ![viewController sendCommandExists:CMD_CH_METER dspId:DSP_5]) {
                btnMeterPrePost.selected = sliderMeter;
            }
            int peqEnableValue = pkt->ch_3_ctrl;
            if (btnPeq1.selected != ((peqEnableValue & 0x08) >> 3) && ![viewController sendCommandExists:CMD_CH3_CTRL dspId:DSP_1]) {
                btnPeq1.selected = peq1Enabled[currentChannel] = (peqEnableValue & 0x08) >> 3;
                valueChanged = YES;
            }
            if (btnPeq2.selected != ((peqEnableValue & 0x10) >> 4) && ![viewController sendCommandExists:CMD_CH3_CTRL dspId:DSP_1]) {
                btnPeq2.selected = peq2Enabled[currentChannel] = (peqEnableValue & 0x10) >> 4;
                valueChanged = YES;
            }
            if (btnPeq3.selected != ((peqEnableValue & 0x20) >> 5) && ![viewController sendCommandExists:CMD_CH3_CTRL dspId:DSP_1]) {
                btnPeq3.selected = peq3Enabled[currentChannel] = (peqEnableValue & 0x20) >> 5;
                valueChanged = YES;
            }
            if (btnPeq4.selected != ((peqEnableValue & 0x40) >> 6) && ![viewController sendCommandExists:CMD_CH3_CTRL dspId:DSP_1]) {
                btnPeq4.selected = peq4Enabled[currentChannel] = (peqEnableValue & 0x40) >> 6;
                valueChanged = YES;
            }
        }
            break;
        case CHANNEL_CH_4:
        {
            if (peq1G[currentChannel] != pkt->ch_4_eq_1_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH4_EQ1_DB dspId:DSP_1]) {
                peq1G[currentChannel] = pkt->ch_4_eq_1_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq1F[currentChannel] != pkt->ch_4_eq_1_freq && ![viewController sendCommandExists:CMD_CH4_EQ1_FREQ dspId:DSP_1]) {
                peq1F[currentChannel] = pkt->ch_4_eq_1_freq;
                valueChanged = YES;
            }
            if (peq1Q[currentChannel] != pkt->ch_4_eq_1_q && ![viewController sendCommandExists:CMD_CH4_EQ1_Q dspId:DSP_1]) {
                peq1Q[currentChannel] = pkt->ch_4_eq_1_q;
                [self updatePeq1Filter];
                valueChanged = YES;
            }
            if (peq2G[currentChannel] != pkt->ch_4_eq_2_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH4_EQ2_DB dspId:DSP_1]) {
                peq2G[currentChannel] = pkt->ch_4_eq_2_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq2F[currentChannel] != pkt->ch_4_eq_2_freq && ![viewController sendCommandExists:CMD_CH4_EQ2_FREQ dspId:DSP_1]) {
                peq2F[currentChannel] = pkt->ch_4_eq_2_freq;
                valueChanged = YES;
            }
            if (peq2Q[currentChannel] != pkt->ch_4_eq_2_q && ![viewController sendCommandExists:CMD_CH4_EQ2_Q dspId:DSP_1]) {
                peq2Q[currentChannel] = pkt->ch_4_eq_2_q;
                valueChanged = YES;
            }
            if (peq3G[currentChannel] != pkt->ch_4_eq_3_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH4_EQ3_DB dspId:DSP_1]) {
                peq3G[currentChannel] = pkt->ch_4_eq_3_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq3F[currentChannel] != pkt->ch_4_eq_3_freq && ![viewController sendCommandExists:CMD_CH4_EQ3_FREQ dspId:DSP_1]) {
                peq3F[currentChannel] = pkt->ch_4_eq_3_freq;
                valueChanged = YES;
            }
            if (peq3Q[currentChannel] != pkt->ch_4_eq_3_q && ![viewController sendCommandExists:CMD_CH4_EQ3_Q dspId:DSP_1]) {
                peq3Q[currentChannel] = pkt->ch_4_eq_3_q;
                valueChanged = YES;
            }
            if (peq4G[currentChannel] != pkt->ch_4_eq_4_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH4_EQ4_DB dspId:DSP_1]) {
                peq4G[currentChannel] = pkt->ch_4_eq_4_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq4F[currentChannel] != pkt->ch_4_eq_4_freq && ![viewController sendCommandExists:CMD_CH4_EQ4_FREQ dspId:DSP_1]) {
                peq4F[currentChannel] = pkt->ch_4_eq_4_freq;
                valueChanged = YES;
            }
            if (peq4Q[currentChannel] != pkt->ch_4_eq_4_q && ![viewController sendCommandExists:CMD_CH4_EQ4_Q dspId:DSP_1]) {
                peq4Q[currentChannel] = pkt->ch_4_eq_4_q;
                [self updatePeq4Filter];
                valueChanged = YES;
            }
            meterIn.progress = ((float)pkt->ch_4_meter_eq_in/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterOut.progress = ((float)pkt->ch_4_meter_eq_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            if (!sliderLock) {
                slider.value = pkt->ch_4_fader;
                [self onSliderAction:nil];
            }
            BOOL sliderOn = (pkt->ch_on_off & 0x08) >> 3;
            if (sliderOn != btnOn.selected && ![viewController sendCommandExists:CMD_CH_ON_OFF dspId:DSP_5]) {
                btnOn.selected = sliderOn;
            }
            BOOL sliderSolo = (pkt->solo_ctrl & 0x08) >> 3;
            if (sliderSolo != btnSolo.selected && ![viewController sendCommandExists:CMD_SOLO_CTRL dspId:DSP_5]) {
                btnSolo.selected = sliderSolo;
            }
            BOOL sliderSoloSafe = (pkt->solo_safe_ctrl & 0x08) >> 3;
            if (sliderSoloSafe) {
                [btnSolo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
                [btnSolo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
            } else {
                [btnSolo setTitle:@"SOLO" forState:UIControlStateNormal];
                [btnSolo setTitle:@"SOLO" forState:UIControlStateSelected];
            }
            BOOL sliderMeter = (pkt->ch_meter_pre_post & 0x08) >> 3;
            if (sliderMeter != btnMeterPrePost.selected && ![viewController sendCommandExists:CMD_CH_METER dspId:DSP_5]) {
                btnMeterPrePost.selected = sliderMeter;
            }
            int peqEnableValue = pkt->ch_4_ctrl;
            if (btnPeq1.selected != ((peqEnableValue & 0x08) >> 3) && ![viewController sendCommandExists:CMD_CH4_CTRL dspId:DSP_1]) {
                btnPeq1.selected = peq1Enabled[currentChannel] = (peqEnableValue & 0x08) >> 3;
                valueChanged = YES;
            }
            if (btnPeq2.selected != ((peqEnableValue & 0x10) >> 4) && ![viewController sendCommandExists:CMD_CH4_CTRL dspId:DSP_1]) {
                btnPeq2.selected = peq2Enabled[currentChannel] = (peqEnableValue & 0x10) >> 4;
                valueChanged = YES;
            }
            if (btnPeq3.selected != ((peqEnableValue & 0x20) >> 5) && ![viewController sendCommandExists:CMD_CH4_CTRL dspId:DSP_1]) {
                btnPeq3.selected = peq3Enabled[currentChannel] = (peqEnableValue & 0x20) >> 5;
                valueChanged = YES;
            }
            if (btnPeq4.selected != ((peqEnableValue & 0x40) >> 6) && ![viewController sendCommandExists:CMD_CH4_CTRL dspId:DSP_1]) {
                btnPeq4.selected = peq4Enabled[currentChannel] = (peqEnableValue & 0x40) >> 6;
                valueChanged = YES;
            }
        }
            break;
        case CHANNEL_CH_5:
        {
            if (peq1G[currentChannel] != pkt->ch_5_eq_1_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH1_EQ1_DB dspId:DSP_2]) {
                peq1G[currentChannel] = pkt->ch_5_eq_1_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq1F[currentChannel] != pkt->ch_5_eq_1_freq && ![viewController sendCommandExists:CMD_CH1_EQ1_FREQ dspId:DSP_2]) {
                peq1F[currentChannel] = pkt->ch_5_eq_1_freq;
                valueChanged = YES;
            }
            if (peq1Q[currentChannel] != pkt->ch_5_eq_1_q && ![viewController sendCommandExists:CMD_CH1_EQ1_Q dspId:DSP_2]) {
                peq1Q[currentChannel] = pkt->ch_5_eq_1_q;
                [self updatePeq1Filter];
                valueChanged = YES;
            }
            if (peq2G[currentChannel] != pkt->ch_5_eq_2_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH1_EQ2_DB dspId:DSP_2]) {
                peq2G[currentChannel] = pkt->ch_5_eq_2_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq2F[currentChannel] != pkt->ch_5_eq_2_freq && ![viewController sendCommandExists:CMD_CH1_EQ2_FREQ dspId:DSP_2]) {
                peq2F[currentChannel] = pkt->ch_5_eq_2_freq;
                valueChanged = YES;
            }
            if (peq2Q[currentChannel] != pkt->ch_5_eq_2_q && ![viewController sendCommandExists:CMD_CH1_EQ2_Q dspId:DSP_2]) {
                peq2Q[currentChannel] = pkt->ch_5_eq_2_q;
                valueChanged = YES;
            }
            if (peq3G[currentChannel] != pkt->ch_5_eq_3_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH1_EQ3_DB dspId:DSP_2]) {
                peq3G[currentChannel] = pkt->ch_5_eq_3_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq3F[currentChannel] != pkt->ch_5_eq_3_freq && ![viewController sendCommandExists:CMD_CH1_EQ3_FREQ dspId:DSP_2]) {
                peq3F[currentChannel] = pkt->ch_5_eq_3_freq;
                valueChanged = YES;
            }
            if (peq3Q[currentChannel] != pkt->ch_5_eq_3_q && ![viewController sendCommandExists:CMD_CH1_EQ3_Q dspId:DSP_2]) {
                peq3Q[currentChannel] = pkt->ch_5_eq_3_q;
                valueChanged = YES;
            }
            if (peq4G[currentChannel] != pkt->ch_5_eq_4_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH1_EQ4_DB dspId:DSP_2]) {
                peq4G[currentChannel] = pkt->ch_5_eq_4_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq4F[currentChannel] != pkt->ch_5_eq_4_freq && ![viewController sendCommandExists:CMD_CH1_EQ4_FREQ dspId:DSP_2]) {
                peq4F[currentChannel] = pkt->ch_5_eq_4_freq;
                valueChanged = YES;
            }
            if (peq4Q[currentChannel] != pkt->ch_5_eq_4_q && ![viewController sendCommandExists:CMD_CH1_EQ4_Q dspId:DSP_2]) {
                peq4Q[currentChannel] = pkt->ch_5_eq_4_q;
                [self updatePeq4Filter];
                valueChanged = YES;
            }
            meterIn.progress = ((float)pkt->ch_5_meter_eq_in/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterOut.progress = ((float)pkt->ch_5_meter_eq_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            if (!sliderLock) {
                slider.value = pkt->ch_5_fader;
                [self onSliderAction:nil];
            }
            BOOL sliderOn = (pkt->ch_on_off & 0x10) >> 4;
            if (sliderOn != btnOn.selected && ![viewController sendCommandExists:CMD_CH_ON_OFF dspId:DSP_5]) {
                btnOn.selected = sliderOn;
            }
            BOOL sliderSolo = (pkt->solo_ctrl & 0x10) >> 4;
            if (sliderSolo != btnSolo.selected && ![viewController sendCommandExists:CMD_SOLO_CTRL dspId:DSP_5]) {
                btnSolo.selected = sliderSolo;
            }
            BOOL sliderSoloSafe = (pkt->solo_safe_ctrl & 0x10) >> 4;
            if (sliderSoloSafe) {
                [btnSolo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
                [btnSolo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
            } else {
                [btnSolo setTitle:@"SOLO" forState:UIControlStateNormal];
                [btnSolo setTitle:@"SOLO" forState:UIControlStateSelected];
            }
            BOOL sliderMeter = (pkt->ch_meter_pre_post & 0x10) >> 4;
            if (sliderMeter != btnMeterPrePost.selected && ![viewController sendCommandExists:CMD_CH_METER dspId:DSP_5]) {
                btnMeterPrePost.selected = sliderMeter;
            }
            int peqEnableValue = pkt->ch_5_ctrl;
            if (btnPeq1.selected != ((peqEnableValue & 0x08) >> 3) && ![viewController sendCommandExists:CMD_CH1_CTRL dspId:DSP_2]) {
                btnPeq1.selected = peq1Enabled[currentChannel] = (peqEnableValue & 0x08) >> 3;
                valueChanged = YES;
            }
            if (btnPeq2.selected != ((peqEnableValue & 0x10) >> 4) && ![viewController sendCommandExists:CMD_CH1_CTRL dspId:DSP_2]) {
                btnPeq2.selected = peq2Enabled[currentChannel] = (peqEnableValue & 0x10) >> 4;
                valueChanged = YES;
            }
            if (btnPeq3.selected != ((peqEnableValue & 0x20) >> 5) && ![viewController sendCommandExists:CMD_CH1_CTRL dspId:DSP_2]) {
                btnPeq3.selected = peq3Enabled[currentChannel] = (peqEnableValue & 0x20) >> 5;
                valueChanged = YES;
            }
            if (btnPeq4.selected != ((peqEnableValue & 0x40) >> 6) && ![viewController sendCommandExists:CMD_CH1_CTRL dspId:DSP_2]) {
                btnPeq4.selected = peq4Enabled[currentChannel] = (peqEnableValue & 0x40) >> 6;
                valueChanged = YES;
            }
        }
            break;
        case CHANNEL_CH_6:
        {
            if (peq1G[currentChannel] != pkt->ch_6_eq_1_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH2_EQ1_DB dspId:DSP_2]) {
                peq1G[currentChannel] = pkt->ch_6_eq_1_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq1F[currentChannel] != pkt->ch_6_eq_1_freq && ![viewController sendCommandExists:CMD_CH2_EQ1_FREQ dspId:DSP_2]) {
                peq1F[currentChannel] = pkt->ch_6_eq_1_freq;
                valueChanged = YES;
            }
            if (peq1Q[currentChannel] != pkt->ch_6_eq_1_q && ![viewController sendCommandExists:CMD_CH2_EQ1_Q dspId:DSP_2]) {
                peq1Q[currentChannel] = pkt->ch_6_eq_1_q;
                [self updatePeq1Filter];
                valueChanged = YES;
            }
            if (peq2G[currentChannel] != pkt->ch_6_eq_2_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH2_EQ2_DB dspId:DSP_2]) {
                peq2G[currentChannel] = pkt->ch_6_eq_2_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq2F[currentChannel] != pkt->ch_6_eq_2_freq && ![viewController sendCommandExists:CMD_CH2_EQ2_FREQ dspId:DSP_2]) {
                peq2F[currentChannel] = pkt->ch_6_eq_2_freq;
                valueChanged = YES;
            }
            if (peq2Q[currentChannel] != pkt->ch_6_eq_2_q && ![viewController sendCommandExists:CMD_CH2_EQ2_Q dspId:DSP_2]) {
                peq2Q[currentChannel] = pkt->ch_6_eq_2_q;
                valueChanged = YES;
            }
            if (peq3G[currentChannel] != pkt->ch_6_eq_3_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH2_EQ3_DB dspId:DSP_2]) {
                peq3G[currentChannel] = pkt->ch_6_eq_3_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq3F[currentChannel] != pkt->ch_6_eq_3_freq && ![viewController sendCommandExists:CMD_CH2_EQ3_FREQ dspId:DSP_2]) {
                peq3F[currentChannel] = pkt->ch_6_eq_3_freq;
                valueChanged = YES;
            }
            if (peq3Q[currentChannel] != pkt->ch_6_eq_3_q && ![viewController sendCommandExists:CMD_CH2_EQ3_Q dspId:DSP_2]) {
                peq3Q[currentChannel] = pkt->ch_6_eq_3_q;
                valueChanged = YES;
            }
            if (peq4G[currentChannel] != pkt->ch_6_eq_4_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH2_EQ4_DB dspId:DSP_2]) {
                peq4G[currentChannel] = pkt->ch_6_eq_4_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq4F[currentChannel] != pkt->ch_6_eq_4_freq && ![viewController sendCommandExists:CMD_CH2_EQ4_FREQ dspId:DSP_2]) {
                peq4F[currentChannel] = pkt->ch_6_eq_4_freq;
                valueChanged = YES;
            }
            if (peq4Q[currentChannel] != pkt->ch_6_eq_4_q && ![viewController sendCommandExists:CMD_CH2_EQ4_Q dspId:DSP_2]) {
                peq4Q[currentChannel] = pkt->ch_6_eq_4_q;
                [self updatePeq4Filter];
                valueChanged = YES;
            }
            meterIn.progress = ((float)pkt->ch_6_meter_eq_in/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterOut.progress = ((float)pkt->ch_6_meter_eq_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            if (!sliderLock) {
                slider.value = pkt->ch_6_fader;
                [self onSliderAction:nil];
            }
            BOOL sliderOn = (pkt->ch_on_off & 0x20) >> 5;
            if (sliderOn != btnOn.selected && ![viewController sendCommandExists:CMD_CH_ON_OFF dspId:DSP_5]) {
                btnOn.selected = sliderOn;
            }
            BOOL sliderSolo = (pkt->solo_ctrl & 0x20) >> 5;
            if (sliderSolo != btnSolo.selected && ![viewController sendCommandExists:CMD_SOLO_CTRL dspId:DSP_5]) {
                btnSolo.selected = sliderSolo;
            }
            BOOL sliderSoloSafe = (pkt->solo_safe_ctrl & 0x20) >> 5;
            if (sliderSoloSafe) {
                [btnSolo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
                [btnSolo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
            } else {
                [btnSolo setTitle:@"SOLO" forState:UIControlStateNormal];
                [btnSolo setTitle:@"SOLO" forState:UIControlStateSelected];
            }
            BOOL sliderMeter = (pkt->ch_meter_pre_post & 0x20) >> 5;
            if (sliderMeter != btnMeterPrePost.selected && ![viewController sendCommandExists:CMD_CH_METER dspId:DSP_5]) {
                btnMeterPrePost.selected = sliderMeter;
            }
            int peqEnableValue = pkt->ch_6_ctrl;
            if (btnPeq1.selected != ((peqEnableValue & 0x08) >> 3) && ![viewController sendCommandExists:CMD_CH2_CTRL dspId:DSP_2]) {
                btnPeq1.selected = peq1Enabled[currentChannel] = (peqEnableValue & 0x08) >> 3;
                valueChanged = YES;
            }
            if (btnPeq2.selected != ((peqEnableValue & 0x10) >> 4) && ![viewController sendCommandExists:CMD_CH2_CTRL dspId:DSP_2]) {
                btnPeq2.selected = peq2Enabled[currentChannel] = (peqEnableValue & 0x10) >> 4;
                valueChanged = YES;
            }
            if (btnPeq3.selected != ((peqEnableValue & 0x20) >> 5) && ![viewController sendCommandExists:CMD_CH2_CTRL dspId:DSP_2]) {
                btnPeq3.selected = peq3Enabled[currentChannel] = (peqEnableValue & 0x20) >> 5;
                valueChanged = YES;
            }
            if (btnPeq4.selected != ((peqEnableValue & 0x40) >> 6) && ![viewController sendCommandExists:CMD_CH2_CTRL dspId:DSP_2]) {
                btnPeq4.selected = peq4Enabled[currentChannel] = (peqEnableValue & 0x40) >> 6;
                valueChanged = YES;
            }
        }
            break;
        case CHANNEL_CH_7:
        {
            if (peq1G[currentChannel] != pkt->ch_7_eq_1_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH3_EQ1_DB dspId:DSP_2]) {
                peq1G[currentChannel] = pkt->ch_7_eq_1_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq1F[currentChannel] != pkt->ch_7_eq_1_freq && ![viewController sendCommandExists:CMD_CH3_EQ1_FREQ dspId:DSP_2]) {
                peq1F[currentChannel] = pkt->ch_7_eq_1_freq;
                valueChanged = YES;
            }
            if (peq1Q[currentChannel] != pkt->ch_7_eq_1_q && ![viewController sendCommandExists:CMD_CH3_EQ1_Q dspId:DSP_2]) {
                peq1Q[currentChannel] = pkt->ch_7_eq_1_q;
                [self updatePeq1Filter];
                valueChanged = YES;
            }
            if (peq2G[currentChannel] != pkt->ch_7_eq_2_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH3_EQ2_DB dspId:DSP_2]) {
                peq2G[currentChannel] = pkt->ch_7_eq_2_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq2F[currentChannel] != pkt->ch_7_eq_2_freq && ![viewController sendCommandExists:CMD_CH3_EQ2_FREQ dspId:DSP_2]) {
                peq2F[currentChannel] = pkt->ch_7_eq_2_freq;
                valueChanged = YES;
            }
            if (peq2Q[currentChannel] != pkt->ch_7_eq_2_q && ![viewController sendCommandExists:CMD_CH3_EQ2_Q dspId:DSP_2]) {
                peq2Q[currentChannel] = pkt->ch_7_eq_2_q;
                valueChanged = YES;
            }
            if (peq3G[currentChannel] != pkt->ch_7_eq_3_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH3_EQ3_DB dspId:DSP_2]) {
                peq3G[currentChannel] = pkt->ch_7_eq_3_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq3F[currentChannel] != pkt->ch_7_eq_3_freq && ![viewController sendCommandExists:CMD_CH3_EQ3_FREQ dspId:DSP_2]) {
                peq3F[currentChannel] = pkt->ch_7_eq_3_freq;
                valueChanged = YES;
            }
            if (peq3Q[currentChannel] != pkt->ch_7_eq_3_q && ![viewController sendCommandExists:CMD_CH3_EQ3_Q dspId:DSP_2]) {
                peq3Q[currentChannel] = pkt->ch_7_eq_3_q;
                valueChanged = YES;
            }
            if (peq4G[currentChannel] != pkt->ch_7_eq_4_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH3_EQ4_DB dspId:DSP_2]) {
                peq4G[currentChannel] = pkt->ch_7_eq_4_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq4F[currentChannel] != pkt->ch_7_eq_4_freq && ![viewController sendCommandExists:CMD_CH3_EQ4_FREQ dspId:DSP_2]) {
                peq4F[currentChannel] = pkt->ch_7_eq_4_freq;
                valueChanged = YES;
            }
            if (peq4Q[currentChannel] != pkt->ch_7_eq_4_q && ![viewController sendCommandExists:CMD_CH3_EQ4_Q dspId:DSP_2]) {
                peq4Q[currentChannel] = pkt->ch_7_eq_4_q;
                [self updatePeq4Filter];
                valueChanged = YES;
            }
            meterIn.progress = ((float)pkt->ch_7_meter_eq_in/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterOut.progress = ((float)pkt->ch_7_meter_eq_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            if (!sliderLock) {
                slider.value = pkt->ch_7_fader;
                [self onSliderAction:nil];
            }
            BOOL sliderOn = (pkt->ch_on_off & 0x40) >> 6;
            if (sliderOn != btnOn.selected && ![viewController sendCommandExists:CMD_CH_ON_OFF dspId:DSP_5]) {
                btnOn.selected = sliderOn;
            }
            BOOL sliderSolo = (pkt->solo_ctrl & 0x40) >> 6;
            if (sliderSolo != btnSolo.selected && ![viewController sendCommandExists:CMD_SOLO_CTRL dspId:DSP_5]) {
                btnSolo.selected = sliderSolo;
            }
            BOOL sliderSoloSafe = (pkt->solo_safe_ctrl & 0x40) >> 6;
            if (sliderSoloSafe) {
                [btnSolo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
                [btnSolo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
            } else {
                [btnSolo setTitle:@"SOLO" forState:UIControlStateNormal];
                [btnSolo setTitle:@"SOLO" forState:UIControlStateSelected];
            }
            BOOL sliderMeter = (pkt->ch_meter_pre_post & 0x40) >> 6;
            if (sliderMeter != btnMeterPrePost.selected && ![viewController sendCommandExists:CMD_CH_METER dspId:DSP_5]) {
                btnMeterPrePost.selected = sliderMeter;
            }
            int peqEnableValue = pkt->ch_7_ctrl;
            if (btnPeq1.selected != ((peqEnableValue & 0x08) >> 3) && ![viewController sendCommandExists:CMD_CH3_CTRL dspId:DSP_2]) {
                btnPeq1.selected = peq1Enabled[currentChannel] = (peqEnableValue & 0x08) >> 3;
                valueChanged = YES;
            }
            if (btnPeq2.selected != ((peqEnableValue & 0x10) >> 4) && ![viewController sendCommandExists:CMD_CH3_CTRL dspId:DSP_2]) {
                btnPeq2.selected = peq2Enabled[currentChannel] = (peqEnableValue & 0x10) >> 4;
                valueChanged = YES;
            }
            if (btnPeq3.selected != ((peqEnableValue & 0x20) >> 5) && ![viewController sendCommandExists:CMD_CH3_CTRL dspId:DSP_2]) {
                btnPeq3.selected = peq3Enabled[currentChannel] = (peqEnableValue & 0x20) >> 5;
                valueChanged = YES;
            }
            if (btnPeq4.selected != ((peqEnableValue & 0x40) >> 6) && ![viewController sendCommandExists:CMD_CH3_CTRL dspId:DSP_2]) {
                btnPeq4.selected = peq4Enabled[currentChannel] = (peqEnableValue & 0x40) >> 6;
                valueChanged = YES;
            }
        }
            break;
        case CHANNEL_CH_8:
        {
            if (peq1G[currentChannel] != pkt->ch_8_eq_1_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH4_EQ1_DB dspId:DSP_2]) {
                peq1G[currentChannel] = pkt->ch_8_eq_1_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq1F[currentChannel] != pkt->ch_8_eq_1_freq && ![viewController sendCommandExists:CMD_CH4_EQ1_FREQ dspId:DSP_2]) {
                peq1F[currentChannel] = pkt->ch_8_eq_1_freq;
                valueChanged = YES;
            }
            if (peq1Q[currentChannel] != pkt->ch_8_eq_1_q && ![viewController sendCommandExists:CMD_CH4_EQ1_Q dspId:DSP_2]) {
                peq1Q[currentChannel] = pkt->ch_8_eq_1_q;
                [self updatePeq1Filter];
                valueChanged = YES;
            }
            if (peq2G[currentChannel] != pkt->ch_8_eq_2_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH4_EQ2_DB dspId:DSP_2]) {
                peq2G[currentChannel] = pkt->ch_8_eq_2_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq2F[currentChannel] != pkt->ch_8_eq_2_freq && ![viewController sendCommandExists:CMD_CH4_EQ2_FREQ dspId:DSP_2]) {
                peq2F[currentChannel] = pkt->ch_8_eq_2_freq;
                valueChanged = YES;
            }
            if (peq2Q[currentChannel] != pkt->ch_8_eq_2_q && ![viewController sendCommandExists:CMD_CH4_EQ2_Q dspId:DSP_2]) {
                peq2Q[currentChannel] = pkt->ch_8_eq_2_q;
                valueChanged = YES;
            }
            if (peq3G[currentChannel] != pkt->ch_8_eq_3_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH4_EQ3_DB dspId:DSP_2]) {
                peq3G[currentChannel] = pkt->ch_8_eq_3_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq3F[currentChannel] != pkt->ch_8_eq_3_freq && ![viewController sendCommandExists:CMD_CH4_EQ3_FREQ dspId:DSP_2]) {
                peq3F[currentChannel] = pkt->ch_8_eq_3_freq;
                valueChanged = YES;
            }
            if (peq3Q[currentChannel] != pkt->ch_8_eq_3_q && ![viewController sendCommandExists:CMD_CH4_EQ3_Q dspId:DSP_2]) {
                peq3Q[currentChannel] = pkt->ch_8_eq_3_q;
                valueChanged = YES;
            }
            if (peq4G[currentChannel] != pkt->ch_8_eq_4_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH4_EQ4_DB dspId:DSP_2]) {
                peq4G[currentChannel] = pkt->ch_8_eq_4_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq4F[currentChannel] != pkt->ch_8_eq_4_freq && ![viewController sendCommandExists:CMD_CH4_EQ4_FREQ dspId:DSP_2]) {
                peq4F[currentChannel] = pkt->ch_8_eq_4_freq;
                valueChanged = YES;
            }
            if (peq4Q[currentChannel] != pkt->ch_8_eq_4_q && ![viewController sendCommandExists:CMD_CH4_EQ4_Q dspId:DSP_2]) {
                peq4Q[currentChannel] = pkt->ch_8_eq_4_q;
                [self updatePeq4Filter];
                valueChanged = YES;
            }
            meterIn.progress = ((float)pkt->ch_8_meter_eq_in/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterOut.progress = ((float)pkt->ch_8_meter_eq_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            if (!sliderLock) {
                slider.value = pkt->ch_8_fader;
                [self onSliderAction:nil];
            }
            BOOL sliderOn = (pkt->ch_on_off & 0x80) >> 7;
            if (sliderOn != btnOn.selected && ![viewController sendCommandExists:CMD_CH_ON_OFF dspId:DSP_5]) {
                btnOn.selected = sliderOn;
            }
            BOOL sliderSolo = (pkt->solo_ctrl & 0x80) >> 7;
            if (sliderSolo != btnSolo.selected && ![viewController sendCommandExists:CMD_SOLO_CTRL dspId:DSP_5]) {
                btnSolo.selected = sliderSolo;
            }
            BOOL sliderSoloSafe = (pkt->solo_safe_ctrl & 0x80) >> 7;
            if (sliderSoloSafe) {
                [btnSolo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
                [btnSolo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
            } else {
                [btnSolo setTitle:@"SOLO" forState:UIControlStateNormal];
                [btnSolo setTitle:@"SOLO" forState:UIControlStateSelected];
            }
            BOOL sliderMeter = (pkt->ch_meter_pre_post & 0x80) >> 7;
            if (sliderMeter != btnMeterPrePost.selected && ![viewController sendCommandExists:CMD_CH_METER dspId:DSP_5]) {
                btnMeterPrePost.selected = sliderMeter;
            }
            int peqEnableValue = pkt->ch_8_ctrl;
            if (btnPeq1.selected != ((peqEnableValue & 0x08) >> 3) && ![viewController sendCommandExists:CMD_CH4_CTRL dspId:DSP_2]) {
                btnPeq1.selected = peq1Enabled[currentChannel] = (peqEnableValue & 0x08) >> 3;
                valueChanged = YES;
            }
            if (btnPeq2.selected != ((peqEnableValue & 0x10) >> 4) && ![viewController sendCommandExists:CMD_CH4_CTRL dspId:DSP_2]) {
                btnPeq2.selected = peq2Enabled[currentChannel] = (peqEnableValue & 0x10) >> 4;
                valueChanged = YES;
            }
            if (btnPeq3.selected != ((peqEnableValue & 0x20) >> 5) && ![viewController sendCommandExists:CMD_CH4_CTRL dspId:DSP_2]) {
                btnPeq3.selected = peq3Enabled[currentChannel] = (peqEnableValue & 0x20) >> 5;
                valueChanged = YES;
            }
            if (btnPeq4.selected != ((peqEnableValue & 0x40) >> 6) && ![viewController sendCommandExists:CMD_CH4_CTRL dspId:DSP_2]) {
                btnPeq4.selected = peq4Enabled[currentChannel] = (peqEnableValue & 0x40) >> 6;
                valueChanged = YES;
            }
        }
            break;
        case CHANNEL_CH_9:
        {
            if (peq1G[currentChannel] != pkt->ch_9_eq_1_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH1_EQ1_DB dspId:DSP_3]) {
                peq1G[currentChannel] = pkt->ch_9_eq_1_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq1F[currentChannel] != pkt->ch_9_eq_1_freq && ![viewController sendCommandExists:CMD_CH1_EQ1_FREQ dspId:DSP_3]) {
                peq1F[currentChannel] = pkt->ch_9_eq_1_freq;
                valueChanged = YES;
            }
            if (peq1Q[currentChannel] != pkt->ch_9_eq_1_q && ![viewController sendCommandExists:CMD_CH1_EQ1_Q dspId:DSP_3]) {
                peq1Q[currentChannel] = pkt->ch_9_eq_1_q;
                [self updatePeq1Filter];
                valueChanged = YES;
            }
            if (peq2G[currentChannel] != pkt->ch_9_eq_2_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH1_EQ2_DB dspId:DSP_3]) {
                peq2G[currentChannel] = pkt->ch_9_eq_2_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq2F[currentChannel] != pkt->ch_9_eq_2_freq && ![viewController sendCommandExists:CMD_CH1_EQ2_FREQ dspId:DSP_3]) {
                peq2F[currentChannel] = pkt->ch_9_eq_2_freq;
                valueChanged = YES;
            }
            if (peq2Q[currentChannel] != pkt->ch_9_eq_2_q && ![viewController sendCommandExists:CMD_CH1_EQ2_Q dspId:DSP_3]) {
                peq2Q[currentChannel] = pkt->ch_9_eq_2_q;
                valueChanged = YES;
            }
            if (peq3G[currentChannel] != pkt->ch_9_eq_3_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH1_EQ3_DB dspId:DSP_3]) {
                peq3G[currentChannel] = pkt->ch_9_eq_3_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq3F[currentChannel] != pkt->ch_9_eq_3_freq && ![viewController sendCommandExists:CMD_CH1_EQ3_FREQ dspId:DSP_3]) {
                peq3F[currentChannel] = pkt->ch_9_eq_3_freq;
                valueChanged = YES;
            }
            if (peq3Q[currentChannel] != pkt->ch_9_eq_3_q && ![viewController sendCommandExists:CMD_CH1_EQ3_Q dspId:DSP_3]) {
                peq3Q[currentChannel] = pkt->ch_9_eq_3_q;
                valueChanged = YES;
            }
            if (peq4G[currentChannel] != pkt->ch_9_eq_4_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH1_EQ4_DB dspId:DSP_3]) {
                peq4G[currentChannel] = pkt->ch_9_eq_4_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq4F[currentChannel] != pkt->ch_9_eq_4_freq && ![viewController sendCommandExists:CMD_CH1_EQ4_FREQ dspId:DSP_3]) {
                peq4F[currentChannel] = pkt->ch_9_eq_4_freq;
                valueChanged = YES;
            }
            if (peq4Q[currentChannel] != pkt->ch_9_eq_4_q && ![viewController sendCommandExists:CMD_CH1_EQ4_Q dspId:DSP_3]) {
                peq4Q[currentChannel] = pkt->ch_9_eq_4_q;
                [self updatePeq4Filter];
                valueChanged = YES;
            }
            meterIn.progress = ((float)pkt->ch_9_meter_eq_in/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterOut.progress = ((float)pkt->ch_9_meter_eq_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            if (!sliderLock) {
                slider.value = pkt->ch_9_fader;
                [self onSliderAction:nil];
            }
            BOOL sliderOn = (pkt->ch_on_off & 0x0100) >> 8;
            if (sliderOn != btnOn.selected && ![viewController sendCommandExists:CMD_CH_ON_OFF dspId:DSP_5]) {
                btnOn.selected = sliderOn;
            }
            BOOL sliderSolo = (pkt->solo_ctrl & 0x0100) >> 8;
            if (sliderSolo != btnSolo.selected && ![viewController sendCommandExists:CMD_SOLO_CTRL dspId:DSP_5]) {
                btnSolo.selected = sliderSolo;
            }
            BOOL sliderSoloSafe = (pkt->solo_safe_ctrl & 0x0100) >> 8;
            if (sliderSoloSafe) {
                [btnSolo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
                [btnSolo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
            } else {
                [btnSolo setTitle:@"SOLO" forState:UIControlStateNormal];
                [btnSolo setTitle:@"SOLO" forState:UIControlStateSelected];
            }
            BOOL sliderMeter = (pkt->ch_meter_pre_post & 0x0100) >> 8;
            if (sliderMeter != btnMeterPrePost.selected && ![viewController sendCommandExists:CMD_CH_METER dspId:DSP_5]) {
                btnMeterPrePost.selected = sliderMeter;
            }
            int peqEnableValue = pkt->ch_9_ctrl;
            if (btnPeq1.selected != ((peqEnableValue & 0x08) >> 3) && ![viewController sendCommandExists:CMD_CH1_CTRL dspId:DSP_3]) {
                btnPeq1.selected = peq1Enabled[currentChannel] = (peqEnableValue & 0x08) >> 3;
                valueChanged = YES;
            }
            if (btnPeq2.selected != ((peqEnableValue & 0x10) >> 4) && ![viewController sendCommandExists:CMD_CH1_CTRL dspId:DSP_3]) {
                btnPeq2.selected = peq2Enabled[currentChannel] = (peqEnableValue & 0x10) >> 4;
                valueChanged = YES;
            }
            if (btnPeq3.selected != ((peqEnableValue & 0x20) >> 5) && ![viewController sendCommandExists:CMD_CH1_CTRL dspId:DSP_3]) {
                btnPeq3.selected = peq3Enabled[currentChannel] = (peqEnableValue & 0x20) >> 5;
                valueChanged = YES;
            }
            if (btnPeq4.selected != ((peqEnableValue & 0x40) >> 6) && ![viewController sendCommandExists:CMD_CH1_CTRL dspId:DSP_3]) {
                btnPeq4.selected = peq4Enabled[currentChannel] = (peqEnableValue & 0x40) >> 6;
                valueChanged = YES;
            }
        }
            break;
        case CHANNEL_CH_10:
        {
            if (peq1G[currentChannel] != pkt->ch_10_eq_1_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH2_EQ1_DB dspId:DSP_3]) {
                peq1G[currentChannel] = pkt->ch_10_eq_1_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq1F[currentChannel] != pkt->ch_10_eq_1_freq && ![viewController sendCommandExists:CMD_CH2_EQ1_FREQ dspId:DSP_3]) {
                peq1F[currentChannel] = pkt->ch_10_eq_1_freq;
                valueChanged = YES;
            }
            if (peq1Q[currentChannel] != pkt->ch_10_eq_1_q && ![viewController sendCommandExists:CMD_CH2_EQ1_Q dspId:DSP_3]) {
                peq1Q[currentChannel] = pkt->ch_10_eq_1_q;
                [self updatePeq1Filter];
                valueChanged = YES;
            }
            if (peq2G[currentChannel] != pkt->ch_10_eq_2_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH2_EQ2_DB dspId:DSP_3]) {
                peq2G[currentChannel] = pkt->ch_10_eq_2_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq2F[currentChannel] != pkt->ch_10_eq_2_freq && ![viewController sendCommandExists:CMD_CH2_EQ2_FREQ dspId:DSP_3]) {
                peq2F[currentChannel] = pkt->ch_10_eq_2_freq;
                valueChanged = YES;
            }
            if (peq2Q[currentChannel] != pkt->ch_10_eq_2_q && ![viewController sendCommandExists:CMD_CH2_EQ2_Q dspId:DSP_3]) {
                peq2Q[currentChannel] = pkt->ch_10_eq_2_q;
                valueChanged = YES;
            }
            if (peq3G[currentChannel] != pkt->ch_10_eq_3_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH2_EQ3_DB dspId:DSP_3]) {
                peq3G[currentChannel] = pkt->ch_10_eq_3_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq3F[currentChannel] != pkt->ch_10_eq_3_freq && ![viewController sendCommandExists:CMD_CH2_EQ3_FREQ dspId:DSP_3]) {
                peq3F[currentChannel] = pkt->ch_10_eq_3_freq;
                valueChanged = YES;
            }
            if (peq3Q[currentChannel] != pkt->ch_10_eq_3_q && ![viewController sendCommandExists:CMD_CH2_EQ3_Q dspId:DSP_3]) {
                peq3Q[currentChannel] = pkt->ch_10_eq_3_q;
                valueChanged = YES;
            }
            if (peq4G[currentChannel] != pkt->ch_10_eq_4_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH2_EQ4_DB dspId:DSP_3]) {
                peq4G[currentChannel] = pkt->ch_10_eq_4_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq4F[currentChannel] != pkt->ch_10_eq_4_freq && ![viewController sendCommandExists:CMD_CH2_EQ4_FREQ dspId:DSP_3]) {
                peq4F[currentChannel] = pkt->ch_10_eq_4_freq;
                valueChanged = YES;
            }
            if (peq4Q[currentChannel] != pkt->ch_10_eq_4_q && ![viewController sendCommandExists:CMD_CH2_EQ4_Q dspId:DSP_3]) {
                peq4Q[currentChannel] = pkt->ch_10_eq_4_q;
                [self updatePeq4Filter];
                valueChanged = YES;
            }
            meterIn.progress = ((float)pkt->ch_10_meter_eq_in/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterOut.progress = ((float)pkt->ch_10_meter_eq_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            if (!sliderLock) {
                slider.value = pkt->ch_10_fader;
                [self onSliderAction:nil];
            }
            BOOL sliderOn = (pkt->ch_on_off & 0x0200) >> 9;
            if (sliderOn != btnOn.selected && ![viewController sendCommandExists:CMD_CH_ON_OFF dspId:DSP_5]) {
                btnOn.selected = sliderOn;
            }
            BOOL sliderSolo = (pkt->solo_ctrl & 0x0200) >> 9;
            if (sliderSolo != btnSolo.selected && ![viewController sendCommandExists:CMD_SOLO_CTRL dspId:DSP_5]) {
                btnSolo.selected = sliderSolo;
            }
            BOOL sliderSoloSafe = (pkt->solo_safe_ctrl & 0x0200) >> 9;
            if (sliderSoloSafe) {
                [btnSolo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
                [btnSolo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
            } else {
                [btnSolo setTitle:@"SOLO" forState:UIControlStateNormal];
                [btnSolo setTitle:@"SOLO" forState:UIControlStateSelected];
            }
            BOOL sliderMeter = (pkt->ch_meter_pre_post & 0x0200) >> 9;
            if (sliderMeter != btnMeterPrePost.selected && ![viewController sendCommandExists:CMD_CH_METER dspId:DSP_5]) {
                btnMeterPrePost.selected = sliderMeter;
            }
            int peqEnableValue = pkt->ch_10_ctrl;
            if (btnPeq1.selected != ((peqEnableValue & 0x08) >> 3) && ![viewController sendCommandExists:CMD_CH2_CTRL dspId:DSP_3]) {
                btnPeq1.selected = peq1Enabled[currentChannel] = (peqEnableValue & 0x08) >> 3;
                valueChanged = YES;
            }
            if (btnPeq2.selected != ((peqEnableValue & 0x10) >> 4) && ![viewController sendCommandExists:CMD_CH2_CTRL dspId:DSP_3]) {
                btnPeq2.selected = peq2Enabled[currentChannel] = (peqEnableValue & 0x10) >> 4;
                valueChanged = YES;
            }
            if (btnPeq3.selected != ((peqEnableValue & 0x20) >> 5) && ![viewController sendCommandExists:CMD_CH2_CTRL dspId:DSP_3]) {
                btnPeq3.selected = peq3Enabled[currentChannel] = (peqEnableValue & 0x20) >> 5;
                valueChanged = YES;
            }
            if (btnPeq4.selected != ((peqEnableValue & 0x40) >> 6) && ![viewController sendCommandExists:CMD_CH2_CTRL dspId:DSP_3]) {
                btnPeq4.selected = peq4Enabled[currentChannel] = (peqEnableValue & 0x40) >> 6;
                valueChanged = YES;
            }
        }
            break;
        case CHANNEL_CH_11:
        {
            if (peq1G[currentChannel] != pkt->ch_11_eq_1_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH3_EQ1_DB dspId:DSP_3]) {
                peq1G[currentChannel] = pkt->ch_11_eq_1_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq1F[currentChannel] != pkt->ch_11_eq_1_freq && ![viewController sendCommandExists:CMD_CH3_EQ1_FREQ dspId:DSP_3]) {
                peq1F[currentChannel] = pkt->ch_11_eq_1_freq;
                valueChanged = YES;
            }
            if (peq1Q[currentChannel] != pkt->ch_11_eq_1_q && ![viewController sendCommandExists:CMD_CH3_EQ1_Q dspId:DSP_3]) {
                peq1Q[currentChannel] = pkt->ch_11_eq_1_q;
                [self updatePeq1Filter];
                valueChanged = YES;
            }
            if (peq2G[currentChannel] != pkt->ch_11_eq_2_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH3_EQ2_DB dspId:DSP_3]) {
                peq2G[currentChannel] = pkt->ch_11_eq_2_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq2F[currentChannel] != pkt->ch_11_eq_2_freq && ![viewController sendCommandExists:CMD_CH3_EQ2_FREQ dspId:DSP_3]) {
                peq2F[currentChannel] = pkt->ch_11_eq_2_freq;
                valueChanged = YES;
            }
            if (peq2Q[currentChannel] != pkt->ch_11_eq_2_q && ![viewController sendCommandExists:CMD_CH3_EQ2_Q dspId:DSP_3]) {
                peq2Q[currentChannel] = pkt->ch_11_eq_2_q;
                valueChanged = YES;
            }
            if (peq3G[currentChannel] != pkt->ch_11_eq_3_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH3_EQ3_DB dspId:DSP_3]) {
                peq3G[currentChannel] = pkt->ch_11_eq_3_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq3F[currentChannel] != pkt->ch_11_eq_3_freq && ![viewController sendCommandExists:CMD_CH3_EQ3_FREQ dspId:DSP_3]) {
                peq3F[currentChannel] = pkt->ch_11_eq_3_freq;
                valueChanged = YES;
            }
            if (peq3Q[currentChannel] != pkt->ch_11_eq_3_q && ![viewController sendCommandExists:CMD_CH3_EQ3_Q dspId:DSP_3]) {
                peq3Q[currentChannel] = pkt->ch_11_eq_3_q;
                valueChanged = YES;
            }
            if (peq4G[currentChannel] != pkt->ch_11_eq_4_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH3_EQ4_DB dspId:DSP_3]) {
                peq4G[currentChannel] = pkt->ch_11_eq_4_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq4F[currentChannel] != pkt->ch_11_eq_4_freq && ![viewController sendCommandExists:CMD_CH3_EQ4_FREQ dspId:DSP_3]) {
                peq4F[currentChannel] = pkt->ch_11_eq_4_freq;
                valueChanged = YES;
            }
            if (peq4Q[currentChannel] != pkt->ch_11_eq_4_q && ![viewController sendCommandExists:CMD_CH3_EQ4_Q dspId:DSP_3]) {
                peq4Q[currentChannel] = pkt->ch_11_eq_4_q;
                [self updatePeq4Filter];
                valueChanged = YES;
            }
            meterIn.progress = ((float)pkt->ch_11_meter_eq_in/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterOut.progress = ((float)pkt->ch_11_meter_eq_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            if (!sliderLock) {
                slider.value = pkt->ch_11_fader;
                [self onSliderAction:nil];
            }
            BOOL sliderOn = (pkt->ch_on_off & 0x0400) >> 10;
            if (sliderOn != btnOn.selected && ![viewController sendCommandExists:CMD_CH_ON_OFF dspId:DSP_5]) {
                btnOn.selected = sliderOn;
            }
            BOOL sliderSolo = (pkt->solo_ctrl & 0x0400) >> 10;
            if (sliderSolo != btnSolo.selected && ![viewController sendCommandExists:CMD_SOLO_CTRL dspId:DSP_5]) {
                btnSolo.selected = sliderSolo;
            }
            BOOL sliderSoloSafe = (pkt->solo_safe_ctrl & 0x0400) >> 10;
            if (sliderSoloSafe) {
                [btnSolo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
                [btnSolo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
            } else {
                [btnSolo setTitle:@"SOLO" forState:UIControlStateNormal];
                [btnSolo setTitle:@"SOLO" forState:UIControlStateSelected];
            }
            BOOL sliderMeter = (pkt->ch_meter_pre_post & 0x0400) >> 10;
            if (sliderMeter != btnMeterPrePost.selected && ![viewController sendCommandExists:CMD_CH_METER dspId:DSP_5]) {
                btnMeterPrePost.selected = sliderMeter;
            }
            int peqEnableValue = pkt->ch_11_ctrl;
            if (btnPeq1.selected != ((peqEnableValue & 0x08) >> 3) && ![viewController sendCommandExists:CMD_CH3_CTRL dspId:DSP_3]) {
                btnPeq1.selected = peq1Enabled[currentChannel] = (peqEnableValue & 0x08) >> 3;
                valueChanged = YES;
            }
            if (btnPeq2.selected != ((peqEnableValue & 0x10) >> 4) && ![viewController sendCommandExists:CMD_CH3_CTRL dspId:DSP_3]) {
                btnPeq2.selected = peq2Enabled[currentChannel] = (peqEnableValue & 0x10) >> 4;
                valueChanged = YES;
            }
            if (btnPeq3.selected != ((peqEnableValue & 0x20) >> 5) && ![viewController sendCommandExists:CMD_CH3_CTRL dspId:DSP_3]) {
                btnPeq3.selected = peq3Enabled[currentChannel] = (peqEnableValue & 0x20) >> 5;
                valueChanged = YES;
            }
            if (btnPeq4.selected != ((peqEnableValue & 0x40) >> 6) && ![viewController sendCommandExists:CMD_CH3_CTRL dspId:DSP_3]) {
                btnPeq4.selected = peq4Enabled[currentChannel] = (peqEnableValue & 0x40) >> 6;
                valueChanged = YES;
            }
        }
            break;
        case CHANNEL_CH_12:
        {
            if (peq1G[currentChannel] != pkt->ch_12_eq_1_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH4_EQ1_DB dspId:DSP_3]) {
                peq1G[currentChannel] = pkt->ch_12_eq_1_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq1F[currentChannel] != pkt->ch_12_eq_1_freq && ![viewController sendCommandExists:CMD_CH4_EQ1_FREQ dspId:DSP_3]) {
                peq1F[currentChannel] = pkt->ch_12_eq_1_freq;
                valueChanged = YES;
            }
            if (peq1Q[currentChannel] != pkt->ch_12_eq_1_q && ![viewController sendCommandExists:CMD_CH4_EQ1_Q dspId:DSP_3]) {
                peq1Q[currentChannel] = pkt->ch_12_eq_1_q;
                [self updatePeq1Filter];
                valueChanged = YES;
            }
            if (peq2G[currentChannel] != pkt->ch_12_eq_2_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH4_EQ2_DB dspId:DSP_3]) {
                peq2G[currentChannel] = pkt->ch_12_eq_2_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq2F[currentChannel] != pkt->ch_12_eq_2_freq && ![viewController sendCommandExists:CMD_CH4_EQ2_FREQ dspId:DSP_3]) {
                peq2F[currentChannel] = pkt->ch_12_eq_2_freq;
                valueChanged = YES;
            }
            if (peq2Q[currentChannel] != pkt->ch_12_eq_2_q && ![viewController sendCommandExists:CMD_CH4_EQ2_Q dspId:DSP_3]) {
                peq2Q[currentChannel] = pkt->ch_12_eq_2_q;
                valueChanged = YES;
            }
            if (peq3G[currentChannel] != pkt->ch_12_eq_3_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH4_EQ3_DB dspId:DSP_3]) {
                peq3G[currentChannel] = pkt->ch_12_eq_3_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq3F[currentChannel] != pkt->ch_12_eq_3_freq && ![viewController sendCommandExists:CMD_CH4_EQ3_FREQ dspId:DSP_3]) {
                peq3F[currentChannel] = pkt->ch_12_eq_3_freq;
                valueChanged = YES;
            }
            if (peq3Q[currentChannel] != pkt->ch_12_eq_3_q && ![viewController sendCommandExists:CMD_CH4_EQ3_Q dspId:DSP_3]) {
                peq3Q[currentChannel] = pkt->ch_12_eq_3_q;
                valueChanged = YES;
            }
            if (peq4G[currentChannel] != pkt->ch_12_eq_4_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH4_EQ4_DB dspId:DSP_3]) {
                peq4G[currentChannel] = pkt->ch_12_eq_4_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq4F[currentChannel] != pkt->ch_12_eq_4_freq && ![viewController sendCommandExists:CMD_CH4_EQ4_FREQ dspId:DSP_3]) {
                peq4F[currentChannel] = pkt->ch_12_eq_4_freq;
                valueChanged = YES;
            }
            if (peq4Q[currentChannel] != pkt->ch_12_eq_4_q && ![viewController sendCommandExists:CMD_CH4_EQ4_Q dspId:DSP_3]) {
                peq4Q[currentChannel] = pkt->ch_12_eq_4_q;
                [self updatePeq4Filter];
                valueChanged = YES;
            }
            meterIn.progress = ((float)pkt->ch_12_meter_eq_in/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterOut.progress = ((float)pkt->ch_12_meter_eq_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            if (!sliderLock) {
                slider.value = pkt->ch_12_fader;
                [self onSliderAction:nil];
            }
            BOOL sliderOn = (pkt->ch_on_off & 0x0800) >> 11;
            if (sliderOn != btnOn.selected && ![viewController sendCommandExists:CMD_CH_ON_OFF dspId:DSP_5]) {
                btnOn.selected = sliderOn;
            }
            BOOL sliderSolo = (pkt->solo_ctrl & 0x0800) >> 11;
            if (sliderSolo != btnSolo.selected && ![viewController sendCommandExists:CMD_SOLO_CTRL dspId:DSP_5]) {
                btnSolo.selected = sliderSolo;
            }
            BOOL sliderSoloSafe = (pkt->solo_safe_ctrl & 0x0800) >> 11;
            if (sliderSoloSafe) {
                [btnSolo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
                [btnSolo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
            } else {
                [btnSolo setTitle:@"SOLO" forState:UIControlStateNormal];
                [btnSolo setTitle:@"SOLO" forState:UIControlStateSelected];
            }
            BOOL sliderMeter = (pkt->ch_meter_pre_post & 0x0800) >> 11;
            if (sliderMeter != btnMeterPrePost.selected && ![viewController sendCommandExists:CMD_CH_METER dspId:DSP_5]) {
                btnMeterPrePost.selected = sliderMeter;
            }
            int peqEnableValue = pkt->ch_12_ctrl;
            if (btnPeq1.selected != ((peqEnableValue & 0x08) >> 3) && ![viewController sendCommandExists:CMD_CH4_CTRL dspId:DSP_3]) {
                btnPeq1.selected = peq1Enabled[currentChannel] = (peqEnableValue & 0x08) >> 3;
                valueChanged = YES;
            }
            if (btnPeq2.selected != ((peqEnableValue & 0x10) >> 4) && ![viewController sendCommandExists:CMD_CH4_CTRL dspId:DSP_3]) {
                btnPeq2.selected = peq2Enabled[currentChannel] = (peqEnableValue & 0x10) >> 4;
                valueChanged = YES;
            }
            if (btnPeq3.selected != ((peqEnableValue & 0x20) >> 5) && ![viewController sendCommandExists:CMD_CH4_CTRL dspId:DSP_3]) {
                btnPeq3.selected = peq3Enabled[currentChannel] = (peqEnableValue & 0x20) >> 5;
                valueChanged = YES;
            }
            if (btnPeq4.selected != ((peqEnableValue & 0x40) >> 6) && ![viewController sendCommandExists:CMD_CH4_CTRL dspId:DSP_3]) {
                btnPeq4.selected = peq4Enabled[currentChannel] = (peqEnableValue & 0x40) >> 6;
                valueChanged = YES;
            }
        }
            break;
        case CHANNEL_CH_13:
        {
            if (peq1G[currentChannel] != pkt->ch_13_eq_1_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH1_EQ1_DB dspId:DSP_4]) {
                peq1G[currentChannel] = pkt->ch_13_eq_1_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq1F[currentChannel] != pkt->ch_13_eq_1_freq && ![viewController sendCommandExists:CMD_CH1_EQ1_FREQ dspId:DSP_4]) {
                peq1F[currentChannel] = pkt->ch_13_eq_1_freq;
                valueChanged = YES;
            }
            if (peq1Q[currentChannel] != pkt->ch_13_eq_1_q && ![viewController sendCommandExists:CMD_CH1_EQ1_Q dspId:DSP_4]) {
                peq1Q[currentChannel] = pkt->ch_13_eq_1_q;
                [self updatePeq1Filter];
                valueChanged = YES;
            }
            if (peq2G[currentChannel] != pkt->ch_13_eq_2_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH1_EQ2_DB dspId:DSP_4]) {
                peq2G[currentChannel] = pkt->ch_13_eq_2_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq2F[currentChannel] != pkt->ch_13_eq_2_freq && ![viewController sendCommandExists:CMD_CH1_EQ2_FREQ dspId:DSP_4]) {
                peq2F[currentChannel] = pkt->ch_13_eq_2_freq;
                valueChanged = YES;
            }
            if (peq2Q[currentChannel] != pkt->ch_13_eq_2_q && ![viewController sendCommandExists:CMD_CH1_EQ2_Q dspId:DSP_4]) {
                peq2Q[currentChannel] = pkt->ch_13_eq_2_q;
                valueChanged = YES;
            }
            if (peq3G[currentChannel] != pkt->ch_13_eq_3_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH1_EQ3_DB dspId:DSP_4]) {
                peq3G[currentChannel] = pkt->ch_13_eq_3_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq3F[currentChannel] != pkt->ch_13_eq_3_freq && ![viewController sendCommandExists:CMD_CH1_EQ3_FREQ dspId:DSP_4]) {
                peq3F[currentChannel] = pkt->ch_13_eq_3_freq;
                valueChanged = YES;
            }
            if (peq3Q[currentChannel] != pkt->ch_13_eq_3_q && ![viewController sendCommandExists:CMD_CH1_EQ3_Q dspId:DSP_4]) {
                peq3Q[currentChannel] = pkt->ch_13_eq_3_q;
                valueChanged = YES;
            }
            if (peq4G[currentChannel] != pkt->ch_13_eq_4_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH1_EQ4_DB dspId:DSP_4]) {
                peq4G[currentChannel] = pkt->ch_13_eq_4_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq4F[currentChannel] != pkt->ch_13_eq_4_freq && ![viewController sendCommandExists:CMD_CH1_EQ4_FREQ dspId:DSP_4]) {
                peq4F[currentChannel] = pkt->ch_13_eq_4_freq;
                valueChanged = YES;
            }
            if (peq4Q[currentChannel] != pkt->ch_13_eq_4_q && ![viewController sendCommandExists:CMD_CH1_EQ4_Q dspId:DSP_4]) {
                peq4Q[currentChannel] = pkt->ch_13_eq_4_q;
                [self updatePeq4Filter];
                valueChanged = YES;
            }
            meterIn.progress = ((float)pkt->ch_13_meter_eq_in/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterOut.progress = ((float)pkt->ch_13_meter_eq_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            if (!sliderLock) {
                slider.value = pkt->ch_13_fader;
                [self onSliderAction:nil];
            }
            BOOL sliderOn = (pkt->ch_on_off & 0x1000) >> 12;
            if (sliderOn != btnOn.selected && ![viewController sendCommandExists:CMD_CH_ON_OFF dspId:DSP_5]) {
                btnOn.selected = sliderOn;
            }
            BOOL sliderSolo = (pkt->solo_ctrl & 0x1000) >> 12;
            if (sliderSolo != btnSolo.selected && ![viewController sendCommandExists:CMD_SOLO_CTRL dspId:DSP_5]) {
                btnSolo.selected = sliderSolo;
            }
            BOOL sliderSoloSafe = (pkt->solo_safe_ctrl & 0x1000) >> 12;
            if (sliderSoloSafe) {
                [btnSolo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
                [btnSolo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
            } else {
                [btnSolo setTitle:@"SOLO" forState:UIControlStateNormal];
                [btnSolo setTitle:@"SOLO" forState:UIControlStateSelected];
            }
            BOOL sliderMeter = (pkt->ch_meter_pre_post & 0x1000) >> 12;
            if (sliderMeter != btnMeterPrePost.selected && ![viewController sendCommandExists:CMD_CH_METER dspId:DSP_5]) {
                btnMeterPrePost.selected = sliderMeter;
            }
            int peqEnableValue = pkt->ch_13_ctrl;
            if (btnPeq1.selected != ((peqEnableValue & 0x08) >> 3) && ![viewController sendCommandExists:CMD_CH1_CTRL dspId:DSP_4]) {
                btnPeq1.selected = peq1Enabled[currentChannel] = (peqEnableValue & 0x08) >> 3;
                valueChanged = YES;
            }
            if (btnPeq2.selected != ((peqEnableValue & 0x10) >> 4) && ![viewController sendCommandExists:CMD_CH1_CTRL dspId:DSP_4]) {
                btnPeq2.selected = peq2Enabled[currentChannel] = (peqEnableValue & 0x10) >> 4;
                valueChanged = YES;
            }
            if (btnPeq3.selected != ((peqEnableValue & 0x20) >> 5) && ![viewController sendCommandExists:CMD_CH1_CTRL dspId:DSP_4]) {
                btnPeq3.selected = peq3Enabled[currentChannel] = (peqEnableValue & 0x20) >> 5;
                valueChanged = YES;
            }
            if (btnPeq4.selected != ((peqEnableValue & 0x40) >> 6) && ![viewController sendCommandExists:CMD_CH1_CTRL dspId:DSP_4]) {
                btnPeq4.selected = peq4Enabled[currentChannel] = (peqEnableValue & 0x40) >> 6;
                valueChanged = YES;
            }
        }
            break;
        case CHANNEL_CH_14:
        {
            if (peq1G[currentChannel] != pkt->ch_14_eq_1_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH2_EQ1_DB dspId:DSP_4]) {
                peq1G[currentChannel] = pkt->ch_14_eq_1_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq1F[currentChannel] != pkt->ch_14_eq_1_freq && ![viewController sendCommandExists:CMD_CH2_EQ1_FREQ dspId:DSP_4]) {
                peq1F[currentChannel] = pkt->ch_14_eq_1_freq;
                valueChanged = YES;
            }
            if (peq1Q[currentChannel] != pkt->ch_14_eq_1_q && ![viewController sendCommandExists:CMD_CH2_EQ1_Q dspId:DSP_4]) {
                peq1Q[currentChannel] = pkt->ch_14_eq_1_q;
                [self updatePeq1Filter];
                valueChanged = YES;
            }
            if (peq2G[currentChannel] != pkt->ch_14_eq_2_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH2_EQ2_DB dspId:DSP_4]) {
                peq2G[currentChannel] = pkt->ch_14_eq_2_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq2F[currentChannel] != pkt->ch_14_eq_2_freq && ![viewController sendCommandExists:CMD_CH2_EQ2_FREQ dspId:DSP_4]) {
                peq2F[currentChannel] = pkt->ch_14_eq_2_freq;
                valueChanged = YES;
            }
            if (peq2Q[currentChannel] != pkt->ch_14_eq_2_q && ![viewController sendCommandExists:CMD_CH2_EQ2_Q dspId:DSP_4]) {
                peq2Q[currentChannel] = pkt->ch_14_eq_2_q;
                valueChanged = YES;
            }
            if (peq3G[currentChannel] != pkt->ch_14_eq_3_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH2_EQ3_DB dspId:DSP_4]) {
                peq3G[currentChannel] = pkt->ch_14_eq_3_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq3F[currentChannel] != pkt->ch_14_eq_3_freq && ![viewController sendCommandExists:CMD_CH2_EQ3_FREQ dspId:DSP_4]) {
                peq3F[currentChannel] = pkt->ch_14_eq_3_freq;
                valueChanged = YES;
            }
            if (peq3Q[currentChannel] != pkt->ch_14_eq_3_q && ![viewController sendCommandExists:CMD_CH2_EQ3_Q dspId:DSP_4]) {
                peq3Q[currentChannel] = pkt->ch_14_eq_3_q;
                valueChanged = YES;
            }
            if (peq4G[currentChannel] != pkt->ch_14_eq_4_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH2_EQ4_DB dspId:DSP_4]) {
                peq4G[currentChannel] = pkt->ch_14_eq_4_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq4F[currentChannel] != pkt->ch_14_eq_4_freq && ![viewController sendCommandExists:CMD_CH2_EQ4_FREQ dspId:DSP_4]) {
                peq4F[currentChannel] = pkt->ch_14_eq_4_freq;
                valueChanged = YES;
            }
            if (peq4Q[currentChannel] != pkt->ch_14_eq_4_q && ![viewController sendCommandExists:CMD_CH2_EQ4_Q dspId:DSP_4]) {
                peq4Q[currentChannel] = pkt->ch_14_eq_4_q;
                [self updatePeq4Filter];
                valueChanged = YES;
            }
            meterIn.progress = ((float)pkt->ch_14_meter_eq_in/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterOut.progress = ((float)pkt->ch_14_meter_eq_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            if (!sliderLock) {
                slider.value = pkt->ch_14_fader;
                [self onSliderAction:nil];
            }
            BOOL sliderOn = (pkt->ch_on_off & 0x2000) >> 13;
            if (sliderOn != btnOn.selected && ![viewController sendCommandExists:CMD_CH_ON_OFF dspId:DSP_5]) {
                btnOn.selected = sliderOn;
            }
            BOOL sliderSolo = (pkt->solo_ctrl & 0x2000) >> 13;
            if (sliderSolo != btnSolo.selected && ![viewController sendCommandExists:CMD_SOLO_CTRL dspId:DSP_5]) {
                btnSolo.selected = sliderSolo;
            }
            BOOL sliderSoloSafe = (pkt->solo_safe_ctrl & 0x2000) >> 13;
            if (sliderSoloSafe) {
                [btnSolo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
                [btnSolo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
            } else {
                [btnSolo setTitle:@"SOLO" forState:UIControlStateNormal];
                [btnSolo setTitle:@"SOLO" forState:UIControlStateSelected];
            }
            BOOL sliderMeter = (pkt->ch_meter_pre_post & 0x2000) >> 13;
            if (sliderMeter != btnMeterPrePost.selected && ![viewController sendCommandExists:CMD_CH_METER dspId:DSP_5]) {
                btnMeterPrePost.selected = sliderMeter;
            }
            int peqEnableValue = pkt->ch_14_ctrl;
            if (btnPeq1.selected != ((peqEnableValue & 0x08) >> 3) && ![viewController sendCommandExists:CMD_CH2_CTRL dspId:DSP_4]) {
                btnPeq1.selected = peq1Enabled[currentChannel] = (peqEnableValue & 0x08) >> 3;
                valueChanged = YES;
            }
            if (btnPeq2.selected != ((peqEnableValue & 0x10) >> 4) && ![viewController sendCommandExists:CMD_CH2_CTRL dspId:DSP_4]) {
                btnPeq2.selected = peq2Enabled[currentChannel] = (peqEnableValue & 0x10) >> 4;
                valueChanged = YES;
            }
            if (btnPeq3.selected != ((peqEnableValue & 0x20) >> 5) && ![viewController sendCommandExists:CMD_CH2_CTRL dspId:DSP_4]) {
                btnPeq3.selected = peq3Enabled[currentChannel] = (peqEnableValue & 0x20) >> 5;
                valueChanged = YES;
            }
            if (btnPeq4.selected != ((peqEnableValue & 0x40) >> 6) && ![viewController sendCommandExists:CMD_CH2_CTRL dspId:DSP_4]) {
                btnPeq4.selected = peq4Enabled[currentChannel] = (peqEnableValue & 0x40) >> 6;
                valueChanged = YES;
            }
        }
            break;
        case CHANNEL_CH_15:
        {
            if (peq1G[currentChannel] != pkt->ch_15_eq_1_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH3_EQ1_DB dspId:DSP_4]) {
                peq1G[currentChannel] = pkt->ch_15_eq_1_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq1F[currentChannel] != pkt->ch_15_eq_1_freq && ![viewController sendCommandExists:CMD_CH3_EQ1_FREQ dspId:DSP_4]) {
                peq1F[currentChannel] = pkt->ch_15_eq_1_freq;
                valueChanged = YES;
            }
            if (peq1Q[currentChannel] != pkt->ch_15_eq_1_q && ![viewController sendCommandExists:CMD_CH3_EQ1_Q dspId:DSP_4]) {
                peq1Q[currentChannel] = pkt->ch_15_eq_1_q;
                [self updatePeq1Filter];
                valueChanged = YES;
            }
            if (peq2G[currentChannel] != pkt->ch_15_eq_2_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH3_EQ2_DB dspId:DSP_4]) {
                peq2G[currentChannel] = pkt->ch_15_eq_2_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq2F[currentChannel] != pkt->ch_15_eq_2_freq && ![viewController sendCommandExists:CMD_CH3_EQ2_FREQ dspId:DSP_4]) {
                peq2F[currentChannel] = pkt->ch_15_eq_2_freq;
                valueChanged = YES;
            }
            if (peq2Q[currentChannel] != pkt->ch_15_eq_2_q && ![viewController sendCommandExists:CMD_CH3_EQ2_Q dspId:DSP_4]) {
                peq2Q[currentChannel] = pkt->ch_15_eq_2_q;
                valueChanged = YES;
            }
            if (peq3G[currentChannel] != pkt->ch_15_eq_3_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH3_EQ3_DB dspId:DSP_4]) {
                peq3G[currentChannel] = pkt->ch_15_eq_3_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq3F[currentChannel] != pkt->ch_15_eq_3_freq && ![viewController sendCommandExists:CMD_CH3_EQ3_FREQ dspId:DSP_4]) {
                peq3F[currentChannel] = pkt->ch_15_eq_3_freq;
                valueChanged = YES;
            }
            if (peq3Q[currentChannel] != pkt->ch_15_eq_3_q && ![viewController sendCommandExists:CMD_CH3_EQ3_Q dspId:DSP_4]) {
                peq3Q[currentChannel] = pkt->ch_15_eq_3_q;
                valueChanged = YES;
            }
            if (peq4G[currentChannel] != pkt->ch_15_eq_4_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH3_EQ4_DB dspId:DSP_4]) {
                peq4G[currentChannel] = pkt->ch_15_eq_4_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq4F[currentChannel] != pkt->ch_15_eq_4_freq && ![viewController sendCommandExists:CMD_CH3_EQ4_FREQ dspId:DSP_4]) {
                peq4F[currentChannel] = pkt->ch_15_eq_4_freq;
                valueChanged = YES;
            }
            if (peq4Q[currentChannel] != pkt->ch_15_eq_4_q && ![viewController sendCommandExists:CMD_CH3_EQ4_Q dspId:DSP_4]) {
                peq4Q[currentChannel] = pkt->ch_15_eq_4_q;
                [self updatePeq4Filter];
                valueChanged = YES;
            }
            meterIn.progress = ((float)pkt->ch_15_meter_eq_in/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterOut.progress = ((float)pkt->ch_15_meter_eq_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            if (!sliderLock) {
                slider.value = pkt->ch_15_fader;
                [self onSliderAction:nil];
            }
            BOOL sliderOn = (pkt->ch_on_off & 0x4000) >> 14;
            if (sliderOn != btnOn.selected && ![viewController sendCommandExists:CMD_CH_ON_OFF dspId:DSP_5]) {
                btnOn.selected = sliderOn;
            }
            BOOL sliderSolo = (pkt->solo_ctrl & 0x4000) >> 14;
            if (sliderSolo != btnSolo.selected && ![viewController sendCommandExists:CMD_SOLO_CTRL dspId:DSP_5]) {
                btnSolo.selected = sliderSolo;
            }
            BOOL sliderSoloSafe = (pkt->solo_safe_ctrl & 0x8000) >> 14;
            if (sliderSoloSafe) {
                [btnSolo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
                [btnSolo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
            } else {
                [btnSolo setTitle:@"SOLO" forState:UIControlStateNormal];
                [btnSolo setTitle:@"SOLO" forState:UIControlStateSelected];
            }
            BOOL sliderMeter = (pkt->ch_meter_pre_post & 0x4000) >> 14;
            if (sliderMeter != btnMeterPrePost.selected && ![viewController sendCommandExists:CMD_CH_METER dspId:DSP_5]) {
                btnMeterPrePost.selected = sliderMeter;
            }
            int peqEnableValue = pkt->ch_15_ctrl;
            if (btnPeq1.selected != ((peqEnableValue & 0x08) >> 3) && ![viewController sendCommandExists:CMD_CH3_CTRL dspId:DSP_4]) {
                btnPeq1.selected = peq1Enabled[currentChannel] = (peqEnableValue & 0x08) >> 3;
                valueChanged = YES;
            }
            if (btnPeq2.selected != ((peqEnableValue & 0x10) >> 4) && ![viewController sendCommandExists:CMD_CH3_CTRL dspId:DSP_4]) {
                btnPeq2.selected = peq2Enabled[currentChannel] = (peqEnableValue & 0x10) >> 4;
                valueChanged = YES;
            }
            if (btnPeq3.selected != ((peqEnableValue & 0x20) >> 5) && ![viewController sendCommandExists:CMD_CH3_CTRL dspId:DSP_4]) {
                btnPeq3.selected = peq3Enabled[currentChannel] = (peqEnableValue & 0x20) >> 5;
                valueChanged = YES;
            }
            if (btnPeq4.selected != ((peqEnableValue & 0x40) >> 6) && ![viewController sendCommandExists:CMD_CH3_CTRL dspId:DSP_4]) {
                btnPeq4.selected = peq4Enabled[currentChannel] = (peqEnableValue & 0x40) >> 6;
                valueChanged = YES;
            }
        }
            break;
        case CHANNEL_CH_16:
        {
            if (peq1G[currentChannel] != pkt->ch_16_eq_1_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH4_EQ1_DB dspId:DSP_4]) {
                peq1G[currentChannel] = pkt->ch_16_eq_1_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq1F[currentChannel] != pkt->ch_16_eq_1_freq && ![viewController sendCommandExists:CMD_CH4_EQ1_FREQ dspId:DSP_4]) {
                peq1F[currentChannel] = pkt->ch_16_eq_1_freq;
                valueChanged = YES;
            }
            if (peq1Q[currentChannel] != pkt->ch_16_eq_1_q && ![viewController sendCommandExists:CMD_CH4_EQ1_Q dspId:DSP_4]) {
                peq1Q[currentChannel] = pkt->ch_16_eq_1_q;
                [self updatePeq1Filter];
                valueChanged = YES;
            }
            if (peq2G[currentChannel] != pkt->ch_16_eq_2_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH4_EQ2_DB dspId:DSP_4]) {
                peq2G[currentChannel] = pkt->ch_16_eq_2_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq2F[currentChannel] != pkt->ch_16_eq_2_freq && ![viewController sendCommandExists:CMD_CH4_EQ2_FREQ dspId:DSP_4]) {
                peq2F[currentChannel] = pkt->ch_16_eq_2_freq;
                valueChanged = YES;
            }
            if (peq2Q[currentChannel] != pkt->ch_16_eq_2_q && ![viewController sendCommandExists:CMD_CH4_EQ2_Q dspId:DSP_4]) {
                peq2Q[currentChannel] = pkt->ch_16_eq_2_q;
                valueChanged = YES;
            }
            if (peq3G[currentChannel] != pkt->ch_16_eq_3_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH4_EQ3_DB dspId:DSP_4]) {
                peq3G[currentChannel] = pkt->ch_16_eq_3_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq3F[currentChannel] != pkt->ch_16_eq_3_freq && ![viewController sendCommandExists:CMD_CH4_EQ3_FREQ dspId:DSP_4]) {
                peq3F[currentChannel] = pkt->ch_16_eq_3_freq;
                valueChanged = YES;
            }
            if (peq3Q[currentChannel] != pkt->ch_16_eq_3_q && ![viewController sendCommandExists:CMD_CH4_EQ3_Q dspId:DSP_4]) {
                peq3Q[currentChannel] = pkt->ch_16_eq_3_q;
                valueChanged = YES;
            }
            if (peq4G[currentChannel] != pkt->ch_16_eq_4_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH4_EQ4_DB dspId:DSP_4]) {
                peq4G[currentChannel] = pkt->ch_16_eq_4_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq4F[currentChannel] != pkt->ch_16_eq_4_freq && ![viewController sendCommandExists:CMD_CH4_EQ4_FREQ dspId:DSP_4]) {
                peq4F[currentChannel] = pkt->ch_16_eq_4_freq;
                valueChanged = YES;
            }
            if (peq4Q[currentChannel] != pkt->ch_16_eq_4_q && ![viewController sendCommandExists:CMD_CH4_EQ4_Q dspId:DSP_4]) {
                peq4Q[currentChannel] = pkt->ch_16_eq_4_q;
                [self updatePeq4Filter];
                valueChanged = YES;
            }
            meterIn.progress = ((float)pkt->ch_16_meter_eq_in/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterOut.progress = ((float)pkt->ch_16_meter_eq_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            if (!sliderLock) {
                slider.value = pkt->ch_16_fader;
                [self onSliderAction:nil];
            }
            BOOL sliderOn = (pkt->ch_on_off & 0x8000) >> 15;
            if (sliderOn != btnOn.selected && ![viewController sendCommandExists:CMD_CH_ON_OFF dspId:DSP_5]) {
                btnOn.selected = sliderOn;
            }
            BOOL sliderSolo = (pkt->solo_ctrl & 0x8000) >> 15;
            if (sliderSolo != btnSolo.selected && ![viewController sendCommandExists:CMD_SOLO_CTRL dspId:DSP_5]) {
                btnSolo.selected = sliderSolo;
            }
            BOOL sliderSoloSafe = (pkt->solo_safe_ctrl & 0x8000) >> 15;
            if (sliderSoloSafe) {
                [btnSolo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
                [btnSolo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
            } else {
                [btnSolo setTitle:@"SOLO" forState:UIControlStateNormal];
                [btnSolo setTitle:@"SOLO" forState:UIControlStateSelected];
            }
            BOOL sliderMeter = (pkt->ch_meter_pre_post & 0x8000) >> 15;
            if (sliderMeter != btnMeterPrePost.selected && ![viewController sendCommandExists:CMD_CH_METER dspId:DSP_5]) {
                btnMeterPrePost.selected = sliderMeter;
            }
            int peqEnableValue = pkt->ch_16_ctrl;
            if (btnPeq1.selected != ((peqEnableValue & 0x08) >> 3) && ![viewController sendCommandExists:CMD_CH4_CTRL dspId:DSP_4]) {
                btnPeq1.selected = peq1Enabled[currentChannel] = (peqEnableValue & 0x08) >> 3;
                valueChanged = YES;
            }
            if (btnPeq2.selected != ((peqEnableValue & 0x10) >> 4) && ![viewController sendCommandExists:CMD_CH4_CTRL dspId:DSP_4]) {
                btnPeq2.selected = peq2Enabled[currentChannel] = (peqEnableValue & 0x10) >> 4;
                valueChanged = YES;
            }
            if (btnPeq3.selected != ((peqEnableValue & 0x20) >> 5) && ![viewController sendCommandExists:CMD_CH4_CTRL dspId:DSP_4]) {
                btnPeq3.selected = peq3Enabled[currentChannel] = (peqEnableValue & 0x20) >> 5;
                valueChanged = YES;
            }
            if (btnPeq4.selected != ((peqEnableValue & 0x40) >> 6) && ![viewController sendCommandExists:CMD_CH4_CTRL dspId:DSP_4]) {
                btnPeq4.selected = peq4Enabled[currentChannel] = (peqEnableValue & 0x40) >> 6;
                valueChanged = YES;
            }
        }
            break;
        case CHANNEL_CH_17:
        {
            if (peq1G[currentChannel] != pkt->ch_17_eq_1_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH17_EQ1_DB dspId:DSP_6]) {
                peq1G[currentChannel] = pkt->ch_17_eq_1_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq1F[currentChannel] != pkt->ch_17_eq_1_freq && ![viewController sendCommandExists:CMD_CH17_EQ1_FREQ dspId:DSP_6]) {
                peq1F[currentChannel] = pkt->ch_17_eq_1_freq;
                valueChanged = YES;
            }
            if (peq1Q[currentChannel] != pkt->ch_17_eq_1_q && ![viewController sendCommandExists:CMD_CH17_EQ1_Q dspId:DSP_6]) {
                peq1Q[currentChannel] = pkt->ch_17_eq_1_q;
                [self updatePeq1Filter];
                valueChanged = YES;
            }
            if (peq2G[currentChannel] != pkt->ch_17_eq_2_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH17_EQ2_DB dspId:DSP_6]) {
                peq2G[currentChannel] = pkt->ch_17_eq_2_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq2F[currentChannel] != pkt->ch_17_eq_2_freq && ![viewController sendCommandExists:CMD_CH17_EQ2_FREQ dspId:DSP_6]) {
                peq2F[currentChannel] = pkt->ch_17_eq_2_freq;
                valueChanged = YES;
            }
            if (peq2Q[currentChannel] != pkt->ch_17_eq_2_q && ![viewController sendCommandExists:CMD_CH17_EQ2_Q dspId:DSP_6]) {
                peq2Q[currentChannel] = pkt->ch_17_eq_2_q;
                valueChanged = YES;
            }
            if (peq3G[currentChannel] != pkt->ch_17_eq_3_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH17_EQ3_DB dspId:DSP_6]) {
                peq3G[currentChannel] = pkt->ch_17_eq_3_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq3F[currentChannel] != pkt->ch_17_eq_3_freq && ![viewController sendCommandExists:CMD_CH17_EQ3_FREQ dspId:DSP_6]) {
                peq3F[currentChannel] = pkt->ch_17_eq_3_freq;
                valueChanged = YES;
            }
            if (peq3Q[currentChannel] != pkt->ch_17_eq_3_q && ![viewController sendCommandExists:CMD_CH17_EQ3_Q dspId:DSP_6]) {
                peq3Q[currentChannel] = pkt->ch_17_eq_3_q;
                valueChanged = YES;
            }
            if (peq4G[currentChannel] != pkt->ch_17_eq_4_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH17_EQ4_DB dspId:DSP_6]) {
                peq4G[currentChannel] = pkt->ch_17_eq_4_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq4F[currentChannel] != pkt->ch_17_eq_4_freq && ![viewController sendCommandExists:CMD_CH17_EQ4_FREQ dspId:DSP_6]) {
                peq4F[currentChannel] = pkt->ch_17_eq_4_freq;
                valueChanged = YES;
            }
            if (peq4Q[currentChannel] != pkt->ch_17_eq_4_q && ![viewController sendCommandExists:CMD_CH17_EQ4_Q dspId:DSP_6]) {
                peq4Q[currentChannel] = pkt->ch_17_eq_4_q;
                [self updatePeq4Filter];
                valueChanged = YES;
            }
            meterIn.progress = ((float)pkt->ch_17_meter_eq_in/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterOut.progress = ((float)pkt->ch_17_meter_eq_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            if (!sliderLock) {
                slider.value = pkt->ch_17_fader;
                [self onSliderAction:nil];
            }
            BOOL sliderOn = (pkt->ch_17_audio_setting & 0x0100) >> 8;
            if (sliderOn != btnOn.selected && ![viewController sendCommandExists:CMD_CH17_USB_AUDIO_SETTING dspId:DSP_5]) {
                btnOn.selected = sliderOn;
            }
            BOOL sliderSolo = (pkt->ch_17_audio_setting & 0x0400) >> 10;
            if (sliderSolo != btnSolo.selected && ![viewController sendCommandExists:CMD_CH17_USB_AUDIO_SETTING dspId:DSP_5]) {
                btnSolo.selected = sliderSolo;
            }
            BOOL soloSafe = (pkt->ch_17_audio_setting & 0x0800) >> 11;
            if (soloSafe) {
                [btnSolo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
                [btnSolo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
            } else {
                [btnSolo setTitle:@"SOLO" forState:UIControlStateNormal];
                [btnSolo setTitle:@"SOLO" forState:UIControlStateSelected];
            }
            BOOL sliderMeter = (pkt->ch_17_audio_setting & 0x8000) >> 15;
            if (sliderMeter != btnMeterPrePost.selected && ![viewController sendCommandExists:CMD_CH17_USB_AUDIO_SETTING dspId:DSP_5]) {
                btnMeterPrePost.selected = sliderMeter;
            }
            int peqEnableValue = pkt->ch_17_ctrl;
            if (btnPeq1.selected != ((peqEnableValue & 0x08) >> 3) && ![viewController sendCommandExists:CMD_CH17_CTRL dspId:DSP_6]) {
                btnPeq1.selected = peq1Enabled[currentChannel] = (peqEnableValue & 0x08) >> 3;
                valueChanged = YES;
            }
            if (btnPeq2.selected != ((peqEnableValue & 0x10) >> 4) && ![viewController sendCommandExists:CMD_CH17_CTRL dspId:DSP_6]) {
                btnPeq2.selected = peq2Enabled[currentChannel] = (peqEnableValue & 0x10) >> 4;
                valueChanged = YES;
            }
            if (btnPeq3.selected != ((peqEnableValue & 0x20) >> 5) && ![viewController sendCommandExists:CMD_CH17_CTRL dspId:DSP_6]) {
                btnPeq3.selected = peq3Enabled[currentChannel] = (peqEnableValue & 0x20) >> 5;
                valueChanged = YES;
            }
            if (btnPeq4.selected != ((peqEnableValue & 0x40) >> 6) && ![viewController sendCommandExists:CMD_CH17_CTRL dspId:DSP_6]) {
                btnPeq4.selected = peq4Enabled[currentChannel] = (peqEnableValue & 0x40) >> 6;
                valueChanged = YES;
            }
        }
            break;
        case CHANNEL_CH_18:
        {
            if (peq1G[currentChannel] != pkt->ch_18_eq_1_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH18_EQ1_DB dspId:DSP_6]) {
                peq1G[currentChannel] = pkt->ch_18_eq_1_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq1F[currentChannel] != pkt->ch_18_eq_1_freq && ![viewController sendCommandExists:CMD_CH18_EQ1_FREQ dspId:DSP_6]) {
                peq1F[currentChannel] = pkt->ch_18_eq_1_freq;
                valueChanged = YES;
            }
            if (peq1Q[currentChannel] != pkt->ch_18_eq_1_q && ![viewController sendCommandExists:CMD_CH18_EQ1_Q dspId:DSP_6]) {
                peq1Q[currentChannel] = pkt->ch_18_eq_1_q;
                [self updatePeq1Filter];
                valueChanged = YES;
            }
            if (peq2G[currentChannel] != pkt->ch_18_eq_2_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH18_EQ2_DB dspId:DSP_6]) {
                peq2G[currentChannel] = pkt->ch_18_eq_2_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq2F[currentChannel] != pkt->ch_18_eq_2_freq && ![viewController sendCommandExists:CMD_CH18_EQ2_FREQ dspId:DSP_6]) {
                peq2F[currentChannel] = pkt->ch_18_eq_2_freq;
                valueChanged = YES;
            }
            if (peq2Q[currentChannel] != pkt->ch_18_eq_2_q && ![viewController sendCommandExists:CMD_CH18_EQ2_Q dspId:DSP_6]) {
                peq2Q[currentChannel] = pkt->ch_18_eq_2_q;
                valueChanged = YES;
            }
            if (peq3G[currentChannel] != pkt->ch_18_eq_3_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH18_EQ3_DB dspId:DSP_6]) {
                peq3G[currentChannel] = pkt->ch_18_eq_3_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq3F[currentChannel] != pkt->ch_18_eq_3_freq && ![viewController sendCommandExists:CMD_CH18_EQ3_FREQ dspId:DSP_6]) {
                peq3F[currentChannel] = pkt->ch_18_eq_3_freq;
                valueChanged = YES;
            }
            if (peq3Q[currentChannel] != pkt->ch_18_eq_3_q && ![viewController sendCommandExists:CMD_CH18_EQ3_Q dspId:DSP_6]) {
                peq3Q[currentChannel] = pkt->ch_18_eq_3_q;
                valueChanged = YES;
            }
            if (peq4G[currentChannel] != pkt->ch_18_eq_4_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_CH18_EQ4_DB dspId:DSP_6]) {
                peq4G[currentChannel] = pkt->ch_18_eq_4_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq4F[currentChannel] != pkt->ch_18_eq_4_freq && ![viewController sendCommandExists:CMD_CH18_EQ4_FREQ dspId:DSP_6]) {
                peq4F[currentChannel] = pkt->ch_18_eq_4_freq;
                valueChanged = YES;
            }
            if (peq4Q[currentChannel] != pkt->ch_18_eq_4_q && ![viewController sendCommandExists:CMD_CH18_EQ4_Q dspId:DSP_6]) {
                peq4Q[currentChannel] = pkt->ch_18_eq_4_q;
                [self updatePeq4Filter];
                valueChanged = YES;
            }
            meterIn.progress = ((float)pkt->ch_18_meter_eq_in/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterOut.progress = ((float)pkt->ch_18_meter_eq_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            if (!sliderLock) {
                slider.value = pkt->ch_18_fader;
                [self onSliderAction:nil];
            }
            BOOL sliderOn = (pkt->ch_18_audio_setting & 0x0100) >> 8;
            if (sliderOn != btnOn.selected && ![viewController sendCommandExists:CMD_CH18_USB_AUDIO_SETTING dspId:DSP_5]) {
                btnOn.selected = sliderOn;
            }
            BOOL sliderSolo = (pkt->ch_18_audio_setting & 0x0400) >> 10;
            if (sliderSolo != btnSolo.selected && ![viewController sendCommandExists:CMD_CH18_USB_AUDIO_SETTING dspId:DSP_5]) {
                btnSolo.selected = sliderSolo;
            }
            BOOL soloSafe = (pkt->ch_18_audio_setting & 0x0800) >> 11;
            if (soloSafe) {
                [btnSolo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
                [btnSolo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
            } else {
                [btnSolo setTitle:@"SOLO" forState:UIControlStateNormal];
                [btnSolo setTitle:@"SOLO" forState:UIControlStateSelected];
            }
            BOOL sliderMeter = (pkt->ch_18_audio_setting & 0x8000) >> 15;
            if (sliderMeter != btnMeterPrePost.selected && ![viewController sendCommandExists:CMD_CH18_USB_AUDIO_SETTING dspId:DSP_5]) {
                btnMeterPrePost.selected = sliderMeter;
            }
            int peqEnableValue = pkt->ch_18_ctrl;
            if (btnPeq1.selected != ((peqEnableValue & 0x08) >> 3) && ![viewController sendCommandExists:CMD_CH18_CTRL dspId:DSP_6]) {
                btnPeq1.selected = peq1Enabled[currentChannel] = (peqEnableValue & 0x08) >> 3;
                valueChanged = YES;
            }
            if (btnPeq2.selected != ((peqEnableValue & 0x10) >> 4) && ![viewController sendCommandExists:CMD_CH18_CTRL dspId:DSP_6]) {
                btnPeq2.selected = peq2Enabled[currentChannel] = (peqEnableValue & 0x10) >> 4;
                valueChanged = YES;
            }
            if (btnPeq3.selected != ((peqEnableValue & 0x20) >> 5) && ![viewController sendCommandExists:CMD_CH18_CTRL dspId:DSP_6]) {
                btnPeq3.selected = peq3Enabled[currentChannel] = (peqEnableValue & 0x20) >> 5;
                valueChanged = YES;
            }
            if (btnPeq4.selected != ((peqEnableValue & 0x40) >> 6) && ![viewController sendCommandExists:CMD_CH18_CTRL dspId:DSP_6]) {
                btnPeq4.selected = peq4Enabled[currentChannel] = (peqEnableValue & 0x40) >> 6;
                valueChanged = YES;
            }
        }
            break;
        default:
            break;
    }
    
    // Update PEQ enabled status
    btnPeqOnOff.selected = peqEnabled[currentChannel];
    [self updatePeqPlot:btnPeqOnOff.selected];
    
    // Update ball 1-4 status
    if (btnPeq1.selected) {
        btnBall1.hidden = NO;
    } else {
        btnBall1.hidden = YES;
    }
    if (btnPeq2.selected) {
        btnBall2.hidden = NO;
    } else {
        btnBall2.hidden = YES;
    }
    if (btnPeq3.selected) {
        btnBall3.hidden = NO;
    } else {
        btnBall3.hidden = YES;
    }
    if (btnPeq4.selected) {
        btnBall4.hidden = NO;
    } else {
        btnBall4.hidden = YES;
    }
    
    if (valueChanged) {
        [self updatePeqFrame];
    }
}

- (void)processMultiReply:(EqMultiPagePacket *)pkt
{
    BOOL valueChanged = NO;
    
    if ([self.view isHidden]) {
        NSLog(@"processMultiReply: view is hidden!");
        return;
    }
    
    if (peqLock) {
        NSLog(@"processMultiReply: peqLock");
        return;
    }
    
    _multi1Ctrl = pkt->multi_1_ctrl;
    _multi2Ctrl = pkt->multi_2_ctrl;
    _multi3Ctrl = pkt->multi_3_ctrl;
    _multi4Ctrl = pkt->multi_4_ctrl;
    _multiMeter = pkt->multi_meter_pre_post;
    
    switch (currentChannel) {
        case CHANNEL_MULTI_1:
        {
            if (peq1G[currentChannel] != pkt->multi_1_eq_1_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_MULTI1_EQ1_DB dspId:DSP_7]) {
                peq1G[currentChannel] = pkt->multi_1_eq_1_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq1F[currentChannel] != pkt->multi_1_eq_1_freq && ![viewController sendCommandExists:CMD_MULTI1_EQ1_FREQ dspId:DSP_7]) {
                peq1F[currentChannel] = pkt->multi_1_eq_1_freq;
                valueChanged = YES;
            }
            if (peq1Q[currentChannel] != pkt->multi_1_eq_1_q && ![viewController sendCommandExists:CMD_MULTI1_EQ1_Q dspId:DSP_7]) {
                peq1Q[currentChannel] = pkt->multi_1_eq_1_q;
                [self updatePeq1Filter];
                valueChanged = YES;
            }
            if (peq2G[currentChannel] != pkt->multi_1_eq_2_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_MULTI1_EQ2_DB dspId:DSP_7]) {
                peq2G[currentChannel] = pkt->multi_1_eq_2_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq2F[currentChannel] != pkt->multi_1_eq_2_freq && ![viewController sendCommandExists:CMD_MULTI1_EQ2_FREQ dspId:DSP_7]) {
                peq2F[currentChannel] = pkt->multi_1_eq_2_freq;
                valueChanged = YES;
            }
            if (peq2Q[currentChannel] != pkt->multi_1_eq_2_q && ![viewController sendCommandExists:CMD_MULTI1_EQ2_Q dspId:DSP_7]) {
                peq2Q[currentChannel] = pkt->multi_1_eq_2_q;
                valueChanged = YES;
            }
            if (peq3G[currentChannel] != pkt->multi_1_eq_3_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_MULTI1_EQ3_DB dspId:DSP_7]) {
                peq3G[currentChannel] = pkt->multi_1_eq_3_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq3F[currentChannel] != pkt->multi_1_eq_3_freq && ![viewController sendCommandExists:CMD_MULTI1_EQ3_FREQ dspId:DSP_7]) {
                peq3F[currentChannel] = pkt->multi_1_eq_3_freq;
                valueChanged = YES;
            }
            if (peq3Q[currentChannel] != pkt->multi_1_eq_3_q && ![viewController sendCommandExists:CMD_MULTI1_EQ3_Q dspId:DSP_7]) {
                peq3Q[currentChannel] = pkt->multi_1_eq_3_q;
                valueChanged = YES;
            }
            if (peq4G[currentChannel] != pkt->multi_1_eq_4_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_MULTI1_EQ4_DB dspId:DSP_7]) {
                peq4G[currentChannel] = pkt->multi_1_eq_4_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq4F[currentChannel] != pkt->multi_1_eq_4_freq && ![viewController sendCommandExists:CMD_MULTI1_EQ4_FREQ dspId:DSP_7]) {
                peq4F[currentChannel] = pkt->multi_1_eq_4_freq;
                valueChanged = YES;
            }
            if (peq4Q[currentChannel] != pkt->multi_1_eq_4_q && ![viewController sendCommandExists:CMD_MULTI1_EQ4_Q dspId:DSP_7]) {
                peq4Q[currentChannel] = pkt->multi_1_eq_4_q;
                [self updatePeq4Filter];
                valueChanged = YES;
            }
            meterIn.progress = ((float)pkt->multi_1_meter_eq_in/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterOut.progress = ((float)pkt->multi_1_meter_eq_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            if (!sliderLock) {
                slider.value = pkt->multi_1_fader;
                [self onSliderAction:nil];
            }
            BOOL sliderOn = pkt->multi_1_ctrl & 0x01;
            if (sliderOn != btnOn.selected && ![viewController sendCommandExists:CMD_MULTI_1_CTRL dspId:DSP_7]) {
                btnOn.selected = sliderOn;
            }
            BOOL sliderMeter = pkt->multi_meter_pre_post & 0x01;
            if (sliderMeter != btnMeterPrePost.selected && ![viewController sendCommandExists:CMD_MULTI_METER dspId:DSP_7]) {
                btnMeterPrePost.selected = sliderMeter;
            }
            int peqEnableValue = pkt->multi_1_ctrl;
            if (btnPeq1.selected != ((peqEnableValue & 0x08) >> 3) && ![viewController sendCommandExists:CMD_MULTI_1_CTRL dspId:DSP_7]) {
                btnPeq1.selected = peq1Enabled[currentChannel] = (peqEnableValue & 0x08) >> 3;
                valueChanged = YES;
            }
            if (btnPeq2.selected != ((peqEnableValue & 0x10) >> 4) && ![viewController sendCommandExists:CMD_MULTI_1_CTRL dspId:DSP_7]) {
                btnPeq2.selected = peq2Enabled[currentChannel] = (peqEnableValue & 0x10) >> 4;
                valueChanged = YES;
            }
            if (btnPeq3.selected != ((peqEnableValue & 0x20) >> 5) && ![viewController sendCommandExists:CMD_MULTI_1_CTRL dspId:DSP_7]) {
                btnPeq3.selected = peq3Enabled[currentChannel] = (peqEnableValue & 0x20) >> 5;
                valueChanged = YES;
            }
            if (btnPeq4.selected != ((peqEnableValue & 0x40) >> 6) && ![viewController sendCommandExists:CMD_MULTI_1_CTRL dspId:DSP_7]) {
                btnPeq4.selected = peq4Enabled[currentChannel] = (peqEnableValue & 0x40) >> 6;
                valueChanged = YES;
            }
        }
            break;
        case CHANNEL_MULTI_2:
        {
            if (peq1G[currentChannel] != pkt->multi_2_eq_1_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_MULTI2_EQ1_DB dspId:DSP_7]) {
                peq1G[currentChannel] = pkt->multi_2_eq_1_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq1F[currentChannel] != pkt->multi_2_eq_1_freq && ![viewController sendCommandExists:CMD_MULTI2_EQ1_FREQ dspId:DSP_7]) {
                peq1F[currentChannel] = pkt->multi_2_eq_1_freq;
                valueChanged = YES;
            }
            if (peq1Q[currentChannel] != pkt->multi_2_eq_1_q && ![viewController sendCommandExists:CMD_MULTI2_EQ1_Q dspId:DSP_7]) {
                peq1Q[currentChannel] = pkt->multi_2_eq_1_q;
                [self updatePeq1Filter];
                valueChanged = YES;
            }
            if (peq2G[currentChannel] != pkt->multi_2_eq_2_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_MULTI2_EQ2_DB dspId:DSP_7]) {
                peq2G[currentChannel] = pkt->multi_2_eq_2_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq2F[currentChannel] != pkt->multi_2_eq_2_freq && ![viewController sendCommandExists:CMD_MULTI2_EQ2_FREQ dspId:DSP_7]) {
                peq2F[currentChannel] = pkt->multi_2_eq_2_freq;
                valueChanged = YES;
            }
            if (peq2Q[currentChannel] != pkt->multi_2_eq_2_q && ![viewController sendCommandExists:CMD_MULTI2_EQ2_Q dspId:DSP_7]) {
                peq2Q[currentChannel] = pkt->multi_2_eq_2_q;
                valueChanged = YES;
            }
            if (peq3G[currentChannel] != pkt->multi_2_eq_3_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_MULTI2_EQ3_DB dspId:DSP_7]) {
                peq3G[currentChannel] = pkt->multi_2_eq_3_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq3F[currentChannel] != pkt->multi_2_eq_3_freq && ![viewController sendCommandExists:CMD_MULTI2_EQ3_FREQ dspId:DSP_7]) {
                peq3F[currentChannel] = pkt->multi_2_eq_3_freq;
                valueChanged = YES;
            }
            if (peq3Q[currentChannel] != pkt->multi_2_eq_3_q && ![viewController sendCommandExists:CMD_MULTI2_EQ3_Q dspId:DSP_7]) {
                peq3Q[currentChannel] = pkt->multi_2_eq_3_q;
                valueChanged = YES;
            }
            if (peq4G[currentChannel] != pkt->multi_2_eq_4_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_MULTI2_EQ4_DB dspId:DSP_7]) {
                peq4G[currentChannel] = pkt->multi_2_eq_4_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq4F[currentChannel] != pkt->multi_2_eq_4_freq && ![viewController sendCommandExists:CMD_MULTI2_EQ4_FREQ dspId:DSP_7]) {
                peq4F[currentChannel] = pkt->multi_2_eq_4_freq;
                valueChanged = YES;
            }
            if (peq4Q[currentChannel] != pkt->multi_2_eq_4_q && ![viewController sendCommandExists:CMD_MULTI2_EQ4_Q dspId:DSP_7]) {
                peq4Q[currentChannel] = pkt->multi_2_eq_4_q;
                [self updatePeq4Filter];
                valueChanged = YES;
            }
            meterIn.progress = ((float)pkt->multi_2_meter_eq_in/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterOut.progress = ((float)pkt->multi_2_meter_eq_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            if (!sliderLock) {
                slider.value = pkt->multi_2_fader;
                [self onSliderAction:nil];
            }
            BOOL sliderOn = pkt->multi_2_ctrl & 0x01;
            if (sliderOn != btnOn.selected && ![viewController sendCommandExists:CMD_MULTI_2_CTRL dspId:DSP_7]) {
                btnOn.selected = sliderOn;
            }
            BOOL sliderMeter = (pkt->multi_meter_pre_post & 0x02) >> 1;
            if (sliderMeter != btnMeterPrePost.selected && ![viewController sendCommandExists:CMD_MULTI_METER dspId:DSP_7]) {
                btnMeterPrePost.selected = sliderMeter;
            }
            int peqEnableValue = pkt->multi_2_ctrl;
            if (btnPeq1.selected != ((peqEnableValue & 0x08) >> 3) && ![viewController sendCommandExists:CMD_MULTI_2_CTRL dspId:DSP_7]) {
                btnPeq1.selected = peq1Enabled[currentChannel] = (peqEnableValue & 0x08) >> 3;
                valueChanged = YES;
            }
            if (btnPeq2.selected != ((peqEnableValue & 0x10) >> 4) && ![viewController sendCommandExists:CMD_MULTI_2_CTRL dspId:DSP_7]) {
                btnPeq2.selected = peq2Enabled[currentChannel] = (peqEnableValue & 0x10) >> 4;
                valueChanged = YES;
            }
            if (btnPeq3.selected != ((peqEnableValue & 0x20) >> 5) && ![viewController sendCommandExists:CMD_MULTI_2_CTRL dspId:DSP_7]) {
                btnPeq3.selected = peq3Enabled[currentChannel] = (peqEnableValue & 0x20) >> 5;
                valueChanged = YES;
            }
            if (btnPeq4.selected != ((peqEnableValue & 0x40) >> 6) && ![viewController sendCommandExists:CMD_MULTI_2_CTRL dspId:DSP_7]) {
                btnPeq4.selected = peq4Enabled[currentChannel] = (peqEnableValue & 0x40) >> 6;
                valueChanged = YES;
            }
        }
            break;
        case CHANNEL_MULTI_3:
        {
            if (peq1G[currentChannel] != pkt->multi_3_eq_1_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_MULTI3_EQ1_DB dspId:DSP_7]) {
                peq1G[currentChannel] = pkt->multi_3_eq_1_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq1F[currentChannel] != pkt->multi_3_eq_1_freq && ![viewController sendCommandExists:CMD_MULTI3_EQ1_FREQ dspId:DSP_7]) {
                peq1F[currentChannel] = pkt->multi_3_eq_1_freq;
                valueChanged = YES;
            }
            if (peq1Q[currentChannel] != pkt->multi_3_eq_1_q && ![viewController sendCommandExists:CMD_MULTI3_EQ1_Q dspId:DSP_7]) {
                peq1Q[currentChannel] = pkt->multi_3_eq_1_q;
                [self updatePeq1Filter];
                valueChanged = YES;
            }
            if (peq2G[currentChannel] != pkt->multi_3_eq_2_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_MULTI3_EQ2_DB dspId:DSP_7]) {
                peq2G[currentChannel] = pkt->multi_3_eq_2_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq2F[currentChannel] != pkt->multi_3_eq_2_freq && ![viewController sendCommandExists:CMD_MULTI3_EQ2_FREQ dspId:DSP_7]) {
                peq2F[currentChannel] = pkt->multi_3_eq_2_freq;
                valueChanged = YES;
            }
            if (peq2Q[currentChannel] != pkt->multi_3_eq_2_q && ![viewController sendCommandExists:CMD_MULTI3_EQ2_Q dspId:DSP_7]) {
                peq2Q[currentChannel] = pkt->multi_3_eq_2_q;
                valueChanged = YES;
            }
            if (peq3G[currentChannel] != pkt->multi_3_eq_3_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_MULTI3_EQ3_DB dspId:DSP_7]) {
                peq3G[currentChannel] = pkt->multi_3_eq_3_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq3F[currentChannel] != pkt->multi_3_eq_3_freq && ![viewController sendCommandExists:CMD_MULTI3_EQ3_FREQ dspId:DSP_7]) {
                peq3F[currentChannel] = pkt->multi_3_eq_3_freq;
                valueChanged = YES;
            }
            if (peq3Q[currentChannel] != pkt->multi_3_eq_3_q && ![viewController sendCommandExists:CMD_MULTI3_EQ3_Q dspId:DSP_7]) {
                peq3Q[currentChannel] = pkt->multi_3_eq_3_q;
                valueChanged = YES;
            }
            if (peq4G[currentChannel] != pkt->multi_3_eq_4_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_MULTI3_EQ4_DB dspId:DSP_7]) {
                peq4G[currentChannel] = pkt->multi_3_eq_4_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq4F[currentChannel] != pkt->multi_3_eq_4_freq && ![viewController sendCommandExists:CMD_MULTI3_EQ4_FREQ dspId:DSP_7]) {
                peq4F[currentChannel] = pkt->multi_3_eq_4_freq;
                valueChanged = YES;
            }
            if (peq4Q[currentChannel] != pkt->multi_3_eq_4_q && ![viewController sendCommandExists:CMD_MULTI3_EQ4_Q dspId:DSP_7]) {
                peq4Q[currentChannel] = pkt->multi_3_eq_4_q;
                [self updatePeq4Filter];
                valueChanged = YES;
            }
            meterIn.progress = ((float)pkt->multi_3_meter_eq_in/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterOut.progress = ((float)pkt->multi_3_meter_eq_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            if (!sliderLock) {
                slider.value = pkt->multi_3_fader;
                [self onSliderAction:nil];
            }
            BOOL sliderOn = pkt->multi_3_ctrl & 0x01;
            if (sliderOn != btnOn.selected && ![viewController sendCommandExists:CMD_MULTI_3_CTRL dspId:DSP_7]) {
                btnOn.selected = sliderOn;
            }
            BOOL sliderMeter = (pkt->multi_meter_pre_post & 0x04) >> 2;
            if (sliderMeter != btnMeterPrePost.selected && ![viewController sendCommandExists:CMD_MULTI_METER dspId:DSP_7]) {
                btnMeterPrePost.selected = sliderMeter;
            }
            int peqEnableValue = pkt->multi_3_ctrl;
            if (btnPeq1.selected != ((peqEnableValue & 0x08) >> 3) && ![viewController sendCommandExists:CMD_MULTI_3_CTRL dspId:DSP_7]) {
                btnPeq1.selected = peq1Enabled[currentChannel] = (peqEnableValue & 0x08) >> 3;
                valueChanged = YES;
            }
            if (btnPeq2.selected != ((peqEnableValue & 0x10) >> 4) && ![viewController sendCommandExists:CMD_MULTI_3_CTRL dspId:DSP_7]) {
                btnPeq2.selected = peq2Enabled[currentChannel] = (peqEnableValue & 0x10) >> 4;
                valueChanged = YES;
            }
            if (btnPeq3.selected != ((peqEnableValue & 0x20) >> 5) && ![viewController sendCommandExists:CMD_MULTI_3_CTRL dspId:DSP_7]) {
                btnPeq3.selected = peq3Enabled[currentChannel] = (peqEnableValue & 0x20) >> 5;
                valueChanged = YES;
            }
            if (btnPeq4.selected != ((peqEnableValue & 0x40) >> 6) && ![viewController sendCommandExists:CMD_MULTI_3_CTRL dspId:DSP_7]) {
                btnPeq4.selected = peq4Enabled[currentChannel] = (peqEnableValue & 0x40) >> 6;
                valueChanged = YES;
            }
        }
            break;
        case CHANNEL_MULTI_4:
        {
            if (peq1G[currentChannel] != pkt->multi_4_eq_1_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_MULTI4_EQ1_DB dspId:DSP_7]) {
                peq1G[currentChannel] = pkt->multi_4_eq_1_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq1F[currentChannel] != pkt->multi_4_eq_1_freq && ![viewController sendCommandExists:CMD_MULTI4_EQ1_FREQ dspId:DSP_7]) {
                peq1F[currentChannel] = pkt->multi_4_eq_1_freq;
                valueChanged = YES;
            }
            if (peq1Q[currentChannel] != pkt->multi_4_eq_1_q && ![viewController sendCommandExists:CMD_MULTI4_EQ1_Q dspId:DSP_7]) {
                peq1Q[currentChannel] = pkt->multi_4_eq_1_q;
                [self updatePeq1Filter];
                valueChanged = YES;
            }
            if (peq2G[currentChannel] != pkt->multi_4_eq_2_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_MULTI4_EQ2_DB dspId:DSP_7]) {
                peq2G[currentChannel] = pkt->multi_4_eq_2_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq2F[currentChannel] != pkt->multi_4_eq_2_freq && ![viewController sendCommandExists:CMD_MULTI4_EQ2_FREQ dspId:DSP_7]) {
                peq2F[currentChannel] = pkt->multi_4_eq_2_freq;
                valueChanged = YES;
            }
            if (peq2Q[currentChannel] != pkt->multi_4_eq_2_q && ![viewController sendCommandExists:CMD_MULTI4_EQ2_Q dspId:DSP_7]) {
                peq2Q[currentChannel] = pkt->multi_4_eq_2_q;
                valueChanged = YES;
            }
            if (peq3G[currentChannel] != pkt->multi_4_eq_3_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_MULTI4_EQ3_DB dspId:DSP_7]) {
                peq3G[currentChannel] = pkt->multi_4_eq_3_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq3F[currentChannel] != pkt->multi_4_eq_3_freq && ![viewController sendCommandExists:CMD_MULTI4_EQ3_FREQ dspId:DSP_7]) {
                peq3F[currentChannel] = pkt->multi_4_eq_3_freq;
                valueChanged = YES;
            }
            if (peq3Q[currentChannel] != pkt->multi_4_eq_3_q && ![viewController sendCommandExists:CMD_MULTI4_EQ3_Q dspId:DSP_7]) {
                peq3Q[currentChannel] = pkt->multi_4_eq_3_q;
                valueChanged = YES;
            }
            if (peq4G[currentChannel] != pkt->multi_4_eq_4_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_MULTI4_EQ4_DB dspId:DSP_7]) {
                peq4G[currentChannel] = pkt->multi_4_eq_4_db+EQ_GAIN_OFFSET;
                valueChanged = YES;
            }
            if (peq4F[currentChannel] != pkt->multi_4_eq_4_freq && ![viewController sendCommandExists:CMD_MULTI4_EQ4_FREQ dspId:DSP_7]) {
                peq4F[currentChannel] = pkt->multi_4_eq_4_freq;
                valueChanged = YES;
            }
            if (peq4Q[currentChannel] != pkt->multi_4_eq_4_q && ![viewController sendCommandExists:CMD_MULTI4_EQ4_Q dspId:DSP_7]) {
                peq4Q[currentChannel] = pkt->multi_4_eq_4_q;
                [self updatePeq4Filter];
                valueChanged = YES;
            }
            meterIn.progress = ((float)pkt->multi_4_meter_eq_in/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterOut.progress = ((float)pkt->multi_4_meter_eq_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            if (!sliderLock) {
                slider.value = pkt->multi_4_fader;
                [self onSliderAction:nil];
            }
            BOOL sliderOn = pkt->multi_4_ctrl & 0x01;
            if (sliderOn != btnOn.selected && ![viewController sendCommandExists:CMD_MULTI_4_CTRL dspId:DSP_7]) {
                btnOn.selected = sliderOn;
            }
            BOOL sliderMeter = (pkt->multi_meter_pre_post & 0x08) >> 3;
            if (sliderMeter != btnMeterPrePost.selected && ![viewController sendCommandExists:CMD_MULTI_METER dspId:DSP_7]) {
                btnMeterPrePost.selected = sliderMeter;
            }
            int peqEnableValue = pkt->multi_4_ctrl;
            if (btnPeq1.selected != ((peqEnableValue & 0x08) >> 3) && ![viewController sendCommandExists:CMD_MULTI_4_CTRL dspId:DSP_7]) {
                btnPeq1.selected = peq1Enabled[currentChannel] = (peqEnableValue & 0x08) >> 3;
                valueChanged = YES;
            }
            if (btnPeq2.selected != ((peqEnableValue & 0x10) >> 4) && ![viewController sendCommandExists:CMD_MULTI_4_CTRL dspId:DSP_7]) {
                btnPeq2.selected = peq2Enabled[currentChannel] = (peqEnableValue & 0x10) >> 4;
                valueChanged = YES;
            }
            if (btnPeq3.selected != ((peqEnableValue & 0x20) >> 5) && ![viewController sendCommandExists:CMD_MULTI_4_CTRL dspId:DSP_7]) {
                btnPeq3.selected = peq3Enabled[currentChannel] = (peqEnableValue & 0x20) >> 5;
                valueChanged = YES;
            }
            if (btnPeq4.selected != ((peqEnableValue & 0x40) >> 6) && ![viewController sendCommandExists:CMD_MULTI_4_CTRL dspId:DSP_7]) {
                btnPeq4.selected = peq4Enabled[currentChannel] = (peqEnableValue & 0x40) >> 6;
                valueChanged = YES;
            }
        }
            break;
        default:
            break;
    }
    
    // Update PEQ enabled status
    btnPeqOnOff.selected = peqEnabled[currentChannel];
    [self updatePeqPlot:btnPeqOnOff.selected];
    
    // Update ball 1-4 status
    if (btnPeq1.selected) {
        btnBall1.hidden = NO;
    } else {
        btnBall1.hidden = YES;
    }
    if (btnPeq2.selected) {
        btnBall2.hidden = NO;
    } else {
        btnBall2.hidden = YES;
    }
    if (btnPeq3.selected) {
        btnBall3.hidden = NO;
    } else {
        btnBall3.hidden = YES;
    }
    if (btnPeq4.selected) {
        btnBall4.hidden = NO;
    } else {
        btnBall4.hidden = YES;
    }
    
    if (valueChanged) {
        [self updatePeqFrame];
    }
}

- (void)processChannelViewPage:(ViewPageChannelPacket *)pkt {

    BOOL valueChanged = NO;
    
    // CHANNEL_CH_1
    {
        if (peq1G[CHANNEL_CH_1] != pkt->ch_1_eq_1_db+EQ_GAIN_OFFSET) {
            peq1G[CHANNEL_CH_1] = pkt->ch_1_eq_1_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq1F[CHANNEL_CH_1] != pkt->ch_1_eq_1_freq) {
            peq1F[CHANNEL_CH_1] = pkt->ch_1_eq_1_freq;
            valueChanged = YES;
        }
        if (peq1Q[CHANNEL_CH_1] != pkt->ch_1_eq_1_q) {
            peq1Q[CHANNEL_CH_1] = pkt->ch_1_eq_1_q;
            if (peq1Q[CHANNEL_CH_1] == PEQ_1_LOW_SHELF) {
                peq1Filter[CHANNEL_CH_1] = EQ_LOW_SHELF;
            } else if (peq1Q[CHANNEL_CH_1] == PEQ_1_HPF) {
                peq1Filter[CHANNEL_CH_1] = EQ_LOW_CUT;
            } else {
                peq1Filter[CHANNEL_CH_1] = EQ_PEAK;
                lastPeq1Q[CHANNEL_CH_1] = peq1Q[CHANNEL_CH_1];
            }
            valueChanged = YES;
        }
        if (peq2G[CHANNEL_CH_1] != pkt->ch_1_eq_2_db+EQ_GAIN_OFFSET) {
            peq2G[CHANNEL_CH_1] = pkt->ch_1_eq_2_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq2F[CHANNEL_CH_1] != pkt->ch_1_eq_2_freq) {
            peq2F[CHANNEL_CH_1] = pkt->ch_1_eq_2_freq;
            valueChanged = YES;
        }
        if (peq2Q[CHANNEL_CH_1] != pkt->ch_1_eq_2_q) {
            peq2Q[CHANNEL_CH_1] = pkt->ch_1_eq_2_q;
            valueChanged = YES;
        }
        if (peq3G[CHANNEL_CH_1] != pkt->ch_1_eq_3_db+EQ_GAIN_OFFSET) {
            peq3G[CHANNEL_CH_1] = pkt->ch_1_eq_3_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq3F[CHANNEL_CH_1] != pkt->ch_1_eq_3_freq) {
            peq3F[CHANNEL_CH_1] = pkt->ch_1_eq_3_freq;
            valueChanged = YES;
        }
        if (peq3Q[CHANNEL_CH_1] != pkt->ch_1_eq_3_q) {
            peq3Q[CHANNEL_CH_1] = pkt->ch_1_eq_3_q;
            valueChanged = YES;
        }
        if (peq4G[CHANNEL_CH_1] != pkt->ch_1_eq_4_db+EQ_GAIN_OFFSET) {
            peq4G[CHANNEL_CH_1] = pkt->ch_1_eq_4_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq4F[CHANNEL_CH_1] != pkt->ch_1_eq_4_freq) {
            peq4F[CHANNEL_CH_1] = pkt->ch_1_eq_4_freq;
            valueChanged = YES;
        }
        if (peq4Q[CHANNEL_CH_1] != pkt->ch_1_eq_4_q) {
            peq4Q[CHANNEL_CH_1] = pkt->ch_1_eq_4_q;
            if (peq4Q[CHANNEL_CH_1] == PEQ_4_HIGH_SHELF) {
                peq4Filter[CHANNEL_CH_1] = EQ_HIGH_SHELF;
            } else if (peq4Q[CHANNEL_CH_1] == PEQ_4_LPF) {
                peq4Filter[CHANNEL_CH_1] = EQ_HIGH_CUT;
            } else {
                peq4Filter[CHANNEL_CH_1] = EQ_PEAK;
                lastPeq4Q[CHANNEL_CH_1] = peq4Q[CHANNEL_CH_1];
            }
            valueChanged = YES;
        }
        int peqEnableValue = pkt->channel_1_ctrl;
        if (peq1Enabled[CHANNEL_CH_1] != ((peqEnableValue & 0x08) >> 3)) {
            peq1Enabled[CHANNEL_CH_1] = (peqEnableValue & 0x08) >> 3;
            valueChanged = YES;
        }
        if (peq2Enabled[CHANNEL_CH_1] != ((peqEnableValue & 0x10) >> 4)) {
            peq2Enabled[CHANNEL_CH_1] = (peqEnableValue & 0x10) >> 4;
            valueChanged = YES;
        }
        if (peq3Enabled[CHANNEL_CH_1] != ((peqEnableValue & 0x20) >> 5)) {
            peq3Enabled[CHANNEL_CH_1] = (peqEnableValue & 0x20) >> 5;
            valueChanged = YES;
        }
        if (peq4Enabled[CHANNEL_CH_1] != ((peqEnableValue & 0x40) >> 6)) {
            peq4Enabled[CHANNEL_CH_1] = (peqEnableValue & 0x40) >> 6;
            valueChanged = YES;
        }
        if (peqEnabled[CHANNEL_CH_1] != ((peqEnableValue & 0x80) >> 7)) {
            peqEnabled[CHANNEL_CH_1] = (peqEnableValue & 0x80) >> 7;
            valueChanged = YES;
        }
    }
    
    if (valueChanged) {
        [self updatePeqPlot:peqEnabled[CHANNEL_CH_1]];
        [self updatePeqForChannel:CHANNEL_CH_1];
        valueChanged = NO;
    }
    
    // CHANNEL_CH_2
    {
        if (peq1G[CHANNEL_CH_2] != pkt->ch_2_eq_1_db+EQ_GAIN_OFFSET) {
            peq1G[CHANNEL_CH_2] = pkt->ch_2_eq_1_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq1F[CHANNEL_CH_2] != pkt->ch_2_eq_1_freq) {
            peq1F[CHANNEL_CH_2] = pkt->ch_2_eq_1_freq;
            valueChanged = YES;
        }
        if (peq1Q[CHANNEL_CH_2] != pkt->ch_2_eq_1_q) {
            peq1Q[CHANNEL_CH_2] = pkt->ch_2_eq_1_q;
            if (peq1Q[CHANNEL_CH_2] == PEQ_1_LOW_SHELF) {
                peq1Filter[CHANNEL_CH_2] = EQ_LOW_SHELF;
            } else if (peq1Q[CHANNEL_CH_2] == PEQ_1_HPF) {
                peq1Filter[CHANNEL_CH_2] = EQ_LOW_CUT;
            } else {
                peq1Filter[CHANNEL_CH_2] = EQ_PEAK;
                lastPeq1Q[CHANNEL_CH_2] = peq1Q[CHANNEL_CH_2];
            }
            valueChanged = YES;
        }
        if (peq2G[CHANNEL_CH_2] != pkt->ch_2_eq_2_db+EQ_GAIN_OFFSET) {
            peq2G[CHANNEL_CH_2] = pkt->ch_2_eq_2_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq2F[CHANNEL_CH_2] != pkt->ch_2_eq_2_freq) {
            peq2F[CHANNEL_CH_2] = pkt->ch_2_eq_2_freq;
            valueChanged = YES;
        }
        if (peq2Q[CHANNEL_CH_2] != pkt->ch_2_eq_2_q) {
            peq2Q[CHANNEL_CH_2] = pkt->ch_2_eq_2_q;
            valueChanged = YES;
        }
        if (peq3G[CHANNEL_CH_2] != pkt->ch_2_eq_3_db+EQ_GAIN_OFFSET) {
            peq3G[CHANNEL_CH_2] = pkt->ch_2_eq_3_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq3F[CHANNEL_CH_2] != pkt->ch_2_eq_3_freq) {
            peq3F[CHANNEL_CH_2] = pkt->ch_2_eq_3_freq;
            valueChanged = YES;
        }
        if (peq3Q[CHANNEL_CH_2] != pkt->ch_2_eq_3_q) {
            peq3Q[CHANNEL_CH_2] = pkt->ch_2_eq_3_q;
            valueChanged = YES;
        }
        if (peq4G[CHANNEL_CH_2] != pkt->ch_2_eq_4_db+EQ_GAIN_OFFSET) {
            peq4G[CHANNEL_CH_2] = pkt->ch_2_eq_4_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq4F[CHANNEL_CH_2] != pkt->ch_2_eq_4_freq) {
            peq4F[CHANNEL_CH_2] = pkt->ch_2_eq_4_freq;
            valueChanged = YES;
        }
        if (peq4Q[CHANNEL_CH_2] != pkt->ch_2_eq_4_q) {
            peq4Q[CHANNEL_CH_2] = pkt->ch_2_eq_4_q;
            if (peq4Q[CHANNEL_CH_2] == PEQ_4_HIGH_SHELF) {
                peq4Filter[CHANNEL_CH_2] = EQ_HIGH_SHELF;
            } else if (peq4Q[CHANNEL_CH_2] == PEQ_4_LPF) {
                peq4Filter[CHANNEL_CH_2] = EQ_HIGH_CUT;
            } else {
                peq4Filter[CHANNEL_CH_2] = EQ_PEAK;
                lastPeq4Q[CHANNEL_CH_2] = peq4Q[CHANNEL_CH_2];
            }
            valueChanged = YES;
        }
        int peqEnableValue = pkt->channel_2_ctrl;
        if (peq1Enabled[CHANNEL_CH_2] != ((peqEnableValue & 0x08) >> 3)) {
            peq1Enabled[CHANNEL_CH_2] = (peqEnableValue & 0x08) >> 3;
            valueChanged = YES;
        }
        if (peq2Enabled[CHANNEL_CH_2] != ((peqEnableValue & 0x10) >> 4)) {
            peq2Enabled[CHANNEL_CH_2] = (peqEnableValue & 0x10) >> 4;
            valueChanged = YES;
        }
        if (peq3Enabled[CHANNEL_CH_2] != ((peqEnableValue & 0x20) >> 5)) {
            peq3Enabled[CHANNEL_CH_2] = (peqEnableValue & 0x20) >> 5;
            valueChanged = YES;
        }
        if (peq4Enabled[CHANNEL_CH_2] != ((peqEnableValue & 0x40) >> 6)) {
            peq4Enabled[CHANNEL_CH_2] = (peqEnableValue & 0x40) >> 6;
            valueChanged = YES;
        }
        if (peqEnabled[CHANNEL_CH_2] != ((peqEnableValue & 0x80) >> 7)) {
            peqEnabled[CHANNEL_CH_2] = (peqEnableValue & 0x80) >> 7;
            valueChanged = YES;
        }
    }
    
    if (valueChanged) {
        [self updatePeqPlot:peqEnabled[CHANNEL_CH_2]];
        [self updatePeqForChannel:CHANNEL_CH_2];
        valueChanged = NO;
    }
    
    // CHANNEL_CH_3
    {
        if (peq1G[CHANNEL_CH_3] != pkt->ch_3_eq_1_db+EQ_GAIN_OFFSET) {
            peq1G[CHANNEL_CH_3] = pkt->ch_3_eq_1_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq1F[CHANNEL_CH_3] != pkt->ch_3_eq_1_freq) {
            peq1F[CHANNEL_CH_3] = pkt->ch_3_eq_1_freq;
            valueChanged = YES;
        }
        if (peq1Q[CHANNEL_CH_3] != pkt->ch_3_eq_1_q) {
            peq1Q[CHANNEL_CH_3] = pkt->ch_3_eq_1_q;
            if (peq1Q[CHANNEL_CH_3] == PEQ_1_LOW_SHELF) {
                peq1Filter[CHANNEL_CH_3] = EQ_LOW_SHELF;
            } else if (peq1Q[CHANNEL_CH_3] == PEQ_1_HPF) {
                peq1Filter[CHANNEL_CH_3] = EQ_LOW_CUT;
            } else {
                peq1Filter[CHANNEL_CH_3] = EQ_PEAK;
                lastPeq1Q[CHANNEL_CH_3] = peq1Q[CHANNEL_CH_3];
            }
            valueChanged = YES;
        }
        if (peq2G[CHANNEL_CH_3] != pkt->ch_3_eq_2_db+EQ_GAIN_OFFSET) {
            peq2G[CHANNEL_CH_3] = pkt->ch_3_eq_2_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq2F[CHANNEL_CH_3] != pkt->ch_3_eq_2_freq) {
            peq2F[CHANNEL_CH_3] = pkt->ch_3_eq_2_freq;
            valueChanged = YES;
        }
        if (peq2Q[CHANNEL_CH_3] != pkt->ch_3_eq_2_q) {
            peq2Q[CHANNEL_CH_3] = pkt->ch_3_eq_2_q;
            valueChanged = YES;
        }
        if (peq3G[CHANNEL_CH_3] != pkt->ch_3_eq_3_db+EQ_GAIN_OFFSET) {
            peq3G[CHANNEL_CH_3] = pkt->ch_3_eq_3_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq3F[CHANNEL_CH_3] != pkt->ch_3_eq_3_freq) {
            peq3F[CHANNEL_CH_3] = pkt->ch_3_eq_3_freq;
            valueChanged = YES;
        }
        if (peq3Q[CHANNEL_CH_3] != pkt->ch_3_eq_3_q) {
            peq3Q[CHANNEL_CH_3] = pkt->ch_3_eq_3_q;
            valueChanged = YES;
        }
        if (peq4G[CHANNEL_CH_3] != pkt->ch_3_eq_4_db+EQ_GAIN_OFFSET) {
            peq4G[CHANNEL_CH_3] = pkt->ch_3_eq_4_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq4F[CHANNEL_CH_3] != pkt->ch_3_eq_4_freq) {
            peq4F[CHANNEL_CH_3] = pkt->ch_3_eq_4_freq;
            valueChanged = YES;
        }
        if (peq4Q[CHANNEL_CH_3] != pkt->ch_3_eq_4_q) {
            peq4Q[CHANNEL_CH_3] = pkt->ch_3_eq_4_q;
            if (peq4Q[CHANNEL_CH_3] == PEQ_4_HIGH_SHELF) {
                peq4Filter[CHANNEL_CH_3] = EQ_HIGH_SHELF;
            } else if (peq4Q[CHANNEL_CH_3] == PEQ_4_LPF) {
                peq4Filter[CHANNEL_CH_3] = EQ_HIGH_CUT;
            } else {
                peq4Filter[CHANNEL_CH_3] = EQ_PEAK;
                lastPeq4Q[CHANNEL_CH_3] = peq4Q[CHANNEL_CH_3];
            }
            valueChanged = YES;
        }
        int peqEnableValue = pkt->channel_3_ctrl;
        if (peq1Enabled[CHANNEL_CH_3] != ((peqEnableValue & 0x08) >> 3)) {
            peq1Enabled[CHANNEL_CH_3] = (peqEnableValue & 0x08) >> 3;
            valueChanged = YES;
        }
        if (peq2Enabled[CHANNEL_CH_3] != ((peqEnableValue & 0x10) >> 4)) {
            peq2Enabled[CHANNEL_CH_3] = (peqEnableValue & 0x10) >> 4;
            valueChanged = YES;
        }
        if (peq3Enabled[CHANNEL_CH_3] != ((peqEnableValue & 0x20) >> 5)) {
            peq3Enabled[CHANNEL_CH_3] = (peqEnableValue & 0x20) >> 5;
            valueChanged = YES;
        }
        if (peq4Enabled[CHANNEL_CH_3] != ((peqEnableValue & 0x40) >> 6)) {
            peq4Enabled[CHANNEL_CH_3] = (peqEnableValue & 0x40) >> 6;
            valueChanged = YES;
        }
        if (peqEnabled[CHANNEL_CH_3] != ((peqEnableValue & 0x80) >> 7)) {
            peqEnabled[CHANNEL_CH_3] = (peqEnableValue & 0x80) >> 7;
            valueChanged = YES;
        }
    }
    
    if (valueChanged) {
        [self updatePeqPlot:peqEnabled[CHANNEL_CH_3]];
        [self updatePeqForChannel:CHANNEL_CH_3];
        valueChanged = NO;
    }
    
    
    // CHANNEL_CH_4
    {
        if (peq1G[CHANNEL_CH_4] != pkt->ch_4_eq_1_db+EQ_GAIN_OFFSET) {
            peq1G[CHANNEL_CH_4] = pkt->ch_4_eq_1_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq1F[CHANNEL_CH_4] != pkt->ch_4_eq_1_freq) {
            peq1F[CHANNEL_CH_4] = pkt->ch_4_eq_1_freq;
            valueChanged = YES;
        }
        if (peq1Q[CHANNEL_CH_4] != pkt->ch_4_eq_1_q) {
            peq1Q[CHANNEL_CH_4] = pkt->ch_4_eq_1_q;
            if (peq1Q[CHANNEL_CH_4] == PEQ_1_LOW_SHELF) {
                peq1Filter[CHANNEL_CH_4] = EQ_LOW_SHELF;
            } else if (peq1Q[CHANNEL_CH_4] == PEQ_1_HPF) {
                peq1Filter[CHANNEL_CH_4] = EQ_LOW_CUT;
            } else {
                peq1Filter[CHANNEL_CH_4] = EQ_PEAK;
                lastPeq1Q[CHANNEL_CH_4] = peq1Q[CHANNEL_CH_4];
            }
            valueChanged = YES;
        }
        if (peq2G[CHANNEL_CH_4] != pkt->ch_4_eq_2_db+EQ_GAIN_OFFSET) {
            peq2G[CHANNEL_CH_4] = pkt->ch_4_eq_2_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq2F[CHANNEL_CH_4] != pkt->ch_4_eq_2_freq) {
            peq2F[CHANNEL_CH_4] = pkt->ch_4_eq_2_freq;
            valueChanged = YES;
        }
        if (peq2Q[CHANNEL_CH_4] != pkt->ch_4_eq_2_q) {
            peq2Q[CHANNEL_CH_4] = pkt->ch_4_eq_2_q;
            valueChanged = YES;
        }
        if (peq3G[CHANNEL_CH_4] != pkt->ch_4_eq_3_db+EQ_GAIN_OFFSET) {
            peq3G[CHANNEL_CH_4] = pkt->ch_4_eq_3_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq3F[CHANNEL_CH_4] != pkt->ch_4_eq_3_freq) {
            peq3F[CHANNEL_CH_4] = pkt->ch_4_eq_3_freq;
            valueChanged = YES;
        }
        if (peq3Q[CHANNEL_CH_4] != pkt->ch_4_eq_3_q) {
            peq3Q[CHANNEL_CH_4] = pkt->ch_4_eq_3_q;
            valueChanged = YES;
        }
        if (peq4G[CHANNEL_CH_4] != pkt->ch_4_eq_4_db+EQ_GAIN_OFFSET) {
            peq4G[CHANNEL_CH_4] = pkt->ch_4_eq_4_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq4F[CHANNEL_CH_4] != pkt->ch_4_eq_4_freq) {
            peq4F[CHANNEL_CH_4] = pkt->ch_4_eq_4_freq;
            valueChanged = YES;
        }
        if (peq4Q[CHANNEL_CH_4] != pkt->ch_4_eq_4_q) {
            peq4Q[CHANNEL_CH_4] = pkt->ch_4_eq_4_q;
            if (peq4Q[CHANNEL_CH_4] == PEQ_4_HIGH_SHELF) {
                peq4Filter[CHANNEL_CH_4] = EQ_HIGH_SHELF;
            } else if (peq4Q[CHANNEL_CH_4] == PEQ_4_LPF) {
                peq4Filter[CHANNEL_CH_4] = EQ_HIGH_CUT;
            } else {
                peq4Filter[CHANNEL_CH_4] = EQ_PEAK;
                lastPeq4Q[CHANNEL_CH_4] = peq4Q[CHANNEL_CH_4];
            }
            valueChanged = YES;
        }
        int peqEnableValue = pkt->channel_4_ctrl;
        if (peq1Enabled[CHANNEL_CH_4] != ((peqEnableValue & 0x08) >> 3)) {
            peq1Enabled[CHANNEL_CH_4] = (peqEnableValue & 0x08) >> 3;
            valueChanged = YES;
        }
        if (peq2Enabled[CHANNEL_CH_4] != ((peqEnableValue & 0x10) >> 4)) {
            peq2Enabled[CHANNEL_CH_4] = (peqEnableValue & 0x10) >> 4;
            valueChanged = YES;
        }
        if (peq3Enabled[CHANNEL_CH_4] != ((peqEnableValue & 0x20) >> 5)) {
            peq3Enabled[CHANNEL_CH_4] = (peqEnableValue & 0x20) >> 5;
            valueChanged = YES;
        }
        if (peq4Enabled[CHANNEL_CH_4] != ((peqEnableValue & 0x40) >> 6)) {
            peq4Enabled[CHANNEL_CH_4] = (peqEnableValue & 0x40) >> 6;
            valueChanged = YES;
        }
        if (peqEnabled[CHANNEL_CH_4] != ((peqEnableValue & 0x80) >> 7)) {
            peqEnabled[CHANNEL_CH_4] = (peqEnableValue & 0x80) >> 7;
            valueChanged = YES;
        }
    }
    
    if (valueChanged) {
        [self updatePeqPlot:peqEnabled[CHANNEL_CH_4]];
        [self updatePeqForChannel:CHANNEL_CH_4];
        valueChanged = NO;
    }
    
    
    // CHANNEL_CH_5
    {
        if (peq1G[CHANNEL_CH_5] != pkt->ch_5_eq_1_db+EQ_GAIN_OFFSET) {
            peq1G[CHANNEL_CH_5] = pkt->ch_5_eq_1_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq1F[CHANNEL_CH_5] != pkt->ch_5_eq_1_freq) {
            peq1F[CHANNEL_CH_5] = pkt->ch_5_eq_1_freq;
            valueChanged = YES;
        }
        if (peq1Q[CHANNEL_CH_5] != pkt->ch_5_eq_1_q) {
            peq1Q[CHANNEL_CH_5] = pkt->ch_5_eq_1_q;
            if (peq1Q[CHANNEL_CH_5] == PEQ_1_LOW_SHELF) {
                peq1Filter[CHANNEL_CH_5] = EQ_LOW_SHELF;
            } else if (peq1Q[CHANNEL_CH_5] == PEQ_1_HPF) {
                peq1Filter[CHANNEL_CH_5] = EQ_LOW_CUT;
            } else {
                peq1Filter[CHANNEL_CH_5] = EQ_PEAK;
                lastPeq1Q[CHANNEL_CH_5] = peq1Q[CHANNEL_CH_5];
            }
            valueChanged = YES;
        }
        if (peq2G[CHANNEL_CH_5] != pkt->ch_5_eq_2_db+EQ_GAIN_OFFSET) {
            peq2G[CHANNEL_CH_5] = pkt->ch_5_eq_2_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq2F[CHANNEL_CH_5] != pkt->ch_5_eq_2_freq) {
            peq2F[CHANNEL_CH_5] = pkt->ch_5_eq_2_freq;
            valueChanged = YES;
        }
        if (peq2Q[CHANNEL_CH_5] != pkt->ch_5_eq_2_q) {
            peq2Q[CHANNEL_CH_5] = pkt->ch_5_eq_2_q;
            valueChanged = YES;
        }
        if (peq3G[CHANNEL_CH_5] != pkt->ch_5_eq_3_db+EQ_GAIN_OFFSET) {
            peq3G[CHANNEL_CH_5] = pkt->ch_5_eq_3_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq3F[CHANNEL_CH_5] != pkt->ch_5_eq_3_freq) {
            peq3F[CHANNEL_CH_5] = pkt->ch_5_eq_3_freq;
            valueChanged = YES;
        }
        if (peq3Q[CHANNEL_CH_5] != pkt->ch_5_eq_3_q) {
            peq3Q[CHANNEL_CH_5] = pkt->ch_5_eq_3_q;
            valueChanged = YES;
        }
        if (peq4G[CHANNEL_CH_5] != pkt->ch_5_eq_4_db+EQ_GAIN_OFFSET) {
            peq4G[CHANNEL_CH_5] = pkt->ch_5_eq_4_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq4F[CHANNEL_CH_5] != pkt->ch_5_eq_4_freq) {
            peq4F[CHANNEL_CH_5] = pkt->ch_5_eq_4_freq;
            valueChanged = YES;
        }
        if (peq4Q[CHANNEL_CH_5] != pkt->ch_5_eq_4_q) {
            peq4Q[CHANNEL_CH_5] = pkt->ch_5_eq_4_q;
            if (peq4Q[CHANNEL_CH_5] == PEQ_4_HIGH_SHELF) {
                peq4Filter[CHANNEL_CH_5] = EQ_HIGH_SHELF;
            } else if (peq4Q[CHANNEL_CH_5] == PEQ_4_LPF) {
                peq4Filter[CHANNEL_CH_5] = EQ_HIGH_CUT;
            } else {
                peq4Filter[CHANNEL_CH_5] = EQ_PEAK;
                lastPeq4Q[CHANNEL_CH_5] = peq4Q[CHANNEL_CH_5];
            }
            valueChanged = YES;
        }
        int peqEnableValue = pkt->channel_5_ctrl;
        if (peq1Enabled[CHANNEL_CH_5] != ((peqEnableValue & 0x08) >> 3)) {
            peq1Enabled[CHANNEL_CH_5] = (peqEnableValue & 0x08) >> 3;
            valueChanged = YES;
        }
        if (peq2Enabled[CHANNEL_CH_5] != ((peqEnableValue & 0x10) >> 4)) {
            peq2Enabled[CHANNEL_CH_5] = (peqEnableValue & 0x10) >> 4;
            valueChanged = YES;
        }
        if (peq3Enabled[CHANNEL_CH_5] != ((peqEnableValue & 0x20) >> 5)) {
            peq3Enabled[CHANNEL_CH_5] = (peqEnableValue & 0x20) >> 5;
            valueChanged = YES;
        }
        if (peq4Enabled[CHANNEL_CH_5] != ((peqEnableValue & 0x40) >> 6)) {
            peq4Enabled[CHANNEL_CH_5] = (peqEnableValue & 0x40) >> 6;
            valueChanged = YES;
        }
        if (peqEnabled[CHANNEL_CH_5] != ((peqEnableValue & 0x80) >> 7)) {
            peqEnabled[CHANNEL_CH_5] = (peqEnableValue & 0x80) >> 7;
            valueChanged = YES;
        }
    }
    
    if (valueChanged) {
        [self updatePeqPlot:peqEnabled[CHANNEL_CH_5]];
        [self updatePeqForChannel:CHANNEL_CH_5];
        valueChanged = NO;
    }
    
    
    // CHANNEL_CH_6
    {
        if (peq1G[CHANNEL_CH_6] != pkt->ch_6_eq_1_db+EQ_GAIN_OFFSET) {
            peq1G[CHANNEL_CH_6] = pkt->ch_6_eq_1_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq1F[CHANNEL_CH_6] != pkt->ch_6_eq_1_freq) {
            peq1F[CHANNEL_CH_6] = pkt->ch_6_eq_1_freq;
            valueChanged = YES;
        }
        if (peq1Q[CHANNEL_CH_6] != pkt->ch_6_eq_1_q) {
            peq1Q[CHANNEL_CH_6] = pkt->ch_6_eq_1_q;
            if (peq1Q[CHANNEL_CH_6] == PEQ_1_LOW_SHELF) {
                peq1Filter[CHANNEL_CH_6] = EQ_LOW_SHELF;
            } else if (peq1Q[CHANNEL_CH_6] == PEQ_1_HPF) {
                peq1Filter[CHANNEL_CH_6] = EQ_LOW_CUT;
            } else {
                peq1Filter[CHANNEL_CH_6] = EQ_PEAK;
                lastPeq1Q[CHANNEL_CH_6] = peq1Q[CHANNEL_CH_6];
            }
            valueChanged = YES;
        }
        if (peq2G[CHANNEL_CH_6] != pkt->ch_6_eq_2_db+EQ_GAIN_OFFSET) {
            peq2G[CHANNEL_CH_6] = pkt->ch_6_eq_2_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq2F[CHANNEL_CH_6] != pkt->ch_6_eq_2_freq) {
            peq2F[CHANNEL_CH_6] = pkt->ch_6_eq_2_freq;
            valueChanged = YES;
        }
        if (peq2Q[CHANNEL_CH_6] != pkt->ch_6_eq_2_q) {
            peq2Q[CHANNEL_CH_6] = pkt->ch_6_eq_2_q;
            valueChanged = YES;
        }
        if (peq3G[CHANNEL_CH_6] != pkt->ch_6_eq_3_db+EQ_GAIN_OFFSET) {
            peq3G[CHANNEL_CH_6] = pkt->ch_6_eq_3_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq3F[CHANNEL_CH_6] != pkt->ch_6_eq_3_freq) {
            peq3F[CHANNEL_CH_6] = pkt->ch_6_eq_3_freq;
            valueChanged = YES;
        }
        if (peq3Q[CHANNEL_CH_6] != pkt->ch_6_eq_3_q) {
            peq3Q[CHANNEL_CH_6] = pkt->ch_6_eq_3_q;
            valueChanged = YES;
        }
        if (peq4G[CHANNEL_CH_6] != pkt->ch_6_eq_4_db+EQ_GAIN_OFFSET) {
            peq4G[CHANNEL_CH_6] = pkt->ch_6_eq_4_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq4F[CHANNEL_CH_6] != pkt->ch_6_eq_4_freq) {
            peq4F[CHANNEL_CH_6] = pkt->ch_6_eq_4_freq;
            valueChanged = YES;
        }
        if (peq4Q[CHANNEL_CH_6] != pkt->ch_6_eq_4_q) {
            peq4Q[CHANNEL_CH_6] = pkt->ch_6_eq_4_q;
            if (peq4Q[CHANNEL_CH_6] == PEQ_4_HIGH_SHELF) {
                peq4Filter[CHANNEL_CH_6] = EQ_HIGH_SHELF;
            } else if (peq4Q[CHANNEL_CH_6] == PEQ_4_LPF) {
                peq4Filter[CHANNEL_CH_6] = EQ_HIGH_CUT;
            } else {
                peq4Filter[CHANNEL_CH_6] = EQ_PEAK;
                lastPeq4Q[CHANNEL_CH_6] = peq4Q[CHANNEL_CH_6];
            }
            valueChanged = YES;
        }
        int peqEnableValue = pkt->channel_6_ctrl;
        if (peq1Enabled[CHANNEL_CH_6] != ((peqEnableValue & 0x08) >> 3)) {
            peq1Enabled[CHANNEL_CH_6] = (peqEnableValue & 0x08) >> 3;
            valueChanged = YES;
        }
        if (peq2Enabled[CHANNEL_CH_6] != ((peqEnableValue & 0x10) >> 4)) {
            peq2Enabled[CHANNEL_CH_6] = (peqEnableValue & 0x10) >> 4;
            valueChanged = YES;
        }
        if (peq3Enabled[CHANNEL_CH_6] != ((peqEnableValue & 0x20) >> 5)) {
            peq3Enabled[CHANNEL_CH_6] = (peqEnableValue & 0x20) >> 5;
            valueChanged = YES;
        }
        if (peq4Enabled[CHANNEL_CH_6] != ((peqEnableValue & 0x40) >> 6)) {
            peq4Enabled[CHANNEL_CH_6] = (peqEnableValue & 0x40) >> 6;
            valueChanged = YES;
        }
        if (peqEnabled[CHANNEL_CH_6] != ((peqEnableValue & 0x80) >> 7)) {
            peqEnabled[CHANNEL_CH_6] = (peqEnableValue & 0x80) >> 7;
            valueChanged = YES;
        }
    }
    
    if (valueChanged) {
        [self updatePeqPlot:peqEnabled[CHANNEL_CH_6]];
        [self updatePeqForChannel:CHANNEL_CH_6];
        valueChanged = NO;
    }
    
    
    // CHANNEL_CH_7
    {
        if (peq1G[CHANNEL_CH_7] != pkt->ch_7_eq_1_db+EQ_GAIN_OFFSET) {
            peq1G[CHANNEL_CH_7] = pkt->ch_7_eq_1_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq1F[CHANNEL_CH_7] != pkt->ch_7_eq_1_freq) {
            peq1F[CHANNEL_CH_7] = pkt->ch_7_eq_1_freq;
            valueChanged = YES;
        }
        if (peq1Q[CHANNEL_CH_7] != pkt->ch_7_eq_1_q) {
            peq1Q[CHANNEL_CH_7] = pkt->ch_7_eq_1_q;
            if (peq1Q[CHANNEL_CH_7] == PEQ_1_LOW_SHELF) {
                peq1Filter[CHANNEL_CH_7] = EQ_LOW_SHELF;
            } else if (peq1Q[CHANNEL_CH_7] == PEQ_1_HPF) {
                peq1Filter[CHANNEL_CH_7] = EQ_LOW_CUT;
            } else {
                peq1Filter[CHANNEL_CH_7] = EQ_PEAK;
                lastPeq1Q[CHANNEL_CH_7] = peq1Q[CHANNEL_CH_7];
            }
            valueChanged = YES;
        }
        if (peq2G[CHANNEL_CH_7] != pkt->ch_7_eq_2_db+EQ_GAIN_OFFSET) {
            peq2G[CHANNEL_CH_7] = pkt->ch_7_eq_2_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq2F[CHANNEL_CH_7] != pkt->ch_7_eq_2_freq) {
            peq2F[CHANNEL_CH_7] = pkt->ch_7_eq_2_freq;
            valueChanged = YES;
        }
        if (peq2Q[CHANNEL_CH_7] != pkt->ch_7_eq_2_q) {
            peq2Q[CHANNEL_CH_7] = pkt->ch_7_eq_2_q;
            valueChanged = YES;
        }
        if (peq3G[CHANNEL_CH_7] != pkt->ch_7_eq_3_db+EQ_GAIN_OFFSET) {
            peq3G[CHANNEL_CH_7] = pkt->ch_7_eq_3_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq3F[CHANNEL_CH_7] != pkt->ch_7_eq_3_freq) {
            peq3F[CHANNEL_CH_7] = pkt->ch_7_eq_3_freq;
            valueChanged = YES;
        }
        if (peq3Q[CHANNEL_CH_7] != pkt->ch_7_eq_3_q) {
            peq3Q[CHANNEL_CH_7] = pkt->ch_7_eq_3_q;
            valueChanged = YES;
        }
        if (peq4G[CHANNEL_CH_7] != pkt->ch_7_eq_4_db+EQ_GAIN_OFFSET) {
            peq4G[CHANNEL_CH_7] = pkt->ch_7_eq_4_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq4F[CHANNEL_CH_7] != pkt->ch_7_eq_4_freq) {
            peq4F[CHANNEL_CH_7] = pkt->ch_7_eq_4_freq;
            valueChanged = YES;
        }
        if (peq4Q[CHANNEL_CH_7] != pkt->ch_7_eq_4_q) {
            peq4Q[CHANNEL_CH_7] = pkt->ch_7_eq_4_q;
            if (peq4Q[CHANNEL_CH_7] == PEQ_4_HIGH_SHELF) {
                peq4Filter[CHANNEL_CH_7] = EQ_HIGH_SHELF;
            } else if (peq4Q[CHANNEL_CH_7] == PEQ_4_LPF) {
                peq4Filter[CHANNEL_CH_7] = EQ_HIGH_CUT;
            } else {
                peq4Filter[CHANNEL_CH_7] = EQ_PEAK;
                lastPeq4Q[CHANNEL_CH_7] = peq4Q[CHANNEL_CH_7];
            }
            valueChanged = YES;
        }
        int peqEnableValue = pkt->channel_7_ctrl;
        if (peq1Enabled[CHANNEL_CH_7] != ((peqEnableValue & 0x08) >> 3)) {
            peq1Enabled[CHANNEL_CH_7] = (peqEnableValue & 0x08) >> 3;
            valueChanged = YES;
        }
        if (peq2Enabled[CHANNEL_CH_7] != ((peqEnableValue & 0x10) >> 4)) {
            peq2Enabled[CHANNEL_CH_7] = (peqEnableValue & 0x10) >> 4;
            valueChanged = YES;
        }
        if (peq3Enabled[CHANNEL_CH_7] != ((peqEnableValue & 0x20) >> 5)) {
            peq3Enabled[CHANNEL_CH_7] = (peqEnableValue & 0x20) >> 5;
            valueChanged = YES;
        }
        if (peq4Enabled[CHANNEL_CH_7] != ((peqEnableValue & 0x40) >> 6)) {
            peq4Enabled[CHANNEL_CH_7] = (peqEnableValue & 0x40) >> 6;
            valueChanged = YES;
        }
        if (peqEnabled[CHANNEL_CH_7] != ((peqEnableValue & 0x80) >> 7)) {
            peqEnabled[CHANNEL_CH_7] = (peqEnableValue & 0x80) >> 7;
            valueChanged = YES;
        }
    }
    
    if (valueChanged) {
        [self updatePeqPlot:peqEnabled[CHANNEL_CH_7]];
        [self updatePeqForChannel:CHANNEL_CH_7];
        valueChanged = NO;
    }
    
    
    // CHANNEL_CH_8
    {
        if (peq1G[CHANNEL_CH_8] != pkt->ch_8_eq_1_db+EQ_GAIN_OFFSET) {
            peq1G[CHANNEL_CH_8] = pkt->ch_8_eq_1_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq1F[CHANNEL_CH_8] != pkt->ch_8_eq_1_freq) {
            peq1F[CHANNEL_CH_8] = pkt->ch_8_eq_1_freq;
            valueChanged = YES;
        }
        if (peq1Q[CHANNEL_CH_8] != pkt->ch_8_eq_1_q) {
            peq1Q[CHANNEL_CH_8] = pkt->ch_8_eq_1_q;
            if (peq1Q[CHANNEL_CH_8] == PEQ_1_LOW_SHELF) {
                peq1Filter[CHANNEL_CH_8] = EQ_LOW_SHELF;
            } else if (peq1Q[CHANNEL_CH_8] == PEQ_1_HPF) {
                peq1Filter[CHANNEL_CH_8] = EQ_LOW_CUT;
            } else {
                peq1Filter[CHANNEL_CH_8] = EQ_PEAK;
                lastPeq1Q[CHANNEL_CH_8] = peq1Q[CHANNEL_CH_8];
            }
            valueChanged = YES;
        }
        if (peq2G[CHANNEL_CH_8] != pkt->ch_8_eq_2_db+EQ_GAIN_OFFSET) {
            peq2G[CHANNEL_CH_8] = pkt->ch_8_eq_2_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq2F[CHANNEL_CH_8] != pkt->ch_8_eq_2_freq) {
            peq2F[CHANNEL_CH_8] = pkt->ch_8_eq_2_freq;
            valueChanged = YES;
        }
        if (peq2Q[CHANNEL_CH_8] != pkt->ch_8_eq_2_q) {
            peq2Q[CHANNEL_CH_8] = pkt->ch_8_eq_2_q;
            valueChanged = YES;
        }
        if (peq3G[CHANNEL_CH_8] != pkt->ch_8_eq_3_db+EQ_GAIN_OFFSET) {
            peq3G[CHANNEL_CH_8] = pkt->ch_8_eq_3_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq3F[CHANNEL_CH_8] != pkt->ch_8_eq_3_freq) {
            peq3F[CHANNEL_CH_8] = pkt->ch_8_eq_3_freq;
            valueChanged = YES;
        }
        if (peq3Q[CHANNEL_CH_8] != pkt->ch_8_eq_3_q) {
            peq3Q[CHANNEL_CH_8] = pkt->ch_8_eq_3_q;
            valueChanged = YES;
        }
        if (peq4G[CHANNEL_CH_8] != pkt->ch_8_eq_4_db+EQ_GAIN_OFFSET) {
            peq4G[CHANNEL_CH_8] = pkt->ch_8_eq_4_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq4F[CHANNEL_CH_8] != pkt->ch_8_eq_4_freq) {
            peq4F[CHANNEL_CH_8] = pkt->ch_8_eq_4_freq;
            valueChanged = YES;
        }
        if (peq4Q[CHANNEL_CH_8] != pkt->ch_8_eq_4_q) {
            peq4Q[CHANNEL_CH_8] = pkt->ch_8_eq_4_q;
            if (peq4Q[CHANNEL_CH_8] == PEQ_4_HIGH_SHELF) {
                peq4Filter[CHANNEL_CH_8] = EQ_HIGH_SHELF;
            } else if (peq4Q[CHANNEL_CH_8] == PEQ_4_LPF) {
                peq4Filter[CHANNEL_CH_8] = EQ_HIGH_CUT;
            } else {
                peq4Filter[CHANNEL_CH_8] = EQ_PEAK;
                lastPeq4Q[CHANNEL_CH_8] = peq4Q[CHANNEL_CH_8];
            }
            valueChanged = YES;
        }
        int peqEnableValue = pkt->channel_8_ctrl;
        if (peq1Enabled[CHANNEL_CH_8] != ((peqEnableValue & 0x08) >> 3)) {
            peq1Enabled[CHANNEL_CH_8] = (peqEnableValue & 0x08) >> 3;
            valueChanged = YES;
        }
        if (peq2Enabled[CHANNEL_CH_8] != ((peqEnableValue & 0x10) >> 4)) {
            peq2Enabled[CHANNEL_CH_8] = (peqEnableValue & 0x10) >> 4;
            valueChanged = YES;
        }
        if (peq3Enabled[CHANNEL_CH_8] != ((peqEnableValue & 0x20) >> 5)) {
            peq3Enabled[CHANNEL_CH_8] = (peqEnableValue & 0x20) >> 5;
            valueChanged = YES;
        }
        if (peq4Enabled[CHANNEL_CH_8] != ((peqEnableValue & 0x40) >> 6)) {
            peq4Enabled[CHANNEL_CH_8] = (peqEnableValue & 0x40) >> 6;
            valueChanged = YES;
        }
        if (peqEnabled[CHANNEL_CH_8] != ((peqEnableValue & 0x80) >> 7)) {
            peqEnabled[CHANNEL_CH_8] = (peqEnableValue & 0x80) >> 7;
            valueChanged = YES;
        }
    }
    
    if (valueChanged) {
        [self updatePeqPlot:peqEnabled[CHANNEL_CH_8]];
        [self updatePeqForChannel:CHANNEL_CH_8];
        valueChanged = NO;
    }
    
    // CHANNEL_CH_9
    {
        if (peq1G[CHANNEL_CH_9] != pkt->ch_9_eq_1_db+EQ_GAIN_OFFSET) {
            peq1G[CHANNEL_CH_9] = pkt->ch_9_eq_1_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq1F[CHANNEL_CH_9] != pkt->ch_9_eq_1_freq) {
            peq1F[CHANNEL_CH_9] = pkt->ch_9_eq_1_freq;
            valueChanged = YES;
        }
        if (peq1Q[CHANNEL_CH_9] != pkt->ch_9_eq_1_q) {
            peq1Q[CHANNEL_CH_9] = pkt->ch_9_eq_1_q;
            if (peq1Q[CHANNEL_CH_9] == PEQ_1_LOW_SHELF) {
                peq1Filter[CHANNEL_CH_9] = EQ_LOW_SHELF;
            } else if (peq1Q[CHANNEL_CH_9] == PEQ_1_HPF) {
                peq1Filter[CHANNEL_CH_9] = EQ_LOW_CUT;
            } else {
                peq1Filter[CHANNEL_CH_9] = EQ_PEAK;
                lastPeq1Q[CHANNEL_CH_9] = peq1Q[CHANNEL_CH_9];
            }
            valueChanged = YES;
        }
        if (peq2G[CHANNEL_CH_9] != pkt->ch_9_eq_2_db+EQ_GAIN_OFFSET) {
            peq2G[CHANNEL_CH_9] = pkt->ch_9_eq_2_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq2F[CHANNEL_CH_9] != pkt->ch_9_eq_2_freq) {
            peq2F[CHANNEL_CH_9] = pkt->ch_9_eq_2_freq;
            valueChanged = YES;
        }
        if (peq2Q[CHANNEL_CH_9] != pkt->ch_9_eq_2_q) {
            peq2Q[CHANNEL_CH_9] = pkt->ch_9_eq_2_q;
            valueChanged = YES;
        }
        if (peq3G[CHANNEL_CH_9] != pkt->ch_9_eq_3_db+EQ_GAIN_OFFSET) {
            peq3G[CHANNEL_CH_9] = pkt->ch_9_eq_3_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq3F[CHANNEL_CH_9] != pkt->ch_9_eq_3_freq) {
            peq3F[CHANNEL_CH_9] = pkt->ch_9_eq_3_freq;
            valueChanged = YES;
        }
        if (peq3Q[CHANNEL_CH_9] != pkt->ch_9_eq_3_q) {
            peq3Q[CHANNEL_CH_9] = pkt->ch_9_eq_3_q;
            valueChanged = YES;
        }
        if (peq4G[CHANNEL_CH_9] != pkt->ch_9_eq_4_db+EQ_GAIN_OFFSET) {
            peq4G[CHANNEL_CH_9] = pkt->ch_9_eq_4_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq4F[CHANNEL_CH_9] != pkt->ch_9_eq_4_freq) {
            peq4F[CHANNEL_CH_9] = pkt->ch_9_eq_4_freq;
            valueChanged = YES;
        }
        if (peq4Q[CHANNEL_CH_9] != pkt->ch_9_eq_4_q) {
            peq4Q[CHANNEL_CH_9] = pkt->ch_9_eq_4_q;
            if (peq4Q[CHANNEL_CH_9] == PEQ_4_HIGH_SHELF) {
                peq4Filter[CHANNEL_CH_9] = EQ_HIGH_SHELF;
            } else if (peq4Q[CHANNEL_CH_9] == PEQ_4_LPF) {
                peq4Filter[CHANNEL_CH_9] = EQ_HIGH_CUT;
            } else {
                peq4Filter[CHANNEL_CH_9] = EQ_PEAK;
                lastPeq4Q[CHANNEL_CH_9] = peq4Q[CHANNEL_CH_9];
            }
            valueChanged = YES;
        }
        int peqEnableValue = pkt->channel_9_ctrl;
        if (peq1Enabled[CHANNEL_CH_9] != ((peqEnableValue & 0x08) >> 3)) {
            peq1Enabled[CHANNEL_CH_9] = (peqEnableValue & 0x08) >> 3;
            valueChanged = YES;
        }
        if (peq2Enabled[CHANNEL_CH_9] != ((peqEnableValue & 0x10) >> 4)) {
            peq2Enabled[CHANNEL_CH_9] = (peqEnableValue & 0x10) >> 4;
            valueChanged = YES;
        }
        if (peq3Enabled[CHANNEL_CH_9] != ((peqEnableValue & 0x20) >> 5)) {
            peq3Enabled[CHANNEL_CH_9] = (peqEnableValue & 0x20) >> 5;
            valueChanged = YES;
        }
        if (peq4Enabled[CHANNEL_CH_9] != ((peqEnableValue & 0x40) >> 6)) {
            peq4Enabled[CHANNEL_CH_9] = (peqEnableValue & 0x40) >> 6;
            valueChanged = YES;
        }
        if (peqEnabled[CHANNEL_CH_9] != ((peqEnableValue & 0x80) >> 7)) {
            peqEnabled[CHANNEL_CH_9] = (peqEnableValue & 0x80) >> 7;
            valueChanged = YES;
        }
    }
    
    if (valueChanged) {
        [self updatePeqPlot:peqEnabled[CHANNEL_CH_9]];
        [self updatePeqForChannel:CHANNEL_CH_9];
        valueChanged = NO;
    }
    
    // CHANNEL_CH_10
    {
        if (peq1G[CHANNEL_CH_10] != pkt->ch_10_eq_1_db+EQ_GAIN_OFFSET) {
            peq1G[CHANNEL_CH_10] = pkt->ch_10_eq_1_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq1F[CHANNEL_CH_10] != pkt->ch_10_eq_1_freq) {
            peq1F[CHANNEL_CH_10] = pkt->ch_10_eq_1_freq;
            valueChanged = YES;
        }
        if (peq1Q[CHANNEL_CH_10] != pkt->ch_10_eq_1_q) {
            peq1Q[CHANNEL_CH_10] = pkt->ch_10_eq_1_q;
            if (peq1Q[CHANNEL_CH_10] == PEQ_1_LOW_SHELF) {
                [btnPeqFilterFront setImage:[UIImage imageNamed:@"frame-12.png"] forState:UIControlStateNormal];
                peq1Filter[CHANNEL_CH_10] = EQ_LOW_SHELF;
            } else if (peq1Q[CHANNEL_CH_10] == PEQ_1_HPF) {
                [btnPeqFilterFront setImage:[UIImage imageNamed:@"frame-13.png"] forState:UIControlStateNormal];
                peq1Filter[CHANNEL_CH_10] = EQ_LOW_CUT;
            } else {
                [btnPeqFilterFront setImage:[UIImage imageNamed:@"frame-11.png"] forState:UIControlStateNormal];
                peq1Filter[CHANNEL_CH_10] = EQ_PEAK;
                lastPeq1Q[CHANNEL_CH_10] = peq1Q[CHANNEL_CH_10];
            }
            valueChanged = YES;
        }
        if (peq2G[CHANNEL_CH_10] != pkt->ch_10_eq_2_db+EQ_GAIN_OFFSET) {
            peq2G[CHANNEL_CH_10] = pkt->ch_10_eq_2_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq2F[CHANNEL_CH_10] != pkt->ch_10_eq_2_freq) {
            peq2F[CHANNEL_CH_10] = pkt->ch_10_eq_2_freq;
            valueChanged = YES;
        }
        if (peq2Q[CHANNEL_CH_10] != pkt->ch_10_eq_2_q) {
            peq2Q[CHANNEL_CH_10] = pkt->ch_10_eq_2_q;
            valueChanged = YES;
        }
        if (peq3G[CHANNEL_CH_10] != pkt->ch_10_eq_3_db+EQ_GAIN_OFFSET) {
            peq3G[CHANNEL_CH_10] = pkt->ch_10_eq_3_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq3F[CHANNEL_CH_10] != pkt->ch_10_eq_3_freq) {
            peq3F[CHANNEL_CH_10] = pkt->ch_10_eq_3_freq;
            valueChanged = YES;
        }
        if (peq3Q[CHANNEL_CH_10] != pkt->ch_10_eq_3_q) {
            peq3Q[CHANNEL_CH_10] = pkt->ch_10_eq_3_q;
            valueChanged = YES;
        }
        if (peq4G[CHANNEL_CH_10] != pkt->ch_10_eq_4_db+EQ_GAIN_OFFSET) {
            peq4G[CHANNEL_CH_10] = pkt->ch_10_eq_4_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq4F[CHANNEL_CH_10] != pkt->ch_10_eq_4_freq) {
            peq4F[CHANNEL_CH_10] = pkt->ch_10_eq_4_freq;
            valueChanged = YES;
        }
        if (peq4Q[CHANNEL_CH_10] != pkt->ch_10_eq_4_q) {
            peq4Q[CHANNEL_CH_10] = pkt->ch_10_eq_4_q;
            if (peq4Q[CHANNEL_CH_10] == PEQ_4_HIGH_SHELF) {
                peq4Filter[CHANNEL_CH_10] = EQ_HIGH_SHELF;
            } else if (peq4Q[CHANNEL_CH_10] == PEQ_4_LPF) {
                peq4Filter[CHANNEL_CH_10] = EQ_HIGH_CUT;
            } else {
                peq4Filter[CHANNEL_CH_10] = EQ_PEAK;
                lastPeq4Q[CHANNEL_CH_10] = peq4Q[CHANNEL_CH_10];
            }
            valueChanged = YES;
        }
        int peqEnableValue = pkt->channel_10_ctrl;
        if (peq1Enabled[CHANNEL_CH_10] != ((peqEnableValue & 0x08) >> 3)) {
            peq1Enabled[CHANNEL_CH_10] = (peqEnableValue & 0x08) >> 3;
            valueChanged = YES;
        }
        if (peq2Enabled[CHANNEL_CH_10] != ((peqEnableValue & 0x10) >> 4)) {
            peq2Enabled[CHANNEL_CH_10] = (peqEnableValue & 0x10) >> 4;
            valueChanged = YES;
        }
        if (peq3Enabled[CHANNEL_CH_10] != ((peqEnableValue & 0x20) >> 5)) {
            peq3Enabled[CHANNEL_CH_10] = (peqEnableValue & 0x20) >> 5;
            valueChanged = YES;
        }
        if (peq4Enabled[CHANNEL_CH_10] != ((peqEnableValue & 0x40) >> 6)) {
            peq4Enabled[CHANNEL_CH_10] = (peqEnableValue & 0x40) >> 6;
            valueChanged = YES;
        }
        if (peqEnabled[CHANNEL_CH_10] != ((peqEnableValue & 0x80) >> 7)) {
            peqEnabled[CHANNEL_CH_10] = (peqEnableValue & 0x80) >> 7;
            valueChanged = YES;
        }
    }
    
    if (valueChanged) {
        [self updatePeqPlot:peqEnabled[CHANNEL_CH_10]];
        [self updatePeqForChannel:CHANNEL_CH_10];
        valueChanged = NO;
    }
    
    
    // CHANNEL_CH_11
    {
        if (peq1G[CHANNEL_CH_11] != pkt->ch_11_eq_1_db+EQ_GAIN_OFFSET) {
            peq1G[CHANNEL_CH_11] = pkt->ch_11_eq_1_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq1F[CHANNEL_CH_11] != pkt->ch_11_eq_1_freq) {
            peq1F[CHANNEL_CH_11] = pkt->ch_11_eq_1_freq;
            valueChanged = YES;
        }
        if (peq1Q[CHANNEL_CH_11] != pkt->ch_11_eq_1_q) {
            peq1Q[CHANNEL_CH_11] = pkt->ch_11_eq_1_q;
            if (peq1Q[CHANNEL_CH_11] == PEQ_1_LOW_SHELF) {
                peq1Filter[CHANNEL_CH_11] = EQ_LOW_SHELF;
            } else if (peq1Q[CHANNEL_CH_11] == PEQ_1_HPF) {
                peq1Filter[CHANNEL_CH_11] = EQ_LOW_CUT;
            } else {
                peq1Filter[CHANNEL_CH_11] = EQ_PEAK;
                lastPeq1Q[CHANNEL_CH_11] = peq1Q[CHANNEL_CH_11];
            }
            valueChanged = YES;
        }
        if (peq2G[CHANNEL_CH_11] != pkt->ch_11_eq_2_db+EQ_GAIN_OFFSET) {
            peq2G[CHANNEL_CH_11] = pkt->ch_11_eq_2_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq2F[CHANNEL_CH_11] != pkt->ch_11_eq_2_freq) {
            peq2F[CHANNEL_CH_11] = pkt->ch_11_eq_2_freq;
            valueChanged = YES;
        }
        if (peq2Q[CHANNEL_CH_11] != pkt->ch_11_eq_2_q) {
            peq2Q[CHANNEL_CH_11] = pkt->ch_11_eq_2_q;
            valueChanged = YES;
        }
        if (peq3G[CHANNEL_CH_11] != pkt->ch_11_eq_3_db+EQ_GAIN_OFFSET) {
            peq3G[CHANNEL_CH_11] = pkt->ch_11_eq_3_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq3F[CHANNEL_CH_11] != pkt->ch_11_eq_3_freq) {
            peq3F[CHANNEL_CH_11] = pkt->ch_11_eq_3_freq;
            valueChanged = YES;
        }
        if (peq3Q[CHANNEL_CH_11] != pkt->ch_11_eq_3_q) {
            peq3Q[CHANNEL_CH_11] = pkt->ch_11_eq_3_q;
            valueChanged = YES;
        }
        if (peq4G[CHANNEL_CH_11] != pkt->ch_11_eq_4_db+EQ_GAIN_OFFSET) {
            peq4G[CHANNEL_CH_11] = pkt->ch_11_eq_4_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq4F[CHANNEL_CH_11] != pkt->ch_11_eq_4_freq) {
            peq4F[CHANNEL_CH_11] = pkt->ch_11_eq_4_freq;
            valueChanged = YES;
        }
        if (peq4Q[CHANNEL_CH_11] != pkt->ch_11_eq_4_q) {
            peq4Q[CHANNEL_CH_11] = pkt->ch_11_eq_4_q;
            if (peq4Q[CHANNEL_CH_11] == PEQ_4_HIGH_SHELF) {
                peq4Filter[CHANNEL_CH_11] = EQ_HIGH_SHELF;
            } else if (peq4Q[CHANNEL_CH_11] == PEQ_4_LPF) {
                peq4Filter[CHANNEL_CH_11] = EQ_HIGH_CUT;
            } else {
                peq4Filter[CHANNEL_CH_11] = EQ_PEAK;
                lastPeq4Q[CHANNEL_CH_11] = peq4Q[CHANNEL_CH_11];
            }
            valueChanged = YES;
        }
        int peqEnableValue = pkt->channel_11_ctrl;
        if (peq1Enabled[CHANNEL_CH_11] != ((peqEnableValue & 0x08) >> 3)) {
            peq1Enabled[CHANNEL_CH_11] = (peqEnableValue & 0x08) >> 3;
            valueChanged = YES;
        }
        if (peq2Enabled[CHANNEL_CH_11] != ((peqEnableValue & 0x10) >> 4)) {
            peq2Enabled[CHANNEL_CH_11] = (peqEnableValue & 0x10) >> 4;
            valueChanged = YES;
        }
        if (peq3Enabled[CHANNEL_CH_11] != ((peqEnableValue & 0x20) >> 5)) {
            peq3Enabled[CHANNEL_CH_11] = (peqEnableValue & 0x20) >> 5;
            valueChanged = YES;
        }
        if (peq4Enabled[CHANNEL_CH_11] != ((peqEnableValue & 0x40) >> 6)) {
            peq4Enabled[CHANNEL_CH_11] = (peqEnableValue & 0x40) >> 6;
            valueChanged = YES;
        }
        if (peqEnabled[CHANNEL_CH_11] != ((peqEnableValue & 0x80) >> 7)) {
            peqEnabled[CHANNEL_CH_11] = (peqEnableValue & 0x80) >> 7;
            valueChanged = YES;
        }
    }
    
    if (valueChanged) {
        [self updatePeqPlot:peqEnabled[CHANNEL_CH_11]];
        [self updatePeqForChannel:CHANNEL_CH_11];
        valueChanged = NO;
    }
    
    
    // CHANNEL_CH_12
    {
        if (peq1G[CHANNEL_CH_12] != pkt->ch_12_eq_1_db+EQ_GAIN_OFFSET) {
            peq1G[CHANNEL_CH_12] = pkt->ch_12_eq_1_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq1F[CHANNEL_CH_12] != pkt->ch_12_eq_1_freq) {
            peq1F[CHANNEL_CH_12] = pkt->ch_12_eq_1_freq;
            valueChanged = YES;
        }
        if (peq1Q[CHANNEL_CH_12] != pkt->ch_12_eq_1_q) {
            peq1Q[CHANNEL_CH_12] = pkt->ch_12_eq_1_q;
            if (peq1Q[CHANNEL_CH_12] == PEQ_1_LOW_SHELF) {
                peq1Filter[CHANNEL_CH_12] = EQ_LOW_SHELF;
            } else if (peq1Q[CHANNEL_CH_12] == PEQ_1_HPF) {
                peq1Filter[CHANNEL_CH_12] = EQ_LOW_CUT;
            } else {
                peq1Filter[CHANNEL_CH_12] = EQ_PEAK;
                lastPeq1Q[CHANNEL_CH_12] = peq1Q[CHANNEL_CH_12];
            }
            valueChanged = YES;
        }
        if (peq2G[CHANNEL_CH_12] != pkt->ch_12_eq_2_db+EQ_GAIN_OFFSET) {
            peq2G[CHANNEL_CH_12] = pkt->ch_12_eq_2_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq2F[CHANNEL_CH_12] != pkt->ch_12_eq_2_freq) {
            peq2F[CHANNEL_CH_12] = pkt->ch_12_eq_2_freq;
            valueChanged = YES;
        }
        if (peq2Q[CHANNEL_CH_12] != pkt->ch_12_eq_2_q) {
            peq2Q[CHANNEL_CH_12] = pkt->ch_12_eq_2_q;
            valueChanged = YES;
        }
        if (peq3G[CHANNEL_CH_12] != pkt->ch_12_eq_3_db+EQ_GAIN_OFFSET) {
            peq3G[CHANNEL_CH_12] = pkt->ch_12_eq_3_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq3F[CHANNEL_CH_12] != pkt->ch_12_eq_3_freq) {
            peq3F[CHANNEL_CH_12] = pkt->ch_12_eq_3_freq;
            valueChanged = YES;
        }
        if (peq3Q[CHANNEL_CH_12] != pkt->ch_12_eq_3_q) {
            peq3Q[CHANNEL_CH_12] = pkt->ch_12_eq_3_q;
            valueChanged = YES;
        }
        if (peq4G[CHANNEL_CH_12] != pkt->ch_12_eq_4_db+EQ_GAIN_OFFSET) {
            peq4G[CHANNEL_CH_12] = pkt->ch_12_eq_4_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq4F[CHANNEL_CH_12] != pkt->ch_12_eq_4_freq) {
            peq4F[CHANNEL_CH_12] = pkt->ch_12_eq_4_freq;
            valueChanged = YES;
        }
        if (peq4Q[CHANNEL_CH_12] != pkt->ch_12_eq_4_q) {
            peq4Q[CHANNEL_CH_12] = pkt->ch_12_eq_4_q;
            if (peq4Q[CHANNEL_CH_12] == PEQ_4_HIGH_SHELF) {
                peq4Filter[CHANNEL_CH_12] = EQ_HIGH_SHELF;
            } else if (peq4Q[CHANNEL_CH_12] == PEQ_4_LPF) {
                peq4Filter[CHANNEL_CH_12] = EQ_HIGH_CUT;
            } else {
                peq4Filter[CHANNEL_CH_12] = EQ_PEAK;
                lastPeq4Q[CHANNEL_CH_12] = peq4Q[CHANNEL_CH_12];
            }
            valueChanged = YES;
        }
        int peqEnableValue = pkt->channel_12_ctrl;
        if (peq1Enabled[CHANNEL_CH_12] != ((peqEnableValue & 0x08) >> 3)) {
            peq1Enabled[CHANNEL_CH_12] = (peqEnableValue & 0x08) >> 3;
            valueChanged = YES;
        }
        if (peq2Enabled[CHANNEL_CH_12] != ((peqEnableValue & 0x10) >> 4)) {
            peq2Enabled[CHANNEL_CH_12] = (peqEnableValue & 0x10) >> 4;
            valueChanged = YES;
        }
        if (peq3Enabled[CHANNEL_CH_12] != ((peqEnableValue & 0x20) >> 5)) {
            peq3Enabled[CHANNEL_CH_12] = (peqEnableValue & 0x20) >> 5;
            valueChanged = YES;
        }
        if (peq4Enabled[CHANNEL_CH_12] != ((peqEnableValue & 0x40) >> 6)) {
            peq4Enabled[CHANNEL_CH_12] = (peqEnableValue & 0x40) >> 6;
            valueChanged = YES;
        }
        if (peqEnabled[CHANNEL_CH_12] != ((peqEnableValue & 0x80) >> 7)) {
            peqEnabled[CHANNEL_CH_12] = (peqEnableValue & 0x80) >> 7;
            valueChanged = YES;
        }
    }
    
    if (valueChanged) {
        [self updatePeqPlot:peqEnabled[CHANNEL_CH_12]];
        [self updatePeqForChannel:CHANNEL_CH_12];
        valueChanged = NO;
    }
    
    
    // CHANNEL_CH_13
    {
        if (peq1G[CHANNEL_CH_13] != pkt->ch_13_eq_1_db+EQ_GAIN_OFFSET) {
            peq1G[CHANNEL_CH_13] = pkt->ch_13_eq_1_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq1F[CHANNEL_CH_13] != pkt->ch_13_eq_1_freq) {
            peq1F[CHANNEL_CH_13] = pkt->ch_13_eq_1_freq;
            valueChanged = YES;
        }
        if (peq1Q[CHANNEL_CH_13] != pkt->ch_13_eq_1_q) {
            peq1Q[CHANNEL_CH_13] = pkt->ch_13_eq_1_q;
            if (peq1Q[CHANNEL_CH_13] == PEQ_1_LOW_SHELF) {
                peq1Filter[CHANNEL_CH_13] = EQ_LOW_SHELF;
            } else if (peq1Q[CHANNEL_CH_13] == PEQ_1_HPF) {
                peq1Filter[CHANNEL_CH_13] = EQ_LOW_CUT;
            } else {
                peq1Filter[CHANNEL_CH_13] = EQ_PEAK;
                lastPeq1Q[CHANNEL_CH_13] = peq1Q[CHANNEL_CH_13];
            }
            valueChanged = YES;
        }
        if (peq2G[CHANNEL_CH_13] != pkt->ch_13_eq_2_db+EQ_GAIN_OFFSET) {
            peq2G[CHANNEL_CH_13] = pkt->ch_13_eq_2_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq2F[CHANNEL_CH_13] != pkt->ch_13_eq_2_freq) {
            peq2F[CHANNEL_CH_13] = pkt->ch_13_eq_2_freq;
            valueChanged = YES;
        }
        if (peq2Q[CHANNEL_CH_13] != pkt->ch_13_eq_2_q) {
            peq2Q[CHANNEL_CH_13] = pkt->ch_13_eq_2_q;
            valueChanged = YES;
        }
        if (peq3G[CHANNEL_CH_13] != pkt->ch_13_eq_3_db+EQ_GAIN_OFFSET) {
            peq3G[CHANNEL_CH_13] = pkt->ch_13_eq_3_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq3F[CHANNEL_CH_13] != pkt->ch_13_eq_3_freq) {
            peq3F[CHANNEL_CH_13] = pkt->ch_13_eq_3_freq;
            valueChanged = YES;
        }
        if (peq3Q[CHANNEL_CH_13] != pkt->ch_13_eq_3_q) {
            peq3Q[CHANNEL_CH_13] = pkt->ch_13_eq_3_q;
            valueChanged = YES;
        }
        if (peq4G[CHANNEL_CH_13] != pkt->ch_13_eq_4_db+EQ_GAIN_OFFSET) {
            peq4G[CHANNEL_CH_13] = pkt->ch_13_eq_4_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq4F[CHANNEL_CH_13] != pkt->ch_13_eq_4_freq) {
            peq4F[CHANNEL_CH_13] = pkt->ch_13_eq_4_freq;
            valueChanged = YES;
        }
        if (peq4Q[CHANNEL_CH_13] != pkt->ch_13_eq_4_q) {
            peq4Q[CHANNEL_CH_13] = pkt->ch_13_eq_4_q;
            if (peq4Q[CHANNEL_CH_13] == PEQ_4_HIGH_SHELF) {
                peq4Filter[CHANNEL_CH_13] = EQ_HIGH_SHELF;
            } else if (peq4Q[CHANNEL_CH_13] == PEQ_4_LPF) {
                peq4Filter[CHANNEL_CH_13] = EQ_HIGH_CUT;
            } else {
                peq4Filter[CHANNEL_CH_13] = EQ_PEAK;
                lastPeq4Q[CHANNEL_CH_13] = peq4Q[CHANNEL_CH_13];
            }
            valueChanged = YES;
        }
        int peqEnableValue = pkt->channel_13_ctrl;
        if (peq1Enabled[CHANNEL_CH_13] != ((peqEnableValue & 0x08) >> 3)) {
            peq1Enabled[CHANNEL_CH_13] = (peqEnableValue & 0x08) >> 3;
            valueChanged = YES;
        }
        if (peq2Enabled[CHANNEL_CH_13] != ((peqEnableValue & 0x10) >> 4)) {
            peq2Enabled[CHANNEL_CH_13] = (peqEnableValue & 0x10) >> 4;
            valueChanged = YES;
        }
        if (peq3Enabled[CHANNEL_CH_13] != ((peqEnableValue & 0x20) >> 5)) {
            peq3Enabled[CHANNEL_CH_13] = (peqEnableValue & 0x20) >> 5;
            valueChanged = YES;
        }
        if (peq4Enabled[CHANNEL_CH_13] != ((peqEnableValue & 0x40) >> 6)) {
            peq4Enabled[CHANNEL_CH_13] = (peqEnableValue & 0x40) >> 6;
            valueChanged = YES;
        }
        if (peqEnabled[CHANNEL_CH_13] != ((peqEnableValue & 0x80) >> 7)) {
            peqEnabled[CHANNEL_CH_13] = (peqEnableValue & 0x80) >> 7;
            valueChanged = YES;
        }
    }
    
    if (valueChanged) {
        [self updatePeqPlot:peqEnabled[CHANNEL_CH_13]];
        [self updatePeqForChannel:CHANNEL_CH_13];
        valueChanged = NO;
    }
    
    
    // CHANNEL_CH_14
    {
        if (peq1G[CHANNEL_CH_14] != pkt->ch_14_eq_1_db+EQ_GAIN_OFFSET) {
            peq1G[CHANNEL_CH_14] = pkt->ch_14_eq_1_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq1F[CHANNEL_CH_14] != pkt->ch_14_eq_1_freq) {
            peq1F[CHANNEL_CH_14] = pkt->ch_14_eq_1_freq;
            valueChanged = YES;
        }
        if (peq1Q[CHANNEL_CH_14] != pkt->ch_14_eq_1_q) {
            peq1Q[CHANNEL_CH_14] = pkt->ch_14_eq_1_q;
            if (peq1Q[CHANNEL_CH_14] == PEQ_1_LOW_SHELF) {
                peq1Filter[CHANNEL_CH_14] = EQ_LOW_SHELF;
            } else if (peq1Q[CHANNEL_CH_14] == PEQ_1_HPF) {
                peq1Filter[CHANNEL_CH_14] = EQ_LOW_CUT;
            } else {
                peq1Filter[CHANNEL_CH_14] = EQ_PEAK;
                lastPeq1Q[CHANNEL_CH_14] = peq1Q[CHANNEL_CH_14];
            }
            valueChanged = YES;
        }
        if (peq2G[CHANNEL_CH_14] != pkt->ch_14_eq_2_db+EQ_GAIN_OFFSET) {
            peq2G[CHANNEL_CH_14] = pkt->ch_14_eq_2_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq2F[CHANNEL_CH_14] != pkt->ch_14_eq_2_freq) {
            peq2F[CHANNEL_CH_14] = pkt->ch_14_eq_2_freq;
            valueChanged = YES;
        }
        if (peq2Q[CHANNEL_CH_14] != pkt->ch_14_eq_2_q) {
            peq2Q[CHANNEL_CH_14] = pkt->ch_14_eq_2_q;
            valueChanged = YES;
        }
        if (peq3G[CHANNEL_CH_14] != pkt->ch_14_eq_3_db+EQ_GAIN_OFFSET) {
            peq3G[CHANNEL_CH_14] = pkt->ch_14_eq_3_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq3F[CHANNEL_CH_14] != pkt->ch_14_eq_3_freq) {
            peq3F[CHANNEL_CH_14] = pkt->ch_14_eq_3_freq;
            valueChanged = YES;
        }
        if (peq3Q[CHANNEL_CH_14] != pkt->ch_14_eq_3_q) {
            peq3Q[CHANNEL_CH_14] = pkt->ch_14_eq_3_q;
            valueChanged = YES;
        }
        if (peq4G[CHANNEL_CH_14] != pkt->ch_14_eq_4_db+EQ_GAIN_OFFSET) {
            peq4G[CHANNEL_CH_14] = pkt->ch_14_eq_4_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq4F[CHANNEL_CH_14] != pkt->ch_14_eq_4_freq) {
            peq4F[CHANNEL_CH_14] = pkt->ch_14_eq_4_freq;
            valueChanged = YES;
        }
        if (peq4Q[CHANNEL_CH_14] != pkt->ch_14_eq_4_q) {
            peq4Q[CHANNEL_CH_14] = pkt->ch_14_eq_4_q;
            if (peq4Q[CHANNEL_CH_14] == PEQ_4_HIGH_SHELF) {
                peq4Filter[CHANNEL_CH_14] = EQ_HIGH_SHELF;
            } else if (peq4Q[CHANNEL_CH_14] == PEQ_4_LPF) {
                peq4Filter[CHANNEL_CH_14] = EQ_HIGH_CUT;
            } else {
                peq4Filter[CHANNEL_CH_14] = EQ_PEAK;
                lastPeq4Q[CHANNEL_CH_14] = peq4Q[CHANNEL_CH_14];
            }
            valueChanged = YES;
        }
        int peqEnableValue = pkt->channel_14_ctrl;
        if (peq1Enabled[CHANNEL_CH_14] != ((peqEnableValue & 0x08) >> 3)) {
            peq1Enabled[CHANNEL_CH_14] = (peqEnableValue & 0x08) >> 3;
            valueChanged = YES;
        }
        if (peq2Enabled[CHANNEL_CH_14] != ((peqEnableValue & 0x10) >> 4)) {
            peq2Enabled[CHANNEL_CH_14] = (peqEnableValue & 0x10) >> 4;
            valueChanged = YES;
        }
        if (peq3Enabled[CHANNEL_CH_14] != ((peqEnableValue & 0x20) >> 5)) {
            peq3Enabled[CHANNEL_CH_14] = (peqEnableValue & 0x20) >> 5;
            valueChanged = YES;
        }
        if (peq4Enabled[CHANNEL_CH_14] != ((peqEnableValue & 0x40) >> 6)) {
            peq4Enabled[CHANNEL_CH_14] = (peqEnableValue & 0x40) >> 6;
            valueChanged = YES;
        }
        if (peqEnabled[CHANNEL_CH_14] != ((peqEnableValue & 0x80) >> 7)) {
            peqEnabled[CHANNEL_CH_14] = (peqEnableValue & 0x80) >> 7;
            valueChanged = YES;
        }
    }
    
    if (valueChanged) {
        [self updatePeqPlot:peqEnabled[CHANNEL_CH_14]];
        [self updatePeqForChannel:CHANNEL_CH_14];
        valueChanged = NO;
    }
    
    
    // CHANNEL_CH_15
    {
        if (peq1G[CHANNEL_CH_15] != pkt->ch_15_eq_1_db+EQ_GAIN_OFFSET) {
            peq1G[CHANNEL_CH_15] = pkt->ch_15_eq_1_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq1F[CHANNEL_CH_15] != pkt->ch_15_eq_1_freq) {
            peq1F[CHANNEL_CH_15] = pkt->ch_15_eq_1_freq;
            valueChanged = YES;
        }
        if (peq1Q[CHANNEL_CH_15] != pkt->ch_15_eq_1_q) {
            peq1Q[CHANNEL_CH_15] = pkt->ch_15_eq_1_q;
            if (peq1Q[CHANNEL_CH_15] == PEQ_1_LOW_SHELF) {
                peq1Filter[CHANNEL_CH_15] = EQ_LOW_SHELF;
            } else if (peq1Q[CHANNEL_CH_15] == PEQ_1_HPF) {
                peq1Filter[CHANNEL_CH_15] = EQ_LOW_CUT;
            } else {
                peq1Filter[CHANNEL_CH_15] = EQ_PEAK;
                lastPeq1Q[CHANNEL_CH_15] = peq1Q[CHANNEL_CH_15];
            }
            valueChanged = YES;
        }
        if (peq2G[CHANNEL_CH_15] != pkt->ch_15_eq_2_db+EQ_GAIN_OFFSET) {
            peq2G[CHANNEL_CH_15] = pkt->ch_15_eq_2_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq2F[CHANNEL_CH_15] != pkt->ch_15_eq_2_freq) {
            peq2F[CHANNEL_CH_15] = pkt->ch_15_eq_2_freq;
            valueChanged = YES;
        }
        if (peq2Q[CHANNEL_CH_15] != pkt->ch_15_eq_2_q) {
            peq2Q[CHANNEL_CH_15] = pkt->ch_15_eq_2_q;
            valueChanged = YES;
        }
        if (peq3G[CHANNEL_CH_15] != pkt->ch_15_eq_3_db+EQ_GAIN_OFFSET) {
            peq3G[CHANNEL_CH_15] = pkt->ch_15_eq_3_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq3F[CHANNEL_CH_15] != pkt->ch_15_eq_3_freq) {
            peq3F[CHANNEL_CH_15] = pkt->ch_15_eq_3_freq;
            valueChanged = YES;
        }
        if (peq3Q[CHANNEL_CH_15] != pkt->ch_15_eq_3_q) {
            peq3Q[CHANNEL_CH_15] = pkt->ch_15_eq_3_q;
            valueChanged = YES;
        }
        if (peq4G[CHANNEL_CH_15] != pkt->ch_15_eq_4_db+EQ_GAIN_OFFSET) {
            peq4G[CHANNEL_CH_15] = pkt->ch_15_eq_4_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq4F[CHANNEL_CH_15] != pkt->ch_15_eq_4_freq) {
            peq4F[CHANNEL_CH_15] = pkt->ch_15_eq_4_freq;
            valueChanged = YES;
        }
        if (peq4Q[CHANNEL_CH_15] != pkt->ch_15_eq_4_q) {
            peq4Q[CHANNEL_CH_15] = pkt->ch_15_eq_4_q;
            if (peq4Q[CHANNEL_CH_15] == PEQ_4_HIGH_SHELF) {
                peq4Filter[CHANNEL_CH_15] = EQ_HIGH_SHELF;
            } else if (peq4Q[CHANNEL_CH_15] == PEQ_4_LPF) {
                peq4Filter[CHANNEL_CH_15] = EQ_HIGH_CUT;
            } else {
                peq4Filter[CHANNEL_CH_15] = EQ_PEAK;
                lastPeq4Q[CHANNEL_CH_15] = peq4Q[CHANNEL_CH_15];
            }
            valueChanged = YES;
        }
        int peqEnableValue = pkt->channel_15_ctrl;
        if (peq1Enabled[CHANNEL_CH_15] != ((peqEnableValue & 0x08) >> 3)) {
            peq1Enabled[CHANNEL_CH_15] = (peqEnableValue & 0x08) >> 3;
            valueChanged = YES;
        }
        if (peq2Enabled[CHANNEL_CH_15] != ((peqEnableValue & 0x10) >> 4)) {
            peq2Enabled[CHANNEL_CH_15] = (peqEnableValue & 0x10) >> 4;
            valueChanged = YES;
        }
        if (peq3Enabled[CHANNEL_CH_15] != ((peqEnableValue & 0x20) >> 5)) {
            peq3Enabled[CHANNEL_CH_15] = (peqEnableValue & 0x20) >> 5;
            valueChanged = YES;
        }
        if (peq4Enabled[CHANNEL_CH_15] != ((peqEnableValue & 0x40) >> 6)) {
            peq4Enabled[CHANNEL_CH_15] = (peqEnableValue & 0x40) >> 6;
            valueChanged = YES;
        }
        if (peqEnabled[CHANNEL_CH_15] != ((peqEnableValue & 0x80) >> 7)) {
            peqEnabled[CHANNEL_CH_15] = (peqEnableValue & 0x80) >> 7;
            valueChanged = YES;
        }
    }
    
    if (valueChanged) {
        [self updatePeqPlot:peqEnabled[CHANNEL_CH_15]];
        [self updatePeqForChannel:CHANNEL_CH_15];
        valueChanged = NO;
    }
    
    
    // CHANNEL_CH_16
    {
        if (peq1G[CHANNEL_CH_16] != pkt->ch_16_eq_1_db+EQ_GAIN_OFFSET) {
            peq1G[CHANNEL_CH_16] = pkt->ch_16_eq_1_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq1F[CHANNEL_CH_16] != pkt->ch_16_eq_1_freq) {
            peq1F[CHANNEL_CH_16] = pkt->ch_16_eq_1_freq;
            valueChanged = YES;
        }
        if (peq1Q[CHANNEL_CH_16] != pkt->ch_16_eq_1_q) {
            peq1Q[CHANNEL_CH_16] = pkt->ch_16_eq_1_q;
            if (peq1Q[CHANNEL_CH_16] == PEQ_1_LOW_SHELF) {
                peq1Filter[CHANNEL_CH_16] = EQ_LOW_SHELF;
            } else if (peq1Q[CHANNEL_CH_16] == PEQ_1_HPF) {
                peq1Filter[CHANNEL_CH_16] = EQ_LOW_CUT;
            } else {
                peq1Filter[CHANNEL_CH_16] = EQ_PEAK;
                lastPeq1Q[CHANNEL_CH_16] = peq1Q[CHANNEL_CH_16];
            }
            valueChanged = YES;
        }
        if (peq2G[CHANNEL_CH_16] != pkt->ch_16_eq_2_db+EQ_GAIN_OFFSET) {
            peq2G[CHANNEL_CH_16] = pkt->ch_16_eq_2_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq2F[CHANNEL_CH_16] != pkt->ch_16_eq_2_freq) {
            peq2F[CHANNEL_CH_16] = pkt->ch_16_eq_2_freq;
            valueChanged = YES;
        }
        if (peq2Q[CHANNEL_CH_16] != pkt->ch_16_eq_2_q) {
            peq2Q[CHANNEL_CH_16] = pkt->ch_16_eq_2_q;
            valueChanged = YES;
        }
        if (peq3G[CHANNEL_CH_16] != pkt->ch_16_eq_3_db+EQ_GAIN_OFFSET) {
            peq3G[CHANNEL_CH_16] = pkt->ch_16_eq_3_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq3F[CHANNEL_CH_16] != pkt->ch_16_eq_3_freq) {
            peq3F[CHANNEL_CH_16] = pkt->ch_16_eq_3_freq;
            valueChanged = YES;
        }
        if (peq3Q[CHANNEL_CH_16] != pkt->ch_16_eq_3_q) {
            peq3Q[CHANNEL_CH_16] = pkt->ch_16_eq_3_q;
            valueChanged = YES;
        }
        if (peq4G[CHANNEL_CH_16] != pkt->ch_16_eq_4_db+EQ_GAIN_OFFSET) {
            peq4G[CHANNEL_CH_16] = pkt->ch_16_eq_4_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq4F[CHANNEL_CH_16] != pkt->ch_16_eq_4_freq) {
            peq4F[CHANNEL_CH_16] = pkt->ch_16_eq_4_freq;
            valueChanged = YES;
        }
        if (peq4Q[CHANNEL_CH_16] != pkt->ch_16_eq_4_q) {
            peq4Q[CHANNEL_CH_16] = pkt->ch_16_eq_4_q;
            if (peq4Q[CHANNEL_CH_16] == PEQ_4_HIGH_SHELF) {
                peq4Filter[CHANNEL_CH_16] = EQ_HIGH_SHELF;
            } else if (peq4Q[CHANNEL_CH_16] == PEQ_4_LPF) {
                peq4Filter[CHANNEL_CH_16] = EQ_HIGH_CUT;
            } else {
                peq4Filter[CHANNEL_CH_16] = EQ_PEAK;
                lastPeq4Q[CHANNEL_CH_16] = peq4Q[CHANNEL_CH_16];
            }
            valueChanged = YES;
        }
        int peqEnableValue = pkt->channel_16_ctrl;
        if (peq1Enabled[CHANNEL_CH_16] != ((peqEnableValue & 0x08) >> 3)) {
            peq1Enabled[CHANNEL_CH_16] = (peqEnableValue & 0x08) >> 3;
            valueChanged = YES;
        }
        if (peq2Enabled[CHANNEL_CH_16] != ((peqEnableValue & 0x10) >> 4)) {
            peq2Enabled[CHANNEL_CH_16] = (peqEnableValue & 0x10) >> 4;
            valueChanged = YES;
        }
        if (peq3Enabled[CHANNEL_CH_16] != ((peqEnableValue & 0x20) >> 5)) {
            peq3Enabled[CHANNEL_CH_16] = (peqEnableValue & 0x20) >> 5;
            valueChanged = YES;
        }
        if (peq4Enabled[CHANNEL_CH_16] != ((peqEnableValue & 0x40) >> 6)) {
            peq4Enabled[CHANNEL_CH_16] = (peqEnableValue & 0x40) >> 6;
            valueChanged = YES;
        }
        if (peqEnabled[CHANNEL_CH_16] != ((peqEnableValue & 0x80) >> 7)) {
            peqEnabled[CHANNEL_CH_16] = (peqEnableValue & 0x80) >> 7;
            valueChanged = YES;
        }
    }
    
    if (valueChanged) {
        [self updatePeqPlot:peqEnabled[CHANNEL_CH_16]];
        [self updatePeqForChannel:CHANNEL_CH_16];
        valueChanged = NO;
    }
}

- (void)processUsbAudioViewPage:(ViewPageUsbAudioPacket *)pkt {
    
    BOOL valueChanged = NO;
    
    // CHANNEL_CH_17:
    {
        if (peq1G[CHANNEL_CH_17] != pkt->ch_17_eq_1_db+EQ_GAIN_OFFSET) {
            peq1G[CHANNEL_CH_17] = pkt->ch_17_eq_1_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq1F[CHANNEL_CH_17] != pkt->ch_17_eq_1_freq) {
            peq1F[CHANNEL_CH_17] = pkt->ch_17_eq_1_freq;
            valueChanged = YES;
        }
        if (peq1Q[CHANNEL_CH_17] != pkt->ch_17_eq_1_q) {
            peq1Q[CHANNEL_CH_17] = pkt->ch_17_eq_1_q;
            if (peq1Q[CHANNEL_CH_17] == PEQ_1_LOW_SHELF) {
                [btnPeqFilterFront setImage:[UIImage imageNamed:@"frame-12.png"] forState:UIControlStateNormal];
                peq1Filter[CHANNEL_CH_17] = EQ_LOW_SHELF;
            } else if (peq1Q[CHANNEL_CH_17] == PEQ_1_HPF) {
                [btnPeqFilterFront setImage:[UIImage imageNamed:@"frame-13.png"] forState:UIControlStateNormal];
                peq1Filter[CHANNEL_CH_17] = EQ_LOW_CUT;
            } else {
                [btnPeqFilterFront setImage:[UIImage imageNamed:@"frame-11.png"] forState:UIControlStateNormal];
                peq1Filter[CHANNEL_CH_17] = EQ_PEAK;
                lastPeq1Q[CHANNEL_CH_17] = peq1Q[CHANNEL_CH_17];
            }
            valueChanged = YES;
        }
        if (peq2G[CHANNEL_CH_17] != pkt->ch_17_eq_2_db+EQ_GAIN_OFFSET) {
            peq2G[CHANNEL_CH_17] = pkt->ch_17_eq_2_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq2F[CHANNEL_CH_17] != pkt->ch_17_eq_2_freq) {
            peq2F[CHANNEL_CH_17] = pkt->ch_17_eq_2_freq;
            valueChanged = YES;
        }
        if (peq2Q[CHANNEL_CH_17] != pkt->ch_17_eq_2_q) {
            peq2Q[CHANNEL_CH_17] = pkt->ch_17_eq_2_q;
            valueChanged = YES;
        }
        if (peq3G[CHANNEL_CH_17] != pkt->ch_17_eq_3_db+EQ_GAIN_OFFSET) {
            peq3G[CHANNEL_CH_17] = pkt->ch_17_eq_3_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq3F[CHANNEL_CH_17] != pkt->ch_17_eq_3_freq) {
            peq3F[CHANNEL_CH_17] = pkt->ch_17_eq_3_freq;
            valueChanged = YES;
        }
        if (peq3Q[CHANNEL_CH_17] != pkt->ch_17_eq_3_q) {
            peq3Q[CHANNEL_CH_17] = pkt->ch_17_eq_3_q;
            valueChanged = YES;
        }
        if (peq4G[CHANNEL_CH_17] != pkt->ch_17_eq_4_db+EQ_GAIN_OFFSET) {
            peq4G[CHANNEL_CH_17] = pkt->ch_17_eq_4_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq4F[CHANNEL_CH_17] != pkt->ch_17_eq_4_freq) {
            peq4F[CHANNEL_CH_17] = pkt->ch_17_eq_4_freq;
            valueChanged = YES;
        }
        if (peq4Q[CHANNEL_CH_17] != pkt->ch_17_eq_4_q) {
            peq4Q[CHANNEL_CH_17] = pkt->ch_17_eq_4_q;
            if (peq4Q[CHANNEL_CH_17] == PEQ_4_HIGH_SHELF) {
                [btnPeqFilterBack setImage:[UIImage imageNamed:@"frame-15.png"] forState:UIControlStateNormal];
                peq4Filter[CHANNEL_CH_17] = EQ_HIGH_SHELF;
            } else if (peq4Q[CHANNEL_CH_17] == PEQ_4_LPF) {
                [btnPeqFilterBack setImage:[UIImage imageNamed:@"frame-14.png"] forState:UIControlStateNormal];
                peq4Filter[CHANNEL_CH_17] = EQ_HIGH_CUT;
            } else {
                [btnPeqFilterBack setImage:[UIImage imageNamed:@"frame-11.png"] forState:UIControlStateNormal];
                peq4Filter[CHANNEL_CH_17] = EQ_PEAK;
                lastPeq4Q[CHANNEL_CH_17] = peq4Q[CHANNEL_CH_17];
            }
            valueChanged = YES;
        }
        int peqEnableValue = pkt->ch_17_ctrl;
        if (peq1Enabled[CHANNEL_CH_17] != ((peqEnableValue & 0x08) >> 3)) {
            peq1Enabled[CHANNEL_CH_17] = (peqEnableValue & 0x08) >> 3;
            valueChanged = YES;
        }
        if (peq2Enabled[CHANNEL_CH_17] != ((peqEnableValue & 0x10) >> 4)) {
            peq2Enabled[CHANNEL_CH_17] = (peqEnableValue & 0x10) >> 4;
            valueChanged = YES;
        }
        if (peq3Enabled[CHANNEL_CH_17] != ((peqEnableValue & 0x20) >> 5)) {
            peq3Enabled[CHANNEL_CH_17] = (peqEnableValue & 0x20) >> 5;
            valueChanged = YES;
        }
        if (peq4Enabled[CHANNEL_CH_17] != ((peqEnableValue & 0x40) >> 6)) {
            peq4Enabled[CHANNEL_CH_17] = (peqEnableValue & 0x40) >> 6;
            valueChanged = YES;
        }
        if (peqEnabled[CHANNEL_CH_17] != ((peqEnableValue & 0x80) >> 7)) {
            peqEnabled[CHANNEL_CH_17] = (peqEnableValue & 0x80) >> 7;
            valueChanged = YES;
        }
    }
    
    if (valueChanged) {
        [self updatePeqPlot:peqEnabled[CHANNEL_CH_17]];
        [self updatePeqForChannel:CHANNEL_CH_17];
        valueChanged = NO;
    }
    
    
    // CHANNEL_CH_18:
    {
        if (peq1G[CHANNEL_CH_18] != pkt->ch_18_eq_1_db+EQ_GAIN_OFFSET) {
            peq1G[CHANNEL_CH_18] = pkt->ch_18_eq_1_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq1F[CHANNEL_CH_18] != pkt->ch_18_eq_1_freq) {
            peq1F[CHANNEL_CH_18] = pkt->ch_18_eq_1_freq;
            valueChanged = YES;
        }
        if (peq1Q[CHANNEL_CH_18] != pkt->ch_18_eq_1_q) {
            peq1Q[CHANNEL_CH_18] = pkt->ch_18_eq_1_q;
            if (peq1Q[CHANNEL_CH_18] == PEQ_1_LOW_SHELF) {
                [btnPeqFilterFront setImage:[UIImage imageNamed:@"frame-12.png"] forState:UIControlStateNormal];
                peq1Filter[CHANNEL_CH_18] = EQ_LOW_SHELF;
            } else if (peq1Q[CHANNEL_CH_18] == PEQ_1_HPF) {
                [btnPeqFilterFront setImage:[UIImage imageNamed:@"frame-13.png"] forState:UIControlStateNormal];
                peq1Filter[CHANNEL_CH_18] = EQ_LOW_CUT;
            } else {
                [btnPeqFilterFront setImage:[UIImage imageNamed:@"frame-11.png"] forState:UIControlStateNormal];
                peq1Filter[CHANNEL_CH_18] = EQ_PEAK;
                lastPeq1Q[CHANNEL_CH_18] = peq1Q[CHANNEL_CH_18];
            }
            valueChanged = YES;
        }
        if (peq2G[CHANNEL_CH_18] != pkt->ch_18_eq_2_db+EQ_GAIN_OFFSET) {
            peq2G[CHANNEL_CH_18] = pkt->ch_18_eq_2_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq2F[CHANNEL_CH_18] != pkt->ch_18_eq_2_freq) {
            peq2F[CHANNEL_CH_18] = pkt->ch_18_eq_2_freq;
            valueChanged = YES;
        }
        if (peq2Q[CHANNEL_CH_18] != pkt->ch_18_eq_2_q) {
            peq2Q[CHANNEL_CH_18] = pkt->ch_18_eq_2_q;
            valueChanged = YES;
        }
        if (peq3G[CHANNEL_CH_18] != pkt->ch_18_eq_3_db+EQ_GAIN_OFFSET) {
            peq3G[CHANNEL_CH_18] = pkt->ch_18_eq_3_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq3F[CHANNEL_CH_18] != pkt->ch_18_eq_3_freq) {
            peq3F[CHANNEL_CH_18] = pkt->ch_18_eq_3_freq;
            valueChanged = YES;
        }
        if (peq3Q[CHANNEL_CH_18] != pkt->ch_18_eq_3_q) {
            peq3Q[CHANNEL_CH_18] = pkt->ch_18_eq_3_q;
            valueChanged = YES;
        }
        if (peq4G[CHANNEL_CH_18] != pkt->ch_18_eq_4_db+EQ_GAIN_OFFSET) {
            peq4G[CHANNEL_CH_18] = pkt->ch_18_eq_4_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq4F[CHANNEL_CH_18] != pkt->ch_18_eq_4_freq) {
            peq4F[CHANNEL_CH_18] = pkt->ch_18_eq_4_freq;
            valueChanged = YES;
        }
        if (peq4Q[CHANNEL_CH_18] != pkt->ch_18_eq_4_q) {
            peq4Q[CHANNEL_CH_18] = pkt->ch_18_eq_4_q;
            if (peq4Q[CHANNEL_CH_18] == PEQ_4_HIGH_SHELF) {
                [btnPeqFilterBack setImage:[UIImage imageNamed:@"frame-15.png"] forState:UIControlStateNormal];
                peq4Filter[CHANNEL_CH_18] = EQ_HIGH_SHELF;
            } else if (peq4Q[CHANNEL_CH_18] == PEQ_4_LPF) {
                [btnPeqFilterBack setImage:[UIImage imageNamed:@"frame-14.png"] forState:UIControlStateNormal];
                peq4Filter[CHANNEL_CH_18] = EQ_HIGH_CUT;
            } else {
                [btnPeqFilterBack setImage:[UIImage imageNamed:@"frame-11.png"] forState:UIControlStateNormal];
                peq4Filter[CHANNEL_CH_18] = EQ_PEAK;
                lastPeq4Q[CHANNEL_CH_18] = peq4Q[CHANNEL_CH_18];
            }
            valueChanged = YES;
        }
        int peqEnableValue = pkt->ch_18_ctrl;
        if (peq1Enabled[CHANNEL_CH_18] != ((peqEnableValue & 0x08) >> 3)) {
            peq1Enabled[CHANNEL_CH_18] = (peqEnableValue & 0x08) >> 3;
            valueChanged = YES;
        }
        if (peq2Enabled[CHANNEL_CH_18] != ((peqEnableValue & 0x10) >> 4)) {
            peq2Enabled[CHANNEL_CH_18] = (peqEnableValue & 0x10) >> 4;
            valueChanged = YES;
        }
        if (peq3Enabled[CHANNEL_CH_18] != ((peqEnableValue & 0x20) >> 5)) {
            peq3Enabled[CHANNEL_CH_18] = (peqEnableValue & 0x20) >> 5;
            valueChanged = YES;
        }
        if (peq4Enabled[CHANNEL_CH_18] != ((peqEnableValue & 0x40) >> 6)) {
            peq4Enabled[CHANNEL_CH_18] = (peqEnableValue & 0x40) >> 6;
            valueChanged = YES;
        }
        if (peqEnabled[CHANNEL_CH_18] != ((peqEnableValue & 0x80) >> 7)) {
            peqEnabled[CHANNEL_CH_18] = (peqEnableValue & 0x80) >> 7;
            valueChanged = YES;
        }
    }
    
    if (valueChanged) {
        [self updatePeqPlot:peqEnabled[CHANNEL_CH_18]];
        [self updatePeqForChannel:CHANNEL_CH_18];
        valueChanged = NO;
    }    
}

- (void)processMultiViewPage:(ViewPageMultiPacket *)pkt {
    
    BOOL valueChanged = NO;
    
    // CHANNEL_MULTI_1
    {
        if (peq1G[CHANNEL_MULTI_1] != pkt->multi_1_eq_1_db+EQ_GAIN_OFFSET) {
            peq1G[CHANNEL_MULTI_1] = pkt->multi_1_eq_1_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq1F[CHANNEL_MULTI_1] != pkt->multi_1_eq_1_freq) {
            peq1F[CHANNEL_MULTI_1] = pkt->multi_1_eq_1_freq;
            valueChanged = YES;
        }
        if (peq1Q[CHANNEL_MULTI_1] != pkt->multi_1_eq_1_q) {
            peq1Q[CHANNEL_MULTI_1] = pkt->multi_1_eq_1_q;
            if (peq1Q[CHANNEL_MULTI_1] == PEQ_1_LOW_SHELF) {
                peq1Filter[CHANNEL_MULTI_1] = EQ_LOW_SHELF;
            } else if (peq1Q[CHANNEL_MULTI_1] == PEQ_1_HPF) {
                peq1Filter[CHANNEL_MULTI_1] = EQ_LOW_CUT;
            } else {
                peq1Filter[CHANNEL_MULTI_1] = EQ_PEAK;
                lastPeq1Q[CHANNEL_MULTI_1] = peq1Q[CHANNEL_MULTI_1];
            }
            valueChanged = YES;
        }
        if (peq2G[CHANNEL_MULTI_1] != pkt->multi_1_eq_2_db+EQ_GAIN_OFFSET) {
            peq2G[CHANNEL_MULTI_1] = pkt->multi_1_eq_2_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq2F[CHANNEL_MULTI_1] != pkt->multi_1_eq_2_freq) {
            peq2F[CHANNEL_MULTI_1] = pkt->multi_1_eq_2_freq;
            valueChanged = YES;
        }
        if (peq2Q[CHANNEL_MULTI_1] != pkt->multi_1_eq_2_q) {
            peq2Q[CHANNEL_MULTI_1] = pkt->multi_1_eq_2_q;
            valueChanged = YES;
        }
        if (peq3G[CHANNEL_MULTI_1] != pkt->multi_1_eq_3_db+EQ_GAIN_OFFSET) {
            peq3G[CHANNEL_MULTI_1] = pkt->multi_1_eq_3_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq3F[CHANNEL_MULTI_1] != pkt->multi_1_eq_3_freq) {
            peq3F[CHANNEL_MULTI_1] = pkt->multi_1_eq_3_freq;
            valueChanged = YES;
        }
        if (peq3Q[CHANNEL_MULTI_1] != pkt->multi_1_eq_3_q) {
            peq3Q[CHANNEL_MULTI_1] = pkt->multi_1_eq_3_q;
            valueChanged = YES;
        }
        if (peq4G[CHANNEL_MULTI_1] != pkt->multi_1_eq_4_db+EQ_GAIN_OFFSET) {
            peq4G[CHANNEL_MULTI_1] = pkt->multi_1_eq_4_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq4F[CHANNEL_MULTI_1] != pkt->multi_1_eq_4_freq) {
            peq4F[CHANNEL_MULTI_1] = pkt->multi_1_eq_4_freq;
            valueChanged = YES;
        }
        if (peq4Q[CHANNEL_MULTI_1] != pkt->multi_1_eq_4_q) {
            peq4Q[CHANNEL_MULTI_1] = pkt->multi_1_eq_4_q;
            if (peq4Q[CHANNEL_MULTI_1] == PEQ_4_HIGH_SHELF) {
                peq4Filter[CHANNEL_MULTI_1] = EQ_HIGH_SHELF;
            } else if (peq4Q[CHANNEL_MULTI_1] == PEQ_4_LPF) {
                peq4Filter[CHANNEL_MULTI_1] = EQ_HIGH_CUT;
            } else {
                peq4Filter[CHANNEL_MULTI_1] = EQ_PEAK;
                lastPeq4Q[CHANNEL_MULTI_1] = peq4Q[CHANNEL_MULTI_1];
            }
            valueChanged = YES;
        }
        int peqEnableValue = pkt->multi_1_ctrl;
        if (peq1Enabled[CHANNEL_MULTI_1] != ((peqEnableValue & 0x08) >> 3)) {
            peq1Enabled[CHANNEL_MULTI_1] = (peqEnableValue & 0x08) >> 3;
            valueChanged = YES;
        }
        if (peq2Enabled[CHANNEL_MULTI_1] != ((peqEnableValue & 0x10) >> 4)) {
            peq2Enabled[CHANNEL_MULTI_1] = (peqEnableValue & 0x10) >> 4;
            valueChanged = YES;
        }
        if (peq3Enabled[CHANNEL_MULTI_1] != ((peqEnableValue & 0x20) >> 5)) {
            peq3Enabled[CHANNEL_MULTI_1] = (peqEnableValue & 0x20) >> 5;
            valueChanged = YES;
        }
        if (peq4Enabled[CHANNEL_MULTI_1] != ((peqEnableValue & 0x40) >> 6)) {
            peq4Enabled[CHANNEL_MULTI_1] = (peqEnableValue & 0x40) >> 6;
            valueChanged = YES;
        }
        if (peqEnabled[CHANNEL_MULTI_1] != ((peqEnableValue & 0x80) >> 7)) {
            peqEnabled[CHANNEL_MULTI_1] = (peqEnableValue & 0x80) >> 7;
            valueChanged = YES;
        }
    }
    
    if (valueChanged) {
        [self updatePeqPlot:peqEnabled[CHANNEL_MULTI_1]];
        [self updatePeqForChannel:CHANNEL_MULTI_1];
        valueChanged = NO;
    }
    
    // CHANNEL_MULTI_2
    {
        if (peq1G[CHANNEL_MULTI_2] != pkt->multi_2_eq_1_db+EQ_GAIN_OFFSET) {
            peq1G[CHANNEL_MULTI_2] = pkt->multi_2_eq_1_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq1F[CHANNEL_MULTI_2] != pkt->multi_2_eq_1_freq) {
            peq1F[CHANNEL_MULTI_2] = pkt->multi_2_eq_1_freq;
            valueChanged = YES;
        }
        if (peq1Q[CHANNEL_MULTI_2] != pkt->multi_2_eq_1_q) {
            peq1Q[CHANNEL_MULTI_2] = pkt->multi_2_eq_1_q;
            if (peq1Q[CHANNEL_MULTI_2] == PEQ_1_LOW_SHELF) {
                peq1Filter[CHANNEL_MULTI_2] = EQ_LOW_SHELF;
            } else if (peq1Q[CHANNEL_MULTI_2] == PEQ_1_HPF) {
                peq1Filter[CHANNEL_MULTI_2] = EQ_LOW_CUT;
            } else {
                peq1Filter[CHANNEL_MULTI_2] = EQ_PEAK;
                lastPeq1Q[CHANNEL_MULTI_2] = peq1Q[CHANNEL_MULTI_2];
            }
            valueChanged = YES;
        }
        if (peq2G[CHANNEL_MULTI_2] != pkt->multi_2_eq_2_db+EQ_GAIN_OFFSET) {
            peq2G[CHANNEL_MULTI_2] = pkt->multi_2_eq_2_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq2F[CHANNEL_MULTI_2] != pkt->multi_2_eq_2_freq) {
            peq2F[CHANNEL_MULTI_2] = pkt->multi_2_eq_2_freq;
            valueChanged = YES;
        }
        if (peq2Q[CHANNEL_MULTI_2] != pkt->multi_2_eq_2_q) {
            peq2Q[CHANNEL_MULTI_2] = pkt->multi_2_eq_2_q;
            valueChanged = YES;
        }
        if (peq3G[CHANNEL_MULTI_2] != pkt->multi_2_eq_3_db+EQ_GAIN_OFFSET) {
            peq3G[CHANNEL_MULTI_2] = pkt->multi_2_eq_3_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq3F[CHANNEL_MULTI_2] != pkt->multi_2_eq_3_freq) {
            peq3F[CHANNEL_MULTI_2] = pkt->multi_2_eq_3_freq;
            valueChanged = YES;
        }
        if (peq3Q[CHANNEL_MULTI_2] != pkt->multi_2_eq_3_q) {
            peq3Q[CHANNEL_MULTI_2] = pkt->multi_2_eq_3_q;
            valueChanged = YES;
        }
        if (peq4G[CHANNEL_MULTI_2] != pkt->multi_2_eq_4_db+EQ_GAIN_OFFSET) {
            peq4G[CHANNEL_MULTI_2] = pkt->multi_2_eq_4_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq4F[CHANNEL_MULTI_2] != pkt->multi_2_eq_4_freq) {
            peq4F[CHANNEL_MULTI_2] = pkt->multi_2_eq_4_freq;
            valueChanged = YES;
        }
        if (peq4Q[CHANNEL_MULTI_2] != pkt->multi_2_eq_4_q) {
            peq4Q[CHANNEL_MULTI_2] = pkt->multi_2_eq_4_q;
            if (peq4Q[CHANNEL_MULTI_2] == PEQ_4_HIGH_SHELF) {
                peq4Filter[CHANNEL_MULTI_2] = EQ_HIGH_SHELF;
            } else if (peq4Q[CHANNEL_MULTI_2] == PEQ_4_LPF) {
                peq4Filter[CHANNEL_MULTI_2] = EQ_HIGH_CUT;
            } else {
                peq4Filter[CHANNEL_MULTI_2] = EQ_PEAK;
                lastPeq4Q[CHANNEL_MULTI_2] = peq4Q[CHANNEL_MULTI_2];
            }
            valueChanged = YES;
        }
        int peqEnableValue = pkt->multi_2_ctrl;
        if (peq1Enabled[CHANNEL_MULTI_2] != ((peqEnableValue & 0x08) >> 3)) {
            peq1Enabled[CHANNEL_MULTI_2] = (peqEnableValue & 0x08) >> 3;
            valueChanged = YES;
        }
        if (peq2Enabled[CHANNEL_MULTI_2] != ((peqEnableValue & 0x10) >> 4)) {
            peq2Enabled[CHANNEL_MULTI_2] = (peqEnableValue & 0x10) >> 4;
            valueChanged = YES;
        }
        if (peq3Enabled[CHANNEL_MULTI_2] != ((peqEnableValue & 0x20) >> 5)) {
            peq3Enabled[CHANNEL_MULTI_2] = (peqEnableValue & 0x20) >> 5;
            valueChanged = YES;
        }
        if (peq4Enabled[CHANNEL_MULTI_2] != ((peqEnableValue & 0x40) >> 6)) {
            peq4Enabled[CHANNEL_MULTI_2] = (peqEnableValue & 0x40) >> 6;
            valueChanged = YES;
        }
        if (peqEnabled[CHANNEL_MULTI_2] != ((peqEnableValue & 0x80) >> 7)) {
            peqEnabled[CHANNEL_MULTI_2] = (peqEnableValue & 0x80) >> 7;
            valueChanged = YES;
        }
    }
    
    if (valueChanged) {
        [self updatePeqPlot:peqEnabled[CHANNEL_MULTI_2]];
        [self updatePeqForChannel:CHANNEL_MULTI_2];
        valueChanged = NO;
    }
    
    // CHANNEL_MULTI_3
    {
        if (peq1G[CHANNEL_MULTI_3] != pkt->multi_3_eq_1_db+EQ_GAIN_OFFSET) {
            peq1G[CHANNEL_MULTI_3] = pkt->multi_3_eq_1_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq1F[CHANNEL_MULTI_3] != pkt->multi_3_eq_1_freq) {
            peq1F[CHANNEL_MULTI_3] = pkt->multi_3_eq_1_freq;
            valueChanged = YES;
        }
        if (peq1Q[CHANNEL_MULTI_3] != pkt->multi_3_eq_1_q) {
            peq1Q[CHANNEL_MULTI_3] = pkt->multi_3_eq_1_q;
            if (peq1Q[CHANNEL_MULTI_3] == PEQ_1_LOW_SHELF) {
                peq1Filter[CHANNEL_MULTI_3] = EQ_LOW_SHELF;
            } else if (peq1Q[CHANNEL_MULTI_3] == PEQ_1_HPF) {
                peq1Filter[CHANNEL_MULTI_3] = EQ_LOW_CUT;
            } else {
                peq1Filter[CHANNEL_MULTI_3] = EQ_PEAK;
                lastPeq1Q[CHANNEL_MULTI_3] = peq1Q[CHANNEL_MULTI_3];
            }
            valueChanged = YES;
        }
        if (peq2G[CHANNEL_MULTI_3] != pkt->multi_3_eq_2_db+EQ_GAIN_OFFSET) {
            peq2G[CHANNEL_MULTI_3] = pkt->multi_3_eq_2_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq2F[CHANNEL_MULTI_3] != pkt->multi_3_eq_2_freq) {
            peq2F[CHANNEL_MULTI_3] = pkt->multi_3_eq_2_freq;
            valueChanged = YES;
        }
        if (peq2Q[CHANNEL_MULTI_3] != pkt->multi_3_eq_2_q) {
            peq2Q[CHANNEL_MULTI_3] = pkt->multi_3_eq_2_q;
            valueChanged = YES;
        }
        if (peq3G[CHANNEL_MULTI_3] != pkt->multi_3_eq_3_db+EQ_GAIN_OFFSET) {
            peq3G[CHANNEL_MULTI_3] = pkt->multi_3_eq_3_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq3F[CHANNEL_MULTI_3] != pkt->multi_3_eq_3_freq) {
            peq3F[CHANNEL_MULTI_3] = pkt->multi_3_eq_3_freq;
            valueChanged = YES;
        }
        if (peq3Q[CHANNEL_MULTI_3] != pkt->multi_3_eq_3_q) {
            peq3Q[CHANNEL_MULTI_3] = pkt->multi_3_eq_3_q;
            valueChanged = YES;
        }
        if (peq4G[CHANNEL_MULTI_3] != pkt->multi_3_eq_4_db+EQ_GAIN_OFFSET) {
            peq4G[CHANNEL_MULTI_3] = pkt->multi_3_eq_4_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq4F[CHANNEL_MULTI_3] != pkt->multi_3_eq_4_freq) {
            peq4F[CHANNEL_MULTI_3] = pkt->multi_3_eq_4_freq;
            valueChanged = YES;
        }
        if (peq4Q[CHANNEL_MULTI_3] != pkt->multi_3_eq_4_q) {
            peq4Q[CHANNEL_MULTI_3] = pkt->multi_3_eq_4_q;
            if (peq4Q[CHANNEL_MULTI_3] == PEQ_4_HIGH_SHELF) {
                peq4Filter[CHANNEL_MULTI_3] = EQ_HIGH_SHELF;
            } else if (peq4Q[CHANNEL_MULTI_3] == PEQ_4_LPF) {
                peq4Filter[CHANNEL_MULTI_3] = EQ_HIGH_CUT;
            } else {
                peq4Filter[CHANNEL_MULTI_3] = EQ_PEAK;
                lastPeq4Q[CHANNEL_MULTI_3] = peq4Q[CHANNEL_MULTI_3];
            }
            valueChanged = YES;
        }
        int peqEnableValue = pkt->multi_3_ctrl;
        if (peq1Enabled[CHANNEL_MULTI_3] != ((peqEnableValue & 0x08) >> 3)) {
            peq1Enabled[CHANNEL_MULTI_3] = (peqEnableValue & 0x08) >> 3;
            valueChanged = YES;
        }
        if (peq2Enabled[CHANNEL_MULTI_3] != ((peqEnableValue & 0x10) >> 4)) {
            peq2Enabled[CHANNEL_MULTI_3] = (peqEnableValue & 0x10) >> 4;
            valueChanged = YES;
        }
        if (peq3Enabled[CHANNEL_MULTI_3] != ((peqEnableValue & 0x20) >> 5)) {
            peq3Enabled[CHANNEL_MULTI_3] = (peqEnableValue & 0x20) >> 5;
            valueChanged = YES;
        }
        if (peq4Enabled[CHANNEL_MULTI_3] != ((peqEnableValue & 0x40) >> 6)) {
            peq4Enabled[CHANNEL_MULTI_3] = (peqEnableValue & 0x40) >> 6;
            valueChanged = YES;
        }
        if (peqEnabled[CHANNEL_MULTI_3] != ((peqEnableValue & 0x80) >> 7)) {
            peqEnabled[CHANNEL_MULTI_3] = (peqEnableValue & 0x80) >> 7;
            valueChanged = YES;
        }
    }
    
    if (valueChanged) {
        [self updatePeqPlot:peqEnabled[CHANNEL_MULTI_3]];
        [self updatePeqForChannel:CHANNEL_MULTI_3];
        valueChanged = NO;
    }
    
    // CHANNEL_MULTI_4
    {
        if (peq1G[CHANNEL_MULTI_4] != pkt->multi_4_eq_1_db+EQ_GAIN_OFFSET) {
            peq1G[CHANNEL_MULTI_4] = pkt->multi_4_eq_1_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq1F[CHANNEL_MULTI_4] != pkt->multi_4_eq_1_freq) {
            peq1F[CHANNEL_MULTI_4] = pkt->multi_4_eq_1_freq;
            valueChanged = YES;
        }
        if (peq1Q[CHANNEL_MULTI_4] != pkt->multi_4_eq_1_q) {
            peq1Q[CHANNEL_MULTI_4] = pkt->multi_4_eq_1_q;
            if (peq1Q[CHANNEL_MULTI_4] == PEQ_1_LOW_SHELF) {
                peq1Filter[CHANNEL_MULTI_4] = EQ_LOW_SHELF;
            } else if (peq1Q[CHANNEL_MULTI_4] == PEQ_1_HPF) {
                peq1Filter[CHANNEL_MULTI_4] = EQ_LOW_CUT;
            } else {
                peq1Filter[CHANNEL_MULTI_4] = EQ_PEAK;
                lastPeq1Q[CHANNEL_MULTI_4] = peq1Q[CHANNEL_MULTI_4];
            }
            valueChanged = YES;
        }
        if (peq2G[CHANNEL_MULTI_4] != pkt->multi_4_eq_2_db+EQ_GAIN_OFFSET) {
            peq2G[CHANNEL_MULTI_4] = pkt->multi_4_eq_2_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq2F[CHANNEL_MULTI_4] != pkt->multi_4_eq_2_freq) {
            peq2F[CHANNEL_MULTI_4] = pkt->multi_4_eq_2_freq;
            valueChanged = YES;
        }
        if (peq2Q[CHANNEL_MULTI_4] != pkt->multi_4_eq_2_q) {
            peq2Q[CHANNEL_MULTI_4] = pkt->multi_4_eq_2_q;
            valueChanged = YES;
        }
        if (peq3G[CHANNEL_MULTI_4] != pkt->multi_4_eq_3_db+EQ_GAIN_OFFSET) {
            peq3G[CHANNEL_MULTI_4] = pkt->multi_4_eq_3_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq3F[CHANNEL_MULTI_4] != pkt->multi_4_eq_3_freq) {
            peq3F[CHANNEL_MULTI_4] = pkt->multi_4_eq_3_freq;
            valueChanged = YES;
        }
        if (peq3Q[CHANNEL_MULTI_4] != pkt->multi_4_eq_3_q) {
            peq3Q[CHANNEL_MULTI_4] = pkt->multi_4_eq_3_q;
            valueChanged = YES;
        }
        if (peq4G[CHANNEL_MULTI_4] != pkt->multi_4_eq_4_db+EQ_GAIN_OFFSET) {
            peq4G[CHANNEL_MULTI_4] = pkt->multi_4_eq_4_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq4F[CHANNEL_MULTI_4] != pkt->multi_4_eq_4_freq) {
            peq4F[CHANNEL_MULTI_4] = pkt->multi_4_eq_4_freq;
            valueChanged = YES;
        }
        if (peq4Q[CHANNEL_MULTI_4] != pkt->multi_4_eq_4_q) {
            peq4Q[CHANNEL_MULTI_4] = pkt->multi_4_eq_4_q;
            if (peq4Q[CHANNEL_MULTI_4] == PEQ_4_HIGH_SHELF) {
                peq4Filter[CHANNEL_MULTI_4] = EQ_HIGH_SHELF;
            } else if (peq4Q[CHANNEL_MULTI_4] == PEQ_4_LPF) {
                peq4Filter[CHANNEL_MULTI_4] = EQ_HIGH_CUT;
            } else {
                peq4Filter[CHANNEL_MULTI_4] = EQ_PEAK;
                lastPeq4Q[CHANNEL_MULTI_4] = peq4Q[CHANNEL_MULTI_4];
            }
            valueChanged = YES;
        }
        int peqEnableValue = pkt->multi_4_ctrl;
        if (peq1Enabled[CHANNEL_MULTI_4] != ((peqEnableValue & 0x08) >> 3)) {
            peq1Enabled[CHANNEL_MULTI_4] = (peqEnableValue & 0x08) >> 3;
            valueChanged = YES;
        }
        if (peq2Enabled[CHANNEL_MULTI_4] != ((peqEnableValue & 0x10) >> 4)) {
            peq2Enabled[CHANNEL_MULTI_4] = (peqEnableValue & 0x10) >> 4;
            valueChanged = YES;
        }
        if (peq3Enabled[CHANNEL_MULTI_4] != ((peqEnableValue & 0x20) >> 5)) {
            peq3Enabled[CHANNEL_MULTI_4] = (peqEnableValue & 0x20) >> 5;
            valueChanged = YES;
        }
        if (peq4Enabled[CHANNEL_MULTI_4] != ((peqEnableValue & 0x40) >> 6)) {
            peq4Enabled[CHANNEL_MULTI_4] = (peqEnableValue & 0x40) >> 6;
            valueChanged = YES;
        }
        if (peqEnabled[CHANNEL_MULTI_4] != ((peqEnableValue & 0x80) >> 7)) {
            peqEnabled[CHANNEL_MULTI_4] = (peqEnableValue & 0x80) >> 7;
            valueChanged = YES;
        }
    }
    
    if (valueChanged) {
        [self updatePeqPlot:peqEnabled[CHANNEL_MULTI_4]];
        [self updatePeqForChannel:CHANNEL_MULTI_4];
        valueChanged = NO;
    }
}

- (void)processMainViewPage:(ViewPageMainPacket *)pkt {
    
    int order = (pkt->main_l_r_ctrl & 0xE000) >> 13;
    if (order == ORDER_MAIN_EQ_DYN_DELAY ||
        order == ORDER_MAIN_DYN_EQ_DELAY) {
        if (mainEqType != MAIN_EQ_PEQ) {
            mainEqType = MAIN_EQ_PEQ;
            [self updatePreviewImageForChannel:MAIN];
        }
    } else {
        if (mainEqType != MAIN_EQ_GEQ) {
            mainEqType = MAIN_EQ_GEQ;
            [self updatePreviewImageForChannel:MAIN];
        }
    }
    
    if (mainEqType == MAIN_EQ_GEQ) {
        BOOL needsUpdate = NO;
        
        // Update GEQ sliders
        int geq1Value = pkt->geq_20_hz_db;
        int geq2Value = pkt->geq_25_hz_db;
        int geq3Value = pkt->geq_31_5_hz_db;
        int geq4Value = pkt->geq_40_hz_db;
        int geq5Value = pkt->geq_50_hz_db;
        int geq6Value = pkt->geq_63_hz_db;
        int geq7Value = pkt->geq_80_hz_db;
        int geq8Value = pkt->geq_100_hz_db;
        int geq9Value = pkt->geq_125_hz_db;
        int geq10Value = pkt->geq_160_hz_db;
        int geq11Value = pkt->geq_200_hz_db;
        int geq12Value = pkt->geq_250_hz_db;
        int geq13Value = pkt->geq_315_hz_db;
        int geq14Value = pkt->geq_400_hz_db;
        int geq15Value = pkt->geq_500_hz_db;
        int geq16Value = pkt->geq_630_hz_db;
        int geq17Value = pkt->geq_800_hz_db;
        int geq18Value = pkt->geq_1_khz_db;
        int geq19Value = pkt->geq_1_25_khz_db;
        int geq20Value = pkt->geq_1_6_khz_db;
        int geq21Value = pkt->geq_2_khz_db;
        int geq22Value = pkt->geq_2_5_khz_db;
        int geq23Value = pkt->geq_3_15_khz_db;
        int geq24Value = pkt->geq_4_khz_db;
        int geq25Value = pkt->geq_5_khz_db;
        int geq26Value = pkt->geq_6_3_khz_db;
        int geq27Value = pkt->geq_8_khz_db;
        int geq28Value = pkt->geq_10_khz_db;
        int geq29Value = pkt->geq_12_5_khz_db;
        int geq30Value = pkt->geq_16_khz_db;
        int geq31Value = pkt->geq_20_khz_db;
        
        if (sliderGeq1.value != geq1Value) {
            sliderGeq1.value = geq1Value;
            if (sliderGeq1.value > 0.0) {
                meter1Top.progress = sliderGeq1.value/GEQ_SLIDER_MAX_VALUE;
                meter1Btm.progress = 0.0;
            } else if (sliderGeq2.value < 0.0) {
                meter1Btm.progress = sliderGeq1.value/GEQ_SLIDER_MIN_VALUE;
                meter1Top.progress = 0.0;
            } else {
                meter1Top.progress = 0.0;
                meter1Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq2.value != geq2Value) {
            sliderGeq2.value = geq2Value;
            if (sliderGeq2.value > 0.0) {
                meter2Top.progress = sliderGeq2.value/GEQ_SLIDER_MAX_VALUE;
                meter2Btm.progress = 0.0;
            } else if (sliderGeq2.value < 0.0) {
                meter2Btm.progress = sliderGeq2.value/GEQ_SLIDER_MIN_VALUE;
                meter2Top.progress = 0.0;
            } else {
                meter2Top.progress = 0.0;
                meter2Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq3.value != geq3Value) {
            sliderGeq3.value = geq3Value;
            if (sliderGeq3.value > 0.0) {
                meter3Top.progress = sliderGeq3.value/GEQ_SLIDER_MAX_VALUE;
                meter3Btm.progress = 0.0;
            } else if (sliderGeq3.value < 0.0) {
                meter3Btm.progress = sliderGeq3.value/GEQ_SLIDER_MIN_VALUE;
                meter3Top.progress = 0.0;
            } else {
                meter3Top.progress = 0.0;
                meter3Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq4.value != geq4Value) {
            sliderGeq4.value = geq4Value;
            if (sliderGeq4.value > 0.0) {
                meter4Top.progress = sliderGeq4.value/GEQ_SLIDER_MAX_VALUE;
                meter4Btm.progress = 0.0;
            } else if (sliderGeq4.value < 0.0) {
                meter4Btm.progress = sliderGeq4.value/GEQ_SLIDER_MIN_VALUE;
                meter4Top.progress = 0.0;
            } else {
                meter4Top.progress = 0.0;
                meter4Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq5.value != geq5Value) {
            sliderGeq5.value = geq5Value;
            if (sliderGeq5.value > 0.0) {
                meter5Top.progress = sliderGeq5.value/GEQ_SLIDER_MAX_VALUE;
                meter5Btm.progress = 0.0;
            } else if (sliderGeq5.value < 0.0) {
                meter5Btm.progress = sliderGeq5.value/GEQ_SLIDER_MIN_VALUE;
                meter5Top.progress = 0.0;
            } else {
                meter5Top.progress = 0.0;
                meter5Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq6.value != geq6Value) {
            sliderGeq6.value = geq6Value;
            if (sliderGeq6.value > 0.0) {
                meter6Top.progress = sliderGeq6.value/GEQ_SLIDER_MAX_VALUE;
                meter6Btm.progress = 0.0;
            } else if (sliderGeq6.value < 0.0) {
                meter6Btm.progress = sliderGeq6.value/GEQ_SLIDER_MIN_VALUE;
                meter6Top.progress = 0.0;
            } else {
                meter6Top.progress = 0.0;
                meter6Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq7.value != geq7Value) {
            sliderGeq7.value = geq7Value;
            if (sliderGeq7.value > 0.0) {
                meter7Top.progress = sliderGeq7.value/GEQ_SLIDER_MAX_VALUE;
                meter7Btm.progress = 0.0;
            } else if (sliderGeq7.value < 0.0) {
                meter7Btm.progress = sliderGeq7.value/GEQ_SLIDER_MIN_VALUE;
                meter7Top.progress = 0.0;
            } else {
                meter7Top.progress = 0.0;
                meter7Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq8.value != geq8Value) {
            sliderGeq8.value = geq8Value;
            if (sliderGeq8.value > 0.0) {
                meter8Top.progress = sliderGeq8.value/GEQ_SLIDER_MAX_VALUE;
                meter8Btm.progress = 0.0;
            } else if (sliderGeq8.value < 0.0) {
                meter8Btm.progress = sliderGeq8.value/GEQ_SLIDER_MIN_VALUE;
                meter8Top.progress = 0.0;
            } else {
                meter8Top.progress = 0.0;
                meter8Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq9.value != geq9Value) {
            sliderGeq9.value = geq9Value;
            if (sliderGeq9.value > 0.0) {
                meter9Top.progress = sliderGeq9.value/GEQ_SLIDER_MAX_VALUE;
                meter9Btm.progress = 0.0;
            } else if (sliderGeq9.value < 0.0) {
                meter9Btm.progress = sliderGeq9.value/GEQ_SLIDER_MIN_VALUE;
                meter9Top.progress = 0.0;
            } else {
                meter9Top.progress = 0.0;
                meter9Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq10.value != geq10Value) {
            sliderGeq10.value = geq10Value;
            if (sliderGeq10.value > 0.0) {
                meter10Top.progress = sliderGeq10.value/GEQ_SLIDER_MAX_VALUE;
                meter10Btm.progress = 0.0;
            } else if (sliderGeq10.value < 0.0) {
                meter10Btm.progress = sliderGeq10.value/GEQ_SLIDER_MIN_VALUE;
                meter10Top.progress = 0.0;
            } else {
                meter10Top.progress = 0.0;
                meter10Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq11.value != geq11Value) {
            sliderGeq11.value = geq11Value;
            if (sliderGeq11.value > 0.0) {
                meter11Top.progress = sliderGeq11.value/GEQ_SLIDER_MAX_VALUE;
                meter11Btm.progress = 0.0;
            } else if (sliderGeq11.value < 0.0) {
                meter11Btm.progress = sliderGeq11.value/GEQ_SLIDER_MIN_VALUE;
                meter1Top.progress = 0.0;
            } else {
                meter11Top.progress = 0.0;
                meter11Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq12.value != geq12Value) {
            sliderGeq12.value = geq12Value;
            if (sliderGeq12.value > 0.0) {
                meter12Top.progress = sliderGeq12.value/GEQ_SLIDER_MAX_VALUE;
                meter12Btm.progress = 0.0;
            } else if (sliderGeq12.value < 0.0) {
                meter12Btm.progress = sliderGeq12.value/GEQ_SLIDER_MIN_VALUE;
                meter12Top.progress = 0.0;
            } else {
                meter12Top.progress = 0.0;
                meter12Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq13.value != geq13Value) {
            sliderGeq13.value = geq13Value;
            if (sliderGeq13.value > 0.0) {
                meter13Top.progress = sliderGeq13.value/GEQ_SLIDER_MAX_VALUE;
                meter13Btm.progress = 0.0;
            } else if (sliderGeq13.value < 0.0) {
                meter13Btm.progress = sliderGeq13.value/GEQ_SLIDER_MIN_VALUE;
                meter13Top.progress = 0.0;
            } else {
                meter13Top.progress = 0.0;
                meter13Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq14.value != geq14Value) {
            sliderGeq14.value = geq14Value;
            if (sliderGeq14.value > 0.0) {
                meter14Top.progress = sliderGeq14.value/GEQ_SLIDER_MAX_VALUE;
                meter14Btm.progress = 0.0;
            } else if (sliderGeq14.value < 0.0) {
                meter14Btm.progress = sliderGeq14.value/GEQ_SLIDER_MIN_VALUE;
                meter14Top.progress = 0.0;
            } else {
                meter14Top.progress = 0.0;
                meter14Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq15.value != geq15Value) {
            sliderGeq15.value = geq15Value;
            if (sliderGeq15.value > 0.0) {
                meter15Top.progress = sliderGeq15.value/GEQ_SLIDER_MAX_VALUE;
                meter15Btm.progress = 0.0;
            } else if (sliderGeq15.value < 0.0) {
                meter15Btm.progress = sliderGeq15.value/GEQ_SLIDER_MIN_VALUE;
                meter15Top.progress = 0.0;
            } else {
                meter15Top.progress = 0.0;
                meter15Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq16.value != geq16Value) {
            sliderGeq16.value = geq16Value;
            if (sliderGeq16.value > 0.0) {
                meter16Top.progress = sliderGeq16.value/GEQ_SLIDER_MAX_VALUE;
                meter16Btm.progress = 0.0;
            } else if (sliderGeq16.value < 0.0) {
                meter16Btm.progress = sliderGeq16.value/GEQ_SLIDER_MIN_VALUE;
                meter16Top.progress = 0.0;
            } else {
                meter16Top.progress = 0.0;
                meter16Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq17.value != geq17Value) {
            sliderGeq17.value = geq17Value;
            if (sliderGeq17.value > 0.0) {
                meter17Top.progress = sliderGeq17.value/GEQ_SLIDER_MAX_VALUE;
                meter17Btm.progress = 0.0;
            } else if (sliderGeq17.value < 0.0) {
                meter17Btm.progress = sliderGeq17.value/GEQ_SLIDER_MIN_VALUE;
                meter17Top.progress = 0.0;
            } else {
                meter17Top.progress = 0.0;
                meter17Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq18.value != geq18Value) {
            sliderGeq18.value = geq18Value;
            if (sliderGeq18.value > 0.0) {
                meter18Top.progress = sliderGeq18.value/GEQ_SLIDER_MAX_VALUE;
                meter18Btm.progress = 0.0;
            } else if (sliderGeq18.value < 0.0) {
                meter18Btm.progress = sliderGeq18.value/GEQ_SLIDER_MIN_VALUE;
                meter18Top.progress = 0.0;
            } else {
                meter18Top.progress = 0.0;
                meter18Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq19.value != geq19Value) {
            sliderGeq19.value = geq19Value;
            if (sliderGeq19.value > 0.0) {
                meter19Top.progress = sliderGeq19.value/GEQ_SLIDER_MAX_VALUE;
                meter19Btm.progress = 0.0;
            } else if (sliderGeq19.value < 0.0) {
                meter19Btm.progress = sliderGeq19.value/GEQ_SLIDER_MIN_VALUE;
                meter19Top.progress = 0.0;
            } else {
                meter19Top.progress = 0.0;
                meter19Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq20.value != geq20Value) {
            sliderGeq20.value = geq20Value;
            if (sliderGeq20.value > 0.0) {
                meter20Top.progress = sliderGeq20.value/GEQ_SLIDER_MAX_VALUE;
                meter20Btm.progress = 0.0;
            } else if (sliderGeq20.value < 0.0) {
                meter20Btm.progress = sliderGeq20.value/GEQ_SLIDER_MIN_VALUE;
                meter20Top.progress = 0.0;
            } else {
                meter20Top.progress = 0.0;
                meter20Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq21.value != geq21Value) {
            sliderGeq21.value = geq21Value;
            if (sliderGeq21.value > 0.0) {
                meter21Top.progress = sliderGeq21.value/GEQ_SLIDER_MAX_VALUE;
                meter21Btm.progress = 0.0;
            } else if (sliderGeq21.value < 0.0) {
                meter21Btm.progress = sliderGeq21.value/GEQ_SLIDER_MIN_VALUE;
                meter21Top.progress = 0.0;
            } else {
                meter21Top.progress = 0.0;
                meter21Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq22.value != geq22Value) {
            sliderGeq22.value = geq22Value;
            if (sliderGeq22.value > 0.0) {
                meter22Top.progress = sliderGeq22.value/GEQ_SLIDER_MAX_VALUE;
                meter22Btm.progress = 0.0;
            } else if (sliderGeq22.value < 0.0) {
                meter22Btm.progress = sliderGeq22.value/GEQ_SLIDER_MIN_VALUE;
                meter22Top.progress = 0.0;
            } else {
                meter22Top.progress = 0.0;
                meter22Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq23.value != geq23Value) {
            sliderGeq23.value = geq23Value;
            if (sliderGeq23.value > 0.0) {
                meter23Top.progress = sliderGeq23.value/GEQ_SLIDER_MAX_VALUE;
                meter23Btm.progress = 0.0;
            } else if (sliderGeq23.value < 0.0) {
                meter23Btm.progress = sliderGeq23.value/GEQ_SLIDER_MIN_VALUE;
                meter23Top.progress = 0.0;
            } else {
                meter23Top.progress = 0.0;
                meter23Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq24.value != geq24Value) {
            sliderGeq24.value = geq24Value;
            if (sliderGeq24.value > 0.0) {
                meter24Top.progress = sliderGeq24.value/GEQ_SLIDER_MAX_VALUE;
                meter24Btm.progress = 0.0;
            } else if (sliderGeq24.value < 0.0) {
                meter24Btm.progress = sliderGeq24.value/GEQ_SLIDER_MIN_VALUE;
                meter24Top.progress = 0.0;
            } else {
                meter24Top.progress = 0.0;
                meter24Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq25.value != geq25Value) {
            sliderGeq25.value = geq25Value;
            if (sliderGeq25.value > 0.0) {
                meter25Top.progress = sliderGeq25.value/GEQ_SLIDER_MAX_VALUE;
                meter25Btm.progress = 0.0;
            } else if (sliderGeq25.value < 0.0) {
                meter25Btm.progress = sliderGeq25.value/GEQ_SLIDER_MIN_VALUE;
                meter25Top.progress = 0.0;
            } else {
                meter25Top.progress = 0.0;
                meter25Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq26.value != geq26Value) {
            sliderGeq26.value = geq26Value;
            if (sliderGeq26.value > 0.0) {
                meter26Top.progress = sliderGeq26.value/GEQ_SLIDER_MAX_VALUE;
                meter26Btm.progress = 0.0;
            } else if (sliderGeq26.value < 0.0) {
                meter26Btm.progress = sliderGeq26.value/GEQ_SLIDER_MIN_VALUE;
                meter26Top.progress = 0.0;
            } else {
                meter26Top.progress = 0.0;
                meter26Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq27.value != geq27Value) {
            sliderGeq27.value = geq27Value;
            if (sliderGeq27.value > 0.0) {
                meter27Top.progress = sliderGeq27.value/GEQ_SLIDER_MAX_VALUE;
                meter27Btm.progress = 0.0;
            } else if (sliderGeq27.value < 0.0) {
                meter27Btm.progress = sliderGeq27.value/GEQ_SLIDER_MIN_VALUE;
                meter27Top.progress = 0.0;
            } else {
                meter27Top.progress = 0.0;
                meter27Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq28.value != geq28Value) {
            sliderGeq28.value = geq28Value;
            if (sliderGeq28.value > 0.0) {
                meter28Top.progress = sliderGeq28.value/GEQ_SLIDER_MAX_VALUE;
                meter28Btm.progress = 0.0;
            } else if (sliderGeq28.value < 0.0) {
                meter28Btm.progress = sliderGeq28.value/GEQ_SLIDER_MIN_VALUE;
                meter28Top.progress = 0.0;
            } else {
                meter28Top.progress = 0.0;
                meter28Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq29.value != geq29Value) {
            sliderGeq29.value = geq29Value;
            if (sliderGeq29.value > 0.0) {
                meter29Top.progress = sliderGeq29.value/GEQ_SLIDER_MAX_VALUE;
                meter29Btm.progress = 0.0;
            } else if (sliderGeq29.value < 0.0) {
                meter29Btm.progress = sliderGeq29.value/GEQ_SLIDER_MIN_VALUE;
                meter29Top.progress = 0.0;
            } else {
                meter29Top.progress = 0.0;
                meter29Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq30.value != geq30Value) {
            sliderGeq30.value = geq30Value;
            if (sliderGeq30.value > 0.0) {
                meter30Top.progress = sliderGeq30.value/GEQ_SLIDER_MAX_VALUE;
                meter30Btm.progress = 0.0;
            } else if (sliderGeq30.value < 0.0) {
                meter30Btm.progress = sliderGeq30.value/GEQ_SLIDER_MIN_VALUE;
                meter30Top.progress = 0.0;
            } else {
                meter30Top.progress = 0.0;
                meter30Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq31.value != geq31Value) {
            sliderGeq31.value = geq31Value;
            if (sliderGeq31.value > 0.0) {
                meter31Top.progress = sliderGeq31.value/GEQ_SLIDER_MAX_VALUE;
                meter31Btm.progress = 0.0;
            } else if (sliderGeq31.value < 0.0) {
                meter31Btm.progress = sliderGeq31.value/GEQ_SLIDER_MIN_VALUE;
                meter31Top.progress = 0.0;
            } else {
                meter31Top.progress = 0.0;
                meter31Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        int geqEnabled = (pkt->main_l_r_ctrl & 0x80) >> 7;
        if (btnGeqOnOff.selected != geqEnabled) {
            btnGeqOnOff.selected = geqEnabled;
            peqEnabled[CHANNEL_MAIN] = btnGeqOnOff.selected;
            [self updateGeqMeters];
            needsUpdate = YES;
        }
        
        if (needsUpdate) {
            [self updatePreviewImageForChannel:CHANNEL_MAIN];
        }
    } else {
        BOOL valueChanged = NO;
        
        if (peq1G[CHANNEL_MAIN] != pkt->main_l_r_eq_1_db+EQ_GAIN_OFFSET) {
            peq1G[CHANNEL_MAIN] = pkt->main_l_r_eq_1_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq1F[CHANNEL_MAIN] != pkt->main_l_r_eq_1_freq) {
            peq1F[CHANNEL_MAIN] = pkt->main_l_r_eq_1_freq;
            valueChanged = YES;
        }
        if (peq1Q[CHANNEL_MAIN] != pkt->main_l_r_eq_1_q) {
            peq1Q[CHANNEL_MAIN] = pkt->main_l_r_eq_1_q;
            if (peq1Q[CHANNEL_MAIN] == PEQ_1_LOW_SHELF) {
                peq1Filter[CHANNEL_MAIN] = EQ_LOW_SHELF;
            } else if (peq1Q[CHANNEL_MAIN] == PEQ_1_HPF) {
                peq1Filter[CHANNEL_MAIN] = EQ_LOW_CUT;
            } else {
                peq1Filter[CHANNEL_MAIN] = EQ_PEAK;
                lastPeq1Q[CHANNEL_MAIN] = peq1Q[CHANNEL_MAIN];
            }
            valueChanged = YES;
        }
        if (peq2G[CHANNEL_MAIN] != pkt->main_l_r_eq_2_db+EQ_GAIN_OFFSET) {
            peq2G[CHANNEL_MAIN] = pkt->main_l_r_eq_2_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq2F[CHANNEL_MAIN] != pkt->main_l_r_eq_2_freq) {
            peq2F[CHANNEL_MAIN] = pkt->main_l_r_eq_2_freq;
            valueChanged = YES;
        }
        if (peq2Q[CHANNEL_MAIN] != pkt->main_l_r_eq_2_q) {
            peq2Q[CHANNEL_MAIN] = pkt->main_l_r_eq_2_q;
            valueChanged = YES;
        }
        if (peq3G[CHANNEL_MAIN] != pkt->main_l_r_eq_3_db+EQ_GAIN_OFFSET) {
            peq3G[CHANNEL_MAIN] = pkt->main_l_r_eq_3_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq3F[CHANNEL_MAIN] != pkt->main_l_r_eq_3_freq) {
            peq3F[CHANNEL_MAIN] = pkt->main_l_r_eq_3_freq;
            valueChanged = YES;
        }
        if (peq3Q[CHANNEL_MAIN] != pkt->main_l_r_eq_3_q) {
            peq3Q[CHANNEL_MAIN] = pkt->main_l_r_eq_3_q;
            valueChanged = YES;
        }
        if (peq4G[CHANNEL_MAIN] != pkt->main_l_r_eq_4_db+EQ_GAIN_OFFSET) {
            peq4G[CHANNEL_MAIN] = pkt->main_l_r_eq_4_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq4F[CHANNEL_MAIN] != pkt->main_l_r_eq_4_freq) {
            peq4F[CHANNEL_MAIN] = pkt->main_l_r_eq_4_freq;
            valueChanged = YES;
        }
        if (peq4Q[CHANNEL_MAIN] != pkt->main_l_r_eq_4_q) {
            peq4Q[CHANNEL_MAIN] = pkt->main_l_r_eq_4_q;
            if (peq4Q[CHANNEL_MAIN] == PEQ_4_HIGH_SHELF) {
                peq4Filter[CHANNEL_MAIN] = EQ_HIGH_SHELF;
            } else if (peq4Q[CHANNEL_MAIN] == PEQ_4_LPF) {
                peq4Filter[CHANNEL_MAIN] = EQ_HIGH_CUT;
            } else {
                peq4Filter[CHANNEL_MAIN] = EQ_PEAK;
                lastPeq4Q[CHANNEL_MAIN] = peq4Q[CHANNEL_MAIN];
            }
            valueChanged = YES;
        }
        
        int peqEnableValue = pkt->main_l_r_ctrl;
        if (peq1Enabled[CHANNEL_MAIN] != ((peqEnableValue & 0x08) >> 3)) {
            peq1Enabled[CHANNEL_MAIN] = (peqEnableValue & 0x08) >> 3;
            valueChanged = YES;
        }
        if (peq2Enabled[CHANNEL_MAIN] != ((peqEnableValue & 0x10) >> 4)) {
            peq2Enabled[CHANNEL_MAIN] = (peqEnableValue & 0x10) >> 4;
            valueChanged = YES;
        }
        if (peq3Enabled[CHANNEL_MAIN] != ((peqEnableValue & 0x20) >> 5)) {
            peq3Enabled[CHANNEL_MAIN] = (peqEnableValue & 0x20) >> 5;
            valueChanged = YES;
        }
        if (peq4Enabled[CHANNEL_MAIN] != ((peqEnableValue & 0x40) >> 6)) {
            peq4Enabled[CHANNEL_MAIN] = (peqEnableValue & 0x40) >> 6;
            valueChanged = YES;
        }
        if (peqEnabled[CHANNEL_MAIN] != ((peqEnableValue & 0x80) >> 7)) {
            peqEnabled[CHANNEL_MAIN] = (peqEnableValue & 0x80) >> 7;
            valueChanged = YES;
        }
        
        if (valueChanged) {
            [self updatePeqPlot:peqEnabled[CHANNEL_MAIN]];
            [self updatePeqForChannel:CHANNEL_MAIN];
        }
    }
}

- (void)processMainReply:(EqMainPagePacket *)pkt
{
    BOOL needsUpdate = NO;
    
    if ([self.view isHidden]) {
        NSLog(@"processMainReply: view is hidden!");
        return;
    }
    
    if (peqLock) {
        NSLog(@"processMainReply: peqLock");
        return;
    }
    
    _mainCtrl = pkt->main_l_r_ctrl;
    _mainMeter = pkt->main_meter_pre_post;
    
    meterInL.progress = ((float)pkt->main_l_meter_eq_in/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
    meterInR.progress = ((float)pkt->main_r_meter_eq_in/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
    meterOutL.progress = ((float)pkt->main_l_meter_eq_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
    meterOutR.progress = ((float)pkt->main_r_meter_eq_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
    if (!sliderLock) {
        slider.value = pkt->main_fader;
        [self onSliderAction:nil];
    }
    
    BOOL sliderOn = pkt->main_l_r_ctrl & 0x01;
    if (sliderOn != btnOn.selected && ![viewController sendCommandExists:CMD_MAIN_LR_CTRL dspId:DSP_6]) {
        btnOn.selected = sliderOn;
    }
    BOOL sliderSolo = (pkt->main_meter_pre_post & 0x80) >> 7;
    if (sliderSolo != btnSolo.selected && ![viewController sendCommandExists:CMD_MAIN_METER dspId:DSP_6]) {
        btnSolo.selected = sliderSolo;
    }
    BOOL sliderMeter = pkt->main_meter_pre_post & 0x01;
    if (sliderMeter != btnMeterPrePost.selected && ![viewController sendCommandExists:CMD_MAIN_METER dspId:DSP_6]) {
        btnMeterPrePost.selected = sliderMeter;
    }
    
    int orderMain = (pkt->main_l_r_ctrl & 0xE000) >> 13;
    if (orderMain == ORDER_MAIN_EQ_DYN_DELAY ||
        orderMain == ORDER_MAIN_DYN_EQ_DELAY) {
        mainEqType = MAIN_EQ_PEQ;
        if (currentChannel == CHANNEL_MAIN) {
            [viewPeq setHidden:NO];
            [viewGeq setHidden:YES];
        }
        [meterInL setHidden:NO];
        [meterInR setHidden:NO];
        [meterOutL setHidden:NO];
        [meterOutR setHidden:NO];
    } else {
        mainEqType = MAIN_EQ_GEQ;
        if (currentChannel == CHANNEL_MAIN) {
          [viewPeq setHidden:YES];
          [viewGeq setHidden:NO];
        }
        [meterInL setHidden:YES];
        [meterInR setHidden:YES];
        [meterOutL setHidden:YES];
        [meterOutR setHidden:YES];
    }

    if (mainEqType == MAIN_EQ_GEQ) {
        
        // Update GEQ sliders
        int geq1Value = pkt->geq_20_hz_db;
        int geq2Value = pkt->geq_25_hz_db;
        int geq3Value = pkt->geq_31_5_hz_db;
        int geq4Value = pkt->geq_40_hz_db;
        int geq5Value = pkt->geq_50_hz_db;
        int geq6Value = pkt->geq_63_hz_db;
        int geq7Value = pkt->geq_80_hz_db;
        int geq8Value = pkt->geq_100_hz_db;
        int geq9Value = pkt->geq_125_hz_db;
        int geq10Value = pkt->geq_160_hz_db;
        int geq11Value = pkt->geq_200_hz_db;
        int geq12Value = pkt->geq_250_hz_db;
        int geq13Value = pkt->geq_315_hz_db;
        int geq14Value = pkt->geq_400_hz_db;
        int geq15Value = pkt->geq_500_hz_db;
        int geq16Value = pkt->geq_630_hz_db;
        int geq17Value = pkt->geq_800_hz_db;
        int geq18Value = pkt->geq_1_khz_db;
        int geq19Value = pkt->geq_1_25_khz_db;
        int geq20Value = pkt->geq_1_6_khz_db;
        int geq21Value = pkt->geq_2_khz_db;
        int geq22Value = pkt->geq_2_5_khz_db;
        int geq23Value = pkt->geq_3_15_khz_db;
        int geq24Value = pkt->geq_4_khz_db;
        int geq25Value = pkt->geq_5_khz_db;
        int geq26Value = pkt->geq_6_3_khz_db;
        int geq27Value = pkt->geq_8_khz_db;
        int geq28Value = pkt->geq_10_khz_db;
        int geq29Value = pkt->geq_12_5_khz_db;
        int geq30Value = pkt->geq_16_khz_db;
        int geq31Value = pkt->geq_20_khz_db;
        
        if (sliderGeq1.value != geq1Value && ![viewController sendCommandExists:CMD_MAIN_GEQ_20HZ_DB dspId:DSP_6]) {
            sliderGeq1.value = geq1Value;
            [self onSlider1Action:sliderGeq1];
            if (sliderGeq1.value > 0.0) {
                meter1Top.progress = sliderGeq1.value/GEQ_SLIDER_MAX_VALUE;
                meter1Btm.progress = 0.0;
            } else if (sliderGeq1.value < 0.0) {
                meter1Btm.progress = sliderGeq1.value/GEQ_SLIDER_MIN_VALUE;
                meter1Top.progress = 0.0;
            } else {
                meter1Top.progress = 0.0;
                meter1Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq2.value != geq2Value && ![viewController sendCommandExists:CMD_MAIN_GEQ_25HZ_DB dspId:DSP_6]) {
            sliderGeq2.value = geq2Value;
            if (sliderGeq2.value > 0.0) {
                meter2Top.progress = sliderGeq2.value/GEQ_SLIDER_MAX_VALUE;
                meter2Btm.progress = 0.0;
            } else if (sliderGeq2.value < 0.0) {
                meter2Btm.progress = sliderGeq2.value/GEQ_SLIDER_MIN_VALUE;
                meter2Top.progress = 0.0;
            } else {
                meter2Top.progress = 0.0;
                meter2Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq3.value != geq3Value && ![viewController sendCommandExists:CMD_MAIN_GEQ_31_5HZ_DB dspId:DSP_6]) {
            sliderGeq3.value = geq3Value;
            if (sliderGeq3.value > 0.0) {
                meter3Top.progress = sliderGeq3.value/GEQ_SLIDER_MAX_VALUE;
                meter3Btm.progress = 0.0;
            } else if (sliderGeq3.value < 0.0) {
                meter3Btm.progress = sliderGeq3.value/GEQ_SLIDER_MIN_VALUE;
                meter3Top.progress = 0.0;
            } else {
                meter3Top.progress = 0.0;
                meter3Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq4.value != geq4Value && ![viewController sendCommandExists:CMD_MAIN_GEQ_40HZ_DB dspId:DSP_6]) {
            sliderGeq4.value = geq4Value;
            if (sliderGeq4.value > 0.0) {
                meter4Top.progress = sliderGeq4.value/GEQ_SLIDER_MAX_VALUE;
                meter4Btm.progress = 0.0;
            } else if (sliderGeq4.value < 0.0) {
                meter4Btm.progress = sliderGeq4.value/GEQ_SLIDER_MIN_VALUE;
                meter4Top.progress = 0.0;
            } else {
                meter4Top.progress = 0.0;
                meter4Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq5.value != geq5Value && ![viewController sendCommandExists:CMD_MAIN_GEQ_50HZ_DB dspId:DSP_6]) {
            sliderGeq5.value = geq5Value;
            if (sliderGeq5.value > 0.0) {
                meter5Top.progress = sliderGeq5.value/GEQ_SLIDER_MAX_VALUE;
                meter5Btm.progress = 0.0;
            } else if (sliderGeq5.value < 0.0) {
                meter5Btm.progress = sliderGeq5.value/GEQ_SLIDER_MIN_VALUE;
                meter5Top.progress = 0.0;
            } else {
                meter5Top.progress = 0.0;
                meter5Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq6.value != geq6Value && ![viewController sendCommandExists:CMD_MAIN_GEQ_63HZ_DB dspId:DSP_6]) {
            sliderGeq6.value = geq6Value;
            if (sliderGeq6.value > 0.0) {
                meter6Top.progress = sliderGeq6.value/GEQ_SLIDER_MAX_VALUE;
                meter6Btm.progress = 0.0;
            } else if (sliderGeq6.value < 0.0) {
                meter6Btm.progress = sliderGeq6.value/GEQ_SLIDER_MIN_VALUE;
                meter6Top.progress = 0.0;
            } else {
                meter6Top.progress = 0.0;
                meter6Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq7.value != geq7Value && ![viewController sendCommandExists:CMD_MAIN_GEQ_80HZ_DB dspId:DSP_6]) {
            sliderGeq7.value = geq7Value;
            if (sliderGeq7.value > 0.0) {
                meter7Top.progress = sliderGeq7.value/GEQ_SLIDER_MAX_VALUE;
                meter7Btm.progress = 0.0;
            } else if (sliderGeq7.value < 0.0) {
                meter7Btm.progress = sliderGeq7.value/GEQ_SLIDER_MIN_VALUE;
                meter7Top.progress = 0.0;
            } else {
                meter7Top.progress = 0.0;
                meter7Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq8.value != geq8Value && ![viewController sendCommandExists:CMD_MAIN_GEQ_100HZ_DB dspId:DSP_6]) {
            sliderGeq8.value = geq8Value;
            if (sliderGeq8.value > 0.0) {
                meter8Top.progress = sliderGeq8.value/GEQ_SLIDER_MAX_VALUE;
                meter8Btm.progress = 0.0;
            } else if (sliderGeq8.value < 0.0) {
                meter8Btm.progress = sliderGeq8.value/GEQ_SLIDER_MIN_VALUE;
                meter8Top.progress = 0.0;
            } else {
                meter8Top.progress = 0.0;
                meter8Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq9.value != geq9Value && ![viewController sendCommandExists:CMD_MAIN_GEQ_125HZ_DB dspId:DSP_6]) {
            sliderGeq9.value = geq9Value;
            if (sliderGeq9.value > 0.0) {
                meter9Top.progress = sliderGeq9.value/GEQ_SLIDER_MAX_VALUE;
                meter9Btm.progress = 0.0;
            } else if (sliderGeq9.value < 0.0) {
                meter9Btm.progress = sliderGeq9.value/GEQ_SLIDER_MIN_VALUE;
                meter9Top.progress = 0.0;
            } else {
                meter9Top.progress = 0.0;
                meter9Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq10.value != geq10Value && ![viewController sendCommandExists:CMD_MAIN_GEQ_160HZ_DB dspId:DSP_6]) {
            sliderGeq10.value = geq10Value;
            if (sliderGeq10.value > 0.0) {
                meter10Top.progress = sliderGeq10.value/GEQ_SLIDER_MAX_VALUE;
                meter10Btm.progress = 0.0;
            } else if (sliderGeq10.value < 0.0) {
                meter10Btm.progress = sliderGeq10.value/GEQ_SLIDER_MIN_VALUE;
                meter10Top.progress = 0.0;
            } else {
                meter10Top.progress = 0.0;
                meter10Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq11.value != geq11Value && ![viewController sendCommandExists:CMD_MAIN_GEQ_200HZ_DB dspId:DSP_6]) {
            sliderGeq11.value = geq11Value;
            if (sliderGeq11.value > 0.0) {
                meter11Top.progress = sliderGeq11.value/GEQ_SLIDER_MAX_VALUE;
                meter11Btm.progress = 0.0;
            } else if (sliderGeq11.value < 0.0) {
                meter11Btm.progress = sliderGeq11.value/GEQ_SLIDER_MIN_VALUE;
                meter1Top.progress = 0.0;
            } else {
                meter11Top.progress = 0.0;
                meter11Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq12.value != geq12Value && ![viewController sendCommandExists:CMD_MAIN_GEQ_250HZ_DB dspId:DSP_6]) {
            sliderGeq12.value = geq12Value;
            if (sliderGeq12.value > 0.0) {
                meter12Top.progress = sliderGeq12.value/GEQ_SLIDER_MAX_VALUE;
                meter12Btm.progress = 0.0;
            } else if (sliderGeq12.value < 0.0) {
                meter12Btm.progress = sliderGeq12.value/GEQ_SLIDER_MIN_VALUE;
                meter12Top.progress = 0.0;
            } else {
                meter12Top.progress = 0.0;
                meter12Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq13.value != geq13Value && ![viewController sendCommandExists:CMD_MAIN_GEQ_315HZ_DB dspId:DSP_6]) {
            sliderGeq13.value = geq13Value;
            if (sliderGeq13.value > 0.0) {
                meter13Top.progress = sliderGeq13.value/GEQ_SLIDER_MAX_VALUE;
                meter13Btm.progress = 0.0;
            } else if (sliderGeq13.value < 0.0) {
                meter13Btm.progress = sliderGeq13.value/GEQ_SLIDER_MIN_VALUE;
                meter13Top.progress = 0.0;
            } else {
                meter13Top.progress = 0.0;
                meter13Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq14.value != geq14Value && ![viewController sendCommandExists:CMD_MAIN_GEQ_400HZ_DB dspId:DSP_6]) {
            sliderGeq14.value = geq14Value;
            if (sliderGeq14.value > 0.0) {
                meter14Top.progress = sliderGeq14.value/GEQ_SLIDER_MAX_VALUE;
                meter14Btm.progress = 0.0;
            } else if (sliderGeq14.value < 0.0) {
                meter14Btm.progress = sliderGeq14.value/GEQ_SLIDER_MIN_VALUE;
                meter14Top.progress = 0.0;
            } else {
                meter14Top.progress = 0.0;
                meter14Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq15.value != geq15Value && ![viewController sendCommandExists:CMD_MAIN_GEQ_500HZ_DB dspId:DSP_6]) {
            sliderGeq15.value = geq15Value;
            if (sliderGeq15.value > 0.0) {
                meter15Top.progress = sliderGeq15.value/GEQ_SLIDER_MAX_VALUE;
                meter15Btm.progress = 0.0;
            } else if (sliderGeq15.value < 0.0) {
                meter15Btm.progress = sliderGeq15.value/GEQ_SLIDER_MIN_VALUE;
                meter15Top.progress = 0.0;
            } else {
                meter15Top.progress = 0.0;
                meter15Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq16.value != geq16Value && ![viewController sendCommandExists:CMD_MAIN_GEQ_630HZ_DB dspId:DSP_6]) {
            sliderGeq16.value = geq16Value;
            if (sliderGeq16.value > 0.0) {
                meter16Top.progress = sliderGeq16.value/GEQ_SLIDER_MAX_VALUE;
                meter16Btm.progress = 0.0;
            } else if (sliderGeq16.value < 0.0) {
                meter16Btm.progress = sliderGeq16.value/GEQ_SLIDER_MIN_VALUE;
                meter16Top.progress = 0.0;
            } else {
                meter16Top.progress = 0.0;
                meter16Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq17.value != geq17Value && ![viewController sendCommandExists:CMD_MAIN_GEQ_800HZ_DB dspId:DSP_6]) {
            sliderGeq17.value = geq17Value;
            if (sliderGeq17.value > 0.0) {
                meter17Top.progress = sliderGeq17.value/GEQ_SLIDER_MAX_VALUE;
                meter17Btm.progress = 0.0;
            } else if (sliderGeq17.value < 0.0) {
                meter17Btm.progress = sliderGeq17.value/GEQ_SLIDER_MIN_VALUE;
                meter17Top.progress = 0.0;
            } else {
                meter17Top.progress = 0.0;
                meter17Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq18.value != geq18Value && ![viewController sendCommandExists:CMD_MAIN_GEQ_1KHZ_DB dspId:DSP_6]) {
            sliderGeq18.value = geq18Value;
            if (sliderGeq18.value > 0.0) {
                meter18Top.progress = sliderGeq18.value/GEQ_SLIDER_MAX_VALUE;
                meter18Btm.progress = 0.0;
            } else if (sliderGeq18.value < 0.0) {
                meter18Btm.progress = sliderGeq18.value/GEQ_SLIDER_MIN_VALUE;
                meter18Top.progress = 0.0;
            } else {
                meter18Top.progress = 0.0;
                meter18Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq19.value != geq19Value && ![viewController sendCommandExists:CMD_MAIN_GEQ_1_25KHZ_DB dspId:DSP_6]) {
            sliderGeq19.value = geq19Value;
            if (sliderGeq19.value > 0.0) {
                meter19Top.progress = sliderGeq19.value/GEQ_SLIDER_MAX_VALUE;
                meter19Btm.progress = 0.0;
            } else if (sliderGeq19.value < 0.0) {
                meter19Btm.progress = sliderGeq19.value/GEQ_SLIDER_MIN_VALUE;
                meter19Top.progress = 0.0;
            } else {
                meter19Top.progress = 0.0;
                meter19Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq20.value != geq20Value && ![viewController sendCommandExists:CMD_MAIN_GEQ_1_6KHZ_DB dspId:DSP_6]) {
            sliderGeq20.value = geq20Value;
            if (sliderGeq20.value > 0.0) {
                meter20Top.progress = sliderGeq20.value/GEQ_SLIDER_MAX_VALUE;
                meter20Btm.progress = 0.0;
            } else if (sliderGeq20.value < 0.0) {
                meter20Btm.progress = sliderGeq20.value/GEQ_SLIDER_MIN_VALUE;
                meter20Top.progress = 0.0;
            } else {
                meter20Top.progress = 0.0;
                meter20Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq21.value != geq21Value && ![viewController sendCommandExists:CMD_MAIN_GEQ_2KHZ_DB dspId:DSP_6]) {
            sliderGeq21.value = geq21Value;
            if (sliderGeq21.value > 0.0) {
                meter21Top.progress = sliderGeq21.value/GEQ_SLIDER_MAX_VALUE;
                meter21Btm.progress = 0.0;
            } else if (sliderGeq21.value < 0.0) {
                meter21Btm.progress = sliderGeq21.value/GEQ_SLIDER_MIN_VALUE;
                meter21Top.progress = 0.0;
            } else {
                meter21Top.progress = 0.0;
                meter21Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq22.value != geq22Value && ![viewController sendCommandExists:CMD_MAIN_GEQ_2_5KHZ_DB dspId:DSP_6]) {
            sliderGeq22.value = geq22Value;
            if (sliderGeq22.value > 0.0) {
                meter22Top.progress = sliderGeq22.value/GEQ_SLIDER_MAX_VALUE;
                meter22Btm.progress = 0.0;
            } else if (sliderGeq22.value < 0.0) {
                meter22Btm.progress = sliderGeq22.value/GEQ_SLIDER_MIN_VALUE;
                meter22Top.progress = 0.0;
            } else {
                meter22Top.progress = 0.0;
                meter22Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq23.value != geq23Value && ![viewController sendCommandExists:CMD_MAIN_GEQ_3_15KHZ_DB dspId:DSP_6]) {
            sliderGeq23.value = geq23Value;
            if (sliderGeq23.value > 0.0) {
                meter23Top.progress = sliderGeq23.value/GEQ_SLIDER_MAX_VALUE;
                meter23Btm.progress = 0.0;
            } else if (sliderGeq23.value < 0.0) {
                meter23Btm.progress = sliderGeq23.value/GEQ_SLIDER_MIN_VALUE;
                meter23Top.progress = 0.0;
            } else {
                meter23Top.progress = 0.0;
                meter23Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq24.value != geq24Value && ![viewController sendCommandExists:CMD_MAIN_GEQ_4KHZ_DB dspId:DSP_6]) {
            sliderGeq24.value = geq24Value;
            if (sliderGeq24.value > 0.0) {
                meter24Top.progress = sliderGeq24.value/GEQ_SLIDER_MAX_VALUE;
                meter24Btm.progress = 0.0;
            } else if (sliderGeq24.value < 0.0) {
                meter24Btm.progress = sliderGeq24.value/GEQ_SLIDER_MIN_VALUE;
                meter24Top.progress = 0.0;
            } else {
                meter24Top.progress = 0.0;
                meter24Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq25.value != geq25Value && ![viewController sendCommandExists:CMD_MAIN_GEQ_5KHZ_DB dspId:DSP_6]) {
            sliderGeq25.value = geq25Value;
            if (sliderGeq25.value > 0.0) {
                meter25Top.progress = sliderGeq25.value/GEQ_SLIDER_MAX_VALUE;
                meter25Btm.progress = 0.0;
            } else if (sliderGeq25.value < 0.0) {
                meter25Btm.progress = sliderGeq25.value/GEQ_SLIDER_MIN_VALUE;
                meter25Top.progress = 0.0;
            } else {
                meter25Top.progress = 0.0;
                meter25Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq26.value != geq26Value && ![viewController sendCommandExists:CMD_MAIN_GEQ_6_3KHZ_DB dspId:DSP_6]) {
            sliderGeq26.value = geq26Value;
            if (sliderGeq26.value > 0.0) {
                meter26Top.progress = sliderGeq26.value/GEQ_SLIDER_MAX_VALUE;
                meter26Btm.progress = 0.0;
            } else if (sliderGeq26.value < 0.0) {
                meter26Btm.progress = sliderGeq26.value/GEQ_SLIDER_MIN_VALUE;
                meter26Top.progress = 0.0;
            } else {
                meter26Top.progress = 0.0;
                meter26Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq27.value != geq27Value && ![viewController sendCommandExists:CMD_MAIN_GEQ_8KHZ_DB dspId:DSP_6]) {
            sliderGeq27.value = geq27Value;
            if (sliderGeq27.value > 0.0) {
                meter27Top.progress = sliderGeq27.value/GEQ_SLIDER_MAX_VALUE;
                meter27Btm.progress = 0.0;
            } else if (sliderGeq27.value < 0.0) {
                meter27Btm.progress = sliderGeq27.value/GEQ_SLIDER_MIN_VALUE;
                meter27Top.progress = 0.0;
            } else {
                meter27Top.progress = 0.0;
                meter27Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq28.value != geq28Value && ![viewController sendCommandExists:CMD_MAIN_GEQ_10KHZ_DB dspId:DSP_6]) {
            sliderGeq28.value = geq28Value;
            if (sliderGeq28.value > 0.0) {
                meter28Top.progress = sliderGeq28.value/GEQ_SLIDER_MAX_VALUE;
                meter28Btm.progress = 0.0;
            } else if (sliderGeq28.value < 0.0) {
                meter28Btm.progress = sliderGeq28.value/GEQ_SLIDER_MIN_VALUE;
                meter28Top.progress = 0.0;
            } else {
                meter28Top.progress = 0.0;
                meter28Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq29.value != geq29Value && ![viewController sendCommandExists:CMD_MAIN_GEQ_12_5KHZ_DB dspId:DSP_6]) {
            sliderGeq29.value = geq29Value;
            if (sliderGeq29.value > 0.0) {
                meter29Top.progress = sliderGeq29.value/GEQ_SLIDER_MAX_VALUE;
                meter29Btm.progress = 0.0;
            } else if (sliderGeq29.value < 0.0) {
                meter29Btm.progress = sliderGeq29.value/GEQ_SLIDER_MIN_VALUE;
                meter29Top.progress = 0.0;
            } else {
                meter29Top.progress = 0.0;
                meter29Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq30.value != geq30Value && ![viewController sendCommandExists:CMD_MAIN_GEQ_16KHZ_DB dspId:DSP_6]) {
            sliderGeq30.value = geq30Value;
            if (sliderGeq30.value > 0.0) {
                meter30Top.progress = sliderGeq30.value/GEQ_SLIDER_MAX_VALUE;
                meter30Btm.progress = 0.0;
            } else if (sliderGeq30.value < 0.0) {
                meter30Btm.progress = sliderGeq30.value/GEQ_SLIDER_MIN_VALUE;
                meter30Top.progress = 0.0;
            } else {
                meter30Top.progress = 0.0;
                meter30Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq31.value != geq31Value && ![viewController sendCommandExists:CMD_MAIN_GEQ_20KHZ_DB dspId:DSP_6]) {
            sliderGeq31.value = geq31Value;
            if (sliderGeq31.value > 0.0) {
                meter31Top.progress = sliderGeq31.value/GEQ_SLIDER_MAX_VALUE;
                meter31Btm.progress = 0.0;
            } else if (sliderGeq31.value < 0.0) {
                meter31Btm.progress = sliderGeq31.value/GEQ_SLIDER_MIN_VALUE;
                meter31Top.progress = 0.0;
            } else {
                meter31Top.progress = 0.0;
                meter31Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }        
        int geqEnabled = (pkt->main_l_r_ctrl & 0x80) >> 7;
        if (btnGeqOnOff.selected != geqEnabled) {
            btnGeqOnOff.selected = geqEnabled;
            peqEnabled[CHANNEL_MAIN] = btnGeqOnOff.selected;
            [self updateGeqMeters];
            needsUpdate = YES;
        }
        
        if (needsUpdate) {
            [self updatePreviewImageForChannel:CHANNEL_MAIN];
        }
    } else {
        BOOL valueChanged = NO;
        
        switch (currentChannel) {
            case CHANNEL_MAIN:
            {
                if (peq1G[currentChannel] != pkt->main_l_r_eq_1_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_MAIN_LR_EQ1_DB dspId:DSP_6]) {
                    peq1G[currentChannel] = pkt->main_l_r_eq_1_db+EQ_GAIN_OFFSET;
                    valueChanged = YES;
                }
                if (peq1F[currentChannel] != pkt->main_l_r_eq_1_freq && ![viewController sendCommandExists:CMD_MAIN_LR_EQ1_FREQ dspId:DSP_6]) {
                    peq1F[currentChannel] = pkt->main_l_r_eq_1_freq;
                    valueChanged = YES;
                }
                if (peq1Q[currentChannel] != pkt->main_l_r_eq_1_q && ![viewController sendCommandExists:CMD_MAIN_LR_EQ1_Q dspId:DSP_6]) {
                    peq1Q[currentChannel] = pkt->main_l_r_eq_1_q;
                    if (peq1Q[currentChannel] == PEQ_1_LOW_SHELF) {
                        [btnPeqFilterFront setImage:[UIImage imageNamed:@"frame-12.png"] forState:UIControlStateNormal];
                        peq1Filter[currentChannel] = EQ_LOW_SHELF;
                    } else if (peq1Q[currentChannel] == PEQ_1_HPF) {
                        [btnPeqFilterFront setImage:[UIImage imageNamed:@"frame-13.png"] forState:UIControlStateNormal];
                        peq1Filter[currentChannel] = EQ_LOW_CUT;
                    } else {
                        [btnPeqFilterFront setImage:[UIImage imageNamed:@"frame-11.png"] forState:UIControlStateNormal];
                        peq1Filter[currentChannel] = EQ_PEAK;
                        lastPeq1Q[currentChannel] = peq1Q[currentChannel];
                    }
                    valueChanged = YES;
                }
                if (peq2G[currentChannel] != pkt->main_l_r_eq_2_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_MAIN_LR_EQ2_DB dspId:DSP_6]) {
                    peq2G[currentChannel] = pkt->main_l_r_eq_2_db+EQ_GAIN_OFFSET;
                    valueChanged = YES;
                }
                if (peq2F[currentChannel] != pkt->main_l_r_eq_2_freq && ![viewController sendCommandExists:CMD_MAIN_LR_EQ2_FREQ dspId:DSP_6]) {
                    peq2F[currentChannel] = pkt->main_l_r_eq_2_freq;
                    valueChanged = YES;
                }
                if (peq2Q[currentChannel] != pkt->main_l_r_eq_2_q && ![viewController sendCommandExists:CMD_MAIN_LR_EQ2_Q dspId:DSP_6]) {
                    peq2Q[currentChannel] = pkt->main_l_r_eq_2_q;
                    valueChanged = YES;
                }
                if (peq3G[currentChannel] != pkt->main_l_r_eq_3_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_MAIN_LR_EQ3_DB dspId:DSP_6]) {
                    peq3G[currentChannel] = pkt->main_l_r_eq_3_db+EQ_GAIN_OFFSET;
                    valueChanged = YES;
                }
                if (peq3F[currentChannel] != pkt->main_l_r_eq_3_freq && ![viewController sendCommandExists:CMD_MAIN_LR_EQ3_FREQ dspId:DSP_6]) {
                    peq3F[currentChannel] = pkt->main_l_r_eq_3_freq;
                    valueChanged = YES;
                }
                if (peq3Q[currentChannel] != pkt->main_l_r_eq_3_q && ![viewController sendCommandExists:CMD_MAIN_LR_EQ3_Q dspId:DSP_6]) {
                    peq3Q[currentChannel] = pkt->main_l_r_eq_3_q;
                    valueChanged = YES;
                }
                if (peq4G[currentChannel] != pkt->main_l_r_eq_4_db+EQ_GAIN_OFFSET && ![viewController sendCommandExists:CMD_MAIN_LR_EQ4_DB dspId:DSP_6]) {
                    peq4G[currentChannel] = pkt->main_l_r_eq_4_db+EQ_GAIN_OFFSET;
                    valueChanged = YES;
                }
                if (peq4F[currentChannel] != pkt->main_l_r_eq_4_freq && ![viewController sendCommandExists:CMD_MAIN_LR_EQ4_FREQ dspId:DSP_6]) {
                    peq4F[currentChannel] = pkt->main_l_r_eq_4_freq;
                    valueChanged = YES;
                }
                if (peq4Q[currentChannel] != pkt->main_l_r_eq_4_q && ![viewController sendCommandExists:CMD_MAIN_LR_EQ4_Q dspId:DSP_6]) {
                    peq4Q[currentChannel] = pkt->main_l_r_eq_4_q;
                    if (peq4Q[currentChannel] == PEQ_4_HIGH_SHELF) {
                        [btnPeqFilterBack setImage:[UIImage imageNamed:@"frame-15.png"] forState:UIControlStateNormal];
                        peq4Filter[currentChannel] = EQ_HIGH_SHELF;
                    } else if (peq4Q[currentChannel] == PEQ_4_LPF) {
                        [btnPeqFilterBack setImage:[UIImage imageNamed:@"frame-14.png"] forState:UIControlStateNormal];
                        peq4Filter[currentChannel] = EQ_HIGH_CUT;
                    } else {
                        [btnPeqFilterBack setImage:[UIImage imageNamed:@"frame-11.png"] forState:UIControlStateNormal];
                        peq4Filter[currentChannel] = EQ_PEAK;
                        lastPeq4Q[currentChannel] = peq4Q[currentChannel];
                    }
                    valueChanged = YES;
                }
                int peqEnableValue = pkt->main_l_r_ctrl;
                if (btnPeq1.selected != ((peqEnableValue & 0x08) >> 3) && ![viewController sendCommandExists:CMD_MAIN_LR_CTRL dspId:DSP_6]) {
                    btnPeq1.selected = peq1Enabled[currentChannel] = (peqEnableValue & 0x08) >> 3;
                    valueChanged = YES;
                }
                if (btnPeq2.selected != ((peqEnableValue & 0x10) >> 4) && ![viewController sendCommandExists:CMD_MAIN_LR_CTRL dspId:DSP_6]) {
                    btnPeq2.selected = peq2Enabled[currentChannel] = (peqEnableValue & 0x10) >> 4;
                    valueChanged = YES;
                }
                if (btnPeq3.selected != ((peqEnableValue & 0x20) >> 5) && ![viewController sendCommandExists:CMD_MAIN_LR_CTRL dspId:DSP_6]) {
                    btnPeq3.selected = peq3Enabled[currentChannel] = (peqEnableValue & 0x20) >> 5;
                    valueChanged = YES;
                }
                if (btnPeq4.selected != ((peqEnableValue & 0x40) >> 6) && ![viewController sendCommandExists:CMD_MAIN_LR_CTRL dspId:DSP_6]) {
                    btnPeq4.selected = peq4Enabled[currentChannel] = (peqEnableValue & 0x40) >> 6;
                    valueChanged = YES;
                }
                }
            default:
                break;
        }
        
        // Update PEQ enabled status
        btnPeqOnOff.selected = peqEnabled[currentChannel];
        [self updatePeqPlot:btnPeqOnOff.selected];
        
        // Update ball 1-4 status
        if (btnPeq1.selected) {
            btnBall1.hidden = NO;
        } else {
            btnBall1.hidden = YES;
        }
        if (btnPeq2.selected) {
            btnBall2.hidden = NO;
        } else {
            btnBall2.hidden = YES;
        }
        if (btnPeq3.selected) {
            btnBall3.hidden = NO;
        } else {
            btnBall3.hidden = YES;
        }
        if (btnPeq4.selected) {
            btnBall4.hidden = NO;
        } else {
            btnBall4.hidden = YES;
        }
        
        if (valueChanged) {
            [self updatePeqFrame];
        }
    }
}

- (void)processChannel1_8PageReply:(ChannelCh1_8PagePacket *)pkt {
    
    BOOL valueChanged = NO;
    
    // CHANNEL_CH_1
    {
        if (peq1G[CHANNEL_CH_1] != pkt->ch_1_eq_1_db+EQ_GAIN_OFFSET) {
            peq1G[CHANNEL_CH_1] = pkt->ch_1_eq_1_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq1F[CHANNEL_CH_1] != pkt->ch_1_eq_1_freq) {
            peq1F[CHANNEL_CH_1] = pkt->ch_1_eq_1_freq;
            valueChanged = YES;
        }
        if (peq1Q[CHANNEL_CH_1] != pkt->ch_1_eq_1_q) {
            peq1Q[CHANNEL_CH_1] = pkt->ch_1_eq_1_q;
            if (peq1Q[CHANNEL_CH_1] == PEQ_1_LOW_SHELF) {
                peq1Filter[CHANNEL_CH_1] = EQ_LOW_SHELF;
            } else if (peq1Q[CHANNEL_CH_1] == PEQ_1_HPF) {
                peq1Filter[CHANNEL_CH_1] = EQ_LOW_CUT;
            } else {
                peq1Filter[CHANNEL_CH_1] = EQ_PEAK;
                lastPeq1Q[CHANNEL_CH_1] = peq1Q[CHANNEL_CH_1];
            }
            valueChanged = YES;
        }
        if (peq2G[CHANNEL_CH_1] != pkt->ch_1_eq_2_db+EQ_GAIN_OFFSET) {
            peq2G[CHANNEL_CH_1] = pkt->ch_1_eq_2_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq2F[CHANNEL_CH_1] != pkt->ch_1_eq_2_freq) {
            peq2F[CHANNEL_CH_1] = pkt->ch_1_eq_2_freq;
            valueChanged = YES;
        }
        if (peq2Q[CHANNEL_CH_1] != pkt->ch_1_eq_2_q) {
            peq2Q[CHANNEL_CH_1] = pkt->ch_1_eq_2_q;
            valueChanged = YES;
        }
        if (peq3G[CHANNEL_CH_1] != pkt->ch_1_eq_3_db+EQ_GAIN_OFFSET) {
            peq3G[CHANNEL_CH_1] = pkt->ch_1_eq_3_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq3F[CHANNEL_CH_1] != pkt->ch_1_eq_3_freq) {
            peq3F[CHANNEL_CH_1] = pkt->ch_1_eq_3_freq;
            valueChanged = YES;
        }
        if (peq3Q[CHANNEL_CH_1] != pkt->ch_1_eq_3_q) {
            peq3Q[CHANNEL_CH_1] = pkt->ch_1_eq_3_q;
            valueChanged = YES;
        }
        if (peq4G[CHANNEL_CH_1] != pkt->ch_1_eq_4_db+EQ_GAIN_OFFSET) {
            peq4G[CHANNEL_CH_1] = pkt->ch_1_eq_4_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq4F[CHANNEL_CH_1] != pkt->ch_1_eq_4_freq) {
            peq4F[CHANNEL_CH_1] = pkt->ch_1_eq_4_freq;
            valueChanged = YES;
        }
        if (peq4Q[CHANNEL_CH_1] != pkt->ch_1_eq_4_q) {
            peq4Q[CHANNEL_CH_1] = pkt->ch_1_eq_4_q;
            if (peq4Q[CHANNEL_CH_1] == PEQ_4_HIGH_SHELF) {
                peq4Filter[CHANNEL_CH_1] = EQ_HIGH_SHELF;
            } else if (peq4Q[CHANNEL_CH_1] == PEQ_4_LPF) {
                peq4Filter[CHANNEL_CH_1] = EQ_HIGH_CUT;
            } else {
                peq4Filter[CHANNEL_CH_1] = EQ_PEAK;
                lastPeq4Q[CHANNEL_CH_1] = peq4Q[CHANNEL_CH_1];
            }
            valueChanged = YES;
        }
        int peqEnableValue = pkt->ch_1_ctrl;
        if (peq1Enabled[CHANNEL_CH_1] != ((peqEnableValue & 0x08) >> 3)) {
            peq1Enabled[CHANNEL_CH_1] = (peqEnableValue & 0x08) >> 3;
            valueChanged = YES;
        }
        if (peq2Enabled[CHANNEL_CH_1] != ((peqEnableValue & 0x10) >> 4)) {
            peq2Enabled[CHANNEL_CH_1] = (peqEnableValue & 0x10) >> 4;
            valueChanged = YES;
        }
        if (peq3Enabled[CHANNEL_CH_1] != ((peqEnableValue & 0x20) >> 5)) {
            peq3Enabled[CHANNEL_CH_1] = (peqEnableValue & 0x20) >> 5;
            valueChanged = YES;
        }
        if (peq4Enabled[CHANNEL_CH_1] != ((peqEnableValue & 0x40) >> 6)) {
            peq4Enabled[CHANNEL_CH_1] = (peqEnableValue & 0x40) >> 6;
            valueChanged = YES;
        }
        if (peqEnabled[CHANNEL_CH_1] != ((peqEnableValue & 0x80) >> 7)) {
            peqEnabled[CHANNEL_CH_1] = (peqEnableValue & 0x80) >> 7;
            valueChanged = YES;
        }
    }
    
    if (valueChanged) {
        [self updatePeqPlot:peqEnabled[CHANNEL_CH_1]];
        [self updatePeqForChannel:CHANNEL_CH_1];
        valueChanged = NO;
    }
    
    // CHANNEL_CH_2
    {
        if (peq1G[CHANNEL_CH_2] != pkt->ch_2_eq_1_db+EQ_GAIN_OFFSET) {
            peq1G[CHANNEL_CH_2] = pkt->ch_2_eq_1_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq1F[CHANNEL_CH_2] != pkt->ch_2_eq_1_freq) {
            peq1F[CHANNEL_CH_2] = pkt->ch_2_eq_1_freq;
            valueChanged = YES;
        }
        if (peq1Q[CHANNEL_CH_2] != pkt->ch_2_eq_1_q) {
            peq1Q[CHANNEL_CH_2] = pkt->ch_2_eq_1_q;
            if (peq1Q[CHANNEL_CH_2] == PEQ_1_LOW_SHELF) {
                peq1Filter[CHANNEL_CH_2] = EQ_LOW_SHELF;
            } else if (peq1Q[CHANNEL_CH_2] == PEQ_1_HPF) {
                peq1Filter[CHANNEL_CH_2] = EQ_LOW_CUT;
            } else {
                peq1Filter[CHANNEL_CH_2] = EQ_PEAK;
                lastPeq1Q[CHANNEL_CH_2] = peq1Q[CHANNEL_CH_2];
            }
            valueChanged = YES;
        }
        if (peq2G[CHANNEL_CH_2] != pkt->ch_2_eq_2_db+EQ_GAIN_OFFSET) {
            peq2G[CHANNEL_CH_2] = pkt->ch_2_eq_2_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq2F[CHANNEL_CH_2] != pkt->ch_2_eq_2_freq) {
            peq2F[CHANNEL_CH_2] = pkt->ch_2_eq_2_freq;
            valueChanged = YES;
        }
        if (peq2Q[CHANNEL_CH_2] != pkt->ch_2_eq_2_q) {
            peq2Q[CHANNEL_CH_2] = pkt->ch_2_eq_2_q;
            valueChanged = YES;
        }
        if (peq3G[CHANNEL_CH_2] != pkt->ch_2_eq_3_db+EQ_GAIN_OFFSET) {
            peq3G[CHANNEL_CH_2] = pkt->ch_2_eq_3_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq3F[CHANNEL_CH_2] != pkt->ch_2_eq_3_freq) {
            peq3F[CHANNEL_CH_2] = pkt->ch_2_eq_3_freq;
            valueChanged = YES;
        }
        if (peq3Q[CHANNEL_CH_2] != pkt->ch_2_eq_3_q) {
            peq3Q[CHANNEL_CH_2] = pkt->ch_2_eq_3_q;
            valueChanged = YES;
        }
        if (peq4G[CHANNEL_CH_2] != pkt->ch_2_eq_4_db+EQ_GAIN_OFFSET) {
            peq4G[CHANNEL_CH_2] = pkt->ch_2_eq_4_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq4F[CHANNEL_CH_2] != pkt->ch_2_eq_4_freq) {
            peq4F[CHANNEL_CH_2] = pkt->ch_2_eq_4_freq;
            valueChanged = YES;
        }
        if (peq4Q[CHANNEL_CH_2] != pkt->ch_2_eq_4_q) {
            peq4Q[CHANNEL_CH_2] = pkt->ch_2_eq_4_q;
            if (peq4Q[CHANNEL_CH_2] == PEQ_4_HIGH_SHELF) {
                peq4Filter[CHANNEL_CH_2] = EQ_HIGH_SHELF;
            } else if (peq4Q[CHANNEL_CH_2] == PEQ_4_LPF) {
                peq4Filter[CHANNEL_CH_2] = EQ_HIGH_CUT;
            } else {
                peq4Filter[CHANNEL_CH_2] = EQ_PEAK;
                lastPeq4Q[CHANNEL_CH_2] = peq4Q[CHANNEL_CH_2];
            }
            valueChanged = YES;
        }
        int peqEnableValue = pkt->ch_2_ctrl;
        if (peq1Enabled[CHANNEL_CH_2] != ((peqEnableValue & 0x08) >> 3)) {
            peq1Enabled[CHANNEL_CH_2] = (peqEnableValue & 0x08) >> 3;
            valueChanged = YES;
        }
        if (peq2Enabled[CHANNEL_CH_2] != ((peqEnableValue & 0x10) >> 4)) {
            peq2Enabled[CHANNEL_CH_2] = (peqEnableValue & 0x10) >> 4;
            valueChanged = YES;
        }
        if (peq3Enabled[CHANNEL_CH_2] != ((peqEnableValue & 0x20) >> 5)) {
            peq3Enabled[CHANNEL_CH_2] = (peqEnableValue & 0x20) >> 5;
            valueChanged = YES;
        }
        if (peq4Enabled[CHANNEL_CH_2] != ((peqEnableValue & 0x40) >> 6)) {
            peq4Enabled[CHANNEL_CH_2] = (peqEnableValue & 0x40) >> 6;
            valueChanged = YES;
        }
        if (peqEnabled[CHANNEL_CH_2] != ((peqEnableValue & 0x80) >> 7)) {
            peqEnabled[CHANNEL_CH_2] = (peqEnableValue & 0x80) >> 7;
            valueChanged = YES;
        }
    }
    
    if (valueChanged) {
        [self updatePeqPlot:peqEnabled[CHANNEL_CH_2]];
        [self updatePeqForChannel:CHANNEL_CH_2];
        valueChanged = NO;
    }
    
    // CHANNEL_CH_3
    {
        if (peq1G[CHANNEL_CH_3] != pkt->ch_3_eq_1_db+EQ_GAIN_OFFSET) {
            peq1G[CHANNEL_CH_3] = pkt->ch_3_eq_1_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq1F[CHANNEL_CH_3] != pkt->ch_3_eq_1_freq) {
            peq1F[CHANNEL_CH_3] = pkt->ch_3_eq_1_freq;
            valueChanged = YES;
        }
        if (peq1Q[CHANNEL_CH_3] != pkt->ch_3_eq_1_q) {
            peq1Q[CHANNEL_CH_3] = pkt->ch_3_eq_1_q;
            if (peq1Q[CHANNEL_CH_3] == PEQ_1_LOW_SHELF) {
                peq1Filter[CHANNEL_CH_3] = EQ_LOW_SHELF;
            } else if (peq1Q[CHANNEL_CH_3] == PEQ_1_HPF) {
                peq1Filter[CHANNEL_CH_3] = EQ_LOW_CUT;
            } else {
                peq1Filter[CHANNEL_CH_3] = EQ_PEAK;
                lastPeq1Q[CHANNEL_CH_3] = peq1Q[CHANNEL_CH_3];
            }
            valueChanged = YES;
        }
        if (peq2G[CHANNEL_CH_3] != pkt->ch_3_eq_2_db+EQ_GAIN_OFFSET) {
            peq2G[CHANNEL_CH_3] = pkt->ch_3_eq_2_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq2F[CHANNEL_CH_3] != pkt->ch_3_eq_2_freq) {
            peq2F[CHANNEL_CH_3] = pkt->ch_3_eq_2_freq;
            valueChanged = YES;
        }
        if (peq2Q[CHANNEL_CH_3] != pkt->ch_3_eq_2_q) {
            peq2Q[CHANNEL_CH_3] = pkt->ch_3_eq_2_q;
            valueChanged = YES;
        }
        if (peq3G[CHANNEL_CH_3] != pkt->ch_3_eq_3_db+EQ_GAIN_OFFSET) {
            peq3G[CHANNEL_CH_3] = pkt->ch_3_eq_3_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq3F[CHANNEL_CH_3] != pkt->ch_3_eq_3_freq) {
            peq3F[CHANNEL_CH_3] = pkt->ch_3_eq_3_freq;
            valueChanged = YES;
        }
        if (peq3Q[CHANNEL_CH_3] != pkt->ch_3_eq_3_q) {
            peq3Q[CHANNEL_CH_3] = pkt->ch_3_eq_3_q;
            valueChanged = YES;
        }
        if (peq4G[CHANNEL_CH_3] != pkt->ch_3_eq_4_db+EQ_GAIN_OFFSET) {
            peq4G[CHANNEL_CH_3] = pkt->ch_3_eq_4_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq4F[CHANNEL_CH_3] != pkt->ch_3_eq_4_freq) {
            peq4F[CHANNEL_CH_3] = pkt->ch_3_eq_4_freq;
            valueChanged = YES;
        }
        if (peq4Q[CHANNEL_CH_3] != pkt->ch_3_eq_4_q) {
            peq4Q[CHANNEL_CH_3] = pkt->ch_3_eq_4_q;
            if (peq4Q[CHANNEL_CH_3] == PEQ_4_HIGH_SHELF) {
                peq4Filter[CHANNEL_CH_3] = EQ_HIGH_SHELF;
            } else if (peq4Q[CHANNEL_CH_3] == PEQ_4_LPF) {
                peq4Filter[CHANNEL_CH_3] = EQ_HIGH_CUT;
            } else {
                peq4Filter[CHANNEL_CH_3] = EQ_PEAK;
                lastPeq4Q[CHANNEL_CH_3] = peq4Q[CHANNEL_CH_3];
            }
            valueChanged = YES;
        }
        int peqEnableValue = pkt->ch_3_ctrl;
        if (peq1Enabled[CHANNEL_CH_3] != ((peqEnableValue & 0x08) >> 3)) {
            peq1Enabled[CHANNEL_CH_3] = (peqEnableValue & 0x08) >> 3;
            valueChanged = YES;
        }
        if (peq2Enabled[CHANNEL_CH_3] != ((peqEnableValue & 0x10) >> 4)) {
            peq2Enabled[CHANNEL_CH_3] = (peqEnableValue & 0x10) >> 4;
            valueChanged = YES;
        }
        if (peq3Enabled[CHANNEL_CH_3] != ((peqEnableValue & 0x20) >> 5)) {
            peq3Enabled[CHANNEL_CH_3] = (peqEnableValue & 0x20) >> 5;
            valueChanged = YES;
        }
        if (peq4Enabled[CHANNEL_CH_3] != ((peqEnableValue & 0x40) >> 6)) {
            peq4Enabled[CHANNEL_CH_3] = (peqEnableValue & 0x40) >> 6;
            valueChanged = YES;
        }
        if (peqEnabled[CHANNEL_CH_3] != ((peqEnableValue & 0x80) >> 7)) {
            peqEnabled[CHANNEL_CH_3] = (peqEnableValue & 0x80) >> 7;
            valueChanged = YES;
        }
    }
    
    if (valueChanged) {
        [self updatePeqPlot:peqEnabled[CHANNEL_CH_3]];
        [self updatePeqForChannel:CHANNEL_CH_3];
        valueChanged = NO;
    }
    
    
    // CHANNEL_CH_4
    {
        if (peq1G[CHANNEL_CH_4] != pkt->ch_4_eq_1_db+EQ_GAIN_OFFSET) {
            peq1G[CHANNEL_CH_4] = pkt->ch_4_eq_1_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq1F[CHANNEL_CH_4] != pkt->ch_4_eq_1_freq) {
            peq1F[CHANNEL_CH_4] = pkt->ch_4_eq_1_freq;
            valueChanged = YES;
        }
        if (peq1Q[CHANNEL_CH_4] != pkt->ch_4_eq_1_q) {
            peq1Q[CHANNEL_CH_4] = pkt->ch_4_eq_1_q;
            if (peq1Q[CHANNEL_CH_4] == PEQ_1_LOW_SHELF) {
                peq1Filter[CHANNEL_CH_4] = EQ_LOW_SHELF;
            } else if (peq1Q[CHANNEL_CH_4] == PEQ_1_HPF) {
                peq1Filter[CHANNEL_CH_4] = EQ_LOW_CUT;
            } else {
                peq1Filter[CHANNEL_CH_4] = EQ_PEAK;
                lastPeq1Q[CHANNEL_CH_4] = peq1Q[CHANNEL_CH_4];
            }
            valueChanged = YES;
        }
        if (peq2G[CHANNEL_CH_4] != pkt->ch_4_eq_2_db+EQ_GAIN_OFFSET) {
            peq2G[CHANNEL_CH_4] = pkt->ch_4_eq_2_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq2F[CHANNEL_CH_4] != pkt->ch_4_eq_2_freq) {
            peq2F[CHANNEL_CH_4] = pkt->ch_4_eq_2_freq;
            valueChanged = YES;
        }
        if (peq2Q[CHANNEL_CH_4] != pkt->ch_4_eq_2_q) {
            peq2Q[CHANNEL_CH_4] = pkt->ch_4_eq_2_q;
            valueChanged = YES;
        }
        if (peq3G[CHANNEL_CH_4] != pkt->ch_4_eq_3_db+EQ_GAIN_OFFSET) {
            peq3G[CHANNEL_CH_4] = pkt->ch_4_eq_3_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq3F[CHANNEL_CH_4] != pkt->ch_4_eq_3_freq) {
            peq3F[CHANNEL_CH_4] = pkt->ch_4_eq_3_freq;
            valueChanged = YES;
        }
        if (peq3Q[CHANNEL_CH_4] != pkt->ch_4_eq_3_q) {
            peq3Q[CHANNEL_CH_4] = pkt->ch_4_eq_3_q;
            valueChanged = YES;
        }
        if (peq4G[CHANNEL_CH_4] != pkt->ch_4_eq_4_db+EQ_GAIN_OFFSET) {
            peq4G[CHANNEL_CH_4] = pkt->ch_4_eq_4_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq4F[CHANNEL_CH_4] != pkt->ch_4_eq_4_freq) {
            peq4F[CHANNEL_CH_4] = pkt->ch_4_eq_4_freq;
            valueChanged = YES;
        }
        if (peq4Q[CHANNEL_CH_4] != pkt->ch_4_eq_4_q) {
            peq4Q[CHANNEL_CH_4] = pkt->ch_4_eq_4_q;
            if (peq4Q[CHANNEL_CH_4] == PEQ_4_HIGH_SHELF) {
                peq4Filter[CHANNEL_CH_4] = EQ_HIGH_SHELF;
            } else if (peq4Q[CHANNEL_CH_4] == PEQ_4_LPF) {
                peq4Filter[CHANNEL_CH_4] = EQ_HIGH_CUT;
            } else {
                peq4Filter[CHANNEL_CH_4] = EQ_PEAK;
                lastPeq4Q[CHANNEL_CH_4] = peq4Q[CHANNEL_CH_4];
            }
            valueChanged = YES;
        }
        int peqEnableValue = pkt->ch_4_ctrl;
        if (peq1Enabled[CHANNEL_CH_4] != ((peqEnableValue & 0x08) >> 3)) {
            peq1Enabled[CHANNEL_CH_4] = (peqEnableValue & 0x08) >> 3;
            valueChanged = YES;
        }
        if (peq2Enabled[CHANNEL_CH_4] != ((peqEnableValue & 0x10) >> 4)) {
            peq2Enabled[CHANNEL_CH_4] = (peqEnableValue & 0x10) >> 4;
            valueChanged = YES;
        }
        if (peq3Enabled[CHANNEL_CH_4] != ((peqEnableValue & 0x20) >> 5)) {
            peq3Enabled[CHANNEL_CH_4] = (peqEnableValue & 0x20) >> 5;
            valueChanged = YES;
        }
        if (peq4Enabled[CHANNEL_CH_4] != ((peqEnableValue & 0x40) >> 6)) {
            peq4Enabled[CHANNEL_CH_4] = (peqEnableValue & 0x40) >> 6;
            valueChanged = YES;
        }
        if (peqEnabled[CHANNEL_CH_4] != ((peqEnableValue & 0x80) >> 7)) {
            peqEnabled[CHANNEL_CH_4] = (peqEnableValue & 0x80) >> 7;
            valueChanged = YES;
        }
    }
    
    if (valueChanged) {
        [self updatePeqPlot:peqEnabled[CHANNEL_CH_4]];
        [self updatePeqForChannel:CHANNEL_CH_4];
        valueChanged = NO;
    }
    
    
    // CHANNEL_CH_5
    {
        if (peq1G[CHANNEL_CH_5] != pkt->ch_5_eq_1_db+EQ_GAIN_OFFSET) {
            peq1G[CHANNEL_CH_5] = pkt->ch_5_eq_1_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq1F[CHANNEL_CH_5] != pkt->ch_5_eq_1_freq) {
            peq1F[CHANNEL_CH_5] = pkt->ch_5_eq_1_freq;
            valueChanged = YES;
        }
        if (peq1Q[CHANNEL_CH_5] != pkt->ch_5_eq_1_q) {
            peq1Q[CHANNEL_CH_5] = pkt->ch_5_eq_1_q;
            if (peq1Q[CHANNEL_CH_5] == PEQ_1_LOW_SHELF) {
                peq1Filter[CHANNEL_CH_5] = EQ_LOW_SHELF;
            } else if (peq1Q[CHANNEL_CH_5] == PEQ_1_HPF) {
                peq1Filter[CHANNEL_CH_5] = EQ_LOW_CUT;
            } else {
                peq1Filter[CHANNEL_CH_5] = EQ_PEAK;
                lastPeq1Q[CHANNEL_CH_5] = peq1Q[CHANNEL_CH_5];
            }
            valueChanged = YES;
        }
        if (peq2G[CHANNEL_CH_5] != pkt->ch_5_eq_2_db+EQ_GAIN_OFFSET) {
            peq2G[CHANNEL_CH_5] = pkt->ch_5_eq_2_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq2F[CHANNEL_CH_5] != pkt->ch_5_eq_2_freq) {
            peq2F[CHANNEL_CH_5] = pkt->ch_5_eq_2_freq;
            valueChanged = YES;
        }
        if (peq2Q[CHANNEL_CH_5] != pkt->ch_5_eq_2_q) {
            peq2Q[CHANNEL_CH_5] = pkt->ch_5_eq_2_q;
            valueChanged = YES;
        }
        if (peq3G[CHANNEL_CH_5] != pkt->ch_5_eq_3_db+EQ_GAIN_OFFSET) {
            peq3G[CHANNEL_CH_5] = pkt->ch_5_eq_3_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq3F[CHANNEL_CH_5] != pkt->ch_5_eq_3_freq) {
            peq3F[CHANNEL_CH_5] = pkt->ch_5_eq_3_freq;
            valueChanged = YES;
        }
        if (peq3Q[CHANNEL_CH_5] != pkt->ch_5_eq_3_q) {
            peq3Q[CHANNEL_CH_5] = pkt->ch_5_eq_3_q;
            valueChanged = YES;
        }
        if (peq4G[CHANNEL_CH_5] != pkt->ch_5_eq_4_db+EQ_GAIN_OFFSET) {
            peq4G[CHANNEL_CH_5] = pkt->ch_5_eq_4_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq4F[CHANNEL_CH_5] != pkt->ch_5_eq_4_freq) {
            peq4F[CHANNEL_CH_5] = pkt->ch_5_eq_4_freq;
            valueChanged = YES;
        }
        if (peq4Q[CHANNEL_CH_5] != pkt->ch_5_eq_4_q) {
            peq4Q[CHANNEL_CH_5] = pkt->ch_5_eq_4_q;
            if (peq4Q[CHANNEL_CH_5] == PEQ_4_HIGH_SHELF) {
                peq4Filter[CHANNEL_CH_5] = EQ_HIGH_SHELF;
            } else if (peq4Q[CHANNEL_CH_5] == PEQ_4_LPF) {
                peq4Filter[CHANNEL_CH_5] = EQ_HIGH_CUT;
            } else {
                peq4Filter[CHANNEL_CH_5] = EQ_PEAK;
                lastPeq4Q[CHANNEL_CH_5] = peq4Q[CHANNEL_CH_5];
            }
            valueChanged = YES;
        }
        int peqEnableValue = pkt->ch_5_ctrl;
        if (peq1Enabled[CHANNEL_CH_5] != ((peqEnableValue & 0x08) >> 3)) {
            peq1Enabled[CHANNEL_CH_5] = (peqEnableValue & 0x08) >> 3;
            valueChanged = YES;
        }
        if (peq2Enabled[CHANNEL_CH_5] != ((peqEnableValue & 0x10) >> 4)) {
            peq2Enabled[CHANNEL_CH_5] = (peqEnableValue & 0x10) >> 4;
            valueChanged = YES;
        }
        if (peq3Enabled[CHANNEL_CH_5] != ((peqEnableValue & 0x20) >> 5)) {
            peq3Enabled[CHANNEL_CH_5] = (peqEnableValue & 0x20) >> 5;
            valueChanged = YES;
        }
        if (peq4Enabled[CHANNEL_CH_5] != ((peqEnableValue & 0x40) >> 6)) {
            peq4Enabled[CHANNEL_CH_5] = (peqEnableValue & 0x40) >> 6;
            valueChanged = YES;
        }
        if (peqEnabled[CHANNEL_CH_5] != ((peqEnableValue & 0x80) >> 7)) {
            peqEnabled[CHANNEL_CH_5] = (peqEnableValue & 0x80) >> 7;
            valueChanged = YES;
        }
    }
    
    if (valueChanged) {
        [self updatePeqPlot:peqEnabled[CHANNEL_CH_5]];
        [self updatePeqForChannel:CHANNEL_CH_5];
        valueChanged = NO;
    }
    
    
    // CHANNEL_CH_6
    {
        if (peq1G[CHANNEL_CH_6] != pkt->ch_6_eq_1_db+EQ_GAIN_OFFSET) {
            peq1G[CHANNEL_CH_6] = pkt->ch_6_eq_1_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq1F[CHANNEL_CH_6] != pkt->ch_6_eq_1_freq) {
            peq1F[CHANNEL_CH_6] = pkt->ch_6_eq_1_freq;
            valueChanged = YES;
        }
        if (peq1Q[CHANNEL_CH_6] != pkt->ch_6_eq_1_q) {
            peq1Q[CHANNEL_CH_6] = pkt->ch_6_eq_1_q;
            if (peq1Q[CHANNEL_CH_6] == PEQ_1_LOW_SHELF) {
                peq1Filter[CHANNEL_CH_6] = EQ_LOW_SHELF;
            } else if (peq1Q[CHANNEL_CH_6] == PEQ_1_HPF) {
                peq1Filter[CHANNEL_CH_6] = EQ_LOW_CUT;
            } else {
                peq1Filter[CHANNEL_CH_6] = EQ_PEAK;
                lastPeq1Q[CHANNEL_CH_6] = peq1Q[CHANNEL_CH_6];
            }
            valueChanged = YES;
        }
        if (peq2G[CHANNEL_CH_6] != pkt->ch_6_eq_2_db+EQ_GAIN_OFFSET) {
            peq2G[CHANNEL_CH_6] = pkt->ch_6_eq_2_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq2F[CHANNEL_CH_6] != pkt->ch_6_eq_2_freq) {
            peq2F[CHANNEL_CH_6] = pkt->ch_6_eq_2_freq;
            valueChanged = YES;
        }
        if (peq2Q[CHANNEL_CH_6] != pkt->ch_6_eq_2_q) {
            peq2Q[CHANNEL_CH_6] = pkt->ch_6_eq_2_q;
            valueChanged = YES;
        }
        if (peq3G[CHANNEL_CH_6] != pkt->ch_6_eq_3_db+EQ_GAIN_OFFSET) {
            peq3G[CHANNEL_CH_6] = pkt->ch_6_eq_3_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq3F[CHANNEL_CH_6] != pkt->ch_6_eq_3_freq) {
            peq3F[CHANNEL_CH_6] = pkt->ch_6_eq_3_freq;
            valueChanged = YES;
        }
        if (peq3Q[CHANNEL_CH_6] != pkt->ch_6_eq_3_q) {
            peq3Q[CHANNEL_CH_6] = pkt->ch_6_eq_3_q;
            valueChanged = YES;
        }
        if (peq4G[CHANNEL_CH_6] != pkt->ch_6_eq_4_db+EQ_GAIN_OFFSET) {
            peq4G[CHANNEL_CH_6] = pkt->ch_6_eq_4_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq4F[CHANNEL_CH_6] != pkt->ch_6_eq_4_freq) {
            peq4F[CHANNEL_CH_6] = pkt->ch_6_eq_4_freq;
            valueChanged = YES;
        }
        if (peq4Q[CHANNEL_CH_6] != pkt->ch_6_eq_4_q) {
            peq4Q[CHANNEL_CH_6] = pkt->ch_6_eq_4_q;
            if (peq4Q[CHANNEL_CH_6] == PEQ_4_HIGH_SHELF) {
                peq4Filter[CHANNEL_CH_6] = EQ_HIGH_SHELF;
            } else if (peq4Q[CHANNEL_CH_6] == PEQ_4_LPF) {
                peq4Filter[CHANNEL_CH_6] = EQ_HIGH_CUT;
            } else {
                peq4Filter[CHANNEL_CH_6] = EQ_PEAK;
                lastPeq4Q[CHANNEL_CH_6] = peq4Q[CHANNEL_CH_6];
            }
            valueChanged = YES;
        }
        int peqEnableValue = pkt->ch_6_ctrl;
        if (peq1Enabled[CHANNEL_CH_6] != ((peqEnableValue & 0x08) >> 3)) {
            peq1Enabled[CHANNEL_CH_6] = (peqEnableValue & 0x08) >> 3;
            valueChanged = YES;
        }
        if (peq2Enabled[CHANNEL_CH_6] != ((peqEnableValue & 0x10) >> 4)) {
            peq2Enabled[CHANNEL_CH_6] = (peqEnableValue & 0x10) >> 4;
            valueChanged = YES;
        }
        if (peq3Enabled[CHANNEL_CH_6] != ((peqEnableValue & 0x20) >> 5)) {
            peq3Enabled[CHANNEL_CH_6] = (peqEnableValue & 0x20) >> 5;
            valueChanged = YES;
        }
        if (peq4Enabled[CHANNEL_CH_6] != ((peqEnableValue & 0x40) >> 6)) {
            peq4Enabled[CHANNEL_CH_6] = (peqEnableValue & 0x40) >> 6;
            valueChanged = YES;
        }
        if (peqEnabled[CHANNEL_CH_6] != ((peqEnableValue & 0x80) >> 7)) {
            peqEnabled[CHANNEL_CH_6] = (peqEnableValue & 0x80) >> 7;
            valueChanged = YES;
        }
    }
    
    if (valueChanged) {
        [self updatePeqPlot:peqEnabled[CHANNEL_CH_6]];
        [self updatePeqForChannel:CHANNEL_CH_6];
        valueChanged = NO;
    }
    
    
    // CHANNEL_CH_7
    {
        if (peq1G[CHANNEL_CH_7] != pkt->ch_7_eq_1_db+EQ_GAIN_OFFSET) {
            peq1G[CHANNEL_CH_7] = pkt->ch_7_eq_1_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq1F[CHANNEL_CH_7] != pkt->ch_7_eq_1_freq) {
            peq1F[CHANNEL_CH_7] = pkt->ch_7_eq_1_freq;
            valueChanged = YES;
        }
        if (peq1Q[CHANNEL_CH_7] != pkt->ch_7_eq_1_q) {
            peq1Q[CHANNEL_CH_7] = pkt->ch_7_eq_1_q;
            if (peq1Q[CHANNEL_CH_7] == PEQ_1_LOW_SHELF) {
                peq1Filter[CHANNEL_CH_7] = EQ_LOW_SHELF;
            } else if (peq1Q[CHANNEL_CH_7] == PEQ_1_HPF) {
                peq1Filter[CHANNEL_CH_7] = EQ_LOW_CUT;
            } else {
                peq1Filter[CHANNEL_CH_7] = EQ_PEAK;
                lastPeq1Q[CHANNEL_CH_7] = peq1Q[CHANNEL_CH_7];
            }
            valueChanged = YES;
        }
        if (peq2G[CHANNEL_CH_7] != pkt->ch_7_eq_2_db+EQ_GAIN_OFFSET) {
            peq2G[CHANNEL_CH_7] = pkt->ch_7_eq_2_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq2F[CHANNEL_CH_7] != pkt->ch_7_eq_2_freq) {
            peq2F[CHANNEL_CH_7] = pkt->ch_7_eq_2_freq;
            valueChanged = YES;
        }
        if (peq2Q[CHANNEL_CH_7] != pkt->ch_7_eq_2_q) {
            peq2Q[CHANNEL_CH_7] = pkt->ch_7_eq_2_q;
            valueChanged = YES;
        }
        if (peq3G[CHANNEL_CH_7] != pkt->ch_7_eq_3_db+EQ_GAIN_OFFSET) {
            peq3G[CHANNEL_CH_7] = pkt->ch_7_eq_3_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq3F[CHANNEL_CH_7] != pkt->ch_7_eq_3_freq) {
            peq3F[CHANNEL_CH_7] = pkt->ch_7_eq_3_freq;
            valueChanged = YES;
        }
        if (peq3Q[CHANNEL_CH_7] != pkt->ch_7_eq_3_q) {
            peq3Q[CHANNEL_CH_7] = pkt->ch_7_eq_3_q;
            valueChanged = YES;
        }
        if (peq4G[CHANNEL_CH_7] != pkt->ch_7_eq_4_db+EQ_GAIN_OFFSET) {
            peq4G[CHANNEL_CH_7] = pkt->ch_7_eq_4_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq4F[CHANNEL_CH_7] != pkt->ch_7_eq_4_freq) {
            peq4F[CHANNEL_CH_7] = pkt->ch_7_eq_4_freq;
            valueChanged = YES;
        }
        if (peq4Q[CHANNEL_CH_7] != pkt->ch_7_eq_4_q) {
            peq4Q[CHANNEL_CH_7] = pkt->ch_7_eq_4_q;
            if (peq4Q[CHANNEL_CH_7] == PEQ_4_HIGH_SHELF) {
                peq4Filter[CHANNEL_CH_7] = EQ_HIGH_SHELF;
            } else if (peq4Q[CHANNEL_CH_7] == PEQ_4_LPF) {
                peq4Filter[CHANNEL_CH_7] = EQ_HIGH_CUT;
            } else {
                peq4Filter[CHANNEL_CH_7] = EQ_PEAK;
                lastPeq4Q[CHANNEL_CH_7] = peq4Q[CHANNEL_CH_7];
            }
            valueChanged = YES;
        }
        int peqEnableValue = pkt->ch_7_ctrl;
        if (peq1Enabled[CHANNEL_CH_7] != ((peqEnableValue & 0x08) >> 3)) {
            peq1Enabled[CHANNEL_CH_7] = (peqEnableValue & 0x08) >> 3;
            valueChanged = YES;
        }
        if (peq2Enabled[CHANNEL_CH_7] != ((peqEnableValue & 0x10) >> 4)) {
            peq2Enabled[CHANNEL_CH_7] = (peqEnableValue & 0x10) >> 4;
            valueChanged = YES;
        }
        if (peq3Enabled[CHANNEL_CH_7] != ((peqEnableValue & 0x20) >> 5)) {
            peq3Enabled[CHANNEL_CH_7] = (peqEnableValue & 0x20) >> 5;
            valueChanged = YES;
        }
        if (peq4Enabled[CHANNEL_CH_7] != ((peqEnableValue & 0x40) >> 6)) {
            peq4Enabled[CHANNEL_CH_7] = (peqEnableValue & 0x40) >> 6;
            valueChanged = YES;
        }
        if (peqEnabled[CHANNEL_CH_7] != ((peqEnableValue & 0x80) >> 7)) {
            peqEnabled[CHANNEL_CH_7] = (peqEnableValue & 0x80) >> 7;
            valueChanged = YES;
        }
    }
    
    if (valueChanged) {
        [self updatePeqPlot:peqEnabled[CHANNEL_CH_7]];
        [self updatePeqForChannel:CHANNEL_CH_7];
        valueChanged = NO;
    }
    
    
    // CHANNEL_CH_8
    {
        if (peq1G[CHANNEL_CH_8] != pkt->ch_8_eq_1_db+EQ_GAIN_OFFSET) {
            peq1G[CHANNEL_CH_8] = pkt->ch_8_eq_1_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq1F[CHANNEL_CH_8] != pkt->ch_8_eq_1_freq) {
            peq1F[CHANNEL_CH_8] = pkt->ch_8_eq_1_freq;
            valueChanged = YES;
        }
        if (peq1Q[CHANNEL_CH_8] != pkt->ch_8_eq_1_q) {
            peq1Q[CHANNEL_CH_8] = pkt->ch_8_eq_1_q;
            if (peq1Q[CHANNEL_CH_8] == PEQ_1_LOW_SHELF) {
                peq1Filter[CHANNEL_CH_8] = EQ_LOW_SHELF;
            } else if (peq1Q[CHANNEL_CH_8] == PEQ_1_HPF) {
                peq1Filter[CHANNEL_CH_8] = EQ_LOW_CUT;
            } else {
                peq1Filter[CHANNEL_CH_8] = EQ_PEAK;
                lastPeq1Q[CHANNEL_CH_8] = peq1Q[CHANNEL_CH_8];
            }
            valueChanged = YES;
        }
        if (peq2G[CHANNEL_CH_8] != pkt->ch_8_eq_2_db+EQ_GAIN_OFFSET) {
            peq2G[CHANNEL_CH_8] = pkt->ch_8_eq_2_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq2F[CHANNEL_CH_8] != pkt->ch_8_eq_2_freq) {
            peq2F[CHANNEL_CH_8] = pkt->ch_8_eq_2_freq;
            valueChanged = YES;
        }
        if (peq2Q[CHANNEL_CH_8] != pkt->ch_8_eq_2_q) {
            peq2Q[CHANNEL_CH_8] = pkt->ch_8_eq_2_q;
            valueChanged = YES;
        }
        if (peq3G[CHANNEL_CH_8] != pkt->ch_8_eq_3_db+EQ_GAIN_OFFSET) {
            peq3G[CHANNEL_CH_8] = pkt->ch_8_eq_3_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq3F[CHANNEL_CH_8] != pkt->ch_8_eq_3_freq) {
            peq3F[CHANNEL_CH_8] = pkt->ch_8_eq_3_freq;
            valueChanged = YES;
        }
        if (peq3Q[CHANNEL_CH_8] != pkt->ch_8_eq_3_q) {
            peq3Q[CHANNEL_CH_8] = pkt->ch_8_eq_3_q;
            valueChanged = YES;
        }
        if (peq4G[CHANNEL_CH_8] != pkt->ch_8_eq_4_db+EQ_GAIN_OFFSET) {
            peq4G[CHANNEL_CH_8] = pkt->ch_8_eq_4_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq4F[CHANNEL_CH_8] != pkt->ch_8_eq_4_freq) {
            peq4F[CHANNEL_CH_8] = pkt->ch_8_eq_4_freq;
            valueChanged = YES;
        }
        if (peq4Q[CHANNEL_CH_8] != pkt->ch_8_eq_4_q) {
            peq4Q[CHANNEL_CH_8] = pkt->ch_8_eq_4_q;
            if (peq4Q[CHANNEL_CH_8] == PEQ_4_HIGH_SHELF) {
                peq4Filter[CHANNEL_CH_8] = EQ_HIGH_SHELF;
            } else if (peq4Q[CHANNEL_CH_8] == PEQ_4_LPF) {
                peq4Filter[CHANNEL_CH_8] = EQ_HIGH_CUT;
            } else {
                peq4Filter[CHANNEL_CH_8] = EQ_PEAK;
                lastPeq4Q[CHANNEL_CH_8] = peq4Q[CHANNEL_CH_8];
            }
            valueChanged = YES;
        }
        int peqEnableValue = pkt->ch_8_ctrl;
        if (peq1Enabled[CHANNEL_CH_8] != ((peqEnableValue & 0x08) >> 3)) {
            peq1Enabled[CHANNEL_CH_8] = (peqEnableValue & 0x08) >> 3;
            valueChanged = YES;
        }
        if (peq2Enabled[CHANNEL_CH_8] != ((peqEnableValue & 0x10) >> 4)) {
            peq2Enabled[CHANNEL_CH_8] = (peqEnableValue & 0x10) >> 4;
            valueChanged = YES;
        }
        if (peq3Enabled[CHANNEL_CH_8] != ((peqEnableValue & 0x20) >> 5)) {
            peq3Enabled[CHANNEL_CH_8] = (peqEnableValue & 0x20) >> 5;
            valueChanged = YES;
        }
        if (peq4Enabled[CHANNEL_CH_8] != ((peqEnableValue & 0x40) >> 6)) {
            peq4Enabled[CHANNEL_CH_8] = (peqEnableValue & 0x40) >> 6;
            valueChanged = YES;
        }
        if (peqEnabled[CHANNEL_CH_8] != ((peqEnableValue & 0x80) >> 7)) {
            peqEnabled[CHANNEL_CH_8] = (peqEnableValue & 0x80) >> 7;
            valueChanged = YES;
        }
    }
    
    if (valueChanged) {
        [self updatePeqPlot:peqEnabled[CHANNEL_CH_8]];
        [self updatePeqForChannel:CHANNEL_CH_8];
        valueChanged = NO;
    }
    
    // CHANNEL_MAIN
    if (mainEqType == MAIN_EQ_GEQ) {
        BOOL needsUpdate = NO;
        
        // Update GEQ sliders
        int geq1Value = pkt->geq_20_hz_db;
        int geq2Value = pkt->geq_25_hz_db;
        int geq3Value = pkt->geq_31_5_hz_db;
        int geq4Value = pkt->geq_40_hz_db;
        int geq5Value = pkt->geq_50_hz_db;
        int geq6Value = pkt->geq_63_hz_db;
        int geq7Value = pkt->geq_80_hz_db;
        int geq8Value = pkt->geq_100_hz_db;
        int geq9Value = pkt->geq_125_hz_db;
        int geq10Value = pkt->geq_160_hz_db;
        int geq11Value = pkt->geq_200_hz_db;
        int geq12Value = pkt->geq_250_hz_db;
        int geq13Value = pkt->geq_315_hz_db;
        int geq14Value = pkt->geq_400_hz_db;
        int geq15Value = pkt->geq_500_hz_db;
        int geq16Value = pkt->geq_630_hz_db;
        int geq17Value = pkt->geq_800_hz_db;
        int geq18Value = pkt->geq_1_khz_db;
        int geq19Value = pkt->geq_1_25_khz_db;
        int geq20Value = pkt->geq_1_6_khz_db;
        int geq21Value = pkt->geq_2_khz_db;
        int geq22Value = pkt->geq_2_5_khz_db;
        int geq23Value = pkt->geq_3_15_khz_db;
        int geq24Value = pkt->geq_4_khz_db;
        int geq25Value = pkt->geq_5_khz_db;
        int geq26Value = pkt->geq_6_3_khz_db;
        int geq27Value = pkt->geq_8_khz_db;
        int geq28Value = pkt->geq_10_khz_db;
        int geq29Value = pkt->geq_12_5_khz_db;
        int geq30Value = pkt->geq_16_khz_db;
        int geq31Value = pkt->geq_20_khz_db;
        
        if (sliderGeq1.value != geq1Value) {
            sliderGeq1.value = geq1Value;
            if (sliderGeq1.value > 0.0) {
                meter1Top.progress = sliderGeq1.value/GEQ_SLIDER_MAX_VALUE;
                meter1Btm.progress = 0.0;
            } else if (sliderGeq2.value < 0.0) {
                meter1Btm.progress = sliderGeq1.value/GEQ_SLIDER_MIN_VALUE;
                meter1Top.progress = 0.0;
            } else {
                meter1Top.progress = 0.0;
                meter1Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq2.value != geq2Value) {
            sliderGeq2.value = geq2Value;
            if (sliderGeq2.value > 0.0) {
                meter2Top.progress = sliderGeq2.value/GEQ_SLIDER_MAX_VALUE;
                meter2Btm.progress = 0.0;
            } else if (sliderGeq2.value < 0.0) {
                meter2Btm.progress = sliderGeq2.value/GEQ_SLIDER_MIN_VALUE;
                meter2Top.progress = 0.0;
            } else {
                meter2Top.progress = 0.0;
                meter2Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq3.value != geq3Value) {
            sliderGeq3.value = geq3Value;
            if (sliderGeq3.value > 0.0) {
                meter3Top.progress = sliderGeq3.value/GEQ_SLIDER_MAX_VALUE;
                meter3Btm.progress = 0.0;
            } else if (sliderGeq3.value < 0.0) {
                meter3Btm.progress = sliderGeq3.value/GEQ_SLIDER_MIN_VALUE;
                meter3Top.progress = 0.0;
            } else {
                meter3Top.progress = 0.0;
                meter3Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq4.value != geq4Value) {
            sliderGeq4.value = geq4Value;
            if (sliderGeq4.value > 0.0) {
                meter4Top.progress = sliderGeq4.value/GEQ_SLIDER_MAX_VALUE;
                meter4Btm.progress = 0.0;
            } else if (sliderGeq4.value < 0.0) {
                meter4Btm.progress = sliderGeq4.value/GEQ_SLIDER_MIN_VALUE;
                meter4Top.progress = 0.0;
            } else {
                meter4Top.progress = 0.0;
                meter4Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq5.value != geq5Value) {
            sliderGeq5.value = geq5Value;
            if (sliderGeq5.value > 0.0) {
                meter5Top.progress = sliderGeq5.value/GEQ_SLIDER_MAX_VALUE;
                meter5Btm.progress = 0.0;
            } else if (sliderGeq5.value < 0.0) {
                meter5Btm.progress = sliderGeq5.value/GEQ_SLIDER_MIN_VALUE;
                meter5Top.progress = 0.0;
            } else {
                meter5Top.progress = 0.0;
                meter5Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq6.value != geq6Value) {
            sliderGeq6.value = geq6Value;
            if (sliderGeq6.value > 0.0) {
                meter6Top.progress = sliderGeq6.value/GEQ_SLIDER_MAX_VALUE;
                meter6Btm.progress = 0.0;
            } else if (sliderGeq6.value < 0.0) {
                meter6Btm.progress = sliderGeq6.value/GEQ_SLIDER_MIN_VALUE;
                meter6Top.progress = 0.0;
            } else {
                meter6Top.progress = 0.0;
                meter6Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq7.value != geq7Value) {
            sliderGeq7.value = geq7Value;
            if (sliderGeq7.value > 0.0) {
                meter7Top.progress = sliderGeq7.value/GEQ_SLIDER_MAX_VALUE;
                meter7Btm.progress = 0.0;
            } else if (sliderGeq7.value < 0.0) {
                meter7Btm.progress = sliderGeq7.value/GEQ_SLIDER_MIN_VALUE;
                meter7Top.progress = 0.0;
            } else {
                meter7Top.progress = 0.0;
                meter7Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq8.value != geq8Value) {
            sliderGeq8.value = geq8Value;
            if (sliderGeq8.value > 0.0) {
                meter8Top.progress = sliderGeq8.value/GEQ_SLIDER_MAX_VALUE;
                meter8Btm.progress = 0.0;
            } else if (sliderGeq8.value < 0.0) {
                meter8Btm.progress = sliderGeq8.value/GEQ_SLIDER_MIN_VALUE;
                meter8Top.progress = 0.0;
            } else {
                meter8Top.progress = 0.0;
                meter8Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq9.value != geq9Value) {
            sliderGeq9.value = geq9Value;
            if (sliderGeq9.value > 0.0) {
                meter9Top.progress = sliderGeq9.value/GEQ_SLIDER_MAX_VALUE;
                meter9Btm.progress = 0.0;
            } else if (sliderGeq9.value < 0.0) {
                meter9Btm.progress = sliderGeq9.value/GEQ_SLIDER_MIN_VALUE;
                meter9Top.progress = 0.0;
            } else {
                meter9Top.progress = 0.0;
                meter9Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq10.value != geq10Value) {
            sliderGeq10.value = geq10Value;
            if (sliderGeq10.value > 0.0) {
                meter10Top.progress = sliderGeq10.value/GEQ_SLIDER_MAX_VALUE;
                meter10Btm.progress = 0.0;
            } else if (sliderGeq10.value < 0.0) {
                meter10Btm.progress = sliderGeq10.value/GEQ_SLIDER_MIN_VALUE;
                meter10Top.progress = 0.0;
            } else {
                meter10Top.progress = 0.0;
                meter10Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq11.value != geq11Value) {
            sliderGeq11.value = geq11Value;
            if (sliderGeq11.value > 0.0) {
                meter11Top.progress = sliderGeq11.value/GEQ_SLIDER_MAX_VALUE;
                meter11Btm.progress = 0.0;
            } else if (sliderGeq11.value < 0.0) {
                meter11Btm.progress = sliderGeq11.value/GEQ_SLIDER_MIN_VALUE;
                meter1Top.progress = 0.0;
            } else {
                meter11Top.progress = 0.0;
                meter11Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq12.value != geq12Value) {
            sliderGeq12.value = geq12Value;
            if (sliderGeq12.value > 0.0) {
                meter12Top.progress = sliderGeq12.value/GEQ_SLIDER_MAX_VALUE;
                meter12Btm.progress = 0.0;
            } else if (sliderGeq12.value < 0.0) {
                meter12Btm.progress = sliderGeq12.value/GEQ_SLIDER_MIN_VALUE;
                meter12Top.progress = 0.0;
            } else {
                meter12Top.progress = 0.0;
                meter12Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq13.value != geq13Value) {
            sliderGeq13.value = geq13Value;
            if (sliderGeq13.value > 0.0) {
                meter13Top.progress = sliderGeq13.value/GEQ_SLIDER_MAX_VALUE;
                meter13Btm.progress = 0.0;
            } else if (sliderGeq13.value < 0.0) {
                meter13Btm.progress = sliderGeq13.value/GEQ_SLIDER_MIN_VALUE;
                meter13Top.progress = 0.0;
            } else {
                meter13Top.progress = 0.0;
                meter13Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq14.value != geq14Value) {
            sliderGeq14.value = geq14Value;
            if (sliderGeq14.value > 0.0) {
                meter14Top.progress = sliderGeq14.value/GEQ_SLIDER_MAX_VALUE;
                meter14Btm.progress = 0.0;
            } else if (sliderGeq14.value < 0.0) {
                meter14Btm.progress = sliderGeq14.value/GEQ_SLIDER_MIN_VALUE;
                meter14Top.progress = 0.0;
            } else {
                meter14Top.progress = 0.0;
                meter14Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq15.value != geq15Value) {
            sliderGeq15.value = geq15Value;
            if (sliderGeq15.value > 0.0) {
                meter15Top.progress = sliderGeq15.value/GEQ_SLIDER_MAX_VALUE;
                meter15Btm.progress = 0.0;
            } else if (sliderGeq15.value < 0.0) {
                meter15Btm.progress = sliderGeq15.value/GEQ_SLIDER_MIN_VALUE;
                meter15Top.progress = 0.0;
            } else {
                meter15Top.progress = 0.0;
                meter15Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq16.value != geq16Value) {
            sliderGeq16.value = geq16Value;
            if (sliderGeq16.value > 0.0) {
                meter16Top.progress = sliderGeq16.value/GEQ_SLIDER_MAX_VALUE;
                meter16Btm.progress = 0.0;
            } else if (sliderGeq16.value < 0.0) {
                meter16Btm.progress = sliderGeq16.value/GEQ_SLIDER_MIN_VALUE;
                meter16Top.progress = 0.0;
            } else {
                meter16Top.progress = 0.0;
                meter16Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq17.value != geq17Value) {
            sliderGeq17.value = geq17Value;
            if (sliderGeq17.value > 0.0) {
                meter17Top.progress = sliderGeq17.value/GEQ_SLIDER_MAX_VALUE;
                meter17Btm.progress = 0.0;
            } else if (sliderGeq17.value < 0.0) {
                meter17Btm.progress = sliderGeq17.value/GEQ_SLIDER_MIN_VALUE;
                meter17Top.progress = 0.0;
            } else {
                meter17Top.progress = 0.0;
                meter17Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq18.value != geq18Value) {
            sliderGeq18.value = geq18Value;
            if (sliderGeq18.value > 0.0) {
                meter18Top.progress = sliderGeq18.value/GEQ_SLIDER_MAX_VALUE;
                meter18Btm.progress = 0.0;
            } else if (sliderGeq18.value < 0.0) {
                meter18Btm.progress = sliderGeq18.value/GEQ_SLIDER_MIN_VALUE;
                meter18Top.progress = 0.0;
            } else {
                meter18Top.progress = 0.0;
                meter18Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq19.value != geq19Value) {
            sliderGeq19.value = geq19Value;
            if (sliderGeq19.value > 0.0) {
                meter19Top.progress = sliderGeq19.value/GEQ_SLIDER_MAX_VALUE;
                meter19Btm.progress = 0.0;
            } else if (sliderGeq19.value < 0.0) {
                meter19Btm.progress = sliderGeq19.value/GEQ_SLIDER_MIN_VALUE;
                meter19Top.progress = 0.0;
            } else {
                meter19Top.progress = 0.0;
                meter19Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq20.value != geq20Value) {
            sliderGeq20.value = geq20Value;
            if (sliderGeq20.value > 0.0) {
                meter20Top.progress = sliderGeq20.value/GEQ_SLIDER_MAX_VALUE;
                meter20Btm.progress = 0.0;
            } else if (sliderGeq20.value < 0.0) {
                meter20Btm.progress = sliderGeq20.value/GEQ_SLIDER_MIN_VALUE;
                meter20Top.progress = 0.0;
            } else {
                meter20Top.progress = 0.0;
                meter20Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq21.value != geq21Value) {
            sliderGeq21.value = geq21Value;
            if (sliderGeq21.value > 0.0) {
                meter21Top.progress = sliderGeq21.value/GEQ_SLIDER_MAX_VALUE;
                meter21Btm.progress = 0.0;
            } else if (sliderGeq21.value < 0.0) {
                meter21Btm.progress = sliderGeq21.value/GEQ_SLIDER_MIN_VALUE;
                meter21Top.progress = 0.0;
            } else {
                meter21Top.progress = 0.0;
                meter21Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq22.value != geq22Value) {
            sliderGeq22.value = geq22Value;
            if (sliderGeq22.value > 0.0) {
                meter22Top.progress = sliderGeq22.value/GEQ_SLIDER_MAX_VALUE;
                meter22Btm.progress = 0.0;
            } else if (sliderGeq22.value < 0.0) {
                meter22Btm.progress = sliderGeq22.value/GEQ_SLIDER_MIN_VALUE;
                meter22Top.progress = 0.0;
            } else {
                meter22Top.progress = 0.0;
                meter22Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq23.value != geq23Value) {
            sliderGeq23.value = geq23Value;
            if (sliderGeq23.value > 0.0) {
                meter23Top.progress = sliderGeq23.value/GEQ_SLIDER_MAX_VALUE;
                meter23Btm.progress = 0.0;
            } else if (sliderGeq23.value < 0.0) {
                meter23Btm.progress = sliderGeq23.value/GEQ_SLIDER_MIN_VALUE;
                meter23Top.progress = 0.0;
            } else {
                meter23Top.progress = 0.0;
                meter23Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq24.value != geq24Value) {
            sliderGeq24.value = geq24Value;
            if (sliderGeq24.value > 0.0) {
                meter24Top.progress = sliderGeq24.value/GEQ_SLIDER_MAX_VALUE;
                meter24Btm.progress = 0.0;
            } else if (sliderGeq24.value < 0.0) {
                meter24Btm.progress = sliderGeq24.value/GEQ_SLIDER_MIN_VALUE;
                meter24Top.progress = 0.0;
            } else {
                meter24Top.progress = 0.0;
                meter24Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq25.value != geq25Value) {
            sliderGeq25.value = geq25Value;
            if (sliderGeq25.value > 0.0) {
                meter25Top.progress = sliderGeq25.value/GEQ_SLIDER_MAX_VALUE;
                meter25Btm.progress = 0.0;
            } else if (sliderGeq25.value < 0.0) {
                meter25Btm.progress = sliderGeq25.value/GEQ_SLIDER_MIN_VALUE;
                meter25Top.progress = 0.0;
            } else {
                meter25Top.progress = 0.0;
                meter25Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq26.value != geq26Value) {
            sliderGeq26.value = geq26Value;
            if (sliderGeq26.value > 0.0) {
                meter26Top.progress = sliderGeq26.value/GEQ_SLIDER_MAX_VALUE;
                meter26Btm.progress = 0.0;
            } else if (sliderGeq26.value < 0.0) {
                meter26Btm.progress = sliderGeq26.value/GEQ_SLIDER_MIN_VALUE;
                meter26Top.progress = 0.0;
            } else {
                meter26Top.progress = 0.0;
                meter26Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq27.value != geq27Value) {
            sliderGeq27.value = geq27Value;
            if (sliderGeq27.value > 0.0) {
                meter27Top.progress = sliderGeq27.value/GEQ_SLIDER_MAX_VALUE;
                meter27Btm.progress = 0.0;
            } else if (sliderGeq27.value < 0.0) {
                meter27Btm.progress = sliderGeq27.value/GEQ_SLIDER_MIN_VALUE;
                meter27Top.progress = 0.0;
            } else {
                meter27Top.progress = 0.0;
                meter27Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq28.value != geq28Value) {
            sliderGeq28.value = geq28Value;
            if (sliderGeq28.value > 0.0) {
                meter28Top.progress = sliderGeq28.value/GEQ_SLIDER_MAX_VALUE;
                meter28Btm.progress = 0.0;
            } else if (sliderGeq28.value < 0.0) {
                meter28Btm.progress = sliderGeq28.value/GEQ_SLIDER_MIN_VALUE;
                meter28Top.progress = 0.0;
            } else {
                meter28Top.progress = 0.0;
                meter28Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq29.value != geq29Value) {
            sliderGeq29.value = geq29Value;
            if (sliderGeq29.value > 0.0) {
                meter29Top.progress = sliderGeq29.value/GEQ_SLIDER_MAX_VALUE;
                meter29Btm.progress = 0.0;
            } else if (sliderGeq29.value < 0.0) {
                meter29Btm.progress = sliderGeq29.value/GEQ_SLIDER_MIN_VALUE;
                meter29Top.progress = 0.0;
            } else {
                meter29Top.progress = 0.0;
                meter29Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq30.value != geq30Value) {
            sliderGeq30.value = geq30Value;
            if (sliderGeq30.value > 0.0) {
                meter30Top.progress = sliderGeq30.value/GEQ_SLIDER_MAX_VALUE;
                meter30Btm.progress = 0.0;
            } else if (sliderGeq30.value < 0.0) {
                meter30Btm.progress = sliderGeq30.value/GEQ_SLIDER_MIN_VALUE;
                meter30Top.progress = 0.0;
            } else {
                meter30Top.progress = 0.0;
                meter30Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq31.value != geq31Value) {
            sliderGeq31.value = geq31Value;
            if (sliderGeq31.value > 0.0) {
                meter31Top.progress = sliderGeq31.value/GEQ_SLIDER_MAX_VALUE;
                meter31Btm.progress = 0.0;
            } else if (sliderGeq31.value < 0.0) {
                meter31Btm.progress = sliderGeq31.value/GEQ_SLIDER_MIN_VALUE;
                meter31Top.progress = 0.0;
            } else {
                meter31Top.progress = 0.0;
                meter31Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        int geqEnabled = (pkt->main_l_r_ctrl & 0x80) >> 7;
        if (btnGeqOnOff.selected != geqEnabled) {
            btnGeqOnOff.selected = geqEnabled;
            peqEnabled[CHANNEL_MAIN] = btnGeqOnOff.selected;
            [self updateGeqMeters];
            needsUpdate = YES;
        }
        
        if (needsUpdate) {
            [self updatePreviewImageForChannel:CHANNEL_MAIN];
        }
    } else {
        if (peq1G[CHANNEL_MAIN] != pkt->main_l_r_eq_1_db+EQ_GAIN_OFFSET) {
            peq1G[CHANNEL_MAIN] = pkt->main_l_r_eq_1_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq1F[CHANNEL_MAIN] != pkt->main_l_r_eq_1_freq) {
            peq1F[CHANNEL_MAIN] = pkt->main_l_r_eq_1_freq;
            valueChanged = YES;
        }
        if (peq1Q[CHANNEL_MAIN] != pkt->main_l_r_eq_1_q) {
            peq1Q[CHANNEL_MAIN] = pkt->main_l_r_eq_1_q;
            if (peq1Q[CHANNEL_MAIN] == PEQ_1_LOW_SHELF) {
                peq1Filter[CHANNEL_MAIN] = EQ_LOW_SHELF;
            } else if (peq1Q[CHANNEL_MAIN] == PEQ_1_HPF) {
                peq1Filter[CHANNEL_MAIN] = EQ_LOW_CUT;
            } else {
                peq1Filter[CHANNEL_MAIN] = EQ_PEAK;
                lastPeq1Q[CHANNEL_MAIN] = peq1Q[CHANNEL_MAIN];
            }
            valueChanged = YES;
        }
        if (peq2G[CHANNEL_MAIN] != pkt->main_l_r_eq_2_db+EQ_GAIN_OFFSET) {
            peq2G[CHANNEL_MAIN] = pkt->main_l_r_eq_2_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq2F[CHANNEL_MAIN] != pkt->main_l_r_eq_2_freq) {
            peq2F[CHANNEL_MAIN] = pkt->main_l_r_eq_2_freq;
            valueChanged = YES;
        }
        if (peq2Q[CHANNEL_MAIN] != pkt->main_l_r_eq_2_q) {
            peq2Q[CHANNEL_MAIN] = pkt->main_l_r_eq_2_q;
            valueChanged = YES;
        }
        if (peq3G[CHANNEL_MAIN] != pkt->main_l_r_eq_3_db+EQ_GAIN_OFFSET) {
            peq3G[CHANNEL_MAIN] = pkt->main_l_r_eq_3_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq3F[CHANNEL_MAIN] != pkt->main_l_r_eq_3_freq) {
            peq3F[CHANNEL_MAIN] = pkt->main_l_r_eq_3_freq;
            valueChanged = YES;
        }
        if (peq3Q[CHANNEL_MAIN] != pkt->main_l_r_eq_3_q) {
            peq3Q[CHANNEL_MAIN] = pkt->main_l_r_eq_3_q;
            valueChanged = YES;
        }
        if (peq4G[CHANNEL_MAIN] != pkt->main_l_r_eq_4_db+EQ_GAIN_OFFSET) {
            peq4G[CHANNEL_MAIN] = pkt->main_l_r_eq_4_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq4F[CHANNEL_MAIN] != pkt->main_l_r_eq_4_freq) {
            peq4F[CHANNEL_MAIN] = pkt->main_l_r_eq_4_freq;
            valueChanged = YES;
        }
        if (peq4Q[CHANNEL_MAIN] != pkt->main_l_r_eq_4_q) {
            peq4Q[CHANNEL_MAIN] = pkt->main_l_r_eq_4_q;
            if (peq4Q[CHANNEL_MAIN] == PEQ_4_HIGH_SHELF) {
                peq4Filter[CHANNEL_MAIN] = EQ_HIGH_SHELF;
            } else if (peq4Q[CHANNEL_MAIN] == PEQ_4_LPF) {
                peq4Filter[CHANNEL_MAIN] = EQ_HIGH_CUT;
            } else {
                peq4Filter[CHANNEL_MAIN] = EQ_PEAK;
                lastPeq4Q[CHANNEL_MAIN] = peq4Q[CHANNEL_MAIN];
            }
            valueChanged = YES;
        }
        
        int peqEnableValue = pkt->main_l_r_ctrl;
        if (peq1Enabled[CHANNEL_MAIN] != ((peqEnableValue & 0x08) >> 3)) {
            peq1Enabled[CHANNEL_MAIN] = (peqEnableValue & 0x08) >> 3;
            valueChanged = YES;
        }
        if (peq2Enabled[CHANNEL_MAIN] != ((peqEnableValue & 0x10) >> 4)) {
            peq2Enabled[CHANNEL_MAIN] = (peqEnableValue & 0x10) >> 4;
            valueChanged = YES;
        }
        if (peq3Enabled[CHANNEL_MAIN] != ((peqEnableValue & 0x20) >> 5)) {
            peq3Enabled[CHANNEL_MAIN] = (peqEnableValue & 0x20) >> 5;
            valueChanged = YES;
        }
        if (peq4Enabled[CHANNEL_MAIN] != ((peqEnableValue & 0x40) >> 6)) {
            peq4Enabled[CHANNEL_MAIN] = (peqEnableValue & 0x40) >> 6;
            valueChanged = YES;
        }
        if (peqEnabled[CHANNEL_MAIN] != ((peqEnableValue & 0x80) >> 7)) {
            peqEnabled[CHANNEL_MAIN] = (peqEnableValue & 0x80) >> 7;
            valueChanged = YES;
        }
        
        if (valueChanged) {
            [self updatePeqPlot:peqEnabled[CHANNEL_MAIN]];
            [self updatePeqForChannel:CHANNEL_MAIN];
        }
    }
}

- (void)processChannel9_16PageReply:(ChannelCh9_16PagePacket *)pkt {
    BOOL valueChanged = NO;
    
    // CHANNEL_CH_9
    {
        if (peq1G[CHANNEL_CH_9] != pkt->ch_9_eq_1_db+EQ_GAIN_OFFSET) {
            peq1G[CHANNEL_CH_9] = pkt->ch_9_eq_1_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq1F[CHANNEL_CH_9] != pkt->ch_9_eq_1_freq) {
            peq1F[CHANNEL_CH_9] = pkt->ch_9_eq_1_freq;
            valueChanged = YES;
        }
        if (peq1Q[CHANNEL_CH_9] != pkt->ch_9_eq_1_q) {
            peq1Q[CHANNEL_CH_9] = pkt->ch_9_eq_1_q;
            if (peq1Q[CHANNEL_CH_9] == PEQ_1_LOW_SHELF) {
                peq1Filter[CHANNEL_CH_9] = EQ_LOW_SHELF;
            } else if (peq1Q[CHANNEL_CH_9] == PEQ_1_HPF) {
                peq1Filter[CHANNEL_CH_9] = EQ_LOW_CUT;
            } else {
                peq1Filter[CHANNEL_CH_9] = EQ_PEAK;
                lastPeq1Q[CHANNEL_CH_9] = peq1Q[CHANNEL_CH_9];
            }
            valueChanged = YES;
        }
        if (peq2G[CHANNEL_CH_9] != pkt->ch_9_eq_2_db+EQ_GAIN_OFFSET) {
            peq2G[CHANNEL_CH_9] = pkt->ch_9_eq_2_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq2F[CHANNEL_CH_9] != pkt->ch_9_eq_2_freq) {
            peq2F[CHANNEL_CH_9] = pkt->ch_9_eq_2_freq;
            valueChanged = YES;
        }
        if (peq2Q[CHANNEL_CH_9] != pkt->ch_9_eq_2_q) {
            peq2Q[CHANNEL_CH_9] = pkt->ch_9_eq_2_q;
            valueChanged = YES;
        }
        if (peq3G[CHANNEL_CH_9] != pkt->ch_9_eq_3_db+EQ_GAIN_OFFSET) {
            peq3G[CHANNEL_CH_9] = pkt->ch_9_eq_3_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq3F[CHANNEL_CH_9] != pkt->ch_9_eq_3_freq) {
            peq3F[CHANNEL_CH_9] = pkt->ch_9_eq_3_freq;
            valueChanged = YES;
        }
        if (peq3Q[CHANNEL_CH_9] != pkt->ch_9_eq_3_q) {
            peq3Q[CHANNEL_CH_9] = pkt->ch_9_eq_3_q;
            valueChanged = YES;
        }
        if (peq4G[CHANNEL_CH_9] != pkt->ch_9_eq_4_db+EQ_GAIN_OFFSET) {
            peq4G[CHANNEL_CH_9] = pkt->ch_9_eq_4_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq4F[CHANNEL_CH_9] != pkt->ch_9_eq_4_freq) {
            peq4F[CHANNEL_CH_9] = pkt->ch_9_eq_4_freq;
            valueChanged = YES;
        }
        if (peq4Q[CHANNEL_CH_9] != pkt->ch_9_eq_4_q) {
            peq4Q[CHANNEL_CH_9] = pkt->ch_9_eq_4_q;
            if (peq4Q[CHANNEL_CH_9] == PEQ_4_HIGH_SHELF) {
                peq4Filter[CHANNEL_CH_9] = EQ_HIGH_SHELF;
            } else if (peq4Q[CHANNEL_CH_9] == PEQ_4_LPF) {
                peq4Filter[CHANNEL_CH_9] = EQ_HIGH_CUT;
            } else {
                peq4Filter[CHANNEL_CH_9] = EQ_PEAK;
                lastPeq4Q[CHANNEL_CH_9] = peq4Q[CHANNEL_CH_9];
            }
            valueChanged = YES;
        }
        int peqEnableValue = pkt->ch_9_ctrl;
        if (peq1Enabled[CHANNEL_CH_9] != ((peqEnableValue & 0x08) >> 3)) {
            peq1Enabled[CHANNEL_CH_9] = (peqEnableValue & 0x08) >> 3;
            valueChanged = YES;
        }
        if (peq2Enabled[CHANNEL_CH_9] != ((peqEnableValue & 0x10) >> 4)) {
            peq2Enabled[CHANNEL_CH_9] = (peqEnableValue & 0x10) >> 4;
            valueChanged = YES;
        }
        if (peq3Enabled[CHANNEL_CH_9] != ((peqEnableValue & 0x20) >> 5)) {
            peq3Enabled[CHANNEL_CH_9] = (peqEnableValue & 0x20) >> 5;
            valueChanged = YES;
        }
        if (peq4Enabled[CHANNEL_CH_9] != ((peqEnableValue & 0x40) >> 6)) {
            peq4Enabled[CHANNEL_CH_9] = (peqEnableValue & 0x40) >> 6;
            valueChanged = YES;
        }
        if (peqEnabled[CHANNEL_CH_9] != ((peqEnableValue & 0x80) >> 7)) {
            peqEnabled[CHANNEL_CH_9] = (peqEnableValue & 0x80) >> 7;
            valueChanged = YES;
        }
    }
    
    if (valueChanged) {
        [self updatePeqPlot:peqEnabled[CHANNEL_CH_9]];
        [self updatePeqForChannel:CHANNEL_CH_9];
        valueChanged = NO;
    }
    
    // CHANNEL_CH_10
    {
        if (peq1G[CHANNEL_CH_10] != pkt->ch_10_eq_1_db+EQ_GAIN_OFFSET) {
            peq1G[CHANNEL_CH_10] = pkt->ch_10_eq_1_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq1F[CHANNEL_CH_10] != pkt->ch_10_eq_1_freq) {
            peq1F[CHANNEL_CH_10] = pkt->ch_10_eq_1_freq;
            valueChanged = YES;
        }
        if (peq1Q[CHANNEL_CH_10] != pkt->ch_10_eq_1_q) {
            peq1Q[CHANNEL_CH_10] = pkt->ch_10_eq_1_q;
            if (peq1Q[CHANNEL_CH_10] == PEQ_1_LOW_SHELF) {
                [btnPeqFilterFront setImage:[UIImage imageNamed:@"frame-12.png"] forState:UIControlStateNormal];
                peq1Filter[CHANNEL_CH_10] = EQ_LOW_SHELF;
            } else if (peq1Q[CHANNEL_CH_10] == PEQ_1_HPF) {
                [btnPeqFilterFront setImage:[UIImage imageNamed:@"frame-13.png"] forState:UIControlStateNormal];
                peq1Filter[CHANNEL_CH_10] = EQ_LOW_CUT;
            } else {
                [btnPeqFilterFront setImage:[UIImage imageNamed:@"frame-11.png"] forState:UIControlStateNormal];
                peq1Filter[CHANNEL_CH_10] = EQ_PEAK;
                lastPeq1Q[CHANNEL_CH_10] = peq1Q[CHANNEL_CH_10];
            }
            valueChanged = YES;
        }
        if (peq2G[CHANNEL_CH_10] != pkt->ch_10_eq_2_db+EQ_GAIN_OFFSET) {
            peq2G[CHANNEL_CH_10] = pkt->ch_10_eq_2_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq2F[CHANNEL_CH_10] != pkt->ch_10_eq_2_freq) {
            peq2F[CHANNEL_CH_10] = pkt->ch_10_eq_2_freq;
            valueChanged = YES;
        }
        if (peq2Q[CHANNEL_CH_10] != pkt->ch_10_eq_2_q) {
            peq2Q[CHANNEL_CH_10] = pkt->ch_10_eq_2_q;
            valueChanged = YES;
        }
        if (peq3G[CHANNEL_CH_10] != pkt->ch_10_eq_3_db+EQ_GAIN_OFFSET) {
            peq3G[CHANNEL_CH_10] = pkt->ch_10_eq_3_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq3F[CHANNEL_CH_10] != pkt->ch_10_eq_3_freq) {
            peq3F[CHANNEL_CH_10] = pkt->ch_10_eq_3_freq;
            valueChanged = YES;
        }
        if (peq3Q[CHANNEL_CH_10] != pkt->ch_10_eq_3_q) {
            peq3Q[CHANNEL_CH_10] = pkt->ch_10_eq_3_q;
            valueChanged = YES;
        }
        if (peq4G[CHANNEL_CH_10] != pkt->ch_10_eq_4_db+EQ_GAIN_OFFSET) {
            peq4G[CHANNEL_CH_10] = pkt->ch_10_eq_4_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq4F[CHANNEL_CH_10] != pkt->ch_10_eq_4_freq) {
            peq4F[CHANNEL_CH_10] = pkt->ch_10_eq_4_freq;
            valueChanged = YES;
        }
        if (peq4Q[CHANNEL_CH_10] != pkt->ch_10_eq_4_q) {
            peq4Q[CHANNEL_CH_10] = pkt->ch_10_eq_4_q;
            if (peq4Q[CHANNEL_CH_10] == PEQ_4_HIGH_SHELF) {
                peq4Filter[CHANNEL_CH_10] = EQ_HIGH_SHELF;
            } else if (peq4Q[CHANNEL_CH_10] == PEQ_4_LPF) {
                peq4Filter[CHANNEL_CH_10] = EQ_HIGH_CUT;
            } else {
                peq4Filter[CHANNEL_CH_10] = EQ_PEAK;
                lastPeq4Q[CHANNEL_CH_10] = peq4Q[CHANNEL_CH_10];
            }
            valueChanged = YES;
        }
        int peqEnableValue = pkt->ch_10_ctrl;
        if (peq1Enabled[CHANNEL_CH_10] != ((peqEnableValue & 0x08) >> 3)) {
            peq1Enabled[CHANNEL_CH_10] = (peqEnableValue & 0x08) >> 3;
            valueChanged = YES;
        }
        if (peq2Enabled[CHANNEL_CH_10] != ((peqEnableValue & 0x10) >> 4)) {
            peq2Enabled[CHANNEL_CH_10] = (peqEnableValue & 0x10) >> 4;
            valueChanged = YES;
        }
        if (peq3Enabled[CHANNEL_CH_10] != ((peqEnableValue & 0x20) >> 5)) {
            peq3Enabled[CHANNEL_CH_10] = (peqEnableValue & 0x20) >> 5;
            valueChanged = YES;
        }
        if (peq4Enabled[CHANNEL_CH_10] != ((peqEnableValue & 0x40) >> 6)) {
            peq4Enabled[CHANNEL_CH_10] = (peqEnableValue & 0x40) >> 6;
            valueChanged = YES;
        }
        if (peqEnabled[CHANNEL_CH_10] != ((peqEnableValue & 0x80) >> 7)) {
            peqEnabled[CHANNEL_CH_10] = (peqEnableValue & 0x80) >> 7;
            valueChanged = YES;
        }
    }
    
    if (valueChanged) {
        [self updatePeqPlot:peqEnabled[CHANNEL_CH_10]];
        [self updatePeqForChannel:CHANNEL_CH_10];
        valueChanged = NO;
    }
    
    
    // CHANNEL_CH_11
    {
        if (peq1G[CHANNEL_CH_11] != pkt->ch_11_eq_1_db+EQ_GAIN_OFFSET) {
            peq1G[CHANNEL_CH_11] = pkt->ch_11_eq_1_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq1F[CHANNEL_CH_11] != pkt->ch_11_eq_1_freq) {
            peq1F[CHANNEL_CH_11] = pkt->ch_11_eq_1_freq;
            valueChanged = YES;
        }
        if (peq1Q[CHANNEL_CH_11] != pkt->ch_11_eq_1_q) {
            peq1Q[CHANNEL_CH_11] = pkt->ch_11_eq_1_q;
            if (peq1Q[CHANNEL_CH_11] == PEQ_1_LOW_SHELF) {
                peq1Filter[CHANNEL_CH_11] = EQ_LOW_SHELF;
            } else if (peq1Q[CHANNEL_CH_11] == PEQ_1_HPF) {
                peq1Filter[CHANNEL_CH_11] = EQ_LOW_CUT;
            } else {
                peq1Filter[CHANNEL_CH_11] = EQ_PEAK;
                lastPeq1Q[CHANNEL_CH_11] = peq1Q[CHANNEL_CH_11];
            }
            valueChanged = YES;
        }
        if (peq2G[CHANNEL_CH_11] != pkt->ch_11_eq_2_db+EQ_GAIN_OFFSET) {
            peq2G[CHANNEL_CH_11] = pkt->ch_11_eq_2_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq2F[CHANNEL_CH_11] != pkt->ch_11_eq_2_freq) {
            peq2F[CHANNEL_CH_11] = pkt->ch_11_eq_2_freq;
            valueChanged = YES;
        }
        if (peq2Q[CHANNEL_CH_11] != pkt->ch_11_eq_2_q) {
            peq2Q[CHANNEL_CH_11] = pkt->ch_11_eq_2_q;
            valueChanged = YES;
        }
        if (peq3G[CHANNEL_CH_11] != pkt->ch_11_eq_3_db+EQ_GAIN_OFFSET) {
            peq3G[CHANNEL_CH_11] = pkt->ch_11_eq_3_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq3F[CHANNEL_CH_11] != pkt->ch_11_eq_3_freq) {
            peq3F[CHANNEL_CH_11] = pkt->ch_11_eq_3_freq;
            valueChanged = YES;
        }
        if (peq3Q[CHANNEL_CH_11] != pkt->ch_11_eq_3_q) {
            peq3Q[CHANNEL_CH_11] = pkt->ch_11_eq_3_q;
            valueChanged = YES;
        }
        if (peq4G[CHANNEL_CH_11] != pkt->ch_11_eq_4_db+EQ_GAIN_OFFSET) {
            peq4G[CHANNEL_CH_11] = pkt->ch_11_eq_4_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq4F[CHANNEL_CH_11] != pkt->ch_11_eq_4_freq) {
            peq4F[CHANNEL_CH_11] = pkt->ch_11_eq_4_freq;
            valueChanged = YES;
        }
        if (peq4Q[CHANNEL_CH_11] != pkt->ch_11_eq_4_q) {
            peq4Q[CHANNEL_CH_11] = pkt->ch_11_eq_4_q;
            if (peq4Q[CHANNEL_CH_11] == PEQ_4_HIGH_SHELF) {
                peq4Filter[CHANNEL_CH_11] = EQ_HIGH_SHELF;
            } else if (peq4Q[CHANNEL_CH_11] == PEQ_4_LPF) {
                peq4Filter[CHANNEL_CH_11] = EQ_HIGH_CUT;
            } else {
                peq4Filter[CHANNEL_CH_11] = EQ_PEAK;
                lastPeq4Q[CHANNEL_CH_11] = peq4Q[CHANNEL_CH_11];
            }
            valueChanged = YES;
        }
        int peqEnableValue = pkt->ch_11_ctrl;
        if (peq1Enabled[CHANNEL_CH_11] != ((peqEnableValue & 0x08) >> 3)) {
            peq1Enabled[CHANNEL_CH_11] = (peqEnableValue & 0x08) >> 3;
            valueChanged = YES;
        }
        if (peq2Enabled[CHANNEL_CH_11] != ((peqEnableValue & 0x10) >> 4)) {
            peq2Enabled[CHANNEL_CH_11] = (peqEnableValue & 0x10) >> 4;
            valueChanged = YES;
        }
        if (peq3Enabled[CHANNEL_CH_11] != ((peqEnableValue & 0x20) >> 5)) {
            peq3Enabled[CHANNEL_CH_11] = (peqEnableValue & 0x20) >> 5;
            valueChanged = YES;
        }
        if (peq4Enabled[CHANNEL_CH_11] != ((peqEnableValue & 0x40) >> 6)) {
            peq4Enabled[CHANNEL_CH_11] = (peqEnableValue & 0x40) >> 6;
            valueChanged = YES;
        }
        if (peqEnabled[CHANNEL_CH_11] != ((peqEnableValue & 0x80) >> 7)) {
            peqEnabled[CHANNEL_CH_11] = (peqEnableValue & 0x80) >> 7;
            valueChanged = YES;
        }
    }
    
    if (valueChanged) {
        [self updatePeqPlot:peqEnabled[CHANNEL_CH_11]];
        [self updatePeqForChannel:CHANNEL_CH_11];
        valueChanged = NO;
    }
    
    
    // CHANNEL_CH_12
    {
        if (peq1G[CHANNEL_CH_12] != pkt->ch_12_eq_1_db+EQ_GAIN_OFFSET) {
            peq1G[CHANNEL_CH_12] = pkt->ch_12_eq_1_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq1F[CHANNEL_CH_12] != pkt->ch_12_eq_1_freq) {
            peq1F[CHANNEL_CH_12] = pkt->ch_12_eq_1_freq;
            valueChanged = YES;
        }
        if (peq1Q[CHANNEL_CH_12] != pkt->ch_12_eq_1_q) {
            peq1Q[CHANNEL_CH_12] = pkt->ch_12_eq_1_q;
            if (peq1Q[CHANNEL_CH_12] == PEQ_1_LOW_SHELF) {
                peq1Filter[CHANNEL_CH_12] = EQ_LOW_SHELF;
            } else if (peq1Q[CHANNEL_CH_12] == PEQ_1_HPF) {
                peq1Filter[CHANNEL_CH_12] = EQ_LOW_CUT;
            } else {
                peq1Filter[CHANNEL_CH_12] = EQ_PEAK;
                lastPeq1Q[CHANNEL_CH_12] = peq1Q[CHANNEL_CH_12];
            }
            valueChanged = YES;
        }
        if (peq2G[CHANNEL_CH_12] != pkt->ch_12_eq_2_db+EQ_GAIN_OFFSET) {
            peq2G[CHANNEL_CH_12] = pkt->ch_12_eq_2_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq2F[CHANNEL_CH_12] != pkt->ch_12_eq_2_freq) {
            peq2F[CHANNEL_CH_12] = pkt->ch_12_eq_2_freq;
            valueChanged = YES;
        }
        if (peq2Q[CHANNEL_CH_12] != pkt->ch_12_eq_2_q) {
            peq2Q[CHANNEL_CH_12] = pkt->ch_12_eq_2_q;
            valueChanged = YES;
        }
        if (peq3G[CHANNEL_CH_12] != pkt->ch_12_eq_3_db+EQ_GAIN_OFFSET) {
            peq3G[CHANNEL_CH_12] = pkt->ch_12_eq_3_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq3F[CHANNEL_CH_12] != pkt->ch_12_eq_3_freq) {
            peq3F[CHANNEL_CH_12] = pkt->ch_12_eq_3_freq;
            valueChanged = YES;
        }
        if (peq3Q[CHANNEL_CH_12] != pkt->ch_12_eq_3_q) {
            peq3Q[CHANNEL_CH_12] = pkt->ch_12_eq_3_q;
            valueChanged = YES;
        }
        if (peq4G[CHANNEL_CH_12] != pkt->ch_12_eq_4_db+EQ_GAIN_OFFSET) {
            peq4G[CHANNEL_CH_12] = pkt->ch_12_eq_4_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq4F[CHANNEL_CH_12] != pkt->ch_12_eq_4_freq) {
            peq4F[CHANNEL_CH_12] = pkt->ch_12_eq_4_freq;
            valueChanged = YES;
        }
        if (peq4Q[CHANNEL_CH_12] != pkt->ch_12_eq_4_q) {
            peq4Q[CHANNEL_CH_12] = pkt->ch_12_eq_4_q;
            if (peq4Q[CHANNEL_CH_12] == PEQ_4_HIGH_SHELF) {
                peq4Filter[CHANNEL_CH_12] = EQ_HIGH_SHELF;
            } else if (peq4Q[CHANNEL_CH_12] == PEQ_4_LPF) {
                peq4Filter[CHANNEL_CH_12] = EQ_HIGH_CUT;
            } else {
                peq4Filter[CHANNEL_CH_12] = EQ_PEAK;
                lastPeq4Q[CHANNEL_CH_12] = peq4Q[CHANNEL_CH_12];
            }
            valueChanged = YES;
        }
        int peqEnableValue = pkt->ch_12_ctrl;
        if (peq1Enabled[CHANNEL_CH_12] != ((peqEnableValue & 0x08) >> 3)) {
            peq1Enabled[CHANNEL_CH_12] = (peqEnableValue & 0x08) >> 3;
            valueChanged = YES;
        }
        if (peq2Enabled[CHANNEL_CH_12] != ((peqEnableValue & 0x10) >> 4)) {
            peq2Enabled[CHANNEL_CH_12] = (peqEnableValue & 0x10) >> 4;
            valueChanged = YES;
        }
        if (peq3Enabled[CHANNEL_CH_12] != ((peqEnableValue & 0x20) >> 5)) {
            peq3Enabled[CHANNEL_CH_12] = (peqEnableValue & 0x20) >> 5;
            valueChanged = YES;
        }
        if (peq4Enabled[CHANNEL_CH_12] != ((peqEnableValue & 0x40) >> 6)) {
            peq4Enabled[CHANNEL_CH_12] = (peqEnableValue & 0x40) >> 6;
            valueChanged = YES;
        }
        if (peqEnabled[CHANNEL_CH_12] != ((peqEnableValue & 0x80) >> 7)) {
            peqEnabled[CHANNEL_CH_12] = (peqEnableValue & 0x80) >> 7;
            valueChanged = YES;
        }
    }
    
    if (valueChanged) {
        [self updatePeqPlot:peqEnabled[CHANNEL_CH_12]];
        [self updatePeqForChannel:CHANNEL_CH_12];
        valueChanged = NO;
    }
    
    
    // CHANNEL_CH_13
    {
        if (peq1G[CHANNEL_CH_13] != pkt->ch_13_eq_1_db+EQ_GAIN_OFFSET) {
            peq1G[CHANNEL_CH_13] = pkt->ch_13_eq_1_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq1F[CHANNEL_CH_13] != pkt->ch_13_eq_1_freq) {
            peq1F[CHANNEL_CH_13] = pkt->ch_13_eq_1_freq;
            valueChanged = YES;
        }
        if (peq1Q[CHANNEL_CH_13] != pkt->ch_13_eq_1_q) {
            peq1Q[CHANNEL_CH_13] = pkt->ch_13_eq_1_q;
            if (peq1Q[CHANNEL_CH_13] == PEQ_1_LOW_SHELF) {
                peq1Filter[CHANNEL_CH_13] = EQ_LOW_SHELF;
            } else if (peq1Q[CHANNEL_CH_13] == PEQ_1_HPF) {
                peq1Filter[CHANNEL_CH_13] = EQ_LOW_CUT;
            } else {
                peq1Filter[CHANNEL_CH_13] = EQ_PEAK;
                lastPeq1Q[CHANNEL_CH_13] = peq1Q[CHANNEL_CH_13];
            }
            valueChanged = YES;
        }
        if (peq2G[CHANNEL_CH_13] != pkt->ch_13_eq_2_db+EQ_GAIN_OFFSET) {
            peq2G[CHANNEL_CH_13] = pkt->ch_13_eq_2_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq2F[CHANNEL_CH_13] != pkt->ch_13_eq_2_freq) {
            peq2F[CHANNEL_CH_13] = pkt->ch_13_eq_2_freq;
            valueChanged = YES;
        }
        if (peq2Q[CHANNEL_CH_13] != pkt->ch_13_eq_2_q) {
            peq2Q[CHANNEL_CH_13] = pkt->ch_13_eq_2_q;
            valueChanged = YES;
        }
        if (peq3G[CHANNEL_CH_13] != pkt->ch_13_eq_3_db+EQ_GAIN_OFFSET) {
            peq3G[CHANNEL_CH_13] = pkt->ch_13_eq_3_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq3F[CHANNEL_CH_13] != pkt->ch_13_eq_3_freq) {
            peq3F[CHANNEL_CH_13] = pkt->ch_13_eq_3_freq;
            valueChanged = YES;
        }
        if (peq3Q[CHANNEL_CH_13] != pkt->ch_13_eq_3_q) {
            peq3Q[CHANNEL_CH_13] = pkt->ch_13_eq_3_q;
            valueChanged = YES;
        }
        if (peq4G[CHANNEL_CH_13] != pkt->ch_13_eq_4_db+EQ_GAIN_OFFSET) {
            peq4G[CHANNEL_CH_13] = pkt->ch_13_eq_4_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq4F[CHANNEL_CH_13] != pkt->ch_13_eq_4_freq) {
            peq4F[CHANNEL_CH_13] = pkt->ch_13_eq_4_freq;
            valueChanged = YES;
        }
        if (peq4Q[CHANNEL_CH_13] != pkt->ch_13_eq_4_q) {
            peq4Q[CHANNEL_CH_13] = pkt->ch_13_eq_4_q;
            if (peq4Q[CHANNEL_CH_13] == PEQ_4_HIGH_SHELF) {
                peq4Filter[CHANNEL_CH_13] = EQ_HIGH_SHELF;
            } else if (peq4Q[CHANNEL_CH_13] == PEQ_4_LPF) {
                peq4Filter[CHANNEL_CH_13] = EQ_HIGH_CUT;
            } else {
                peq4Filter[CHANNEL_CH_13] = EQ_PEAK;
                lastPeq4Q[CHANNEL_CH_13] = peq4Q[CHANNEL_CH_13];
            }
            valueChanged = YES;
        }
        int peqEnableValue = pkt->ch_13_ctrl;
        if (peq1Enabled[CHANNEL_CH_13] != ((peqEnableValue & 0x08) >> 3)) {
            peq1Enabled[CHANNEL_CH_13] = (peqEnableValue & 0x08) >> 3;
            valueChanged = YES;
        }
        if (peq2Enabled[CHANNEL_CH_13] != ((peqEnableValue & 0x10) >> 4)) {
            peq2Enabled[CHANNEL_CH_13] = (peqEnableValue & 0x10) >> 4;
            valueChanged = YES;
        }
        if (peq3Enabled[CHANNEL_CH_13] != ((peqEnableValue & 0x20) >> 5)) {
            peq3Enabled[CHANNEL_CH_13] = (peqEnableValue & 0x20) >> 5;
            valueChanged = YES;
        }
        if (peq4Enabled[CHANNEL_CH_13] != ((peqEnableValue & 0x40) >> 6)) {
            peq4Enabled[CHANNEL_CH_13] = (peqEnableValue & 0x40) >> 6;
            valueChanged = YES;
        }
        if (peqEnabled[CHANNEL_CH_13] != ((peqEnableValue & 0x80) >> 7)) {
            peqEnabled[CHANNEL_CH_13] = (peqEnableValue & 0x80) >> 7;
            valueChanged = YES;
        }
    }
    
    if (valueChanged) {
        [self updatePeqPlot:peqEnabled[CHANNEL_CH_13]];
        [self updatePeqForChannel:CHANNEL_CH_13];
        valueChanged = NO;
    }
    
    
    // CHANNEL_CH_14
    {
        if (peq1G[CHANNEL_CH_14] != pkt->ch_14_eq_1_db+EQ_GAIN_OFFSET) {
            peq1G[CHANNEL_CH_14] = pkt->ch_14_eq_1_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq1F[CHANNEL_CH_14] != pkt->ch_14_eq_1_freq) {
            peq1F[CHANNEL_CH_14] = pkt->ch_14_eq_1_freq;
            valueChanged = YES;
        }
        if (peq1Q[CHANNEL_CH_14] != pkt->ch_14_eq_1_q) {
            peq1Q[CHANNEL_CH_14] = pkt->ch_14_eq_1_q;
            if (peq1Q[CHANNEL_CH_14] == PEQ_1_LOW_SHELF) {
                peq1Filter[CHANNEL_CH_14] = EQ_LOW_SHELF;
            } else if (peq1Q[CHANNEL_CH_14] == PEQ_1_HPF) {
                peq1Filter[CHANNEL_CH_14] = EQ_LOW_CUT;
            } else {
                peq1Filter[CHANNEL_CH_14] = EQ_PEAK;
                lastPeq1Q[CHANNEL_CH_14] = peq1Q[CHANNEL_CH_14];
            }
            valueChanged = YES;
        }
        if (peq2G[CHANNEL_CH_14] != pkt->ch_14_eq_2_db+EQ_GAIN_OFFSET) {
            peq2G[CHANNEL_CH_14] = pkt->ch_14_eq_2_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq2F[CHANNEL_CH_14] != pkt->ch_14_eq_2_freq) {
            peq2F[CHANNEL_CH_14] = pkt->ch_14_eq_2_freq;
            valueChanged = YES;
        }
        if (peq2Q[CHANNEL_CH_14] != pkt->ch_14_eq_2_q) {
            peq2Q[CHANNEL_CH_14] = pkt->ch_14_eq_2_q;
            valueChanged = YES;
        }
        if (peq3G[CHANNEL_CH_14] != pkt->ch_14_eq_3_db+EQ_GAIN_OFFSET) {
            peq3G[CHANNEL_CH_14] = pkt->ch_14_eq_3_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq3F[CHANNEL_CH_14] != pkt->ch_14_eq_3_freq) {
            peq3F[CHANNEL_CH_14] = pkt->ch_14_eq_3_freq;
            valueChanged = YES;
        }
        if (peq3Q[CHANNEL_CH_14] != pkt->ch_14_eq_3_q) {
            peq3Q[CHANNEL_CH_14] = pkt->ch_14_eq_3_q;
            valueChanged = YES;
        }
        if (peq4G[CHANNEL_CH_14] != pkt->ch_14_eq_4_db+EQ_GAIN_OFFSET) {
            peq4G[CHANNEL_CH_14] = pkt->ch_14_eq_4_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq4F[CHANNEL_CH_14] != pkt->ch_14_eq_4_freq) {
            peq4F[CHANNEL_CH_14] = pkt->ch_14_eq_4_freq;
            valueChanged = YES;
        }
        if (peq4Q[CHANNEL_CH_14] != pkt->ch_14_eq_4_q) {
            peq4Q[CHANNEL_CH_14] = pkt->ch_14_eq_4_q;
            if (peq4Q[CHANNEL_CH_14] == PEQ_4_HIGH_SHELF) {
                peq4Filter[CHANNEL_CH_14] = EQ_HIGH_SHELF;
            } else if (peq4Q[CHANNEL_CH_14] == PEQ_4_LPF) {
                peq4Filter[CHANNEL_CH_14] = EQ_HIGH_CUT;
            } else {
                peq4Filter[CHANNEL_CH_14] = EQ_PEAK;
                lastPeq4Q[CHANNEL_CH_14] = peq4Q[CHANNEL_CH_14];
            }
            valueChanged = YES;
        }
        int peqEnableValue = pkt->ch_14_ctrl;
        if (peq1Enabled[CHANNEL_CH_14] != ((peqEnableValue & 0x08) >> 3)) {
            peq1Enabled[CHANNEL_CH_14] = (peqEnableValue & 0x08) >> 3;
            valueChanged = YES;
        }
        if (peq2Enabled[CHANNEL_CH_14] != ((peqEnableValue & 0x10) >> 4)) {
            peq2Enabled[CHANNEL_CH_14] = (peqEnableValue & 0x10) >> 4;
            valueChanged = YES;
        }
        if (peq3Enabled[CHANNEL_CH_14] != ((peqEnableValue & 0x20) >> 5)) {
            peq3Enabled[CHANNEL_CH_14] = (peqEnableValue & 0x20) >> 5;
            valueChanged = YES;
        }
        if (peq4Enabled[CHANNEL_CH_14] != ((peqEnableValue & 0x40) >> 6)) {
            peq4Enabled[CHANNEL_CH_14] = (peqEnableValue & 0x40) >> 6;
            valueChanged = YES;
        }
        if (peqEnabled[CHANNEL_CH_14] != ((peqEnableValue & 0x80) >> 7)) {
            peqEnabled[CHANNEL_CH_14] = (peqEnableValue & 0x80) >> 7;
            valueChanged = YES;
        }
    }
    
    if (valueChanged) {
        [self updatePeqPlot:peqEnabled[CHANNEL_CH_14]];
        [self updatePeqForChannel:CHANNEL_CH_14];
        valueChanged = NO;
    }
    
    
    // CHANNEL_CH_15
    {
        if (peq1G[CHANNEL_CH_15] != pkt->ch_15_eq_1_db+EQ_GAIN_OFFSET) {
            peq1G[CHANNEL_CH_15] = pkt->ch_15_eq_1_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq1F[CHANNEL_CH_15] != pkt->ch_15_eq_1_freq) {
            peq1F[CHANNEL_CH_15] = pkt->ch_15_eq_1_freq;
            valueChanged = YES;
        }
        if (peq1Q[CHANNEL_CH_15] != pkt->ch_15_eq_1_q) {
            peq1Q[CHANNEL_CH_15] = pkt->ch_15_eq_1_q;
            if (peq1Q[CHANNEL_CH_15] == PEQ_1_LOW_SHELF) {
                peq1Filter[CHANNEL_CH_15] = EQ_LOW_SHELF;
            } else if (peq1Q[CHANNEL_CH_15] == PEQ_1_HPF) {
                peq1Filter[CHANNEL_CH_15] = EQ_LOW_CUT;
            } else {
                peq1Filter[CHANNEL_CH_15] = EQ_PEAK;
                lastPeq1Q[CHANNEL_CH_15] = peq1Q[CHANNEL_CH_15];
            }
            valueChanged = YES;
        }
        if (peq2G[CHANNEL_CH_15] != pkt->ch_15_eq_2_db+EQ_GAIN_OFFSET) {
            peq2G[CHANNEL_CH_15] = pkt->ch_15_eq_2_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq2F[CHANNEL_CH_15] != pkt->ch_15_eq_2_freq) {
            peq2F[CHANNEL_CH_15] = pkt->ch_15_eq_2_freq;
            valueChanged = YES;
        }
        if (peq2Q[CHANNEL_CH_15] != pkt->ch_15_eq_2_q) {
            peq2Q[CHANNEL_CH_15] = pkt->ch_15_eq_2_q;
            valueChanged = YES;
        }
        if (peq3G[CHANNEL_CH_15] != pkt->ch_15_eq_3_db+EQ_GAIN_OFFSET) {
            peq3G[CHANNEL_CH_15] = pkt->ch_15_eq_3_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq3F[CHANNEL_CH_15] != pkt->ch_15_eq_3_freq) {
            peq3F[CHANNEL_CH_15] = pkt->ch_15_eq_3_freq;
            valueChanged = YES;
        }
        if (peq3Q[CHANNEL_CH_15] != pkt->ch_15_eq_3_q) {
            peq3Q[CHANNEL_CH_15] = pkt->ch_15_eq_3_q;
            valueChanged = YES;
        }
        if (peq4G[CHANNEL_CH_15] != pkt->ch_15_eq_4_db+EQ_GAIN_OFFSET) {
            peq4G[CHANNEL_CH_15] = pkt->ch_15_eq_4_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq4F[CHANNEL_CH_15] != pkt->ch_15_eq_4_freq) {
            peq4F[CHANNEL_CH_15] = pkt->ch_15_eq_4_freq;
            valueChanged = YES;
        }
        if (peq4Q[CHANNEL_CH_15] != pkt->ch_15_eq_4_q) {
            peq4Q[CHANNEL_CH_15] = pkt->ch_15_eq_4_q;
            if (peq4Q[CHANNEL_CH_15] == PEQ_4_HIGH_SHELF) {
                peq4Filter[CHANNEL_CH_15] = EQ_HIGH_SHELF;
            } else if (peq4Q[CHANNEL_CH_15] == PEQ_4_LPF) {
                peq4Filter[CHANNEL_CH_15] = EQ_HIGH_CUT;
            } else {
                peq4Filter[CHANNEL_CH_15] = EQ_PEAK;
                lastPeq4Q[CHANNEL_CH_15] = peq4Q[CHANNEL_CH_15];
            }
            valueChanged = YES;
        }
        int peqEnableValue = pkt->ch_15_ctrl;
        if (peq1Enabled[CHANNEL_CH_15] != ((peqEnableValue & 0x08) >> 3)) {
            peq1Enabled[CHANNEL_CH_15] = (peqEnableValue & 0x08) >> 3;
            valueChanged = YES;
        }
        if (peq2Enabled[CHANNEL_CH_15] != ((peqEnableValue & 0x10) >> 4)) {
            peq2Enabled[CHANNEL_CH_15] = (peqEnableValue & 0x10) >> 4;
            valueChanged = YES;
        }
        if (peq3Enabled[CHANNEL_CH_15] != ((peqEnableValue & 0x20) >> 5)) {
            peq3Enabled[CHANNEL_CH_15] = (peqEnableValue & 0x20) >> 5;
            valueChanged = YES;
        }
        if (peq4Enabled[CHANNEL_CH_15] != ((peqEnableValue & 0x40) >> 6)) {
            peq4Enabled[CHANNEL_CH_15] = (peqEnableValue & 0x40) >> 6;
            valueChanged = YES;
        }
        if (peqEnabled[CHANNEL_CH_15] != ((peqEnableValue & 0x80) >> 7)) {
            peqEnabled[CHANNEL_CH_15] = (peqEnableValue & 0x80) >> 7;
            valueChanged = YES;
        }
    }
    
    if (valueChanged) {
        [self updatePeqPlot:peqEnabled[CHANNEL_CH_15]];
        [self updatePeqForChannel:CHANNEL_CH_15];
        valueChanged = NO;
    }
    
    
    // CHANNEL_CH_16
    {
        if (peq1G[CHANNEL_CH_16] != pkt->ch_16_eq_1_db+EQ_GAIN_OFFSET) {
            peq1G[CHANNEL_CH_16] = pkt->ch_16_eq_1_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq1F[CHANNEL_CH_16] != pkt->ch_16_eq_1_freq) {
            peq1F[CHANNEL_CH_16] = pkt->ch_16_eq_1_freq;
            valueChanged = YES;
        }
        if (peq1Q[CHANNEL_CH_16] != pkt->ch_16_eq_1_q) {
            peq1Q[CHANNEL_CH_16] = pkt->ch_16_eq_1_q;
            if (peq1Q[CHANNEL_CH_16] == PEQ_1_LOW_SHELF) {
                peq1Filter[CHANNEL_CH_16] = EQ_LOW_SHELF;
            } else if (peq1Q[CHANNEL_CH_16] == PEQ_1_HPF) {
                peq1Filter[CHANNEL_CH_16] = EQ_LOW_CUT;
            } else {
                peq1Filter[CHANNEL_CH_16] = EQ_PEAK;
                lastPeq1Q[CHANNEL_CH_16] = peq1Q[CHANNEL_CH_16];
            }
            valueChanged = YES;
        }
        if (peq2G[CHANNEL_CH_16] != pkt->ch_16_eq_2_db+EQ_GAIN_OFFSET) {
            peq2G[CHANNEL_CH_16] = pkt->ch_16_eq_2_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq2F[CHANNEL_CH_16] != pkt->ch_16_eq_2_freq) {
            peq2F[CHANNEL_CH_16] = pkt->ch_16_eq_2_freq;
            valueChanged = YES;
        }
        if (peq2Q[CHANNEL_CH_16] != pkt->ch_16_eq_2_q) {
            peq2Q[CHANNEL_CH_16] = pkt->ch_16_eq_2_q;
            valueChanged = YES;
        }
        if (peq3G[CHANNEL_CH_16] != pkt->ch_16_eq_3_db+EQ_GAIN_OFFSET) {
            peq3G[CHANNEL_CH_16] = pkt->ch_16_eq_3_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq3F[CHANNEL_CH_16] != pkt->ch_16_eq_3_freq) {
            peq3F[CHANNEL_CH_16] = pkt->ch_16_eq_3_freq;
            valueChanged = YES;
        }
        if (peq3Q[CHANNEL_CH_16] != pkt->ch_16_eq_3_q) {
            peq3Q[CHANNEL_CH_16] = pkt->ch_16_eq_3_q;
            valueChanged = YES;
        }
        if (peq4G[CHANNEL_CH_16] != pkt->ch_16_eq_4_db+EQ_GAIN_OFFSET) {
            peq4G[CHANNEL_CH_16] = pkt->ch_16_eq_4_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq4F[CHANNEL_CH_16] != pkt->ch_16_eq_4_freq) {
            peq4F[CHANNEL_CH_16] = pkt->ch_16_eq_4_freq;
            valueChanged = YES;
        }
        if (peq4Q[CHANNEL_CH_16] != pkt->ch_16_eq_4_q) {
            peq4Q[CHANNEL_CH_16] = pkt->ch_16_eq_4_q;
            if (peq4Q[CHANNEL_CH_16] == PEQ_4_HIGH_SHELF) {
                peq4Filter[CHANNEL_CH_16] = EQ_HIGH_SHELF;
            } else if (peq4Q[CHANNEL_CH_16] == PEQ_4_LPF) {
                peq4Filter[CHANNEL_CH_16] = EQ_HIGH_CUT;
            } else {
                peq4Filter[CHANNEL_CH_16] = EQ_PEAK;
                lastPeq4Q[CHANNEL_CH_16] = peq4Q[CHANNEL_CH_16];
            }
            valueChanged = YES;
        }
        int peqEnableValue = pkt->ch_16_ctrl;
        if (peq1Enabled[CHANNEL_CH_16] != ((peqEnableValue & 0x08) >> 3)) {
            peq1Enabled[CHANNEL_CH_16] = (peqEnableValue & 0x08) >> 3;
            valueChanged = YES;
        }
        if (peq2Enabled[CHANNEL_CH_16] != ((peqEnableValue & 0x10) >> 4)) {
            peq2Enabled[CHANNEL_CH_16] = (peqEnableValue & 0x10) >> 4;
            valueChanged = YES;
        }
        if (peq3Enabled[CHANNEL_CH_16] != ((peqEnableValue & 0x20) >> 5)) {
            peq3Enabled[CHANNEL_CH_16] = (peqEnableValue & 0x20) >> 5;
            valueChanged = YES;
        }
        if (peq4Enabled[CHANNEL_CH_16] != ((peqEnableValue & 0x40) >> 6)) {
            peq4Enabled[CHANNEL_CH_16] = (peqEnableValue & 0x40) >> 6;
            valueChanged = YES;
        }
        if (peqEnabled[CHANNEL_CH_16] != ((peqEnableValue & 0x80) >> 7)) {
            peqEnabled[CHANNEL_CH_16] = (peqEnableValue & 0x80) >> 7;
            valueChanged = YES;
        }
    }
    
    if (valueChanged) {
        [self updatePeqPlot:peqEnabled[CHANNEL_CH_16]];
        [self updatePeqForChannel:CHANNEL_CH_16];
        valueChanged = NO;
    }
    
    // CHANNEL_MAIN
    if (mainEqType == MAIN_EQ_GEQ) {
        BOOL needsUpdate = NO;
        
        // Update GEQ sliders
        int geq1Value = pkt->geq_20_hz_db;
        int geq2Value = pkt->geq_25_hz_db;
        int geq3Value = pkt->geq_31_5_hz_db;
        int geq4Value = pkt->geq_40_hz_db;
        int geq5Value = pkt->geq_50_hz_db;
        int geq6Value = pkt->geq_63_hz_db;
        int geq7Value = pkt->geq_80_hz_db;
        int geq8Value = pkt->geq_100_hz_db;
        int geq9Value = pkt->geq_125_hz_db;
        int geq10Value = pkt->geq_160_hz_db;
        int geq11Value = pkt->geq_200_hz_db;
        int geq12Value = pkt->geq_250_hz_db;
        int geq13Value = pkt->geq_315_hz_db;
        int geq14Value = pkt->geq_400_hz_db;
        int geq15Value = pkt->geq_500_hz_db;
        int geq16Value = pkt->geq_630_hz_db;
        int geq17Value = pkt->geq_800_hz_db;
        int geq18Value = pkt->geq_1_khz_db;
        int geq19Value = pkt->geq_1_25_khz_db;
        int geq20Value = pkt->geq_1_6_khz_db;
        int geq21Value = pkt->geq_2_khz_db;
        int geq22Value = pkt->geq_2_5_khz_db;
        int geq23Value = pkt->geq_3_15_khz_db;
        int geq24Value = pkt->geq_4_khz_db;
        int geq25Value = pkt->geq_5_khz_db;
        int geq26Value = pkt->geq_6_3_khz_db;
        int geq27Value = pkt->geq_8_khz_db;
        int geq28Value = pkt->geq_10_khz_db;
        int geq29Value = pkt->geq_12_5_khz_db;
        int geq30Value = pkt->geq_16_khz_db;
        int geq31Value = pkt->geq_20_khz_db;
        
        if (sliderGeq1.value != geq1Value) {
            sliderGeq1.value = geq1Value;
            if (sliderGeq1.value > 0.0) {
                meter1Top.progress = sliderGeq1.value/GEQ_SLIDER_MAX_VALUE;
                meter1Btm.progress = 0.0;
            } else if (sliderGeq2.value < 0.0) {
                meter1Btm.progress = sliderGeq1.value/GEQ_SLIDER_MIN_VALUE;
                meter1Top.progress = 0.0;
            } else {
                meter1Top.progress = 0.0;
                meter1Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq2.value != geq2Value) {
            sliderGeq2.value = geq2Value;
            if (sliderGeq2.value > 0.0) {
                meter2Top.progress = sliderGeq2.value/GEQ_SLIDER_MAX_VALUE;
                meter2Btm.progress = 0.0;
            } else if (sliderGeq2.value < 0.0) {
                meter2Btm.progress = sliderGeq2.value/GEQ_SLIDER_MIN_VALUE;
                meter2Top.progress = 0.0;
            } else {
                meter2Top.progress = 0.0;
                meter2Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq3.value != geq3Value) {
            sliderGeq3.value = geq3Value;
            if (sliderGeq3.value > 0.0) {
                meter3Top.progress = sliderGeq3.value/GEQ_SLIDER_MAX_VALUE;
                meter3Btm.progress = 0.0;
            } else if (sliderGeq3.value < 0.0) {
                meter3Btm.progress = sliderGeq3.value/GEQ_SLIDER_MIN_VALUE;
                meter3Top.progress = 0.0;
            } else {
                meter3Top.progress = 0.0;
                meter3Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq4.value != geq4Value) {
            sliderGeq4.value = geq4Value;
            if (sliderGeq4.value > 0.0) {
                meter4Top.progress = sliderGeq4.value/GEQ_SLIDER_MAX_VALUE;
                meter4Btm.progress = 0.0;
            } else if (sliderGeq4.value < 0.0) {
                meter4Btm.progress = sliderGeq4.value/GEQ_SLIDER_MIN_VALUE;
                meter4Top.progress = 0.0;
            } else {
                meter4Top.progress = 0.0;
                meter4Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq5.value != geq5Value) {
            sliderGeq5.value = geq5Value;
            if (sliderGeq5.value > 0.0) {
                meter5Top.progress = sliderGeq5.value/GEQ_SLIDER_MAX_VALUE;
                meter5Btm.progress = 0.0;
            } else if (sliderGeq5.value < 0.0) {
                meter5Btm.progress = sliderGeq5.value/GEQ_SLIDER_MIN_VALUE;
                meter5Top.progress = 0.0;
            } else {
                meter5Top.progress = 0.0;
                meter5Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq6.value != geq6Value) {
            sliderGeq6.value = geq6Value;
            if (sliderGeq6.value > 0.0) {
                meter6Top.progress = sliderGeq6.value/GEQ_SLIDER_MAX_VALUE;
                meter6Btm.progress = 0.0;
            } else if (sliderGeq6.value < 0.0) {
                meter6Btm.progress = sliderGeq6.value/GEQ_SLIDER_MIN_VALUE;
                meter6Top.progress = 0.0;
            } else {
                meter6Top.progress = 0.0;
                meter6Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq7.value != geq7Value) {
            sliderGeq7.value = geq7Value;
            if (sliderGeq7.value > 0.0) {
                meter7Top.progress = sliderGeq7.value/GEQ_SLIDER_MAX_VALUE;
                meter7Btm.progress = 0.0;
            } else if (sliderGeq7.value < 0.0) {
                meter7Btm.progress = sliderGeq7.value/GEQ_SLIDER_MIN_VALUE;
                meter7Top.progress = 0.0;
            } else {
                meter7Top.progress = 0.0;
                meter7Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq8.value != geq8Value) {
            sliderGeq8.value = geq8Value;
            if (sliderGeq8.value > 0.0) {
                meter8Top.progress = sliderGeq8.value/GEQ_SLIDER_MAX_VALUE;
                meter8Btm.progress = 0.0;
            } else if (sliderGeq8.value < 0.0) {
                meter8Btm.progress = sliderGeq8.value/GEQ_SLIDER_MIN_VALUE;
                meter8Top.progress = 0.0;
            } else {
                meter8Top.progress = 0.0;
                meter8Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq9.value != geq9Value) {
            sliderGeq9.value = geq9Value;
            if (sliderGeq9.value > 0.0) {
                meter9Top.progress = sliderGeq9.value/GEQ_SLIDER_MAX_VALUE;
                meter9Btm.progress = 0.0;
            } else if (sliderGeq9.value < 0.0) {
                meter9Btm.progress = sliderGeq9.value/GEQ_SLIDER_MIN_VALUE;
                meter9Top.progress = 0.0;
            } else {
                meter9Top.progress = 0.0;
                meter9Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq10.value != geq10Value) {
            sliderGeq10.value = geq10Value;
            if (sliderGeq10.value > 0.0) {
                meter10Top.progress = sliderGeq10.value/GEQ_SLIDER_MAX_VALUE;
                meter10Btm.progress = 0.0;
            } else if (sliderGeq10.value < 0.0) {
                meter10Btm.progress = sliderGeq10.value/GEQ_SLIDER_MIN_VALUE;
                meter10Top.progress = 0.0;
            } else {
                meter10Top.progress = 0.0;
                meter10Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq11.value != geq11Value) {
            sliderGeq11.value = geq11Value;
            if (sliderGeq11.value > 0.0) {
                meter11Top.progress = sliderGeq11.value/GEQ_SLIDER_MAX_VALUE;
                meter11Btm.progress = 0.0;
            } else if (sliderGeq11.value < 0.0) {
                meter11Btm.progress = sliderGeq11.value/GEQ_SLIDER_MIN_VALUE;
                meter1Top.progress = 0.0;
            } else {
                meter11Top.progress = 0.0;
                meter11Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq12.value != geq12Value) {
            sliderGeq12.value = geq12Value;
            if (sliderGeq12.value > 0.0) {
                meter12Top.progress = sliderGeq12.value/GEQ_SLIDER_MAX_VALUE;
                meter12Btm.progress = 0.0;
            } else if (sliderGeq12.value < 0.0) {
                meter12Btm.progress = sliderGeq12.value/GEQ_SLIDER_MIN_VALUE;
                meter12Top.progress = 0.0;
            } else {
                meter12Top.progress = 0.0;
                meter12Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq13.value != geq13Value) {
            sliderGeq13.value = geq13Value;
            if (sliderGeq13.value > 0.0) {
                meter13Top.progress = sliderGeq13.value/GEQ_SLIDER_MAX_VALUE;
                meter13Btm.progress = 0.0;
            } else if (sliderGeq13.value < 0.0) {
                meter13Btm.progress = sliderGeq13.value/GEQ_SLIDER_MIN_VALUE;
                meter13Top.progress = 0.0;
            } else {
                meter13Top.progress = 0.0;
                meter13Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq14.value != geq14Value) {
            sliderGeq14.value = geq14Value;
            if (sliderGeq14.value > 0.0) {
                meter14Top.progress = sliderGeq14.value/GEQ_SLIDER_MAX_VALUE;
                meter14Btm.progress = 0.0;
            } else if (sliderGeq14.value < 0.0) {
                meter14Btm.progress = sliderGeq14.value/GEQ_SLIDER_MIN_VALUE;
                meter14Top.progress = 0.0;
            } else {
                meter14Top.progress = 0.0;
                meter14Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq15.value != geq15Value) {
            sliderGeq15.value = geq15Value;
            if (sliderGeq15.value > 0.0) {
                meter15Top.progress = sliderGeq15.value/GEQ_SLIDER_MAX_VALUE;
                meter15Btm.progress = 0.0;
            } else if (sliderGeq15.value < 0.0) {
                meter15Btm.progress = sliderGeq15.value/GEQ_SLIDER_MIN_VALUE;
                meter15Top.progress = 0.0;
            } else {
                meter15Top.progress = 0.0;
                meter15Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq16.value != geq16Value) {
            sliderGeq16.value = geq16Value;
            if (sliderGeq16.value > 0.0) {
                meter16Top.progress = sliderGeq16.value/GEQ_SLIDER_MAX_VALUE;
                meter16Btm.progress = 0.0;
            } else if (sliderGeq16.value < 0.0) {
                meter16Btm.progress = sliderGeq16.value/GEQ_SLIDER_MIN_VALUE;
                meter16Top.progress = 0.0;
            } else {
                meter16Top.progress = 0.0;
                meter16Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq17.value != geq17Value) {
            sliderGeq17.value = geq17Value;
            if (sliderGeq17.value > 0.0) {
                meter17Top.progress = sliderGeq17.value/GEQ_SLIDER_MAX_VALUE;
                meter17Btm.progress = 0.0;
            } else if (sliderGeq17.value < 0.0) {
                meter17Btm.progress = sliderGeq17.value/GEQ_SLIDER_MIN_VALUE;
                meter17Top.progress = 0.0;
            } else {
                meter17Top.progress = 0.0;
                meter17Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq18.value != geq18Value) {
            sliderGeq18.value = geq18Value;
            if (sliderGeq18.value > 0.0) {
                meter18Top.progress = sliderGeq18.value/GEQ_SLIDER_MAX_VALUE;
                meter18Btm.progress = 0.0;
            } else if (sliderGeq18.value < 0.0) {
                meter18Btm.progress = sliderGeq18.value/GEQ_SLIDER_MIN_VALUE;
                meter18Top.progress = 0.0;
            } else {
                meter18Top.progress = 0.0;
                meter18Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq19.value != geq19Value) {
            sliderGeq19.value = geq19Value;
            if (sliderGeq19.value > 0.0) {
                meter19Top.progress = sliderGeq19.value/GEQ_SLIDER_MAX_VALUE;
                meter19Btm.progress = 0.0;
            } else if (sliderGeq19.value < 0.0) {
                meter19Btm.progress = sliderGeq19.value/GEQ_SLIDER_MIN_VALUE;
                meter19Top.progress = 0.0;
            } else {
                meter19Top.progress = 0.0;
                meter19Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq20.value != geq20Value) {
            sliderGeq20.value = geq20Value;
            if (sliderGeq20.value > 0.0) {
                meter20Top.progress = sliderGeq20.value/GEQ_SLIDER_MAX_VALUE;
                meter20Btm.progress = 0.0;
            } else if (sliderGeq20.value < 0.0) {
                meter20Btm.progress = sliderGeq20.value/GEQ_SLIDER_MIN_VALUE;
                meter20Top.progress = 0.0;
            } else {
                meter20Top.progress = 0.0;
                meter20Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq21.value != geq21Value) {
            sliderGeq21.value = geq21Value;
            if (sliderGeq21.value > 0.0) {
                meter21Top.progress = sliderGeq21.value/GEQ_SLIDER_MAX_VALUE;
                meter21Btm.progress = 0.0;
            } else if (sliderGeq21.value < 0.0) {
                meter21Btm.progress = sliderGeq21.value/GEQ_SLIDER_MIN_VALUE;
                meter21Top.progress = 0.0;
            } else {
                meter21Top.progress = 0.0;
                meter21Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq22.value != geq22Value) {
            sliderGeq22.value = geq22Value;
            if (sliderGeq22.value > 0.0) {
                meter22Top.progress = sliderGeq22.value/GEQ_SLIDER_MAX_VALUE;
                meter22Btm.progress = 0.0;
            } else if (sliderGeq22.value < 0.0) {
                meter22Btm.progress = sliderGeq22.value/GEQ_SLIDER_MIN_VALUE;
                meter22Top.progress = 0.0;
            } else {
                meter22Top.progress = 0.0;
                meter22Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq23.value != geq23Value) {
            sliderGeq23.value = geq23Value;
            if (sliderGeq23.value > 0.0) {
                meter23Top.progress = sliderGeq23.value/GEQ_SLIDER_MAX_VALUE;
                meter23Btm.progress = 0.0;
            } else if (sliderGeq23.value < 0.0) {
                meter23Btm.progress = sliderGeq23.value/GEQ_SLIDER_MIN_VALUE;
                meter23Top.progress = 0.0;
            } else {
                meter23Top.progress = 0.0;
                meter23Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq24.value != geq24Value) {
            sliderGeq24.value = geq24Value;
            if (sliderGeq24.value > 0.0) {
                meter24Top.progress = sliderGeq24.value/GEQ_SLIDER_MAX_VALUE;
                meter24Btm.progress = 0.0;
            } else if (sliderGeq24.value < 0.0) {
                meter24Btm.progress = sliderGeq24.value/GEQ_SLIDER_MIN_VALUE;
                meter24Top.progress = 0.0;
            } else {
                meter24Top.progress = 0.0;
                meter24Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq25.value != geq25Value) {
            sliderGeq25.value = geq25Value;
            if (sliderGeq25.value > 0.0) {
                meter25Top.progress = sliderGeq25.value/GEQ_SLIDER_MAX_VALUE;
                meter25Btm.progress = 0.0;
            } else if (sliderGeq25.value < 0.0) {
                meter25Btm.progress = sliderGeq25.value/GEQ_SLIDER_MIN_VALUE;
                meter25Top.progress = 0.0;
            } else {
                meter25Top.progress = 0.0;
                meter25Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq26.value != geq26Value) {
            sliderGeq26.value = geq26Value;
            if (sliderGeq26.value > 0.0) {
                meter26Top.progress = sliderGeq26.value/GEQ_SLIDER_MAX_VALUE;
                meter26Btm.progress = 0.0;
            } else if (sliderGeq26.value < 0.0) {
                meter26Btm.progress = sliderGeq26.value/GEQ_SLIDER_MIN_VALUE;
                meter26Top.progress = 0.0;
            } else {
                meter26Top.progress = 0.0;
                meter26Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq27.value != geq27Value) {
            sliderGeq27.value = geq27Value;
            if (sliderGeq27.value > 0.0) {
                meter27Top.progress = sliderGeq27.value/GEQ_SLIDER_MAX_VALUE;
                meter27Btm.progress = 0.0;
            } else if (sliderGeq27.value < 0.0) {
                meter27Btm.progress = sliderGeq27.value/GEQ_SLIDER_MIN_VALUE;
                meter27Top.progress = 0.0;
            } else {
                meter27Top.progress = 0.0;
                meter27Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq28.value != geq28Value) {
            sliderGeq28.value = geq28Value;
            if (sliderGeq28.value > 0.0) {
                meter28Top.progress = sliderGeq28.value/GEQ_SLIDER_MAX_VALUE;
                meter28Btm.progress = 0.0;
            } else if (sliderGeq28.value < 0.0) {
                meter28Btm.progress = sliderGeq28.value/GEQ_SLIDER_MIN_VALUE;
                meter28Top.progress = 0.0;
            } else {
                meter28Top.progress = 0.0;
                meter28Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq29.value != geq29Value) {
            sliderGeq29.value = geq29Value;
            if (sliderGeq29.value > 0.0) {
                meter29Top.progress = sliderGeq29.value/GEQ_SLIDER_MAX_VALUE;
                meter29Btm.progress = 0.0;
            } else if (sliderGeq29.value < 0.0) {
                meter29Btm.progress = sliderGeq29.value/GEQ_SLIDER_MIN_VALUE;
                meter29Top.progress = 0.0;
            } else {
                meter29Top.progress = 0.0;
                meter29Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq30.value != geq30Value) {
            sliderGeq30.value = geq30Value;
            if (sliderGeq30.value > 0.0) {
                meter30Top.progress = sliderGeq30.value/GEQ_SLIDER_MAX_VALUE;
                meter30Btm.progress = 0.0;
            } else if (sliderGeq30.value < 0.0) {
                meter30Btm.progress = sliderGeq30.value/GEQ_SLIDER_MIN_VALUE;
                meter30Top.progress = 0.0;
            } else {
                meter30Top.progress = 0.0;
                meter30Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq31.value != geq31Value) {
            sliderGeq31.value = geq31Value;
            if (sliderGeq31.value > 0.0) {
                meter31Top.progress = sliderGeq31.value/GEQ_SLIDER_MAX_VALUE;
                meter31Btm.progress = 0.0;
            } else if (sliderGeq31.value < 0.0) {
                meter31Btm.progress = sliderGeq31.value/GEQ_SLIDER_MIN_VALUE;
                meter31Top.progress = 0.0;
            } else {
                meter31Top.progress = 0.0;
                meter31Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        int geqEnabled = (pkt->main_l_r_ctrl & 0x80) >> 7;
        if (btnGeqOnOff.selected != geqEnabled) {
            btnGeqOnOff.selected = geqEnabled;
            peqEnabled[CHANNEL_MAIN] = btnGeqOnOff.selected;
            [self updateGeqMeters];
            needsUpdate = YES;
        }
        
        if (needsUpdate) {
            [self updatePreviewImageForChannel:CHANNEL_MAIN];
        }
    } else {
        if (peq1G[CHANNEL_MAIN] != pkt->main_l_r_eq_1_db+EQ_GAIN_OFFSET) {
            peq1G[CHANNEL_MAIN] = pkt->main_l_r_eq_1_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq1F[CHANNEL_MAIN] != pkt->main_l_r_eq_1_freq) {
            peq1F[CHANNEL_MAIN] = pkt->main_l_r_eq_1_freq;
            valueChanged = YES;
        }
        if (peq1Q[CHANNEL_MAIN] != pkt->main_l_r_eq_1_q) {
            peq1Q[CHANNEL_MAIN] = pkt->main_l_r_eq_1_q;
            if (peq1Q[CHANNEL_MAIN] == PEQ_1_LOW_SHELF) {
                peq1Filter[CHANNEL_MAIN] = EQ_LOW_SHELF;
            } else if (peq1Q[CHANNEL_MAIN] == PEQ_1_HPF) {
                peq1Filter[CHANNEL_MAIN] = EQ_LOW_CUT;
            } else {
                peq1Filter[CHANNEL_MAIN] = EQ_PEAK;
                lastPeq1Q[CHANNEL_MAIN] = peq1Q[CHANNEL_MAIN];
            }
            valueChanged = YES;
        }
        if (peq2G[CHANNEL_MAIN] != pkt->main_l_r_eq_2_db+EQ_GAIN_OFFSET) {
            peq2G[CHANNEL_MAIN] = pkt->main_l_r_eq_2_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq2F[CHANNEL_MAIN] != pkt->main_l_r_eq_2_freq) {
            peq2F[CHANNEL_MAIN] = pkt->main_l_r_eq_2_freq;
            valueChanged = YES;
        }
        if (peq2Q[CHANNEL_MAIN] != pkt->main_l_r_eq_2_q) {
            peq2Q[CHANNEL_MAIN] = pkt->main_l_r_eq_2_q;
            valueChanged = YES;
        }
        if (peq3G[CHANNEL_MAIN] != pkt->main_l_r_eq_3_db+EQ_GAIN_OFFSET) {
            peq3G[CHANNEL_MAIN] = pkt->main_l_r_eq_3_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq3F[CHANNEL_MAIN] != pkt->main_l_r_eq_3_freq) {
            peq3F[CHANNEL_MAIN] = pkt->main_l_r_eq_3_freq;
            valueChanged = YES;
        }
        if (peq3Q[CHANNEL_MAIN] != pkt->main_l_r_eq_3_q) {
            peq3Q[CHANNEL_MAIN] = pkt->main_l_r_eq_3_q;
            valueChanged = YES;
        }
        if (peq4G[CHANNEL_MAIN] != pkt->main_l_r_eq_4_db+EQ_GAIN_OFFSET) {
            peq4G[CHANNEL_MAIN] = pkt->main_l_r_eq_4_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq4F[CHANNEL_MAIN] != pkt->main_l_r_eq_4_freq) {
            peq4F[CHANNEL_MAIN] = pkt->main_l_r_eq_4_freq;
            valueChanged = YES;
        }
        if (peq4Q[CHANNEL_MAIN] != pkt->main_l_r_eq_4_q) {
            peq4Q[CHANNEL_MAIN] = pkt->main_l_r_eq_4_q;
            if (peq4Q[CHANNEL_MAIN] == PEQ_4_HIGH_SHELF) {
                peq4Filter[CHANNEL_MAIN] = EQ_HIGH_SHELF;
            } else if (peq4Q[CHANNEL_MAIN] == PEQ_4_LPF) {
                peq4Filter[CHANNEL_MAIN] = EQ_HIGH_CUT;
            } else {
                peq4Filter[CHANNEL_MAIN] = EQ_PEAK;
                lastPeq4Q[CHANNEL_MAIN] = peq4Q[CHANNEL_MAIN];
            }
            valueChanged = YES;
        }
        
        int peqEnableValue = pkt->main_l_r_ctrl;
        if (peq1Enabled[CHANNEL_MAIN] != ((peqEnableValue & 0x08) >> 3)) {
            peq1Enabled[CHANNEL_MAIN] = (peqEnableValue & 0x08) >> 3;
            valueChanged = YES;
        }
        if (peq2Enabled[CHANNEL_MAIN] != ((peqEnableValue & 0x10) >> 4)) {
            peq2Enabled[CHANNEL_MAIN] = (peqEnableValue & 0x10) >> 4;
            valueChanged = YES;
        }
        if (peq3Enabled[CHANNEL_MAIN] != ((peqEnableValue & 0x20) >> 5)) {
            peq3Enabled[CHANNEL_MAIN] = (peqEnableValue & 0x20) >> 5;
            valueChanged = YES;
        }
        if (peq4Enabled[CHANNEL_MAIN] != ((peqEnableValue & 0x40) >> 6)) {
            peq4Enabled[CHANNEL_MAIN] = (peqEnableValue & 0x40) >> 6;
            valueChanged = YES;
        }
        if (peqEnabled[CHANNEL_MAIN] != ((peqEnableValue & 0x80) >> 7)) {
            peqEnabled[CHANNEL_MAIN] = (peqEnableValue & 0x80) >> 7;
            valueChanged = YES;
        }
    }
    
    if (valueChanged) {
        [self updatePeqPlot:peqEnabled[CHANNEL_MAIN]];
        [self updatePeqForChannel:CHANNEL_MAIN];
    }
}

- (void)processChannel17_18PageReply:(ChannelCh17_18PagePacket *)pkt {
    BOOL valueChanged = NO;
    
    // CHANNEL_CH_17:
    {
        if (peq1G[CHANNEL_CH_17] != pkt->ch_17_eq_1_db+EQ_GAIN_OFFSET) {
            peq1G[CHANNEL_CH_17] = pkt->ch_17_eq_1_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq1F[CHANNEL_CH_17] != pkt->ch_17_eq_1_freq) {
            peq1F[CHANNEL_CH_17] = pkt->ch_17_eq_1_freq;
            valueChanged = YES;
        }
        if (peq1Q[CHANNEL_CH_17] != pkt->ch_17_eq_1_q) {
            peq1Q[CHANNEL_CH_17] = pkt->ch_17_eq_1_q;
            if (peq1Q[CHANNEL_CH_17] == PEQ_1_LOW_SHELF) {
                [btnPeqFilterFront setImage:[UIImage imageNamed:@"frame-12.png"] forState:UIControlStateNormal];
                peq1Filter[CHANNEL_CH_17] = EQ_LOW_SHELF;
            } else if (peq1Q[CHANNEL_CH_17] == PEQ_1_HPF) {
                [btnPeqFilterFront setImage:[UIImage imageNamed:@"frame-13.png"] forState:UIControlStateNormal];
                peq1Filter[CHANNEL_CH_17] = EQ_LOW_CUT;
            } else {
                [btnPeqFilterFront setImage:[UIImage imageNamed:@"frame-11.png"] forState:UIControlStateNormal];
                peq1Filter[CHANNEL_CH_17] = EQ_PEAK;
                lastPeq1Q[CHANNEL_CH_17] = peq1Q[CHANNEL_CH_17];
            }
            valueChanged = YES;
        }
        if (peq2G[CHANNEL_CH_17] != pkt->ch_17_eq_2_db+EQ_GAIN_OFFSET) {
            peq2G[CHANNEL_CH_17] = pkt->ch_17_eq_2_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq2F[CHANNEL_CH_17] != pkt->ch_17_eq_2_freq) {
            peq2F[CHANNEL_CH_17] = pkt->ch_17_eq_2_freq;
            valueChanged = YES;
        }
        if (peq2Q[CHANNEL_CH_17] != pkt->ch_17_eq_2_q) {
            peq2Q[CHANNEL_CH_17] = pkt->ch_17_eq_2_q;
            valueChanged = YES;
        }
        if (peq3G[CHANNEL_CH_17] != pkt->ch_17_eq_3_db+EQ_GAIN_OFFSET) {
            peq3G[CHANNEL_CH_17] = pkt->ch_17_eq_3_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq3F[CHANNEL_CH_17] != pkt->ch_17_eq_3_freq) {
            peq3F[CHANNEL_CH_17] = pkt->ch_17_eq_3_freq;
            valueChanged = YES;
        }
        if (peq3Q[CHANNEL_CH_17] != pkt->ch_17_eq_3_q) {
            peq3Q[CHANNEL_CH_17] = pkt->ch_17_eq_3_q;
            valueChanged = YES;
        }
        if (peq4G[CHANNEL_CH_17] != pkt->ch_17_eq_4_db+EQ_GAIN_OFFSET) {
            peq4G[CHANNEL_CH_17] = pkt->ch_17_eq_4_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq4F[CHANNEL_CH_17] != pkt->ch_17_eq_4_freq) {
            peq4F[CHANNEL_CH_17] = pkt->ch_17_eq_4_freq;
            valueChanged = YES;
        }
        if (peq4Q[CHANNEL_CH_17] != pkt->ch_17_eq_4_q) {
            peq4Q[CHANNEL_CH_17] = pkt->ch_17_eq_4_q;
            if (peq4Q[CHANNEL_CH_17] == PEQ_4_HIGH_SHELF) {
                [btnPeqFilterBack setImage:[UIImage imageNamed:@"frame-15.png"] forState:UIControlStateNormal];
                peq4Filter[CHANNEL_CH_17] = EQ_HIGH_SHELF;
            } else if (peq4Q[CHANNEL_CH_17] == PEQ_4_LPF) {
                [btnPeqFilterBack setImage:[UIImage imageNamed:@"frame-14.png"] forState:UIControlStateNormal];
                peq4Filter[CHANNEL_CH_17] = EQ_HIGH_CUT;
            } else {
                [btnPeqFilterBack setImage:[UIImage imageNamed:@"frame-11.png"] forState:UIControlStateNormal];
                peq4Filter[CHANNEL_CH_17] = EQ_PEAK;
                lastPeq4Q[CHANNEL_CH_17] = peq4Q[CHANNEL_CH_17];
            }
            valueChanged = YES;
        }
        int peqEnableValue = pkt->ch_17_ctrl;
        if (peq1Enabled[CHANNEL_CH_17] != ((peqEnableValue & 0x08) >> 3)) {
            peq1Enabled[CHANNEL_CH_17] = (peqEnableValue & 0x08) >> 3;
            valueChanged = YES;
        }
        if (peq2Enabled[CHANNEL_CH_17] != ((peqEnableValue & 0x10) >> 4)) {
            peq2Enabled[CHANNEL_CH_17] = (peqEnableValue & 0x10) >> 4;
            valueChanged = YES;
        }
        if (peq3Enabled[CHANNEL_CH_17] != ((peqEnableValue & 0x20) >> 5)) {
            peq3Enabled[CHANNEL_CH_17] = (peqEnableValue & 0x20) >> 5;
            valueChanged = YES;
        }
        if (peq4Enabled[CHANNEL_CH_17] != ((peqEnableValue & 0x40) >> 6)) {
            peq4Enabled[CHANNEL_CH_17] = (peqEnableValue & 0x40) >> 6;
            valueChanged = YES;
        }
        if (peqEnabled[CHANNEL_CH_17] != ((peqEnableValue & 0x80) >> 7)) {
            peqEnabled[CHANNEL_CH_17] = (peqEnableValue & 0x80) >> 7;
            valueChanged = YES;
        }
    }
    
    if (valueChanged) {
        [self updatePeqPlot:peqEnabled[CHANNEL_CH_17]];
        [self updatePeqForChannel:CHANNEL_CH_17];
        valueChanged = NO;
    }
    
    
    // CHANNEL_CH_18:
    {
        if (peq1G[CHANNEL_CH_18] != pkt->ch_18_eq_1_db+EQ_GAIN_OFFSET) {
            peq1G[CHANNEL_CH_18] = pkt->ch_18_eq_1_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq1F[CHANNEL_CH_18] != pkt->ch_18_eq_1_freq) {
            peq1F[CHANNEL_CH_18] = pkt->ch_18_eq_1_freq;
            valueChanged = YES;
        }
        if (peq1Q[CHANNEL_CH_18] != pkt->ch_18_eq_1_q) {
            peq1Q[CHANNEL_CH_18] = pkt->ch_18_eq_1_q;
            if (peq1Q[CHANNEL_CH_18] == PEQ_1_LOW_SHELF) {
                [btnPeqFilterFront setImage:[UIImage imageNamed:@"frame-12.png"] forState:UIControlStateNormal];
                peq1Filter[CHANNEL_CH_18] = EQ_LOW_SHELF;
            } else if (peq1Q[CHANNEL_CH_18] == PEQ_1_HPF) {
                [btnPeqFilterFront setImage:[UIImage imageNamed:@"frame-13.png"] forState:UIControlStateNormal];
                peq1Filter[CHANNEL_CH_18] = EQ_LOW_CUT;
            } else {
                [btnPeqFilterFront setImage:[UIImage imageNamed:@"frame-11.png"] forState:UIControlStateNormal];
                peq1Filter[CHANNEL_CH_18] = EQ_PEAK;
                lastPeq1Q[CHANNEL_CH_18] = peq1Q[CHANNEL_CH_18];
            }
            valueChanged = YES;
        }
        if (peq2G[CHANNEL_CH_18] != pkt->ch_18_eq_2_db+EQ_GAIN_OFFSET) {
            peq2G[CHANNEL_CH_18] = pkt->ch_18_eq_2_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq2F[CHANNEL_CH_18] != pkt->ch_18_eq_2_freq) {
            peq2F[CHANNEL_CH_18] = pkt->ch_18_eq_2_freq;
            valueChanged = YES;
        }
        if (peq2Q[CHANNEL_CH_18] != pkt->ch_18_eq_2_q) {
            peq2Q[CHANNEL_CH_18] = pkt->ch_18_eq_2_q;
            valueChanged = YES;
        }
        if (peq3G[CHANNEL_CH_18] != pkt->ch_18_eq_3_db+EQ_GAIN_OFFSET) {
            peq3G[CHANNEL_CH_18] = pkt->ch_18_eq_3_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq3F[CHANNEL_CH_18] != pkt->ch_18_eq_3_freq) {
            peq3F[CHANNEL_CH_18] = pkt->ch_18_eq_3_freq;
            valueChanged = YES;
        }
        if (peq3Q[CHANNEL_CH_18] != pkt->ch_18_eq_3_q) {
            peq3Q[CHANNEL_CH_18] = pkt->ch_18_eq_3_q;
            valueChanged = YES;
        }
        if (peq4G[CHANNEL_CH_18] != pkt->ch_18_eq_4_db+EQ_GAIN_OFFSET) {
            peq4G[CHANNEL_CH_18] = pkt->ch_18_eq_4_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq4F[CHANNEL_CH_18] != pkt->ch_18_eq_4_freq) {
            peq4F[CHANNEL_CH_18] = pkt->ch_18_eq_4_freq;
            valueChanged = YES;
        }
        if (peq4Q[CHANNEL_CH_18] != pkt->ch_18_eq_4_q) {
            peq4Q[CHANNEL_CH_18] = pkt->ch_18_eq_4_q;
            if (peq4Q[CHANNEL_CH_18] == PEQ_4_HIGH_SHELF) {
                [btnPeqFilterBack setImage:[UIImage imageNamed:@"frame-15.png"] forState:UIControlStateNormal];
                peq4Filter[CHANNEL_CH_18] = EQ_HIGH_SHELF;
            } else if (peq4Q[CHANNEL_CH_18] == PEQ_4_LPF) {
                [btnPeqFilterBack setImage:[UIImage imageNamed:@"frame-14.png"] forState:UIControlStateNormal];
                peq4Filter[CHANNEL_CH_18] = EQ_HIGH_CUT;
            } else {
                [btnPeqFilterBack setImage:[UIImage imageNamed:@"frame-11.png"] forState:UIControlStateNormal];
                peq4Filter[CHANNEL_CH_18] = EQ_PEAK;
                lastPeq4Q[CHANNEL_CH_18] = peq4Q[CHANNEL_CH_18];
            }
            valueChanged = YES;
        }
        int peqEnableValue = pkt->ch_18_ctrl;
        if (peq1Enabled[CHANNEL_CH_18] != ((peqEnableValue & 0x08) >> 3)) {
            peq1Enabled[CHANNEL_CH_18] = (peqEnableValue & 0x08) >> 3;
            valueChanged = YES;
        }
        if (peq2Enabled[CHANNEL_CH_18] != ((peqEnableValue & 0x10) >> 4)) {
            peq2Enabled[CHANNEL_CH_18] = (peqEnableValue & 0x10) >> 4;
            valueChanged = YES;
        }
        if (peq3Enabled[CHANNEL_CH_18] != ((peqEnableValue & 0x20) >> 5)) {
            peq3Enabled[CHANNEL_CH_18] = (peqEnableValue & 0x20) >> 5;
            valueChanged = YES;
        }
        if (peq4Enabled[CHANNEL_CH_18] != ((peqEnableValue & 0x40) >> 6)) {
            peq4Enabled[CHANNEL_CH_18] = (peqEnableValue & 0x40) >> 6;
            valueChanged = YES;
        }
        if (peqEnabled[CHANNEL_CH_18] != ((peqEnableValue & 0x80) >> 7)) {
            peqEnabled[CHANNEL_CH_18] = (peqEnableValue & 0x80) >> 7;
            valueChanged = YES;
        }
    }
    
    if (valueChanged) {
        [self updatePeqPlot:peqEnabled[CHANNEL_CH_18]];
        [self updatePeqForChannel:CHANNEL_CH_18];
        valueChanged = NO;
    }
    
    // CHANNEL_MAIN
    if (mainEqType == MAIN_EQ_GEQ) {
        BOOL needsUpdate = NO;
        
        // Update GEQ sliders
        int geq1Value = pkt->geq_20_hz_db;
        int geq2Value = pkt->geq_25_hz_db;
        int geq3Value = pkt->geq_31_5_hz_db;
        int geq4Value = pkt->geq_40_hz_db;
        int geq5Value = pkt->geq_50_hz_db;
        int geq6Value = pkt->geq_63_hz_db;
        int geq7Value = pkt->geq_80_hz_db;
        int geq8Value = pkt->geq_100_hz_db;
        int geq9Value = pkt->geq_125_hz_db;
        int geq10Value = pkt->geq_160_hz_db;
        int geq11Value = pkt->geq_200_hz_db;
        int geq12Value = pkt->geq_250_hz_db;
        int geq13Value = pkt->geq_315_hz_db;
        int geq14Value = pkt->geq_400_hz_db;
        int geq15Value = pkt->geq_500_hz_db;
        int geq16Value = pkt->geq_630_hz_db;
        int geq17Value = pkt->geq_800_hz_db;
        int geq18Value = pkt->geq_1_khz_db;
        int geq19Value = pkt->geq_1_25_khz_db;
        int geq20Value = pkt->geq_1_6_khz_db;
        int geq21Value = pkt->geq_2_khz_db;
        int geq22Value = pkt->geq_2_5_khz_db;
        int geq23Value = pkt->geq_3_15_khz_db;
        int geq24Value = pkt->geq_4_khz_db;
        int geq25Value = pkt->geq_5_khz_db;
        int geq26Value = pkt->geq_6_3_khz_db;
        int geq27Value = pkt->geq_8_khz_db;
        int geq28Value = pkt->geq_10_khz_db;
        int geq29Value = pkt->geq_12_5_khz_db;
        int geq30Value = pkt->geq_16_khz_db;
        int geq31Value = pkt->geq_20_khz_db;
        
        if (sliderGeq1.value != geq1Value) {
            sliderGeq1.value = geq1Value;
            if (sliderGeq1.value > 0.0) {
                meter1Top.progress = sliderGeq1.value/GEQ_SLIDER_MAX_VALUE;
                meter1Btm.progress = 0.0;
            } else if (sliderGeq2.value < 0.0) {
                meter1Btm.progress = sliderGeq1.value/GEQ_SLIDER_MIN_VALUE;
                meter1Top.progress = 0.0;
            } else {
                meter1Top.progress = 0.0;
                meter1Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq2.value != geq2Value) {
            sliderGeq2.value = geq2Value;
            if (sliderGeq2.value > 0.0) {
                meter2Top.progress = sliderGeq2.value/GEQ_SLIDER_MAX_VALUE;
                meter2Btm.progress = 0.0;
            } else if (sliderGeq2.value < 0.0) {
                meter2Btm.progress = sliderGeq2.value/GEQ_SLIDER_MIN_VALUE;
                meter2Top.progress = 0.0;
            } else {
                meter2Top.progress = 0.0;
                meter2Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq3.value != geq3Value) {
            sliderGeq3.value = geq3Value;
            if (sliderGeq3.value > 0.0) {
                meter3Top.progress = sliderGeq3.value/GEQ_SLIDER_MAX_VALUE;
                meter3Btm.progress = 0.0;
            } else if (sliderGeq3.value < 0.0) {
                meter3Btm.progress = sliderGeq3.value/GEQ_SLIDER_MIN_VALUE;
                meter3Top.progress = 0.0;
            } else {
                meter3Top.progress = 0.0;
                meter3Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq4.value != geq4Value) {
            sliderGeq4.value = geq4Value;
            if (sliderGeq4.value > 0.0) {
                meter4Top.progress = sliderGeq4.value/GEQ_SLIDER_MAX_VALUE;
                meter4Btm.progress = 0.0;
            } else if (sliderGeq4.value < 0.0) {
                meter4Btm.progress = sliderGeq4.value/GEQ_SLIDER_MIN_VALUE;
                meter4Top.progress = 0.0;
            } else {
                meter4Top.progress = 0.0;
                meter4Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq5.value != geq5Value) {
            sliderGeq5.value = geq5Value;
            if (sliderGeq5.value > 0.0) {
                meter5Top.progress = sliderGeq5.value/GEQ_SLIDER_MAX_VALUE;
                meter5Btm.progress = 0.0;
            } else if (sliderGeq5.value < 0.0) {
                meter5Btm.progress = sliderGeq5.value/GEQ_SLIDER_MIN_VALUE;
                meter5Top.progress = 0.0;
            } else {
                meter5Top.progress = 0.0;
                meter5Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq6.value != geq6Value) {
            sliderGeq6.value = geq6Value;
            if (sliderGeq6.value > 0.0) {
                meter6Top.progress = sliderGeq6.value/GEQ_SLIDER_MAX_VALUE;
                meter6Btm.progress = 0.0;
            } else if (sliderGeq6.value < 0.0) {
                meter6Btm.progress = sliderGeq6.value/GEQ_SLIDER_MIN_VALUE;
                meter6Top.progress = 0.0;
            } else {
                meter6Top.progress = 0.0;
                meter6Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq7.value != geq7Value) {
            sliderGeq7.value = geq7Value;
            if (sliderGeq7.value > 0.0) {
                meter7Top.progress = sliderGeq7.value/GEQ_SLIDER_MAX_VALUE;
                meter7Btm.progress = 0.0;
            } else if (sliderGeq7.value < 0.0) {
                meter7Btm.progress = sliderGeq7.value/GEQ_SLIDER_MIN_VALUE;
                meter7Top.progress = 0.0;
            } else {
                meter7Top.progress = 0.0;
                meter7Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq8.value != geq8Value) {
            sliderGeq8.value = geq8Value;
            if (sliderGeq8.value > 0.0) {
                meter8Top.progress = sliderGeq8.value/GEQ_SLIDER_MAX_VALUE;
                meter8Btm.progress = 0.0;
            } else if (sliderGeq8.value < 0.0) {
                meter8Btm.progress = sliderGeq8.value/GEQ_SLIDER_MIN_VALUE;
                meter8Top.progress = 0.0;
            } else {
                meter8Top.progress = 0.0;
                meter8Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq9.value != geq9Value) {
            sliderGeq9.value = geq9Value;
            if (sliderGeq9.value > 0.0) {
                meter9Top.progress = sliderGeq9.value/GEQ_SLIDER_MAX_VALUE;
                meter9Btm.progress = 0.0;
            } else if (sliderGeq9.value < 0.0) {
                meter9Btm.progress = sliderGeq9.value/GEQ_SLIDER_MIN_VALUE;
                meter9Top.progress = 0.0;
            } else {
                meter9Top.progress = 0.0;
                meter9Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq10.value != geq10Value) {
            sliderGeq10.value = geq10Value;
            if (sliderGeq10.value > 0.0) {
                meter10Top.progress = sliderGeq10.value/GEQ_SLIDER_MAX_VALUE;
                meter10Btm.progress = 0.0;
            } else if (sliderGeq10.value < 0.0) {
                meter10Btm.progress = sliderGeq10.value/GEQ_SLIDER_MIN_VALUE;
                meter10Top.progress = 0.0;
            } else {
                meter10Top.progress = 0.0;
                meter10Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq11.value != geq11Value) {
            sliderGeq11.value = geq11Value;
            if (sliderGeq11.value > 0.0) {
                meter11Top.progress = sliderGeq11.value/GEQ_SLIDER_MAX_VALUE;
                meter11Btm.progress = 0.0;
            } else if (sliderGeq11.value < 0.0) {
                meter11Btm.progress = sliderGeq11.value/GEQ_SLIDER_MIN_VALUE;
                meter1Top.progress = 0.0;
            } else {
                meter11Top.progress = 0.0;
                meter11Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq12.value != geq12Value) {
            sliderGeq12.value = geq12Value;
            if (sliderGeq12.value > 0.0) {
                meter12Top.progress = sliderGeq12.value/GEQ_SLIDER_MAX_VALUE;
                meter12Btm.progress = 0.0;
            } else if (sliderGeq12.value < 0.0) {
                meter12Btm.progress = sliderGeq12.value/GEQ_SLIDER_MIN_VALUE;
                meter12Top.progress = 0.0;
            } else {
                meter12Top.progress = 0.0;
                meter12Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq13.value != geq13Value) {
            sliderGeq13.value = geq13Value;
            if (sliderGeq13.value > 0.0) {
                meter13Top.progress = sliderGeq13.value/GEQ_SLIDER_MAX_VALUE;
                meter13Btm.progress = 0.0;
            } else if (sliderGeq13.value < 0.0) {
                meter13Btm.progress = sliderGeq13.value/GEQ_SLIDER_MIN_VALUE;
                meter13Top.progress = 0.0;
            } else {
                meter13Top.progress = 0.0;
                meter13Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq14.value != geq14Value) {
            sliderGeq14.value = geq14Value;
            if (sliderGeq14.value > 0.0) {
                meter14Top.progress = sliderGeq14.value/GEQ_SLIDER_MAX_VALUE;
                meter14Btm.progress = 0.0;
            } else if (sliderGeq14.value < 0.0) {
                meter14Btm.progress = sliderGeq14.value/GEQ_SLIDER_MIN_VALUE;
                meter14Top.progress = 0.0;
            } else {
                meter14Top.progress = 0.0;
                meter14Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq15.value != geq15Value) {
            sliderGeq15.value = geq15Value;
            if (sliderGeq15.value > 0.0) {
                meter15Top.progress = sliderGeq15.value/GEQ_SLIDER_MAX_VALUE;
                meter15Btm.progress = 0.0;
            } else if (sliderGeq15.value < 0.0) {
                meter15Btm.progress = sliderGeq15.value/GEQ_SLIDER_MIN_VALUE;
                meter15Top.progress = 0.0;
            } else {
                meter15Top.progress = 0.0;
                meter15Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq16.value != geq16Value) {
            sliderGeq16.value = geq16Value;
            if (sliderGeq16.value > 0.0) {
                meter16Top.progress = sliderGeq16.value/GEQ_SLIDER_MAX_VALUE;
                meter16Btm.progress = 0.0;
            } else if (sliderGeq16.value < 0.0) {
                meter16Btm.progress = sliderGeq16.value/GEQ_SLIDER_MIN_VALUE;
                meter16Top.progress = 0.0;
            } else {
                meter16Top.progress = 0.0;
                meter16Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq17.value != geq17Value) {
            sliderGeq17.value = geq17Value;
            if (sliderGeq17.value > 0.0) {
                meter17Top.progress = sliderGeq17.value/GEQ_SLIDER_MAX_VALUE;
                meter17Btm.progress = 0.0;
            } else if (sliderGeq17.value < 0.0) {
                meter17Btm.progress = sliderGeq17.value/GEQ_SLIDER_MIN_VALUE;
                meter17Top.progress = 0.0;
            } else {
                meter17Top.progress = 0.0;
                meter17Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq18.value != geq18Value) {
            sliderGeq18.value = geq18Value;
            if (sliderGeq18.value > 0.0) {
                meter18Top.progress = sliderGeq18.value/GEQ_SLIDER_MAX_VALUE;
                meter18Btm.progress = 0.0;
            } else if (sliderGeq18.value < 0.0) {
                meter18Btm.progress = sliderGeq18.value/GEQ_SLIDER_MIN_VALUE;
                meter18Top.progress = 0.0;
            } else {
                meter18Top.progress = 0.0;
                meter18Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq19.value != geq19Value) {
            sliderGeq19.value = geq19Value;
            if (sliderGeq19.value > 0.0) {
                meter19Top.progress = sliderGeq19.value/GEQ_SLIDER_MAX_VALUE;
                meter19Btm.progress = 0.0;
            } else if (sliderGeq19.value < 0.0) {
                meter19Btm.progress = sliderGeq19.value/GEQ_SLIDER_MIN_VALUE;
                meter19Top.progress = 0.0;
            } else {
                meter19Top.progress = 0.0;
                meter19Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq20.value != geq20Value) {
            sliderGeq20.value = geq20Value;
            if (sliderGeq20.value > 0.0) {
                meter20Top.progress = sliderGeq20.value/GEQ_SLIDER_MAX_VALUE;
                meter20Btm.progress = 0.0;
            } else if (sliderGeq20.value < 0.0) {
                meter20Btm.progress = sliderGeq20.value/GEQ_SLIDER_MIN_VALUE;
                meter20Top.progress = 0.0;
            } else {
                meter20Top.progress = 0.0;
                meter20Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq21.value != geq21Value) {
            sliderGeq21.value = geq21Value;
            if (sliderGeq21.value > 0.0) {
                meter21Top.progress = sliderGeq21.value/GEQ_SLIDER_MAX_VALUE;
                meter21Btm.progress = 0.0;
            } else if (sliderGeq21.value < 0.0) {
                meter21Btm.progress = sliderGeq21.value/GEQ_SLIDER_MIN_VALUE;
                meter21Top.progress = 0.0;
            } else {
                meter21Top.progress = 0.0;
                meter21Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq22.value != geq22Value) {
            sliderGeq22.value = geq22Value;
            if (sliderGeq22.value > 0.0) {
                meter22Top.progress = sliderGeq22.value/GEQ_SLIDER_MAX_VALUE;
                meter22Btm.progress = 0.0;
            } else if (sliderGeq22.value < 0.0) {
                meter22Btm.progress = sliderGeq22.value/GEQ_SLIDER_MIN_VALUE;
                meter22Top.progress = 0.0;
            } else {
                meter22Top.progress = 0.0;
                meter22Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq23.value != geq23Value) {
            sliderGeq23.value = geq23Value;
            if (sliderGeq23.value > 0.0) {
                meter23Top.progress = sliderGeq23.value/GEQ_SLIDER_MAX_VALUE;
                meter23Btm.progress = 0.0;
            } else if (sliderGeq23.value < 0.0) {
                meter23Btm.progress = sliderGeq23.value/GEQ_SLIDER_MIN_VALUE;
                meter23Top.progress = 0.0;
            } else {
                meter23Top.progress = 0.0;
                meter23Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq24.value != geq24Value) {
            sliderGeq24.value = geq24Value;
            if (sliderGeq24.value > 0.0) {
                meter24Top.progress = sliderGeq24.value/GEQ_SLIDER_MAX_VALUE;
                meter24Btm.progress = 0.0;
            } else if (sliderGeq24.value < 0.0) {
                meter24Btm.progress = sliderGeq24.value/GEQ_SLIDER_MIN_VALUE;
                meter24Top.progress = 0.0;
            } else {
                meter24Top.progress = 0.0;
                meter24Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq25.value != geq25Value) {
            sliderGeq25.value = geq25Value;
            if (sliderGeq25.value > 0.0) {
                meter25Top.progress = sliderGeq25.value/GEQ_SLIDER_MAX_VALUE;
                meter25Btm.progress = 0.0;
            } else if (sliderGeq25.value < 0.0) {
                meter25Btm.progress = sliderGeq25.value/GEQ_SLIDER_MIN_VALUE;
                meter25Top.progress = 0.0;
            } else {
                meter25Top.progress = 0.0;
                meter25Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq26.value != geq26Value) {
            sliderGeq26.value = geq26Value;
            if (sliderGeq26.value > 0.0) {
                meter26Top.progress = sliderGeq26.value/GEQ_SLIDER_MAX_VALUE;
                meter26Btm.progress = 0.0;
            } else if (sliderGeq26.value < 0.0) {
                meter26Btm.progress = sliderGeq26.value/GEQ_SLIDER_MIN_VALUE;
                meter26Top.progress = 0.0;
            } else {
                meter26Top.progress = 0.0;
                meter26Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq27.value != geq27Value) {
            sliderGeq27.value = geq27Value;
            if (sliderGeq27.value > 0.0) {
                meter27Top.progress = sliderGeq27.value/GEQ_SLIDER_MAX_VALUE;
                meter27Btm.progress = 0.0;
            } else if (sliderGeq27.value < 0.0) {
                meter27Btm.progress = sliderGeq27.value/GEQ_SLIDER_MIN_VALUE;
                meter27Top.progress = 0.0;
            } else {
                meter27Top.progress = 0.0;
                meter27Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq28.value != geq28Value) {
            sliderGeq28.value = geq28Value;
            if (sliderGeq28.value > 0.0) {
                meter28Top.progress = sliderGeq28.value/GEQ_SLIDER_MAX_VALUE;
                meter28Btm.progress = 0.0;
            } else if (sliderGeq28.value < 0.0) {
                meter28Btm.progress = sliderGeq28.value/GEQ_SLIDER_MIN_VALUE;
                meter28Top.progress = 0.0;
            } else {
                meter28Top.progress = 0.0;
                meter28Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq29.value != geq29Value) {
            sliderGeq29.value = geq29Value;
            if (sliderGeq29.value > 0.0) {
                meter29Top.progress = sliderGeq29.value/GEQ_SLIDER_MAX_VALUE;
                meter29Btm.progress = 0.0;
            } else if (sliderGeq29.value < 0.0) {
                meter29Btm.progress = sliderGeq29.value/GEQ_SLIDER_MIN_VALUE;
                meter29Top.progress = 0.0;
            } else {
                meter29Top.progress = 0.0;
                meter29Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq30.value != geq30Value) {
            sliderGeq30.value = geq30Value;
            if (sliderGeq30.value > 0.0) {
                meter30Top.progress = sliderGeq30.value/GEQ_SLIDER_MAX_VALUE;
                meter30Btm.progress = 0.0;
            } else if (sliderGeq30.value < 0.0) {
                meter30Btm.progress = sliderGeq30.value/GEQ_SLIDER_MIN_VALUE;
                meter30Top.progress = 0.0;
            } else {
                meter30Top.progress = 0.0;
                meter30Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq31.value != geq31Value) {
            sliderGeq31.value = geq31Value;
            if (sliderGeq31.value > 0.0) {
                meter31Top.progress = sliderGeq31.value/GEQ_SLIDER_MAX_VALUE;
                meter31Btm.progress = 0.0;
            } else if (sliderGeq31.value < 0.0) {
                meter31Btm.progress = sliderGeq31.value/GEQ_SLIDER_MIN_VALUE;
                meter31Top.progress = 0.0;
            } else {
                meter31Top.progress = 0.0;
                meter31Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        int geqEnabled = (pkt->main_l_r_ctrl & 0x80) >> 7;
        if (btnGeqOnOff.selected != geqEnabled) {
            btnGeqOnOff.selected = geqEnabled;
            peqEnabled[CHANNEL_MAIN] = btnGeqOnOff.selected;
            [self updateGeqMeters];
            needsUpdate = YES;
        }
        
        if (needsUpdate) {
            [self updatePreviewImageForChannel:CHANNEL_MAIN];
        }
    } else {
        if (peq1G[CHANNEL_MAIN] != pkt->main_l_r_eq_1_db+EQ_GAIN_OFFSET) {
            peq1G[CHANNEL_MAIN] = pkt->main_l_r_eq_1_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq1F[CHANNEL_MAIN] != pkt->main_l_r_eq_1_freq) {
            peq1F[CHANNEL_MAIN] = pkt->main_l_r_eq_1_freq;
            valueChanged = YES;
        }
        if (peq1Q[CHANNEL_MAIN] != pkt->main_l_r_eq_1_q) {
            peq1Q[CHANNEL_MAIN] = pkt->main_l_r_eq_1_q;
            if (peq1Q[CHANNEL_MAIN] == PEQ_1_LOW_SHELF) {
                peq1Filter[CHANNEL_MAIN] = EQ_LOW_SHELF;
            } else if (peq1Q[CHANNEL_MAIN] == PEQ_1_HPF) {
                peq1Filter[CHANNEL_MAIN] = EQ_LOW_CUT;
            } else {
                peq1Filter[CHANNEL_MAIN] = EQ_PEAK;
                lastPeq1Q[CHANNEL_MAIN] = peq1Q[CHANNEL_MAIN];
            }
            valueChanged = YES;
        }
        if (peq2G[CHANNEL_MAIN] != pkt->main_l_r_eq_2_db+EQ_GAIN_OFFSET) {
            peq2G[CHANNEL_MAIN] = pkt->main_l_r_eq_2_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq2F[CHANNEL_MAIN] != pkt->main_l_r_eq_2_freq) {
            peq2F[CHANNEL_MAIN] = pkt->main_l_r_eq_2_freq;
            valueChanged = YES;
        }
        if (peq2Q[CHANNEL_MAIN] != pkt->main_l_r_eq_2_q) {
            peq2Q[CHANNEL_MAIN] = pkt->main_l_r_eq_2_q;
            valueChanged = YES;
        }
        if (peq3G[CHANNEL_MAIN] != pkt->main_l_r_eq_3_db+EQ_GAIN_OFFSET) {
            peq3G[CHANNEL_MAIN] = pkt->main_l_r_eq_3_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq3F[CHANNEL_MAIN] != pkt->main_l_r_eq_3_freq) {
            peq3F[CHANNEL_MAIN] = pkt->main_l_r_eq_3_freq;
            valueChanged = YES;
        }
        if (peq3Q[CHANNEL_MAIN] != pkt->main_l_r_eq_3_q) {
            peq3Q[CHANNEL_MAIN] = pkt->main_l_r_eq_3_q;
            valueChanged = YES;
        }
        if (peq4G[CHANNEL_MAIN] != pkt->main_l_r_eq_4_db+EQ_GAIN_OFFSET) {
            peq4G[CHANNEL_MAIN] = pkt->main_l_r_eq_4_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq4F[CHANNEL_MAIN] != pkt->main_l_r_eq_4_freq) {
            peq4F[CHANNEL_MAIN] = pkt->main_l_r_eq_4_freq;
            valueChanged = YES;
        }
        if (peq4Q[CHANNEL_MAIN] != pkt->main_l_r_eq_4_q) {
            peq4Q[CHANNEL_MAIN] = pkt->main_l_r_eq_4_q;
            if (peq4Q[CHANNEL_MAIN] == PEQ_4_HIGH_SHELF) {
                peq4Filter[CHANNEL_MAIN] = EQ_HIGH_SHELF;
            } else if (peq4Q[CHANNEL_MAIN] == PEQ_4_LPF) {
                peq4Filter[CHANNEL_MAIN] = EQ_HIGH_CUT;
            } else {
                peq4Filter[CHANNEL_MAIN] = EQ_PEAK;
                lastPeq4Q[CHANNEL_MAIN] = peq4Q[CHANNEL_MAIN];
            }
            valueChanged = YES;
        }
        
        int peqEnableValue = pkt->main_l_r_ctrl;
        if (peq1Enabled[CHANNEL_MAIN] != ((peqEnableValue & 0x08) >> 3)) {
            peq1Enabled[CHANNEL_MAIN] = (peqEnableValue & 0x08) >> 3;
            valueChanged = YES;
        }
        if (peq2Enabled[CHANNEL_MAIN] != ((peqEnableValue & 0x10) >> 4)) {
            peq2Enabled[CHANNEL_MAIN] = (peqEnableValue & 0x10) >> 4;
            valueChanged = YES;
        }
        if (peq3Enabled[CHANNEL_MAIN] != ((peqEnableValue & 0x20) >> 5)) {
            peq3Enabled[CHANNEL_MAIN] = (peqEnableValue & 0x20) >> 5;
            valueChanged = YES;
        }
        if (peq4Enabled[CHANNEL_MAIN] != ((peqEnableValue & 0x40) >> 6)) {
            peq4Enabled[CHANNEL_MAIN] = (peqEnableValue & 0x40) >> 6;
            valueChanged = YES;
        }
        if (peqEnabled[CHANNEL_MAIN] != ((peqEnableValue & 0x80) >> 7)) {
            peqEnabled[CHANNEL_MAIN] = (peqEnableValue & 0x80) >> 7;
            valueChanged = YES;
        }
    }
    
    if (valueChanged) {
        [self updatePeqPlot:peqEnabled[CHANNEL_MAIN]];
        [self updatePeqForChannel:CHANNEL_MAIN];
    }
}

- (void)processMulti1_4PageReply:(ChannelMulti1_4PagePacket *)pkt {
    BOOL valueChanged = NO;
    
    // CHANNEL_MULTI_1
    {
        if (peq1G[CHANNEL_MULTI_1] != pkt->multi_1_eq_1_db+EQ_GAIN_OFFSET) {
            peq1G[CHANNEL_MULTI_1] = pkt->multi_1_eq_1_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq1F[CHANNEL_MULTI_1] != pkt->multi_1_eq_1_freq) {
            peq1F[CHANNEL_MULTI_1] = pkt->multi_1_eq_1_freq;
            valueChanged = YES;
        }
        if (peq1Q[CHANNEL_MULTI_1] != pkt->multi_1_eq_1_q) {
            peq1Q[CHANNEL_MULTI_1] = pkt->multi_1_eq_1_q;
            if (peq1Q[CHANNEL_MULTI_1] == PEQ_1_LOW_SHELF) {
                peq1Filter[CHANNEL_MULTI_1] = EQ_LOW_SHELF;
            } else if (peq1Q[CHANNEL_MULTI_1] == PEQ_1_HPF) {
                peq1Filter[CHANNEL_MULTI_1] = EQ_LOW_CUT;
            } else {
                peq1Filter[CHANNEL_MULTI_1] = EQ_PEAK;
                lastPeq1Q[CHANNEL_MULTI_1] = peq1Q[CHANNEL_MULTI_1];
            }
            valueChanged = YES;
        }
        if (peq2G[CHANNEL_MULTI_1] != pkt->multi_1_eq_2_db+EQ_GAIN_OFFSET) {
            peq2G[CHANNEL_MULTI_1] = pkt->multi_1_eq_2_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq2F[CHANNEL_MULTI_1] != pkt->multi_1_eq_2_freq) {
            peq2F[CHANNEL_MULTI_1] = pkt->multi_1_eq_2_freq;
            valueChanged = YES;
        }
        if (peq2Q[CHANNEL_MULTI_1] != pkt->multi_1_eq_2_q) {
            peq2Q[CHANNEL_MULTI_1] = pkt->multi_1_eq_2_q;
            valueChanged = YES;
        }
        if (peq3G[CHANNEL_MULTI_1] != pkt->multi_1_eq_3_db+EQ_GAIN_OFFSET) {
            peq3G[CHANNEL_MULTI_1] = pkt->multi_1_eq_3_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq3F[CHANNEL_MULTI_1] != pkt->multi_1_eq_3_freq) {
            peq3F[CHANNEL_MULTI_1] = pkt->multi_1_eq_3_freq;
            valueChanged = YES;
        }
        if (peq3Q[CHANNEL_MULTI_1] != pkt->multi_1_eq_3_q) {
            peq3Q[CHANNEL_MULTI_1] = pkt->multi_1_eq_3_q;
            valueChanged = YES;
        }
        if (peq4G[CHANNEL_MULTI_1] != pkt->multi_1_eq_4_db+EQ_GAIN_OFFSET) {
            peq4G[CHANNEL_MULTI_1] = pkt->multi_1_eq_4_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq4F[CHANNEL_MULTI_1] != pkt->multi_1_eq_4_freq) {
            peq4F[CHANNEL_MULTI_1] = pkt->multi_1_eq_4_freq;
            valueChanged = YES;
        }
        if (peq4Q[CHANNEL_MULTI_1] != pkt->multi_1_eq_4_q) {
            peq4Q[CHANNEL_MULTI_1] = pkt->multi_1_eq_4_q;
            if (peq4Q[CHANNEL_MULTI_1] == PEQ_4_HIGH_SHELF) {
                peq4Filter[CHANNEL_MULTI_1] = EQ_HIGH_SHELF;
            } else if (peq4Q[CHANNEL_MULTI_1] == PEQ_4_LPF) {
                peq4Filter[CHANNEL_MULTI_1] = EQ_HIGH_CUT;
            } else {
                peq4Filter[CHANNEL_MULTI_1] = EQ_PEAK;
                lastPeq4Q[CHANNEL_MULTI_1] = peq4Q[CHANNEL_MULTI_1];
            }
            valueChanged = YES;
        }
        int peqEnableValue = pkt->multi_1_ctrl;
        if (peq1Enabled[CHANNEL_MULTI_1] != ((peqEnableValue & 0x08) >> 3)) {
            peq1Enabled[CHANNEL_MULTI_1] = (peqEnableValue & 0x08) >> 3;
            valueChanged = YES;
        }
        if (peq2Enabled[CHANNEL_MULTI_1] != ((peqEnableValue & 0x10) >> 4)) {
            peq2Enabled[CHANNEL_MULTI_1] = (peqEnableValue & 0x10) >> 4;
            valueChanged = YES;
        }
        if (peq3Enabled[CHANNEL_MULTI_1] != ((peqEnableValue & 0x20) >> 5)) {
            peq3Enabled[CHANNEL_MULTI_1] = (peqEnableValue & 0x20) >> 5;
            valueChanged = YES;
        }
        if (peq4Enabled[CHANNEL_MULTI_1] != ((peqEnableValue & 0x40) >> 6)) {
            peq4Enabled[CHANNEL_MULTI_1] = (peqEnableValue & 0x40) >> 6;
            valueChanged = YES;
        }
        if (peqEnabled[CHANNEL_MULTI_1] != ((peqEnableValue & 0x80) >> 7)) {
            peqEnabled[CHANNEL_MULTI_1] = (peqEnableValue & 0x80) >> 7;
            valueChanged = YES;
        }
    }
    
    if (valueChanged) {
        [self updatePeqPlot:peqEnabled[CHANNEL_MULTI_1]];
        [self updatePeqForChannel:CHANNEL_MULTI_1];
        valueChanged = NO;
    }
    
    // CHANNEL_MULTI_2
    {
        if (peq1G[CHANNEL_MULTI_2] != pkt->multi_2_eq_1_db+EQ_GAIN_OFFSET) {
            peq1G[CHANNEL_MULTI_2] = pkt->multi_2_eq_1_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq1F[CHANNEL_MULTI_2] != pkt->multi_2_eq_1_freq) {
            peq1F[CHANNEL_MULTI_2] = pkt->multi_2_eq_1_freq;
            valueChanged = YES;
        }
        if (peq1Q[CHANNEL_MULTI_2] != pkt->multi_2_eq_1_q) {
            peq1Q[CHANNEL_MULTI_2] = pkt->multi_2_eq_1_q;
            if (peq1Q[CHANNEL_MULTI_2] == PEQ_1_LOW_SHELF) {
                peq1Filter[CHANNEL_MULTI_2] = EQ_LOW_SHELF;
            } else if (peq1Q[CHANNEL_MULTI_2] == PEQ_1_HPF) {
                peq1Filter[CHANNEL_MULTI_2] = EQ_LOW_CUT;
            } else {
                peq1Filter[CHANNEL_MULTI_2] = EQ_PEAK;
                lastPeq1Q[CHANNEL_MULTI_2] = peq1Q[CHANNEL_MULTI_2];
            }
            valueChanged = YES;
        }
        if (peq2G[CHANNEL_MULTI_2] != pkt->multi_2_eq_2_db+EQ_GAIN_OFFSET) {
            peq2G[CHANNEL_MULTI_2] = pkt->multi_2_eq_2_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq2F[CHANNEL_MULTI_2] != pkt->multi_2_eq_2_freq) {
            peq2F[CHANNEL_MULTI_2] = pkt->multi_2_eq_2_freq;
            valueChanged = YES;
        }
        if (peq2Q[CHANNEL_MULTI_2] != pkt->multi_2_eq_2_q) {
            peq2Q[CHANNEL_MULTI_2] = pkt->multi_2_eq_2_q;
            valueChanged = YES;
        }
        if (peq3G[CHANNEL_MULTI_2] != pkt->multi_2_eq_3_db+EQ_GAIN_OFFSET) {
            peq3G[CHANNEL_MULTI_2] = pkt->multi_2_eq_3_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq3F[CHANNEL_MULTI_2] != pkt->multi_2_eq_3_freq) {
            peq3F[CHANNEL_MULTI_2] = pkt->multi_2_eq_3_freq;
            valueChanged = YES;
        }
        if (peq3Q[CHANNEL_MULTI_2] != pkt->multi_2_eq_3_q) {
            peq3Q[CHANNEL_MULTI_2] = pkt->multi_2_eq_3_q;
            valueChanged = YES;
        }
        if (peq4G[CHANNEL_MULTI_2] != pkt->multi_2_eq_4_db+EQ_GAIN_OFFSET) {
            peq4G[CHANNEL_MULTI_2] = pkt->multi_2_eq_4_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq4F[CHANNEL_MULTI_2] != pkt->multi_2_eq_4_freq) {
            peq4F[CHANNEL_MULTI_2] = pkt->multi_2_eq_4_freq;
            valueChanged = YES;
        }
        if (peq4Q[CHANNEL_MULTI_2] != pkt->multi_2_eq_4_q) {
            peq4Q[CHANNEL_MULTI_2] = pkt->multi_2_eq_4_q;
            if (peq4Q[CHANNEL_MULTI_2] == PEQ_4_HIGH_SHELF) {
                peq4Filter[CHANNEL_MULTI_2] = EQ_HIGH_SHELF;
            } else if (peq4Q[CHANNEL_MULTI_2] == PEQ_4_LPF) {
                peq4Filter[CHANNEL_MULTI_2] = EQ_HIGH_CUT;
            } else {
                peq4Filter[CHANNEL_MULTI_2] = EQ_PEAK;
                lastPeq4Q[CHANNEL_MULTI_2] = peq4Q[CHANNEL_MULTI_2];
            }
            valueChanged = YES;
        }
        int peqEnableValue = pkt->multi_2_ctrl;
        if (peq1Enabled[CHANNEL_MULTI_2] != ((peqEnableValue & 0x08) >> 3)) {
            peq1Enabled[CHANNEL_MULTI_2] = (peqEnableValue & 0x08) >> 3;
            valueChanged = YES;
        }
        if (peq2Enabled[CHANNEL_MULTI_2] != ((peqEnableValue & 0x10) >> 4)) {
            peq2Enabled[CHANNEL_MULTI_2] = (peqEnableValue & 0x10) >> 4;
            valueChanged = YES;
        }
        if (peq3Enabled[CHANNEL_MULTI_2] != ((peqEnableValue & 0x20) >> 5)) {
            peq3Enabled[CHANNEL_MULTI_2] = (peqEnableValue & 0x20) >> 5;
            valueChanged = YES;
        }
        if (peq4Enabled[CHANNEL_MULTI_2] != ((peqEnableValue & 0x40) >> 6)) {
            peq4Enabled[CHANNEL_MULTI_2] = (peqEnableValue & 0x40) >> 6;
            valueChanged = YES;
        }
        if (peqEnabled[CHANNEL_MULTI_2] != ((peqEnableValue & 0x80) >> 7)) {
            peqEnabled[CHANNEL_MULTI_2] = (peqEnableValue & 0x80) >> 7;
            valueChanged = YES;
        }
    }
    
    if (valueChanged) {
        [self updatePeqPlot:peqEnabled[CHANNEL_MULTI_2]];
        [self updatePeqForChannel:CHANNEL_MULTI_2];
        valueChanged = NO;
    }
    
    // CHANNEL_MULTI_3
    {
        if (peq1G[CHANNEL_MULTI_3] != pkt->multi_3_eq_1_db+EQ_GAIN_OFFSET) {
            peq1G[CHANNEL_MULTI_3] = pkt->multi_3_eq_1_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq1F[CHANNEL_MULTI_3] != pkt->multi_3_eq_1_freq) {
            peq1F[CHANNEL_MULTI_3] = pkt->multi_3_eq_1_freq;
            valueChanged = YES;
        }
        if (peq1Q[CHANNEL_MULTI_3] != pkt->multi_3_eq_1_q) {
            peq1Q[CHANNEL_MULTI_3] = pkt->multi_3_eq_1_q;
            if (peq1Q[CHANNEL_MULTI_3] == PEQ_1_LOW_SHELF) {
                peq1Filter[CHANNEL_MULTI_3] = EQ_LOW_SHELF;
            } else if (peq1Q[CHANNEL_MULTI_3] == PEQ_1_HPF) {
                peq1Filter[CHANNEL_MULTI_3] = EQ_LOW_CUT;
            } else {
                peq1Filter[CHANNEL_MULTI_3] = EQ_PEAK;
                lastPeq1Q[CHANNEL_MULTI_3] = peq1Q[CHANNEL_MULTI_3];
            }
            valueChanged = YES;
        }
        if (peq2G[CHANNEL_MULTI_3] != pkt->multi_3_eq_2_db+EQ_GAIN_OFFSET) {
            peq2G[CHANNEL_MULTI_3] = pkt->multi_3_eq_2_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq2F[CHANNEL_MULTI_3] != pkt->multi_3_eq_2_freq) {
            peq2F[CHANNEL_MULTI_3] = pkt->multi_3_eq_2_freq;
            valueChanged = YES;
        }
        if (peq2Q[CHANNEL_MULTI_3] != pkt->multi_3_eq_2_q) {
            peq2Q[CHANNEL_MULTI_3] = pkt->multi_3_eq_2_q;
            valueChanged = YES;
        }
        if (peq3G[CHANNEL_MULTI_3] != pkt->multi_3_eq_3_db+EQ_GAIN_OFFSET) {
            peq3G[CHANNEL_MULTI_3] = pkt->multi_3_eq_3_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq3F[CHANNEL_MULTI_3] != pkt->multi_3_eq_3_freq) {
            peq3F[CHANNEL_MULTI_3] = pkt->multi_3_eq_3_freq;
            valueChanged = YES;
        }
        if (peq3Q[CHANNEL_MULTI_3] != pkt->multi_3_eq_3_q) {
            peq3Q[CHANNEL_MULTI_3] = pkt->multi_3_eq_3_q;
            valueChanged = YES;
        }
        if (peq4G[CHANNEL_MULTI_3] != pkt->multi_3_eq_4_db+EQ_GAIN_OFFSET) {
            peq4G[CHANNEL_MULTI_3] = pkt->multi_3_eq_4_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq4F[CHANNEL_MULTI_3] != pkt->multi_3_eq_4_freq) {
            peq4F[CHANNEL_MULTI_3] = pkt->multi_3_eq_4_freq;
            valueChanged = YES;
        }
        if (peq4Q[CHANNEL_MULTI_3] != pkt->multi_3_eq_4_q) {
            peq4Q[CHANNEL_MULTI_3] = pkt->multi_3_eq_4_q;
            if (peq4Q[CHANNEL_MULTI_3] == PEQ_4_HIGH_SHELF) {
                peq4Filter[CHANNEL_MULTI_3] = EQ_HIGH_SHELF;
            } else if (peq4Q[CHANNEL_MULTI_3] == PEQ_4_LPF) {
                peq4Filter[CHANNEL_MULTI_3] = EQ_HIGH_CUT;
            } else {
                peq4Filter[CHANNEL_MULTI_3] = EQ_PEAK;
                lastPeq4Q[CHANNEL_MULTI_3] = peq4Q[CHANNEL_MULTI_3];
            }
            valueChanged = YES;
        }
        int peqEnableValue = pkt->multi_3_ctrl;
        if (peq1Enabled[CHANNEL_MULTI_3] != ((peqEnableValue & 0x08) >> 3)) {
            peq1Enabled[CHANNEL_MULTI_3] = (peqEnableValue & 0x08) >> 3;
            valueChanged = YES;
        }
        if (peq2Enabled[CHANNEL_MULTI_3] != ((peqEnableValue & 0x10) >> 4)) {
            peq2Enabled[CHANNEL_MULTI_3] = (peqEnableValue & 0x10) >> 4;
            valueChanged = YES;
        }
        if (peq3Enabled[CHANNEL_MULTI_3] != ((peqEnableValue & 0x20) >> 5)) {
            peq3Enabled[CHANNEL_MULTI_3] = (peqEnableValue & 0x20) >> 5;
            valueChanged = YES;
        }
        if (peq4Enabled[CHANNEL_MULTI_3] != ((peqEnableValue & 0x40) >> 6)) {
            peq4Enabled[CHANNEL_MULTI_3] = (peqEnableValue & 0x40) >> 6;
            valueChanged = YES;
        }
        if (peqEnabled[CHANNEL_MULTI_3] != ((peqEnableValue & 0x80) >> 7)) {
            peqEnabled[CHANNEL_MULTI_3] = (peqEnableValue & 0x80) >> 7;
            valueChanged = YES;
        }
    }
    
    if (valueChanged) {
        [self updatePeqPlot:peqEnabled[CHANNEL_MULTI_3]];
        [self updatePeqForChannel:CHANNEL_MULTI_3];
        valueChanged = NO;
    }
    
    // CHANNEL_MULTI_4
    {
        if (peq1G[CHANNEL_MULTI_4] != pkt->multi_4_eq_1_db+EQ_GAIN_OFFSET) {
            peq1G[CHANNEL_MULTI_4] = pkt->multi_4_eq_1_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq1F[CHANNEL_MULTI_4] != pkt->multi_4_eq_1_freq) {
            peq1F[CHANNEL_MULTI_4] = pkt->multi_4_eq_1_freq;
            valueChanged = YES;
        }
        if (peq1Q[CHANNEL_MULTI_4] != pkt->multi_4_eq_1_q) {
            peq1Q[CHANNEL_MULTI_4] = pkt->multi_4_eq_1_q;
            if (peq1Q[CHANNEL_MULTI_4] == PEQ_1_LOW_SHELF) {
                peq1Filter[CHANNEL_MULTI_4] = EQ_LOW_SHELF;
            } else if (peq1Q[CHANNEL_MULTI_4] == PEQ_1_HPF) {
                peq1Filter[CHANNEL_MULTI_4] = EQ_LOW_CUT;
            } else {
                peq1Filter[CHANNEL_MULTI_4] = EQ_PEAK;
                lastPeq1Q[CHANNEL_MULTI_4] = peq1Q[CHANNEL_MULTI_4];
            }
            valueChanged = YES;
        }
        if (peq2G[CHANNEL_MULTI_4] != pkt->multi_4_eq_2_db+EQ_GAIN_OFFSET) {
            peq2G[CHANNEL_MULTI_4] = pkt->multi_4_eq_2_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq2F[CHANNEL_MULTI_4] != pkt->multi_4_eq_2_freq) {
            peq2F[CHANNEL_MULTI_4] = pkt->multi_4_eq_2_freq;
            valueChanged = YES;
        }
        if (peq2Q[CHANNEL_MULTI_4] != pkt->multi_4_eq_2_q) {
            peq2Q[CHANNEL_MULTI_4] = pkt->multi_4_eq_2_q;
            valueChanged = YES;
        }
        if (peq3G[CHANNEL_MULTI_4] != pkt->multi_4_eq_3_db+EQ_GAIN_OFFSET) {
            peq3G[CHANNEL_MULTI_4] = pkt->multi_4_eq_3_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq3F[CHANNEL_MULTI_4] != pkt->multi_4_eq_3_freq) {
            peq3F[CHANNEL_MULTI_4] = pkt->multi_4_eq_3_freq;
            valueChanged = YES;
        }
        if (peq3Q[CHANNEL_MULTI_4] != pkt->multi_4_eq_3_q) {
            peq3Q[CHANNEL_MULTI_4] = pkt->multi_4_eq_3_q;
            valueChanged = YES;
        }
        if (peq4G[CHANNEL_MULTI_4] != pkt->multi_4_eq_4_db+EQ_GAIN_OFFSET) {
            peq4G[CHANNEL_MULTI_4] = pkt->multi_4_eq_4_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq4F[CHANNEL_MULTI_4] != pkt->multi_4_eq_4_freq) {
            peq4F[CHANNEL_MULTI_4] = pkt->multi_4_eq_4_freq;
            valueChanged = YES;
        }
        if (peq4Q[CHANNEL_MULTI_4] != pkt->multi_4_eq_4_q) {
            peq4Q[CHANNEL_MULTI_4] = pkt->multi_4_eq_4_q;
            if (peq4Q[CHANNEL_MULTI_4] == PEQ_4_HIGH_SHELF) {
                peq4Filter[CHANNEL_MULTI_4] = EQ_HIGH_SHELF;
            } else if (peq4Q[CHANNEL_MULTI_4] == PEQ_4_LPF) {
                peq4Filter[CHANNEL_MULTI_4] = EQ_HIGH_CUT;
            } else {
                peq4Filter[CHANNEL_MULTI_4] = EQ_PEAK;
                lastPeq4Q[CHANNEL_MULTI_4] = peq4Q[CHANNEL_MULTI_4];
            }
            valueChanged = YES;
        }
        int peqEnableValue = pkt->multi_4_ctrl;
        if (peq1Enabled[CHANNEL_MULTI_4] != ((peqEnableValue & 0x08) >> 3)) {
            peq1Enabled[CHANNEL_MULTI_4] = (peqEnableValue & 0x08) >> 3;
            valueChanged = YES;
        }
        if (peq2Enabled[CHANNEL_MULTI_4] != ((peqEnableValue & 0x10) >> 4)) {
            peq2Enabled[CHANNEL_MULTI_4] = (peqEnableValue & 0x10) >> 4;
            valueChanged = YES;
        }
        if (peq3Enabled[CHANNEL_MULTI_4] != ((peqEnableValue & 0x20) >> 5)) {
            peq3Enabled[CHANNEL_MULTI_4] = (peqEnableValue & 0x20) >> 5;
            valueChanged = YES;
        }
        if (peq4Enabled[CHANNEL_MULTI_4] != ((peqEnableValue & 0x40) >> 6)) {
            peq4Enabled[CHANNEL_MULTI_4] = (peqEnableValue & 0x40) >> 6;
            valueChanged = YES;
        }
        if (peqEnabled[CHANNEL_MULTI_4] != ((peqEnableValue & 0x80) >> 7)) {
            peqEnabled[CHANNEL_MULTI_4] = (peqEnableValue & 0x80) >> 7;
            valueChanged = YES;
        }
    }
    
    if (valueChanged) {
        [self updatePeqPlot:peqEnabled[CHANNEL_MULTI_4]];
        [self updatePeqForChannel:CHANNEL_MULTI_4];
        valueChanged = NO;
    }
    
    // CHANNEL_MAIN
    if (mainEqType == MAIN_EQ_GEQ) {
        BOOL needsUpdate = NO;
        
        // Update GEQ sliders
        int geq1Value = pkt->geq_20_hz_db;
        int geq2Value = pkt->geq_25_hz_db;
        int geq3Value = pkt->geq_31_5_hz_db;
        int geq4Value = pkt->geq_40_hz_db;
        int geq5Value = pkt->geq_50_hz_db;
        int geq6Value = pkt->geq_63_hz_db;
        int geq7Value = pkt->geq_80_hz_db;
        int geq8Value = pkt->geq_100_hz_db;
        int geq9Value = pkt->geq_125_hz_db;
        int geq10Value = pkt->geq_160_hz_db;
        int geq11Value = pkt->geq_200_hz_db;
        int geq12Value = pkt->geq_250_hz_db;
        int geq13Value = pkt->geq_315_hz_db;
        int geq14Value = pkt->geq_400_hz_db;
        int geq15Value = pkt->geq_500_hz_db;
        int geq16Value = pkt->geq_630_hz_db;
        int geq17Value = pkt->geq_800_hz_db;
        int geq18Value = pkt->geq_1_khz_db;
        int geq19Value = pkt->geq_1_25_khz_db;
        int geq20Value = pkt->geq_1_6_khz_db;
        int geq21Value = pkt->geq_2_khz_db;
        int geq22Value = pkt->geq_2_5_khz_db;
        int geq23Value = pkt->geq_3_15_khz_db;
        int geq24Value = pkt->geq_4_khz_db;
        int geq25Value = pkt->geq_5_khz_db;
        int geq26Value = pkt->geq_6_3_khz_db;
        int geq27Value = pkt->geq_8_khz_db;
        int geq28Value = pkt->geq_10_khz_db;
        int geq29Value = pkt->geq_12_5_khz_db;
        int geq30Value = pkt->geq_16_khz_db;
        int geq31Value = pkt->geq_20_khz_db;
        
        if (sliderGeq1.value != geq1Value) {
            sliderGeq1.value = geq1Value;
            if (sliderGeq1.value > 0.0) {
                meter1Top.progress = sliderGeq1.value/GEQ_SLIDER_MAX_VALUE;
                meter1Btm.progress = 0.0;
            } else if (sliderGeq2.value < 0.0) {
                meter1Btm.progress = sliderGeq1.value/GEQ_SLIDER_MIN_VALUE;
                meter1Top.progress = 0.0;
            } else {
                meter1Top.progress = 0.0;
                meter1Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq2.value != geq2Value) {
            sliderGeq2.value = geq2Value;
            if (sliderGeq2.value > 0.0) {
                meter2Top.progress = sliderGeq2.value/GEQ_SLIDER_MAX_VALUE;
                meter2Btm.progress = 0.0;
            } else if (sliderGeq2.value < 0.0) {
                meter2Btm.progress = sliderGeq2.value/GEQ_SLIDER_MIN_VALUE;
                meter2Top.progress = 0.0;
            } else {
                meter2Top.progress = 0.0;
                meter2Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq3.value != geq3Value) {
            sliderGeq3.value = geq3Value;
            if (sliderGeq3.value > 0.0) {
                meter3Top.progress = sliderGeq3.value/GEQ_SLIDER_MAX_VALUE;
                meter3Btm.progress = 0.0;
            } else if (sliderGeq3.value < 0.0) {
                meter3Btm.progress = sliderGeq3.value/GEQ_SLIDER_MIN_VALUE;
                meter3Top.progress = 0.0;
            } else {
                meter3Top.progress = 0.0;
                meter3Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq4.value != geq4Value) {
            sliderGeq4.value = geq4Value;
            if (sliderGeq4.value > 0.0) {
                meter4Top.progress = sliderGeq4.value/GEQ_SLIDER_MAX_VALUE;
                meter4Btm.progress = 0.0;
            } else if (sliderGeq4.value < 0.0) {
                meter4Btm.progress = sliderGeq4.value/GEQ_SLIDER_MIN_VALUE;
                meter4Top.progress = 0.0;
            } else {
                meter4Top.progress = 0.0;
                meter4Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq5.value != geq5Value) {
            sliderGeq5.value = geq5Value;
            if (sliderGeq5.value > 0.0) {
                meter5Top.progress = sliderGeq5.value/GEQ_SLIDER_MAX_VALUE;
                meter5Btm.progress = 0.0;
            } else if (sliderGeq5.value < 0.0) {
                meter5Btm.progress = sliderGeq5.value/GEQ_SLIDER_MIN_VALUE;
                meter5Top.progress = 0.0;
            } else {
                meter5Top.progress = 0.0;
                meter5Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq6.value != geq6Value) {
            sliderGeq6.value = geq6Value;
            if (sliderGeq6.value > 0.0) {
                meter6Top.progress = sliderGeq6.value/GEQ_SLIDER_MAX_VALUE;
                meter6Btm.progress = 0.0;
            } else if (sliderGeq6.value < 0.0) {
                meter6Btm.progress = sliderGeq6.value/GEQ_SLIDER_MIN_VALUE;
                meter6Top.progress = 0.0;
            } else {
                meter6Top.progress = 0.0;
                meter6Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq7.value != geq7Value) {
            sliderGeq7.value = geq7Value;
            if (sliderGeq7.value > 0.0) {
                meter7Top.progress = sliderGeq7.value/GEQ_SLIDER_MAX_VALUE;
                meter7Btm.progress = 0.0;
            } else if (sliderGeq7.value < 0.0) {
                meter7Btm.progress = sliderGeq7.value/GEQ_SLIDER_MIN_VALUE;
                meter7Top.progress = 0.0;
            } else {
                meter7Top.progress = 0.0;
                meter7Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq8.value != geq8Value) {
            sliderGeq8.value = geq8Value;
            if (sliderGeq8.value > 0.0) {
                meter8Top.progress = sliderGeq8.value/GEQ_SLIDER_MAX_VALUE;
                meter8Btm.progress = 0.0;
            } else if (sliderGeq8.value < 0.0) {
                meter8Btm.progress = sliderGeq8.value/GEQ_SLIDER_MIN_VALUE;
                meter8Top.progress = 0.0;
            } else {
                meter8Top.progress = 0.0;
                meter8Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq9.value != geq9Value) {
            sliderGeq9.value = geq9Value;
            if (sliderGeq9.value > 0.0) {
                meter9Top.progress = sliderGeq9.value/GEQ_SLIDER_MAX_VALUE;
                meter9Btm.progress = 0.0;
            } else if (sliderGeq9.value < 0.0) {
                meter9Btm.progress = sliderGeq9.value/GEQ_SLIDER_MIN_VALUE;
                meter9Top.progress = 0.0;
            } else {
                meter9Top.progress = 0.0;
                meter9Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq10.value != geq10Value) {
            sliderGeq10.value = geq10Value;
            if (sliderGeq10.value > 0.0) {
                meter10Top.progress = sliderGeq10.value/GEQ_SLIDER_MAX_VALUE;
                meter10Btm.progress = 0.0;
            } else if (sliderGeq10.value < 0.0) {
                meter10Btm.progress = sliderGeq10.value/GEQ_SLIDER_MIN_VALUE;
                meter10Top.progress = 0.0;
            } else {
                meter10Top.progress = 0.0;
                meter10Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq11.value != geq11Value) {
            sliderGeq11.value = geq11Value;
            if (sliderGeq11.value > 0.0) {
                meter11Top.progress = sliderGeq11.value/GEQ_SLIDER_MAX_VALUE;
                meter11Btm.progress = 0.0;
            } else if (sliderGeq11.value < 0.0) {
                meter11Btm.progress = sliderGeq11.value/GEQ_SLIDER_MIN_VALUE;
                meter1Top.progress = 0.0;
            } else {
                meter11Top.progress = 0.0;
                meter11Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq12.value != geq12Value) {
            sliderGeq12.value = geq12Value;
            if (sliderGeq12.value > 0.0) {
                meter12Top.progress = sliderGeq12.value/GEQ_SLIDER_MAX_VALUE;
                meter12Btm.progress = 0.0;
            } else if (sliderGeq12.value < 0.0) {
                meter12Btm.progress = sliderGeq12.value/GEQ_SLIDER_MIN_VALUE;
                meter12Top.progress = 0.0;
            } else {
                meter12Top.progress = 0.0;
                meter12Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq13.value != geq13Value) {
            sliderGeq13.value = geq13Value;
            if (sliderGeq13.value > 0.0) {
                meter13Top.progress = sliderGeq13.value/GEQ_SLIDER_MAX_VALUE;
                meter13Btm.progress = 0.0;
            } else if (sliderGeq13.value < 0.0) {
                meter13Btm.progress = sliderGeq13.value/GEQ_SLIDER_MIN_VALUE;
                meter13Top.progress = 0.0;
            } else {
                meter13Top.progress = 0.0;
                meter13Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq14.value != geq14Value) {
            sliderGeq14.value = geq14Value;
            if (sliderGeq14.value > 0.0) {
                meter14Top.progress = sliderGeq14.value/GEQ_SLIDER_MAX_VALUE;
                meter14Btm.progress = 0.0;
            } else if (sliderGeq14.value < 0.0) {
                meter14Btm.progress = sliderGeq14.value/GEQ_SLIDER_MIN_VALUE;
                meter14Top.progress = 0.0;
            } else {
                meter14Top.progress = 0.0;
                meter14Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq15.value != geq15Value) {
            sliderGeq15.value = geq15Value;
            if (sliderGeq15.value > 0.0) {
                meter15Top.progress = sliderGeq15.value/GEQ_SLIDER_MAX_VALUE;
                meter15Btm.progress = 0.0;
            } else if (sliderGeq15.value < 0.0) {
                meter15Btm.progress = sliderGeq15.value/GEQ_SLIDER_MIN_VALUE;
                meter15Top.progress = 0.0;
            } else {
                meter15Top.progress = 0.0;
                meter15Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq16.value != geq16Value) {
            sliderGeq16.value = geq16Value;
            if (sliderGeq16.value > 0.0) {
                meter16Top.progress = sliderGeq16.value/GEQ_SLIDER_MAX_VALUE;
                meter16Btm.progress = 0.0;
            } else if (sliderGeq16.value < 0.0) {
                meter16Btm.progress = sliderGeq16.value/GEQ_SLIDER_MIN_VALUE;
                meter16Top.progress = 0.0;
            } else {
                meter16Top.progress = 0.0;
                meter16Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq17.value != geq17Value) {
            sliderGeq17.value = geq17Value;
            if (sliderGeq17.value > 0.0) {
                meter17Top.progress = sliderGeq17.value/GEQ_SLIDER_MAX_VALUE;
                meter17Btm.progress = 0.0;
            } else if (sliderGeq17.value < 0.0) {
                meter17Btm.progress = sliderGeq17.value/GEQ_SLIDER_MIN_VALUE;
                meter17Top.progress = 0.0;
            } else {
                meter17Top.progress = 0.0;
                meter17Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq18.value != geq18Value) {
            sliderGeq18.value = geq18Value;
            if (sliderGeq18.value > 0.0) {
                meter18Top.progress = sliderGeq18.value/GEQ_SLIDER_MAX_VALUE;
                meter18Btm.progress = 0.0;
            } else if (sliderGeq18.value < 0.0) {
                meter18Btm.progress = sliderGeq18.value/GEQ_SLIDER_MIN_VALUE;
                meter18Top.progress = 0.0;
            } else {
                meter18Top.progress = 0.0;
                meter18Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq19.value != geq19Value) {
            sliderGeq19.value = geq19Value;
            if (sliderGeq19.value > 0.0) {
                meter19Top.progress = sliderGeq19.value/GEQ_SLIDER_MAX_VALUE;
                meter19Btm.progress = 0.0;
            } else if (sliderGeq19.value < 0.0) {
                meter19Btm.progress = sliderGeq19.value/GEQ_SLIDER_MIN_VALUE;
                meter19Top.progress = 0.0;
            } else {
                meter19Top.progress = 0.0;
                meter19Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq20.value != geq20Value) {
            sliderGeq20.value = geq20Value;
            if (sliderGeq20.value > 0.0) {
                meter20Top.progress = sliderGeq20.value/GEQ_SLIDER_MAX_VALUE;
                meter20Btm.progress = 0.0;
            } else if (sliderGeq20.value < 0.0) {
                meter20Btm.progress = sliderGeq20.value/GEQ_SLIDER_MIN_VALUE;
                meter20Top.progress = 0.0;
            } else {
                meter20Top.progress = 0.0;
                meter20Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq21.value != geq21Value) {
            sliderGeq21.value = geq21Value;
            if (sliderGeq21.value > 0.0) {
                meter21Top.progress = sliderGeq21.value/GEQ_SLIDER_MAX_VALUE;
                meter21Btm.progress = 0.0;
            } else if (sliderGeq21.value < 0.0) {
                meter21Btm.progress = sliderGeq21.value/GEQ_SLIDER_MIN_VALUE;
                meter21Top.progress = 0.0;
            } else {
                meter21Top.progress = 0.0;
                meter21Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq22.value != geq22Value) {
            sliderGeq22.value = geq22Value;
            if (sliderGeq22.value > 0.0) {
                meter22Top.progress = sliderGeq22.value/GEQ_SLIDER_MAX_VALUE;
                meter22Btm.progress = 0.0;
            } else if (sliderGeq22.value < 0.0) {
                meter22Btm.progress = sliderGeq22.value/GEQ_SLIDER_MIN_VALUE;
                meter22Top.progress = 0.0;
            } else {
                meter22Top.progress = 0.0;
                meter22Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq23.value != geq23Value) {
            sliderGeq23.value = geq23Value;
            if (sliderGeq23.value > 0.0) {
                meter23Top.progress = sliderGeq23.value/GEQ_SLIDER_MAX_VALUE;
                meter23Btm.progress = 0.0;
            } else if (sliderGeq23.value < 0.0) {
                meter23Btm.progress = sliderGeq23.value/GEQ_SLIDER_MIN_VALUE;
                meter23Top.progress = 0.0;
            } else {
                meter23Top.progress = 0.0;
                meter23Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq24.value != geq24Value) {
            sliderGeq24.value = geq24Value;
            if (sliderGeq24.value > 0.0) {
                meter24Top.progress = sliderGeq24.value/GEQ_SLIDER_MAX_VALUE;
                meter24Btm.progress = 0.0;
            } else if (sliderGeq24.value < 0.0) {
                meter24Btm.progress = sliderGeq24.value/GEQ_SLIDER_MIN_VALUE;
                meter24Top.progress = 0.0;
            } else {
                meter24Top.progress = 0.0;
                meter24Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq25.value != geq25Value) {
            sliderGeq25.value = geq25Value;
            if (sliderGeq25.value > 0.0) {
                meter25Top.progress = sliderGeq25.value/GEQ_SLIDER_MAX_VALUE;
                meter25Btm.progress = 0.0;
            } else if (sliderGeq25.value < 0.0) {
                meter25Btm.progress = sliderGeq25.value/GEQ_SLIDER_MIN_VALUE;
                meter25Top.progress = 0.0;
            } else {
                meter25Top.progress = 0.0;
                meter25Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq26.value != geq26Value) {
            sliderGeq26.value = geq26Value;
            if (sliderGeq26.value > 0.0) {
                meter26Top.progress = sliderGeq26.value/GEQ_SLIDER_MAX_VALUE;
                meter26Btm.progress = 0.0;
            } else if (sliderGeq26.value < 0.0) {
                meter26Btm.progress = sliderGeq26.value/GEQ_SLIDER_MIN_VALUE;
                meter26Top.progress = 0.0;
            } else {
                meter26Top.progress = 0.0;
                meter26Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq27.value != geq27Value) {
            sliderGeq27.value = geq27Value;
            if (sliderGeq27.value > 0.0) {
                meter27Top.progress = sliderGeq27.value/GEQ_SLIDER_MAX_VALUE;
                meter27Btm.progress = 0.0;
            } else if (sliderGeq27.value < 0.0) {
                meter27Btm.progress = sliderGeq27.value/GEQ_SLIDER_MIN_VALUE;
                meter27Top.progress = 0.0;
            } else {
                meter27Top.progress = 0.0;
                meter27Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq28.value != geq28Value) {
            sliderGeq28.value = geq28Value;
            if (sliderGeq28.value > 0.0) {
                meter28Top.progress = sliderGeq28.value/GEQ_SLIDER_MAX_VALUE;
                meter28Btm.progress = 0.0;
            } else if (sliderGeq28.value < 0.0) {
                meter28Btm.progress = sliderGeq28.value/GEQ_SLIDER_MIN_VALUE;
                meter28Top.progress = 0.0;
            } else {
                meter28Top.progress = 0.0;
                meter28Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq29.value != geq29Value) {
            sliderGeq29.value = geq29Value;
            if (sliderGeq29.value > 0.0) {
                meter29Top.progress = sliderGeq29.value/GEQ_SLIDER_MAX_VALUE;
                meter29Btm.progress = 0.0;
            } else if (sliderGeq29.value < 0.0) {
                meter29Btm.progress = sliderGeq29.value/GEQ_SLIDER_MIN_VALUE;
                meter29Top.progress = 0.0;
            } else {
                meter29Top.progress = 0.0;
                meter29Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq30.value != geq30Value) {
            sliderGeq30.value = geq30Value;
            if (sliderGeq30.value > 0.0) {
                meter30Top.progress = sliderGeq30.value/GEQ_SLIDER_MAX_VALUE;
                meter30Btm.progress = 0.0;
            } else if (sliderGeq30.value < 0.0) {
                meter30Btm.progress = sliderGeq30.value/GEQ_SLIDER_MIN_VALUE;
                meter30Top.progress = 0.0;
            } else {
                meter30Top.progress = 0.0;
                meter30Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        if (sliderGeq31.value != geq31Value) {
            sliderGeq31.value = geq31Value;
            if (sliderGeq31.value > 0.0) {
                meter31Top.progress = sliderGeq31.value/GEQ_SLIDER_MAX_VALUE;
                meter31Btm.progress = 0.0;
            } else if (sliderGeq31.value < 0.0) {
                meter31Btm.progress = sliderGeq31.value/GEQ_SLIDER_MIN_VALUE;
                meter31Top.progress = 0.0;
            } else {
                meter31Top.progress = 0.0;
                meter31Btm.progress = 0.0;
            }
            needsUpdate = YES;
        }
        int geqEnabled = (pkt->main_l_r_ctrl & 0x80) >> 7;
        if (btnGeqOnOff.selected != geqEnabled) {
            btnGeqOnOff.selected = geqEnabled;
            peqEnabled[CHANNEL_MAIN] = btnGeqOnOff.selected;
            [self updateGeqMeters];
            needsUpdate = YES;
        }
        
        if (needsUpdate) {
            [self updatePreviewImageForChannel:CHANNEL_MAIN];
        }
    } else {
        if (peq1G[CHANNEL_MAIN] != pkt->main_l_r_eq_1_db+EQ_GAIN_OFFSET) {
            peq1G[CHANNEL_MAIN] = pkt->main_l_r_eq_1_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq1F[CHANNEL_MAIN] != pkt->main_l_r_eq_1_freq) {
            peq1F[CHANNEL_MAIN] = pkt->main_l_r_eq_1_freq;
            valueChanged = YES;
        }
        if (peq1Q[CHANNEL_MAIN] != pkt->main_l_r_eq_1_q) {
            peq1Q[CHANNEL_MAIN] = pkt->main_l_r_eq_1_q;
            if (peq1Q[CHANNEL_MAIN] == PEQ_1_LOW_SHELF) {
                peq1Filter[CHANNEL_MAIN] = EQ_LOW_SHELF;
            } else if (peq1Q[CHANNEL_MAIN] == PEQ_1_HPF) {
                peq1Filter[CHANNEL_MAIN] = EQ_LOW_CUT;
            } else {
                peq1Filter[CHANNEL_MAIN] = EQ_PEAK;
                lastPeq1Q[CHANNEL_MAIN] = peq1Q[CHANNEL_MAIN];
            }
            valueChanged = YES;
        }
        if (peq2G[CHANNEL_MAIN] != pkt->main_l_r_eq_2_db+EQ_GAIN_OFFSET) {
            peq2G[CHANNEL_MAIN] = pkt->main_l_r_eq_2_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq2F[CHANNEL_MAIN] != pkt->main_l_r_eq_2_freq) {
            peq2F[CHANNEL_MAIN] = pkt->main_l_r_eq_2_freq;
            valueChanged = YES;
        }
        if (peq2Q[CHANNEL_MAIN] != pkt->main_l_r_eq_2_q) {
            peq2Q[CHANNEL_MAIN] = pkt->main_l_r_eq_2_q;
            valueChanged = YES;
        }
        if (peq3G[CHANNEL_MAIN] != pkt->main_l_r_eq_3_db+EQ_GAIN_OFFSET) {
            peq3G[CHANNEL_MAIN] = pkt->main_l_r_eq_3_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq3F[CHANNEL_MAIN] != pkt->main_l_r_eq_3_freq) {
            peq3F[CHANNEL_MAIN] = pkt->main_l_r_eq_3_freq;
            valueChanged = YES;
        }
        if (peq3Q[CHANNEL_MAIN] != pkt->main_l_r_eq_3_q) {
            peq3Q[CHANNEL_MAIN] = pkt->main_l_r_eq_3_q;
            valueChanged = YES;
        }
        if (peq4G[CHANNEL_MAIN] != pkt->main_l_r_eq_4_db+EQ_GAIN_OFFSET) {
            peq4G[CHANNEL_MAIN] = pkt->main_l_r_eq_4_db+EQ_GAIN_OFFSET;
            valueChanged = YES;
        }
        if (peq4F[CHANNEL_MAIN] != pkt->main_l_r_eq_4_freq) {
            peq4F[CHANNEL_MAIN] = pkt->main_l_r_eq_4_freq;
            valueChanged = YES;
        }
        if (peq4Q[CHANNEL_MAIN] != pkt->main_l_r_eq_4_q) {
            peq4Q[CHANNEL_MAIN] = pkt->main_l_r_eq_4_q;
            if (peq4Q[CHANNEL_MAIN] == PEQ_4_HIGH_SHELF) {
                peq4Filter[CHANNEL_MAIN] = EQ_HIGH_SHELF;
            } else if (peq4Q[CHANNEL_MAIN] == PEQ_4_LPF) {
                peq4Filter[CHANNEL_MAIN] = EQ_HIGH_CUT;
            } else {
                peq4Filter[CHANNEL_MAIN] = EQ_PEAK;
                lastPeq4Q[CHANNEL_MAIN] = peq4Q[CHANNEL_MAIN];
            }
            valueChanged = YES;
        }
        
        int peqEnableValue = pkt->main_l_r_ctrl;
        if (peq1Enabled[CHANNEL_MAIN] != ((peqEnableValue & 0x08) >> 3)) {
            peq1Enabled[CHANNEL_MAIN] = (peqEnableValue & 0x08) >> 3;
            valueChanged = YES;
        }
        if (peq2Enabled[CHANNEL_MAIN] != ((peqEnableValue & 0x10) >> 4)) {
            peq2Enabled[CHANNEL_MAIN] = (peqEnableValue & 0x10) >> 4;
            valueChanged = YES;
        }
        if (peq3Enabled[CHANNEL_MAIN] != ((peqEnableValue & 0x20) >> 5)) {
            peq3Enabled[CHANNEL_MAIN] = (peqEnableValue & 0x20) >> 5;
            valueChanged = YES;
        }
        if (peq4Enabled[CHANNEL_MAIN] != ((peqEnableValue & 0x40) >> 6)) {
            peq4Enabled[CHANNEL_MAIN] = (peqEnableValue & 0x40) >> 6;
            valueChanged = YES;
        }
        if (peqEnabled[CHANNEL_MAIN] != ((peqEnableValue & 0x80) >> 7)) {
            peqEnabled[CHANNEL_MAIN] = (peqEnableValue & 0x80) >> 7;
            valueChanged = YES;
        }
    }
    
    if (valueChanged) {
        [self updatePeqPlot:peqEnabled[CHANNEL_MAIN]];
        [self updatePeqForChannel:CHANNEL_MAIN];
    }
}

- (void)processChannelLabel:(AllMeterValueRxPacket *)pkt
{
    switch (currentChannel) {
        case CH_1:
            txtPage.text = [NSString stringWithUTF8String:(const char *)pkt->channel_1_audio_name];
            break;
        case CH_2:
            txtPage.text = [NSString stringWithUTF8String:(const char *)pkt->channel_2_audio_name];
            break;
        case CH_3:
            txtPage.text = [NSString stringWithUTF8String:(const char *)pkt->channel_3_audio_name];
            break;
        case CH_4:
            txtPage.text = [NSString stringWithUTF8String:(const char *)pkt->channel_4_audio_name];
            break;
        case CH_5:
            txtPage.text = [NSString stringWithUTF8String:(const char *)pkt->channel_5_audio_name];
            break;
        case CH_6:
            txtPage.text = [NSString stringWithUTF8String:(const char *)pkt->channel_6_audio_name];
            break;
        case CH_7:
            txtPage.text = [NSString stringWithUTF8String:(const char *)pkt->channel_7_audio_name];
            break;
        case CH_8:
            txtPage.text = [NSString stringWithUTF8String:(const char *)pkt->channel_8_audio_name];
            break;
        case CH_9:
            txtPage.text = [NSString stringWithUTF8String:(const char *)pkt->channel_9_audio_name];
            break;
        case CH_10:
            txtPage.text = [NSString stringWithUTF8String:(const char *)pkt->channel_10_audio_name];
            break;
        case CH_11:
            txtPage.text = [NSString stringWithUTF8String:(const char *)pkt->channel_11_audio_name];
            break;
        case CH_12:
            txtPage.text = [NSString stringWithUTF8String:(const char *)pkt->channel_12_audio_name];
            break;
        case CH_13:
            txtPage.text = [NSString stringWithUTF8String:(const char *)pkt->channel_13_audio_name];
            break;
        case CH_14:
            txtPage.text = [NSString stringWithUTF8String:(const char *)pkt->channel_14_audio_name];
            break;
        case CH_15:
            txtPage.text = [NSString stringWithUTF8String:(const char *)pkt->channel_15_audio_name];
            break;
        case CH_16:
            txtPage.text = [NSString stringWithUTF8String:(const char *)pkt->channel_16_audio_name];
            break;
        case CH_17:
            txtPage.text = [NSString stringWithUTF8String:(const char *)pkt->channel_17_audio_name];
            break;
        case CH_18:
            txtPage.text = [NSString stringWithUTF8String:(const char *)pkt->channel_18_audio_name];
            break;
        case MT_1:
            txtPage.text = [NSString stringWithUTF8String:(const char *)pkt->multi_1_audio_name];
            break;
        case MT_2:
            txtPage.text = [NSString stringWithUTF8String:(const char *)pkt->multi_2_audio_name];
            break;
        case MT_3:
            txtPage.text = [NSString stringWithUTF8String:(const char *)pkt->multi_3_audio_name];
            break;
        case MT_4:
            txtPage.text = [NSString stringWithUTF8String:(const char *)pkt->multi_4_audio_name];
            break;
        case CTRL_RM:
            txtPage.text = [NSString stringWithUTF8String:(const char *)pkt->ctrl_room_audio_name];
            break;
        case MAIN:
            txtPage.text = [NSString stringWithUTF8String:(const char *)pkt->main_audio_name];
            break;
        default:
            break;
    }
}

- (void)processMeter:(AllMeterValueRxPacket *)meterValue
{
    switch (currentChannel) {
        case CHANNEL_CH_1:
            meterR.progress = ((float)meterValue->channel_1_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case CHANNEL_CH_2:
            meterR.progress = ((float)meterValue->channel_2_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case CHANNEL_CH_3:
            meterR.progress = ((float)meterValue->channel_3_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case CHANNEL_CH_4:
            meterR.progress = ((float)meterValue->channel_4_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case CHANNEL_CH_5:
            meterR.progress = ((float)meterValue->channel_5_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case CHANNEL_CH_6:
            meterR.progress = ((float)meterValue->channel_6_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case CHANNEL_CH_7:
            meterR.progress = ((float)meterValue->channel_7_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case CHANNEL_CH_8:
            meterR.progress = ((float)meterValue->channel_8_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case CHANNEL_CH_9:
            meterR.progress = ((float)meterValue->channel_9_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case CHANNEL_CH_10:
            meterR.progress = ((float)meterValue->channel_10_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case CHANNEL_CH_11:
            meterR.progress = ((float)meterValue->channel_11_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case CHANNEL_CH_12:
            meterR.progress = ((float)meterValue->channel_12_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case CHANNEL_CH_13:
            meterR.progress = ((float)meterValue->channel_13_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case CHANNEL_CH_14:
            meterR.progress = ((float)meterValue->channel_14_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case CHANNEL_CH_15:
            meterR.progress = ((float)meterValue->channel_15_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case CHANNEL_CH_16:
            meterR.progress = ((float)meterValue->channel_16_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case CHANNEL_CH_17:
            meterR.progress = ((float)meterValue->channel_17_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case CHANNEL_CH_18:
            meterR.progress = ((float)meterValue->channel_18_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case CHANNEL_MULTI_1:
            meterR.progress = ((float)meterValue->multi_1_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case CHANNEL_MULTI_2:
            meterR.progress = ((float)meterValue->multi_2_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case CHANNEL_MULTI_3:
            meterR.progress = ((float)meterValue->multi_3_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case CHANNEL_MULTI_4:
            meterR.progress = ((float)meterValue->multi_4_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case CHANNEL_MAIN:
            meterL.progress = ((float)meterValue->main_l_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterR.progress = ((float)meterValue->main_r_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        default:
            break;
    }
}

- (void)updateChannelLabels
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [viewPeq setHidden:NO];
    [viewGeq setHidden:YES];
    [meterL setHidden:YES];
    [meterIn setHidden:NO];
    [meterOut setHidden:NO];
    [meterInL setHidden:YES];
    [meterInR setHidden:YES];
    [meterOutL setHidden:YES];
    [meterOutR setHidden:YES];
    
    [imgPanelBg setImage:[UIImage imageNamed:@"frame-17.png"]];
    [imgBackground setImage:[UIImage imageNamed:@"basemap-34.png"]];
    currentChannelLabel = currentChannel;
    
    [btnSolo setTitle:@"SOLO" forState:UIControlStateNormal];
    [btnSolo setBackgroundImage:[UIImage imageNamed:@"button-2.png"] forState:UIControlStateNormal];
    [btnSolo setTitle:@"SOLO" forState:UIControlStateSelected];
    [btnSolo setBackgroundImage:[UIImage imageNamed:@"button-4.png"] forState:UIControlStateSelected];
    
    // Refresh filter buttons
    [self updatePeq1Filter];
    [self updatePeq4Filter];
    
    switch (currentChannel) {
        case CHANNEL_CH_1:
            if ([userDefaults objectForKey:@"Ch1"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch1"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch1", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch1", @"Acapela", nil)];
            [btnSolo setHidden:NO];
            [slider setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            [btnMeterPrePost setTitle:@"METER INPUT" forState:UIControlStateNormal];
            [btnMeterPrePost setBackgroundImage:[UIImage imageNamed:@"button-6.png"] forState:UIControlStateNormal];
            [viewController setSendPage:EQ_PAGE reserve2:0];
            break;
        case CHANNEL_CH_2:
            if ([userDefaults objectForKey:@"Ch2"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch2"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch2", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch2", @"Acapela", nil)];
            [btnSolo setHidden:NO];
            [slider setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            [btnMeterPrePost setTitle:@"METER INPUT" forState:UIControlStateNormal];
            [btnMeterPrePost setBackgroundImage:[UIImage imageNamed:@"button-6.png"] forState:UIControlStateNormal];
            [viewController setSendPage:EQ_PAGE reserve2:0];
            break;
        case CHANNEL_CH_3:
            if ([userDefaults objectForKey:@"Ch3"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch3"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch3", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch3", @"Acapela", nil)];
            [btnSolo setHidden:NO];
            [slider setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            [btnMeterPrePost setTitle:@"METER INPUT" forState:UIControlStateNormal];
            [btnMeterPrePost setBackgroundImage:[UIImage imageNamed:@"button-6.png"] forState:UIControlStateNormal];
            [viewController setSendPage:EQ_PAGE reserve2:0];
            break;
        case CHANNEL_CH_4:
            if ([userDefaults objectForKey:@"Ch4"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch4"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch4", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch4", @"Acapela", nil)];
            [btnSolo setHidden:NO];
            [slider setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            [btnMeterPrePost setTitle:@"METER INPUT" forState:UIControlStateNormal];
            [btnMeterPrePost setBackgroundImage:[UIImage imageNamed:@"button-6.png"] forState:UIControlStateNormal];
            [viewController setSendPage:EQ_PAGE reserve2:0];
            break;
        case CHANNEL_CH_5:
            if ([userDefaults objectForKey:@"Ch5"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch5"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch5", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch5", @"Acapela", nil)];
            [btnSolo setHidden:NO];
            [slider setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            [btnMeterPrePost setTitle:@"METER INPUT" forState:UIControlStateNormal];
            [btnMeterPrePost setBackgroundImage:[UIImage imageNamed:@"button-6.png"] forState:UIControlStateNormal];
            [viewController setSendPage:EQ_PAGE reserve2:0];
            break;
        case CHANNEL_CH_6:
            if ([userDefaults objectForKey:@"Ch6"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch6"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch6", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch6", @"Acapela", nil)];
            [btnSolo setHidden:NO];
            [slider setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            [btnMeterPrePost setTitle:@"METER INPUT" forState:UIControlStateNormal];
            [btnMeterPrePost setBackgroundImage:[UIImage imageNamed:@"button-6.png"] forState:UIControlStateNormal];
            [viewController setSendPage:EQ_PAGE reserve2:0];
            break;
        case CHANNEL_CH_7:
            if ([userDefaults objectForKey:@"Ch7"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch7"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch7", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch7", @"Acapela", nil)];
            [btnSolo setHidden:NO];
            [slider setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            [btnMeterPrePost setTitle:@"METER INPUT" forState:UIControlStateNormal];
            [btnMeterPrePost setBackgroundImage:[UIImage imageNamed:@"button-6.png"] forState:UIControlStateNormal];
            [viewController setSendPage:EQ_PAGE reserve2:0];
            break;
        case CHANNEL_CH_8:
            if ([userDefaults objectForKey:@"Ch8"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch8"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch8", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch8", @"Acapela", nil)];
            [btnSolo setHidden:NO];
            [slider setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            [btnMeterPrePost setTitle:@"METER INPUT" forState:UIControlStateNormal];
            [btnMeterPrePost setBackgroundImage:[UIImage imageNamed:@"button-6.png"] forState:UIControlStateNormal];
            [viewController setSendPage:EQ_PAGE reserve2:0];
            break;
        case CHANNEL_CH_9:
            if ([userDefaults objectForKey:@"Ch9"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch9"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch9", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch9", @"Acapela", nil)];
            [btnSolo setHidden:NO];
            [slider setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            [btnMeterPrePost setTitle:@"METER INPUT" forState:UIControlStateNormal];
            [btnMeterPrePost setBackgroundImage:[UIImage imageNamed:@"button-6.png"] forState:UIControlStateNormal];
            [viewController setSendPage:EQ_PAGE reserve2:0];
            break;
        case CHANNEL_CH_10:
            if ([userDefaults objectForKey:@"Ch10"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch10"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch10", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch10", @"Acapela", nil)];
            [btnSolo setHidden:NO];
            [slider setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            [btnMeterPrePost setTitle:@"METER INPUT" forState:UIControlStateNormal];
            [btnMeterPrePost setBackgroundImage:[UIImage imageNamed:@"button-6.png"] forState:UIControlStateNormal];
            [viewController setSendPage:EQ_PAGE reserve2:0];
            break;
        case CHANNEL_CH_11:
            if ([userDefaults objectForKey:@"Ch11"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch11"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch11", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch11", @"Acapela", nil)];
            [btnSolo setHidden:NO];
            [slider setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            [btnMeterPrePost setTitle:@"METER INPUT" forState:UIControlStateNormal];
            [btnMeterPrePost setBackgroundImage:[UIImage imageNamed:@"button-6.png"] forState:UIControlStateNormal];
            [viewController setSendPage:EQ_PAGE reserve2:0];
            break;
        case CHANNEL_CH_12:
            if ([userDefaults objectForKey:@"Ch12"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch12"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch12", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch12", @"Acapela", nil)];
            [btnSolo setHidden:NO];
            [slider setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            [btnMeterPrePost setTitle:@"METER INPUT" forState:UIControlStateNormal];
            [btnMeterPrePost setBackgroundImage:[UIImage imageNamed:@"button-6.png"] forState:UIControlStateNormal];
            [viewController setSendPage:EQ_PAGE reserve2:0];
            break;
        case CHANNEL_CH_13:
            if ([userDefaults objectForKey:@"Ch13"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch13"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch13", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch13", @"Acapela", nil)];
            [btnSolo setHidden:NO];
            [slider setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            [btnMeterPrePost setTitle:@"METER INPUT" forState:UIControlStateNormal];
            [btnMeterPrePost setBackgroundImage:[UIImage imageNamed:@"button-6.png"] forState:UIControlStateNormal];
            [viewController setSendPage:EQ_PAGE reserve2:0];
            break;
        case CHANNEL_CH_14:
            if ([userDefaults objectForKey:@"Ch14"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch14"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch14", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch14", @"Acapela", nil)];
            [btnSolo setHidden:NO];
            [slider setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            [btnMeterPrePost setTitle:@"METER INPUT" forState:UIControlStateNormal];
            [btnMeterPrePost setBackgroundImage:[UIImage imageNamed:@"button-6.png"] forState:UIControlStateNormal];
            [viewController setSendPage:EQ_PAGE reserve2:0];
            break;
        case CHANNEL_CH_15:
            if ([userDefaults objectForKey:@"Ch15"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch15"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch15", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch15", @"Acapela", nil)];
            [btnSolo setHidden:NO];
            [slider setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            [btnMeterPrePost setTitle:@"METER INPUT" forState:UIControlStateNormal];
            [btnMeterPrePost setBackgroundImage:[UIImage imageNamed:@"button-6.png"] forState:UIControlStateNormal];
            [viewController setSendPage:EQ_PAGE reserve2:0];
            break;
        case CHANNEL_CH_16:
            if ([userDefaults objectForKey:@"Ch16"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch16"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch16", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch16", @"Acapela", nil)];
            [btnSolo setHidden:NO];
            [slider setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            [btnMeterPrePost setTitle:@"METER INPUT" forState:UIControlStateNormal];
            [btnMeterPrePost setBackgroundImage:[UIImage imageNamed:@"button-6.png"] forState:UIControlStateNormal];
            [viewController setSendPage:EQ_PAGE reserve2:0];
            break;
        case CHANNEL_CH_17:
            if ([userDefaults objectForKey:@"Ch17"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch17"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch17", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch17", @"Acapela", nil)];
            [btnSolo setHidden:NO];
            [slider setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            [btnMeterPrePost setTitle:@"METER INPUT" forState:UIControlStateNormal];
            [btnMeterPrePost setBackgroundImage:[UIImage imageNamed:@"button-6.png"] forState:UIControlStateNormal];
            [viewController setSendPage:EQ_PAGE reserve2:0];
            break;
        case CHANNEL_CH_18:
            if ([userDefaults objectForKey:@"Ch18"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch18"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch18", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch18", @"Acapela", nil)];
            [btnSolo setHidden:NO];
            [slider setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            [btnMeterPrePost setTitle:@"METER INPUT" forState:UIControlStateNormal];
            [btnMeterPrePost setBackgroundImage:[UIImage imageNamed:@"button-6.png"] forState:UIControlStateNormal];
            [viewController setSendPage:EQ_PAGE reserve2:0];
            break;
        case CHANNEL_MULTI_1:
            if ([userDefaults objectForKey:@"Multi1"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Multi1"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Multi1", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Multi1", @"Acapela", nil)];
            [btnSolo setHidden:YES];
            [slider setThumbImage:[UIImage imageNamed:@"fader-4.png"] forState:UIControlStateNormal];
            [btnMeterPrePost setTitle:@"METER PRE" forState:UIControlStateNormal];
            [btnMeterPrePost setBackgroundImage:[UIImage imageNamed:@"button-3.png"] forState:UIControlStateNormal];
            [viewController setSendPage:EQ_PAGE reserve2:1];
            break;
        case CHANNEL_MULTI_2:
            if ([userDefaults objectForKey:@"Multi2"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Multi2"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Multi2", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Multi2", @"Acapela", nil)];
            [btnSolo setHidden:YES];
            [slider setThumbImage:[UIImage imageNamed:@"fader-4.png"] forState:UIControlStateNormal];
            [btnMeterPrePost setTitle:@"METER PRE" forState:UIControlStateNormal];
            [btnMeterPrePost setBackgroundImage:[UIImage imageNamed:@"button-3.png"] forState:UIControlStateNormal];
            [viewController setSendPage:EQ_PAGE reserve2:1];
            break;
        case CHANNEL_MULTI_3:
            if ([userDefaults objectForKey:@"Multi3"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Multi3"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Multi3", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Multi3", @"Acapela", nil)];
            [btnSolo setHidden:YES];
            [slider setThumbImage:[UIImage imageNamed:@"fader-4.png"] forState:UIControlStateNormal];
            [btnMeterPrePost setTitle:@"METER PRE" forState:UIControlStateNormal];
            [btnMeterPrePost setBackgroundImage:[UIImage imageNamed:@"button-3.png"] forState:UIControlStateNormal];
            [viewController setSendPage:EQ_PAGE reserve2:1];
            break;
        case CHANNEL_MULTI_4:
            if ([userDefaults objectForKey:@"Multi4"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Multi4"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Multi4", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Multi4", @"Acapela", nil)];
            [btnSolo setHidden:YES];
            [slider setThumbImage:[UIImage imageNamed:@"fader-4.png"] forState:UIControlStateNormal];
            [btnMeterPrePost setTitle:@"METER PRE" forState:UIControlStateNormal];
            [btnMeterPrePost setBackgroundImage:[UIImage imageNamed:@"button-3.png"] forState:UIControlStateNormal];
            [viewController setSendPage:EQ_PAGE reserve2:1];
            break;
        case CHANNEL_MAIN:
            if (mainEqType == MAIN_EQ_PEQ) {
                [viewPeq setHidden:NO];
                [viewGeq setHidden:YES];
            } else {
                [viewPeq setHidden:YES];
                [viewGeq setHidden:NO];
            }
            
            if ([userDefaults objectForKey:@"Main"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Main"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Main", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Main", @"Acapela", nil)];
            [btnSolo setHidden:NO];
            [btnSolo setTitle:@"STEREO" forState:UIControlStateNormal];
            [btnSolo setBackgroundImage:[UIImage imageNamed:@"button-4.png"] forState:UIControlStateNormal];
            [btnSolo setTitle:@"MONO" forState:UIControlStateSelected];
            [btnSolo setBackgroundImage:[UIImage imageNamed:@"button-5.png"] forState:UIControlStateSelected];
            [slider setThumbImage:[UIImage imageNamed:@"fader-3.png"] forState:UIControlStateNormal];
            [meterL setHidden:NO];
            [meterIn setHidden:YES];
            [meterOut setHidden:YES];
            if (mainEqType == MAIN_EQ_PEQ) {
                [meterInL setHidden:NO];
                [meterInR setHidden:NO];
                [meterOutL setHidden:NO];
                [meterOutR setHidden:NO];
            }
            [imgPanelBg setImage:[UIImage imageNamed:@"frame-17.png"]];
            [btnMeterPrePost setTitle:@"METER PRE" forState:UIControlStateNormal];
            [btnMeterPrePost setBackgroundImage:[UIImage imageNamed:@"button-3.png"] forState:UIControlStateNormal];
            [imgBackground setImage:[UIImage imageNamed:@"basemap-33.png"]];
            [viewController setSendPage:EQ_PAGE reserve2:2];
            break;
        default:
            break;
    }
}

- (void)updatePeqPlot:(BOOL)enabled
{
    CPTGraph *graph = self.hostView.hostedGraph;
    CPTScatterPlot *peqPlot = (CPTScatterPlot *)[graph plotAtIndex:0];
    if (enabled) {
        peqPlot.areaFill = [CPTFill fillWithColor:[CPTColor colorWithComponentRed:46.0/255.0 green:146.0/255.0 blue:43.0/255.0 alpha:1.0]];
        
        // Create styles and symbols
        CPTMutableLineStyle *peqLineStyle = [peqPlot.dataLineStyle mutableCopy];
        peqLineStyle.lineWidth = 5.0;
        peqLineStyle.lineColor = [CPTColor colorWithComponentRed:0.0 green:254.0/255.0 blue:82.0/255.0 alpha:1.0];
        peqPlot.dataLineStyle = peqLineStyle;
    } else {
        peqPlot.areaFill = [CPTFill fillWithColor:[CPTColor colorWithComponentRed:197.0/255.0 green:197.0/255.0 blue:197.0/255.0 alpha:1.0]];
        
        // Create styles and symbols
        CPTMutableLineStyle *peqLineStyle = [peqPlot.dataLineStyle mutableCopy];
        peqLineStyle.lineWidth = 5.0;
        peqLineStyle.lineColor = [CPTColor colorWithComponentRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1.0];
        peqPlot.dataLineStyle = peqLineStyle;
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self initPlot];
    [self updatePeqFrame];
}

// Update image preview
- (void)updatePreviewImage
{
    if (!initDone) {
        NSLog(@"EQ updatePreviewImage: Init not done!");
        return;
    }
    
    NSLog(@"EQ updatePreviewImage for channel %d", currentChannel);
    
    // Main view EQ preview image
    UIImage *baseImage = [UIImage imageNamed:@"pan-1.png"];
    UIImage *previewImage, *finalImage;
    
    UIGraphicsBeginImageContext(baseImage.size);
    [baseImage drawAtPoint:CGPointMake(0, 0)];
    
    if (currentChannel == MAIN && mainEqType == MAIN_EQ_GEQ) {
        previewImage = [Global imageWithView:self.viewGeqFrame scaledToSize:CGSizeMake(85, 85)];
    } else {
        previewImage = [Global imageWithView:self.hostView scaledToSize:CGSizeMake(85, 85)];
    }
    [previewImage drawAtPoint:CGPointMake(2, 2)];
    
    finalImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    if (finalImage != nil) {
        [peqImages replaceObjectAtIndex:currentChannel withObject:finalImage];
    } else {
        NSLog(@"EQ updatePreviewImage: NULL preview image!");
    }
    
    // View page EQ preview image
    if (currentChannel == MAIN && mainEqType == MAIN_EQ_GEQ) {
        UIGraphicsBeginImageContext(viewGeqFrame.frame.size);
        [self.viewGeqFrame.layer renderInContext:UIGraphicsGetCurrentContext()];
    } else {
        UIGraphicsBeginImageContext(hostView.frame.size);
        [self.hostView.layer renderInContext:UIGraphicsGetCurrentContext()];
    }
    
    finalImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    if (finalImage != nil) {
        [eqPreviewImages replaceObjectAtIndex:currentChannel withObject:finalImage];
    } else {
        NSLog(@"EQ updatePreviewImage: NULL preview image!");
    }
    
    // Update delegate image
    [self.delegate didFinishEditing];
}

// Update image preview
- (void)updatePreviewImageForChannel:(int)channel
{
    if (!initDone) {
        NSLog(@"EQ updatePreviewImageForChannel: Init not done!");
        return;
    }
    
    NSLog(@"EQ updatePreviewImageForChannel for channel %d", channel);
    
    UIImage *baseImage = [UIImage imageNamed:@"pan-1.png"];
    UIImage *previewImage, *finalImage;
    
    // Main view EQ preview image
    UIGraphicsBeginImageContext(baseImage.size);
    [baseImage drawAtPoint:CGPointMake(0, 0)];
    
    if (channel == MAIN && mainEqType == MAIN_EQ_GEQ) {
        previewImage = [Global imageWithView:self.viewGeqFrame scaledToSize:CGSizeMake(85, 85)];
    } else {
        previewImage = [Global imageWithView:self.hostView scaledToSize:CGSizeMake(85, 85)];
    }
    [previewImage drawAtPoint:CGPointMake(2, 2)];
    
    finalImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    if (finalImage != nil) {
        [peqImages replaceObjectAtIndex:channel withObject:finalImage];
    } else {
        NSLog(@"EQ updatePreviewImageForChannel: NULL preview image!");
    }
    
    // View page EQ preview image
    if (channel == MAIN && mainEqType == MAIN_EQ_GEQ) {
        UIGraphicsBeginImageContext(viewGeqFrame.frame.size);
        [self.viewGeqFrame.layer renderInContext:UIGraphicsGetCurrentContext()];
    } else {
        UIGraphicsBeginImageContext(hostView.frame.size);
        [self.hostView.layer renderInContext:UIGraphicsGetCurrentContext()];
    }
    
    finalImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    if (finalImage != nil) {
        [eqPreviewImages replaceObjectAtIndex:channel withObject:finalImage];
    } else {
        NSLog(@"EQ updatePreviewImageForChannel: NULL preview image!");
    }
    
    // Update delegate image
    [self.delegate didFinishEditing];
}

// Convert PEQ x-coordinate to frequency index
- (int)peqXToFreq:(CGFloat)ptX
{
    return (int)(ptX * EQ_FREQ_MAX_VALUE/PEQ_FRAME_WIDTH);
}

// Convert PEQ y-coordinate to gain index
- (int)peqYToGain:(CGFloat)ptY
{
    return (int)(ptY * EQ_GAIN_MAX_VALUE/PEQ_FRAME_HEIGHT);
}

// Convert frequency index to PEQ x-coordinate 
- (CGFloat)peqFreqToX:(int)freq
{
    return freq * PEQ_FRAME_WIDTH/EQ_FREQ_MAX_VALUE;
}

// Convert gain index to PEQ y-coordinate
- (CGFloat)peqGainToY:(int)gain
{
    return (EQ_GAIN_MAX_VALUE - gain) * PEQ_FRAME_HEIGHT/EQ_GAIN_MAX_VALUE;
}

#pragma mark -
#pragma mark Right panel event handler methods

-(void)releaseMainEqLock:(NSTimer *)timer {
    NSLog(@"Released main EQ lock");
    mainEqLock = NO;
}

- (IBAction)onBtnSelectDown:(id)sender {
    // Refresh preview image
    [self updatePreviewImage];
    
    if (currentChannel > CHANNEL_CH_1) {
        currentChannel--;
        currentChannelLabel = currentChannel;
    }
    [self updateChannelLabels];
    [self updatePeqFrame];
}

- (IBAction)onBtnSelectUp:(id)sender {
    // Refresh preview image
    [self updatePreviewImage];
    
    if (currentChannel < CHANNEL_MAIN) {
        currentChannel++;
        currentChannelLabel = currentChannel;
    }
    [self updateChannelLabels];
    [self updatePeqFrame];
}

- (IBAction)onBtnOn:(id)sender {
    btnOn.selected = !btnOn.selected;
    switch (currentChannel) {
        case CHANNEL_CH_1:
            [Global setClearBit:&_chOnOff bitValue:btnOn.selected bitOrder:0];
            [viewController sendData:CMD_CH_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_chOnOff];
            break;
        case CHANNEL_CH_2:
            [Global setClearBit:&_chOnOff bitValue:btnOn.selected bitOrder:1];
            [viewController sendData:CMD_CH_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_chOnOff];
            break;
        case CHANNEL_CH_3:
            [Global setClearBit:&_chOnOff bitValue:btnOn.selected bitOrder:2];
            [viewController sendData:CMD_CH_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_chOnOff];
            break;
        case CHANNEL_CH_4:
            [Global setClearBit:&_chOnOff bitValue:btnOn.selected bitOrder:3];
            [viewController sendData:CMD_CH_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_chOnOff];
            break;
        case CHANNEL_CH_5:
            [Global setClearBit:&_chOnOff bitValue:btnOn.selected bitOrder:4];
            [viewController sendData:CMD_CH_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_chOnOff];
            break;
        case CHANNEL_CH_6:
            [Global setClearBit:&_chOnOff bitValue:btnOn.selected bitOrder:5];
            [viewController sendData:CMD_CH_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_chOnOff];
            break;
        case CHANNEL_CH_7:
            [Global setClearBit:&_chOnOff bitValue:btnOn.selected bitOrder:6];
            [viewController sendData:CMD_CH_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_chOnOff];
            break;
        case CHANNEL_CH_8:
            [Global setClearBit:&_chOnOff bitValue:btnOn.selected bitOrder:7];
            [viewController sendData:CMD_CH_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_chOnOff];
            break;
        case CHANNEL_CH_9:
            [Global setClearBit:&_chOnOff bitValue:btnOn.selected bitOrder:8];
            [viewController sendData:CMD_CH_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_chOnOff];
            break;
        case CHANNEL_CH_10:
            [Global setClearBit:&_chOnOff bitValue:btnOn.selected bitOrder:9];
            [viewController sendData:CMD_CH_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_chOnOff];
            break;
        case CHANNEL_CH_11:
            [Global setClearBit:&_chOnOff bitValue:btnOn.selected bitOrder:10];
            [viewController sendData:CMD_CH_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_chOnOff];
            break;
        case CHANNEL_CH_12:
            [Global setClearBit:&_chOnOff bitValue:btnOn.selected bitOrder:11];
            [viewController sendData:CMD_CH_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_chOnOff];
            break;
        case CHANNEL_CH_13:
            [Global setClearBit:&_chOnOff bitValue:btnOn.selected bitOrder:12];
            [viewController sendData:CMD_CH_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_chOnOff];
            break;
        case CHANNEL_CH_14:
            [Global setClearBit:&_chOnOff bitValue:btnOn.selected bitOrder:13];
            [viewController sendData:CMD_CH_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_chOnOff];
            break;
        case CHANNEL_CH_15:
            [Global setClearBit:&_chOnOff bitValue:btnOn.selected bitOrder:14];
            [viewController sendData:CMD_CH_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_chOnOff];
            break;
        case CHANNEL_CH_16:
            [Global setClearBit:&_chOnOff bitValue:btnOn.selected bitOrder:15];
            [viewController sendData:CMD_CH_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_chOnOff];
            break;
        case CHANNEL_CH_17:
            [Global setClearBit:&_ch17Setting bitValue:btnOn.selected bitOrder:8];
            [viewController sendData:CMD_CH17_USB_AUDIO_SETTING commandType:DSP_WRITE dspId:DSP_5 value:_ch17Setting];
            break;
        case CHANNEL_CH_18:
            [Global setClearBit:&_ch18Setting bitValue:btnOn.selected bitOrder:8];
            [viewController sendData:CMD_CH18_USB_AUDIO_SETTING commandType:DSP_WRITE dspId:DSP_5 value:_ch18Setting];
            break;
        case CHANNEL_MULTI_1:
            [Global setClearBit:&_multi1Ctrl bitValue:btnOn.selected bitOrder:0];
            [viewController sendData:CMD_MULTI_1_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi1Ctrl];
            break;
        case CHANNEL_MULTI_2:
            [Global setClearBit:&_multi2Ctrl bitValue:btnOn.selected bitOrder:0];
            [viewController sendData:CMD_MULTI_2_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi2Ctrl];
            break;
        case CHANNEL_MULTI_3:
            [Global setClearBit:&_multi3Ctrl bitValue:btnOn.selected bitOrder:0];
            [viewController sendData:CMD_MULTI_3_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi3Ctrl];
            break;
        case CHANNEL_MULTI_4:
            [Global setClearBit:&_multi4Ctrl bitValue:btnOn.selected bitOrder:0];
            [viewController sendData:CMD_MULTI_4_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi4Ctrl];
            break;
        case CHANNEL_MAIN:
            [Global setClearBit:&_mainCtrl bitValue:btnOn.selected bitOrder:0];
            [viewController sendData:CMD_MAIN_LR_CTRL commandType:DSP_WRITE dspId:DSP_6 value:_mainCtrl];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnMeterPrePost:(id)sender {
    btnMeterPrePost.selected = !btnMeterPrePost.selected;
    switch (currentChannel) {
        case CHANNEL_CH_1:
            [Global setClearBit:&_chMeterPrePost bitValue:btnMeterPrePost.selected bitOrder:0];
            [viewController sendData:CMD_CH_METER commandType:DSP_WRITE dspId:DSP_5 value:_chMeterPrePost];
            break;
        case CHANNEL_CH_2:
            [Global setClearBit:&_chMeterPrePost bitValue:btnMeterPrePost.selected bitOrder:1];
            [viewController sendData:CMD_CH_METER commandType:DSP_WRITE dspId:DSP_5 value:_chMeterPrePost];
            break;
        case CHANNEL_CH_3:
            [Global setClearBit:&_chMeterPrePost bitValue:btnMeterPrePost.selected bitOrder:2];
            [viewController sendData:CMD_CH_METER commandType:DSP_WRITE dspId:DSP_5 value:_chMeterPrePost];
            break;
        case CHANNEL_CH_4:
            [Global setClearBit:&_chMeterPrePost bitValue:btnMeterPrePost.selected bitOrder:3];
            [viewController sendData:CMD_CH_METER commandType:DSP_WRITE dspId:DSP_5 value:_chMeterPrePost];
            break;
        case CHANNEL_CH_5:
            [Global setClearBit:&_chMeterPrePost bitValue:btnMeterPrePost.selected bitOrder:4];
            [viewController sendData:CMD_CH_METER commandType:DSP_WRITE dspId:DSP_5 value:_chMeterPrePost];
            break;
        case CHANNEL_CH_6:
            [Global setClearBit:&_chMeterPrePost bitValue:btnMeterPrePost.selected bitOrder:5];
            [viewController sendData:CMD_CH_METER commandType:DSP_WRITE dspId:DSP_5 value:_chMeterPrePost];
            break;
        case CHANNEL_CH_7:
            [Global setClearBit:&_chMeterPrePost bitValue:btnMeterPrePost.selected bitOrder:6];
            [viewController sendData:CMD_CH_METER commandType:DSP_WRITE dspId:DSP_5 value:_chMeterPrePost];
            break;
        case CHANNEL_CH_8:
            [Global setClearBit:&_chMeterPrePost bitValue:btnMeterPrePost.selected bitOrder:7];
            [viewController sendData:CMD_CH_METER commandType:DSP_WRITE dspId:DSP_5 value:_chMeterPrePost];
            break;
        case CHANNEL_CH_9:
            [Global setClearBit:&_chMeterPrePost bitValue:btnMeterPrePost.selected bitOrder:8];
            [viewController sendData:CMD_CH_METER commandType:DSP_WRITE dspId:DSP_5 value:_chMeterPrePost];
            break;
        case CHANNEL_CH_10:
            [Global setClearBit:&_chMeterPrePost bitValue:btnMeterPrePost.selected bitOrder:9];
            [viewController sendData:CMD_CH_METER commandType:DSP_WRITE dspId:DSP_5 value:_chMeterPrePost];
            break;
        case CHANNEL_CH_11:
            [Global setClearBit:&_chMeterPrePost bitValue:btnMeterPrePost.selected bitOrder:10];
            [viewController sendData:CMD_CH_METER commandType:DSP_WRITE dspId:DSP_5 value:_chMeterPrePost];
            break;
        case CHANNEL_CH_12:
            [Global setClearBit:&_chMeterPrePost bitValue:btnMeterPrePost.selected bitOrder:11];
            [viewController sendData:CMD_CH_METER commandType:DSP_WRITE dspId:DSP_5 value:_chMeterPrePost];
            break;
        case CHANNEL_CH_13:
            [Global setClearBit:&_chMeterPrePost bitValue:btnMeterPrePost.selected bitOrder:12];
            [viewController sendData:CMD_CH_METER commandType:DSP_WRITE dspId:DSP_5 value:_chMeterPrePost];
            break;
        case CHANNEL_CH_14:
            [Global setClearBit:&_chMeterPrePost bitValue:btnMeterPrePost.selected bitOrder:13];
            [viewController sendData:CMD_CH_METER commandType:DSP_WRITE dspId:DSP_5 value:_chMeterPrePost];
            break;
        case CHANNEL_CH_15:
            [Global setClearBit:&_chMeterPrePost bitValue:btnMeterPrePost.selected bitOrder:14];
            [viewController sendData:CMD_CH_METER commandType:DSP_WRITE dspId:DSP_5 value:_chMeterPrePost];
            break;
        case CHANNEL_CH_16:
            [Global setClearBit:&_chMeterPrePost bitValue:btnMeterPrePost.selected bitOrder:15];
            [viewController sendData:CMD_CH_METER commandType:DSP_WRITE dspId:DSP_5 value:_chMeterPrePost];
            break;
        case CHANNEL_CH_17:
            [Global setClearBit:&_ch17Setting bitValue:btnMeterPrePost.selected bitOrder:15];
            [viewController sendData:CMD_CH17_USB_AUDIO_SETTING commandType:DSP_WRITE dspId:DSP_5 value:_ch17Setting];
            break;
        case CHANNEL_CH_18:
            [Global setClearBit:&_ch18Setting bitValue:btnMeterPrePost.selected bitOrder:15];
            [viewController sendData:CMD_CH18_USB_AUDIO_SETTING commandType:DSP_WRITE dspId:DSP_5 value:_ch18Setting];
            break;
        case CHANNEL_MULTI_1:
            [Global setClearBit:&_multiMeter bitValue:btnMeterPrePost.selected bitOrder:0];
            [viewController sendData:CMD_MULTI_METER commandType:DSP_WRITE dspId:DSP_7 value:_multiMeter];
            break;
        case CHANNEL_MULTI_2:
            [Global setClearBit:&_multiMeter bitValue:btnMeterPrePost.selected bitOrder:1];
            [viewController sendData:CMD_MULTI_METER commandType:DSP_WRITE dspId:DSP_7 value:_multiMeter];
            break;
        case CHANNEL_MULTI_3:
            [Global setClearBit:&_multiMeter bitValue:btnMeterPrePost.selected bitOrder:2];
            [viewController sendData:CMD_MULTI_METER commandType:DSP_WRITE dspId:DSP_7 value:_multiMeter];
            break;
        case CHANNEL_MULTI_4:
            [Global setClearBit:&_multiMeter bitValue:btnMeterPrePost.selected bitOrder:3];
            [viewController sendData:CMD_MULTI_METER commandType:DSP_WRITE dspId:DSP_7 value:_multiMeter];
            break;
        case CHANNEL_MAIN:
            [Global setClearBit:&_mainMeter bitValue:btnMeterPrePost.selected bitOrder:0];
            [viewController sendData:CMD_MAIN_METER commandType:DSP_WRITE dspId:DSP_6 value:_mainMeter];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnSolo:(id)sender {
    btnSolo.selected = !btnSolo.selected;
    switch (currentChannel) {
        case CHANNEL_CH_1:
            [Global setClearBit:&_soloCtrl bitValue:btnSolo.selected bitOrder:0];
            [viewController sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloCtrl];
            break;
        case CHANNEL_CH_2:
            [Global setClearBit:&_soloCtrl bitValue:btnSolo.selected bitOrder:1];
            [viewController sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloCtrl];
            break;
        case CHANNEL_CH_3:
            [Global setClearBit:&_soloCtrl bitValue:btnSolo.selected bitOrder:2];
            [viewController sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloCtrl];
            break;
        case CHANNEL_CH_4:
            [Global setClearBit:&_soloCtrl bitValue:btnSolo.selected bitOrder:3];
            [viewController sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloCtrl];
            break;
        case CHANNEL_CH_5:
            [Global setClearBit:&_soloCtrl bitValue:btnSolo.selected bitOrder:4];
            [viewController sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloCtrl];
            break;
        case CHANNEL_CH_6:
            [Global setClearBit:&_soloCtrl bitValue:btnSolo.selected bitOrder:5];
            [viewController sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloCtrl];
            break;
        case CHANNEL_CH_7:
            [Global setClearBit:&_soloCtrl bitValue:btnSolo.selected bitOrder:6];
            [viewController sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloCtrl];
            break;
        case CHANNEL_CH_8:
            [Global setClearBit:&_soloCtrl bitValue:btnSolo.selected bitOrder:7];
            [viewController sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloCtrl];
            break;
        case CHANNEL_CH_9:
            [Global setClearBit:&_soloCtrl bitValue:btnSolo.selected bitOrder:8];
            [viewController sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloCtrl];
            break;
        case CHANNEL_CH_10:
            [Global setClearBit:&_soloCtrl bitValue:btnSolo.selected bitOrder:9];
            [viewController sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloCtrl];
            break;
        case CHANNEL_CH_11:
            [Global setClearBit:&_soloCtrl bitValue:btnSolo.selected bitOrder:10];
            [viewController sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloCtrl];
            break;
        case CHANNEL_CH_12:
            [Global setClearBit:&_soloCtrl bitValue:btnSolo.selected bitOrder:11];
            [viewController sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloCtrl];
            break;
        case CHANNEL_CH_13:
            [Global setClearBit:&_soloCtrl bitValue:btnSolo.selected bitOrder:12];
            [viewController sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloCtrl];
            break;
        case CHANNEL_CH_14:
            [Global setClearBit:&_soloCtrl bitValue:btnSolo.selected bitOrder:13];
            [viewController sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloCtrl];
            break;
        case CHANNEL_CH_15:
            [Global setClearBit:&_soloCtrl bitValue:btnSolo.selected bitOrder:14];
            [viewController sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloCtrl];
            break;
        case CHANNEL_CH_16:
            [Global setClearBit:&_soloCtrl bitValue:btnSolo.selected bitOrder:15];
            [viewController sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloCtrl];
            break;
        case CHANNEL_CH_17:
            [Global setClearBit:&_ch17Setting bitValue:btnSolo.selected bitOrder:10];
            [viewController sendData:CMD_CH17_USB_AUDIO_SETTING commandType:DSP_WRITE dspId:DSP_5 value:_ch17Setting];
            break;
        case CHANNEL_CH_18:
            [Global setClearBit:&_ch18Setting bitValue:btnSolo.selected bitOrder:10];
            [viewController sendData:CMD_CH18_USB_AUDIO_SETTING commandType:DSP_WRITE dspId:DSP_5 value:_ch18Setting];
            break;
        case CHANNEL_MAIN:
            [Global setClearBit:&_mainMeter bitValue:btnSolo.selected bitOrder:7];
            [viewController sendData:CMD_MAIN_METER commandType:DSP_WRITE dspId:DSP_6 value:_mainMeter];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnMixer:(id)sender {
    [self.view setHidden:YES];
    [self.delegate didFinishEditing];
}

- (IBAction)onBtnFile:(id)sender {
    if (currentChannel == MAIN && mainEqType == MAIN_EQ_GEQ) {
        [viewController setFileSceneMode:SCENE_MODE_GEQ sceneChannel:0];
    } else {
        int sceneChannel;
        switch (currentChannel) {
            case CHANNEL_CH_17:
                sceneChannel = SCENE_CHANNEL_CH_17;
                break;
            case CHANNEL_CH_18:
                sceneChannel = SCENE_CHANNEL_CH_18;
                break;
            case CHANNEL_MULTI_1:
                sceneChannel = SCENE_CHANNEL_MULTI_1;
                break;
            case CHANNEL_MULTI_2:
                sceneChannel = SCENE_CHANNEL_MULTI_2;
                break;
            case CHANNEL_MULTI_3:
                sceneChannel = SCENE_CHANNEL_MULTI_3;
                break;
            case CHANNEL_MULTI_4:
                sceneChannel = SCENE_CHANNEL_MULTI_4;
                break;
            case CHANNEL_MAIN:
                sceneChannel = SCENE_CHANNEL_MAIN;
                break;
            default:
                sceneChannel = currentChannel;
                break;
        }
        
        [viewController setFileSceneMode:SCENE_MODE_PEQ sceneChannel:sceneChannel];
    }
    [viewController.view bringSubviewToFront:viewController.viewScenes];
    [viewController.viewScenes setHidden:NO];
}

- (IBAction)onBtnBack:(id)sender {
    // Refresh preview image
    [self updatePreviewImage];

    [self.delegate didFinishEditing];
    [self.view setHidden:YES];
    [viewController restoreSendPage];
}

- (IBAction)onBtnReset:(id)sender {
    if (currentChannel == MAIN) {
        if (mainEqType == MAIN_EQ_GEQ)
            [self resetGeqDataDefault];
        else
            [self resetPeqDataDefault];
    } else {
        [self resetPeqDataDefault];
    }
    [self updatePreviewImage];
}

- (IBAction)onSliderBegin:(id)sender {
    sliderLock = YES;
}

- (IBAction)onSliderEnd:(id)sender {
    sliderLock = NO;
}

- (IBAction)onSliderAction:(id)sender {
    [lblSliderValue setText:[Global getSliderGain:slider.value appendDb:YES]];
    if (sender != nil) {
        switch (currentChannel) {
            case CHANNEL_CH_1:
                [viewController sendData:CMD_CH1_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:slider.value];
                break;
            case CHANNEL_CH_2:
                [viewController sendData:CMD_CH2_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:slider.value];
                break;
            case CHANNEL_CH_3:
                [viewController sendData:CMD_CH3_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:slider.value];
                break;
            case CHANNEL_CH_4:
                [viewController sendData:CMD_CH4_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:slider.value];
                break;
            case CHANNEL_CH_5:
                [viewController sendData:CMD_CH5_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:slider.value];
                break;
            case CHANNEL_CH_6:
                [viewController sendData:CMD_CH6_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:slider.value];
                break;
            case CHANNEL_CH_7:
                [viewController sendData:CMD_CH7_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:slider.value];
                break;
            case CHANNEL_CH_8:
                [viewController sendData:CMD_CH8_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:slider.value];
                break;
            case CHANNEL_CH_9:
                [viewController sendData:CMD_CH9_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:slider.value];
                break;
            case CHANNEL_CH_10:
                [viewController sendData:CMD_CH10_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:slider.value];
                break;
            case CHANNEL_CH_11:
                [viewController sendData:CMD_CH11_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:slider.value];
                break;
            case CHANNEL_CH_12:
                [viewController sendData:CMD_CH12_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:slider.value];
                break;
            case CHANNEL_CH_13:
                [viewController sendData:CMD_CH13_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:slider.value];
                break;
            case CHANNEL_CH_14:
                [viewController sendData:CMD_CH14_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:slider.value];
                break;
            case CHANNEL_CH_15:
                [viewController sendData:CMD_CH15_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:slider.value];
                break;
            case CHANNEL_CH_16:
                [viewController sendData:CMD_CH16_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:slider.value];
                break;
            case CHANNEL_CH_17:
                [viewController sendData:CMD_CH17_USB_AUDIO_FADER commandType:DSP_WRITE dspId:DSP_5 value:slider.value];
                break;
            case CHANNEL_CH_18:
                [viewController sendData:CMD_CH18_USB_AUDIO_FADER commandType:DSP_WRITE dspId:DSP_5 value:slider.value];
                break;
            case CHANNEL_MULTI_1:
                [viewController sendData:CMD_MULTI1_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_7 value:slider.value];
                break;
            case CHANNEL_MULTI_2:
                [viewController sendData:CMD_MULTI2_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_7 value:slider.value];
                break;
            case CHANNEL_MULTI_3:
                [viewController sendData:CMD_MULTI3_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_7 value:slider.value];
                break;
            case CHANNEL_MULTI_4:
                [viewController sendData:CMD_MULTI4_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_7 value:slider.value];
                break;
            case CHANNEL_MAIN:
                [viewController sendData:CMD_MAIN_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_6 value:slider.value];
                break;
            default:
                break;
        }
    }
}

#pragma mark -
#pragma mark PEQ event handler methods

- (IBAction)onBtnPeqOnOff:(id)sender {
    btnPeqOnOff.selected = !btnPeqOnOff.selected;
    peqEnabled[currentChannel] = btnPeqOnOff.selected;
    [self updatePeqPlot:btnPeqOnOff.selected];
    [self updatePreviewImage];
    
    switch (currentChannel) {
        case CHANNEL_CH_1:
            [Global setClearBit:&_ch1Ctrl bitValue:peqEnabled[currentChannel] bitOrder:7];
            [viewController sendData:CMD_CH1_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch1Ctrl];
            break;
        case CHANNEL_CH_2:
            [Global setClearBit:&_ch2Ctrl bitValue:peqEnabled[currentChannel] bitOrder:7];
            [viewController sendData:CMD_CH2_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch2Ctrl];
            break;
        case CHANNEL_CH_3:
            [Global setClearBit:&_ch3Ctrl bitValue:peqEnabled[currentChannel] bitOrder:7];
            [viewController sendData:CMD_CH3_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch3Ctrl];
            break;
        case CHANNEL_CH_4:
            [Global setClearBit:&_ch4Ctrl bitValue:peqEnabled[currentChannel] bitOrder:7];
            [viewController sendData:CMD_CH4_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch4Ctrl];
            break;
        case CHANNEL_CH_5:
            [Global setClearBit:&_ch5Ctrl bitValue:peqEnabled[currentChannel] bitOrder:7];
            [viewController sendData:CMD_CH5_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch5Ctrl];
            break;
        case CHANNEL_CH_6:
            [Global setClearBit:&_ch6Ctrl bitValue:peqEnabled[currentChannel] bitOrder:7];
            [viewController sendData:CMD_CH6_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch6Ctrl];
            break;
        case CHANNEL_CH_7:
            [Global setClearBit:&_ch7Ctrl bitValue:peqEnabled[currentChannel] bitOrder:7];
            [viewController sendData:CMD_CH7_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch7Ctrl];
            break;
        case CHANNEL_CH_8:
            [Global setClearBit:&_ch8Ctrl bitValue:peqEnabled[currentChannel] bitOrder:7];
            [viewController sendData:CMD_CH8_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch8Ctrl];
            break;
        case CHANNEL_CH_9:
            [Global setClearBit:&_ch9Ctrl bitValue:peqEnabled[currentChannel] bitOrder:7];
            [viewController sendData:CMD_CH9_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch9Ctrl];
            break;
        case CHANNEL_CH_10:
            [Global setClearBit:&_ch10Ctrl bitValue:peqEnabled[currentChannel] bitOrder:7];
            [viewController sendData:CMD_CH10_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch10Ctrl];
            break;
        case CHANNEL_CH_11:
            [Global setClearBit:&_ch11Ctrl bitValue:peqEnabled[currentChannel] bitOrder:7];
            [viewController sendData:CMD_CH11_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch11Ctrl];
            break;
        case CHANNEL_CH_12:
            [Global setClearBit:&_ch12Ctrl bitValue:peqEnabled[currentChannel] bitOrder:7];
            [viewController sendData:CMD_CH12_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch12Ctrl];
            break;
        case CHANNEL_CH_13:
            [Global setClearBit:&_ch13Ctrl bitValue:peqEnabled[currentChannel] bitOrder:7];
            [viewController sendData:CMD_CH13_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch13Ctrl];
            break;
        case CHANNEL_CH_14:
            [Global setClearBit:&_ch14Ctrl bitValue:peqEnabled[currentChannel] bitOrder:7];
            [viewController sendData:CMD_CH14_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch14Ctrl];
            break;
        case CHANNEL_CH_15:
            [Global setClearBit:&_ch15Ctrl bitValue:peqEnabled[currentChannel] bitOrder:7];
            [viewController sendData:CMD_CH15_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch15Ctrl];
            break;
        case CHANNEL_CH_16:
            [Global setClearBit:&_ch16Ctrl bitValue:peqEnabled[currentChannel] bitOrder:7];
            [viewController sendData:CMD_CH16_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch16Ctrl];
            break;
        case CHANNEL_CH_17:
            [Global setClearBit:&_ch17Ctrl bitValue:peqEnabled[currentChannel] bitOrder:7];
            [viewController sendData:CMD_CH17_CTRL commandType:DSP_WRITE dspId:DSP_6 value:_ch17Ctrl];
            break;
        case CHANNEL_CH_18:
            [Global setClearBit:&_ch18Ctrl bitValue:peqEnabled[currentChannel] bitOrder:7];
            [viewController sendData:CMD_CH18_CTRL commandType:DSP_WRITE dspId:DSP_6 value:_ch18Ctrl];
            break;
        case CHANNEL_MULTI_1:
            [Global setClearBit:&_multi1Ctrl bitValue:peqEnabled[currentChannel] bitOrder:7];
            [viewController sendData:CMD_MULTI_1_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi1Ctrl];
            break;
        case CHANNEL_MULTI_2:
            [Global setClearBit:&_multi2Ctrl bitValue:peqEnabled[currentChannel] bitOrder:7];
            [viewController sendData:CMD_MULTI_2_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi2Ctrl];
            break;
        case CHANNEL_MULTI_3:
            [Global setClearBit:&_multi3Ctrl bitValue:peqEnabled[currentChannel] bitOrder:7];
            [viewController sendData:CMD_MULTI_3_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi3Ctrl];
            break;
        case CHANNEL_MULTI_4:
            [Global setClearBit:&_multi4Ctrl bitValue:peqEnabled[currentChannel] bitOrder:7];
            [viewController sendData:CMD_MULTI_4_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi4Ctrl];
            break;
        case CHANNEL_MAIN:
            [Global setClearBit:&_mainCtrl bitValue:peqEnabled[currentChannel] bitOrder:7];
            [viewController sendData:CMD_MAIN_LR_CTRL commandType:DSP_WRITE dspId:DSP_6 value:_mainCtrl];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnPeq1:(id)sender {
    btnPeq1.selected = !btnPeq1.selected;
    peq1Enabled[currentChannel] = btnPeq1.selected;
    if (btnPeq1.selected) {
        btnBall1.hidden = NO;
    } else {
        btnBall1.hidden = YES;
    }
    switch (currentChannel) {
        case CHANNEL_CH_1:
            [Global setClearBit:&_ch1Ctrl bitValue:peq1Enabled[currentChannel] bitOrder:3];
            [viewController sendData:CMD_CH1_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch1Ctrl];
            break;
        case CHANNEL_CH_2:
            [Global setClearBit:&_ch2Ctrl bitValue:peq1Enabled[currentChannel] bitOrder:3];
            [viewController sendData:CMD_CH2_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch2Ctrl];
            break;
        case CHANNEL_CH_3:
            [Global setClearBit:&_ch3Ctrl bitValue:peq1Enabled[currentChannel] bitOrder:3];
            [viewController sendData:CMD_CH3_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch3Ctrl];
            break;
        case CHANNEL_CH_4:
            [Global setClearBit:&_ch4Ctrl bitValue:peq1Enabled[currentChannel] bitOrder:3];
            [viewController sendData:CMD_CH4_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch4Ctrl];
            break;
        case CHANNEL_CH_5:
            [Global setClearBit:&_ch5Ctrl bitValue:peq1Enabled[currentChannel] bitOrder:3];
            [viewController sendData:CMD_CH5_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch5Ctrl];
            break;
        case CHANNEL_CH_6:
            [Global setClearBit:&_ch6Ctrl bitValue:peq1Enabled[currentChannel] bitOrder:3];
            [viewController sendData:CMD_CH6_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch6Ctrl];
            break;
        case CHANNEL_CH_7:
            [Global setClearBit:&_ch7Ctrl bitValue:peq1Enabled[currentChannel] bitOrder:3];
            [viewController sendData:CMD_CH7_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch7Ctrl];
            break;
        case CHANNEL_CH_8:
            [Global setClearBit:&_ch8Ctrl bitValue:peq1Enabled[currentChannel] bitOrder:3];
            [viewController sendData:CMD_CH8_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch8Ctrl];
            break;
        case CHANNEL_CH_9:
            [Global setClearBit:&_ch9Ctrl bitValue:peq1Enabled[currentChannel] bitOrder:3];
            [viewController sendData:CMD_CH9_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch9Ctrl];
            break;
        case CHANNEL_CH_10:
            [Global setClearBit:&_ch10Ctrl bitValue:peq1Enabled[currentChannel] bitOrder:3];
            [viewController sendData:CMD_CH10_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch10Ctrl];
            break;
        case CHANNEL_CH_11:
            [Global setClearBit:&_ch11Ctrl bitValue:peq1Enabled[currentChannel] bitOrder:3];
            [viewController sendData:CMD_CH11_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch11Ctrl];
            break;
        case CHANNEL_CH_12:
            [Global setClearBit:&_ch12Ctrl bitValue:peq1Enabled[currentChannel] bitOrder:3];
            [viewController sendData:CMD_CH12_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch12Ctrl];
            break;
        case CHANNEL_CH_13:
            [Global setClearBit:&_ch13Ctrl bitValue:peq1Enabled[currentChannel] bitOrder:3];
            [viewController sendData:CMD_CH13_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch13Ctrl];
            break;
        case CHANNEL_CH_14:
            [Global setClearBit:&_ch14Ctrl bitValue:peq1Enabled[currentChannel] bitOrder:3];
            [viewController sendData:CMD_CH14_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch14Ctrl];
            break;
        case CHANNEL_CH_15:
            [Global setClearBit:&_ch15Ctrl bitValue:peq1Enabled[currentChannel] bitOrder:3];
            [viewController sendData:CMD_CH15_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch15Ctrl];
            break;
        case CHANNEL_CH_16:
            [Global setClearBit:&_ch16Ctrl bitValue:peq1Enabled[currentChannel] bitOrder:3];
            [viewController sendData:CMD_CH16_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch16Ctrl];
            break;
        case CHANNEL_CH_17:
            [Global setClearBit:&_ch17Ctrl bitValue:peq1Enabled[currentChannel] bitOrder:3];
            [viewController sendData:CMD_CH17_CTRL commandType:DSP_WRITE dspId:DSP_6 value:_ch17Ctrl];
            break;
        case CHANNEL_CH_18:
            [Global setClearBit:&_ch18Ctrl bitValue:peq1Enabled[currentChannel] bitOrder:3];
            [viewController sendData:CMD_CH18_CTRL commandType:DSP_WRITE dspId:DSP_6 value:_ch18Ctrl];
            break;
        case CHANNEL_MULTI_1:
            [Global setClearBit:&_multi1Ctrl bitValue:peq1Enabled[currentChannel] bitOrder:3];
            [viewController sendData:CMD_MULTI_1_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi1Ctrl];
            break;
        case CHANNEL_MULTI_2:
            [Global setClearBit:&_multi2Ctrl bitValue:peq1Enabled[currentChannel] bitOrder:3];
            [viewController sendData:CMD_MULTI_2_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi2Ctrl];
            break;
        case CHANNEL_MULTI_3:
            [Global setClearBit:&_multi3Ctrl bitValue:peq1Enabled[currentChannel] bitOrder:3];
            [viewController sendData:CMD_MULTI_3_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi3Ctrl];
            break;
        case CHANNEL_MULTI_4:
            [Global setClearBit:&_multi4Ctrl bitValue:peq1Enabled[currentChannel] bitOrder:3];
            [viewController sendData:CMD_MULTI_4_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi4Ctrl];
            break;
        case CHANNEL_MAIN:
            [Global setClearBit:&_mainCtrl bitValue:peq1Enabled[currentChannel] bitOrder:3];
            [viewController sendData:CMD_MAIN_LR_CTRL commandType:DSP_WRITE dspId:DSP_6 value:_mainCtrl];
            break;
        default:
            break;
    }
    [self updatePeqFrame];
}

- (IBAction)onBtnPeq2:(id)sender {
    btnPeq2.selected = !btnPeq2.selected;
    peq2Enabled[currentChannel] = btnPeq2.selected;
    if (btnPeq2.selected) {
        btnBall2.hidden = NO;
    } else {
        btnBall2.hidden = YES;
    }
    switch (currentChannel) {
        case CHANNEL_CH_1:
            [Global setClearBit:&_ch1Ctrl bitValue:peq2Enabled[currentChannel] bitOrder:4];
            [viewController sendData:CMD_CH1_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch1Ctrl];
            break;
        case CHANNEL_CH_2:
            [Global setClearBit:&_ch2Ctrl bitValue:peq2Enabled[currentChannel] bitOrder:4];
            [viewController sendData:CMD_CH2_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch2Ctrl];
            break;
        case CHANNEL_CH_3:
            [Global setClearBit:&_ch3Ctrl bitValue:peq2Enabled[currentChannel] bitOrder:4];
            [viewController sendData:CMD_CH3_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch3Ctrl];
            break;
        case CHANNEL_CH_4:
            [Global setClearBit:&_ch4Ctrl bitValue:peq2Enabled[currentChannel] bitOrder:4];
            [viewController sendData:CMD_CH4_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch4Ctrl];
            break;
        case CHANNEL_CH_5:
            [Global setClearBit:&_ch5Ctrl bitValue:peq2Enabled[currentChannel] bitOrder:4];
            [viewController sendData:CMD_CH5_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch5Ctrl];
            break;
        case CHANNEL_CH_6:
            [Global setClearBit:&_ch6Ctrl bitValue:peq2Enabled[currentChannel] bitOrder:4];
            [viewController sendData:CMD_CH6_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch6Ctrl];
            break;
        case CHANNEL_CH_7:
            [Global setClearBit:&_ch7Ctrl bitValue:peq2Enabled[currentChannel] bitOrder:4];
            [viewController sendData:CMD_CH7_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch7Ctrl];
            break;
        case CHANNEL_CH_8:
            [Global setClearBit:&_ch8Ctrl bitValue:peq2Enabled[currentChannel] bitOrder:4];
            [viewController sendData:CMD_CH8_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch8Ctrl];
            break;
        case CHANNEL_CH_9:
            [Global setClearBit:&_ch9Ctrl bitValue:peq2Enabled[currentChannel] bitOrder:4];
            [viewController sendData:CMD_CH9_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch9Ctrl];
            break;
        case CHANNEL_CH_10:
            [Global setClearBit:&_ch10Ctrl bitValue:peq2Enabled[currentChannel] bitOrder:4];
            [viewController sendData:CMD_CH10_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch10Ctrl];
            break;
        case CHANNEL_CH_11:
            [Global setClearBit:&_ch11Ctrl bitValue:peq2Enabled[currentChannel] bitOrder:4];
            [viewController sendData:CMD_CH11_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch11Ctrl];
            break;
        case CHANNEL_CH_12:
            [Global setClearBit:&_ch12Ctrl bitValue:peq2Enabled[currentChannel] bitOrder:4];
            [viewController sendData:CMD_CH12_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch12Ctrl];
            break;
        case CHANNEL_CH_13:
            [Global setClearBit:&_ch13Ctrl bitValue:peq2Enabled[currentChannel] bitOrder:4];
            [viewController sendData:CMD_CH13_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch13Ctrl];
            break;
        case CHANNEL_CH_14:
            [Global setClearBit:&_ch14Ctrl bitValue:peq2Enabled[currentChannel] bitOrder:4];
            [viewController sendData:CMD_CH14_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch14Ctrl];
            break;
        case CHANNEL_CH_15:
            [Global setClearBit:&_ch15Ctrl bitValue:peq2Enabled[currentChannel] bitOrder:4];
            [viewController sendData:CMD_CH15_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch15Ctrl];
            break;
        case CHANNEL_CH_16:
            [Global setClearBit:&_ch16Ctrl bitValue:peq2Enabled[currentChannel] bitOrder:4];
            [viewController sendData:CMD_CH16_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch16Ctrl];
            break;
        case CHANNEL_CH_17:
            [Global setClearBit:&_ch17Ctrl bitValue:peq2Enabled[currentChannel] bitOrder:4];
            [viewController sendData:CMD_CH17_CTRL commandType:DSP_WRITE dspId:DSP_6 value:_ch17Ctrl];
            break;
        case CHANNEL_CH_18:
            [Global setClearBit:&_ch18Ctrl bitValue:peq2Enabled[currentChannel] bitOrder:4];
            [viewController sendData:CMD_CH18_CTRL commandType:DSP_WRITE dspId:DSP_6 value:_ch18Ctrl];
            break;
        case CHANNEL_MULTI_1:
            [Global setClearBit:&_multi1Ctrl bitValue:peq2Enabled[currentChannel] bitOrder:4];
            [viewController sendData:CMD_MULTI_1_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi1Ctrl];
            break;
        case CHANNEL_MULTI_2:
            [Global setClearBit:&_multi2Ctrl bitValue:peq2Enabled[currentChannel] bitOrder:4];
            [viewController sendData:CMD_MULTI_2_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi2Ctrl];
            break;
        case CHANNEL_MULTI_3:
            [Global setClearBit:&_multi3Ctrl bitValue:peq2Enabled[currentChannel] bitOrder:4];
            [viewController sendData:CMD_MULTI_3_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi3Ctrl];
            break;
        case CHANNEL_MULTI_4:
            [Global setClearBit:&_multi4Ctrl bitValue:peq2Enabled[currentChannel] bitOrder:4];
            [viewController sendData:CMD_MULTI_4_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi4Ctrl];
            break;
        case CHANNEL_MAIN:
            [Global setClearBit:&_mainCtrl bitValue:peq2Enabled[currentChannel] bitOrder:4];
            [viewController sendData:CMD_MAIN_LR_CTRL commandType:DSP_WRITE dspId:DSP_6 value:_mainCtrl];
            break;
        default:
            break;
    }
    [self updatePeqFrame];
}

- (IBAction)onBtnPeq3:(id)sender {
    btnPeq3.selected = !btnPeq3.selected;
    peq3Enabled[currentChannel] = btnPeq3.selected;
    if (btnPeq3.selected) {
        btnBall3.hidden = NO;
    } else {
        btnBall3.hidden = YES;
    }
    switch (currentChannel) {
        case CHANNEL_CH_1:
            [Global setClearBit:&_ch1Ctrl bitValue:peq3Enabled[currentChannel] bitOrder:5];
            [viewController sendData:CMD_CH1_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch1Ctrl];
            break;
        case CHANNEL_CH_2:
            [Global setClearBit:&_ch2Ctrl bitValue:peq3Enabled[currentChannel] bitOrder:5];
            [viewController sendData:CMD_CH2_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch2Ctrl];
            break;
        case CHANNEL_CH_3:
            [Global setClearBit:&_ch3Ctrl bitValue:peq3Enabled[currentChannel] bitOrder:5];
            [viewController sendData:CMD_CH3_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch3Ctrl];
            break;
        case CHANNEL_CH_4:
            [Global setClearBit:&_ch4Ctrl bitValue:peq3Enabled[currentChannel] bitOrder:5];
            [viewController sendData:CMD_CH4_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch4Ctrl];
            break;
        case CHANNEL_CH_5:
            [Global setClearBit:&_ch5Ctrl bitValue:peq3Enabled[currentChannel] bitOrder:5];
            [viewController sendData:CMD_CH5_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch5Ctrl];
            break;
        case CHANNEL_CH_6:
            [Global setClearBit:&_ch6Ctrl bitValue:peq3Enabled[currentChannel] bitOrder:5];
            [viewController sendData:CMD_CH6_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch6Ctrl];
            break;
        case CHANNEL_CH_7:
            [Global setClearBit:&_ch7Ctrl bitValue:peq3Enabled[currentChannel] bitOrder:5];
            [viewController sendData:CMD_CH7_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch7Ctrl];
            break;
        case CHANNEL_CH_8:
            [Global setClearBit:&_ch8Ctrl bitValue:peq3Enabled[currentChannel] bitOrder:5];
            [viewController sendData:CMD_CH8_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch8Ctrl];
            break;
        case CHANNEL_CH_9:
            [Global setClearBit:&_ch9Ctrl bitValue:peq3Enabled[currentChannel] bitOrder:5];
            [viewController sendData:CMD_CH9_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch9Ctrl];
            break;
        case CHANNEL_CH_10:
            [Global setClearBit:&_ch10Ctrl bitValue:peq3Enabled[currentChannel] bitOrder:5];
            [viewController sendData:CMD_CH10_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch10Ctrl];
            break;
        case CHANNEL_CH_11:
            [Global setClearBit:&_ch11Ctrl bitValue:peq3Enabled[currentChannel] bitOrder:5];
            [viewController sendData:CMD_CH11_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch11Ctrl];
            break;
        case CHANNEL_CH_12:
            [Global setClearBit:&_ch12Ctrl bitValue:peq3Enabled[currentChannel] bitOrder:5];
            [viewController sendData:CMD_CH12_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch12Ctrl];
            break;
        case CHANNEL_CH_13:
            [Global setClearBit:&_ch13Ctrl bitValue:peq3Enabled[currentChannel] bitOrder:5];
            [viewController sendData:CMD_CH13_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch13Ctrl];
            break;
        case CHANNEL_CH_14:
            [Global setClearBit:&_ch14Ctrl bitValue:peq3Enabled[currentChannel] bitOrder:5];
            [viewController sendData:CMD_CH14_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch14Ctrl];
            break;
        case CHANNEL_CH_15:
            [Global setClearBit:&_ch15Ctrl bitValue:peq3Enabled[currentChannel] bitOrder:5];
            [viewController sendData:CMD_CH15_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch15Ctrl];
            break;
        case CHANNEL_CH_16:
            [Global setClearBit:&_ch16Ctrl bitValue:peq3Enabled[currentChannel] bitOrder:5];
            [viewController sendData:CMD_CH16_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch16Ctrl];
            break;
        case CHANNEL_CH_17:
            [Global setClearBit:&_ch17Ctrl bitValue:peq3Enabled[currentChannel] bitOrder:5];
            [viewController sendData:CMD_CH17_CTRL commandType:DSP_WRITE dspId:DSP_6 value:_ch17Ctrl];
            break;
        case CHANNEL_CH_18:
            [Global setClearBit:&_ch18Ctrl bitValue:peq3Enabled[currentChannel] bitOrder:5];
            [viewController sendData:CMD_CH18_CTRL commandType:DSP_WRITE dspId:DSP_6 value:_ch18Ctrl];
            break;
        case CHANNEL_MULTI_1:
            [Global setClearBit:&_multi1Ctrl bitValue:peq3Enabled[currentChannel] bitOrder:5];
            [viewController sendData:CMD_MULTI_1_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi1Ctrl];
            break;
        case CHANNEL_MULTI_2:
            [Global setClearBit:&_multi2Ctrl bitValue:peq3Enabled[currentChannel] bitOrder:5];
            [viewController sendData:CMD_MULTI_2_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi2Ctrl];
            break;
        case CHANNEL_MULTI_3:
            [Global setClearBit:&_multi3Ctrl bitValue:peq3Enabled[currentChannel] bitOrder:5];
            [viewController sendData:CMD_MULTI_3_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi3Ctrl];
            break;
        case CHANNEL_MULTI_4:
            [Global setClearBit:&_multi4Ctrl bitValue:peq3Enabled[currentChannel] bitOrder:5];
            [viewController sendData:CMD_MULTI_4_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi4Ctrl];
            break;
        case CHANNEL_MAIN:
            [Global setClearBit:&_mainCtrl bitValue:peq3Enabled[currentChannel] bitOrder:5];
            [viewController sendData:CMD_MAIN_LR_CTRL commandType:DSP_WRITE dspId:DSP_6 value:_mainCtrl];
            break;
        default:
            break;
    }
    [self updatePeqFrame];
}

- (IBAction)onBtnPeq4:(id)sender {
    btnPeq4.selected = !btnPeq4.selected;
    peq4Enabled[currentChannel] = btnPeq4.selected;
    if (btnPeq4.selected) {
        btnBall4.hidden = NO;
    } else {
        btnBall4.hidden = YES;
    }
    switch (currentChannel) {
        case CHANNEL_CH_1:
            [Global setClearBit:&_ch1Ctrl bitValue:peq4Enabled[currentChannel] bitOrder:6];
            [viewController sendData:CMD_CH1_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch1Ctrl];
            break;
        case CHANNEL_CH_2:
            [Global setClearBit:&_ch2Ctrl bitValue:peq4Enabled[currentChannel] bitOrder:6];
            [viewController sendData:CMD_CH2_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch2Ctrl];
            break;
        case CHANNEL_CH_3:
            [Global setClearBit:&_ch3Ctrl bitValue:peq4Enabled[currentChannel] bitOrder:6];
            [viewController sendData:CMD_CH3_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch3Ctrl];
            break;
        case CHANNEL_CH_4:
            [Global setClearBit:&_ch4Ctrl bitValue:peq4Enabled[currentChannel] bitOrder:6];
            [viewController sendData:CMD_CH4_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch4Ctrl];
            break;
        case CHANNEL_CH_5:
            [Global setClearBit:&_ch5Ctrl bitValue:peq4Enabled[currentChannel] bitOrder:6];
            [viewController sendData:CMD_CH5_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch5Ctrl];
            break;
        case CHANNEL_CH_6:
            [Global setClearBit:&_ch6Ctrl bitValue:peq4Enabled[currentChannel] bitOrder:6];
            [viewController sendData:CMD_CH6_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch6Ctrl];
            break;
        case CHANNEL_CH_7:
            [Global setClearBit:&_ch7Ctrl bitValue:peq4Enabled[currentChannel] bitOrder:6];
            [viewController sendData:CMD_CH7_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch7Ctrl];
            break;
        case CHANNEL_CH_8:
            [Global setClearBit:&_ch8Ctrl bitValue:peq4Enabled[currentChannel] bitOrder:6];
            [viewController sendData:CMD_CH8_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch8Ctrl];
            break;
        case CHANNEL_CH_9:
            [Global setClearBit:&_ch9Ctrl bitValue:peq4Enabled[currentChannel] bitOrder:6];
            [viewController sendData:CMD_CH9_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch9Ctrl];
            break;
        case CHANNEL_CH_10:
            [Global setClearBit:&_ch10Ctrl bitValue:peq4Enabled[currentChannel] bitOrder:6];
            [viewController sendData:CMD_CH10_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch10Ctrl];
            break;
        case CHANNEL_CH_11:
            [Global setClearBit:&_ch11Ctrl bitValue:peq4Enabled[currentChannel] bitOrder:6];
            [viewController sendData:CMD_CH11_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch11Ctrl];
            break;
        case CHANNEL_CH_12:
            [Global setClearBit:&_ch12Ctrl bitValue:peq4Enabled[currentChannel] bitOrder:6];
            [viewController sendData:CMD_CH12_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch12Ctrl];
            break;
        case CHANNEL_CH_13:
            [Global setClearBit:&_ch13Ctrl bitValue:peq4Enabled[currentChannel] bitOrder:6];
            [viewController sendData:CMD_CH13_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch13Ctrl];
            break;
        case CHANNEL_CH_14:
            [Global setClearBit:&_ch14Ctrl bitValue:peq4Enabled[currentChannel] bitOrder:6];
            [viewController sendData:CMD_CH14_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch14Ctrl];
            break;
        case CHANNEL_CH_15:
            [Global setClearBit:&_ch15Ctrl bitValue:peq4Enabled[currentChannel] bitOrder:6];
            [viewController sendData:CMD_CH15_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch15Ctrl];
            break;
        case CHANNEL_CH_16:
            [Global setClearBit:&_ch16Ctrl bitValue:peq4Enabled[currentChannel] bitOrder:6];
            [viewController sendData:CMD_CH16_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch16Ctrl];
            break;
        case CHANNEL_CH_17:
            [Global setClearBit:&_ch17Ctrl bitValue:peq4Enabled[currentChannel] bitOrder:6];
            [viewController sendData:CMD_CH17_CTRL commandType:DSP_WRITE dspId:DSP_6 value:_ch17Ctrl];
            break;
        case CHANNEL_CH_18:
            [Global setClearBit:&_ch18Ctrl bitValue:peq4Enabled[currentChannel] bitOrder:6];
            [viewController sendData:CMD_CH18_CTRL commandType:DSP_WRITE dspId:DSP_6 value:_ch18Ctrl];
            break;
        case CHANNEL_MULTI_1:
            [Global setClearBit:&_multi1Ctrl bitValue:peq4Enabled[currentChannel] bitOrder:6];
            [viewController sendData:CMD_MULTI_1_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi1Ctrl];
            break;
        case CHANNEL_MULTI_2:
            [Global setClearBit:&_multi2Ctrl bitValue:peq4Enabled[currentChannel] bitOrder:6];
            [viewController sendData:CMD_MULTI_2_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi2Ctrl];
            break;
        case CHANNEL_MULTI_3:
            [Global setClearBit:&_multi3Ctrl bitValue:peq4Enabled[currentChannel] bitOrder:6];
            [viewController sendData:CMD_MULTI_3_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi3Ctrl];
            break;
        case CHANNEL_MULTI_4:
            [Global setClearBit:&_multi4Ctrl bitValue:peq4Enabled[currentChannel] bitOrder:6];
            [viewController sendData:CMD_MULTI_4_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi4Ctrl];
            break;
        case CHANNEL_MAIN:
            [Global setClearBit:&_mainCtrl bitValue:peq4Enabled[currentChannel] bitOrder:6];
            [viewController sendData:CMD_MAIN_LR_CTRL commandType:DSP_WRITE dspId:DSP_6 value:_mainCtrl];
            break;
        default:
            break;
    }
    [self updatePeqFrame];
}

- (IBAction)onBtnFilterFront:(id)sender {
    if (!viewSelectFilterBack.hidden) {
        viewSelectFilterBack.hidden = YES;
    }
    viewSelectFilterFront.hidden = !viewSelectFilterFront.hidden;
}

- (IBAction)onBtnFilter1Peak:(id)sender {
    [btnPeqFilterFront setImage:[UIImage imageNamed:@"frame-11.png"] forState:UIControlStateNormal];
    viewSelectFilterFront.hidden = YES;
    lblPeq1ValueG.hidden = NO;
    lblPeq1ValueQ.hidden = NO;
    peq1Filter[currentChannel] = EQ_PEAK;
    peq1Q[currentChannel] = lastPeq1Q[currentChannel];
    peqData1 = [NSMutableArray arrayWithArray:[Global eqPeakData:[Global getEqFreq:peq1F[currentChannel]] eqDb:[Global getEqGain:peq1G[currentChannel]] eqQ:[Global getEqQ:peq1Q[currentChannel]]]];
    [self calculatePeqData];
    [self updatePreviewImage];
    
    // Send data
    switch (currentChannel) {
        case CHANNEL_CH_1:
            [viewController sendData:CMD_CH1_EQ1_Q commandType:DSP_WRITE dspId:DSP_1 value:peq1Q[currentChannel]];
            break;
        case CHANNEL_CH_2:
            [viewController sendData:CMD_CH2_EQ1_Q commandType:DSP_WRITE dspId:DSP_1 value:peq1Q[currentChannel]];
            break;
        case CHANNEL_CH_3:
            [viewController sendData:CMD_CH3_EQ1_Q commandType:DSP_WRITE dspId:DSP_1 value:peq1Q[currentChannel]];
            break;
        case CHANNEL_CH_4:
            [viewController sendData:CMD_CH4_EQ1_Q commandType:DSP_WRITE dspId:DSP_1 value:peq1Q[currentChannel]];
            break;
        case CHANNEL_CH_5:
            [viewController sendData:CMD_CH1_EQ1_Q commandType:DSP_WRITE dspId:DSP_2 value:peq1Q[currentChannel]];
            break;
        case CHANNEL_CH_6:
            [viewController sendData:CMD_CH2_EQ1_Q commandType:DSP_WRITE dspId:DSP_2 value:peq1Q[currentChannel]];
            break;
        case CHANNEL_CH_7:
            [viewController sendData:CMD_CH3_EQ1_Q commandType:DSP_WRITE dspId:DSP_2 value:peq1Q[currentChannel]];
            break;
        case CHANNEL_CH_8:
            [viewController sendData:CMD_CH4_EQ1_Q commandType:DSP_WRITE dspId:DSP_2 value:peq1Q[currentChannel]];
            break;
        case CHANNEL_CH_9:
            [viewController sendData:CMD_CH1_EQ1_Q commandType:DSP_WRITE dspId:DSP_3 value:peq1Q[currentChannel]];
            break;
        case CHANNEL_CH_10:
            [viewController sendData:CMD_CH2_EQ1_Q commandType:DSP_WRITE dspId:DSP_3 value:peq1Q[currentChannel]];
            break;
        case CHANNEL_CH_11:
            [viewController sendData:CMD_CH3_EQ1_Q commandType:DSP_WRITE dspId:DSP_3 value:peq1Q[currentChannel]];
            break;
        case CHANNEL_CH_12:
            [viewController sendData:CMD_CH4_EQ1_Q commandType:DSP_WRITE dspId:DSP_3 value:peq1Q[currentChannel]];
            break;
        case CHANNEL_CH_13:
            [viewController sendData:CMD_CH1_EQ1_Q commandType:DSP_WRITE dspId:DSP_4 value:peq1Q[currentChannel]];
            break;
        case CHANNEL_CH_14:
            [viewController sendData:CMD_CH2_EQ1_Q commandType:DSP_WRITE dspId:DSP_4 value:peq1Q[currentChannel]];
            break;
        case CHANNEL_CH_15:
            [viewController sendData:CMD_CH3_EQ1_Q commandType:DSP_WRITE dspId:DSP_4 value:peq1Q[currentChannel]];
            break;
        case CHANNEL_CH_16:
            [viewController sendData:CMD_CH4_EQ1_Q commandType:DSP_WRITE dspId:DSP_4 value:peq1Q[currentChannel]];
            break;
        case CHANNEL_CH_17:
            [viewController sendData:CMD_CH17_EQ1_Q commandType:DSP_WRITE dspId:DSP_6 value:peq1Q[currentChannel]];
            break;
        case CHANNEL_CH_18:
            [viewController sendData:CMD_CH18_EQ1_Q commandType:DSP_WRITE dspId:DSP_6 value:peq1Q[currentChannel]];
            break;
        case CHANNEL_MULTI_1:
            [viewController sendData:CMD_MULTI1_EQ1_Q commandType:DSP_WRITE dspId:DSP_7 value:peq1Q[currentChannel]];
            break;
        case CHANNEL_MULTI_2:
            [viewController sendData:CMD_MULTI2_EQ1_Q commandType:DSP_WRITE dspId:DSP_7 value:peq1Q[currentChannel]];
            break;
        case CHANNEL_MULTI_3:
            [viewController sendData:CMD_MULTI3_EQ1_Q commandType:DSP_WRITE dspId:DSP_7 value:peq1Q[currentChannel]];
            break;
        case CHANNEL_MULTI_4:
            [viewController sendData:CMD_MULTI4_EQ1_Q commandType:DSP_WRITE dspId:DSP_7 value:peq1Q[currentChannel]];
            break;
        case CHANNEL_MAIN:
            [viewController sendData:CMD_MAIN_LR_EQ1_Q commandType:DSP_WRITE dspId:DSP_6 value:peq1Q[currentChannel]];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnFilter1LowShelf:(id)sender {
    [btnPeqFilterFront setImage:[UIImage imageNamed:@"frame-12.png"] forState:UIControlStateNormal];
    viewSelectFilterFront.hidden = YES;
    lblPeq1ValueG.hidden = NO;
    lblPeq1ValueQ.hidden = YES;
    peq1Filter[currentChannel] = EQ_LOW_SHELF;
    peqData1 = [NSMutableArray arrayWithArray:[Global eqLowShelfData:[Global getEqFreq:peq1F[currentChannel]] eqDb:[Global getEqGain:peq1G[currentChannel]]]];
    [self calculatePeqData];
    [self updatePreviewImage];
    
    // Send data
    switch (currentChannel) {
        case CHANNEL_CH_1:
            [viewController sendData:CMD_CH1_EQ1_Q commandType:DSP_WRITE dspId:DSP_1 value:PEQ_1_LOW_SHELF];
            break;
        case CHANNEL_CH_2:
            [viewController sendData:CMD_CH2_EQ1_Q commandType:DSP_WRITE dspId:DSP_1 value:PEQ_1_LOW_SHELF];
            break;
        case CHANNEL_CH_3:
            [viewController sendData:CMD_CH3_EQ1_Q commandType:DSP_WRITE dspId:DSP_1 value:PEQ_1_LOW_SHELF];
            break;
        case CHANNEL_CH_4:
            [viewController sendData:CMD_CH4_EQ1_Q commandType:DSP_WRITE dspId:DSP_1 value:PEQ_1_LOW_SHELF];
            break;
        case CHANNEL_CH_5:
            [viewController sendData:CMD_CH1_EQ1_Q commandType:DSP_WRITE dspId:DSP_2 value:PEQ_1_LOW_SHELF];
            break;
        case CHANNEL_CH_6:
            [viewController sendData:CMD_CH2_EQ1_Q commandType:DSP_WRITE dspId:DSP_2 value:PEQ_1_LOW_SHELF];
            break;
        case CHANNEL_CH_7:
            [viewController sendData:CMD_CH3_EQ1_Q commandType:DSP_WRITE dspId:DSP_2 value:PEQ_1_LOW_SHELF];
            break;
        case CHANNEL_CH_8:
            [viewController sendData:CMD_CH4_EQ1_Q commandType:DSP_WRITE dspId:DSP_2 value:PEQ_1_LOW_SHELF];
            break;
        case CHANNEL_CH_9:
            [viewController sendData:CMD_CH1_EQ1_Q commandType:DSP_WRITE dspId:DSP_3 value:PEQ_1_LOW_SHELF];
            break;
        case CHANNEL_CH_10:
            [viewController sendData:CMD_CH2_EQ1_Q commandType:DSP_WRITE dspId:DSP_3 value:PEQ_1_LOW_SHELF];
            break;
        case CHANNEL_CH_11:
            [viewController sendData:CMD_CH3_EQ1_Q commandType:DSP_WRITE dspId:DSP_3 value:PEQ_1_LOW_SHELF];
            break;
        case CHANNEL_CH_12:
            [viewController sendData:CMD_CH4_EQ1_Q commandType:DSP_WRITE dspId:DSP_3 value:PEQ_1_LOW_SHELF];
            break;
        case CHANNEL_CH_13:
            [viewController sendData:CMD_CH1_EQ1_Q commandType:DSP_WRITE dspId:DSP_4 value:PEQ_1_LOW_SHELF];
            break;
        case CHANNEL_CH_14:
            [viewController sendData:CMD_CH2_EQ1_Q commandType:DSP_WRITE dspId:DSP_4 value:PEQ_1_LOW_SHELF];
            break;
        case CHANNEL_CH_15:
            [viewController sendData:CMD_CH3_EQ1_Q commandType:DSP_WRITE dspId:DSP_4 value:PEQ_1_LOW_SHELF];
            break;
        case CHANNEL_CH_16:
            [viewController sendData:CMD_CH4_EQ1_Q commandType:DSP_WRITE dspId:DSP_4 value:PEQ_1_LOW_SHELF];
            break;
        case CHANNEL_CH_17:
            [viewController sendData:CMD_CH17_EQ1_Q commandType:DSP_WRITE dspId:DSP_6 value:PEQ_1_LOW_SHELF];
            break;
        case CHANNEL_CH_18:
            [viewController sendData:CMD_CH18_EQ1_Q commandType:DSP_WRITE dspId:DSP_6 value:PEQ_1_LOW_SHELF];
            break;
        case CHANNEL_MULTI_1:
            [viewController sendData:CMD_MULTI1_EQ1_Q commandType:DSP_WRITE dspId:DSP_7 value:PEQ_1_LOW_SHELF];
            break;
        case CHANNEL_MULTI_2:
            [viewController sendData:CMD_MULTI2_EQ1_Q commandType:DSP_WRITE dspId:DSP_7 value:PEQ_1_LOW_SHELF];
            break;
        case CHANNEL_MULTI_3:
            [viewController sendData:CMD_MULTI3_EQ1_Q commandType:DSP_WRITE dspId:DSP_7 value:PEQ_1_LOW_SHELF];
            break;
        case CHANNEL_MULTI_4:
            [viewController sendData:CMD_MULTI4_EQ1_Q commandType:DSP_WRITE dspId:DSP_7 value:PEQ_1_LOW_SHELF];
            break;
        case CHANNEL_MAIN:
            [viewController sendData:CMD_MAIN_LR_EQ1_Q commandType:DSP_WRITE dspId:DSP_6 value:PEQ_1_LOW_SHELF];
            break;
        default:
            break;
    }    
}

- (IBAction)onBtnFilter1LowCut:(id)sender {
    [btnPeqFilterFront setImage:[UIImage imageNamed:@"frame-13.png"] forState:UIControlStateNormal];
    viewSelectFilterFront.hidden = YES;
    lblPeq1ValueG.hidden = YES;
    lblPeq1ValueQ.hidden = YES;
    peq1Filter[currentChannel] = EQ_LOW_CUT;
    peqData1 = [NSMutableArray arrayWithArray:[Global eqHPFData:[Global getEqFreq:peq1F[currentChannel]]]];
    [self calculatePeqData];
    [self updatePreviewImage];
    
    // Send data
    switch (currentChannel) {
        case CHANNEL_CH_1:
            [viewController sendData:CMD_CH1_EQ1_Q commandType:DSP_WRITE dspId:DSP_1 value:PEQ_1_HPF];
            break;
        case CHANNEL_CH_2:
            [viewController sendData:CMD_CH2_EQ1_Q commandType:DSP_WRITE dspId:DSP_1 value:PEQ_1_HPF];
            break;
        case CHANNEL_CH_3:
            [viewController sendData:CMD_CH3_EQ1_Q commandType:DSP_WRITE dspId:DSP_1 value:PEQ_1_HPF];
            break;
        case CHANNEL_CH_4:
            [viewController sendData:CMD_CH4_EQ1_Q commandType:DSP_WRITE dspId:DSP_1 value:PEQ_1_HPF];
            break;
        case CHANNEL_CH_5:
            [viewController sendData:CMD_CH1_EQ1_Q commandType:DSP_WRITE dspId:DSP_2 value:PEQ_1_HPF];
            break;
        case CHANNEL_CH_6:
            [viewController sendData:CMD_CH2_EQ1_Q commandType:DSP_WRITE dspId:DSP_2 value:PEQ_1_HPF];
            break;
        case CHANNEL_CH_7:
            [viewController sendData:CMD_CH3_EQ1_Q commandType:DSP_WRITE dspId:DSP_2 value:PEQ_1_HPF];
            break;
        case CHANNEL_CH_8:
            [viewController sendData:CMD_CH4_EQ1_Q commandType:DSP_WRITE dspId:DSP_2 value:PEQ_1_HPF];
            break;
        case CHANNEL_CH_9:
            [viewController sendData:CMD_CH1_EQ1_Q commandType:DSP_WRITE dspId:DSP_3 value:PEQ_1_HPF];
            break;
        case CHANNEL_CH_10:
            [viewController sendData:CMD_CH2_EQ1_Q commandType:DSP_WRITE dspId:DSP_3 value:PEQ_1_HPF];
            break;
        case CHANNEL_CH_11:
            [viewController sendData:CMD_CH3_EQ1_Q commandType:DSP_WRITE dspId:DSP_3 value:PEQ_1_HPF];
            break;
        case CHANNEL_CH_12:
            [viewController sendData:CMD_CH4_EQ1_Q commandType:DSP_WRITE dspId:DSP_3 value:PEQ_1_HPF];
            break;
        case CHANNEL_CH_13:
            [viewController sendData:CMD_CH1_EQ1_Q commandType:DSP_WRITE dspId:DSP_4 value:PEQ_1_HPF];
            break;
        case CHANNEL_CH_14:
            [viewController sendData:CMD_CH2_EQ1_Q commandType:DSP_WRITE dspId:DSP_4 value:PEQ_1_HPF];
            break;
        case CHANNEL_CH_15:
            [viewController sendData:CMD_CH3_EQ1_Q commandType:DSP_WRITE dspId:DSP_4 value:PEQ_1_HPF];
            break;
        case CHANNEL_CH_16:
            [viewController sendData:CMD_CH4_EQ1_Q commandType:DSP_WRITE dspId:DSP_4 value:PEQ_1_HPF];
            break;
        case CHANNEL_CH_17:
            [viewController sendData:CMD_CH17_EQ1_Q commandType:DSP_WRITE dspId:DSP_6 value:PEQ_1_HPF];
            break;
        case CHANNEL_CH_18:
            [viewController sendData:CMD_CH18_EQ1_Q commandType:DSP_WRITE dspId:DSP_6 value:PEQ_1_HPF];
            break;
        case CHANNEL_MULTI_1:
            [viewController sendData:CMD_MULTI1_EQ1_Q commandType:DSP_WRITE dspId:DSP_7 value:PEQ_1_HPF];
            break;
        case CHANNEL_MULTI_2:
            [viewController sendData:CMD_MULTI2_EQ1_Q commandType:DSP_WRITE dspId:DSP_7 value:PEQ_1_HPF];
            break;
        case CHANNEL_MULTI_3:
            [viewController sendData:CMD_MULTI3_EQ1_Q commandType:DSP_WRITE dspId:DSP_7 value:PEQ_1_HPF];
            break;
        case CHANNEL_MULTI_4:
            [viewController sendData:CMD_MULTI4_EQ1_Q commandType:DSP_WRITE dspId:DSP_7 value:PEQ_1_HPF];
            break;
        case CHANNEL_MAIN:
            [viewController sendData:CMD_MAIN_LR_EQ1_Q commandType:DSP_WRITE dspId:DSP_6 value:PEQ_1_HPF];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnFilterBack:(id)sender {
    if (!viewSelectFilterFront.hidden) {
        viewSelectFilterFront.hidden = YES;
    }
    viewSelectFilterBack.hidden = !viewSelectFilterBack.hidden;
}

- (IBAction)onBtnFilter2Peak:(id)sender {
    [btnPeqFilterBack setImage:[UIImage imageNamed:@"frame-11.png"] forState:UIControlStateNormal];
    viewSelectFilterBack.hidden = YES;
    lblPeq4ValueG.hidden = NO;
    lblPeq4ValueQ.hidden = NO;
    peq4Filter[currentChannel] = EQ_PEAK;
    peq4Q[currentChannel] = lastPeq4Q[currentChannel];
    peqData4 = [NSMutableArray arrayWithArray:[Global eqPeakData:[Global getEqFreq:peq4F[currentChannel]] eqDb:[Global getEqGain:peq4G[currentChannel]] eqQ:[Global getEqQ:peq4Q[currentChannel]]]];
    [self calculatePeqData];
    [self updatePreviewImage];
    
    // Send data
    switch (currentChannel) {
        case CHANNEL_CH_1:
            [viewController sendData:CMD_CH1_EQ4_Q commandType:DSP_WRITE dspId:DSP_1 value:peq4Q[currentChannel]];
            break;
        case CHANNEL_CH_2:
            [viewController sendData:CMD_CH2_EQ4_Q commandType:DSP_WRITE dspId:DSP_1 value:peq4Q[currentChannel]];
            break;
        case CHANNEL_CH_3:
            [viewController sendData:CMD_CH3_EQ4_Q commandType:DSP_WRITE dspId:DSP_1 value:peq4Q[currentChannel]];
            break;
        case CHANNEL_CH_4:
            [viewController sendData:CMD_CH4_EQ4_Q commandType:DSP_WRITE dspId:DSP_1 value:peq4Q[currentChannel]];
            break;
        case CHANNEL_CH_5:
            [viewController sendData:CMD_CH1_EQ4_Q commandType:DSP_WRITE dspId:DSP_2 value:peq4Q[currentChannel]];
            break;
        case CHANNEL_CH_6:
            [viewController sendData:CMD_CH2_EQ4_Q commandType:DSP_WRITE dspId:DSP_2 value:peq4Q[currentChannel]];
            break;
        case CHANNEL_CH_7:
            [viewController sendData:CMD_CH3_EQ4_Q commandType:DSP_WRITE dspId:DSP_2 value:peq4Q[currentChannel]];
            break;
        case CHANNEL_CH_8:
            [viewController sendData:CMD_CH4_EQ4_Q commandType:DSP_WRITE dspId:DSP_2 value:peq4Q[currentChannel]];
            break;
        case CHANNEL_CH_9:
            [viewController sendData:CMD_CH1_EQ4_Q commandType:DSP_WRITE dspId:DSP_3 value:peq4Q[currentChannel]];
            break;
        case CHANNEL_CH_10:
            [viewController sendData:CMD_CH2_EQ4_Q commandType:DSP_WRITE dspId:DSP_3 value:peq4Q[currentChannel]];
            break;
        case CHANNEL_CH_11:
            [viewController sendData:CMD_CH3_EQ4_Q commandType:DSP_WRITE dspId:DSP_3 value:peq4Q[currentChannel]];
            break;
        case CHANNEL_CH_12:
            [viewController sendData:CMD_CH4_EQ4_Q commandType:DSP_WRITE dspId:DSP_3 value:peq4Q[currentChannel]];
            break;
        case CHANNEL_CH_13:
            [viewController sendData:CMD_CH1_EQ4_Q commandType:DSP_WRITE dspId:DSP_4 value:peq4Q[currentChannel]];
            break;
        case CHANNEL_CH_14:
            [viewController sendData:CMD_CH2_EQ4_Q commandType:DSP_WRITE dspId:DSP_4 value:peq4Q[currentChannel]];
            break;
        case CHANNEL_CH_15:
            [viewController sendData:CMD_CH3_EQ4_Q commandType:DSP_WRITE dspId:DSP_4 value:peq4Q[currentChannel]];
            break;
        case CHANNEL_CH_16:
            [viewController sendData:CMD_CH4_EQ4_Q commandType:DSP_WRITE dspId:DSP_4 value:peq4Q[currentChannel]];
            break;
        case CHANNEL_CH_17:
            [viewController sendData:CMD_CH17_EQ4_Q commandType:DSP_WRITE dspId:DSP_6 value:peq4Q[currentChannel]];
            break;
        case CHANNEL_CH_18:
            [viewController sendData:CMD_CH18_EQ4_Q commandType:DSP_WRITE dspId:DSP_6 value:peq4Q[currentChannel]];
            break;
        case CHANNEL_MULTI_1:
            [viewController sendData:CMD_MULTI1_EQ4_Q commandType:DSP_WRITE dspId:DSP_7 value:peq4Q[currentChannel]];
            break;
        case CHANNEL_MULTI_2:
            [viewController sendData:CMD_MULTI2_EQ4_Q commandType:DSP_WRITE dspId:DSP_7 value:peq4Q[currentChannel]];
            break;
        case CHANNEL_MULTI_3:
            [viewController sendData:CMD_MULTI3_EQ4_Q commandType:DSP_WRITE dspId:DSP_7 value:peq4Q[currentChannel]];
            break;
        case CHANNEL_MULTI_4:
            [viewController sendData:CMD_MULTI4_EQ4_Q commandType:DSP_WRITE dspId:DSP_7 value:peq4Q[currentChannel]];
            break;
        case CHANNEL_MAIN:
            [viewController sendData:CMD_MAIN_LR_EQ4_Q commandType:DSP_WRITE dspId:DSP_6 value:peq4Q[currentChannel]];
            break;
        default:
            break;
    }    
}

- (IBAction)onBtnFilter2HighShelf:(id)sender {
    [btnPeqFilterBack setImage:[UIImage imageNamed:@"frame-15.png"] forState:UIControlStateNormal];
    viewSelectFilterBack.hidden = YES;
    lblPeq4ValueG.hidden = NO;
    lblPeq4ValueQ.hidden = YES;
    peq4Filter[currentChannel] = EQ_HIGH_SHELF;
    peqData4 = [NSMutableArray arrayWithArray:[Global eqHighShelfData:[Global getEqFreq:peq4F[currentChannel]] eqDb:[Global getEqGain:peq4G[currentChannel]]]];
    [self calculatePeqData];
    [self updatePreviewImage];
    
    // Send data
    switch (currentChannel) {
        case CHANNEL_CH_1:
            [viewController sendData:CMD_CH1_EQ4_Q commandType:DSP_WRITE dspId:DSP_1 value:PEQ_4_HIGH_SHELF];
            break;
        case CHANNEL_CH_2:
            [viewController sendData:CMD_CH2_EQ4_Q commandType:DSP_WRITE dspId:DSP_1 value:PEQ_4_HIGH_SHELF];
            break;
        case CHANNEL_CH_3:
            [viewController sendData:CMD_CH3_EQ4_Q commandType:DSP_WRITE dspId:DSP_1 value:PEQ_4_HIGH_SHELF];
            break;
        case CHANNEL_CH_4:
            [viewController sendData:CMD_CH4_EQ4_Q commandType:DSP_WRITE dspId:DSP_1 value:PEQ_4_HIGH_SHELF];
            break;
        case CHANNEL_CH_5:
            [viewController sendData:CMD_CH1_EQ4_Q commandType:DSP_WRITE dspId:DSP_2 value:PEQ_4_HIGH_SHELF];
            break;
        case CHANNEL_CH_6:
            [viewController sendData:CMD_CH2_EQ4_Q commandType:DSP_WRITE dspId:DSP_2 value:PEQ_4_HIGH_SHELF];
            break;
        case CHANNEL_CH_7:
            [viewController sendData:CMD_CH3_EQ4_Q commandType:DSP_WRITE dspId:DSP_2 value:PEQ_4_HIGH_SHELF];
            break;
        case CHANNEL_CH_8:
            [viewController sendData:CMD_CH4_EQ4_Q commandType:DSP_WRITE dspId:DSP_2 value:PEQ_4_HIGH_SHELF];
            break;
        case CHANNEL_CH_9:
            [viewController sendData:CMD_CH1_EQ4_Q commandType:DSP_WRITE dspId:DSP_3 value:PEQ_4_HIGH_SHELF];
            break;
        case CHANNEL_CH_10:
            [viewController sendData:CMD_CH2_EQ4_Q commandType:DSP_WRITE dspId:DSP_3 value:PEQ_4_HIGH_SHELF];
            break;
        case CHANNEL_CH_11:
            [viewController sendData:CMD_CH3_EQ4_Q commandType:DSP_WRITE dspId:DSP_3 value:PEQ_4_HIGH_SHELF];
            break;
        case CHANNEL_CH_12:
            [viewController sendData:CMD_CH4_EQ4_Q commandType:DSP_WRITE dspId:DSP_3 value:PEQ_4_HIGH_SHELF];
            break;
        case CHANNEL_CH_13:
            [viewController sendData:CMD_CH1_EQ4_Q commandType:DSP_WRITE dspId:DSP_4 value:PEQ_4_HIGH_SHELF];
            break;
        case CHANNEL_CH_14:
            [viewController sendData:CMD_CH2_EQ4_Q commandType:DSP_WRITE dspId:DSP_4 value:PEQ_4_HIGH_SHELF];
            break;
        case CHANNEL_CH_15:
            [viewController sendData:CMD_CH3_EQ4_Q commandType:DSP_WRITE dspId:DSP_4 value:PEQ_4_HIGH_SHELF];
            break;
        case CHANNEL_CH_16:
            [viewController sendData:CMD_CH4_EQ4_Q commandType:DSP_WRITE dspId:DSP_4 value:PEQ_4_HIGH_SHELF];
            break;
        case CHANNEL_CH_17:
            [viewController sendData:CMD_CH17_EQ4_Q commandType:DSP_WRITE dspId:DSP_6 value:PEQ_4_HIGH_SHELF];
            break;
        case CHANNEL_CH_18:
            [viewController sendData:CMD_CH18_EQ4_Q commandType:DSP_WRITE dspId:DSP_6 value:PEQ_4_HIGH_SHELF];
            break;
        case CHANNEL_MULTI_1:
            [viewController sendData:CMD_MULTI1_EQ4_Q commandType:DSP_WRITE dspId:DSP_7 value:PEQ_4_HIGH_SHELF];
            break;
        case CHANNEL_MULTI_2:
            [viewController sendData:CMD_MULTI2_EQ4_Q commandType:DSP_WRITE dspId:DSP_7 value:PEQ_4_HIGH_SHELF];
            break;
        case CHANNEL_MULTI_3:
            [viewController sendData:CMD_MULTI3_EQ4_Q commandType:DSP_WRITE dspId:DSP_7 value:PEQ_4_HIGH_SHELF];
            break;
        case CHANNEL_MULTI_4:
            [viewController sendData:CMD_MULTI4_EQ4_Q commandType:DSP_WRITE dspId:DSP_7 value:PEQ_4_HIGH_SHELF];
            break;
        case CHANNEL_MAIN:
            [viewController sendData:CMD_MAIN_LR_EQ4_Q commandType:DSP_WRITE dspId:DSP_6 value:PEQ_4_HIGH_SHELF];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnFilter2HighCut:(id)sender {
    [btnPeqFilterBack setImage:[UIImage imageNamed:@"frame-14.png"] forState:UIControlStateNormal];
    viewSelectFilterBack.hidden = YES;
    lblPeq4ValueG.hidden = YES;
    lblPeq4ValueQ.hidden = YES;
    peq4Filter[currentChannel] = EQ_HIGH_CUT;
    peqData4 = [NSMutableArray arrayWithArray:[Global eqLPFData:[Global getEqFreq:peq4F[currentChannel]]]];
    [self calculatePeqData];
    [self updatePreviewImage];
    
    // Send data
    switch (currentChannel) {
        case CHANNEL_CH_1:
            [viewController sendData:CMD_CH1_EQ4_Q commandType:DSP_WRITE dspId:DSP_1 value:PEQ_4_LPF];
            break;
        case CHANNEL_CH_2:
            [viewController sendData:CMD_CH2_EQ4_Q commandType:DSP_WRITE dspId:DSP_1 value:PEQ_4_LPF];
            break;
        case CHANNEL_CH_3:
            [viewController sendData:CMD_CH3_EQ4_Q commandType:DSP_WRITE dspId:DSP_1 value:PEQ_4_LPF];
            break;
        case CHANNEL_CH_4:
            [viewController sendData:CMD_CH4_EQ4_Q commandType:DSP_WRITE dspId:DSP_1 value:PEQ_4_LPF];
            break;
        case CHANNEL_CH_5:
            [viewController sendData:CMD_CH1_EQ4_Q commandType:DSP_WRITE dspId:DSP_2 value:PEQ_4_LPF];
            break;
        case CHANNEL_CH_6:
            [viewController sendData:CMD_CH2_EQ4_Q commandType:DSP_WRITE dspId:DSP_2 value:PEQ_4_LPF];
            break;
        case CHANNEL_CH_7:
            [viewController sendData:CMD_CH3_EQ4_Q commandType:DSP_WRITE dspId:DSP_2 value:PEQ_4_LPF];
            break;
        case CHANNEL_CH_8:
            [viewController sendData:CMD_CH4_EQ4_Q commandType:DSP_WRITE dspId:DSP_2 value:PEQ_4_LPF];
            break;
        case CHANNEL_CH_9:
            [viewController sendData:CMD_CH1_EQ4_Q commandType:DSP_WRITE dspId:DSP_3 value:PEQ_4_LPF];
            break;
        case CHANNEL_CH_10:
            [viewController sendData:CMD_CH2_EQ4_Q commandType:DSP_WRITE dspId:DSP_3 value:PEQ_4_LPF];
            break;
        case CHANNEL_CH_11:
            [viewController sendData:CMD_CH3_EQ4_Q commandType:DSP_WRITE dspId:DSP_3 value:PEQ_4_LPF];
            break;
        case CHANNEL_CH_12:
            [viewController sendData:CMD_CH4_EQ4_Q commandType:DSP_WRITE dspId:DSP_3 value:PEQ_4_LPF];
            break;
        case CHANNEL_CH_13:
            [viewController sendData:CMD_CH1_EQ4_Q commandType:DSP_WRITE dspId:DSP_4 value:PEQ_4_LPF];
            break;
        case CHANNEL_CH_14:
            [viewController sendData:CMD_CH2_EQ4_Q commandType:DSP_WRITE dspId:DSP_4 value:PEQ_4_LPF];
            break;
        case CHANNEL_CH_15:
            [viewController sendData:CMD_CH3_EQ4_Q commandType:DSP_WRITE dspId:DSP_4 value:PEQ_4_LPF];
            break;
        case CHANNEL_CH_16:
            [viewController sendData:CMD_CH4_EQ4_Q commandType:DSP_WRITE dspId:DSP_4 value:PEQ_4_LPF];
            break;
        case CHANNEL_CH_17:
            [viewController sendData:CMD_CH17_EQ4_Q commandType:DSP_WRITE dspId:DSP_6 value:PEQ_4_LPF];
            break;
        case CHANNEL_CH_18:
            [viewController sendData:CMD_CH18_EQ4_Q commandType:DSP_WRITE dspId:DSP_6 value:PEQ_4_LPF];
            break;
        case CHANNEL_MULTI_1:
            [viewController sendData:CMD_MULTI1_EQ4_Q commandType:DSP_WRITE dspId:DSP_7 value:PEQ_4_LPF];
            break;
        case CHANNEL_MULTI_2:
            [viewController sendData:CMD_MULTI2_EQ4_Q commandType:DSP_WRITE dspId:DSP_7 value:PEQ_4_LPF];
            break;
        case CHANNEL_MULTI_3:
            [viewController sendData:CMD_MULTI3_EQ4_Q commandType:DSP_WRITE dspId:DSP_7 value:PEQ_4_LPF];
            break;
        case CHANNEL_MULTI_4:
            [viewController sendData:CMD_MULTI4_EQ4_Q commandType:DSP_WRITE dspId:DSP_7 value:PEQ_4_LPF];
            break;
        case CHANNEL_MAIN:
            [viewController sendData:CMD_MAIN_LR_EQ4_Q commandType:DSP_WRITE dspId:DSP_6 value:PEQ_4_LPF];
            break;
        default:
            break;
    }
}

- (void)resetBalls {
    btnBall1.selected = NO;
    btnBall2.selected = NO;
    btnBall3.selected = NO;
    btnBall4.selected = NO;
    btnBall1.highlighted = NO;
    btnBall2.highlighted = NO;
    btnBall3.highlighted = NO;
    btnBall4.highlighted = NO;
}

- (IBAction)onBtnBall1Touched:(id)sender {
    [self resetBalls];
    btnBall1.highlighted = YES;
    currentBall = 1;
}

- (IBAction)onBtnBall1Moved:(id)sender withEvent:(UIEvent *)event {
    // Ball can only be moved if corresponding button is pressed
    if (!btnPeq1.selected)
        return;
    
    peqLock = YES;
    
    CGPoint point = [[[event allTouches] anyObject] locationInView:self.viewPeqFrame];
    
    CGFloat ptX, ptY;
    if (point.x < 0.0) {
        ptX = 0.0;
    } else if (point.x > self.viewPeqFrame.frame.size.width) {
        ptX = self.viewPeqFrame.frame.size.width;
    } else {
        ptX = point.x;
    }
    
    if (point.y < 0.0) {
        ptY = 0.0;
    } else if (point.y > self.viewPeqFrame.frame.size.height) {
        ptY = self.viewPeqFrame.frame.size.height;
    } else {
        ptY = point.y;
    }
    
    // Update ball position
    btnBall1.center = CGPointMake(ptX, ptY);
    
    // Update labels
    CGFloat valueX = point.x, valueY = self.viewPeqFrame.frame.size.height - point.y;

    if (valueX < 0.0)
        valueX = 0.0;
    if (valueX > self.viewPeqFrame.frame.size.width)
        valueX = self.viewPeqFrame.frame.size.width;
    if (valueY < 0.0)
        valueY = 0.0;
    if (valueY > self.viewPeqFrame.frame.size.height)
        valueY = self.viewPeqFrame.frame.size.height;
    
    peq1F[currentChannel] = [self peqXToFreq:valueX];
    peq1G[currentChannel] = [self peqYToGain:valueY];
    lblPeq1ValueG.text = [Global getEqGainString:peq1G[currentChannel]];
    lblPeq1ValueF.text = [Global getEqFreqString:peq1F[currentChannel]];
    
    // Update graph
    if (peq1Filter[currentChannel] == EQ_PEAK) {
        peqData1 = [NSMutableArray arrayWithArray:[Global eqPeakData:[Global getEqFreq:peq1F[currentChannel]] eqDb:[Global getEqGain:peq1G[currentChannel]] eqQ:[Global getEqQ:peq1Q[currentChannel]]]];
    } else if (peq1Filter[currentChannel] == EQ_LOW_SHELF) {
        peqData1 = [NSMutableArray arrayWithArray:[Global eqLowShelfData:[Global getEqFreq:peq1F[currentChannel]] eqDb:[Global getEqGain:peq1G[currentChannel]]]];
    } else {
        peqData1 = [NSMutableArray arrayWithArray:[Global eqHPFData:[Global getEqFreq:peq1F[currentChannel]]]];
    }
    [self calculatePeqData];
    
    // Send data
    switch (currentChannel) {
        case CHANNEL_CH_1:
            [viewController sendData:CMD_CH1_EQ1_DB commandType:DSP_WRITE dspId:DSP_1 value:peq1G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH1_EQ1_FREQ commandType:DSP_WRITE dspId:DSP_1 value:peq1F[currentChannel]];
            break;
        case CHANNEL_CH_2:
            [viewController sendData:CMD_CH2_EQ1_DB commandType:DSP_WRITE dspId:DSP_1 value:peq1G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH2_EQ1_FREQ commandType:DSP_WRITE dspId:DSP_1 value:peq1F[currentChannel]];
            break;
        case CHANNEL_CH_3:
            [viewController sendData:CMD_CH3_EQ1_DB commandType:DSP_WRITE dspId:DSP_1 value:peq1G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH3_EQ1_FREQ commandType:DSP_WRITE dspId:DSP_1 value:peq1F[currentChannel]];
            break;
        case CHANNEL_CH_4:
            [viewController sendData:CMD_CH4_EQ1_DB commandType:DSP_WRITE dspId:DSP_1 value:peq1G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH4_EQ1_FREQ commandType:DSP_WRITE dspId:DSP_1 value:peq1F[currentChannel]];
            break;
        case CHANNEL_CH_5:
            [viewController sendData:CMD_CH1_EQ1_DB commandType:DSP_WRITE dspId:DSP_2 value:peq1G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH1_EQ1_FREQ commandType:DSP_WRITE dspId:DSP_2 value:peq1F[currentChannel]];
            break;
        case CHANNEL_CH_6:
            [viewController sendData:CMD_CH2_EQ1_DB commandType:DSP_WRITE dspId:DSP_2 value:peq1G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH2_EQ1_FREQ commandType:DSP_WRITE dspId:DSP_2 value:peq1F[currentChannel]];
            break;
        case CHANNEL_CH_7:
            [viewController sendData:CMD_CH3_EQ1_DB commandType:DSP_WRITE dspId:DSP_2 value:peq1G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH3_EQ1_FREQ commandType:DSP_WRITE dspId:DSP_2 value:peq1F[currentChannel]];
            break;
        case CHANNEL_CH_8:
            [viewController sendData:CMD_CH4_EQ1_DB commandType:DSP_WRITE dspId:DSP_2 value:peq1G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH4_EQ1_FREQ commandType:DSP_WRITE dspId:DSP_2 value:peq1F[currentChannel]];
            break;
        case CHANNEL_CH_9:
            [viewController sendData:CMD_CH1_EQ1_DB commandType:DSP_WRITE dspId:DSP_3 value:peq1G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH1_EQ1_FREQ commandType:DSP_WRITE dspId:DSP_3 value:peq1F[currentChannel]];
            break;
        case CHANNEL_CH_10:
            [viewController sendData:CMD_CH2_EQ1_DB commandType:DSP_WRITE dspId:DSP_3 value:peq1G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH2_EQ1_FREQ commandType:DSP_WRITE dspId:DSP_3 value:peq1F[currentChannel]];
            break;
        case CHANNEL_CH_11:
            [viewController sendData:CMD_CH3_EQ1_DB commandType:DSP_WRITE dspId:DSP_3 value:peq1G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH3_EQ1_FREQ commandType:DSP_WRITE dspId:DSP_3 value:peq1F[currentChannel]];
            break;
        case CHANNEL_CH_12:
            [viewController sendData:CMD_CH4_EQ1_DB commandType:DSP_WRITE dspId:DSP_3 value:peq1G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH4_EQ1_FREQ commandType:DSP_WRITE dspId:DSP_3 value:peq1F[currentChannel]];
            break;
        case CHANNEL_CH_13:
            [viewController sendData:CMD_CH1_EQ1_DB commandType:DSP_WRITE dspId:DSP_4 value:peq1G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH1_EQ1_FREQ commandType:DSP_WRITE dspId:DSP_4 value:peq1F[currentChannel]];
            break;
        case CHANNEL_CH_14:
            [viewController sendData:CMD_CH2_EQ1_DB commandType:DSP_WRITE dspId:DSP_4 value:peq1G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH2_EQ1_FREQ commandType:DSP_WRITE dspId:DSP_4 value:peq1F[currentChannel]];
            break;
        case CHANNEL_CH_15:
            [viewController sendData:CMD_CH3_EQ1_DB commandType:DSP_WRITE dspId:DSP_4 value:peq1G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH3_EQ1_FREQ commandType:DSP_WRITE dspId:DSP_4 value:peq1F[currentChannel]];
            break;
        case CHANNEL_CH_16:
            [viewController sendData:CMD_CH4_EQ1_DB commandType:DSP_WRITE dspId:DSP_4 value:peq1G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH4_EQ1_FREQ commandType:DSP_WRITE dspId:DSP_4 value:peq1F[currentChannel]];
            break;
        case CHANNEL_CH_17:
            [viewController sendData:CMD_CH17_EQ1_DB commandType:DSP_WRITE dspId:DSP_6 value:peq1G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH17_EQ1_FREQ commandType:DSP_WRITE dspId:DSP_6 value:peq1F[currentChannel]];
            break;
        case CHANNEL_CH_18:
            [viewController sendData:CMD_CH18_EQ1_DB commandType:DSP_WRITE dspId:DSP_6 value:peq1G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH18_EQ1_FREQ commandType:DSP_WRITE dspId:DSP_6 value:peq1F[currentChannel]];
            break;
        case CHANNEL_MULTI_1:
            [viewController sendData:CMD_MULTI1_EQ1_DB commandType:DSP_WRITE dspId:DSP_7 value:peq1G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_MULTI1_EQ1_FREQ commandType:DSP_WRITE dspId:DSP_7 value:peq1F[currentChannel]];
            break;
        case CHANNEL_MULTI_2:
            [viewController sendData:CMD_MULTI2_EQ1_DB commandType:DSP_WRITE dspId:DSP_7 value:peq1G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_MULTI2_EQ1_FREQ commandType:DSP_WRITE dspId:DSP_7 value:peq1F[currentChannel]];
            break;
        case CHANNEL_MULTI_3:
            [viewController sendData:CMD_MULTI3_EQ1_DB commandType:DSP_WRITE dspId:DSP_7 value:peq1G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_MULTI3_EQ1_FREQ commandType:DSP_WRITE dspId:DSP_7 value:peq1F[currentChannel]];
            break;
        case CHANNEL_MULTI_4:
            [viewController sendData:CMD_MULTI4_EQ1_DB commandType:DSP_WRITE dspId:DSP_7 value:peq1G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_MULTI4_EQ1_FREQ commandType:DSP_WRITE dspId:DSP_7 value:peq1F[currentChannel]];
            break;
        case CHANNEL_MAIN:
            [viewController sendData:CMD_MAIN_LR_EQ1_DB commandType:DSP_WRITE dspId:DSP_6 value:peq1G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_MAIN_LR_EQ1_FREQ commandType:DSP_WRITE dspId:DSP_6 value:peq1F[currentChannel]];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnBall1Released:(id)sender {
    btnBall1.selected = YES;
    peqLock = NO;
    [self updatePreviewImage];
}

- (IBAction)onBtnBall2Touched:(id)sender {
    [self resetBalls];
    btnBall2.highlighted = YES;
    currentBall = 2;
}

- (IBAction)onBtnBall2Moved:(id)sender withEvent:(UIEvent *)event {
    // Ball can only be moved if corresponding button is pressed
    if (!btnPeq2.selected)
        return;
    
    peqLock = YES;

    CGPoint point = [[[event allTouches] anyObject] locationInView:self.viewPeqFrame];
    
    CGFloat ptX, ptY;
    if (point.x < 0.0) {
        ptX = 0.0;
    } else if (point.x > self.viewPeqFrame.frame.size.width) {
        ptX = self.viewPeqFrame.frame.size.width;
    } else {
        ptX = point.x;
    }
    
    if (point.y < 0.0) {
        ptY = 0.0;
    } else if (point.y > self.viewPeqFrame.frame.size.height) {
        ptY = self.viewPeqFrame.frame.size.height;
    } else {
        ptY = point.y;
    }

    // Update ball position
    btnBall2.center = CGPointMake(ptX, ptY);
    
    // Update labels
    CGFloat valueX = point.x, valueY = self.viewPeqFrame.frame.size.height - point.y;

    if (valueX < 0.0)
        valueX = 0.0;
    if (valueX > self.viewPeqFrame.frame.size.width)
        valueX = self.viewPeqFrame.frame.size.width;
    if (valueY < 0.0)
        valueY = 0.0;
    if (valueY > self.viewPeqFrame.frame.size.height)
        valueY = self.viewPeqFrame.frame.size.height;
    
    peq2F[currentChannel] = [self peqXToFreq:valueX];
    peq2G[currentChannel] = [self peqYToGain:valueY];
    lblPeq2ValueG.text = [Global getEqGainString:peq2G[currentChannel]];
    lblPeq2ValueF.text = [Global getEqFreqString:peq2F[currentChannel]];
    
    // Update graph
    peqData2 = [NSMutableArray arrayWithArray:[Global eqPeakData:[Global getEqFreq:peq2F[currentChannel]]
                                                            eqDb:[Global getEqGain:peq2G[currentChannel]] eqQ:[Global getEqQ:peq2Q[currentChannel]]]];
    [self calculatePeqData];
    
    // Send data
    switch (currentChannel) {
        case CHANNEL_CH_1:
            [viewController sendData:CMD_CH1_EQ2_DB commandType:DSP_WRITE dspId:DSP_1 value:peq2G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH1_EQ2_FREQ commandType:DSP_WRITE dspId:DSP_1 value:peq2F[currentChannel]];
            break;
        case CHANNEL_CH_2:
            [viewController sendData:CMD_CH2_EQ2_DB commandType:DSP_WRITE dspId:DSP_1 value:peq2G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH2_EQ2_FREQ commandType:DSP_WRITE dspId:DSP_1 value:peq2F[currentChannel]];
            break;
        case CHANNEL_CH_3:
            [viewController sendData:CMD_CH3_EQ2_DB commandType:DSP_WRITE dspId:DSP_1 value:peq2G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH3_EQ2_FREQ commandType:DSP_WRITE dspId:DSP_1 value:peq2F[currentChannel]];
            break;
        case CHANNEL_CH_4:
            [viewController sendData:CMD_CH4_EQ2_DB commandType:DSP_WRITE dspId:DSP_1 value:peq2G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH4_EQ2_FREQ commandType:DSP_WRITE dspId:DSP_1 value:peq2F[currentChannel]];
            break;
        case CHANNEL_CH_5:
            [viewController sendData:CMD_CH1_EQ2_DB commandType:DSP_WRITE dspId:DSP_2 value:peq2G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH1_EQ2_FREQ commandType:DSP_WRITE dspId:DSP_2 value:peq2F[currentChannel]];
            break;
        case CHANNEL_CH_6:
            [viewController sendData:CMD_CH2_EQ2_DB commandType:DSP_WRITE dspId:DSP_2 value:peq2G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH2_EQ2_FREQ commandType:DSP_WRITE dspId:DSP_2 value:peq2F[currentChannel]];
            break;
        case CHANNEL_CH_7:
            [viewController sendData:CMD_CH3_EQ2_DB commandType:DSP_WRITE dspId:DSP_2 value:peq2G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH3_EQ2_FREQ commandType:DSP_WRITE dspId:DSP_2 value:peq2F[currentChannel]];
            break;
        case CHANNEL_CH_8:
            [viewController sendData:CMD_CH4_EQ2_DB commandType:DSP_WRITE dspId:DSP_2 value:peq2G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH4_EQ2_FREQ commandType:DSP_WRITE dspId:DSP_2 value:peq2F[currentChannel]];
            break;
        case CHANNEL_CH_9:
            [viewController sendData:CMD_CH1_EQ2_DB commandType:DSP_WRITE dspId:DSP_3 value:peq2G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH1_EQ2_FREQ commandType:DSP_WRITE dspId:DSP_3 value:peq2F[currentChannel]];
            break;
        case CHANNEL_CH_10:
            [viewController sendData:CMD_CH2_EQ2_DB commandType:DSP_WRITE dspId:DSP_3 value:peq2G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH2_EQ2_FREQ commandType:DSP_WRITE dspId:DSP_3 value:peq2F[currentChannel]];
            break;
        case CHANNEL_CH_11:
            [viewController sendData:CMD_CH3_EQ2_DB commandType:DSP_WRITE dspId:DSP_3 value:peq2G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH3_EQ2_FREQ commandType:DSP_WRITE dspId:DSP_3 value:peq2F[currentChannel]];
            break;
        case CHANNEL_CH_12:
            [viewController sendData:CMD_CH4_EQ2_DB commandType:DSP_WRITE dspId:DSP_3 value:peq2G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH4_EQ2_FREQ commandType:DSP_WRITE dspId:DSP_3 value:peq2F[currentChannel]];
            break;
        case CHANNEL_CH_13:
            [viewController sendData:CMD_CH1_EQ2_DB commandType:DSP_WRITE dspId:DSP_4 value:peq2G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH1_EQ2_FREQ commandType:DSP_WRITE dspId:DSP_4 value:peq2F[currentChannel]];
            break;
        case CHANNEL_CH_14:
            [viewController sendData:CMD_CH2_EQ2_DB commandType:DSP_WRITE dspId:DSP_4 value:peq2G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH2_EQ2_FREQ commandType:DSP_WRITE dspId:DSP_4 value:peq2F[currentChannel]];
            break;
        case CHANNEL_CH_15:
            [viewController sendData:CMD_CH3_EQ2_DB commandType:DSP_WRITE dspId:DSP_4 value:peq2G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH3_EQ2_FREQ commandType:DSP_WRITE dspId:DSP_4 value:peq2F[currentChannel]];
            break;
        case CHANNEL_CH_16:
            [viewController sendData:CMD_CH4_EQ2_DB commandType:DSP_WRITE dspId:DSP_4 value:peq2G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH4_EQ2_FREQ commandType:DSP_WRITE dspId:DSP_4 value:peq2F[currentChannel]];
            break;
        case CHANNEL_CH_17:
            [viewController sendData:CMD_CH17_EQ2_DB commandType:DSP_WRITE dspId:DSP_6 value:peq2G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH17_EQ2_FREQ commandType:DSP_WRITE dspId:DSP_6 value:peq2F[currentChannel]];
            break;
        case CHANNEL_CH_18:
            [viewController sendData:CMD_CH18_EQ2_DB commandType:DSP_WRITE dspId:DSP_6 value:peq2G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH18_EQ2_FREQ commandType:DSP_WRITE dspId:DSP_6 value:peq2F[currentChannel]];
            break;
        case CHANNEL_MULTI_1:
            [viewController sendData:CMD_MULTI1_EQ2_DB commandType:DSP_WRITE dspId:DSP_7 value:peq2G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_MULTI1_EQ2_FREQ commandType:DSP_WRITE dspId:DSP_7 value:peq2F[currentChannel]];
            break;
        case CHANNEL_MULTI_2:
            [viewController sendData:CMD_MULTI2_EQ2_DB commandType:DSP_WRITE dspId:DSP_7 value:peq2G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_MULTI2_EQ2_FREQ commandType:DSP_WRITE dspId:DSP_7 value:peq2F[currentChannel]];
            break;
        case CHANNEL_MULTI_3:
            [viewController sendData:CMD_MULTI3_EQ2_DB commandType:DSP_WRITE dspId:DSP_7 value:peq2G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_MULTI3_EQ2_FREQ commandType:DSP_WRITE dspId:DSP_7 value:peq2F[currentChannel]];
            break;
        case CHANNEL_MULTI_4:
            [viewController sendData:CMD_MULTI4_EQ2_DB commandType:DSP_WRITE dspId:DSP_7 value:peq2G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_MULTI4_EQ2_FREQ commandType:DSP_WRITE dspId:DSP_7 value:peq2F[currentChannel]];
            break;
        case CHANNEL_MAIN:
            [viewController sendData:CMD_MAIN_LR_EQ2_DB commandType:DSP_WRITE dspId:DSP_6 value:peq2G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_MAIN_LR_EQ2_FREQ commandType:DSP_WRITE dspId:DSP_6 value:peq2F[currentChannel]];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnBall2Released:(id)sender {
    btnBall2.selected = YES;
    peqLock = NO;
    [self updatePreviewImage];
}

- (IBAction)onBtnBall3Touched:(id)sender {
    [self resetBalls];
    btnBall3.highlighted = YES;
    currentBall = 3;
}

- (IBAction)onBtnBall3Moved:(id)sender withEvent:(UIEvent *)event {
    // Ball can only be moved if corresponding button is pressed
    if (!btnPeq3.selected)
        return;
    
    peqLock = YES;
    
    CGPoint point = [[[event allTouches] anyObject] locationInView:self.viewPeqFrame];
    
    CGFloat ptX, ptY;
    if (point.x < 0.0) {
        ptX = 0.0;
    } else if (point.x > self.viewPeqFrame.frame.size.width) {
        ptX = self.viewPeqFrame.frame.size.width;
    } else {
        ptX = point.x;
    }
    
    if (point.y < 0.0) {
        ptY = 0.0;
    } else if (point.y > self.viewPeqFrame.frame.size.height) {
        ptY = self.viewPeqFrame.frame.size.height;
    } else {
        ptY = point.y;
    }
    
    // Update ball position
    btnBall3.center = CGPointMake(ptX, ptY);
    
    // Update labels
    CGFloat valueX = point.x, valueY = self.viewPeqFrame.frame.size.height - point.y;
    
    if (valueX < 0.0)
        valueX = 0.0;
    if (valueX > self.viewPeqFrame.frame.size.width)
        valueX = self.viewPeqFrame.frame.size.width;
    if (valueY < 0.0)
        valueY = 0.0;
    if (valueY > self.viewPeqFrame.frame.size.height)
        valueY = self.viewPeqFrame.frame.size.height;
    
    peq3F[currentChannel] = [self peqXToFreq:valueX];
    peq3G[currentChannel] = [self peqYToGain:valueY];
    lblPeq3ValueG.text = [Global getEqGainString:peq3G[currentChannel]];
    lblPeq3ValueF.text = [Global getEqFreqString:peq3F[currentChannel]];
    
    // Update graph
    peqData3 = [NSMutableArray arrayWithArray:[Global eqPeakData:[Global getEqFreq:peq3F[currentChannel]] eqDb:[Global getEqGain:peq3G[currentChannel]] eqQ:[Global getEqQ:peq3Q[currentChannel]]]];
    [self calculatePeqData];
    
    // Send data
    switch (currentChannel) {
        case CHANNEL_CH_1:
            [viewController sendData:CMD_CH1_EQ3_DB commandType:DSP_WRITE dspId:DSP_1 value:peq3G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH1_EQ3_FREQ commandType:DSP_WRITE dspId:DSP_1 value:peq3F[currentChannel]];
            break;
        case CHANNEL_CH_2:
            [viewController sendData:CMD_CH2_EQ3_DB commandType:DSP_WRITE dspId:DSP_1 value:peq3G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH2_EQ3_FREQ commandType:DSP_WRITE dspId:DSP_1 value:peq3F[currentChannel]];
            break;
        case CHANNEL_CH_3:
            [viewController sendData:CMD_CH3_EQ3_DB commandType:DSP_WRITE dspId:DSP_1 value:peq3G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH3_EQ3_FREQ commandType:DSP_WRITE dspId:DSP_1 value:peq3F[currentChannel]];
            break;
        case CHANNEL_CH_4:
            [viewController sendData:CMD_CH4_EQ3_DB commandType:DSP_WRITE dspId:DSP_1 value:peq3G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH4_EQ3_FREQ commandType:DSP_WRITE dspId:DSP_1 value:peq3F[currentChannel]];
            break;
        case CHANNEL_CH_5:
            [viewController sendData:CMD_CH1_EQ3_DB commandType:DSP_WRITE dspId:DSP_2 value:peq3G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH1_EQ3_FREQ commandType:DSP_WRITE dspId:DSP_2 value:peq3F[currentChannel]];
            break;
        case CHANNEL_CH_6:
            [viewController sendData:CMD_CH2_EQ3_DB commandType:DSP_WRITE dspId:DSP_2 value:peq3G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH2_EQ3_FREQ commandType:DSP_WRITE dspId:DSP_2 value:peq3F[currentChannel]];
            break;
        case CHANNEL_CH_7:
            [viewController sendData:CMD_CH3_EQ3_DB commandType:DSP_WRITE dspId:DSP_2 value:peq3G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH3_EQ3_FREQ commandType:DSP_WRITE dspId:DSP_2 value:peq3F[currentChannel]];
            break;
        case CHANNEL_CH_8:
            [viewController sendData:CMD_CH4_EQ3_DB commandType:DSP_WRITE dspId:DSP_2 value:peq3G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH4_EQ3_FREQ commandType:DSP_WRITE dspId:DSP_2 value:peq3F[currentChannel]];
            break;
        case CHANNEL_CH_9:
            [viewController sendData:CMD_CH1_EQ3_DB commandType:DSP_WRITE dspId:DSP_3 value:peq3G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH1_EQ3_FREQ commandType:DSP_WRITE dspId:DSP_3 value:peq3F[currentChannel]];
            break;
        case CHANNEL_CH_10:
            [viewController sendData:CMD_CH2_EQ3_DB commandType:DSP_WRITE dspId:DSP_3 value:peq3G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH2_EQ3_FREQ commandType:DSP_WRITE dspId:DSP_3 value:peq3F[currentChannel]];
            break;
        case CHANNEL_CH_11:
            [viewController sendData:CMD_CH3_EQ3_DB commandType:DSP_WRITE dspId:DSP_3 value:peq3G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH3_EQ3_FREQ commandType:DSP_WRITE dspId:DSP_3 value:peq3F[currentChannel]];
            break;
        case CHANNEL_CH_12:
            [viewController sendData:CMD_CH4_EQ3_DB commandType:DSP_WRITE dspId:DSP_3 value:peq3G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH4_EQ3_FREQ commandType:DSP_WRITE dspId:DSP_3 value:peq3F[currentChannel]];
            break;
        case CHANNEL_CH_13:
            [viewController sendData:CMD_CH1_EQ3_DB commandType:DSP_WRITE dspId:DSP_4 value:peq3G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH1_EQ3_FREQ commandType:DSP_WRITE dspId:DSP_4 value:peq3F[currentChannel]];
            break;
        case CHANNEL_CH_14:
            [viewController sendData:CMD_CH2_EQ3_DB commandType:DSP_WRITE dspId:DSP_4 value:peq3G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH2_EQ3_FREQ commandType:DSP_WRITE dspId:DSP_4 value:peq3F[currentChannel]];
            break;
        case CHANNEL_CH_15:
            [viewController sendData:CMD_CH3_EQ3_DB commandType:DSP_WRITE dspId:DSP_4 value:peq3G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH3_EQ3_FREQ commandType:DSP_WRITE dspId:DSP_4 value:peq3F[currentChannel]];
            break;
        case CHANNEL_CH_16:
            [viewController sendData:CMD_CH4_EQ3_DB commandType:DSP_WRITE dspId:DSP_4 value:peq3G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH4_EQ3_FREQ commandType:DSP_WRITE dspId:DSP_4 value:peq3F[currentChannel]];
            break;
        case CHANNEL_CH_17:
            [viewController sendData:CMD_CH17_EQ3_DB commandType:DSP_WRITE dspId:DSP_6 value:peq3G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH17_EQ3_FREQ commandType:DSP_WRITE dspId:DSP_6 value:peq3F[currentChannel]];
            break;
        case CHANNEL_CH_18:
            [viewController sendData:CMD_CH18_EQ3_DB commandType:DSP_WRITE dspId:DSP_6 value:peq3G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH18_EQ3_FREQ commandType:DSP_WRITE dspId:DSP_6 value:peq3F[currentChannel]];
            break;
        case CHANNEL_MULTI_1:
            [viewController sendData:CMD_MULTI1_EQ3_DB commandType:DSP_WRITE dspId:DSP_7 value:peq3G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_MULTI1_EQ3_FREQ commandType:DSP_WRITE dspId:DSP_7 value:peq3F[currentChannel]];
            break;
        case CHANNEL_MULTI_2:
            [viewController sendData:CMD_MULTI2_EQ3_DB commandType:DSP_WRITE dspId:DSP_7 value:peq3G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_MULTI2_EQ3_FREQ commandType:DSP_WRITE dspId:DSP_7 value:peq3F[currentChannel]];
            break;
        case CHANNEL_MULTI_3:
            [viewController sendData:CMD_MULTI3_EQ3_DB commandType:DSP_WRITE dspId:DSP_7 value:peq3G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_MULTI3_EQ3_FREQ commandType:DSP_WRITE dspId:DSP_7 value:peq3F[currentChannel]];
            break;
        case CHANNEL_MULTI_4:
            [viewController sendData:CMD_MULTI4_EQ3_DB commandType:DSP_WRITE dspId:DSP_7 value:peq3G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_MULTI4_EQ3_FREQ commandType:DSP_WRITE dspId:DSP_7 value:peq3F[currentChannel]];
            break;
        case CHANNEL_MAIN:
            [viewController sendData:CMD_MAIN_LR_EQ3_DB commandType:DSP_WRITE dspId:DSP_6 value:peq3G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_MAIN_LR_EQ3_FREQ commandType:DSP_WRITE dspId:DSP_6 value:peq3F[currentChannel]];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnBall3Released:(id)sender {
    btnBall3.selected = YES;
    peqLock = NO;
    [self updatePreviewImage];
}

- (IBAction)onBtnBall4Touched:(id)sender {
    [self resetBalls];
    btnBall4.highlighted = YES;
    currentBall = 4;
}

- (IBAction)onBtnBall4Moved:(id)sender withEvent:(UIEvent *)event {
    // Ball can only be moved if corresponding button is pressed
    if (!btnPeq4.selected)
        return;

    CGPoint point = [[[event allTouches] anyObject] locationInView:self.viewPeqFrame];
    
    CGFloat ptX, ptY;
    if (point.x < 0.0) {
        ptX = 0.0;
    } else if (point.x > self.viewPeqFrame.frame.size.width) {
        ptX = self.viewPeqFrame.frame.size.width;
    } else {
        ptX = point.x;
    }
    
    if (point.y < 0.0) {
        ptY = 0.0;
    } else if (point.y > self.viewPeqFrame.frame.size.height) {
        ptY = self.viewPeqFrame.frame.size.height;
    } else {
        ptY = point.y;
    }
    
    // Update ball position
    btnBall4.center = CGPointMake(ptX, ptY);
    
    // Update labels
    CGFloat valueX = point.x, valueY = self.viewPeqFrame.frame.size.height - point.y;
    
    if (valueX < 0.0)
        valueX = 0.0;
    if (valueX > self.viewPeqFrame.frame.size.width)
        valueX = self.viewPeqFrame.frame.size.width;
    if (valueY < 0.0)
        valueY = 0.0;
    if (valueY > self.viewPeqFrame.frame.size.height)
        valueY = self.viewPeqFrame.frame.size.height;
    
    peq4F[currentChannel] = [self peqXToFreq:valueX];
    peq4G[currentChannel] = [self peqYToGain:valueY];
    lblPeq4ValueG.text = [Global getEqGainString:peq4G[currentChannel]];
    lblPeq4ValueF.text = [Global getEqFreqString:peq4F[currentChannel]];
    
    // Update graph
    if (peq4Filter[currentChannel] == EQ_PEAK) {
        peqData4 = [NSMutableArray arrayWithArray:[Global eqPeakData:[Global getEqFreq:peq4F[currentChannel]] eqDb:[Global getEqGain:peq4G[currentChannel]] eqQ:[Global getEqQ:peq4Q[currentChannel]]]];
    } else if (peq4Filter[currentChannel] == EQ_HIGH_SHELF) {
        peqData4 = [NSMutableArray arrayWithArray:[Global eqHighShelfData:[Global getEqFreq:peq4F[currentChannel]] eqDb:[Global getEqGain:peq4G[currentChannel]]]];
    } else {
        peqData4 = [NSMutableArray arrayWithArray:[Global eqLPFData:[Global getEqFreq:peq4F[currentChannel]]]];
    }
    [self calculatePeqData];
    
    // Send data
    switch (currentChannel) {
        case CHANNEL_CH_1:
            [viewController sendData:CMD_CH1_EQ4_DB commandType:DSP_WRITE dspId:DSP_1 value:peq4G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH1_EQ4_FREQ commandType:DSP_WRITE dspId:DSP_1 value:peq4F[currentChannel]];
            break;
        case CHANNEL_CH_2:
            [viewController sendData:CMD_CH2_EQ4_DB commandType:DSP_WRITE dspId:DSP_1 value:peq4G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH2_EQ4_FREQ commandType:DSP_WRITE dspId:DSP_1 value:peq4F[currentChannel]];
            break;
        case CHANNEL_CH_3:
            [viewController sendData:CMD_CH3_EQ4_DB commandType:DSP_WRITE dspId:DSP_1 value:peq4G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH3_EQ4_FREQ commandType:DSP_WRITE dspId:DSP_1 value:peq4F[currentChannel]];
            break;
        case CHANNEL_CH_4:
            [viewController sendData:CMD_CH4_EQ4_DB commandType:DSP_WRITE dspId:DSP_1 value:peq4G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH4_EQ4_FREQ commandType:DSP_WRITE dspId:DSP_1 value:peq4F[currentChannel]];
            break;
        case CHANNEL_CH_5:
            [viewController sendData:CMD_CH1_EQ4_DB commandType:DSP_WRITE dspId:DSP_2 value:peq4G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH1_EQ4_FREQ commandType:DSP_WRITE dspId:DSP_2 value:peq4F[currentChannel]];
            break;
        case CHANNEL_CH_6:
            [viewController sendData:CMD_CH2_EQ4_DB commandType:DSP_WRITE dspId:DSP_2 value:peq4G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH2_EQ4_FREQ commandType:DSP_WRITE dspId:DSP_2 value:peq4F[currentChannel]];
            break;
        case CHANNEL_CH_7:
            [viewController sendData:CMD_CH3_EQ4_DB commandType:DSP_WRITE dspId:DSP_2 value:peq4G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH3_EQ4_FREQ commandType:DSP_WRITE dspId:DSP_2 value:peq4F[currentChannel]];
            break;
        case CHANNEL_CH_8:
            [viewController sendData:CMD_CH4_EQ4_DB commandType:DSP_WRITE dspId:DSP_2 value:peq4G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH4_EQ4_FREQ commandType:DSP_WRITE dspId:DSP_2 value:peq4F[currentChannel]];
            break;
        case CHANNEL_CH_9:
            [viewController sendData:CMD_CH1_EQ4_DB commandType:DSP_WRITE dspId:DSP_3 value:peq4G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH1_EQ4_FREQ commandType:DSP_WRITE dspId:DSP_3 value:peq4F[currentChannel]];
            break;
        case CHANNEL_CH_10:
            [viewController sendData:CMD_CH2_EQ4_DB commandType:DSP_WRITE dspId:DSP_3 value:peq4G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH2_EQ4_FREQ commandType:DSP_WRITE dspId:DSP_3 value:peq4F[currentChannel]];
            break;
        case CHANNEL_CH_11:
            [viewController sendData:CMD_CH3_EQ4_DB commandType:DSP_WRITE dspId:DSP_3 value:peq4G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH3_EQ4_FREQ commandType:DSP_WRITE dspId:DSP_3 value:peq4F[currentChannel]];
            break;
        case CHANNEL_CH_12:
            [viewController sendData:CMD_CH4_EQ4_DB commandType:DSP_WRITE dspId:DSP_3 value:peq4G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH4_EQ4_FREQ commandType:DSP_WRITE dspId:DSP_3 value:peq4F[currentChannel]];
            break;
        case CHANNEL_CH_13:
            [viewController sendData:CMD_CH1_EQ4_DB commandType:DSP_WRITE dspId:DSP_4 value:peq4G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH1_EQ4_FREQ commandType:DSP_WRITE dspId:DSP_4 value:peq4F[currentChannel]];
            break;
        case CHANNEL_CH_14:
            [viewController sendData:CMD_CH2_EQ4_DB commandType:DSP_WRITE dspId:DSP_4 value:peq4G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH2_EQ4_FREQ commandType:DSP_WRITE dspId:DSP_4 value:peq4F[currentChannel]];
            break;
        case CHANNEL_CH_15:
            [viewController sendData:CMD_CH3_EQ4_DB commandType:DSP_WRITE dspId:DSP_4 value:peq4G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH3_EQ4_FREQ commandType:DSP_WRITE dspId:DSP_4 value:peq4F[currentChannel]];
            break;
        case CHANNEL_CH_16:
            [viewController sendData:CMD_CH4_EQ4_DB commandType:DSP_WRITE dspId:DSP_4 value:peq4G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH4_EQ4_FREQ commandType:DSP_WRITE dspId:DSP_4 value:peq4F[currentChannel]];
            break;
        case CHANNEL_CH_17:
            [viewController sendData:CMD_CH17_EQ4_DB commandType:DSP_WRITE dspId:DSP_6 value:peq4G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH17_EQ4_FREQ commandType:DSP_WRITE dspId:DSP_6 value:peq4F[currentChannel]];
            break;
        case CHANNEL_CH_18:
            [viewController sendData:CMD_CH18_EQ4_DB commandType:DSP_WRITE dspId:DSP_6 value:peq4G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_CH18_EQ4_FREQ commandType:DSP_WRITE dspId:DSP_6 value:peq4F[currentChannel]];
            break;
        case CHANNEL_MULTI_1:
            [viewController sendData:CMD_MULTI1_EQ4_DB commandType:DSP_WRITE dspId:DSP_7 value:peq4G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_MULTI1_EQ4_FREQ commandType:DSP_WRITE dspId:DSP_7 value:peq4F[currentChannel]];
            break;
        case CHANNEL_MULTI_2:
            [viewController sendData:CMD_MULTI2_EQ4_DB commandType:DSP_WRITE dspId:DSP_7 value:peq4G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_MULTI2_EQ4_FREQ commandType:DSP_WRITE dspId:DSP_7 value:peq4F[currentChannel]];
            break;
        case CHANNEL_MULTI_3:
            [viewController sendData:CMD_MULTI3_EQ4_DB commandType:DSP_WRITE dspId:DSP_7 value:peq4G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_MULTI3_EQ4_FREQ commandType:DSP_WRITE dspId:DSP_7 value:peq4F[currentChannel]];
            break;
        case CHANNEL_MULTI_4:
            [viewController sendData:CMD_MULTI4_EQ4_DB commandType:DSP_WRITE dspId:DSP_7 value:peq4G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_MULTI4_EQ4_FREQ commandType:DSP_WRITE dspId:DSP_7 value:peq4F[currentChannel]];
            break;
        case CHANNEL_MAIN:
            [viewController sendData:CMD_MAIN_LR_EQ4_DB commandType:DSP_WRITE dspId:DSP_6 value:peq4G[currentChannel]-EQ_GAIN_OFFSET];
            [viewController sendData:CMD_MAIN_LR_EQ4_FREQ commandType:DSP_WRITE dspId:DSP_6 value:peq4F[currentChannel]];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnBall4Released:(id)sender {
    btnBall4.selected = YES;
    peqLock = NO;
    [self updatePreviewImage];
}

#pragma mark - Chart behavior

-(void)initPlot {
    [self configureHost];
    [self configureGraph];
    [self configurePlots];
    [self configureAxes];
}

-(void)configureHost {
    self.hostView = [(CPTGraphHostingView *)[CPTGraphHostingView alloc] initWithFrame:CGRectMake(124, 108, 546, 227)];
    self.hostView.allowPinchScaling = NO;
    self.hostView.backgroundColor = [UIColor clearColor];
    [self.viewPeq insertSubview:self.hostView belowSubview:self.viewPeqFrame];
}

-(void)configureGraph {
    // Create the graph
    CPTGraph *graph = [[CPTXYGraph alloc] initWithFrame:self.hostView.frame];
    self.hostView.hostedGraph = graph;
    
    // Set padding for plot area
    [graph.plotAreaFrame setPaddingLeft:0.0f];
    [graph.plotAreaFrame setPaddingBottom:0.0f];
    
    // Enable user interactions for plot space
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *) graph.defaultPlotSpace;
    plotSpace.allowsUserInteraction = NO;
}

-(void)configurePlots {
    // Get graph and plot space
    CPTGraph *graph = self.hostView.hostedGraph;
    graph.axisSet = nil;
    graph.paddingLeft = 0.0;
    graph.paddingTop = 0.0;
    graph.paddingRight = 0.0;
    graph.paddingBottom = 0.0;
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *) graph.defaultPlotSpace;
    
    // Create the plot
    CPTScatterPlot *peqPlot = [[CPTScatterPlot alloc] init];
    peqPlot.dataSource = self;
    CPTColor *peqColor = [CPTColor colorWithComponentRed:247.0/255.0 green:247.0/255.0 blue:247.0/255.0 alpha:1.0];
    [graph addPlot:peqPlot toPlotSpace:plotSpace];
    
    // Set up plot space
    CGFloat xMin = 0.0f;
    CGFloat xMax = 210.0f;
    CGFloat yMin = -24.0f;
    CGFloat yMax = 24.0f;
    plotSpace.xRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(xMin) length:CPTDecimalFromFloat(xMax-xMin)];
    plotSpace.yRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(yMin) length:CPTDecimalFromFloat(yMax-yMin)];
    
    // Set up area fill
    peqPlot.areaFill = [CPTFill fillWithColor:[CPTColor colorWithComponentRed:197.0/255.0 green:197.0/255.0 blue:197.0/255.0 alpha:1.0]];
    peqPlot.areaBaseValue = CPTDecimalFromInt(0);
    
    // Create styles and symbols
    CPTMutableLineStyle *peqLineStyle = [peqPlot.dataLineStyle mutableCopy];
    peqLineStyle.lineWidth = 5.0;
    peqLineStyle.lineColor = peqColor;
    peqPlot.dataLineStyle = peqLineStyle;
    peqPlot.plotSymbol = nil;
    peqPlot.interpolation = CPTScatterPlotInterpolationCurved;
}

-(void)configureAxes {
}

#pragma mark - CPTPlotDataSource methods

-(NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot {
    return [peqData count];
}

-(NSNumber *)numberForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)index {
    NSInteger valueCount = [peqData count];
    switch (fieldEnum) {
        case CPTScatterPlotFieldX:
            if (index < valueCount) {
                return [NSNumber numberWithUnsignedInteger:index];
            }
            break;
            
        case CPTScatterPlotFieldY:
            if (index < valueCount) {
                return (NSNumber *)[peqData objectAtIndex:index];
            }
            break;
    }
    return [NSDecimalNumber zero];
}

#pragma mark - UIPinchGestureRecognizer methods

- (IBAction)handlePinch:(UIPinchGestureRecognizer *)recognizer {
    
    double scale = recognizer.scale;
    recognizer.scale = 1;
    
    if (currentBall == 1) {
        if (peq1Q[currentChannel] > 0) {
            peq1Q[currentChannel] = round(peq1Q[currentChannel]/scale);
        } else if (scale < 1.0) {
            peq1Q[currentChannel] = 1.0;
        }
        if (peq1Q[currentChannel] < 0.0) peq1Q[currentChannel] = 0.0;
        if (peq1Q[currentChannel] > PEQ_23_Q_MAX_VALUE) peq1Q[currentChannel] = PEQ_23_Q_MAX_VALUE;
        lastPeq1Q[currentChannel] = peq1Q[currentChannel];

        [lblPeq1ValueQ setText:[Global getEqQ1String:peq1Q[currentChannel]]];
        if (peq1Filter[currentChannel] == EQ_PEAK) {
            peqData1 = [NSMutableArray arrayWithArray:[Global eqPeakData:[Global getEqFreq:peq1F[currentChannel]] eqDb:[Global getEqGain:peq1G[currentChannel]] eqQ:[Global getEqQ:peq1Q[currentChannel]]]];
        } else if (peq1Filter[currentChannel] == EQ_LOW_SHELF) {
            peqData1 = [NSMutableArray arrayWithArray:[Global eqLowShelfData:[Global getEqFreq:peq1F[currentChannel]] eqDb:[Global getEqGain:peq1G[currentChannel]]]];
        } else {
            peqData1 = [NSMutableArray arrayWithArray:[Global eqHPFData:[Global getEqFreq:peq1F[currentChannel]]]];
        }
        [self calculatePeqData];
        
        // Send data
        switch (currentChannel) {
            case CHANNEL_CH_1:
                [viewController sendData:CMD_CH1_EQ1_Q commandType:DSP_WRITE dspId:DSP_1 value:peq1Q[currentChannel]];
                break;
            case CHANNEL_CH_2:
                [viewController sendData:CMD_CH2_EQ1_Q commandType:DSP_WRITE dspId:DSP_1 value:peq1Q[currentChannel]];
                break;
            case CHANNEL_CH_3:
                [viewController sendData:CMD_CH3_EQ1_Q commandType:DSP_WRITE dspId:DSP_1 value:peq1Q[currentChannel]];
                break;
            case CHANNEL_CH_4:
                [viewController sendData:CMD_CH4_EQ1_Q commandType:DSP_WRITE dspId:DSP_1 value:peq1Q[currentChannel]];
                break;
            case CHANNEL_CH_5:
                [viewController sendData:CMD_CH1_EQ1_Q commandType:DSP_WRITE dspId:DSP_2 value:peq1Q[currentChannel]];
                break;
            case CHANNEL_CH_6:
                [viewController sendData:CMD_CH2_EQ1_Q commandType:DSP_WRITE dspId:DSP_2 value:peq1Q[currentChannel]];
                break;
            case CHANNEL_CH_7:
                [viewController sendData:CMD_CH3_EQ1_Q commandType:DSP_WRITE dspId:DSP_2 value:peq1Q[currentChannel]];
                break;
            case CHANNEL_CH_8:
                [viewController sendData:CMD_CH4_EQ1_Q commandType:DSP_WRITE dspId:DSP_2 value:peq1Q[currentChannel]];
                break;
            case CHANNEL_CH_9:
                [viewController sendData:CMD_CH1_EQ1_Q commandType:DSP_WRITE dspId:DSP_3 value:peq1Q[currentChannel]];
                break;
            case CHANNEL_CH_10:
                [viewController sendData:CMD_CH2_EQ1_Q commandType:DSP_WRITE dspId:DSP_3 value:peq1Q[currentChannel]];
                break;
            case CHANNEL_CH_11:
                [viewController sendData:CMD_CH3_EQ1_Q commandType:DSP_WRITE dspId:DSP_3 value:peq1Q[currentChannel]];
                break;
            case CHANNEL_CH_12:
                [viewController sendData:CMD_CH4_EQ1_Q commandType:DSP_WRITE dspId:DSP_3 value:peq1Q[currentChannel]];
                break;
            case CHANNEL_CH_13:
                [viewController sendData:CMD_CH1_EQ1_Q commandType:DSP_WRITE dspId:DSP_4 value:peq1Q[currentChannel]];
                break;
            case CHANNEL_CH_14:
                [viewController sendData:CMD_CH2_EQ1_Q commandType:DSP_WRITE dspId:DSP_4 value:peq1Q[currentChannel]];
                break;
            case CHANNEL_CH_15:
                [viewController sendData:CMD_CH3_EQ1_Q commandType:DSP_WRITE dspId:DSP_4 value:peq1Q[currentChannel]];
                break;
            case CHANNEL_CH_16:
                [viewController sendData:CMD_CH4_EQ1_Q commandType:DSP_WRITE dspId:DSP_4 value:peq1Q[currentChannel]];
                break;
            case CHANNEL_CH_17:
                [viewController sendData:CMD_CH17_EQ1_Q commandType:DSP_WRITE dspId:DSP_6 value:peq1Q[currentChannel]];
                break;
            case CHANNEL_CH_18:
                [viewController sendData:CMD_CH18_EQ1_Q commandType:DSP_WRITE dspId:DSP_6 value:peq1Q[currentChannel]];
                break;
            case CHANNEL_MULTI_1:
                [viewController sendData:CMD_MULTI1_EQ1_Q commandType:DSP_WRITE dspId:DSP_7 value:peq1Q[currentChannel]];
                break;
            case CHANNEL_MULTI_2:
                [viewController sendData:CMD_MULTI2_EQ1_Q commandType:DSP_WRITE dspId:DSP_7 value:peq1Q[currentChannel]];
                break;
            case CHANNEL_MULTI_3:
                [viewController sendData:CMD_MULTI3_EQ1_Q commandType:DSP_WRITE dspId:DSP_7 value:peq1Q[currentChannel]];
                break;
            case CHANNEL_MULTI_4:
                [viewController sendData:CMD_MULTI4_EQ1_Q commandType:DSP_WRITE dspId:DSP_7 value:peq1Q[currentChannel]];
                break;
            case CHANNEL_MAIN:
                [viewController sendData:CMD_MAIN_LR_EQ1_Q commandType:DSP_WRITE dspId:DSP_6 value:peq1Q[currentChannel]];
                break;
            default:
                break;
        }
    } else if (currentBall == 2) {
        if (peq2Q[currentChannel] > 0) {
            peq2Q[currentChannel] = round(peq2Q[currentChannel]/scale);
        }
        else if (scale < 1.0) {
            peq2Q[currentChannel] = 1.0;
        }
        if (peq2Q[currentChannel] < 0.0) peq2Q[currentChannel] = 0.0;
        if (peq2Q[currentChannel] > PEQ_23_Q_MAX_VALUE) peq2Q[currentChannel] = PEQ_23_Q_MAX_VALUE;
        
        [lblPeq2ValueQ setText:[Global getEqQ23String:peq2Q[currentChannel]]];
        peqData2 = [NSMutableArray arrayWithArray:[Global eqPeakData:[Global getEqFreq:peq2F[currentChannel]] eqDb:[Global getEqGain:peq2G[currentChannel]] eqQ:[Global getEqQ:peq2Q[currentChannel]]]];
        [self calculatePeqData];
        
        // Send data
        switch (currentChannel) {
            case CHANNEL_CH_1:
                [viewController sendData:CMD_CH1_EQ2_Q commandType:DSP_WRITE dspId:DSP_1 value:peq2Q[currentChannel]];
                break;
            case CHANNEL_CH_2:
                [viewController sendData:CMD_CH2_EQ2_Q commandType:DSP_WRITE dspId:DSP_1 value:peq2Q[currentChannel]];
                break;
            case CHANNEL_CH_3:
                [viewController sendData:CMD_CH3_EQ2_Q commandType:DSP_WRITE dspId:DSP_1 value:peq2Q[currentChannel]];
                break;
            case CHANNEL_CH_4:
                [viewController sendData:CMD_CH4_EQ2_Q commandType:DSP_WRITE dspId:DSP_1 value:peq2Q[currentChannel]];
                break;
            case CHANNEL_CH_5:
                [viewController sendData:CMD_CH1_EQ2_Q commandType:DSP_WRITE dspId:DSP_2 value:peq2Q[currentChannel]];
                break;
            case CHANNEL_CH_6:
                [viewController sendData:CMD_CH2_EQ2_Q commandType:DSP_WRITE dspId:DSP_2 value:peq2Q[currentChannel]];
                break;
            case CHANNEL_CH_7:
                [viewController sendData:CMD_CH3_EQ2_Q commandType:DSP_WRITE dspId:DSP_2 value:peq2Q[currentChannel]];
                break;
            case CHANNEL_CH_8:
                [viewController sendData:CMD_CH4_EQ2_Q commandType:DSP_WRITE dspId:DSP_2 value:peq2Q[currentChannel]];
                break;
            case CHANNEL_CH_9:
                [viewController sendData:CMD_CH1_EQ2_Q commandType:DSP_WRITE dspId:DSP_3 value:peq2Q[currentChannel]];
                break;
            case CHANNEL_CH_10:
                [viewController sendData:CMD_CH2_EQ2_Q commandType:DSP_WRITE dspId:DSP_3 value:peq2Q[currentChannel]];
                break;
            case CHANNEL_CH_11:
                [viewController sendData:CMD_CH3_EQ2_Q commandType:DSP_WRITE dspId:DSP_3 value:peq2Q[currentChannel]];
                break;
            case CHANNEL_CH_12:
                [viewController sendData:CMD_CH4_EQ2_Q commandType:DSP_WRITE dspId:DSP_3 value:peq2Q[currentChannel]];
                break;
            case CHANNEL_CH_13:
                [viewController sendData:CMD_CH1_EQ2_Q commandType:DSP_WRITE dspId:DSP_4 value:peq2Q[currentChannel]];
                break;
            case CHANNEL_CH_14:
                [viewController sendData:CMD_CH2_EQ2_Q commandType:DSP_WRITE dspId:DSP_4 value:peq2Q[currentChannel]];
                break;
            case CHANNEL_CH_15:
                [viewController sendData:CMD_CH3_EQ2_Q commandType:DSP_WRITE dspId:DSP_4 value:peq2Q[currentChannel]];
                break;
            case CHANNEL_CH_16:
                [viewController sendData:CMD_CH4_EQ2_Q commandType:DSP_WRITE dspId:DSP_4 value:peq2Q[currentChannel]];
                break;
            case CHANNEL_CH_17:
                [viewController sendData:CMD_CH17_EQ2_Q commandType:DSP_WRITE dspId:DSP_6 value:peq2Q[currentChannel]];
                break;
            case CHANNEL_CH_18:
                [viewController sendData:CMD_CH18_EQ2_Q commandType:DSP_WRITE dspId:DSP_6 value:peq2Q[currentChannel]];
                break;
            case CHANNEL_MULTI_1:
                [viewController sendData:CMD_MULTI1_EQ2_Q commandType:DSP_WRITE dspId:DSP_7 value:peq2Q[currentChannel]];
                break;
            case CHANNEL_MULTI_2:
                [viewController sendData:CMD_MULTI2_EQ2_Q commandType:DSP_WRITE dspId:DSP_7 value:peq2Q[currentChannel]];
                break;
            case CHANNEL_MULTI_3:
                [viewController sendData:CMD_MULTI3_EQ2_Q commandType:DSP_WRITE dspId:DSP_7 value:peq2Q[currentChannel]];
                break;
            case CHANNEL_MULTI_4:
                [viewController sendData:CMD_MULTI4_EQ2_Q commandType:DSP_WRITE dspId:DSP_7 value:peq2Q[currentChannel]];
                break;
            case CHANNEL_MAIN:
                [viewController sendData:CMD_MAIN_LR_EQ2_Q commandType:DSP_WRITE dspId:DSP_6 value:peq2Q[currentChannel]];
                break;
            default:
                break;
        }
    } else if (currentBall == 3) {
        if (peq3Q[currentChannel] > 0) {
            peq3Q[currentChannel] = round(peq3Q[currentChannel]/scale);
        }
        else if (scale < 1.0) {
            peq3Q[currentChannel] = 1.0;
        }
        if (peq3Q[currentChannel] < 0.0) peq3Q[currentChannel] = 0.0;
        if (peq3Q[currentChannel] > PEQ_23_Q_MAX_VALUE) peq3Q[currentChannel] = PEQ_23_Q_MAX_VALUE;

        [lblPeq3ValueQ setText:[Global getEqQ23String:peq3Q[currentChannel]]];
        peqData3 = [NSMutableArray arrayWithArray:[Global eqPeakData:[Global getEqFreq:peq3F[currentChannel]] eqDb:[Global getEqGain:peq3G[currentChannel]] eqQ:[Global getEqQ:peq3Q[currentChannel]]]];
        [self calculatePeqData];
        
        // Send data
        switch (currentChannel) {
            case CHANNEL_CH_1:
                [viewController sendData:CMD_CH1_EQ3_Q commandType:DSP_WRITE dspId:DSP_1 value:peq3Q[currentChannel]];
                break;
            case CHANNEL_CH_2:
                [viewController sendData:CMD_CH2_EQ3_Q commandType:DSP_WRITE dspId:DSP_1 value:peq3Q[currentChannel]];
                break;
            case CHANNEL_CH_3:
                [viewController sendData:CMD_CH3_EQ3_Q commandType:DSP_WRITE dspId:DSP_1 value:peq3Q[currentChannel]];
                break;
            case CHANNEL_CH_4:
                [viewController sendData:CMD_CH4_EQ3_Q commandType:DSP_WRITE dspId:DSP_1 value:peq3Q[currentChannel]];
                break;
            case CHANNEL_CH_5:
                [viewController sendData:CMD_CH1_EQ3_Q commandType:DSP_WRITE dspId:DSP_2 value:peq3Q[currentChannel]];
                break;
            case CHANNEL_CH_6:
                [viewController sendData:CMD_CH2_EQ3_Q commandType:DSP_WRITE dspId:DSP_2 value:peq3Q[currentChannel]];
                break;
            case CHANNEL_CH_7:
                [viewController sendData:CMD_CH3_EQ3_Q commandType:DSP_WRITE dspId:DSP_2 value:peq3Q[currentChannel]];
                break;
            case CHANNEL_CH_8:
                [viewController sendData:CMD_CH4_EQ3_Q commandType:DSP_WRITE dspId:DSP_2 value:peq3Q[currentChannel]];
                break;
            case CHANNEL_CH_9:
                [viewController sendData:CMD_CH1_EQ3_Q commandType:DSP_WRITE dspId:DSP_3 value:peq3Q[currentChannel]];
                break;
            case CHANNEL_CH_10:
                [viewController sendData:CMD_CH2_EQ3_Q commandType:DSP_WRITE dspId:DSP_3 value:peq3Q[currentChannel]];
                break;
            case CHANNEL_CH_11:
                [viewController sendData:CMD_CH3_EQ3_Q commandType:DSP_WRITE dspId:DSP_3 value:peq3Q[currentChannel]];
                break;
            case CHANNEL_CH_12:
                [viewController sendData:CMD_CH4_EQ3_Q commandType:DSP_WRITE dspId:DSP_3 value:peq3Q[currentChannel]];
                break;
            case CHANNEL_CH_13:
                [viewController sendData:CMD_CH1_EQ3_Q commandType:DSP_WRITE dspId:DSP_4 value:peq3Q[currentChannel]];
                break;
            case CHANNEL_CH_14:
                [viewController sendData:CMD_CH2_EQ3_Q commandType:DSP_WRITE dspId:DSP_4 value:peq3Q[currentChannel]];
                break;
            case CHANNEL_CH_15:
                [viewController sendData:CMD_CH3_EQ3_Q commandType:DSP_WRITE dspId:DSP_4 value:peq3Q[currentChannel]];
                break;
            case CHANNEL_CH_16:
                [viewController sendData:CMD_CH4_EQ3_Q commandType:DSP_WRITE dspId:DSP_4 value:peq3Q[currentChannel]];
                break;
            case CHANNEL_CH_17:
                [viewController sendData:CMD_CH17_EQ3_Q commandType:DSP_WRITE dspId:DSP_6 value:peq3Q[currentChannel]];
                break;
            case CHANNEL_CH_18:
                [viewController sendData:CMD_CH18_EQ3_Q commandType:DSP_WRITE dspId:DSP_6 value:peq3Q[currentChannel]];
                break;
            case CHANNEL_MULTI_1:
                [viewController sendData:CMD_MULTI1_EQ3_Q commandType:DSP_WRITE dspId:DSP_7 value:peq3Q[currentChannel]];
                break;
            case CHANNEL_MULTI_2:
                [viewController sendData:CMD_MULTI2_EQ3_Q commandType:DSP_WRITE dspId:DSP_7 value:peq3Q[currentChannel]];
                break;
            case CHANNEL_MULTI_3:
                [viewController sendData:CMD_MULTI3_EQ3_Q commandType:DSP_WRITE dspId:DSP_7 value:peq3Q[currentChannel]];
                break;
            case CHANNEL_MULTI_4:
                [viewController sendData:CMD_MULTI4_EQ3_Q commandType:DSP_WRITE dspId:DSP_7 value:peq3Q[currentChannel]];
                break;
            case CHANNEL_MAIN:
                [viewController sendData:CMD_MAIN_LR_EQ3_Q commandType:DSP_WRITE dspId:DSP_6 value:peq3Q[currentChannel]];
                break;
            default:
                break;
        }
    } else if (currentBall == 4) {
        if (peq4Q[currentChannel] > 0) {
            peq4Q[currentChannel] = round(peq4Q[currentChannel]/scale);
        }
        else if (scale < 1.0) {
            peq4Q[currentChannel] = 1.0;
        }
        if (peq4Q[currentChannel] < 0.0) peq4Q[currentChannel] = 0.0;
        if (peq4Q[currentChannel] > PEQ_23_Q_MAX_VALUE) peq4Q[currentChannel] = PEQ_23_Q_MAX_VALUE;
        lastPeq4Q[currentChannel] = peq4Q[currentChannel];
        
        [lblPeq4ValueQ setText:[Global getEqQ4String:peq4Q[currentChannel]]];
        if (peq4Filter[currentChannel] == EQ_PEAK) {
            peqData4 = [NSMutableArray arrayWithArray:[Global eqPeakData:[Global getEqFreq:peq4F[currentChannel]] eqDb:[Global getEqGain:peq4G[currentChannel]] eqQ:[Global getEqQ:peq4Q[currentChannel]]]];
        } else if (peq4Filter[currentChannel] == EQ_HIGH_SHELF) {
            peqData4 = [NSMutableArray arrayWithArray:[Global eqHighShelfData:[Global getEqFreq:peq4F[currentChannel]] eqDb:[Global getEqGain:peq4G[currentChannel]]]];
        } else {
            peqData4 = [NSMutableArray arrayWithArray:[Global eqLPFData:[Global getEqFreq:peq4F[currentChannel]]]];
        }
        [self calculatePeqData];
        
        // Send data
        switch (currentChannel) {
            case CHANNEL_CH_1:
                [viewController sendData:CMD_CH1_EQ4_Q commandType:DSP_WRITE dspId:DSP_1 value:peq4Q[currentChannel]];
                break;
            case CHANNEL_CH_2:
                [viewController sendData:CMD_CH2_EQ4_Q commandType:DSP_WRITE dspId:DSP_1 value:peq4Q[currentChannel]];
                break;
            case CHANNEL_CH_3:
                [viewController sendData:CMD_CH3_EQ4_Q commandType:DSP_WRITE dspId:DSP_1 value:peq4Q[currentChannel]];
                break;
            case CHANNEL_CH_4:
                [viewController sendData:CMD_CH4_EQ4_Q commandType:DSP_WRITE dspId:DSP_1 value:peq4Q[currentChannel]];
                break;
            case CHANNEL_CH_5:
                [viewController sendData:CMD_CH1_EQ4_Q commandType:DSP_WRITE dspId:DSP_2 value:peq4Q[currentChannel]];
                break;
            case CHANNEL_CH_6:
                [viewController sendData:CMD_CH2_EQ4_Q commandType:DSP_WRITE dspId:DSP_2 value:peq4Q[currentChannel]];
                break;
            case CHANNEL_CH_7:
                [viewController sendData:CMD_CH3_EQ4_Q commandType:DSP_WRITE dspId:DSP_2 value:peq4Q[currentChannel]];
                break;
            case CHANNEL_CH_8:
                [viewController sendData:CMD_CH4_EQ4_Q commandType:DSP_WRITE dspId:DSP_2 value:peq4Q[currentChannel]];
                break;
            case CHANNEL_CH_9:
                [viewController sendData:CMD_CH1_EQ4_Q commandType:DSP_WRITE dspId:DSP_3 value:peq4Q[currentChannel]];
                break;
            case CHANNEL_CH_10:
                [viewController sendData:CMD_CH2_EQ4_Q commandType:DSP_WRITE dspId:DSP_3 value:peq4Q[currentChannel]];
                break;
            case CHANNEL_CH_11:
                [viewController sendData:CMD_CH3_EQ4_Q commandType:DSP_WRITE dspId:DSP_3 value:peq4Q[currentChannel]];
                break;
            case CHANNEL_CH_12:
                [viewController sendData:CMD_CH4_EQ4_Q commandType:DSP_WRITE dspId:DSP_3 value:peq4Q[currentChannel]];
                break;
            case CHANNEL_CH_13:
                [viewController sendData:CMD_CH1_EQ4_Q commandType:DSP_WRITE dspId:DSP_4 value:peq4Q[currentChannel]];
                break;
            case CHANNEL_CH_14:
                [viewController sendData:CMD_CH2_EQ4_Q commandType:DSP_WRITE dspId:DSP_4 value:peq4Q[currentChannel]];
                break;
            case CHANNEL_CH_15:
                [viewController sendData:CMD_CH3_EQ4_Q commandType:DSP_WRITE dspId:DSP_4 value:peq4Q[currentChannel]];
                break;
            case CHANNEL_CH_16:
                [viewController sendData:CMD_CH4_EQ4_Q commandType:DSP_WRITE dspId:DSP_4 value:peq4Q[currentChannel]];
                break;
            case CHANNEL_CH_17:
                [viewController sendData:CMD_CH17_EQ4_Q commandType:DSP_WRITE dspId:DSP_6 value:peq4Q[currentChannel]];
                break;
            case CHANNEL_CH_18:
                [viewController sendData:CMD_CH18_EQ4_Q commandType:DSP_WRITE dspId:DSP_6 value:peq4Q[currentChannel]];
                break;
            case CHANNEL_MULTI_1:
                [viewController sendData:CMD_MULTI1_EQ4_Q commandType:DSP_WRITE dspId:DSP_7 value:peq4Q[currentChannel]];
                break;
            case CHANNEL_MULTI_2:
                [viewController sendData:CMD_MULTI2_EQ4_Q commandType:DSP_WRITE dspId:DSP_7 value:peq4Q[currentChannel]];
                break;
            case CHANNEL_MULTI_3:
                [viewController sendData:CMD_MULTI3_EQ4_Q commandType:DSP_WRITE dspId:DSP_7 value:peq4Q[currentChannel]];
                break;
            case CHANNEL_MULTI_4:
                [viewController sendData:CMD_MULTI4_EQ4_Q commandType:DSP_WRITE dspId:DSP_7 value:peq4Q[currentChannel]];
                break;
            case CHANNEL_MAIN:
                [viewController sendData:CMD_MAIN_LR_EQ4_Q commandType:DSP_WRITE dspId:DSP_6 value:peq4Q[currentChannel]];
                break;
            default:
                break;
        }
    }
}

- (IBAction)onBtn1GDown:(id)sender {
}

- (IBAction)onBtn1GUp:(id)sender {
}

- (IBAction)onBtn2GDown:(id)sender {
}

- (IBAction)onBtn2GUp:(id)sender {
}

- (IBAction)onBtn3GDown:(id)sender {
}

- (IBAction)onBtn3GUp:(id)sender {
}

- (IBAction)onBtn4GDown:(id)sender {
}

- (IBAction)onBtn4GUp:(id)sender {
}

- (IBAction)onBtn1FDown:(id)sender {
}

- (IBAction)onBtn1FUp:(id)sender {
}

- (IBAction)onBtn2FDown:(id)sender {
}

- (IBAction)onBtn2FUp:(id)sender {
}

- (IBAction)onBtn3FDown:(id)sender {
}

- (IBAction)onBtn3FUp:(id)sender {
}

- (IBAction)onBtn4FDown:(id)sender {
}

- (IBAction)onBtn4FUp:(id)sender {
}

- (IBAction)onBtn1QDown:(id)sender {
}

- (IBAction)onBtn1QUp:(id)sender {
}

- (IBAction)onBtn2QDown:(id)sender {
}

- (IBAction)onBtn2QUp:(id)sender {
}

- (IBAction)onBtn3QDown:(id)sender {
}

- (IBAction)onBtn3QUp:(id)sender {
}

- (IBAction)onBtn4QDown:(id)sender {
}

- (IBAction)onBtn4QUp:(id)sender {
}

#pragma mark -
#pragma mark Text label helper methods

- (void)updateCurrentChannelLabel
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    switch (currentChannelLabel) {
        case CHANNEL_CH_1:
            if ([userDefaults objectForKey:@"Ch1"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch1"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch1", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch1", @"Acapela", nil)];
            break;
        case CHANNEL_CH_2:
            if ([userDefaults objectForKey:@"Ch2"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch2"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch2", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch2", @"Acapela", nil)];
            break;
        case CHANNEL_CH_3:
            if ([userDefaults objectForKey:@"Ch3"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch3"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch3", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch3", @"Acapela", nil)];
            break;
        case CHANNEL_CH_4:
            if ([userDefaults objectForKey:@"Ch4"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch4"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch4", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch4", @"Acapela", nil)];
            break;
        case CHANNEL_CH_5:
            if ([userDefaults objectForKey:@"Ch5"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch5"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch5", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch5", @"Acapela", nil)];
            break;
        case CHANNEL_CH_6:
            if ([userDefaults objectForKey:@"Ch6"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch6"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch6", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch6", @"Acapela", nil)];
            break;
        case CHANNEL_CH_7:
            if ([userDefaults objectForKey:@"Ch7"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch7"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch7", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch7", @"Acapela", nil)];
            break;
        case CHANNEL_CH_8:
            if ([userDefaults objectForKey:@"Ch8"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch8"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch8", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch8", @"Acapela", nil)];
            break;
        case CHANNEL_CH_9:
            if ([userDefaults objectForKey:@"Ch9"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch9"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch9", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch9", @"Acapela", nil)];
            break;
        case CHANNEL_CH_10:
            if ([userDefaults objectForKey:@"Ch10"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch10"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch10", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch10", @"Acapela", nil)];
            break;
        case CHANNEL_CH_11:
            if ([userDefaults objectForKey:@"Ch11"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch11"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch11", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch11", @"Acapela", nil)];
            break;
        case CHANNEL_CH_12:
            if ([userDefaults objectForKey:@"Ch12"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch12"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch12", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch12", @"Acapela", nil)];
            break;
        case CHANNEL_CH_13:
            if ([userDefaults objectForKey:@"Ch13"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch13"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch13", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch13", @"Acapela", nil)];
            break;
        case CHANNEL_CH_14:
            if ([userDefaults objectForKey:@"Ch14"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch14"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch14", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch14", @"Acapela", nil)];
            break;
        case CHANNEL_CH_15:
            if ([userDefaults objectForKey:@"Ch15"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch15"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch15", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch15", @"Acapela", nil)];
            break;
        case CHANNEL_CH_16:
            if ([userDefaults objectForKey:@"Ch16"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch16"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch16", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch16", @"Acapela", nil)];
            break;
        case CHANNEL_CH_17:
            if ([userDefaults objectForKey:@"Ch17"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch17"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch17", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch17", @"Acapela", nil)];
            break;
        case CHANNEL_CH_18:
            if ([userDefaults objectForKey:@"Ch18"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch18"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch18", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch18", @"Acapela", nil)];
            break;
        case CHANNEL_MULTI_1:
            if ([userDefaults objectForKey:@"Multi1"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Multi1"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Multi1", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Multi1", @"Acapela", nil)];
            break;
        case CHANNEL_MULTI_2:
            if ([userDefaults objectForKey:@"Multi2"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Multi2"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Multi2", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Multi2", @"Acapela", nil)];
            break;
        case CHANNEL_MULTI_3:
            if ([userDefaults objectForKey:@"Multi3"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Multi3"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Multi3", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Multi3", @"Acapela", nil)];
            break;
        case CHANNEL_MULTI_4:
            if ([userDefaults objectForKey:@"Multi4"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Multi4"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Multi4", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Multi4", @"Acapela", nil)];
            break;
        case CHANNEL_MAIN:
            if ([userDefaults objectForKey:@"Main"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Main"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Main", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Main", @"Acapela", nil)];
            break;
        default:
            break;
    }
}

- (void)writeToLabel
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    // Update name to original text field
    if (currentChannelLabel == currentChannel) {
        [txtPage setText:txtPage.text];
    }
    
    // Save to user defaults
    switch (currentChannelLabel) {
        case CHANNEL_CH_1:
            [userDefaults setObject:txtPage.text forKey:@"Ch1"];
            [viewController setAudioName:1 channelNum:0 channelName:[txtPage.text UTF8String]];
            break;
        case CHANNEL_CH_2:
            [userDefaults setObject:txtPage.text forKey:@"Ch2"];
            [viewController setAudioName:1 channelNum:1 channelName:[txtPage.text UTF8String]];
            break;
        case CHANNEL_CH_3:
            [userDefaults setObject:txtPage.text forKey:@"Ch3"];
            [viewController setAudioName:1 channelNum:2 channelName:[txtPage.text UTF8String]];
            break;
        case CHANNEL_CH_4:
            [userDefaults setObject:txtPage.text forKey:@"Ch4"];
            [viewController setAudioName:1 channelNum:3 channelName:[txtPage.text UTF8String]];
            break;
        case CHANNEL_CH_5:
            [userDefaults setObject:txtPage.text forKey:@"Ch5"];
            [viewController setAudioName:1 channelNum:4 channelName:[txtPage.text UTF8String]];
            break;
        case CHANNEL_CH_6:
            [userDefaults setObject:txtPage.text forKey:@"Ch6"];
            [viewController setAudioName:1 channelNum:5 channelName:[txtPage.text UTF8String]];
            break;
        case CHANNEL_CH_7:
            [userDefaults setObject:txtPage.text forKey:@"Ch7"];
            [viewController setAudioName:1 channelNum:6 channelName:[txtPage.text UTF8String]];
            break;
        case CHANNEL_CH_8:
            [userDefaults setObject:txtPage.text forKey:@"Ch8"];
            [viewController setAudioName:1 channelNum:7 channelName:[txtPage.text UTF8String]];
            break;
        case CHANNEL_CH_9:
            [userDefaults setObject:txtPage.text forKey:@"Ch9"];
            [viewController setAudioName:1 channelNum:8 channelName:[txtPage.text UTF8String]];
            break;
        case CHANNEL_CH_10:
            [userDefaults setObject:txtPage.text forKey:@"Ch10"];
            [viewController setAudioName:1 channelNum:9 channelName:[txtPage.text UTF8String]];
            break;
        case CHANNEL_CH_11:
            [userDefaults setObject:txtPage.text forKey:@"Ch11"];
            [viewController setAudioName:1 channelNum:10 channelName:[txtPage.text UTF8String]];
            break;
        case CHANNEL_CH_12:
            [userDefaults setObject:txtPage.text forKey:@"Ch12"];
            [viewController setAudioName:1 channelNum:11 channelName:[txtPage.text UTF8String]];
            break;
        case CHANNEL_CH_13:
            [userDefaults setObject:txtPage.text forKey:@"Ch13"];
            [viewController setAudioName:1 channelNum:12 channelName:[txtPage.text UTF8String]];
            break;
        case CHANNEL_CH_14:
            [userDefaults setObject:txtPage.text forKey:@"Ch14"];
            [viewController setAudioName:1 channelNum:13 channelName:[txtPage.text UTF8String]];
            break;
        case CHANNEL_CH_15:
            [userDefaults setObject:txtPage.text forKey:@"Ch15"];
            [viewController setAudioName:1 channelNum:14 channelName:[txtPage.text UTF8String]];
            break;
        case CHANNEL_CH_16:
            [userDefaults setObject:txtPage.text forKey:@"Ch16"];
            [viewController setAudioName:1 channelNum:15 channelName:[txtPage.text UTF8String]];
            break;
        case CHANNEL_CH_17:
            [userDefaults setObject:txtPage.text forKey:@"Ch17"];
            [viewController setAudioName:1 channelNum:16 channelName:[txtPage.text UTF8String]];
            break;
        case CHANNEL_CH_18:
            [userDefaults setObject:txtPage.text forKey:@"Ch18"];
            [viewController setAudioName:1 channelNum:17 channelName:[txtPage.text UTF8String]];
            break;
        case CHANNEL_MULTI_1:
            [userDefaults setObject:txtPage.text forKey:@"Multi1"];
            [viewController setAudioName:4 channelNum:0 channelName:[txtPage.text UTF8String]];
            break;
        case CHANNEL_MULTI_2:
            [userDefaults setObject:txtPage.text forKey:@"Multi2"];
            [viewController setAudioName:4 channelNum:1 channelName:[txtPage.text UTF8String]];
            break;
        case CHANNEL_MULTI_3:
            [userDefaults setObject:txtPage.text forKey:@"Multi3"];
            [viewController setAudioName:4 channelNum:2 channelName:[txtPage.text UTF8String]];
            break;
        case CHANNEL_MULTI_4:
            [userDefaults setObject:txtPage.text forKey:@"Multi4"];
            [viewController setAudioName:4 channelNum:3 channelName:[txtPage.text UTF8String]];
            break;
        case CHANNEL_MAIN:
            [userDefaults setObject:txtPage.text forKey:@"Main"];
            [viewController setAudioName:0 channelNum:0 channelName:[txtPage.text UTF8String]];
            break;
        default:
            break;
    }
    
    [userDefaults synchronize];
}

#pragma mark -
#pragma mark GEQ event handler methods

// Convert touch point to slider value
- (float)pointToSliderValue:(CGPoint)point
{
    return (12.0 - (12.0/226.0) * point.y);
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    geqLock = YES;
    
    UITouch *touch = [touches anyObject];
    
    // Handle GEQ draw
    if (!viewGeq.isHidden && btnGeqDraw.selected) {
        CGPoint touchPoint = [touch locationInView:self.viewGeqDraw];
        //NSLog(@"Touch point L = (%.f, %.f)", touchPoint.x, touchPoint.y);
        
        if (CGRectContainsPoint(sliderGeq1.frame, touchPoint)) {
            sliderGeq1.value = [self pointToSliderValue:touchPoint];
            [self onSlider1Action:sliderGeq1];
        } else if (CGRectContainsPoint(sliderGeq2.frame, touchPoint)) {
            sliderGeq2.value = [self pointToSliderValue:touchPoint];
            [self onSlider2Action:sliderGeq2];
        } else if (CGRectContainsPoint(sliderGeq3.frame, touchPoint)) {
            sliderGeq3.value = [self pointToSliderValue:touchPoint];
            [self onSlider3Action:sliderGeq3];
        } else if (CGRectContainsPoint(sliderGeq4.frame, touchPoint)) {
            sliderGeq4.value = [self pointToSliderValue:touchPoint];
            [self onSlider4Action:sliderGeq4];
        } else if (CGRectContainsPoint(sliderGeq5.frame, touchPoint)) {
            sliderGeq5.value = [self pointToSliderValue:touchPoint];
            [self onSlider5Action:sliderGeq5];
        } else if (CGRectContainsPoint(sliderGeq6.frame, touchPoint)) {
            sliderGeq6.value = [self pointToSliderValue:touchPoint];
            [self onSlider6Action:sliderGeq6];
        } else if (CGRectContainsPoint(sliderGeq7.frame, touchPoint)) {
            sliderGeq7.value = [self pointToSliderValue:touchPoint];
            [self onSlider7Action:sliderGeq7];
        } else if (CGRectContainsPoint(sliderGeq8.frame, touchPoint)) {
            sliderGeq8.value = [self pointToSliderValue:touchPoint];
            [self onSlider8Action:sliderGeq8];
        } else if (CGRectContainsPoint(sliderGeq9.frame, touchPoint)) {
            sliderGeq9.value = [self pointToSliderValue:touchPoint];
            [self onSlider9Action:sliderGeq9];
        } else if (CGRectContainsPoint(sliderGeq10.frame, touchPoint)) {
            sliderGeq10.value = [self pointToSliderValue:touchPoint];
            [self onSlider10Action:sliderGeq10];
        } else if (CGRectContainsPoint(sliderGeq11.frame, touchPoint)) {
            sliderGeq11.value = [self pointToSliderValue:touchPoint];
            [self onSlider11Action:sliderGeq11];
        } else if (CGRectContainsPoint(sliderGeq12.frame, touchPoint)) {
            sliderGeq12.value = [self pointToSliderValue:touchPoint];
            [self onSlider12Action:sliderGeq12];
        } else if (CGRectContainsPoint(sliderGeq13.frame, touchPoint)) {
            sliderGeq13.value = [self pointToSliderValue:touchPoint];
            [self onSlider13Action:sliderGeq13];
        } else if (CGRectContainsPoint(sliderGeq14.frame, touchPoint)) {
            sliderGeq14.value = [self pointToSliderValue:touchPoint];
            [self onSlider14Action:sliderGeq14];
        } else if (CGRectContainsPoint(sliderGeq15.frame, touchPoint)) {
            sliderGeq15.value = [self pointToSliderValue:touchPoint];
            [self onSlider15Action:sliderGeq15];
        } else if (CGRectContainsPoint(sliderGeq16.frame, touchPoint)) {
            sliderGeq16.value = [self pointToSliderValue:touchPoint];
            [self onSlider16Action:sliderGeq16];
        } else if (CGRectContainsPoint(sliderGeq17.frame, touchPoint)) {
            sliderGeq17.value = [self pointToSliderValue:touchPoint];
            [self onSlider17Action:sliderGeq17];
        } else if (CGRectContainsPoint(sliderGeq18.frame, touchPoint)) {
            sliderGeq18.value = [self pointToSliderValue:touchPoint];
            [self onSlider18Action:sliderGeq18];
        } else if (CGRectContainsPoint(sliderGeq19.frame, touchPoint)) {
            sliderGeq19.value = [self pointToSliderValue:touchPoint];
            [self onSlider19Action:sliderGeq19];
        } else if (CGRectContainsPoint(sliderGeq20.frame, touchPoint)) {
            sliderGeq20.value = [self pointToSliderValue:touchPoint];
            [self onSlider20Action:sliderGeq20];
        } else if (CGRectContainsPoint(sliderGeq21.frame, touchPoint)) {
            sliderGeq21.value = [self pointToSliderValue:touchPoint];
            [self onSlider21Action:sliderGeq21];
        } else if (CGRectContainsPoint(sliderGeq22.frame, touchPoint)) {
            sliderGeq22.value = [self pointToSliderValue:touchPoint];
            [self onSlider22Action:sliderGeq22];
        } else if (CGRectContainsPoint(sliderGeq23.frame, touchPoint)) {
            sliderGeq23.value = [self pointToSliderValue:touchPoint];
            [self onSlider23Action:sliderGeq23];
        } else if (CGRectContainsPoint(sliderGeq24.frame, touchPoint)) {
            sliderGeq24.value = [self pointToSliderValue:touchPoint];
            [self onSlider24Action:sliderGeq24];
        } else if (CGRectContainsPoint(sliderGeq25.frame, touchPoint)) {
            sliderGeq25.value = [self pointToSliderValue:touchPoint];
            [self onSlider25Action:sliderGeq25];
        } else if (CGRectContainsPoint(sliderGeq26.frame, touchPoint)) {
            sliderGeq26.value = [self pointToSliderValue:touchPoint];
            [self onSlider26Action:sliderGeq26];
        } else if (CGRectContainsPoint(sliderGeq27.frame, touchPoint)) {
            sliderGeq27.value = [self pointToSliderValue:touchPoint];
            [self onSlider27Action:sliderGeq27];
        } else if (CGRectContainsPoint(sliderGeq28.frame, touchPoint)) {
            sliderGeq28.value = [self pointToSliderValue:touchPoint];
            [self onSlider28Action:sliderGeq28];
        } else if (CGRectContainsPoint(sliderGeq29.frame, touchPoint)) {
            sliderGeq29.value = [self pointToSliderValue:touchPoint];
            [self onSlider29Action:sliderGeq29];
        } else if (CGRectContainsPoint(sliderGeq30.frame, touchPoint)) {
            sliderGeq30.value = [self pointToSliderValue:touchPoint];
            [self onSlider30Action:sliderGeq30];
        } else if (CGRectContainsPoint(sliderGeq31.frame, touchPoint)) {
            sliderGeq31.value = [self pointToSliderValue:touchPoint];
            [self onSlider31Action:sliderGeq31];
        }
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    geqLock = NO;
}

- (IBAction)onBtnGeqOnOff:(id) sender {
    btnGeqOnOff.selected = !btnGeqOnOff.selected;
    peqEnabled[currentChannel] = btnGeqOnOff.selected;
    [self updateGeqMeters];
    
    switch (currentChannel) {
        case CHANNEL_MAIN:
            [Global setClearBit:&_mainCtrl bitValue:peqEnabled[currentChannel] bitOrder:7];
            [viewController sendData:CMD_MAIN_LR_CTRL commandType:DSP_WRITE dspId:DSP_6 value:_mainCtrl];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnDraw:(id)sender {
    btnGeqDraw.selected = !btnGeqDraw.selected;
    if (btnGeqDraw.selected) {
        [self.viewGeq bringSubviewToFront:self.viewGeqDraw];
    } else {
        [self.viewGeq sendSubviewToBack:self.viewGeqDraw];
    }
}

- (void)updateGeqMeters {
    // Update meterL images
    UIImage *progressImage;
    if (peqEnabled[MAIN]) {
        progressImage = [[UIImage imageNamed:@"bar-2.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0,0,0,0)];
    } else {
        progressImage = [[UIImage imageNamed:@"bar-4.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0,0,0,0)];
    }
    
    [meter1Top setProgressImage:progressImage];
    [meter1Btm setProgressImage:progressImage];
    [meter2Top setProgressImage:progressImage];
    [meter2Btm setProgressImage:progressImage];
    [meter3Top setProgressImage:progressImage];
    [meter3Btm setProgressImage:progressImage];
    [meter4Top setProgressImage:progressImage];
    [meter4Btm setProgressImage:progressImage];
    [meter5Top setProgressImage:progressImage];
    [meter5Btm setProgressImage:progressImage];
    [meter6Top setProgressImage:progressImage];
    [meter6Btm setProgressImage:progressImage];
    [meter7Top setProgressImage:progressImage];
    [meter7Btm setProgressImage:progressImage];
    [meter8Top setProgressImage:progressImage];
    [meter8Btm setProgressImage:progressImage];
    [meter9Top setProgressImage:progressImage];
    [meter9Btm setProgressImage:progressImage];
    [meter10Top setProgressImage:progressImage];
    [meter10Btm setProgressImage:progressImage];
    [meter11Top setProgressImage:progressImage];
    [meter11Btm setProgressImage:progressImage];
    [meter12Top setProgressImage:progressImage];
    [meter12Btm setProgressImage:progressImage];
    [meter13Top setProgressImage:progressImage];
    [meter13Btm setProgressImage:progressImage];
    [meter14Top setProgressImage:progressImage];
    [meter14Btm setProgressImage:progressImage];
    [meter15Top setProgressImage:progressImage];
    [meter15Btm setProgressImage:progressImage];
    [meter16Top setProgressImage:progressImage];
    [meter16Btm setProgressImage:progressImage];
    [meter17Top setProgressImage:progressImage];
    [meter17Btm setProgressImage:progressImage];
    [meter18Top setProgressImage:progressImage];
    [meter18Btm setProgressImage:progressImage];
    [meter19Top setProgressImage:progressImage];
    [meter19Btm setProgressImage:progressImage];
    [meter20Top setProgressImage:progressImage];
    [meter20Btm setProgressImage:progressImage];
    [meter21Top setProgressImage:progressImage];
    [meter21Btm setProgressImage:progressImage];
    [meter22Top setProgressImage:progressImage];
    [meter22Btm setProgressImage:progressImage];
    [meter23Top setProgressImage:progressImage];
    [meter23Btm setProgressImage:progressImage];
    [meter24Top setProgressImage:progressImage];
    [meter24Btm setProgressImage:progressImage];
    [meter25Top setProgressImage:progressImage];
    [meter25Btm setProgressImage:progressImage];
    [meter26Top setProgressImage:progressImage];
    [meter26Btm setProgressImage:progressImage];
    [meter27Top setProgressImage:progressImage];
    [meter27Btm setProgressImage:progressImage];
    [meter28Top setProgressImage:progressImage];
    [meter28Btm setProgressImage:progressImage];
    [meter29Top setProgressImage:progressImage];
    [meter29Btm setProgressImage:progressImage];
    [meter30Top setProgressImage:progressImage];
    [meter30Btm setProgressImage:progressImage];
    [meter31Top setProgressImage:progressImage];
    [meter31Btm setProgressImage:progressImage];
}

#pragma mark -
#pragma mark Slider event handler methods

- (IBAction)onSlider1Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider1End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider1Action:(id) sender {
    if (sliderGeq1.value > 0.0) {
        meter1Top.progress = sliderGeq1.value/GEQ_SLIDER_MAX_VALUE;
        meter1Btm.progress = 0.0;
    } else if (sliderGeq1.value < 0.0) {
        meter1Btm.progress = sliderGeq1.value/GEQ_SLIDER_MIN_VALUE;
        meter1Top.progress = 0.0;
    } else {
        meter1Top.progress = 0.0;
        meter1Btm.progress = 0.0;
    }
    [self updatePreviewImage];
    [viewController sendData:CMD_MAIN_GEQ_20HZ_DB commandType:DSP_WRITE dspId:DSP_6 value:(int)(sliderGeq1.value)];
}

- (IBAction)onSlider2Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider2End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider2Action:(id) sender {
    if (sliderGeq2.value > 0.0) {
        meter2Top.progress = sliderGeq2.value/GEQ_SLIDER_MAX_VALUE;
        meter2Btm.progress = 0.0;
    } else if (sliderGeq2.value < 0.0) {
        meter2Btm.progress = sliderGeq2.value/GEQ_SLIDER_MIN_VALUE;
        meter2Top.progress = 0.0;
    } else {
        meter2Top.progress = 0.0;
        meter2Btm.progress = 0.0;
    }
    [self updatePreviewImage];
    [viewController sendData:CMD_MAIN_GEQ_25HZ_DB commandType:DSP_WRITE dspId:DSP_6 value:(int)(sliderGeq2.value)];
}

- (IBAction)onSlider3Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider3End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider3Action:(id) sender {
    if (sliderGeq3.value > 0.0) {
        meter3Top.progress = sliderGeq3.value/GEQ_SLIDER_MAX_VALUE;
        meter3Btm.progress = 0.0;
    } else if (sliderGeq3.value < 0.0) {
        meter3Btm.progress = sliderGeq3.value/GEQ_SLIDER_MIN_VALUE;
        meter3Top.progress = 0.0;
    } else {
        meter3Top.progress = 0.0;
        meter3Btm.progress = 0.0;
    }
    [self updatePreviewImage];
    [viewController sendData:CMD_MAIN_GEQ_31_5HZ_DB commandType:DSP_WRITE dspId:DSP_6 value:(int)(sliderGeq3.value)];
}

- (IBAction)onSlider4Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider4End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider4Action:(id) sender {
    if (sliderGeq4.value > 0.0) {
        meter4Top.progress = sliderGeq4.value/GEQ_SLIDER_MAX_VALUE;
        meter4Btm.progress = 0.0;
    } else if (sliderGeq4.value < 0.0) {
        meter4Btm.progress = sliderGeq4.value/GEQ_SLIDER_MIN_VALUE;
        meter4Top.progress = 0.0;
    } else {
        meter4Top.progress = 0.0;
        meter4Btm.progress = 0.0;
    }
    [self updatePreviewImage];
    [viewController sendData:CMD_MAIN_GEQ_40HZ_DB commandType:DSP_WRITE dspId:DSP_6 value:(int)(sliderGeq4.value)];
}

- (IBAction)onSlider5Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider5End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider5Action:(id) sender {
    if (sliderGeq5.value > 0.0) {
        meter5Top.progress = sliderGeq5.value/GEQ_SLIDER_MAX_VALUE;
        meter5Btm.progress = 0.0;
    } else if (sliderGeq5.value < 0.0) {
        meter5Btm.progress = sliderGeq5.value/GEQ_SLIDER_MIN_VALUE;
        meter5Top.progress = 0.0;
    } else {
        meter5Top.progress = 0.0;
        meter5Btm.progress = 0.0;
    }
    [self updatePreviewImage];
    [viewController sendData:CMD_MAIN_GEQ_50HZ_DB commandType:DSP_WRITE dspId:DSP_6 value:(int)(sliderGeq5.value)];
}

- (IBAction)onSlider6Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider6End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider6Action:(id) sender {
    if (sliderGeq6.value > 0.0) {
        meter6Top.progress = sliderGeq6.value/GEQ_SLIDER_MAX_VALUE;
        meter6Btm.progress = 0.0;
    } else if (sliderGeq6.value < 0.0) {
        meter6Btm.progress = sliderGeq6.value/GEQ_SLIDER_MIN_VALUE;
        meter6Top.progress = 0.0;
    } else {
        meter6Top.progress = 0.0;
        meter6Btm.progress = 0.0;
    }
    [self updatePreviewImage];
    [viewController sendData:CMD_MAIN_GEQ_63HZ_DB commandType:DSP_WRITE dspId:DSP_6 value:(int)(sliderGeq6.value)];
}

- (IBAction)onSlider7Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider7End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider7Action:(id) sender {
    if (sliderGeq7.value > 0.0) {
        meter7Top.progress = sliderGeq7.value/GEQ_SLIDER_MAX_VALUE;
        meter7Btm.progress = 0.0;
    } else if (sliderGeq7.value < 0.0) {
        meter7Btm.progress = sliderGeq7.value/GEQ_SLIDER_MIN_VALUE;
        meter7Top.progress = 0.0;
    } else {
        meter7Top.progress = 0.0;
        meter7Btm.progress = 0.0;
    }
    [self updatePreviewImage];
    [viewController sendData:CMD_MAIN_GEQ_80HZ_DB commandType:DSP_WRITE dspId:DSP_6 value:(int)(sliderGeq7.value)];
}

- (IBAction)onSlider8Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider8End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider8Action:(id) sender {
    if (sliderGeq8.value > 0.0) {
        meter8Top.progress = sliderGeq8.value/GEQ_SLIDER_MAX_VALUE;
        meter8Btm.progress = 0.0;
    } else if (sliderGeq8.value < 0.0) {
        meter8Btm.progress = sliderGeq8.value/GEQ_SLIDER_MIN_VALUE;
        meter8Top.progress = 0.0;
    } else {
        meter8Top.progress = 0.0;
        meter8Btm.progress = 0.0;
    }
    [self updatePreviewImage];
    [viewController sendData:CMD_MAIN_GEQ_100HZ_DB commandType:DSP_WRITE dspId:DSP_6 value:(int)(sliderGeq8.value)];
}

- (IBAction)onSlider9Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider9End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider9Action:(id) sender {
    if (sliderGeq9.value > 0.0) {
        meter9Top.progress = sliderGeq9.value/GEQ_SLIDER_MAX_VALUE;
        meter9Btm.progress = 0.0;
    } else if (sliderGeq9.value < 0.0) {
        meter9Btm.progress = sliderGeq9.value/GEQ_SLIDER_MIN_VALUE;
        meter9Top.progress = 0.0;
    } else {
        meter9Top.progress = 0.0;
        meter9Btm.progress = 0.0;
    }
    [self updatePreviewImage];
    [viewController sendData:CMD_MAIN_GEQ_125HZ_DB commandType:DSP_WRITE dspId:DSP_6 value:(int)(sliderGeq9.value)];
}

- (IBAction)onSlider10Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider10End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider10Action:(id) sender {
    if (sliderGeq10.value > 0.0) {
        meter10Top.progress = sliderGeq10.value/GEQ_SLIDER_MAX_VALUE;
        meter10Btm.progress = 0.0;
    } else if (sliderGeq10.value < 0.0) {
        meter10Btm.progress = sliderGeq10.value/GEQ_SLIDER_MIN_VALUE;
        meter10Top.progress = 0.0;
    } else {
        meter10Top.progress = 0.0;
        meter10Btm.progress = 0.0;
    }
    [self updatePreviewImage];
    [viewController sendData:CMD_MAIN_GEQ_160HZ_DB commandType:DSP_WRITE dspId:DSP_6 value:(int)(sliderGeq10.value)];
}

- (IBAction)onSlider11Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider11End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider11Action:(id) sender {
    if (sliderGeq11.value > 0.0) {
        meter11Top.progress = sliderGeq11.value/GEQ_SLIDER_MAX_VALUE;
        meter11Btm.progress = 0.0;
    } else if (sliderGeq11.value < 0.0) {
        meter11Btm.progress = sliderGeq11.value/GEQ_SLIDER_MIN_VALUE;
        meter11Top.progress = 0.0;
    } else {
        meter11Top.progress = 0.0;
        meter11Btm.progress = 0.0;
    }
    [self updatePreviewImage];
    [viewController sendData:CMD_MAIN_GEQ_200HZ_DB commandType:DSP_WRITE dspId:DSP_6 value:(int)(sliderGeq11.value)];
}

- (IBAction)onSlider12Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider12End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider12Action:(id) sender {
    if (sliderGeq12.value > 0.0) {
        meter12Top.progress = sliderGeq12.value/GEQ_SLIDER_MAX_VALUE;
        meter12Btm.progress = 0.0;
    } else if (sliderGeq12.value < 0.0) {
        meter12Btm.progress = sliderGeq12.value/GEQ_SLIDER_MIN_VALUE;
        meter12Top.progress = 0.0;
    } else {
        meter12Top.progress = 0.0;
        meter12Btm.progress = 0.0;
    }
    [self updatePreviewImage];
    [viewController sendData:CMD_MAIN_GEQ_250HZ_DB commandType:DSP_WRITE dspId:DSP_6 value:(int)(sliderGeq12.value)];
}

- (IBAction)onSlider13Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider13End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider13Action:(id) sender {
    if (sliderGeq13.value > 0.0) {
        meter13Top.progress = sliderGeq13.value/GEQ_SLIDER_MAX_VALUE;
        meter13Btm.progress = 0.0;
    } else if (sliderGeq13.value < 0.0) {
        meter13Btm.progress = sliderGeq13.value/GEQ_SLIDER_MIN_VALUE;
        meter13Top.progress = 0.0;
    } else {
        meter13Top.progress = 0.0;
        meter13Btm.progress = 0.0;
    }
    [self updatePreviewImage];
    [viewController sendData:CMD_MAIN_GEQ_315HZ_DB commandType:DSP_WRITE dspId:DSP_6 value:(int)(sliderGeq13.value)];
}

- (IBAction)onSlider14Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider14End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider14Action:(id) sender {
    if (sliderGeq14.value > 0.0) {
        meter14Top.progress = sliderGeq14.value/GEQ_SLIDER_MAX_VALUE;
        meter14Btm.progress = 0.0;
    } else if (sliderGeq14.value < 0.0) {
        meter14Btm.progress = sliderGeq14.value/GEQ_SLIDER_MIN_VALUE;
        meter14Top.progress = 0.0;
    } else {
        meter14Top.progress = 0.0;
        meter14Btm.progress = 0.0;
    }
    [self updatePreviewImage];
    [viewController sendData:CMD_MAIN_GEQ_400HZ_DB commandType:DSP_WRITE dspId:DSP_6 value:(int)(sliderGeq14.value)];
}

- (IBAction)onSlider15Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider15End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider15Action:(id) sender {
    if (sliderGeq15.value > 0.0) {
        meter15Top.progress = sliderGeq15.value/GEQ_SLIDER_MAX_VALUE;
        meter15Btm.progress = 0.0;
    } else if (sliderGeq15.value < 0.0) {
        meter15Btm.progress = sliderGeq15.value/GEQ_SLIDER_MIN_VALUE;
        meter15Top.progress = 0.0;
    } else {
        meter15Top.progress = 0.0;
        meter15Btm.progress = 0.0;
    }
    [self updatePreviewImage];
    [viewController sendData:CMD_MAIN_GEQ_500HZ_DB commandType:DSP_WRITE dspId:DSP_6 value:(int)(sliderGeq15.value)];
}

- (IBAction)onSlider16Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider16End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider16Action:(id) sender {
    if (sliderGeq16.value > 0.0) {
        meter16Top.progress = sliderGeq16.value/GEQ_SLIDER_MAX_VALUE;
        meter16Btm.progress = 0.0;
    } else if (sliderGeq16.value < 0.0) {
        meter16Btm.progress = sliderGeq16.value/GEQ_SLIDER_MIN_VALUE;
        meter16Top.progress = 0.0;
    } else {
        meter16Top.progress = 0.0;
        meter16Btm.progress = 0.0;
    }
    [self updatePreviewImage];
    [viewController sendData:CMD_MAIN_GEQ_630HZ_DB commandType:DSP_WRITE dspId:DSP_6 value:(int)(sliderGeq16.value)];
}

- (IBAction)onSlider17Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider17End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider17Action:(id) sender {
    if (sliderGeq17.value > 0.0) {
        meter17Top.progress = sliderGeq17.value/GEQ_SLIDER_MAX_VALUE;
        meter17Btm.progress = 0.0;
    } else if (sliderGeq17.value < 0.0) {
        meter17Btm.progress = sliderGeq17.value/GEQ_SLIDER_MIN_VALUE;
        meter17Top.progress = 0.0;
    } else {
        meter17Top.progress = 0.0;
        meter17Btm.progress = 0.0;
    }
    [self updatePreviewImage];
    [viewController sendData:CMD_MAIN_GEQ_800HZ_DB commandType:DSP_WRITE dspId:DSP_6 value:(int)(sliderGeq17.value)];
}

- (IBAction)onSlider18Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider18End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider18Action:(id) sender {
    if (sliderGeq18.value > 0.0) {
        meter18Top.progress = sliderGeq18.value/GEQ_SLIDER_MAX_VALUE;
        meter18Btm.progress = 0.0;
    } else if (sliderGeq18.value < 0.0) {
        meter18Btm.progress = sliderGeq18.value/GEQ_SLIDER_MIN_VALUE;
        meter18Top.progress = 0.0;
    } else {
        meter18Top.progress = 0.0;
        meter18Btm.progress = 0.0;
    }
    [self updatePreviewImage];
    [viewController sendData:CMD_MAIN_GEQ_1KHZ_DB commandType:DSP_WRITE dspId:DSP_6 value:(int)(sliderGeq18.value)];
}

- (IBAction)onSlider19Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider19End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider19Action:(id) sender {
    if (sliderGeq19.value > 0.0) {
        meter19Top.progress = sliderGeq19.value/GEQ_SLIDER_MAX_VALUE;
        meter19Btm.progress = 0.0;
    } else if (sliderGeq19.value < 0.0) {
        meter19Btm.progress = sliderGeq19.value/GEQ_SLIDER_MIN_VALUE;
        meter19Top.progress = 0.0;
    } else {
        meter19Top.progress = 0.0;
        meter19Btm.progress = 0.0;
    }
    [self updatePreviewImage];
    [viewController sendData:CMD_MAIN_GEQ_1_25KHZ_DB commandType:DSP_WRITE dspId:DSP_6 value:(int)(sliderGeq19.value)];
}

- (IBAction)onSlider20Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider20End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider20Action:(id) sender {
    if (sliderGeq20.value > 0.0) {
        meter20Top.progress = sliderGeq20.value/GEQ_SLIDER_MAX_VALUE;
        meter20Btm.progress = 0.0;
    } else if (sliderGeq20.value < 0.0) {
        meter20Btm.progress = sliderGeq20.value/GEQ_SLIDER_MIN_VALUE;
        meter20Top.progress = 0.0;
    } else {
        meter20Top.progress = 0.0;
        meter20Btm.progress = 0.0;
    }
    [self updatePreviewImage];
    [viewController sendData:CMD_MAIN_GEQ_1_6KHZ_DB commandType:DSP_WRITE dspId:DSP_6 value:(int)(sliderGeq20.value)];
}

- (IBAction)onSlider21Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider21End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider21Action:(id) sender {
    if (sliderGeq21.value > 0.0) {
        meter21Top.progress = sliderGeq21.value/GEQ_SLIDER_MAX_VALUE;
        meter21Btm.progress = 0.0;
    } else if (sliderGeq21.value < 0.0) {
        meter21Btm.progress = sliderGeq21.value/GEQ_SLIDER_MIN_VALUE;
        meter21Top.progress = 0.0;
    } else {
        meter21Top.progress = 0.0;
        meter21Btm.progress = 0.0;
    }
    [self updatePreviewImage];
    [viewController sendData:CMD_MAIN_GEQ_2KHZ_DB commandType:DSP_WRITE dspId:DSP_6 value:(int)(sliderGeq21.value)];
}

- (IBAction)onSlider22Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider22End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider22Action:(id) sender {
    if (sliderGeq22.value > 0.0) {
        meter22Top.progress = sliderGeq22.value/GEQ_SLIDER_MAX_VALUE;
        meter22Btm.progress = 0.0;
    } else if (sliderGeq22.value < 0.0) {
        meter22Btm.progress = sliderGeq22.value/GEQ_SLIDER_MIN_VALUE;
        meter22Top.progress = 0.0;
    } else {
        meter22Top.progress = 0.0;
        meter22Btm.progress = 0.0;
    }
    [self updatePreviewImage];
    [viewController sendData:CMD_MAIN_GEQ_2_5KHZ_DB commandType:DSP_WRITE dspId:DSP_6 value:(int)(sliderGeq22.value)];
}

- (IBAction)onSlider23Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider23End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider23Action:(id) sender {
    if (sliderGeq23.value > 0.0) {
        meter23Top.progress = sliderGeq23.value/GEQ_SLIDER_MAX_VALUE;
        meter23Btm.progress = 0.0;
    } else if (sliderGeq23.value < 0.0) {
        meter23Btm.progress = sliderGeq23.value/GEQ_SLIDER_MIN_VALUE;
        meter23Top.progress = 0.0;
    } else {
        meter23Top.progress = 0.0;
        meter23Btm.progress = 0.0;
    }
    [self updatePreviewImage];
    [viewController sendData:CMD_MAIN_GEQ_3_15KHZ_DB commandType:DSP_WRITE dspId:DSP_6 value:(int)(sliderGeq23.value)];
}

- (IBAction)onSlider24Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider24End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider24Action:(id) sender {
    if (sliderGeq24.value > 0.0) {
        meter24Top.progress = sliderGeq24.value/GEQ_SLIDER_MAX_VALUE;
        meter24Btm.progress = 0.0;
    } else if (sliderGeq24.value < 0.0) {
        meter24Btm.progress = sliderGeq24.value/GEQ_SLIDER_MIN_VALUE;
        meter24Top.progress = 0.0;
    } else {
        meter24Top.progress = 0.0;
        meter24Btm.progress = 0.0;
    }
    [self updatePreviewImage];
    [viewController sendData:CMD_MAIN_GEQ_4KHZ_DB commandType:DSP_WRITE dspId:DSP_6 value:(int)(sliderGeq24.value)];
}

- (IBAction)onSlider25Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider25End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider25Action:(id) sender {
    if (sliderGeq25.value > 0.0) {
        meter25Top.progress = sliderGeq25.value/GEQ_SLIDER_MAX_VALUE;
        meter25Btm.progress = 0.0;
    } else if (sliderGeq25.value < 0.0) {
        meter25Btm.progress = sliderGeq25.value/GEQ_SLIDER_MIN_VALUE;
        meter25Top.progress = 0.0;
    } else {
        meter25Top.progress = 0.0;
        meter25Btm.progress = 0.0;
    }
    [self updatePreviewImage];
    [viewController sendData:CMD_MAIN_GEQ_5KHZ_DB commandType:DSP_WRITE dspId:DSP_6 value:(int)(sliderGeq25.value)];
}

- (IBAction)onSlider26Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider26End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider26Action:(id) sender {
    if (sliderGeq26.value > 0.0) {
        meter26Top.progress = sliderGeq26.value/GEQ_SLIDER_MAX_VALUE;
        meter26Btm.progress = 0.0;
    } else if (sliderGeq26.value < 0.0) {
        meter26Btm.progress = sliderGeq26.value/GEQ_SLIDER_MIN_VALUE;
        meter26Top.progress = 0.0;
    } else {
        meter26Top.progress = 0.0;
        meter26Btm.progress = 0.0;
    }
    [self updatePreviewImage];
    [viewController sendData:CMD_MAIN_GEQ_6_3KHZ_DB commandType:DSP_WRITE dspId:DSP_6 value:(int)(sliderGeq26.value)];
}

- (IBAction)onSlider27Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider27End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider27Action:(id) sender {
    if (sliderGeq27.value > 0.0) {
        meter27Top.progress = sliderGeq27.value/GEQ_SLIDER_MAX_VALUE;
        meter27Btm.progress = 0.0;
    } else if (sliderGeq27.value < 0.0) {
        meter27Btm.progress = sliderGeq27.value/GEQ_SLIDER_MIN_VALUE;
        meter27Top.progress = 0.0;
    } else {
        meter27Top.progress = 0.0;
        meter27Btm.progress = 0.0;
    }
    [self updatePreviewImage];
    [viewController sendData:CMD_MAIN_GEQ_8KHZ_DB commandType:DSP_WRITE dspId:DSP_6 value:(int)(sliderGeq27.value)];
}

- (IBAction)onSlider28Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider28End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider28Action:(id) sender {
    if (sliderGeq28.value > 0.0) {
        meter28Top.progress = sliderGeq28.value/GEQ_SLIDER_MAX_VALUE;
        meter28Btm.progress = 0.0;
    } else if (sliderGeq28.value < 0.0) {
        meter28Btm.progress = sliderGeq28.value/GEQ_SLIDER_MIN_VALUE;
        meter28Top.progress = 0.0;
    } else {
        meter28Top.progress = 0.0;
        meter28Btm.progress = 0.0;
    }
    [self updatePreviewImage];
    [viewController sendData:CMD_MAIN_GEQ_10KHZ_DB commandType:DSP_WRITE dspId:DSP_6 value:(int)(sliderGeq28.value)];
}

- (IBAction)onSlider29Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider29End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider29Action:(id) sender {
    if (sliderGeq29.value > 0.0) {
        meter29Top.progress = sliderGeq29.value/GEQ_SLIDER_MAX_VALUE;
        meter29Btm.progress = 0.0;
    } else if (sliderGeq29.value < 0.0) {
        meter29Btm.progress = sliderGeq29.value/GEQ_SLIDER_MIN_VALUE;
        meter29Top.progress = 0.0;
    } else {
        meter29Top.progress = 0.0;
        meter29Btm.progress = 0.0;
    }
    [self updatePreviewImage];
    [viewController sendData:CMD_MAIN_GEQ_12_5KHZ_DB commandType:DSP_WRITE dspId:DSP_6 value:(int)(sliderGeq29.value)];
}

- (IBAction)onSlider30Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider30End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider30Action:(id) sender {
    if (sliderGeq30.value > 0.0) {
        meter30Top.progress = sliderGeq30.value/GEQ_SLIDER_MAX_VALUE;
        meter30Btm.progress = 0.0;
    } else if (sliderGeq30.value < 0.0) {
        meter30Btm.progress = sliderGeq30.value/GEQ_SLIDER_MIN_VALUE;
        meter30Top.progress = 0.0;
    } else {
        meter30Top.progress = 0.0;
        meter30Btm.progress = 0.0;
    }
    [self updatePreviewImage];
    [viewController sendData:CMD_MAIN_GEQ_16KHZ_DB commandType:DSP_WRITE dspId:DSP_6 value:(int)(sliderGeq30.value)];
}

- (IBAction)onSlider31Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider31End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider31Action:(id) sender {
    if (sliderGeq31.value > 0.0) {
        meter31Top.progress = sliderGeq31.value/GEQ_SLIDER_MAX_VALUE;
        meter31Btm.progress = 0.0;
    } else if (sliderGeq31.value < 0.0) {
        meter31Btm.progress = sliderGeq31.value/GEQ_SLIDER_MIN_VALUE;
        meter31Top.progress = 0.0;
    } else {
        meter31Top.progress = 0.0;
        meter31Btm.progress = 0.0;
    }
    [self updatePreviewImage];
    [viewController sendData:CMD_MAIN_GEQ_20KHZ_DB commandType:DSP_WRITE dspId:DSP_6 value:(int)(sliderGeq31.value)];
}

- (void)setSceneFilename:(NSString*)fileName {
    [btnFile setTitle:fileName forState:UIControlStateNormal];
}

@end
