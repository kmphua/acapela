//
//  AuxSendViewController.m
//  Acapela
//
//  Created by Kevin on 13/1/3.
//  Copyright (c) 2013年 Kevin Phua. All rights reserved.
//

#import "AuxSendViewController.h"
#import "AppDelegate.h"
#import "ViewController.h"
#import "Global.h"

@interface AuxSendViewController ()

@end

#define AUX_SEND_MAX_VALUE              107
#define AUX_SEND_MIN_VALUE              0

@implementation AuxSendViewController
{
    BOOL    sliderAuxSendMainLock;
    ViewController* viewController;
    int     ch17AudioSetting;
    int     ch18AudioSetting;
    int     soloCtrl;
    int     soloSafeCtrl;
    int     auxGpMaster;
    int     auxGpMeterMaster;
    int     viewMeterCtrl;
    int     multi1Source;
    int     multi2Source;
    int     multi3Source;
    int     multi4Source;
}

@synthesize currentAux, currentAuxLabel;
@synthesize btnAuxSendPrePostAll, viewAuxSend1, knobAuxSend1, cpvAuxSend1, btnAuxSendPrePost1, lblAuxSend1, lblAuxSendValue1, meterAuxSend1;
@synthesize viewAuxSend2, knobAuxSend2, cpvAuxSend2, btnAuxSendPrePost2, lblAuxSend2, lblAuxSendValue2, meterAuxSend2;
@synthesize viewAuxSend3, knobAuxSend3, cpvAuxSend3, btnAuxSendPrePost3, lblAuxSend3, lblAuxSendValue3, meterAuxSend3;
@synthesize viewAuxSend4, knobAuxSend4, cpvAuxSend4, btnAuxSendPrePost4, lblAuxSend4, lblAuxSendValue4, meterAuxSend4;
@synthesize viewAuxSend5, knobAuxSend5, cpvAuxSend5, btnAuxSendPrePost5, lblAuxSend5, lblAuxSendValue5, meterAuxSend5;
@synthesize viewAuxSend6, knobAuxSend6, cpvAuxSend6, btnAuxSendPrePost6, lblAuxSend6, lblAuxSendValue6, meterAuxSend6;
@synthesize viewAuxSend7, knobAuxSend7, cpvAuxSend7, btnAuxSendPrePost7, lblAuxSend7, lblAuxSendValue7, meterAuxSend7;
@synthesize viewAuxSend8, knobAuxSend8, cpvAuxSend8, btnAuxSendPrePost8, lblAuxSend8, lblAuxSendValue8, meterAuxSend8;
@synthesize viewAuxSend9, knobAuxSend9, cpvAuxSend9, btnAuxSendPrePost9, lblAuxSend9, lblAuxSendValue9, meterAuxSend9;
@synthesize viewAuxSend10, knobAuxSend10, cpvAuxSend10, btnAuxSendPrePost10, lblAuxSend10, lblAuxSendValue10, meterAuxSend10;
@synthesize viewAuxSend11, knobAuxSend11, cpvAuxSend11, btnAuxSendPrePost11, lblAuxSend11, lblAuxSendValue11, meterAuxSend11;
@synthesize viewAuxSend12, knobAuxSend12, cpvAuxSend12, btnAuxSendPrePost12, lblAuxSend12, lblAuxSendValue12, meterAuxSend12;
@synthesize viewAuxSend13, knobAuxSend13, cpvAuxSend13, btnAuxSendPrePost13, lblAuxSend13, lblAuxSendValue13, meterAuxSend13;
@synthesize viewAuxSend14, knobAuxSend14, cpvAuxSend14, btnAuxSendPrePost14, lblAuxSend14, lblAuxSendValue14, meterAuxSend14;
@synthesize viewAuxSend15, knobAuxSend15, cpvAuxSend15, btnAuxSendPrePost15, lblAuxSend15, lblAuxSendValue15, meterAuxSend15;
@synthesize viewAuxSend16, knobAuxSend16, cpvAuxSend16, btnAuxSendPrePost16, lblAuxSend16, lblAuxSendValue16, meterAuxSend16;
@synthesize viewAuxSend17, knobAuxSend17, cpvAuxSend17, btnAuxSendPrePost17, lblAuxSend17, lblAuxSendValue17, meterAuxSend17;
@synthesize viewAuxSend18, knobAuxSend18, cpvAuxSend18, btnAuxSendPrePost18, lblAuxSend18, lblAuxSendValue18, meterAuxSend18;
@synthesize btnAuxSendSolo, btnAuxSendOn, btnAuxSelectDown, btnAuxSelectUp, btnAuxMeter, txtCurrentAux, lblCurrentAux, sliderAuxSendMain, meterAuxSendMain, lblSliderAuxMainValue;
@synthesize btnAuxMulti1, btnAuxMulti2, btnAuxMulti3, btnAuxMulti4;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        currentAux = AUX_1;
        currentAuxLabel = currentAux;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    viewController = (ViewController *)appDelegate.window.rootViewController;
    
    // Do any additional setup after loading the view from its nib.
    [btnAuxMeter.titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
    [btnAuxMeter.titleLabel setNumberOfLines:0];
    [btnAuxMeter.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [btnAuxMeter setTitle:@"METER PRE" forState:UIControlStateNormal];
    [btnAuxMeter setBackgroundImage:[UIImage imageNamed:@"button-3.png"] forState:UIControlStateNormal];
    [btnAuxMeter setTitle:@"METER POST" forState:UIControlStateSelected];
    [btnAuxMeter setBackgroundImage:[UIImage imageNamed:@"button-5.png"] forState:UIControlStateSelected];
    [btnAuxSendPrePostAll.titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
    [btnAuxSendPrePostAll.titleLabel setNumberOfLines:0];
    [btnAuxSendPrePostAll.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [btnAuxSendPrePostAll setTitle:@"METER PRE" forState:UIControlStateNormal];
    [btnAuxSendPrePostAll setBackgroundImage:[UIImage imageNamed:@"button-3.png"] forState:UIControlStateNormal];
    [btnAuxSendPrePostAll setTitle:@"METER POST" forState:UIControlStateSelected];
    [btnAuxSendPrePostAll setBackgroundImage:[UIImage imageNamed:@"button-5.png"] forState:UIControlStateSelected];
    
    // Setup aux sends page
    knobAuxSend1.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobAuxSend1.scalingFactor = 2.0f;
	knobAuxSend1.maximumValue = AUX_SEND_MAX_VALUE;
	knobAuxSend1.minimumValue = AUX_SEND_MIN_VALUE;
	knobAuxSend1.value = AUX_SEND_MIN_VALUE;
	knobAuxSend1.defaultValue = knobAuxSend1.value;
	knobAuxSend1.resetsToDefault = YES;
	knobAuxSend1.backgroundColor = [UIColor clearColor];
	[knobAuxSend1 setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobAuxSend1.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobAuxSend1 addTarget:self action:@selector(knobAux1DidChange) forControlEvents:UIControlEventValueChanged];
    
    cpvAuxSend1 = [[DACircularProgressView alloc] initWithFrame:CGRectMake(14.0f, 40.0f, 65.0f, 65.0f)];
    [viewAuxSend1 addSubview:cpvAuxSend1];
    [viewAuxSend1 sendSubviewToBack:cpvAuxSend1];
    
    knobAuxSend2.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobAuxSend2.scalingFactor = 2.0f;
	knobAuxSend2.maximumValue = AUX_SEND_MAX_VALUE;
	knobAuxSend2.minimumValue = AUX_SEND_MIN_VALUE;
	knobAuxSend2.value = AUX_SEND_MIN_VALUE;
	knobAuxSend2.defaultValue = knobAuxSend2.value;
	knobAuxSend2.resetsToDefault = YES;
	knobAuxSend2.backgroundColor = [UIColor clearColor];
	[knobAuxSend2 setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobAuxSend2.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobAuxSend2 addTarget:self action:@selector(knobAux2DidChange) forControlEvents:UIControlEventValueChanged];
    
    cpvAuxSend2 = [[DACircularProgressView alloc] initWithFrame:CGRectMake(14.0f, 40.0f, 65.0f, 65.0f)];
    [viewAuxSend2 addSubview:cpvAuxSend2];
    [viewAuxSend2 sendSubviewToBack:cpvAuxSend2];
    
    knobAuxSend3.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobAuxSend3.scalingFactor = 2.0f;
	knobAuxSend3.maximumValue = AUX_SEND_MAX_VALUE;
	knobAuxSend3.minimumValue = AUX_SEND_MIN_VALUE;
	knobAuxSend3.value = AUX_SEND_MIN_VALUE;
	knobAuxSend3.defaultValue = knobAuxSend3.value;
	knobAuxSend3.resetsToDefault = YES;
	knobAuxSend3.backgroundColor = [UIColor clearColor];
	[knobAuxSend3 setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobAuxSend3.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobAuxSend3 addTarget:self action:@selector(knobAux3DidChange) forControlEvents:UIControlEventValueChanged];
    
    cpvAuxSend3 = [[DACircularProgressView alloc] initWithFrame:CGRectMake(14.0f, 40.0f, 65.0f, 65.0f)];
    [viewAuxSend3 addSubview:cpvAuxSend3];
    [viewAuxSend3 sendSubviewToBack:cpvAuxSend3];
    
    knobAuxSend4.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobAuxSend4.scalingFactor = 2.0f;
	knobAuxSend4.maximumValue = AUX_SEND_MAX_VALUE;
	knobAuxSend4.minimumValue = AUX_SEND_MIN_VALUE;
	knobAuxSend4.value = AUX_SEND_MIN_VALUE;
	knobAuxSend4.defaultValue = knobAuxSend4.value;
	knobAuxSend4.resetsToDefault = YES;
	knobAuxSend4.backgroundColor = [UIColor clearColor];
	[knobAuxSend4 setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobAuxSend4.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobAuxSend4 addTarget:self action:@selector(knobAux4DidChange) forControlEvents:UIControlEventValueChanged];
    
    cpvAuxSend4 = [[DACircularProgressView alloc] initWithFrame:CGRectMake(14.0f, 40.0f, 65.0f, 65.0f)];
    [viewAuxSend4 addSubview:cpvAuxSend4];
    [viewAuxSend4 sendSubviewToBack:cpvAuxSend4];
    
    knobAuxSend5.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobAuxSend5.scalingFactor = 2.0f;
	knobAuxSend5.maximumValue = AUX_SEND_MAX_VALUE;
	knobAuxSend5.minimumValue = AUX_SEND_MIN_VALUE;
	knobAuxSend5.value = AUX_SEND_MIN_VALUE;
	knobAuxSend5.defaultValue = knobAuxSend5.value;
	knobAuxSend5.resetsToDefault = YES;
	knobAuxSend5.backgroundColor = [UIColor clearColor];
	[knobAuxSend5 setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobAuxSend5.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobAuxSend5 addTarget:self action:@selector(knobAux5DidChange) forControlEvents:UIControlEventValueChanged];
    
    cpvAuxSend5 = [[DACircularProgressView alloc] initWithFrame:CGRectMake(14.0f, 40.0f, 65.0f, 65.0f)];
    [viewAuxSend5 addSubview:cpvAuxSend5];
    [viewAuxSend5 sendSubviewToBack:cpvAuxSend5];
    
    knobAuxSend6.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobAuxSend6.scalingFactor = 2.0f;
	knobAuxSend6.maximumValue = AUX_SEND_MAX_VALUE;
	knobAuxSend6.minimumValue = AUX_SEND_MIN_VALUE;
	knobAuxSend6.value = AUX_SEND_MIN_VALUE;
	knobAuxSend6.defaultValue = knobAuxSend6.value;
	knobAuxSend6.resetsToDefault = YES;
	knobAuxSend6.backgroundColor = [UIColor clearColor];
	[knobAuxSend6 setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobAuxSend6.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobAuxSend6 addTarget:self action:@selector(knobAux6DidChange) forControlEvents:UIControlEventValueChanged];
    
    cpvAuxSend6 = [[DACircularProgressView alloc] initWithFrame:CGRectMake(14.0f, 40.0f, 65.0f, 65.0f)];
    [viewAuxSend6 addSubview:cpvAuxSend6];
    [viewAuxSend6 sendSubviewToBack:cpvAuxSend6];
    
    knobAuxSend7.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobAuxSend7.scalingFactor = 2.0f;
	knobAuxSend7.maximumValue = AUX_SEND_MAX_VALUE;
	knobAuxSend7.minimumValue = AUX_SEND_MIN_VALUE;
	knobAuxSend7.value = AUX_SEND_MIN_VALUE;
	knobAuxSend7.defaultValue = knobAuxSend7.value;
	knobAuxSend7.resetsToDefault = YES;
	knobAuxSend7.backgroundColor = [UIColor clearColor];
	[knobAuxSend7 setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobAuxSend7.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobAuxSend7 addTarget:self action:@selector(knobAux7DidChange) forControlEvents:UIControlEventValueChanged];
    
    cpvAuxSend7 = [[DACircularProgressView alloc] initWithFrame:CGRectMake(14.0f, 40.0f, 65.0f, 65.0f)];
    [viewAuxSend7 addSubview:cpvAuxSend7];
    [viewAuxSend7 sendSubviewToBack:cpvAuxSend7];
    
    knobAuxSend8.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobAuxSend8.scalingFactor = 2.0f;
	knobAuxSend8.maximumValue = AUX_SEND_MAX_VALUE;
	knobAuxSend8.minimumValue = AUX_SEND_MIN_VALUE;
	knobAuxSend8.value = AUX_SEND_MIN_VALUE;
	knobAuxSend8.defaultValue = knobAuxSend8.value;
	knobAuxSend8.resetsToDefault = YES;
	knobAuxSend8.backgroundColor = [UIColor clearColor];
	[knobAuxSend8 setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobAuxSend8.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobAuxSend8 addTarget:self action:@selector(knobAux8DidChange) forControlEvents:UIControlEventValueChanged];
    
    cpvAuxSend8 = [[DACircularProgressView alloc] initWithFrame:CGRectMake(14.0f, 40.0f, 65.0f, 65.0f)];
    [viewAuxSend8 addSubview:cpvAuxSend8];
    [viewAuxSend8 sendSubviewToBack:cpvAuxSend8];
    
    knobAuxSend9.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobAuxSend9.scalingFactor = 2.0f;
	knobAuxSend9.maximumValue = AUX_SEND_MAX_VALUE;
	knobAuxSend9.minimumValue = AUX_SEND_MIN_VALUE;
	knobAuxSend9.value = AUX_SEND_MIN_VALUE;
	knobAuxSend9.defaultValue = knobAuxSend9.value;
	knobAuxSend9.resetsToDefault = YES;
	knobAuxSend9.backgroundColor = [UIColor clearColor];
	[knobAuxSend9 setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobAuxSend9.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobAuxSend9 addTarget:self action:@selector(knobAux9DidChange) forControlEvents:UIControlEventValueChanged];
    
    cpvAuxSend9 = [[DACircularProgressView alloc] initWithFrame:CGRectMake(14.0f, 40.0f, 65.0f, 65.0f)];
    [viewAuxSend9 addSubview:cpvAuxSend9];
    [viewAuxSend9 sendSubviewToBack:cpvAuxSend9];
    
    knobAuxSend10.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobAuxSend10.scalingFactor = 2.0f;
	knobAuxSend10.maximumValue = AUX_SEND_MAX_VALUE;
	knobAuxSend10.minimumValue = AUX_SEND_MIN_VALUE;
	knobAuxSend10.value = AUX_SEND_MIN_VALUE;
	knobAuxSend10.defaultValue = knobAuxSend10.value;
	knobAuxSend10.resetsToDefault = YES;
	knobAuxSend10.backgroundColor = [UIColor clearColor];
	[knobAuxSend10 setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobAuxSend10.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobAuxSend10 addTarget:self action:@selector(knobAux10DidChange) forControlEvents:UIControlEventValueChanged];
    
    cpvAuxSend10 = [[DACircularProgressView alloc] initWithFrame:CGRectMake(14.0f, 40.0f, 65.0f, 65.0f)];
    [viewAuxSend10 addSubview:cpvAuxSend10];
    [viewAuxSend10 sendSubviewToBack:cpvAuxSend10];
    
    knobAuxSend11.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobAuxSend11.scalingFactor = 2.0f;
	knobAuxSend11.maximumValue = AUX_SEND_MAX_VALUE;
	knobAuxSend11.minimumValue = AUX_SEND_MIN_VALUE;
	knobAuxSend11.value = AUX_SEND_MIN_VALUE;
	knobAuxSend11.defaultValue = knobAuxSend11.value;
	knobAuxSend11.resetsToDefault = YES;
	knobAuxSend11.backgroundColor = [UIColor clearColor];
	[knobAuxSend11 setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobAuxSend11.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobAuxSend11 addTarget:self action:@selector(knobAux11DidChange) forControlEvents:UIControlEventValueChanged];
    
    cpvAuxSend11 = [[DACircularProgressView alloc] initWithFrame:CGRectMake(14.0f, 40.0f, 65.0f, 65.0f)];
    [viewAuxSend11 addSubview:cpvAuxSend11];
    [viewAuxSend11 sendSubviewToBack:cpvAuxSend11];
    
    knobAuxSend12.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobAuxSend12.scalingFactor = 2.0f;
	knobAuxSend12.maximumValue = AUX_SEND_MAX_VALUE;
	knobAuxSend12.minimumValue = AUX_SEND_MIN_VALUE;
	knobAuxSend12.value = AUX_SEND_MIN_VALUE;
	knobAuxSend12.defaultValue = knobAuxSend12.value;
	knobAuxSend12.resetsToDefault = YES;
	knobAuxSend12.backgroundColor = [UIColor clearColor];
	[knobAuxSend12 setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobAuxSend12.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobAuxSend12 addTarget:self action:@selector(knobAux12DidChange) forControlEvents:UIControlEventValueChanged];
    
    cpvAuxSend12 = [[DACircularProgressView alloc] initWithFrame:CGRectMake(14.0f, 40.0f, 65.0f, 65.0f)];
    [viewAuxSend12 addSubview:cpvAuxSend12];
    [viewAuxSend12 sendSubviewToBack:cpvAuxSend12];
    
    knobAuxSend13.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobAuxSend13.scalingFactor = 2.0f;
	knobAuxSend13.maximumValue = AUX_SEND_MAX_VALUE;
	knobAuxSend13.minimumValue = AUX_SEND_MIN_VALUE;
	knobAuxSend13.value = AUX_SEND_MIN_VALUE;
	knobAuxSend13.defaultValue = knobAuxSend13.value;
	knobAuxSend13.resetsToDefault = YES;
	knobAuxSend13.backgroundColor = [UIColor clearColor];
	[knobAuxSend13 setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobAuxSend13.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobAuxSend13 addTarget:self action:@selector(knobAux13DidChange) forControlEvents:UIControlEventValueChanged];
    
    cpvAuxSend13 = [[DACircularProgressView alloc] initWithFrame:CGRectMake(14.0f, 40.0f, 65.0f, 65.0f)];
    [viewAuxSend13 addSubview:cpvAuxSend13];
    [viewAuxSend13 sendSubviewToBack:cpvAuxSend13];
    
    knobAuxSend14.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobAuxSend14.scalingFactor = 2.0f;
	knobAuxSend14.maximumValue = AUX_SEND_MAX_VALUE;
	knobAuxSend14.minimumValue = AUX_SEND_MIN_VALUE;
	knobAuxSend14.value = AUX_SEND_MIN_VALUE;
	knobAuxSend14.defaultValue = knobAuxSend14.value;
	knobAuxSend14.resetsToDefault = YES;
	knobAuxSend14.backgroundColor = [UIColor clearColor];
	[knobAuxSend14 setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobAuxSend14.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobAuxSend14 addTarget:self action:@selector(knobAux14DidChange) forControlEvents:UIControlEventValueChanged];
    
    cpvAuxSend14 = [[DACircularProgressView alloc] initWithFrame:CGRectMake(14.0f, 40.0f, 65.0f, 65.0f)];
    [viewAuxSend14 addSubview:cpvAuxSend14];
    [viewAuxSend14 sendSubviewToBack:cpvAuxSend14];
    
    knobAuxSend15.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobAuxSend15.scalingFactor = 2.0f;
	knobAuxSend15.maximumValue = AUX_SEND_MAX_VALUE;
	knobAuxSend15.minimumValue = AUX_SEND_MIN_VALUE;
	knobAuxSend15.value = AUX_SEND_MIN_VALUE;
	knobAuxSend15.defaultValue = knobAuxSend15.value;
	knobAuxSend15.resetsToDefault = YES;
	knobAuxSend15.backgroundColor = [UIColor clearColor];
	[knobAuxSend15 setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobAuxSend15.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobAuxSend15 addTarget:self action:@selector(knobAux15DidChange) forControlEvents:UIControlEventValueChanged];
    
    cpvAuxSend15 = [[DACircularProgressView alloc] initWithFrame:CGRectMake(14.0f, 40.0f, 65.0f, 65.0f)];
    [viewAuxSend15 addSubview:cpvAuxSend15];
    [viewAuxSend15 sendSubviewToBack:cpvAuxSend15];
    
    knobAuxSend16.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobAuxSend16.scalingFactor = 2.0f;
	knobAuxSend16.maximumValue = AUX_SEND_MAX_VALUE;
	knobAuxSend16.minimumValue = AUX_SEND_MIN_VALUE;
	knobAuxSend16.value = AUX_SEND_MIN_VALUE;
	knobAuxSend16.defaultValue = knobAuxSend16.value;
	knobAuxSend16.resetsToDefault = YES;
	knobAuxSend16.backgroundColor = [UIColor clearColor];
	[knobAuxSend16 setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobAuxSend16.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobAuxSend16 addTarget:self action:@selector(knobAux16DidChange) forControlEvents:UIControlEventValueChanged];
    
    cpvAuxSend16 = [[DACircularProgressView alloc] initWithFrame:CGRectMake(14.0f, 40.0f, 65.0f, 65.0f)];
    [viewAuxSend16 addSubview:cpvAuxSend16];
    [viewAuxSend16 sendSubviewToBack:cpvAuxSend16];

    knobAuxSend17.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobAuxSend17.scalingFactor = 2.0f;
	knobAuxSend17.maximumValue = AUX_SEND_MAX_VALUE;
	knobAuxSend17.minimumValue = AUX_SEND_MIN_VALUE;
	knobAuxSend17.value = AUX_SEND_MIN_VALUE;
	knobAuxSend17.defaultValue = knobAuxSend17.value;
	knobAuxSend17.resetsToDefault = YES;
	knobAuxSend17.backgroundColor = [UIColor clearColor];
	[knobAuxSend17 setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobAuxSend17.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobAuxSend17 addTarget:self action:@selector(knobAux17DidChange) forControlEvents:UIControlEventValueChanged];
    
    cpvAuxSend17 = [[DACircularProgressView alloc] initWithFrame:CGRectMake(14.0f, 40.0f, 65.0f, 65.0f)];
    [viewAuxSend17 addSubview:cpvAuxSend17];
    [viewAuxSend17 sendSubviewToBack:cpvAuxSend17];

    knobAuxSend18.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobAuxSend18.scalingFactor = 2.0f;
	knobAuxSend18.maximumValue = AUX_SEND_MAX_VALUE;
	knobAuxSend18.minimumValue = AUX_SEND_MIN_VALUE;
	knobAuxSend18.value = AUX_SEND_MIN_VALUE;
	knobAuxSend18.defaultValue = knobAuxSend18.value;
	knobAuxSend18.resetsToDefault = YES;
	knobAuxSend18.backgroundColor = [UIColor clearColor];
	[knobAuxSend18 setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobAuxSend18.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobAuxSend18 addTarget:self action:@selector(knobAux18DidChange) forControlEvents:UIControlEventValueChanged];
    
    cpvAuxSend18 = [[DACircularProgressView alloc] initWithFrame:CGRectMake(14.0f, 40.0f, 65.0f, 65.0f)];
    [viewAuxSend18 addSubview:cpvAuxSend18];
    [viewAuxSend18 sendSubviewToBack:cpvAuxSend18];
    
    // Add aux send meters
    UIImage *meterImage = [[UIImage imageNamed:@"meter-7.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0,0,0,0)];
    UIImage *progressImage = [[UIImage imageNamed:@"meter-1.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0,0,0,0)];
    
    meterAuxSend1 = [[JEProgressView alloc] initWithFrame:CGRectMake(-33, 91, 100, 8)];
    meterAuxSend1.transform = CGAffineTransformRotate(meterAuxSend1.transform, 270.0/180*M_PI);
    [meterAuxSend1 setProgressImage:meterImage];
    [meterAuxSend1 setTrackImage:[UIImage alloc]];
    [meterAuxSend1 setProgress:0.0];
    [self.view addSubview:meterAuxSend1];
    
    meterAuxSend2 = [[JEProgressView alloc] initWithFrame:CGRectMake(129, 91, 100, 8)];
    meterAuxSend2.transform = CGAffineTransformRotate(meterAuxSend2.transform, 270.0/180*M_PI);
    [meterAuxSend2 setProgressImage:meterImage];
    [meterAuxSend2 setTrackImage:[UIImage alloc]];
    [meterAuxSend2 setProgress:0.0];
    [self.view addSubview:meterAuxSend2];
    
    meterAuxSend3 = [[JEProgressView alloc] initWithFrame:CGRectMake(291, 91, 100, 8)];
    meterAuxSend3.transform = CGAffineTransformRotate(meterAuxSend3.transform, 270.0/180*M_PI);
    [meterAuxSend3 setProgressImage:meterImage];
    [meterAuxSend3 setTrackImage:[UIImage alloc]];
    [meterAuxSend3 setProgress:0.0];
    [self.view addSubview:meterAuxSend3];
    
    meterAuxSend4 = [[JEProgressView alloc] initWithFrame:CGRectMake(453, 91, 100, 8)];
    meterAuxSend4.transform = CGAffineTransformRotate(meterAuxSend4.transform, 270.0/180*M_PI);
    [meterAuxSend4 setProgressImage:meterImage];
    [meterAuxSend4 setTrackImage:[UIImage alloc]];
    [meterAuxSend4 setProgress:0.0];
    [self.view addSubview:meterAuxSend4];
    
    meterAuxSend5 = [[JEProgressView alloc] initWithFrame:CGRectMake(-33, 204, 100, 8)];
    meterAuxSend5.transform = CGAffineTransformRotate(meterAuxSend5.transform, 270.0/180*M_PI);
    [meterAuxSend5 setProgressImage:meterImage];
    [meterAuxSend5 setTrackImage:[UIImage alloc]];
    [meterAuxSend5 setProgress:0.0];
    [self.view addSubview:meterAuxSend5];
    
    meterAuxSend6 = [[JEProgressView alloc] initWithFrame:CGRectMake(129, 204, 100, 8)];
    meterAuxSend6.transform = CGAffineTransformRotate(meterAuxSend6.transform, 270.0/180*M_PI);
    [meterAuxSend6 setProgressImage:meterImage];
    [meterAuxSend6 setTrackImage:[UIImage alloc]];
    [meterAuxSend6 setProgress:0.0];
    [self.view addSubview:meterAuxSend6];
    
    meterAuxSend7 = [[JEProgressView alloc] initWithFrame:CGRectMake(291, 204, 100, 8)];
    meterAuxSend7.transform = CGAffineTransformRotate(meterAuxSend7.transform, 270.0/180*M_PI);
    [meterAuxSend7 setProgressImage:meterImage];
    [meterAuxSend7 setTrackImage:[UIImage alloc]];
    [meterAuxSend7 setProgress:0.0];
    [self.view addSubview:meterAuxSend7];
    
    meterAuxSend8 = [[JEProgressView alloc] initWithFrame:CGRectMake(453, 204, 100, 8)];
    meterAuxSend8.transform = CGAffineTransformRotate(meterAuxSend8.transform, 270.0/180*M_PI);
    [meterAuxSend8 setProgressImage:meterImage];
    [meterAuxSend8 setTrackImage:[UIImage alloc]];
    [meterAuxSend8 setProgress:0.0];
    [self.view addSubview:meterAuxSend8];
    
    meterAuxSend9 = [[JEProgressView alloc] initWithFrame:CGRectMake(-33, 317, 100, 8)];
    meterAuxSend9.transform = CGAffineTransformRotate(meterAuxSend9.transform, 270.0/180*M_PI);
    [meterAuxSend9 setProgressImage:meterImage];
    [meterAuxSend9 setTrackImage:[UIImage alloc]];
    [meterAuxSend9 setProgress:0.0];
    [self.view addSubview:meterAuxSend9];
    
    meterAuxSend10 = [[JEProgressView alloc] initWithFrame:CGRectMake(129, 317, 100, 8)];
    meterAuxSend10.transform = CGAffineTransformRotate(meterAuxSend10.transform, 270.0/180*M_PI);
    [meterAuxSend10 setProgressImage:meterImage];
    [meterAuxSend10 setTrackImage:[UIImage alloc]];
    [meterAuxSend10 setProgress:0.0];
    [self.view addSubview:meterAuxSend10];
    
    meterAuxSend11 = [[JEProgressView alloc] initWithFrame:CGRectMake(291, 317, 100, 8)];
    meterAuxSend11.transform = CGAffineTransformRotate(meterAuxSend11.transform, 270.0/180*M_PI);
    [meterAuxSend11 setProgressImage:meterImage];
    [meterAuxSend11 setTrackImage:[UIImage alloc]];
    [meterAuxSend11 setProgress:0.0];
    [self.view addSubview:meterAuxSend11];
    
    meterAuxSend12 = [[JEProgressView alloc] initWithFrame:CGRectMake(453, 317, 100, 8)];
    meterAuxSend12.transform = CGAffineTransformRotate(meterAuxSend12.transform, 270.0/180*M_PI);
    [meterAuxSend12 setProgressImage:meterImage];
    [meterAuxSend12 setTrackImage:[UIImage alloc]];
    [meterAuxSend12 setProgress:0.0];
    [self.view addSubview:meterAuxSend12];
    
    meterAuxSend13 = [[JEProgressView alloc] initWithFrame:CGRectMake(-33, 430, 100, 8)];
    meterAuxSend13.transform = CGAffineTransformRotate(meterAuxSend13.transform, 270.0/180*M_PI);
    [meterAuxSend13 setProgressImage:meterImage];
    [meterAuxSend13 setTrackImage:[UIImage alloc]];
    [meterAuxSend13 setProgress:0.0];
    [self.view addSubview:meterAuxSend13];
    
    meterAuxSend14 = [[JEProgressView alloc] initWithFrame:CGRectMake(129, 430, 100, 8)];
    meterAuxSend14.transform = CGAffineTransformRotate(meterAuxSend14.transform, 270.0/180*M_PI);
    [meterAuxSend14 setProgressImage:meterImage];
    [meterAuxSend14 setTrackImage:[UIImage alloc]];
    [meterAuxSend14 setProgress:0.0];
    [self.view addSubview:meterAuxSend14];
    
    meterAuxSend15 = [[JEProgressView alloc] initWithFrame:CGRectMake(291, 430, 100, 8)];
    meterAuxSend15.transform = CGAffineTransformRotate(meterAuxSend15.transform, 270.0/180*M_PI);
    [meterAuxSend15 setProgressImage:meterImage];
    [meterAuxSend15 setTrackImage:[UIImage alloc]];
    [meterAuxSend15 setProgress:0.0];
    [self.view addSubview:meterAuxSend15];
    
    meterAuxSend16 = [[JEProgressView alloc] initWithFrame:CGRectMake(453, 430, 100, 8)];
    meterAuxSend16.transform = CGAffineTransformRotate(meterAuxSend16.transform, 270.0/180*M_PI);
    [meterAuxSend16 setProgressImage:meterImage];
    [meterAuxSend16 setTrackImage:[UIImage alloc]];
    [meterAuxSend16 setProgress:0.0];
    [self.view addSubview:meterAuxSend16];
    
    meterAuxSend17 = [[JEProgressView alloc] initWithFrame:CGRectMake(-33, 543, 100, 8)];
    meterAuxSend17.transform = CGAffineTransformRotate(meterAuxSend17.transform, 270.0/180*M_PI);
    [meterAuxSend17 setProgressImage:meterImage];
    [meterAuxSend17 setTrackImage:[UIImage alloc]];
    [meterAuxSend17 setProgress:0.0];
    [self.view addSubview:meterAuxSend17];
    
    meterAuxSend18 = [[JEProgressView alloc] initWithFrame:CGRectMake(129, 543, 100, 8)];
    meterAuxSend18.transform = CGAffineTransformRotate(meterAuxSend18.transform, 270.0/180*M_PI);
    [meterAuxSend18 setProgressImage:meterImage];
    [meterAuxSend18 setTrackImage:[UIImage alloc]];
    [meterAuxSend18 setProgress:0.0];
    [self.view addSubview:meterAuxSend18];

    
    // Add aux send slider and meter
    UIImage *faderAuxImage = [UIImage imageNamed:@"fader-2.png"];
    
    sliderAuxSendMain = [[UISlider alloc] initWithFrame:CGRectMake(571, 295, 301, 180)];
    sliderAuxSendMain.transform = CGAffineTransformRotate(sliderAuxSendMain.transform, 270.0/180*M_PI);
    [sliderAuxSendMain addTarget:self action:@selector(onSliderAuxSendMainBegin:) forControlEvents:UIControlEventTouchDown];
    [sliderAuxSendMain addTarget:self action:@selector(onSliderAuxSendMainEnd:) forControlEvents:UIControlEventTouchUpInside];
    [sliderAuxSendMain addTarget:self action:@selector(onSliderAuxSendMainEnd:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderAuxSendMain addTarget:self action:@selector(onSliderAuxSendMainAction:) forControlEvents:UIControlEventValueChanged];
    [sliderAuxSendMain setBackgroundColor:[UIColor clearColor]];
    [sliderAuxSendMain setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderAuxSendMain setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderAuxSendMain setThumbImage:faderAuxImage forState:UIControlStateNormal];
    sliderAuxSendMain.minimumValue = 0.0;
    sliderAuxSendMain.maximumValue = FADER_MAX_VALUE;
    sliderAuxSendMain.continuous = YES;
    sliderAuxSendMain.value = 0.0;
    [self.view addSubview:sliderAuxSendMain];
    
    meterAuxSendMain = [[JEProgressView alloc] initWithFrame:CGRectMake(536, 372, 275, 4)];
    meterAuxSendMain.transform = CGAffineTransformRotate(meterAuxSendMain.transform, 270.0/180*M_PI);
    [meterAuxSendMain setProgressImage:progressImage];
    [meterAuxSendMain setTrackImage:[UIImage alloc]];
    [meterAuxSendMain setProgress:0.0];
    [self.view addSubview:meterAuxSendMain];
    
    [self updateCurrentAuxLabel];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)updateCurrentAuxLabel
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    switch (currentAux) {
        case AUX_1:
            if ([userDefaults objectForKey:@"Aux1"] != nil) {
                [txtCurrentAux setText:(NSString *)[userDefaults objectForKey:@"Aux1"]];
            } else {
                [txtCurrentAux setText:NSLocalizedStringFromTable(@"Aux1", @"Acapela", nil)];
            }
            [lblCurrentAux setText:NSLocalizedStringFromTable(@"Aux1", @"Acapela", nil)];
            break;
        case AUX_2:
            if ([userDefaults objectForKey:@"Aux2"] != nil) {
                [txtCurrentAux setText:(NSString *)[userDefaults objectForKey:@"Aux2"]];
            } else {
                [txtCurrentAux setText:NSLocalizedStringFromTable(@"Aux2", @"Acapela", nil)];
            }
            [lblCurrentAux setText:NSLocalizedStringFromTable(@"Aux2", @"Acapela", nil)];
            break;
        case AUX_3:
            if ([userDefaults objectForKey:@"Aux3"] != nil) {
                [txtCurrentAux setText:(NSString *)[userDefaults objectForKey:@"Aux3"]];
            } else {
                [txtCurrentAux setText:NSLocalizedStringFromTable(@"Aux3", @"Acapela", nil)];
            }
            [lblCurrentAux setText:NSLocalizedStringFromTable(@"Aux3", @"Acapela", nil)];
            break;
        case AUX_4:
            if ([userDefaults objectForKey:@"Aux4"] != nil) {
                [txtCurrentAux setText:(NSString *)[userDefaults objectForKey:@"Aux4"]];
            } else {
                [txtCurrentAux setText:NSLocalizedStringFromTable(@"Aux4", @"Acapela", nil)];
            }
            [lblCurrentAux setText:NSLocalizedStringFromTable(@"Aux4", @"Acapela", nil)];
            break;
        default:
            break;
    }
}

- (void)refreshAuxPrePostButtons1_8:(unsigned char)value
{
    btnAuxSendPrePost1.selected = value & 0x01;
    btnAuxSendPrePost2.selected = (value & 0x02) >> 1;
    btnAuxSendPrePost3.selected = (value & 0x04) >> 2;
    btnAuxSendPrePost4.selected = (value & 0x08) >> 3;
    btnAuxSendPrePost5.selected = (value & 0x10) >> 4;
    btnAuxSendPrePost6.selected = (value & 0x20) >> 5;
    btnAuxSendPrePost7.selected = (value & 0x40) >> 6;
    btnAuxSendPrePost8.selected = (value & 0x80) >> 7;
}

- (void)refreshAuxPrePostButtons9_16:(unsigned char)value
{
    btnAuxSendPrePost9.selected = value & 0x01;
    btnAuxSendPrePost10.selected = (value & 0x02) >> 1;
    btnAuxSendPrePost11.selected = (value & 0x04) >> 2;
    btnAuxSendPrePost12.selected = (value & 0x08) >> 3;
    btnAuxSendPrePost13.selected = (value & 0x10) >> 4;
    btnAuxSendPrePost14.selected = (value & 0x20) >> 5;
    btnAuxSendPrePost15.selected = (value & 0x40) >> 6;
    btnAuxSendPrePost16.selected = (value & 0x80) >> 7;
}

- (void)processReply:(AuxPagePacket *)pkt;
{
    soloCtrl = pkt->solo_ctrl;
    soloSafeCtrl = pkt->solo_safe_ctrl;;
    auxGpMaster = pkt->aux_gp_master;
    auxGpMeterMaster = pkt->aux_gp_meter_master;
    viewMeterCtrl = pkt->view_meter_control;
    multi1Source = pkt->multi_1_source;
    multi2Source = pkt->multi_2_source;
    multi3Source = pkt->multi_3_source;
    multi4Source = pkt->multi_4_source;
    ch17AudioSetting = pkt->ch_17_audio_setting;
    ch18AudioSetting = pkt->ch_18_audio_setting;
    
    switch (currentAux) {
        case AUX_1:
            meterAuxSend1.progress = ((float)pkt->view_aux_1_ch_1_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;            
            meterAuxSend2.progress = ((float)pkt->view_aux_1_ch_2_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend3.progress = ((float)pkt->view_aux_1_ch_3_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend4.progress = ((float)pkt->view_aux_1_ch_4_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend5.progress = ((float)pkt->view_aux_1_ch_5_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend6.progress = ((float)pkt->view_aux_1_ch_6_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend7.progress = ((float)pkt->view_aux_1_ch_7_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend8.progress = ((float)pkt->view_aux_1_ch_8_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend9.progress = ((float)pkt->view_aux_1_ch_9_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend10.progress = ((float)pkt->view_aux_1_ch_10_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend11.progress = ((float)pkt->view_aux_1_ch_11_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend12.progress = ((float)pkt->view_aux_1_ch_12_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend13.progress = ((float)pkt->view_aux_1_ch_13_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend14.progress = ((float)pkt->view_aux_1_ch_14_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend15.progress = ((float)pkt->view_aux_1_ch_15_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend16.progress = ((float)pkt->view_aux_1_ch_16_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend17.progress = ((float)pkt->view_aux_1_ch_17_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend18.progress = ((float)pkt->view_aux_1_ch_18_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            knobAuxSend1.value = pkt->aux_1_ch_1_level;
            knobAuxSend2.value = pkt->aux_1_ch_2_level;
            knobAuxSend3.value = pkt->aux_1_ch_3_level;
            knobAuxSend4.value = pkt->aux_1_ch_4_level;
            knobAuxSend5.value = pkt->aux_1_ch_5_level;
            knobAuxSend6.value = pkt->aux_1_ch_6_level;
            knobAuxSend7.value = pkt->aux_1_ch_7_level;
            knobAuxSend8.value = pkt->aux_1_ch_8_level;
            knobAuxSend9.value = pkt->aux_1_ch_9_level;
            knobAuxSend10.value = pkt->aux_1_ch_10_level;
            knobAuxSend11.value = pkt->aux_1_ch_11_level;
            knobAuxSend12.value = pkt->aux_1_ch_12_level;
            knobAuxSend13.value = pkt->aux_1_ch_13_level;
            knobAuxSend14.value = pkt->aux_1_ch_14_level;
            knobAuxSend15.value = pkt->aux_1_ch_15_level;
            knobAuxSend16.value = pkt->aux_1_ch_16_level;
            knobAuxSend17.value = pkt->aux_1_ch_17_level;
            knobAuxSend18.value = pkt->aux_1_ch_18_level;
            if (![viewController sendCommandExists:CMD_AUX1_SEND dspId:DSP_5]) {
                [self refreshAuxPrePostButtons1_8:(pkt->aux_1_send & 0x00FF)];
                [self refreshAuxPrePostButtons9_16:((pkt->aux_1_send & 0xFF00) >> 8)];
            }
            if (![viewController sendCommandExists:CMD_CH17_USB_AUDIO_SETTING dspId:DSP_5]) {
                btnAuxSendPrePost17.selected = (pkt->ch_17_audio_setting & 0x10) >> 4;
            }
            if (![viewController sendCommandExists:CMD_CH18_USB_AUDIO_SETTING dspId:DSP_5]) {
                btnAuxSendPrePost18.selected = (pkt->ch_18_audio_setting & 0x10) >> 4;
            }
            if (![viewController sendCommandExists:CMD_AUXGP_SOLO_CTRL dspId:DSP_5]) {
                btnAuxSendSolo.selected = (pkt->solo_ctrl & 0x0100) >> 8;
            }
            BOOL aux1SoloSafe = (pkt->solo_safe_ctrl & 0x0100) >> 8;
            if (aux1SoloSafe) {
                [btnAuxSendSolo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
                [btnAuxSendSolo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
            } else {
                [btnAuxSendSolo setTitle:@"SOLO" forState:UIControlStateNormal];
                [btnAuxSendSolo setTitle:@"SOLO" forState:UIControlStateSelected];
            }
            if (![viewController sendCommandExists:CMD_AUXGP_ON_OFF dspId:DSP_5]) {
                btnAuxSendOn.selected = (pkt->aux_gp_master & 0x0100) >> 8;
            }
            if (![viewController sendCommandExists:CMD_AUXGP_METER dspId:DSP_5]) {
                btnAuxMeter.selected = (pkt->aux_gp_meter_master & 0x0100) >> 8;
            }
            if (!sliderAuxSendMainLock) {
                sliderAuxSendMain.value = pkt->aux_1_master_fader;
                [self onSliderAuxSendMainAction:nil];
            }
            if (![viewController sendCommandExists:CMD_VIEW_METER_CONTROL dspId:DSP_5]) {
                btnAuxSendPrePostAll.selected = (pkt->view_meter_control & 0x08) >> 3;
            }
            if (![viewController sendCommandExists:CMD_MULTI1_SOURCE dspId:DSP_5]) {
                btnAuxMulti1.selected = (pkt->multi_1_source & 0x0100) >> 8;
            }
            if (![viewController sendCommandExists:CMD_MULTI2_SOURCE dspId:DSP_5]) {
                btnAuxMulti2.selected = (pkt->multi_2_source & 0x0100) >> 8;
            }
            if (![viewController sendCommandExists:CMD_MULTI3_SOURCE dspId:DSP_5]) {
                btnAuxMulti3.selected = (pkt->multi_3_source & 0x0100) >> 8;
            }
            if (![viewController sendCommandExists:CMD_MULTI4_SOURCE dspId:DSP_5]) {
                btnAuxMulti4.selected = (pkt->multi_4_source & 0x0100) >> 8;
            }
            break;
            
        case AUX_2:
            meterAuxSend1.progress = ((float)pkt->view_aux_2_ch_1_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend2.progress = ((float)pkt->view_aux_2_ch_2_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend3.progress = ((float)pkt->view_aux_2_ch_3_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend4.progress = ((float)pkt->view_aux_2_ch_4_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend5.progress = ((float)pkt->view_aux_2_ch_5_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend6.progress = ((float)pkt->view_aux_2_ch_6_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend7.progress = ((float)pkt->view_aux_2_ch_7_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend8.progress = ((float)pkt->view_aux_2_ch_8_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend9.progress = ((float)pkt->view_aux_2_ch_9_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend10.progress = ((float)pkt->view_aux_2_ch_10_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend11.progress = ((float)pkt->view_aux_2_ch_11_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend12.progress = ((float)pkt->view_aux_2_ch_12_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend13.progress = ((float)pkt->view_aux_2_ch_13_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend14.progress = ((float)pkt->view_aux_2_ch_14_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend15.progress = ((float)pkt->view_aux_2_ch_15_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend16.progress = ((float)pkt->view_aux_2_ch_16_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend17.progress = ((float)pkt->view_aux_2_ch_17_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend18.progress = ((float)pkt->view_aux_2_ch_18_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            knobAuxSend1.value = pkt->aux_2_ch_1_level;
            knobAuxSend2.value = pkt->aux_2_ch_2_level;
            knobAuxSend3.value = pkt->aux_2_ch_3_level;
            knobAuxSend4.value = pkt->aux_2_ch_4_level;
            knobAuxSend5.value = pkt->aux_2_ch_5_level;
            knobAuxSend6.value = pkt->aux_2_ch_6_level;
            knobAuxSend7.value = pkt->aux_2_ch_7_level;
            knobAuxSend8.value = pkt->aux_2_ch_8_level;
            knobAuxSend9.value = pkt->aux_2_ch_9_level;
            knobAuxSend10.value = pkt->aux_2_ch_10_level;
            knobAuxSend11.value = pkt->aux_2_ch_11_level;
            knobAuxSend12.value = pkt->aux_2_ch_12_level;
            knobAuxSend13.value = pkt->aux_2_ch_13_level;
            knobAuxSend14.value = pkt->aux_2_ch_14_level;
            knobAuxSend15.value = pkt->aux_2_ch_15_level;
            knobAuxSend16.value = pkt->aux_2_ch_16_level;
            knobAuxSend17.value = pkt->aux_2_ch_17_level;
            knobAuxSend18.value = pkt->aux_2_ch_18_level;
            if (![viewController sendCommandExists:CMD_AUX2_SEND dspId:DSP_5]) {
                [self refreshAuxPrePostButtons1_8:(pkt->aux_2_send & 0x00FF)];
                [self refreshAuxPrePostButtons9_16:((pkt->aux_2_send & 0xFF00) >> 8)];
            }
            if (![viewController sendCommandExists:CMD_CH17_USB_AUDIO_SETTING dspId:DSP_5]) {
                btnAuxSendPrePost17.selected = (pkt->ch_17_audio_setting & 0x20) >> 5;
            }
            if (![viewController sendCommandExists:CMD_CH18_USB_AUDIO_SETTING dspId:DSP_5]) {
                btnAuxSendPrePost18.selected = (pkt->ch_18_audio_setting & 0x20) >> 5;
            }
            if (![viewController sendCommandExists:CMD_AUXGP_SOLO_CTRL dspId:DSP_5]) {
                btnAuxSendSolo.selected = (pkt->solo_ctrl & 0x0200) >> 9;
            }
            BOOL aux2SoloSafe = (pkt->solo_safe_ctrl & 0x0200) >> 9;
            if (aux2SoloSafe) {
                [btnAuxSendSolo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
                [btnAuxSendSolo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
            } else {
                [btnAuxSendSolo setTitle:@"SOLO" forState:UIControlStateNormal];
                [btnAuxSendSolo setTitle:@"SOLO" forState:UIControlStateSelected];
            }
            if (![viewController sendCommandExists:CMD_AUXGP_ON_OFF dspId:DSP_5]) {
                btnAuxSendOn.selected = (pkt->aux_gp_master & 0x0200) >> 9;
            }
            if (![viewController sendCommandExists:CMD_AUXGP_METER dspId:DSP_5]) {
                btnAuxMeter.selected = (pkt->aux_gp_meter_master & 0x0200) >> 9;
            }
            if (!sliderAuxSendMainLock) {
                sliderAuxSendMain.value = pkt->aux_2_master_fader;
                [self onSliderAuxSendMainAction:nil];
            }
            if (![viewController sendCommandExists:CMD_VIEW_METER_CONTROL dspId:DSP_5]) {
                btnAuxSendPrePostAll.selected = (pkt->view_meter_control & 0x08) >> 3;
            }
            if (![viewController sendCommandExists:CMD_MULTI1_SOURCE dspId:DSP_5]) {
                btnAuxMulti1.selected = (pkt->multi_1_source & 0x0200) >> 9;
            }
            if (![viewController sendCommandExists:CMD_MULTI2_SOURCE dspId:DSP_5]) {
                btnAuxMulti2.selected = (pkt->multi_2_source & 0x0200) >> 9;
            }
            if (![viewController sendCommandExists:CMD_MULTI3_SOURCE dspId:DSP_5]) {
                btnAuxMulti3.selected = (pkt->multi_3_source & 0x0200) >> 9;
            }
            if (![viewController sendCommandExists:CMD_MULTI4_SOURCE dspId:DSP_5]) {
                btnAuxMulti4.selected = (pkt->multi_4_source & 0x0200) >> 9;
            }
            break;
            
        case AUX_3:
            meterAuxSend1.progress = ((float)pkt->view_aux_3_ch_1_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend2.progress = ((float)pkt->view_aux_3_ch_2_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend3.progress = ((float)pkt->view_aux_3_ch_3_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend4.progress = ((float)pkt->view_aux_3_ch_4_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend5.progress = ((float)pkt->view_aux_3_ch_5_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend6.progress = ((float)pkt->view_aux_3_ch_6_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend7.progress = ((float)pkt->view_aux_3_ch_7_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend8.progress = ((float)pkt->view_aux_3_ch_8_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend9.progress = ((float)pkt->view_aux_3_ch_9_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend10.progress = ((float)pkt->view_aux_3_ch_10_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend11.progress = ((float)pkt->view_aux_3_ch_11_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend12.progress = ((float)pkt->view_aux_3_ch_12_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend13.progress = ((float)pkt->view_aux_3_ch_13_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend14.progress = ((float)pkt->view_aux_3_ch_14_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend15.progress = ((float)pkt->view_aux_3_ch_15_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend16.progress = ((float)pkt->view_aux_3_ch_16_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend17.progress = ((float)pkt->view_aux_3_ch_17_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend18.progress = ((float)pkt->view_aux_3_ch_18_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            knobAuxSend1.value = pkt->aux_3_ch_1_level;
            knobAuxSend2.value = pkt->aux_3_ch_2_level;
            knobAuxSend3.value = pkt->aux_3_ch_3_level;
            knobAuxSend4.value = pkt->aux_3_ch_4_level;
            knobAuxSend5.value = pkt->aux_3_ch_5_level;
            knobAuxSend6.value = pkt->aux_3_ch_6_level;
            knobAuxSend7.value = pkt->aux_3_ch_7_level;
            knobAuxSend8.value = pkt->aux_3_ch_8_level;
            knobAuxSend9.value = pkt->aux_3_ch_9_level;
            knobAuxSend10.value = pkt->aux_3_ch_10_level;
            knobAuxSend11.value = pkt->aux_3_ch_11_level;
            knobAuxSend12.value = pkt->aux_3_ch_12_level;
            knobAuxSend13.value = pkt->aux_3_ch_13_level;
            knobAuxSend14.value = pkt->aux_3_ch_14_level;
            knobAuxSend15.value = pkt->aux_3_ch_15_level;
            knobAuxSend16.value = pkt->aux_3_ch_16_level;
            knobAuxSend17.value = pkt->aux_3_ch_17_level;
            knobAuxSend18.value = pkt->aux_3_ch_18_level;
            if (![viewController sendCommandExists:CMD_AUX3_SEND dspId:DSP_5]) {
                [self refreshAuxPrePostButtons1_8:(pkt->aux_3_send & 0x00FF)];
                [self refreshAuxPrePostButtons9_16:((pkt->aux_3_send & 0xFF00) >> 8)];
            }
            if (![viewController sendCommandExists:CMD_CH17_USB_AUDIO_SETTING dspId:DSP_5]) {
                btnAuxSendPrePost17.selected = (pkt->ch_17_audio_setting & 0x40) >> 6;
            }
            if (![viewController sendCommandExists:CMD_CH18_USB_AUDIO_SETTING dspId:DSP_5]) {
                btnAuxSendPrePost18.selected = (pkt->ch_18_audio_setting & 0x40) >> 6;
            }
            if (![viewController sendCommandExists:CMD_AUXGP_SOLO_CTRL dspId:DSP_5]) {
                btnAuxSendSolo.selected = (pkt->solo_ctrl & 0x0400) >> 10;
            }
            BOOL auxSolo3Safe = (pkt->solo_safe_ctrl & 0x0400) >> 10;
            if (auxSolo3Safe) {
                [btnAuxSendSolo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
                [btnAuxSendSolo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
            } else {
                [btnAuxSendSolo setTitle:@"SOLO" forState:UIControlStateNormal];
                [btnAuxSendSolo setTitle:@"SOLO" forState:UIControlStateSelected];
            }
            if (![viewController sendCommandExists:CMD_AUXGP_ON_OFF dspId:DSP_5]) {
                btnAuxSendOn.selected = (pkt->aux_gp_master & 0x0400) >> 10;
            }
            if (![viewController sendCommandExists:CMD_AUXGP_METER dspId:DSP_5]) {
                btnAuxMeter.selected = (pkt->aux_gp_meter_master & 0x0400) >> 10;
            }
            if (!sliderAuxSendMainLock) {
                sliderAuxSendMain.value = pkt->aux_3_master_fader;
                [self onSliderAuxSendMainAction:nil];
            }
            if (![viewController sendCommandExists:CMD_VIEW_METER_CONTROL dspId:DSP_5]) {
                btnAuxSendPrePostAll.selected = (pkt->view_meter_control & 0x08) >> 3;
            }
            if (![viewController sendCommandExists:CMD_MULTI1_SOURCE dspId:DSP_5]) {
                btnAuxMulti1.selected = (pkt->multi_1_source & 0x0400) >> 10;
            }
            if (![viewController sendCommandExists:CMD_MULTI2_SOURCE dspId:DSP_5]) {
                btnAuxMulti2.selected = (pkt->multi_2_source & 0x0400) >> 10;
            }
            if (![viewController sendCommandExists:CMD_MULTI3_SOURCE dspId:DSP_5]) {
                btnAuxMulti3.selected = (pkt->multi_3_source & 0x0400) >> 10;
            }
            if (![viewController sendCommandExists:CMD_MULTI4_SOURCE dspId:DSP_5]) {
                btnAuxMulti4.selected = (pkt->multi_4_source & 0x0400) >> 10;
            }
            break;
            
        case AUX_4:
            meterAuxSend1.progress = ((float)pkt->view_aux_4_ch_1_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend2.progress = ((float)pkt->view_aux_4_ch_2_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend3.progress = ((float)pkt->view_aux_4_ch_3_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend4.progress = ((float)pkt->view_aux_4_ch_4_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend5.progress = ((float)pkt->view_aux_4_ch_5_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend6.progress = ((float)pkt->view_aux_4_ch_6_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend7.progress = ((float)pkt->view_aux_4_ch_7_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend8.progress = ((float)pkt->view_aux_4_ch_8_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend9.progress = ((float)pkt->view_aux_4_ch_9_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend10.progress = ((float)pkt->view_aux_4_ch_10_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend11.progress = ((float)pkt->view_aux_4_ch_11_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend12.progress = ((float)pkt->view_aux_4_ch_12_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend13.progress = ((float)pkt->view_aux_4_ch_13_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend14.progress = ((float)pkt->view_aux_4_ch_14_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend15.progress = ((float)pkt->view_aux_4_ch_15_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend16.progress = ((float)pkt->view_aux_4_ch_16_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend17.progress = ((float)pkt->view_aux_4_ch_17_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterAuxSend18.progress = ((float)pkt->view_aux_4_ch_18_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            knobAuxSend1.value = pkt->aux_4_ch_1_level;
            knobAuxSend2.value = pkt->aux_4_ch_2_level;
            knobAuxSend3.value = pkt->aux_4_ch_3_level;
            knobAuxSend4.value = pkt->aux_4_ch_4_level;
            knobAuxSend5.value = pkt->aux_4_ch_5_level;
            knobAuxSend6.value = pkt->aux_4_ch_6_level;
            knobAuxSend7.value = pkt->aux_4_ch_7_level;
            knobAuxSend8.value = pkt->aux_4_ch_8_level;
            knobAuxSend9.value = pkt->aux_4_ch_9_level;
            knobAuxSend10.value = pkt->aux_4_ch_10_level;
            knobAuxSend11.value = pkt->aux_4_ch_11_level;
            knobAuxSend12.value = pkt->aux_4_ch_12_level;
            knobAuxSend13.value = pkt->aux_4_ch_13_level;
            knobAuxSend14.value = pkt->aux_4_ch_14_level;
            knobAuxSend15.value = pkt->aux_4_ch_15_level;
            knobAuxSend16.value = pkt->aux_4_ch_16_level;
            knobAuxSend17.value = pkt->aux_4_ch_17_level;
            knobAuxSend18.value = pkt->aux_4_ch_18_level;
            if (![viewController sendCommandExists:CMD_AUX4_SEND dspId:DSP_5]) {
                [self refreshAuxPrePostButtons1_8:(pkt->aux_4_send & 0x00FF)];
                [self refreshAuxPrePostButtons9_16:((pkt->aux_4_send & 0xFF00) >> 8)];
            }
            if (![viewController sendCommandExists:CMD_CH17_USB_AUDIO_SETTING dspId:DSP_5]) {
                btnAuxSendPrePost17.selected = (pkt->ch_17_audio_setting & 0x80) >> 7;
            }
            if (![viewController sendCommandExists:CMD_CH18_USB_AUDIO_SETTING dspId:DSP_5]) {
                btnAuxSendPrePost18.selected = (pkt->ch_18_audio_setting & 0x80) >> 7;
            }
            if (![viewController sendCommandExists:CMD_AUXGP_SOLO_CTRL dspId:DSP_5]) {
                btnAuxSendSolo.selected = (pkt->solo_ctrl & 0x0800) >> 11;
            }
            BOOL auxSolo4Safe = (pkt->solo_safe_ctrl & 0x0800) >> 11;
            if (auxSolo4Safe) {
                [btnAuxSendSolo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
                [btnAuxSendSolo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
            } else {
                [btnAuxSendSolo setTitle:@"SOLO" forState:UIControlStateNormal];
                [btnAuxSendSolo setTitle:@"SOLO" forState:UIControlStateSelected];
            }
            if (![viewController sendCommandExists:CMD_AUXGP_ON_OFF dspId:DSP_5]) {
                btnAuxSendOn.selected = (pkt->aux_gp_master & 0x0800) >> 11;
            }
            if (![viewController sendCommandExists:CMD_AUXGP_METER dspId:DSP_5]) {
                btnAuxMeter.selected = (pkt->aux_gp_meter_master & 0x0800) >> 11;
            }
            if (!sliderAuxSendMainLock) {
                sliderAuxSendMain.value = pkt->aux_4_master_fader;
                [self onSliderAuxSendMainAction:nil];
            }
            if (![viewController sendCommandExists:CMD_VIEW_METER_CONTROL dspId:DSP_5]) {
                btnAuxSendPrePostAll.selected = (pkt->view_meter_control & 0x08) >> 3;
            }
            if (![viewController sendCommandExists:CMD_MULTI1_SOURCE dspId:DSP_5]) {
                btnAuxMulti1.selected = (pkt->multi_1_source & 0x0800) >> 11;
            }
            if (![viewController sendCommandExists:CMD_MULTI2_SOURCE dspId:DSP_5]) {
                btnAuxMulti2.selected = (pkt->multi_2_source & 0x0800) >> 11;
            }
            if (![viewController sendCommandExists:CMD_MULTI3_SOURCE dspId:DSP_5]) {
                btnAuxMulti3.selected = (pkt->multi_3_source & 0x0800) >> 11;
            }
            if (![viewController sendCommandExists:CMD_MULTI4_SOURCE dspId:DSP_5]) {
                btnAuxMulti4.selected = (pkt->multi_4_source & 0x0800) >> 11;
            }
            break;
            
        default:
            break;
    }
    
    // Update slider values
    [lblAuxSendValue1 setText:[Global getSliderGain:knobAuxSend1.value appendDb:YES]];
    [lblAuxSendValue2 setText:[Global getSliderGain:knobAuxSend2.value appendDb:YES]];
    [lblAuxSendValue3 setText:[Global getSliderGain:knobAuxSend3.value appendDb:YES]];
    [lblAuxSendValue4 setText:[Global getSliderGain:knobAuxSend4.value appendDb:YES]];
    [lblAuxSendValue5 setText:[Global getSliderGain:knobAuxSend5.value appendDb:YES]];
    [lblAuxSendValue6 setText:[Global getSliderGain:knobAuxSend6.value appendDb:YES]];
    [lblAuxSendValue7 setText:[Global getSliderGain:knobAuxSend7.value appendDb:YES]];
    [lblAuxSendValue8 setText:[Global getSliderGain:knobAuxSend8.value appendDb:YES]];
    [lblAuxSendValue9 setText:[Global getSliderGain:knobAuxSend9.value appendDb:YES]];
    [lblAuxSendValue10 setText:[Global getSliderGain:knobAuxSend10.value appendDb:YES]];
    [lblAuxSendValue11 setText:[Global getSliderGain:knobAuxSend11.value appendDb:YES]];
    [lblAuxSendValue12 setText:[Global getSliderGain:knobAuxSend12.value appendDb:YES]];
    [lblAuxSendValue13 setText:[Global getSliderGain:knobAuxSend13.value appendDb:YES]];
    [lblAuxSendValue14 setText:[Global getSliderGain:knobAuxSend14.value appendDb:YES]];
    [lblAuxSendValue15 setText:[Global getSliderGain:knobAuxSend15.value appendDb:YES]];
    [lblAuxSendValue16 setText:[Global getSliderGain:knobAuxSend16.value appendDb:YES]];
    [lblAuxSendValue17 setText:[Global getSliderGain:knobAuxSend17.value appendDb:YES]];
    [lblAuxSendValue18 setText:[Global getSliderGain:knobAuxSend18.value appendDb:YES]];
    cpvAuxSend1.progress = knobAuxSend1.value/AUX_SEND_MAX_VALUE;
    cpvAuxSend2.progress = knobAuxSend2.value/AUX_SEND_MAX_VALUE;
    cpvAuxSend3.progress = knobAuxSend3.value/AUX_SEND_MAX_VALUE;
    cpvAuxSend4.progress = knobAuxSend4.value/AUX_SEND_MAX_VALUE;
    cpvAuxSend5.progress = knobAuxSend5.value/AUX_SEND_MAX_VALUE;
    cpvAuxSend6.progress = knobAuxSend6.value/AUX_SEND_MAX_VALUE;
    cpvAuxSend7.progress = knobAuxSend7.value/AUX_SEND_MAX_VALUE;
    cpvAuxSend8.progress = knobAuxSend8.value/AUX_SEND_MAX_VALUE;
    cpvAuxSend9.progress = knobAuxSend9.value/AUX_SEND_MAX_VALUE;
    cpvAuxSend10.progress = knobAuxSend10.value/AUX_SEND_MAX_VALUE;
    cpvAuxSend11.progress = knobAuxSend11.value/AUX_SEND_MAX_VALUE;
    cpvAuxSend12.progress = knobAuxSend12.value/AUX_SEND_MAX_VALUE;
    cpvAuxSend13.progress = knobAuxSend13.value/AUX_SEND_MAX_VALUE;
    cpvAuxSend14.progress = knobAuxSend14.value/AUX_SEND_MAX_VALUE;
    cpvAuxSend15.progress = knobAuxSend15.value/AUX_SEND_MAX_VALUE;
    cpvAuxSend16.progress = knobAuxSend16.value/AUX_SEND_MAX_VALUE;
    cpvAuxSend17.progress = knobAuxSend17.value/AUX_SEND_MAX_VALUE;
    cpvAuxSend18.progress = knobAuxSend18.value/AUX_SEND_MAX_VALUE;
}

- (void)processMeter:(AllMeterValueRxPacket *)meterValue
{
    switch (currentAux) {
        case AUX_1:
            meterAuxSendMain.progress = ((float)meterValue->aux_1_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            txtCurrentAux.text = [NSString stringWithUTF8String:(const char *)meterValue->aux_1_audio_name];
            break;
        case AUX_2:
            meterAuxSendMain.progress = ((float)meterValue->aux_2_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            txtCurrentAux.text = [NSString stringWithUTF8String:(const char *)meterValue->aux_2_audio_name];
            break;
        case AUX_3:
            meterAuxSendMain.progress = ((float)meterValue->aux_3_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            txtCurrentAux.text = [NSString stringWithUTF8String:(const char *)meterValue->aux_3_audio_name];
            break;
        case AUX_4:
            meterAuxSendMain.progress = ((float)meterValue->aux_4_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            txtCurrentAux.text = [NSString stringWithUTF8String:(const char *)meterValue->aux_4_audio_name];
            break;
        default:
            break;
    }
}

#pragma mark -
#pragma mark Aux sends event handler methods

- (IBAction)knobAux1DidChange
{
	lblAuxSendValue1.text = [Global getSliderGain:knobAuxSend1.value appendDb:YES];
    cpvAuxSend1.progress = knobAuxSend1.value/AUX_SEND_MAX_VALUE;
    
    switch (currentAux) {
        case AUX_1:
            [viewController sendData:CMD_AUX1_CH1_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend1.value];
            break;
        case AUX_2:
            [viewController sendData:CMD_AUX2_CH1_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend1.value];
            break;
        case AUX_3:
            [viewController sendData:CMD_AUX3_CH1_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend1.value];
            break;
        case AUX_4:
            [viewController sendData:CMD_AUX4_CH1_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend1.value];
            break;
        default:
            break;
    }
}

- (IBAction)knobAux2DidChange
{
	lblAuxSendValue2.text = [Global getSliderGain:knobAuxSend2.value appendDb:YES];
    cpvAuxSend2.progress = knobAuxSend2.value/AUX_SEND_MAX_VALUE;
    
    switch (currentAux) {
        case AUX_1:
            [viewController sendData:CMD_AUX1_CH2_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend2.value];
            break;
        case AUX_2:
            [viewController sendData:CMD_AUX2_CH2_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend2.value];
            break;
        case AUX_3:
            [viewController sendData:CMD_AUX3_CH2_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend2.value];
            break;
        case AUX_4:
            [viewController sendData:CMD_AUX4_CH2_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend2.value];
            break;
        default:
            break;
    }
}

- (IBAction)knobAux3DidChange
{
	lblAuxSendValue3.text = [Global getSliderGain:knobAuxSend3.value appendDb:YES];
    cpvAuxSend3.progress = knobAuxSend3.value/AUX_SEND_MAX_VALUE;
    
    switch (currentAux) {
        case AUX_1:
            [viewController sendData:CMD_AUX1_CH3_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend3.value];
            break;
        case AUX_2:
            [viewController sendData:CMD_AUX2_CH3_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend3.value];
            break;
        case AUX_3:
            [viewController sendData:CMD_AUX3_CH3_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend3.value];
            break;
        case AUX_4:
            [viewController sendData:CMD_AUX4_CH3_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend3.value];
            break;
        default:
            break;
    }
}

- (IBAction)knobAux4DidChange
{
	lblAuxSendValue4.text = [Global getSliderGain:knobAuxSend4.value appendDb:YES];
    cpvAuxSend4.progress = knobAuxSend4.value/AUX_SEND_MAX_VALUE;
    
    switch (currentAux) {
        case AUX_1:
            [viewController sendData:CMD_AUX1_CH4_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend4.value];
            break;
        case AUX_2:
            [viewController sendData:CMD_AUX2_CH4_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend4.value];
            break;
        case AUX_3:
            [viewController sendData:CMD_AUX3_CH4_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend4.value];
            break;
        case AUX_4:
            [viewController sendData:CMD_AUX4_CH4_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend4.value];
            break;
        default:
            break;
    }
}

- (IBAction)knobAux5DidChange
{
	lblAuxSendValue5.text = [Global getSliderGain:knobAuxSend5.value appendDb:YES];
    cpvAuxSend5.progress = knobAuxSend5.value/AUX_SEND_MAX_VALUE;
    
    switch (currentAux) {
        case AUX_1:
            [viewController sendData:CMD_AUX1_CH5_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend5.value];
            break;
        case AUX_2:
            [viewController sendData:CMD_AUX2_CH5_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend5.value];
            break;
        case AUX_3:
            [viewController sendData:CMD_AUX3_CH5_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend5.value];
            break;
        case AUX_4:
            [viewController sendData:CMD_AUX4_CH5_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend5.value];
            break;
        default:
            break;
    }
}

- (IBAction)knobAux6DidChange
{
	lblAuxSendValue6.text = [Global getSliderGain:knobAuxSend6.value appendDb:YES];
    cpvAuxSend6.progress = knobAuxSend6.value/AUX_SEND_MAX_VALUE;
    
    switch (currentAux) {
        case AUX_1:
            [viewController sendData:CMD_AUX1_CH6_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend6.value];
            break;
        case AUX_2:
            [viewController sendData:CMD_AUX2_CH6_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend6.value];
            break;
        case AUX_3:
            [viewController sendData:CMD_AUX3_CH6_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend6.value];
            break;
        case AUX_4:
            [viewController sendData:CMD_AUX4_CH6_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend6.value];
            break;
        default:
            break;
    }
}

- (IBAction)knobAux7DidChange
{
	lblAuxSendValue7.text = [Global getSliderGain:knobAuxSend7.value appendDb:YES];
    cpvAuxSend7.progress = knobAuxSend7.value/AUX_SEND_MAX_VALUE;
    
    switch (currentAux) {
        case AUX_1:
            [viewController sendData:CMD_AUX1_CH7_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend7.value];
            break;
        case AUX_2:
            [viewController sendData:CMD_AUX2_CH7_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend7.value];
            break;
        case AUX_3:
            [viewController sendData:CMD_AUX3_CH7_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend7.value];
            break;
        case AUX_4:
            [viewController sendData:CMD_AUX4_CH7_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend7.value];
            break;
        default:
            break;
    }
}

- (IBAction)knobAux8DidChange
{
	lblAuxSendValue8.text = [Global getSliderGain:knobAuxSend8.value appendDb:YES];
    cpvAuxSend8.progress = knobAuxSend8.value/AUX_SEND_MAX_VALUE;
    
    switch (currentAux) {
        case AUX_1:
            [viewController sendData:CMD_AUX1_CH8_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend8.value];
            break;
        case AUX_2:
            [viewController sendData:CMD_AUX2_CH8_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend8.value];
            break;
        case AUX_3:
            [viewController sendData:CMD_AUX3_CH8_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend8.value];
            break;
        case AUX_4:
            [viewController sendData:CMD_AUX4_CH8_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend8.value];
            break;
        default:
            break;
    }
}

- (IBAction)knobAux9DidChange
{
	lblAuxSendValue9.text = [Global getSliderGain:knobAuxSend9.value appendDb:YES];
    cpvAuxSend9.progress = knobAuxSend9.value/AUX_SEND_MAX_VALUE;
    
    switch (currentAux) {
        case AUX_1:
            [viewController sendData:CMD_AUX1_CH9_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend9.value];
            break;
        case AUX_2:
            [viewController sendData:CMD_AUX2_CH9_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend9.value];
            break;
        case AUX_3:
            [viewController sendData:CMD_AUX3_CH9_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend9.value];
            break;
        case AUX_4:
            [viewController sendData:CMD_AUX4_CH9_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend9.value];
            break;
        default:
            break;
    }
}

- (IBAction)knobAux10DidChange
{
	lblAuxSendValue10.text = [Global getSliderGain:knobAuxSend10.value appendDb:YES];
    cpvAuxSend10.progress = knobAuxSend10.value/AUX_SEND_MAX_VALUE;
    
    switch (currentAux) {
        case AUX_1:
            [viewController sendData:CMD_AUX1_CH10_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend10.value];
            break;
        case AUX_2:
            [viewController sendData:CMD_AUX2_CH10_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend10.value];
            break;
        case AUX_3:
            [viewController sendData:CMD_AUX3_CH10_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend10.value];
            break;
        case AUX_4:
            [viewController sendData:CMD_AUX4_CH10_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend10.value];
            break;
        default:
            break;
    }
}

- (IBAction)knobAux11DidChange
{
	lblAuxSendValue11.text = [Global getSliderGain:knobAuxSend11.value appendDb:YES];
    cpvAuxSend11.progress = knobAuxSend11.value/AUX_SEND_MAX_VALUE;
    
    switch (currentAux) {
        case AUX_1:
            [viewController sendData:CMD_AUX1_CH11_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend11.value];
            break;
        case AUX_2:
            [viewController sendData:CMD_AUX2_CH11_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend11.value];
            break;
        case AUX_3:
            [viewController sendData:CMD_AUX3_CH11_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend11.value];
            break;
        case AUX_4:
            [viewController sendData:CMD_AUX4_CH11_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend11.value];
            break;
        default:
            break;
    }
}

- (IBAction)knobAux12DidChange
{
	lblAuxSendValue12.text = [Global getSliderGain:knobAuxSend12.value appendDb:YES];
    cpvAuxSend12.progress = knobAuxSend12.value/AUX_SEND_MAX_VALUE;
    
    switch (currentAux) {
        case AUX_1:
            [viewController sendData:CMD_AUX1_CH12_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend12.value];
            break;
        case AUX_2:
            [viewController sendData:CMD_AUX2_CH12_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend12.value];
            break;
        case AUX_3:
            [viewController sendData:CMD_AUX3_CH12_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend12.value];
            break;
        case AUX_4:
            [viewController sendData:CMD_AUX4_CH12_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend12.value];
            break;
        default:
            break;
    }
}

- (IBAction)knobAux13DidChange
{
	lblAuxSendValue13.text = [Global getSliderGain:knobAuxSend13.value appendDb:YES];
    cpvAuxSend13.progress = knobAuxSend13.value/AUX_SEND_MAX_VALUE;
    
    switch (currentAux) {
        case AUX_1:
            [viewController sendData:CMD_AUX1_CH13_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend13.value];
            break;
        case AUX_2:
            [viewController sendData:CMD_AUX2_CH13_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend13.value];
            break;
        case AUX_3:
            [viewController sendData:CMD_AUX3_CH13_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend13.value];
            break;
        case AUX_4:
            [viewController sendData:CMD_AUX4_CH13_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend13.value];
            break;
        default:
            break;
    }
}

- (IBAction)knobAux14DidChange
{
	lblAuxSendValue14.text = [Global getSliderGain:knobAuxSend14.value appendDb:YES];
    cpvAuxSend14.progress = knobAuxSend14.value/AUX_SEND_MAX_VALUE;
    
    switch (currentAux) {
        case AUX_1:
            [viewController sendData:CMD_AUX1_CH14_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend14.value];
            break;
        case AUX_2:
            [viewController sendData:CMD_AUX2_CH14_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend14.value];
            break;
        case AUX_3:
            [viewController sendData:CMD_AUX3_CH14_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend14.value];
            break;
        case AUX_4:
            [viewController sendData:CMD_AUX4_CH14_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend14.value];
            break;
        default:
            break;
    }
}

- (IBAction)knobAux15DidChange
{
	lblAuxSendValue15.text = [Global getSliderGain:knobAuxSend15.value appendDb:YES];
    cpvAuxSend15.progress = knobAuxSend15.value/AUX_SEND_MAX_VALUE;
    
    switch (currentAux) {
        case AUX_1:
            [viewController sendData:CMD_AUX1_CH15_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend15.value];
            break;
        case AUX_2:
            [viewController sendData:CMD_AUX2_CH15_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend15.value];
            break;
        case AUX_3:
            [viewController sendData:CMD_AUX3_CH15_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend15.value];
            break;
        case AUX_4:
            [viewController sendData:CMD_AUX4_CH15_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend15.value];
            break;
        default:
            break;
    }
}

- (IBAction)knobAux16DidChange
{
	lblAuxSendValue16.text = [Global getSliderGain:knobAuxSend16.value appendDb:YES];
    cpvAuxSend16.progress = knobAuxSend16.value/AUX_SEND_MAX_VALUE;
    
    switch (currentAux) {
        case AUX_1:
            [viewController sendData:CMD_AUX1_CH16_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend16.value];
            break;
        case AUX_2:
            [viewController sendData:CMD_AUX2_CH16_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend16.value];
            break;
        case AUX_3:
            [viewController sendData:CMD_AUX3_CH16_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend16.value];
            break;
        case AUX_4:
            [viewController sendData:CMD_AUX4_CH16_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend16.value];
            break;
        default:
            break;
    }
}

- (IBAction)knobAux17DidChange
{
	lblAuxSendValue17.text = [Global getSliderGain:knobAuxSend17.value appendDb:YES];
    cpvAuxSend17.progress = knobAuxSend17.value/AUX_SEND_MAX_VALUE;
    
    switch (currentAux) {
        case AUX_1:
            [viewController sendData:CMD_AUX1_CH17_USB_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend17.value];
            break;
        case AUX_2:
            [viewController sendData:CMD_AUX2_CH17_USB_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend17.value];
            break;
        case AUX_3:
            [viewController sendData:CMD_AUX3_CH17_USB_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend17.value];
            break;
        case AUX_4:
            [viewController sendData:CMD_AUX4_CH17_USB_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend17.value];
            break;
        default:
            break;
    }
}

- (IBAction)knobAux18DidChange
{
	lblAuxSendValue18.text = [Global getSliderGain:knobAuxSend18.value appendDb:YES];
    cpvAuxSend18.progress = knobAuxSend18.value/AUX_SEND_MAX_VALUE;
    
    switch (currentAux) {
        case AUX_1:
            [viewController sendData:CMD_AUX1_CH18_USB_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend18.value];
            break;
        case AUX_2:
            [viewController sendData:CMD_AUX2_CH18_USB_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend18.value];
            break;
        case AUX_3:
            [viewController sendData:CMD_AUX3_CH18_USB_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend18.value];
            break;
        case AUX_4:
            [viewController sendData:CMD_AUX4_CH18_USB_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAuxSend18.value];
            break;
        default:
            break;
    }
}

- (void)sendAuxSendData {
    SInt16 auxSendValue = btnAuxSendPrePost1.selected + (btnAuxSendPrePost2.selected << 1) +
                        (btnAuxSendPrePost3.selected << 2) + (btnAuxSendPrePost4.selected << 3) +
                        (btnAuxSendPrePost5.selected << 4) + (btnAuxSendPrePost6.selected << 5) +
                        (btnAuxSendPrePost7.selected << 6) + (btnAuxSendPrePost8.selected << 7) +
                        (btnAuxSendPrePost9.selected << 8) + (btnAuxSendPrePost10.selected << 9) +
                        (btnAuxSendPrePost11.selected << 10) + (btnAuxSendPrePost12.selected << 11) +
                        (btnAuxSendPrePost13.selected << 12) + (btnAuxSendPrePost14.selected << 13) +
                        (btnAuxSendPrePost15.selected << 14) + (btnAuxSendPrePost16.selected << 15);
    
    switch (currentAux) {
        case AUX_1:
            [viewController sendData:CMD_AUX1_SEND commandType:DSP_WRITE dspId:DSP_5 value:auxSendValue];
            break;
        case AUX_2:
            [viewController sendData:CMD_AUX2_SEND commandType:DSP_WRITE dspId:DSP_5 value:auxSendValue];
            break;
        case AUX_3:
            [viewController sendData:CMD_AUX3_SEND commandType:DSP_WRITE dspId:DSP_5 value:auxSendValue];
            break;
        case AUX_4:
            [viewController sendData:CMD_AUX4_SEND commandType:DSP_WRITE dspId:DSP_5 value:auxSendValue];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnAuxSendPrePostAll:(id)sender {
    btnAuxSendPrePostAll.selected = !btnAuxSendPrePostAll.selected;
    [Global setClearBit:&viewMeterCtrl bitValue:btnAuxSendPrePostAll.selected bitOrder:3];
    [viewController sendData:CMD_VIEW_METER_CONTROL commandType:DSP_WRITE dspId:DSP_5 value:viewMeterCtrl];
}

- (IBAction)onBtnAuxSend1:(id)sender {
    btnAuxSendPrePost1.selected = !btnAuxSendPrePost1.selected;
    [self sendAuxSendData];
}

- (IBAction)onBtnAuxSend2:(id)sender {
    btnAuxSendPrePost2.selected = !btnAuxSendPrePost2.selected;
    [self sendAuxSendData];
}

- (IBAction)onBtnAuxSend3:(id)sender {
    btnAuxSendPrePost3.selected = !btnAuxSendPrePost3.selected;
    [self sendAuxSendData];
}

- (IBAction)onBtnAuxSend4:(id)sender {
    btnAuxSendPrePost4.selected = !btnAuxSendPrePost4.selected;
    [self sendAuxSendData];
}

- (IBAction)onBtnAuxSend5:(id)sender {
    btnAuxSendPrePost5.selected = !btnAuxSendPrePost5.selected;
    [self sendAuxSendData];
}

- (IBAction)onBtnAuxSend6:(id)sender {
    btnAuxSendPrePost6.selected = !btnAuxSendPrePost6.selected;
    [self sendAuxSendData];
}

- (IBAction)onBtnAuxSend7:(id)sender {
    btnAuxSendPrePost7.selected = !btnAuxSendPrePost7.selected;
    [self sendAuxSendData];
}

- (IBAction)onBtnAuxSend8:(id)sender {
    btnAuxSendPrePost8.selected = !btnAuxSendPrePost8.selected;
    [self sendAuxSendData];
}

- (IBAction)onBtnAuxSend9:(id)sender {
    btnAuxSendPrePost9.selected = !btnAuxSendPrePost9.selected;
    [self sendAuxSendData];
}

- (IBAction)onBtnAuxSend10:(id)sender {
    btnAuxSendPrePost10.selected = !btnAuxSendPrePost10.selected;
    [self sendAuxSendData];
}

- (IBAction)onBtnAuxSend11:(id)sender {
    btnAuxSendPrePost11.selected = !btnAuxSendPrePost11.selected;
    [self sendAuxSendData];
}

- (IBAction)onBtnAuxSend12:(id)sender {
    btnAuxSendPrePost12.selected = !btnAuxSendPrePost12.selected;
    [self sendAuxSendData];
}

- (IBAction)onBtnAuxSend13:(id)sender {
    btnAuxSendPrePost13.selected = !btnAuxSendPrePost13.selected;
    [self sendAuxSendData];
}

- (IBAction)onBtnAuxSend14:(id)sender {
    btnAuxSendPrePost14.selected = !btnAuxSendPrePost14.selected;
    [self sendAuxSendData];
}

- (IBAction)onBtnAuxSend15:(id)sender {
    btnAuxSendPrePost15.selected = !btnAuxSendPrePost15.selected;
    [self sendAuxSendData];
}

- (IBAction)onBtnAuxSend16:(id)sender {
    btnAuxSendPrePost16.selected = !btnAuxSendPrePost16.selected;
    [self sendAuxSendData];
}

- (IBAction)onBtnAuxSend17:(id)sender {
    btnAuxSendPrePost17.selected = !btnAuxSendPrePost17.selected;
    int bitOrder = 4;
    switch (currentAux) {
        case AUX_1:
            bitOrder = 4;
            break;
        case AUX_2:
            bitOrder = 5;
            break;
        case AUX_3:
            bitOrder = 6;
            break;
        case AUX_4:
            bitOrder = 7;
            break;
        default:
            break;
    }
    [Global setClearBit:&ch17AudioSetting bitValue:btnAuxSendPrePost17.selected bitOrder:bitOrder];
    [viewController sendData:CMD_CH17_USB_AUDIO_SETTING commandType:DSP_WRITE dspId:DSP_5 value:ch17AudioSetting];
}

- (IBAction)onBtnAuxSend18:(id)sender {
    btnAuxSendPrePost18.selected = !btnAuxSendPrePost18.selected;
    int bitOrder = 4;
    switch (currentAux) {
        case AUX_1:
            bitOrder = 4;
            break;
        case AUX_2:
            bitOrder = 5;
            break;
        case AUX_3:
            bitOrder = 6;
            break;
        case AUX_4:
            bitOrder = 7;
            break;
        default:
            break;
    }
    [Global setClearBit:&ch18AudioSetting bitValue:btnAuxSendPrePost18.selected bitOrder:bitOrder];
    [viewController sendData:CMD_CH18_USB_AUDIO_SETTING commandType:DSP_WRITE dspId:DSP_5 value:ch18AudioSetting];
}

- (IBAction)onBtnAuxSendSolo:(id)sender {
    btnAuxSendSolo.selected = !btnAuxSendSolo.selected;
    switch (currentAux) {
        case AUX_1:
            [Global setClearBit:&soloCtrl bitValue:btnAuxSendSolo.selected bitOrder:8];
            [viewController sendData:CMD_AUXGP_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:soloCtrl];
            break;
        case AUX_2:
            [Global setClearBit:&soloCtrl bitValue:btnAuxSendSolo.selected bitOrder:9];
            [viewController sendData:CMD_AUXGP_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:soloCtrl];
            break;
        case AUX_3:
            [Global setClearBit:&soloCtrl bitValue:btnAuxSendSolo.selected bitOrder:10];
            [viewController sendData:CMD_AUXGP_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:soloCtrl];
            break;
        case AUX_4:
            [Global setClearBit:&soloCtrl bitValue:btnAuxSendSolo.selected bitOrder:11];
            [viewController sendData:CMD_AUXGP_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:soloCtrl];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnAuxSendOn:(id)sender {
    btnAuxSendOn.selected = !btnAuxSendOn.selected;
    switch (currentAux) {
        case AUX_1:
            [Global setClearBit:&auxGpMaster bitValue:btnAuxSendOn.selected bitOrder:8];
            [viewController sendData:CMD_AUXGP_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:auxGpMaster];
            break;
        case AUX_2:
            [Global setClearBit:&auxGpMaster bitValue:btnAuxSendOn.selected bitOrder:9];
            [viewController sendData:CMD_AUXGP_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:auxGpMaster];
            break;
        case AUX_3:
            [Global setClearBit:&auxGpMaster bitValue:btnAuxSendOn.selected bitOrder:10];
            [viewController sendData:CMD_AUXGP_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:auxGpMaster];
            break;
        case AUX_4:
            [Global setClearBit:&auxGpMaster bitValue:btnAuxSendOn.selected bitOrder:11];
            [viewController sendData:CMD_AUXGP_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:auxGpMaster];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnAuxSelectDown:(id)sender {
    if (currentAux > AUX_1)
        currentAux--;
    currentAuxLabel = currentAux;
    [self updateCurrentAuxLabel];
}

- (IBAction)onBtnAuxSelectUp:(id)sender {
    if (currentAux < AUX_4)
        currentAux++;
    currentAuxLabel = currentAux;
    [self updateCurrentAuxLabel];
}

- (IBAction)onBtnAuxMeter:(id)sender {
    btnAuxMeter.selected = !btnAuxMeter.selected;
    switch (currentAux) {
        case AUX_1:
            [Global setClearBit:&auxGpMeterMaster bitValue:btnAuxMeter.selected bitOrder:8];
            [viewController sendData:CMD_AUXGP_METER commandType:DSP_WRITE dspId:DSP_5 value:auxGpMeterMaster];
            break;
        case AUX_2:
            [Global setClearBit:&auxGpMeterMaster bitValue:btnAuxMeter.selected bitOrder:9];
            [viewController sendData:CMD_AUXGP_METER commandType:DSP_WRITE dspId:DSP_5 value:auxGpMeterMaster];
            break;
        case AUX_3:
            [Global setClearBit:&auxGpMeterMaster bitValue:btnAuxMeter.selected bitOrder:10];
            [viewController sendData:CMD_AUXGP_METER commandType:DSP_WRITE dspId:DSP_5 value:auxGpMeterMaster];
            break;
        case AUX_4:
            [Global setClearBit:&auxGpMeterMaster bitValue:btnAuxMeter.selected bitOrder:11];
            [viewController sendData:CMD_AUXGP_METER commandType:DSP_WRITE dspId:DSP_5 value:auxGpMeterMaster];
            break;
        default:
            break;
    }
}

- (IBAction)onSliderAuxSendMainBegin:(id) sender {
    sliderAuxSendMainLock = YES;
}

- (IBAction)onSliderAuxSendMainEnd:(id) sender {
    sliderAuxSendMainLock = NO;
}

- (IBAction)onSliderAuxSendMainAction:(id) sender {
    [lblSliderAuxMainValue setText:[Global getSliderGain:sliderAuxSendMain.value appendDb:YES]];
    if (sender != nil) {
        switch (currentAux) {
            case AUX_1:
                [viewController sendData:CMD_AUX1_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderAuxSendMain.value];
                break;
            case AUX_2:
                [viewController sendData:CMD_AUX2_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderAuxSendMain.value];
                break;
            case AUX_3:
                [viewController sendData:CMD_AUX3_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderAuxSendMain.value];
                break;
            case AUX_4:
                [viewController sendData:CMD_AUX4_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderAuxSendMain.value];
                break;
            default:
                break;
        }
    }
}

- (IBAction)onBtnAuxMulti1:(id)sender {
    btnAuxMulti1.selected = !btnAuxMulti1.selected;
    multi1Source = 0;
    switch (currentAux) {
        case AUX_1:
            [Global setClearBit:&multi1Source bitValue:btnAuxMulti1.selected bitOrder:8];
            [viewController sendData:CMD_MULTI1_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:multi1Source];
            break;
        case AUX_2:
            [Global setClearBit:&multi1Source bitValue:btnAuxMulti1.selected bitOrder:9];
            [viewController sendData:CMD_MULTI1_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:multi1Source];
            break;
        case AUX_3:
            [Global setClearBit:&multi1Source bitValue:btnAuxMulti1.selected bitOrder:10];
            [viewController sendData:CMD_MULTI1_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:multi1Source];
            break;
        case AUX_4:
            [Global setClearBit:&multi1Source bitValue:btnAuxMulti1.selected bitOrder:11];
            [viewController sendData:CMD_MULTI1_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:multi1Source];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnAuxMulti2:(id)sender {
    btnAuxMulti2.selected = !btnAuxMulti2.selected;
    multi2Source = 0;
    switch (currentAux) {
        case AUX_1:
            [Global setClearBit:&multi2Source bitValue:btnAuxMulti2.selected bitOrder:8];
            [viewController sendData:CMD_MULTI2_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:multi2Source];
            break;
        case AUX_2:
            [Global setClearBit:&multi2Source bitValue:btnAuxMulti2.selected bitOrder:9];
            [viewController sendData:CMD_MULTI2_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:multi2Source];
            break;
        case AUX_3:
            [Global setClearBit:&multi2Source bitValue:btnAuxMulti2.selected bitOrder:10];
            [viewController sendData:CMD_MULTI2_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:multi2Source];
            break;
        case AUX_4:
            [Global setClearBit:&multi2Source bitValue:btnAuxMulti2.selected bitOrder:11];
            [viewController sendData:CMD_MULTI2_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:multi2Source];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnAuxMulti3:(id)sender {
    btnAuxMulti3.selected = !btnAuxMulti3.selected;
    multi3Source = 0;
    switch (currentAux) {
        case AUX_1:
            [Global setClearBit:&multi3Source bitValue:btnAuxMulti3.selected bitOrder:8];
            [viewController sendData:CMD_MULTI3_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:multi3Source];
            break;
        case AUX_2:
            [Global setClearBit:&multi3Source bitValue:btnAuxMulti3.selected bitOrder:9];
            [viewController sendData:CMD_MULTI3_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:multi3Source];
            break;
        case AUX_3:
            [Global setClearBit:&multi3Source bitValue:btnAuxMulti3.selected bitOrder:10];
            [viewController sendData:CMD_MULTI3_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:multi3Source];
            break;
        case AUX_4:
            [Global setClearBit:&multi3Source bitValue:btnAuxMulti3.selected bitOrder:11];
            [viewController sendData:CMD_MULTI3_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:multi3Source];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnAuxMulti4:(id)sender {
    btnAuxMulti4.selected = !btnAuxMulti4.selected;
    multi4Source = 0;
    switch (currentAux) {
        case AUX_1:
            [Global setClearBit:&multi4Source bitValue:btnAuxMulti4.selected bitOrder:8];
            [viewController sendData:CMD_MULTI4_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:multi4Source];
            break;
        case AUX_2:
            [Global setClearBit:&multi4Source bitValue:btnAuxMulti4.selected bitOrder:9];
            [viewController sendData:CMD_MULTI4_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:multi4Source];
            break;
        case AUX_3:
            [Global setClearBit:&multi4Source bitValue:btnAuxMulti4.selected bitOrder:10];
            [viewController sendData:CMD_MULTI4_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:multi4Source];
            break;
        case AUX_4:
            [Global setClearBit:&multi4Source bitValue:btnAuxMulti4.selected bitOrder:11];
            [viewController sendData:CMD_MULTI4_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:multi4Source];
            break;
        default:
            break;
    }
}

@end