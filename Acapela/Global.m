//
//  Global.m
//  eLive
//
//  Created by Kevin on 13/1/3.
//  Copyright (c) 2013年 Kevin Phua. All rights reserved.
//

#import "Global.h"
#import <QuartzCore/QuartzCore.h>

#define LOW_HIGH_SHELF_Q        0.707
#define DYN_INPUT_COUNT         51
#define DYN_MIN_VALUE           -50

@implementation Global

static Global* _instance = nil;
static NSArray* _faderTableArray;
static NSArray* _panTableArray;
static NSArray* _eqGainTableArray;
static NSArray* _eqFreqTableArray;
static NSArray* _eqQ1TableArray;
static NSArray* _eqQ4TableArray;
static NSArray* _eqQ23TableArray;
static NSArray* _freqTableArray;
static NSArray* _timeTableArray;
static NSArray* _ratioTableArray;
static NSArray* _ratioValueTableArray;
static NSArray* _ratioCompValueTableArray;
static NSArray* _dynInputTableArray;

- (Global *)init {
    self = [super init];
    
    // Init DSP tables
    NSString* plistPath = [[NSBundle mainBundle] pathForResource:@"DspTable" ofType:@"plist"];
    NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:plistPath];
    _faderTableArray = [NSArray arrayWithArray:(NSArray *)[dict valueForKey:@"Fader_Table"]];
    _panTableArray = [NSArray arrayWithArray:(NSArray *)[dict valueForKey:@"Pan_Table"]];
    _eqGainTableArray = [NSArray arrayWithArray:(NSArray *)[dict valueForKey:@"EQ_Gain_Table"]];
    _eqFreqTableArray = [NSArray arrayWithArray:(NSArray *)[dict valueForKey:@"EQ_Freq_Table"]];
    _eqQ1TableArray = [NSArray arrayWithArray:(NSArray *)[dict valueForKey:@"EQ_Q1_Table"]];
    _eqQ4TableArray = [NSArray arrayWithArray:(NSArray *)[dict valueForKey:@"EQ_Q4_Table"]];
    _eqQ23TableArray = [NSArray arrayWithArray:(NSArray *)[dict valueForKey:@"EQ_Q23_Table"]];
    _freqTableArray = [NSArray arrayWithArray:(NSArray *)[dict valueForKey:@"Freq_Table"]];
    _timeTableArray = [NSArray arrayWithArray:(NSArray *)[dict valueForKey:@"Time_Table"]];
    _ratioTableArray = [NSArray arrayWithArray:(NSArray *)[dict valueForKey:@"Ratio_Table"]];
    _ratioValueTableArray = [NSArray arrayWithArray:(NSArray *)[dict valueForKey:@"Ratio_Value_Table"]];
    _ratioCompValueTableArray = [NSArray arrayWithArray:(NSArray *)[dict valueForKey:@"Ratio_Comp_Value_Table"]];
    _dynInputTableArray = [NSArray arrayWithArray:(NSArray *)[dict valueForKey:@"Dyn_Input_Table"]];
    
    _instance = self;
    return self;
}

+ (Global *)getInstance {
    @synchronized(self) {
        if (_instance == nil) {
            _instance = [[Global new] init];
        }
    }
    return _instance;
}

+ (NSString *)getSliderGain:(float)value appendDb:(BOOL)appendDb {
    int index = (int)value;
    if (index < 0 || index >= [_faderTableArray count]) {
        NSLog(@"getSliderGain: Invalid value %f", value);
        return nil;
    }
    
    NSString *strGain = (NSString *)[_faderTableArray objectAtIndex:index];
    if (index == 0 || !appendDb)
        return strGain;
    return [strGain stringByAppendingString:@"dB"];
}

+ (NSString *)getPanString:(float)value {
    int index = (int)value;
    if (index < 0 || index >= [_panTableArray count]) {
        NSLog(@"getPanString: Invalid value %f", value);
        return nil;
    }
    
    NSString *strGain = (NSString *)[_panTableArray objectAtIndex:index];
    return strGain;
}

+ (NSString *)getEqGainString:(int)value {
    int index = (int)value;
    if (index < 0 || index >= [_eqGainTableArray count]) {
        NSLog(@"getEqGainString: Invalid value %d", value);
        return nil;
    }
    
    NSString *strGain = (NSString *)[_eqGainTableArray objectAtIndex:index];
    return [strGain stringByAppendingString:@"dB"];
}

+ (NSString *)getEqFreqString:(int)value {
    int index = (int)value;
    if (index < 0 || index >= [_eqFreqTableArray count]) {
        NSLog(@"getEqFreqString: Invalid value %d", value);
        return nil;
    }
    
    NSString *strFreq = (NSString *)[_eqFreqTableArray objectAtIndex:index];
    //double freq = [strFreq doubleValue];
    //if (freq >= 1000.0) {
    //    return [NSString stringWithFormat:@"%.1fkHz", freq/1000.0];
    //}
    return [strFreq stringByAppendingString:@"Hz"];
}

+ (NSString *)getDelayString:(int)value type:(int)type temp:(int)temp {
    float time = 0.0;
    if (value >= 0 && value <= 199) {
        time = (float)value * 0.1;
    } else if (value >= 200 && value <= 359) {
        time = ((value - 200.0) * 0.5) + 20;
    } else if (value >= 360 && value <= 439) {
        time = ((value - 360) * 5) + 100;
    } else {
        time = ((value - 440) * 10) + 500;
    }
    
    if (type == DELAY_TIME) {
        if (value >= 0 && value <= 199) {
            return [NSString stringWithFormat:@"%.1fmS", time];
        } else if (value >= 200 && value <= 359) {
            return [NSString stringWithFormat:@"%.1fmS", time];
        } else if (value >= 360 && value <= 439) {
            return [NSString stringWithFormat:@"%dmS", (int)time];
        } else {
            return [NSString stringWithFormat:@"%dmS", (int)time];
        }
    } else if (type == DELAY_METER) {
        float meter = (331 + 0.6 * temp) * time * pow(10,-3);
        return [NSString stringWithFormat:@"%.1f M", meter];
    } else if (type == DELAY_FEET) {
        float feet = ((331 + 0.6 * temp) * time * pow(10,-3)) * 3.28084;
        return [NSString stringWithFormat:@"%.1f Ft.", feet];
    }
    
    return @"";
}

+ (NSString *)getTempString:(int)temp {
    double tempF = (double)temp * 9 / 5 + 32.0;
    return [NSString stringWithFormat:@"%d°C/%.1f°F", temp, tempF];
}

+ (int)delayToKnobValue:(int)delay {
    // 0mS ~ 19.9mS Step 0.1mS, 20mS ~ 99.5mS Step 0. 5mS, 100 ~ 495mS Step 5 mS, 500 ~ 1000mS Step 10 mS
    if (delay >= 0 && delay <= 199) {
        return delay;
    } else if (delay >= 200 && delay <= 995) {
        return (delay / 5) + 160;
    } else if (delay >= 1000 && delay <= 4950) {
        return (delay / 50) + 340;
    } else {
        return (delay / 100) + 390;
    }
}

+ (int)knobValueToDelay:(int)knobValue {
    if (knobValue >= 0 && knobValue <= 199) {
        return knobValue;
    } else if (knobValue >= 200 && knobValue <= 359) {
        float val = ((knobValue - 200.0) * 0.5) + 20;
        return (int)(val * 10);
    } else if (knobValue >= 360 && knobValue <= 439) {
        int val = ((knobValue - 360) * 5) + 100;
        return (int)(val * 10);
    } else {
        int val = ((knobValue - 440) * 10) + 500;
        return (int)(val * 10);
    }
}

+ (int)freqToKnobValue:(int)freq {
    return (int)[_freqTableArray indexOfObject:[NSNumber numberWithInt:freq]];
}

+ (int)knobValueToFreq:(int)knobValue {
    NSNumber *value = [_freqTableArray objectAtIndex:knobValue];
    return [value intValue];
}

+ (float)tempToKnobValue:(int)temp {
    return (temp / 12800.0) * TEMP_MAX_VALUE;
}

+ (double)getEqGain:(int)index {
    if (index < 0 || index >= [_eqGainTableArray count]) {
        NSLog(@"getEqGain: Invalid value %d", index);
        return 0;
    }
    
    NSString *strGain = (NSString *)[_eqGainTableArray objectAtIndex:index];
    return [strGain doubleValue];
}

+ (double)getEqFreq:(int)index {
    if (index < 0 || index >= [_eqFreqTableArray count]) {
        NSLog(@"getEqFreq: Invalid value %d", index);
        return 0;
    }
    
    NSString *strFreq = (NSString *)[_eqFreqTableArray objectAtIndex:index];
    return [strFreq doubleValue];
}

+ (double)getEqQ:(int)index {
    if (index < 0 || index >= [_eqQ23TableArray count]) {
        NSLog(@"getEqQ: Invalid value %d", index);
        return 0;
    }
    
    NSString *strQ = (NSString *)[_eqQ23TableArray objectAtIndex:index];
    return [strQ doubleValue];
}

+ (NSString *)getEqQ1String:(float)value {
    int index = (int)value;
    if (index < 0 || index >= [_eqQ1TableArray count]) {
        NSLog(@"getEqQ1String: Invalid value %f", value);
        return nil;
    }
    
    NSString *strGain = (NSString *)[_eqQ1TableArray objectAtIndex:index];
    return strGain;
}

+ (NSString *)getEqQ4String:(float)value {
    int index = (int)value;
    if (index < 0 || index >= [_eqQ4TableArray count]) {
        NSLog(@"getEqQ4String: Invalid value %f", value);
        return nil;
    }
    
    NSString *strGain = (NSString *)[_eqQ4TableArray objectAtIndex:index];
    return strGain;
}

+ (NSString *)getEqQ23String:(float)value {
    int index = (int)value;
    if (index < 0 || index >= [_eqQ23TableArray count]) {
        NSLog(@"getEqQ23String: Invalid value %f", value);
        return nil;
    }
    
    NSString *strGain = (NSString *)[_eqQ23TableArray objectAtIndex:index];
    return strGain;
}

+ (NSString *)getFreqString:(float)value {
    int index = (int)value;
    if (index < 0 || index >= [_freqTableArray count]) {
        NSLog(@"getFreqString: Invalid value %f", value);
        return nil;
    }
    
    NSNumber *freq = (NSNumber *)[_freqTableArray objectAtIndex:index];
    int freqValue = [freq intValue];
    if (freqValue >= 1000) {
        return [NSString stringWithFormat:@"%.2fkHz", (float)freqValue/1000.0];
    } else {
        return [NSString stringWithFormat:@"%dHz", freqValue];
    }
}

+ (NSString *)getTimeString:(float)value {
    int index = (int)value;
    if (index < 0 || index >= [_timeTableArray count]) {
        NSLog(@"getTimeString: Invalid value %f", value);
        return nil;
    }
    
    NSString *strTime = (NSString *)[_timeTableArray objectAtIndex:index];
    return strTime;
}

+ (NSString *)getRatioString:(float)value {
    int index = (int)value;
    if (index < 0 || index >= [_ratioTableArray count]) {
        NSLog(@"getRatioString: Invalid value %f", value);
        return nil;
    }
    
    NSString *strRatio = (NSString *)[_ratioTableArray objectAtIndex:index];
    return strRatio;
}

+ (double)getRatioValue:(float)value {
    int index = (int)value;
    if (index < 0 || index >= [_ratioValueTableArray count]) {
        NSLog(@"getRatioValue: Invalid value %f", value);
        return 0.0;
    }
    
    NSString *ratioValue = (NSString *)[_ratioValueTableArray objectAtIndex:index];
    return [ratioValue doubleValue];
}

+ (double)getRatioCompValue:(float)value {
    int index = (int)value;
    if (index < 0 || index >= [_ratioCompValueTableArray count]) {
        NSLog(@"getRatioCompValue: Invalid value %f", value);
        return 0.0;
    }
    
    NSString *ratioValue = (NSString *)[_ratioCompValueTableArray objectAtIndex:index];
    return [ratioValue doubleValue];
}

+ (UIImage *)imageWithView:(UIView *)view scaledToSize:(CGSize)newSize {
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.opaque, [[UIScreen mainScreen] scale]);
    [view.layer renderInContext:UIGraphicsGetCurrentContext()];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [img drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}

+ (double)eqPeak:(double)freq eqFreq:(double)eqFreq eqDb:(double)eqDb eqQ:(double)eqQ {
    double db = sqrt(pow(10,(eqDb/20)));
    double S = freq/eqFreq;
    double numerator = sqrt(pow(1-pow(S, 2.0), 2.0) + pow(S*db/eqQ, 2.0));
    double denominator = sqrt(pow(1-pow(S, 2.0), 2.0) + pow((S/db/eqQ), 2.0));
    double H = numerator/denominator;
    //NSLog(@"S = %f, num = %f, denom = %f, H = %f", S, numerator, denominator, H);
    return 20 * log10(H);
}

+ (double)eqLowShelf:(double)freq eqFreq:(double)eqFreq eqDb:(double)eqDb {
    double db = sqrt(pow(10,(eqDb/20)));
    double dbSqrt = sqrt(db);
    double S = freq/eqFreq;
    double numerator = db * sqrt(pow(db-pow(S, 2.0), 2.0) + pow(S*dbSqrt/LOW_HIGH_SHELF_Q, 2.0));
    double denominator = sqrt(pow(1-db*pow(S, 2.0), 2.0) + pow((S*dbSqrt/LOW_HIGH_SHELF_Q), 2.0));
    double H = numerator/denominator;
    //NSLog(@"S = %f, num = %f, denom = %f, H = %f", S, numerator, denominator, H);
    return 20 * log10(H);
}

+ (double)eqHighShelf:(double)freq eqFreq:(double)eqFreq eqDb:(double)eqDb {
    double db = sqrt(pow(10,(eqDb/20)));
    double dbSqrt = sqrt(db);
    double S = freq/eqFreq;
    double numerator = db * sqrt(pow(1-db*pow(S, 2.0), 2.0) + pow(S*dbSqrt/LOW_HIGH_SHELF_Q, 2.0));
    double denominator = sqrt(pow(db-pow(S, 2.0), 2.0) + pow((S*dbSqrt/LOW_HIGH_SHELF_Q), 2.0));
    double H = numerator/denominator;
    //NSLog(@"S = %f, num = %f, denom = %f, H = %f", S, numerator, denominator, H);
    return 20 * log10(H);
}

+ (double)eqLPF:(double)freq eqFreq:(double)eqFreq {
    double S = freq/eqFreq;
    double numerator = 1.0;
    double denominator = sqrt(pow(1-pow(S, 2.0), 2.0) + pow(S/0.707, 2.0));
    double H = numerator/denominator;
    //NSLog(@"S = %f, num = %f, denom = %f, H = %f", S, numerator, denominator, H);
    return 20 * log10(H);
}

+ (double)eqHPF:(double)freq eqFreq:(double)eqFreq {
    double S = freq/eqFreq;
    double numerator = pow(S, 2.0);
    double denominator = sqrt(pow(1-pow(S, 2.0), 2.0) + pow(S/0.707, 2.0));
    double H = numerator/denominator;
    //NSLog(@"S = %f, num = %f, denom = %f, H = %f", S, numerator, denominator, H);
    return 20 * log10(H);
}

+ (int)getPeqFrequencyCount {
    return (int)[_eqFreqTableArray count];
}

+ (NSArray *)eqPeakData:(double)eqFreq eqDb:(double)eqDb eqQ:(double)eqQ {
    NSMutableArray *data = [[NSMutableArray alloc] initWithCapacity:[_eqFreqTableArray count]];    
    for (NSString* freq in _eqFreqTableArray) {
        double eqPeak = [self eqPeak:[freq floatValue] eqFreq:eqFreq eqDb:eqDb eqQ:eqQ];
        [data addObject:[NSNumber numberWithDouble:eqPeak]];
    }
    return data;
}

+ (NSArray *)eqLowShelfData:(double)eqFreq eqDb:(double)eqDb {
    NSMutableArray *data = [[NSMutableArray alloc] initWithCapacity:[_eqFreqTableArray count]];
    for (NSString* freq in _eqFreqTableArray) {
        double eqLowShelf = [self eqLowShelf:[freq floatValue] eqFreq:eqFreq eqDb:eqDb];
        [data addObject:[NSNumber numberWithDouble:eqLowShelf]];
    }
    return data;
}

+ (NSArray *)eqHighShelfData:(double)eqFreq eqDb:(double)eqDb {
    NSMutableArray *data = [[NSMutableArray alloc] initWithCapacity:[_eqFreqTableArray count]];
    for (NSString* freq in _eqFreqTableArray) {
        double eqHighShelf = [self eqHighShelf:[freq floatValue] eqFreq:eqFreq eqDb:eqDb];
        [data addObject:[NSNumber numberWithDouble:eqHighShelf]];
    }
    return data;
}

+ (NSArray *)eqLPFData:(double)eqFreq {
    NSMutableArray *data = [[NSMutableArray alloc] initWithCapacity:[_eqFreqTableArray count]];
    for (NSString* freq in _eqFreqTableArray) {
        double eqLPF = [self eqLPF:[freq floatValue] eqFreq:eqFreq];
        [data addObject:[NSNumber numberWithDouble:eqLPF]];
    }
    return data;
}

+ (NSArray *)eqHPFData:(double)eqFreq {
    NSMutableArray *data = [[NSMutableArray alloc] initWithCapacity:[_eqFreqTableArray count]];
    for (NSString* freq in _eqFreqTableArray) {
        double eqHPF = [self eqHPF:[freq floatValue] eqFreq:eqFreq];
        [data addObject:[NSNumber numberWithDouble:eqHPF]];
    }
    return data;
}

+ (NSArray *)dynGateOutput:(double)threshold range:(double)range {
    NSMutableArray *data = [[NSMutableArray alloc] initWithCapacity:DYN_INPUT_COUNT];
    for (int input=DYN_MIN_VALUE; input<=0; input++) {
        double temp = MAX(threshold, input) + 32 * (MIN(threshold, input) - threshold);
        [data addObject:[NSNumber numberWithDouble:input + MAX(temp - input, range)]];
    }
    return data;
}

+ (NSArray *)dynExpanderOutput:(double)threshold ratio:(double)ratio {
    NSMutableArray *data = [[NSMutableArray alloc] initWithCapacity:DYN_INPUT_COUNT];
    for (int input=DYN_MIN_VALUE; input<=0; input++) {
        [data addObject:[NSNumber numberWithDouble:MAX(threshold, input) + ratio * (MIN(threshold, input) - threshold)]];
    }
    return data;
}

+ (NSArray *)dynCompressorOutput:(double)threshold ratio:(double)ratio {
    NSMutableArray *data = [[NSMutableArray alloc] initWithCapacity:DYN_INPUT_COUNT];
    for (int input=DYN_MIN_VALUE; input<=0; input++) {
        [data addObject:[NSNumber numberWithDouble:MIN(threshold, input) + ratio * (MAX(threshold, input) - threshold)]];
    }
    return data;
}

+ (NSArray *)dynLimitOutput:(double)threshold {
    NSMutableArray *data = [[NSMutableArray alloc] initWithCapacity:DYN_INPUT_COUNT];
    for (int input=DYN_MIN_VALUE; input<=0; input++) {
        [data addObject:[NSNumber numberWithDouble:MIN(threshold, input)]];
    }
    return data;
}

+ (double)dynGateValue:(double)threshold range:(double)range {
    double input = threshold;
    double temp = MAX(threshold, input) + 32 * (MIN(threshold, input) - threshold);
    return input + MAX(temp - input, range);
}

+ (double)dynExpanderValue:(double)threshold ratio:(double)ratio {
    double input = threshold;
    return MAX(threshold, input) + ratio * (MIN(threshold, input) - threshold);
}

+ (double)dynCompressorValue:(double)threshold ratio:(double)ratio {
    double input = threshold;
    return MIN(threshold, input) + ratio * (MAX(threshold, input) - threshold);
}

+ (double)dynLimitValue:(double)threshold {
    return threshold;
}

+ (NSNumber *)dynGateOutputValue:(double)input threshold:(double)threshold range:(double)range {
    double temp = MAX(threshold, input) + 32 * (MIN(threshold, input) - threshold);
    return [NSNumber numberWithDouble:input + MAX(temp - input, range)];
}

+ (NSNumber *)dynExpanderOutputValue:(double)input threshold:(double)threshold ratio:(double)ratio {
    return [NSNumber numberWithDouble:MAX(threshold, input) + ratio * (MIN(threshold, input) - threshold)];
}

+ (NSNumber *)dynCompressorOutputValue:(double)input threshold:(double)threshold ratio:(double)ratio {
    return [NSNumber numberWithDouble:MIN(threshold, input) + ratio * (MAX(threshold, input) - threshold)];
}

+ (NSNumber *)dynLimitOutputValue:(double)input threshold:(double)threshold {
    return [NSNumber numberWithDouble:MIN(threshold, input)];
}

+ (NSString *)base64EncodedStringFromString:(NSString *)string {
    NSData *data = [NSData dataWithBytes:[string UTF8String] length:[string lengthOfBytesUsingEncoding:NSUTF8StringEncoding]];
    NSUInteger length = [data length];
    NSMutableData *mutableData = [NSMutableData dataWithLength:((length + 2) / 3) * 4];
    
    uint8_t *input = (uint8_t *)[data bytes];
    uint8_t *output = (uint8_t *)[mutableData mutableBytes];
    
    for (NSUInteger i = 0; i < length; i += 3) {
        NSUInteger value = 0;
        for (NSUInteger j = i; j < (i + 3); j++) {
            value <<= 8;
            if (j < length) {
                value |= (0xFF & input[j]);
            }
        }
        
        static uint8_t const kAFBase64EncodingTable[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
        
        NSUInteger idx = (i / 3) * 4;
        output[idx + 0] = kAFBase64EncodingTable[(value >> 18) & 0x3F];
        output[idx + 1] = kAFBase64EncodingTable[(value >> 12) & 0x3F];
        output[idx + 2] = (i + 1) < length ? kAFBase64EncodingTable[(value >> 6)  & 0x3F] : '=';
        output[idx + 3] = (i + 2) < length ? kAFBase64EncodingTable[(value >> 0)  & 0x3F] : '=';
    }
    
    return [[NSString alloc] initWithData:mutableData encoding:NSASCIIStringEncoding];
}

+ (unsigned char)crc:(unsigned char*)data len:(int)len {
    unsigned char crc = 0;
    if (data == nil || len <= 0) {
        return 0;
    }
    
    for (int i=0; i<len; i++) {
        crc ^= data[i];
    }
    return crc;
}

+ (BOOL)checkCrc:(unsigned char*)data len:(int)len pktCrc:(unsigned char)pktCrc {
    //unsigned char crc = [self crc:data len:len];
    //if (crc != pktCrc)
    //    return NO;
    return YES;
}

+ (void)setClearBit:(int *)value bitValue:(int)bitValue bitOrder:(int)bitOrder {
    if (value == nil)
        return;
    
    if (bitValue == 0) {
        // Clear bit
        *value &= ~(1 << bitOrder);
    } else {
        // Set bit
        *value |= (1 << bitOrder);
    }
}

+ (void)setOrder:(int *)value value:(int)order {
    if (value == nil) {
        return;
    }
    
    [self setClearBit:value bitValue:(order & 0x01) bitOrder:13];
    [self setClearBit:value bitValue:((order & 0x02) << 1) bitOrder:14];
    [self setClearBit:value bitValue:((order & 0x04) << 2) bitOrder:15];
}

+ (NSString *)getCurrentTime {
    NSDateFormatter *formatter;
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
    return [formatter stringFromDate:[NSDate date]];
}

@end
