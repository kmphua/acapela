//
//  DynViewController.m
//  Acapela
//
//  Created by Kevin Phua on 13/10/7.
//  Copyright (c) 2013年 Kevin Phua. All rights reserved.
//

#import "DynViewController.h"
#import "acapela.h"
#import "AppDelegate.h"
#import "ViewController.h"
#import "Global.h"

#define THRESHOLD_MAX               50      // 0dB
#define THRESHOLD_MIN               0       // -50dB
#define GATE_THRESHOLD_DEFAULT      0       // -50dB
#define EXPAND_THRESHOLD_DEFAULT    0       // -50dB
#define COMP_THRESHOLD_DEFAULT      40      // -10dB
#define LIMITER_THRESHOLD_DEFAULT   50      // 0dB

#define RANGE_MAX                   90      // 0 dB
#define RANGE_MIN                   0       // -90dB
#define RANGE_DEFAULT               40      // -50dB

#define ATTACK_RELEASE_MAX          39
#define ATTACK_RELEASE_MIN          0
#define ATTACK_DEFAULT              0       // 1ms
#define GATE_RELEASE_DEFAULT        0       // 1ms
#define EXPAND_RELEASE_DEFAULT      24      // 250ms
#define COMP_RELEASE_DEFAULT        24      // 250ms
#define LIMITER_RELEASE_DEFAULT     24      // 250ms

#define HOLD_MAX                    39
#define HOLD_MIN                    0
#define HOLD_DEFAULT                24      // 250ms

#define RATIO_MAX                   13
#define RATIO_MIN                   0
#define EXPAND_RATIO_DEFAULT        5       // 2:1
#define COMP_RATIO_DEFAULT          6       // 3:1

#define OUT_GAIN_MAX                36      // 0.5dB step
#define OUT_GAIN_MIN                0
#define OUT_GAIN_DEFAULT            0

#define DYN_FRAME_WIDTH             274.0
#define DYN_FRAME_HEIGHT            274.0

#define DYN_PLOT_WIDTH              5.0
#define THRESHOLD_PLOT_WIDTH        3.0

#define DYN_GR_OUT_OFFSET           1.015
#define TR_BALL_WIDTH               50

#define X_VAL                       @"x_value"
#define Y_VAL                       @"y_value"

@interface DynViewController ()

@end

static BOOL initDone = NO;

@implementation DynViewController
{
    ViewController* viewController;
    NSMutableArray* dynData;
    NSMutableArray* dynDataGate;
    NSMutableArray* dynDataExp;
    NSMutableArray* dynDataComp;
    NSMutableArray* dynDataLimit;
    NSMutableArray* thresData;
    double dynGateThreshold[CHANNEL_MAX], dynGateRange[CHANNEL_MAX];
    double dynExpThreshold[CHANNEL_MAX], dynExpRatio[CHANNEL_MAX];
    double dynCompThreshold[CHANNEL_MAX], dynCompRatio[CHANNEL_MAX];
    double dynLimitThreshold[CHANNEL_MAX];
    BOOL   dynGateEnabled[CHANNEL_MAX], dynExpEnabled[CHANNEL_MAX];
    BOOL   dynCompEnabled[CHANNEL_MAX], dynLimitEnabled[CHANNEL_MAX];
    BOOL   dynEnabled[CHANNEL_MAX];

    BOOL            _sliderLock;
    int             _ch1Ctrl, _ch2Ctrl, _ch3Ctrl, _ch4Ctrl;
    int             _ch5Ctrl, _ch6Ctrl, _ch7Ctrl, _ch8Ctrl;
    int             _ch9Ctrl, _ch10Ctrl, _ch11Ctrl, _ch12Ctrl;
    int             _ch13Ctrl, _ch14Ctrl, _ch15Ctrl, _ch16Ctrl;
    int             _ch17Ctrl, _ch18Ctrl, _multi1Ctrl, _multi2Ctrl;
    int             _multi3Ctrl, _multi4Ctrl, _mainCtrl;
    int             _soloCtrl, _chOnOff, _chMeterPrePost;
    int             _ch17Setting, _ch18Setting, _multiMeter, _mainMeter;
}

@synthesize imgPreview;
@synthesize currentChannel, currentChannelLabel;
@synthesize dynImages, dynPreviewImages;
@synthesize imgBackground,btnDyn,btnFile,btnReset;
@synthesize btnDynGate,btnDynExpander,btnDynComp,btnDynLimiter,btnDynModeOnOff;
@synthesize btnSelectDown,btnSelectUp,btnSolo,btnOn,lblPage,lblSliderValue;
@synthesize slider,meterL,meterR,btnMeterPrePost,txtPage;
@synthesize meterDynOutL,meterDynOutR,meterDynGrL,meterDynGrR;

@synthesize viewOutput,btnBallThreshold,btnBallRatio;

@synthesize viewGate,knobGateThreshold,cpvGateThreshold,lblGateThreshold;
@synthesize knobGateRange,cpvGateRange,lblGateRange;
@synthesize knobGateAttack,cpvGateAttack,lblGateAttack;
@synthesize knobGateHold,cpvGateHold,lblGateHold;
@synthesize knobGateRelease,cpvGateRelease,lblGateRelease;

@synthesize viewExpander,knobExpanderThreshold,cpvExpanderThreshold,lblExpanderThreshold;
@synthesize knobExpanderRatio,cpvExpanderRatio,lblExpanderRatio;
@synthesize knobExpanderAttack,cpvExpanderAttack,lblExpanderAttack;
@synthesize knobExpanderRelease,cpvExpanderRelease,lblExpanderRelease;

@synthesize viewComp,knobCompThreshold,cpvCompThreshold,lblCompThreshold;
@synthesize knobCompRatio,cpvCompRatio,lblCompRatio;
@synthesize knobCompOutputGain,cpvCompOutputGain,lblCompOutputGain;
@synthesize knobCompAttack,cpvCompAttack,lblCompAttack;
@synthesize knobCompRelease,cpvCompRelease,lblCompRelease;

@synthesize viewLimiter,knobLimiterThreshold,cpvLimiterThreshold,lblLimiterThreshold;
@synthesize knobLimiterAttack,cpvLimiterAttack,lblLimiterAttack;
@synthesize knobLimiterRelease,cpvLimiterRelease,lblLimiterRelease;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        dynData = [[NSMutableArray alloc] init];
        dynDataGate = [[NSMutableArray alloc] init];
        dynDataExp = [[NSMutableArray alloc] init];
        dynDataComp = [[NSMutableArray alloc] init];
        dynDataLimit = [[NSMutableArray alloc] init];
        dynImages = [[NSMutableArray alloc] initWithCapacity:CHANNEL_MAX];
        dynPreviewImages = [[NSMutableArray alloc] initWithCapacity:CHANNEL_MAX];
        thresData = [[NSMutableArray alloc] init];
        
        // Create blank image
        UIGraphicsBeginImageContextWithOptions(CGSizeMake(80, 80), NO, 0.0);
        UIImage *blankImage = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        for (int i=0; i<CHANNEL_MAX; i++) {
            dynGateThreshold[i] = GATE_THRESHOLD_DEFAULT;
            dynExpThreshold[i] = EXPAND_THRESHOLD_DEFAULT;
            dynCompThreshold[i] = COMP_THRESHOLD_DEFAULT;
            dynLimitThreshold[i] = LIMITER_THRESHOLD_DEFAULT;
            dynGateRange[i] = RANGE_DEFAULT;
            dynExpRatio[i] = EXPAND_RATIO_DEFAULT;
            dynCompRatio[i] = COMP_RATIO_DEFAULT;
            dynEnabled[i] = dynGateEnabled[i] = dynExpEnabled[i] = dynCompEnabled[i] = dynLimitEnabled[i] = NO;
            [dynImages addObject:[UIImage imageNamed:@"pan-8.png"]];
            [dynPreviewImages addObject:blankImage];
        }
        
        [self calculateDyn];
    }
    initDone = YES;
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    viewController = (ViewController *)appDelegate.window.rootViewController;
        
    txtPage.delegate = self;
    
    // Add main slider and meters
    UIImage *faderMainImage = [UIImage imageNamed:@"fader-3.png"];
    UIImage *progressImage = [[UIImage imageNamed:@"meter-1.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0,0,0,0)];
    UIImage *outProgressImage = [[UIImage imageNamed:@"meter-22.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0,0,0,0)];
    UIImage *grProgressImage = [[UIImage imageNamed:@"meter-21.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0,0,0,0)];
    
    slider = [[UISlider alloc] initWithFrame:CGRectMake(843, 295, 301, 180)];
    slider.transform = CGAffineTransformRotate(slider.transform, 270.0/180*M_PI);
    [slider addTarget:self action:@selector(onSliderBegin:) forControlEvents:UIControlEventTouchDown];
    [slider addTarget:self action:@selector(onSliderEnd:) forControlEvents:UIControlEventTouchUpInside];
    [slider addTarget:self action:@selector(onSliderEnd:) forControlEvents:UIControlEventTouchUpOutside];
    [slider addTarget:self action:@selector(onSliderAction:) forControlEvents:UIControlEventValueChanged];
    [slider setBackgroundColor:[UIColor clearColor]];
    [slider setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [slider setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [slider setThumbImage:faderMainImage forState:UIControlStateNormal];
    slider.minimumValue = 0.0;
    slider.maximumValue = FADER_MAX_VALUE;
    slider.continuous = YES;
    slider.value = FADER_DEFAULT_VALUE;
    [self.view addSubview:slider];
    
    meterDynOutL = [[JEProgressView alloc] initWithFrame:CGRectMake(488, 270, 275, 3)];
    meterDynOutL.transform = CGAffineTransformRotate(meterDynOutL.transform, 270.0/180*M_PI);
    [meterDynOutL setProgressImage:outProgressImage];
    [meterDynOutL setTrackImage:[UIImage alloc]];
    [meterDynOutL setProgress:0.0];
    [self.view addSubview:meterDynOutL];

    meterDynOutR = [[JEProgressView alloc] initWithFrame:CGRectMake(568, 270, 275, 3)];
    meterDynOutR.transform = CGAffineTransformRotate(meterDynOutR.transform, 270.0/180*M_PI);
    [meterDynOutR setProgressImage:outProgressImage];
    [meterDynOutR setTrackImage:[UIImage alloc]];
    [meterDynOutR setProgress:0.0];
    [self.view addSubview:meterDynOutR];
    
    meterDynGrL = [[JEProgressView alloc] initWithFrame:CGRectMake(603, 270, 275, 3)];
    meterDynGrL.transform = CGAffineTransformRotate(meterDynGrL.transform, 90.0/180*M_PI);
    [meterDynGrL setProgressImage:grProgressImage];
    [meterDynGrL setTrackImage:[UIImage alloc]];
    [meterDynGrL setProgress:0.0];
    [self.view addSubview:meterDynGrL];
    
    meterDynGrR = [[JEProgressView alloc] initWithFrame:CGRectMake(683, 270, 275, 3)];
    meterDynGrR.transform = CGAffineTransformRotate(meterDynGrR.transform, 90.0/180*M_PI);
    [meterDynGrR setProgressImage:grProgressImage];
    [meterDynGrR setTrackImage:[UIImage alloc]];
    [meterDynGrR setProgress:0.0];
    [self.view addSubview:meterDynGrR];
    
    meterL = [[JEProgressView alloc] initWithFrame:CGRectMake(795, 372, 275, 4)];
    meterL.transform = CGAffineTransformRotate(meterL.transform, 270.0/180*M_PI);
    [meterL setProgressImage:progressImage];
    [meterL setTrackImage:[UIImage alloc]];
    [meterL setProgress:0.0];
    [self.view addSubview:meterL];
    
    meterR = [[JEProgressView alloc] initWithFrame:CGRectMake(805, 372, 275, 4)];
    meterR.transform = CGAffineTransformRotate(meterR.transform, 270.0/180*M_PI);
    [meterR setProgressImage:progressImage];
    [meterR setTrackImage:[UIImage alloc]];
    [meterR setProgress:0.0];
    [self.view addSubview:meterR];
    
    // Add DYN knobs
    knobGateThreshold.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobGateThreshold.scalingFactor = 2.0f;
	knobGateThreshold.maximumValue = THRESHOLD_MAX;
	knobGateThreshold.minimumValue = THRESHOLD_MIN;
	knobGateThreshold.value = GATE_THRESHOLD_DEFAULT;
	knobGateThreshold.defaultValue = knobGateThreshold.value;
	knobGateThreshold.resetsToDefault = YES;
	knobGateThreshold.backgroundColor = [UIColor clearColor];
	[knobGateThreshold setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobGateThreshold.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobGateThreshold addTarget:self action:@selector(knobGateThresholdDidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvGateThreshold = [[DACircularProgressView alloc] initWithFrame:CGRectMake(8.0f, 30.0f, 65.0f, 65.0f)];
    [viewGate addSubview:cpvGateThreshold];
    [viewGate sendSubviewToBack:cpvGateThreshold];

    knobGateRange.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobGateRange.scalingFactor = 2.0f;
	knobGateRange.maximumValue = RANGE_MAX;
	knobGateRange.minimumValue = RANGE_MIN;
	knobGateRange.value = RANGE_DEFAULT;
	knobGateRange.defaultValue = knobGateRange.value;
	knobGateRange.resetsToDefault = YES;
	knobGateRange.backgroundColor = [UIColor clearColor];
	[knobGateRange setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobGateRange.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobGateRange addTarget:self action:@selector(knobGateRangeDidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvGateRange = [[DACircularProgressView alloc] initWithFrame:CGRectMake(148.0f, 30.0f, 65.0f, 65.0f)];
    [viewGate addSubview:cpvGateRange];
    [viewGate sendSubviewToBack:cpvGateRange];

    knobGateAttack.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobGateAttack.scalingFactor = 2.0f;
	knobGateAttack.maximumValue = ATTACK_RELEASE_MAX;
	knobGateAttack.minimumValue = ATTACK_RELEASE_MIN;
	knobGateAttack.value = ATTACK_DEFAULT;
	knobGateAttack.defaultValue = knobGateAttack.value;
	knobGateAttack.resetsToDefault = YES;
	knobGateAttack.backgroundColor = [UIColor clearColor];
	[knobGateAttack setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobGateAttack.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobGateAttack addTarget:self action:@selector(knobGateAttackDidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvGateAttack = [[DACircularProgressView alloc] initWithFrame:CGRectMake(288.0f, 30.0f, 65.0f, 65.0f)];
    [viewGate addSubview:cpvGateAttack];
    [viewGate sendSubviewToBack:cpvGateAttack];
    
    knobGateHold.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobGateHold.scalingFactor = 2.0f;
	knobGateHold.maximumValue = HOLD_MAX;
	knobGateHold.minimumValue = HOLD_MIN;
	knobGateHold.value = HOLD_DEFAULT;
	knobGateHold.defaultValue = knobGateHold.value;
	knobGateHold.resetsToDefault = YES;
	knobGateHold.backgroundColor = [UIColor clearColor];
	[knobGateHold setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobGateHold.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobGateHold addTarget:self action:@selector(knobGateHoldDidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvGateHold = [[DACircularProgressView alloc] initWithFrame:CGRectMake(428.0f, 30.0f, 65.0f, 65.0f)];
    [viewGate addSubview:cpvGateHold];
    [viewGate sendSubviewToBack:cpvGateHold];
    
    knobGateRelease.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobGateRelease.scalingFactor = 2.0f;
	knobGateRelease.maximumValue = ATTACK_RELEASE_MAX;
	knobGateRelease.minimumValue = ATTACK_RELEASE_MIN;
	knobGateRelease.value = GATE_RELEASE_DEFAULT;
	knobGateRelease.defaultValue = knobGateRelease.value;
	knobGateRelease.resetsToDefault = YES;
	knobGateRelease.backgroundColor = [UIColor clearColor];
	[knobGateRelease setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobGateRelease.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobGateRelease addTarget:self action:@selector(knobGateReleaseDidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvGateRelease = [[DACircularProgressView alloc] initWithFrame:CGRectMake(568.0f, 30.0f, 65.0f, 65.0f)];
    [viewGate addSubview:cpvGateRelease];
    [viewGate sendSubviewToBack:cpvGateRelease];
    
    knobExpanderThreshold.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobExpanderThreshold.scalingFactor = 2.0f;
	knobExpanderThreshold.maximumValue = THRESHOLD_MAX;
	knobExpanderThreshold.minimumValue = THRESHOLD_MIN;
	knobExpanderThreshold.value = EXPAND_THRESHOLD_DEFAULT;
	knobExpanderThreshold.defaultValue = knobExpanderThreshold.value;
	knobExpanderThreshold.resetsToDefault = YES;
	knobExpanderThreshold.backgroundColor = [UIColor clearColor];
	[knobExpanderThreshold setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobExpanderThreshold.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobExpanderThreshold addTarget:self action:@selector(knobExpanderThresholdDidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvExpanderThreshold = [[DACircularProgressView alloc] initWithFrame:CGRectMake(8.0f, 30.0f, 65.0f, 65.0f)];
    [viewExpander addSubview:cpvExpanderThreshold];
    [viewExpander sendSubviewToBack:cpvExpanderThreshold];
    
    knobExpanderRatio.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobExpanderRatio.scalingFactor = 2.0f;
	knobExpanderRatio.maximumValue = RATIO_MAX;
	knobExpanderRatio.minimumValue = RATIO_MIN;
	knobExpanderRatio.value = EXPAND_RATIO_DEFAULT;
	knobExpanderRatio.defaultValue = knobExpanderRatio.value;
	knobExpanderRatio.resetsToDefault = YES;
	knobExpanderRatio.backgroundColor = [UIColor clearColor];
	[knobExpanderRatio setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobExpanderRatio.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobExpanderRatio addTarget:self action:@selector(knobExpanderRatioDidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvExpanderRatio = [[DACircularProgressView alloc] initWithFrame:CGRectMake(148.0f, 30.0f, 65.0f, 65.0f)];
    [viewExpander addSubview:cpvExpanderRatio];
    [viewExpander sendSubviewToBack:cpvExpanderRatio];
    
    knobExpanderAttack.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobExpanderAttack.scalingFactor = 2.0f;
	knobExpanderAttack.maximumValue = ATTACK_RELEASE_MAX;
	knobExpanderAttack.minimumValue = ATTACK_RELEASE_MIN;
	knobExpanderAttack.value = ATTACK_DEFAULT;
	knobExpanderAttack.defaultValue = knobExpanderAttack.value;
	knobExpanderAttack.resetsToDefault = YES;
	knobExpanderAttack.backgroundColor = [UIColor clearColor];
	[knobExpanderAttack setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobExpanderAttack.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobExpanderAttack addTarget:self action:@selector(knobExpanderAttackDidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvExpanderAttack = [[DACircularProgressView alloc] initWithFrame:CGRectMake(288.0f, 30.0f, 65.0f, 65.0f)];
    [viewExpander addSubview:cpvExpanderAttack];
    [viewExpander sendSubviewToBack:cpvExpanderAttack];
    
    knobExpanderRelease.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobExpanderRelease.scalingFactor = 2.0f;
	knobExpanderRelease.maximumValue = ATTACK_RELEASE_MAX;
	knobExpanderRelease.minimumValue = ATTACK_RELEASE_MIN;
	knobExpanderRelease.value = EXPAND_RELEASE_DEFAULT;
	knobExpanderRelease.defaultValue = knobExpanderRelease.value;
	knobExpanderRelease.resetsToDefault = YES;
	knobExpanderRelease.backgroundColor = [UIColor clearColor];
	[knobExpanderRelease setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobExpanderRelease.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobExpanderRelease addTarget:self action:@selector(knobExpanderReleaseDidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvExpanderRelease = [[DACircularProgressView alloc] initWithFrame:CGRectMake(428.0f, 30.0f, 65.0f, 65.0f)];
    [viewExpander addSubview:cpvExpanderRelease];
    [viewExpander sendSubviewToBack:cpvExpanderRelease];
    
    knobCompThreshold.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobCompThreshold.scalingFactor = 2.0f;
	knobCompThreshold.maximumValue = THRESHOLD_MAX;
	knobCompThreshold.minimumValue = THRESHOLD_MIN;
	knobCompThreshold.value = COMP_THRESHOLD_DEFAULT;
	knobCompThreshold.defaultValue = knobCompThreshold.value;
	knobCompThreshold.resetsToDefault = YES;
	knobCompThreshold.backgroundColor = [UIColor clearColor];
	[knobCompThreshold setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobCompThreshold.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobCompThreshold addTarget:self action:@selector(knobCompThresholdDidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvCompThreshold = [[DACircularProgressView alloc] initWithFrame:CGRectMake(8.0f, 30.0f, 65.0f, 65.0f)];
    [viewComp addSubview:cpvCompThreshold];
    [viewComp sendSubviewToBack:cpvCompThreshold];
    
    knobCompRatio.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobCompRatio.scalingFactor = 2.0f;
	knobCompRatio.maximumValue = RATIO_MAX;
	knobCompRatio.minimumValue = RATIO_MIN;
	knobCompRatio.value = COMP_RATIO_DEFAULT;
	knobCompRatio.defaultValue = knobCompRatio.value;
	knobCompRatio.resetsToDefault = YES;
	knobCompRatio.backgroundColor = [UIColor clearColor];
	[knobCompRatio setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobCompRatio.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobCompRatio addTarget:self action:@selector(knobCompRatioDidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvCompRatio = [[DACircularProgressView alloc] initWithFrame:CGRectMake(148.0f, 30.0f, 65.0f, 65.0f)];
    [viewComp addSubview:cpvCompRatio];
    [viewComp sendSubviewToBack:cpvCompRatio];

    knobCompOutputGain.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobCompOutputGain.scalingFactor = 2.0f;
	knobCompOutputGain.maximumValue = OUT_GAIN_MAX;
	knobCompOutputGain.minimumValue = OUT_GAIN_MIN;
	knobCompOutputGain.value = OUT_GAIN_DEFAULT;
	knobCompOutputGain.defaultValue = knobCompOutputGain.value;
	knobCompOutputGain.resetsToDefault = YES;
	knobCompOutputGain.backgroundColor = [UIColor clearColor];
	[knobCompOutputGain setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobCompOutputGain.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobCompOutputGain addTarget:self action:@selector(knobCompOutputGainDidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvCompOutputGain = [[DACircularProgressView alloc] initWithFrame:CGRectMake(288.0f, 30.0f, 65.0f, 65.0f)];
    [viewComp addSubview:cpvCompOutputGain];
    [viewComp sendSubviewToBack:cpvCompOutputGain];
    
    knobCompAttack.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobCompAttack.scalingFactor = 2.0f;
	knobCompAttack.maximumValue = ATTACK_RELEASE_MAX;
	knobCompAttack.minimumValue = ATTACK_RELEASE_MIN;
	knobCompAttack.value = ATTACK_DEFAULT;
	knobCompAttack.defaultValue = knobCompAttack.value;
	knobCompAttack.resetsToDefault = YES;
	knobCompAttack.backgroundColor = [UIColor clearColor];
	[knobCompAttack setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobCompAttack.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobCompAttack addTarget:self action:@selector(knobCompAttackDidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvCompAttack = [[DACircularProgressView alloc] initWithFrame:CGRectMake(428.0f, 30.0f, 65.0f, 65.0f)];
    [viewComp addSubview:cpvCompAttack];
    [viewComp sendSubviewToBack:cpvCompAttack];
    
    knobCompRelease.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobCompRelease.scalingFactor = 2.0f;
	knobCompRelease.maximumValue = ATTACK_RELEASE_MAX;
	knobCompRelease.minimumValue = ATTACK_RELEASE_MIN;
	knobCompRelease.value = COMP_RELEASE_DEFAULT;
	knobCompRelease.defaultValue = knobCompRelease.value;
	knobCompRelease.resetsToDefault = YES;
	knobCompRelease.backgroundColor = [UIColor clearColor];
	[knobCompRelease setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobCompRelease.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobCompRelease addTarget:self action:@selector(knobCompReleaseDidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvCompRelease = [[DACircularProgressView alloc] initWithFrame:CGRectMake(568.0f, 30.0f, 65.0f, 65.0f)];
    [viewComp addSubview:cpvCompRelease];
    [viewComp sendSubviewToBack:cpvCompRelease];
    
    knobLimiterThreshold.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobLimiterThreshold.scalingFactor = 2.0f;
	knobLimiterThreshold.maximumValue = THRESHOLD_MAX;
	knobLimiterThreshold.minimumValue = THRESHOLD_MIN;
	knobLimiterThreshold.value = LIMITER_THRESHOLD_DEFAULT;
	knobLimiterThreshold.defaultValue = knobLimiterThreshold.value;
	knobLimiterThreshold.resetsToDefault = YES;
	knobLimiterThreshold.backgroundColor = [UIColor clearColor];
	[knobLimiterThreshold setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobLimiterThreshold.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobLimiterThreshold addTarget:self action:@selector(knobLimiterThresholdDidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvLimiterThreshold = [[DACircularProgressView alloc] initWithFrame:CGRectMake(8.0f, 30.0f, 65.0f, 65.0f)];
    [viewLimiter addSubview:cpvLimiterThreshold];
    [viewLimiter sendSubviewToBack:cpvLimiterThreshold];

    knobLimiterAttack.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobLimiterAttack.scalingFactor = 2.0f;
	knobLimiterAttack.maximumValue = ATTACK_RELEASE_MAX;
	knobLimiterAttack.minimumValue = ATTACK_RELEASE_MIN;
	knobLimiterAttack.value = ATTACK_DEFAULT;
	knobLimiterAttack.defaultValue = knobLimiterAttack.value;
	knobLimiterAttack.resetsToDefault = YES;
	knobLimiterAttack.backgroundColor = [UIColor clearColor];
	[knobLimiterAttack setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobLimiterAttack.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobLimiterAttack addTarget:self action:@selector(knobLimiterAttackDidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvLimiterAttack = [[DACircularProgressView alloc] initWithFrame:CGRectMake(148.0f, 30.0f, 65.0f, 65.0f)];
    [viewLimiter addSubview:cpvLimiterAttack];
    [viewLimiter sendSubviewToBack:cpvLimiterAttack];
    
    knobLimiterRelease.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobLimiterRelease.scalingFactor = 2.0f;
	knobLimiterRelease.maximumValue = ATTACK_RELEASE_MAX;
	knobLimiterRelease.minimumValue = ATTACK_RELEASE_MIN;
	knobLimiterRelease.value = LIMITER_RELEASE_DEFAULT;
	knobLimiterRelease.defaultValue = knobLimiterRelease.value;
	knobLimiterRelease.resetsToDefault = YES;
	knobLimiterRelease.backgroundColor = [UIColor clearColor];
	[knobLimiterRelease setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobLimiterRelease.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobLimiterRelease addTarget:self action:@selector(knobLimiterReleaseDidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvLimiterRelease = [[DACircularProgressView alloc] initWithFrame:CGRectMake(288.0f, 30.0f, 65.0f, 65.0f)];
    [viewLimiter addSubview:cpvLimiterRelease];
    [viewLimiter sendSubviewToBack:cpvLimiterRelease];
    
    _sliderLock = NO;
    [self onBtnGate:nil];
    
    // Init knobs and values
    [self knobGateThresholdDidChange:nil];
    [self knobGateRangeDidChange:nil];
    [self knobGateAttackDidChange:nil];
    [self knobGateHoldDidChange:nil];
    [self knobGateReleaseDidChange:nil];
    [self knobExpanderThresholdDidChange:nil];
    [self knobExpanderRatioDidChange:nil];
    [self knobExpanderAttackDidChange:nil];
    [self knobExpanderReleaseDidChange:nil];
    [self knobCompThresholdDidChange:nil];
    [self knobCompRatioDidChange:nil];
    [self knobCompOutputGainDidChange:nil];
    [self knobCompAttackDidChange:nil];
    [self knobCompReleaseDidChange:nil];
    [self knobLimiterThresholdDidChange:nil];
    [self knobLimiterAttackDidChange:nil];
    [self knobLimiterReleaseDidChange:nil];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self initPlot];
}

- (void)resetDynDefault {
    if (btnDynGate.selected) {
        dynGateThreshold[currentChannel] = knobGateThreshold.value = GATE_THRESHOLD_DEFAULT;
        dynGateRange[currentChannel] = knobGateRange.value = RANGE_DEFAULT;
        knobGateAttack.value = ATTACK_DEFAULT;
        knobGateHold.value = HOLD_DEFAULT;
        knobGateRelease.value = GATE_RELEASE_DEFAULT;
        [self knobGateThresholdDidChange:self];
        [self knobGateRangeDidChange:self];
        [self knobGateAttackDidChange:self];
        [self knobGateHoldDidChange:self];
        [self knobGateReleaseDidChange:self];
    } else if (btnDynExpander.selected) {
        knobExpanderThreshold.value = EXPAND_THRESHOLD_DEFAULT;
        knobExpanderRatio.value = EXPAND_RATIO_DEFAULT;
        knobExpanderAttack.value = ATTACK_DEFAULT;
        knobExpanderRelease.value = EXPAND_RELEASE_DEFAULT;
        [self knobExpanderThresholdDidChange:self];
        [self knobExpanderRatioDidChange:self];
        [self knobExpanderAttackDidChange:self];
        [self knobExpanderReleaseDidChange:self];
    } else if (btnDynComp.selected) {
        knobCompThreshold.value = COMP_THRESHOLD_DEFAULT;
        knobCompRatio.value = COMP_RATIO_DEFAULT;
        knobCompOutputGain.value = OUT_GAIN_DEFAULT;
        knobCompAttack.value = ATTACK_DEFAULT;
        knobCompRelease.value = COMP_RELEASE_DEFAULT;
        [self knobCompThresholdDidChange:self];
        [self knobCompRatioDidChange:self];
        [self knobCompOutputGainDidChange:self];
        [self knobCompAttackDidChange:self];
        [self knobCompReleaseDidChange:self];
    } else {
        knobLimiterThreshold.value = LIMITER_THRESHOLD_DEFAULT;
        knobLimiterAttack.value = ATTACK_DEFAULT;
        knobLimiterRelease.value = LIMITER_RELEASE_DEFAULT;
        [self knobLimiterThresholdDidChange:self];
        [self knobLimiterAttackDidChange:self];
        [self knobLimiterReleaseDidChange:self];
    }
}

- (void)updateDynFrame
{
    // Redraw graph
    [self calculateDyn];
    
    // Update threshold button positions
    if (btnDynGate.selected) {
        btnBallThreshold.center = CGPointMake([self dynThresholdToX:dynGateThreshold[currentChannel]], [self dynThresholdToY:dynGateThreshold[currentChannel]]);
    } else if (btnDynExpander.selected) {
        btnBallThreshold.center = CGPointMake([self dynThresholdToX:dynExpThreshold[currentChannel]], [self dynThresholdToY:dynExpThreshold[currentChannel]]);
        
        // Find input where output is 0
        double ratioX = -50.0;
        for (int i=0; i<[dynDataExp count]; i++) {
            NSNumber *val = [dynDataExp objectAtIndex:i];
            if ([val doubleValue] >= -THRESHOLD_MAX) {
                ratioX = i - THRESHOLD_MAX;
                break;
            }
        }
        
        btnBallRatio.center = CGPointMake([self dynThresholdToX:ratioX], DYN_FRAME_HEIGHT);
    } else if (btnDynComp.selected) {
        btnBallThreshold.center = CGPointMake([self dynThresholdToX:dynCompThreshold[currentChannel]], [self dynThresholdToY:dynCompThreshold[currentChannel]]);
        
        // Find output where input is 0
        NSNumber *ratioY = [dynDataComp objectAtIndex:[dynDataComp count]-1];
        btnBallRatio.center = CGPointMake(DYN_FRAME_WIDTH, [self dynThresholdToY:[ratioY doubleValue]]);
    } else {
        btnBallThreshold.center = CGPointMake([self dynThresholdToX:dynLimitThreshold[currentChannel]], [self dynThresholdToY:dynLimitThreshold[currentChannel]]);
    }
}

- (void)updateDynPlot:(BOOL)enabled
{
    CPTGraph *graph = self.hostView.hostedGraph;
    CPTScatterPlot *dynPlot = (CPTScatterPlot *)[graph plotAtIndex:1];
    if (enabled) {
        // Create styles and symbols
        CPTMutableLineStyle *dynLineStyle = [dynPlot.dataLineStyle mutableCopy];
        dynLineStyle.lineWidth = DYN_PLOT_WIDTH;
        dynLineStyle.lineColor = [CPTColor colorWithComponentRed:1.0 green:153.0/255.0 blue:0.0 alpha:1.0];
        dynPlot.dataLineStyle = dynLineStyle;
    } else {
        // Create styles and symbols
        CPTMutableLineStyle *dynLineStyle = [dynPlot.dataLineStyle mutableCopy];
        dynLineStyle.lineWidth = DYN_PLOT_WIDTH;
        dynLineStyle.lineColor = [CPTColor grayColor];
        dynPlot.dataLineStyle = dynLineStyle;
    }
}

- (void)updateThresPlot:(BOOL)enabled
{
    CPTGraph *graph = self.hostView.hostedGraph;
    CPTScatterPlot *thresPlot = (CPTScatterPlot *)[graph plotAtIndex:0];
    if (enabled) {
        // Create styles and symbols
        CPTMutableLineStyle *thresLineStyle = [thresPlot.dataLineStyle mutableCopy];
        thresLineStyle.lineWidth = THRESHOLD_PLOT_WIDTH;
        thresLineStyle.lineColor = [CPTColor colorWithComponentRed:0.0 green:0.0 blue:1.0 alpha:1.0];
        thresPlot.dataLineStyle = thresLineStyle;
    } else {
        // Create styles and symbols
        CPTMutableLineStyle *thresLineStyle = [thresPlot.dataLineStyle mutableCopy];
        thresLineStyle.lineWidth = THRESHOLD_PLOT_WIDTH;
        thresLineStyle.lineColor = [CPTColor colorWithComponentRed:0.0 green:0.0 blue:0.0 alpha:1.0];
        thresPlot.dataLineStyle = thresLineStyle;
    }
}

// Update image preview
- (void)updatePreviewImage
{
    if (!initDone) {
        NSLog(@"DYN updatePreviewImage: Init not done!");
        return;
    }
    
    // Hide threshold plot
    CPTGraph *graph = self.hostView.hostedGraph;
    CPTScatterPlot *thresPlot = (CPTScatterPlot *)[graph plotAtIndex:0];
    [thresPlot setHidden:YES];
    
    // Main view EQ preview image
    UIImage *baseImage = [UIImage imageNamed:@"pan-8.png"];
    UIImage *previewImage, *finalImage;
    
    UIGraphicsBeginImageContext(baseImage.size);
    [baseImage drawAtPoint:CGPointMake(0, 0)];
    
    previewImage = [Global imageWithView:self.hostView scaledToSize:CGSizeMake(70, 83)];
    [previewImage drawAtPoint:CGPointMake(2, 2)];
    
    finalImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    if (finalImage != nil) {
        [dynImages replaceObjectAtIndex:currentChannel withObject:finalImage];
        [imgPreview setImage:finalImage];
    } else {
        NSLog(@"DYN updatePreviewImage: NULL preview image!");
    }
    
    // View page EQ preview image
    UIGraphicsBeginImageContext(viewOutput.frame.size);
    
    [self.hostView.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    finalImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    if (finalImage != nil) {
        [dynPreviewImages replaceObjectAtIndex:currentChannel withObject:finalImage];
    } else {
        NSLog(@"DYN updatePreviewImage: NULL preview image!");
    }
    
    // Restore threshold plot
    [thresPlot setHidden:NO];
}

// Update image preview
- (void)updatePreviewImageForChannel:(int)channel
{
    if (!initDone) {
        NSLog(@"DYN updatePreviewImageForChannel: Init not done!");
        return;
    }
    
    NSLog(@"DYN updatePreviewImageForChannel for channel %d", channel);
    
    // Hide threshold plots
    CPTGraph *graph = self.hostView.hostedGraph;
    CPTScatterPlot *thresPlot = (CPTScatterPlot *)[graph plotAtIndex:0];
    [thresPlot setHidden:YES];
    
    UIImage *baseImage = [UIImage imageNamed:@"pan-8.png"];
    UIImage *previewImage, *finalImage;
    
    // Main view EQ preview image
    UIGraphicsBeginImageContext(baseImage.size);
    [baseImage drawAtPoint:CGPointMake(0, 0)];
    
    previewImage = [Global imageWithView:self.hostView scaledToSize:CGSizeMake(70, 83)];
    [previewImage drawAtPoint:CGPointMake(2, 2)];
    
    finalImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    if (finalImage != nil) {
        [dynImages replaceObjectAtIndex:channel withObject:finalImage];
    } else {
        NSLog(@"DYN updatePreviewImageForChannel: NULL preview image!");
    }
    
    // View page DYN preview image
    UIGraphicsBeginImageContext(viewOutput.frame.size);
    
    [self.hostView.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    finalImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    if (finalImage != nil) {
        [dynPreviewImages replaceObjectAtIndex:channel withObject:finalImage];
    } else {
        NSLog(@"DYN updatePreviewImageForChannel: NULL preview image!");
    }
    
    // Update delegate image
    [self.delegate didFinishEditingDyn];
    
    // Restore threshold plot
    [thresPlot setHidden:NO];
}

- (void)calculateDyn
{
    //NSLog(@"calculateDyn");
    
    [dynData removeAllObjects];
    [dynDataGate removeAllObjects];
    [dynDataExp removeAllObjects];
    [dynDataComp removeAllObjects];
    [dynDataLimit removeAllObjects];
    [thresData removeAllObjects];
    
    [dynDataGate addObjectsFromArray:[Global dynGateOutput:dynGateThreshold[currentChannel] range:dynGateRange[currentChannel]]];
    [dynDataExp addObjectsFromArray:[Global dynExpanderOutput:dynExpThreshold[currentChannel] ratio:dynExpRatio[currentChannel]]];
    [dynDataComp addObjectsFromArray:[Global dynCompressorOutput:dynCompThreshold[currentChannel] ratio:dynCompRatio[currentChannel]]];
    [dynDataLimit addObjectsFromArray:[Global dynLimitOutput:dynLimitThreshold[currentChannel]]];

    // Graph is sum of Gate, Exp, Comp & Limit output
    int count = (int)[dynDataGate count];
    for (int index=0; index<count; index++) {
        double sumOutput = -50.0 + index;
        
        // Draw linear graph if all DYN values are disabled
        if (!dynGateEnabled[currentChannel] && !dynExpEnabled[currentChannel] &&
            !dynCompEnabled[currentChannel] && !dynLimitEnabled[currentChannel]) {
            sumOutput = -50.0 + index;
        } else {
            NSNumber *outputGate = (NSNumber *)[dynDataGate objectAtIndex:index];
            NSNumber *outputExp = (NSNumber *)[dynDataExp objectAtIndex:index];
            NSNumber *outputComp = (NSNumber *)[dynDataComp objectAtIndex:index];
            NSNumber *outputLimit = (NSNumber *)[dynDataLimit objectAtIndex:index];
            
            if (dynGateEnabled[currentChannel]) {
                sumOutput = outputGate.doubleValue;
            }
            if (dynExpEnabled[currentChannel]) {
                if (dynGateEnabled[currentChannel]) {
                    // Use outputs from Gate as inputs
                    outputExp = [Global dynExpanderOutputValue:outputGate.doubleValue threshold:dynExpThreshold[currentChannel] ratio:dynExpRatio[currentChannel]];
                }
                sumOutput = outputExp.doubleValue;
            }
            if (dynCompEnabled[currentChannel]) {
                if (dynExpEnabled[currentChannel]) {
                    // Use outputs from Exp as inputs
                    outputComp = [Global dynCompressorOutputValue:outputExp.doubleValue threshold:dynCompThreshold[currentChannel] ratio:dynCompRatio[currentChannel]];
                } else {
                    if (dynGateEnabled[currentChannel]) {
                        // Use outputs from Gate as inputs
                        outputComp = [Global dynCompressorOutputValue:outputGate.doubleValue threshold:dynCompThreshold[currentChannel] ratio:dynCompRatio[currentChannel]];
                    }
                }
                sumOutput = outputComp.doubleValue;
            }
            if (dynLimitEnabled[currentChannel]) {
                if (dynCompEnabled[currentChannel]) {
                    // Use outputs from Comp as inputs
                    outputLimit = [Global dynLimitOutputValue:outputComp.doubleValue threshold:dynLimitThreshold[currentChannel]];
                } else {
                    if (dynExpEnabled[currentChannel]) {
                        // Use outputs from Exp as inputs
                        outputLimit = [Global dynLimitOutputValue:outputExp.doubleValue threshold:dynLimitThreshold[currentChannel]];
                    } else {
                        if (dynGateEnabled[currentChannel]) {
                            // Use outputs from Gate as inputs
                            outputLimit = [Global dynLimitOutputValue:outputGate.doubleValue threshold:dynLimitThreshold[currentChannel]];
                        }
                    }
                }
                sumOutput = outputLimit.doubleValue;
            }
        }
    
        [dynData addObject:[NSNumber numberWithDouble:sumOutput]];
    }
    
    // Calculate threshold data
    double xValue = 0.0;
    double yValue = 0.0;
    if (btnDynGate.selected) {
        xValue = dynGateThreshold[currentChannel] + THRESHOLD_MAX;
        yValue = [Global dynGateValue:dynGateThreshold[currentChannel] range:dynGateRange[currentChannel]];
        
        // Anchor to top right
        NSDictionary *sample = [NSDictionary dictionaryWithObjectsAndKeys:
                                [NSNumber numberWithDouble:50.0], X_VAL,
                                [NSNumber numberWithDouble:0.0], Y_VAL, nil];
        [thresData addObject:sample];
        
        sample = [NSDictionary dictionaryWithObjectsAndKeys:
                                [NSNumber numberWithDouble:xValue], X_VAL,
                                [NSNumber numberWithDouble:yValue], Y_VAL, nil];
        [thresData addObject:sample];
        
        sample = [NSDictionary dictionaryWithObjectsAndKeys:
                  [NSNumber numberWithDouble:xValue], X_VAL,
                  [NSNumber numberWithDouble:-50.0], Y_VAL, nil];
        [thresData addObject:sample];
    } else if (btnDynExpander.selected) {
        xValue = dynExpThreshold[currentChannel] + THRESHOLD_MAX;
        yValue = [Global dynExpanderValue:dynExpThreshold[currentChannel] ratio:dynExpRatio[currentChannel]];
        
        // Anchor to top right
        NSDictionary *sample = [NSDictionary dictionaryWithObjectsAndKeys:
                  [NSNumber numberWithDouble:50.0], X_VAL,
                  [NSNumber numberWithDouble:0.0], Y_VAL, nil];
        [thresData addObject:sample];
        
        sample = [NSDictionary dictionaryWithObjectsAndKeys:
                [NSNumber numberWithDouble:xValue], X_VAL,
                [NSNumber numberWithDouble:yValue], Y_VAL, nil];
        [thresData addObject:sample];
        
        // Find input where output is 0
        double ratioX = -50.0;
        for (int i=0; i<[dynDataExp count]; i++) {
            NSNumber *val = [dynDataExp objectAtIndex:i];
            if ([val doubleValue] >= -THRESHOLD_MAX) {
                ratioX = i;
                break;
            }
        }

        sample = [NSDictionary dictionaryWithObjectsAndKeys:
                  [NSNumber numberWithDouble:ratioX], X_VAL,
                  [NSNumber numberWithDouble:-50.0], Y_VAL, nil];
        [thresData addObject:sample];
    } else if (btnDynComp.selected) {
        xValue = dynCompThreshold[currentChannel] + THRESHOLD_MAX;
        yValue = [Global dynCompressorValue:dynCompThreshold[currentChannel] ratio:dynCompRatio[currentChannel]];
        
        // Anchor to bottom left
        NSDictionary *sample = [NSDictionary dictionaryWithObjectsAndKeys:
                  [NSNumber numberWithDouble:0.0], X_VAL,
                  [NSNumber numberWithDouble:-50.0], Y_VAL, nil];
        [thresData addObject:sample];
        
        sample = [NSDictionary dictionaryWithObjectsAndKeys:
                [NSNumber numberWithDouble:xValue], X_VAL,
                [NSNumber numberWithDouble:yValue], Y_VAL, nil];
        [thresData addObject:sample];
        
        // Find output where input is 0
        NSNumber *ratioY = [dynDataComp objectAtIndex:[dynDataComp count]-1];

        sample = [NSDictionary dictionaryWithObjectsAndKeys:
                  [NSNumber numberWithDouble:50.0], X_VAL,
                  ratioY, Y_VAL, nil];
        [thresData addObject:sample];
        
    } else {
        xValue = dynLimitThreshold[currentChannel] + THRESHOLD_MAX;
        yValue = [Global dynLimitValue:dynLimitThreshold[currentChannel]];
        
        // Anchor to bottom left
        NSDictionary *sample = [NSDictionary dictionaryWithObjectsAndKeys:
                                [NSNumber numberWithDouble:0.0], X_VAL,
                                [NSNumber numberWithDouble:-50.0], Y_VAL, nil];
        [thresData addObject:sample];
        
         sample = [NSDictionary dictionaryWithObjectsAndKeys:
                    [NSNumber numberWithDouble:xValue], X_VAL,
                    [NSNumber numberWithDouble:yValue], Y_VAL, nil];
        [thresData addObject:sample];
        
        sample = [NSDictionary dictionaryWithObjectsAndKeys:
                  [NSNumber numberWithDouble:50.0], X_VAL,
                  [NSNumber numberWithDouble:yValue], Y_VAL, nil];
        [thresData addObject:sample];
    }
    
    // Refresh graph
    [self.hostView.hostedGraph reloadData];
    
    // Refresh preview image
    if (initDone) {
        [self updatePreviewImage];
    }
}

- (void)setDynChannel:(ChannelType)channel {
    currentChannel = channel;
    [self updateChannelLabels];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)updateDynStatus:(int)value
{
    BOOL valueChanged = NO;
    
    btnDyn.selected = (value & 0x02) >> 1;
    if (btnDynGate.selected) {
        btnDynModeOnOff.selected = (value & 0x0100) >> 8;
    } else if (btnDynExpander.selected) {
        btnDynModeOnOff.selected = (value & 0x0200) >> 9;
    } else if (btnDynComp.selected) {
        btnDynModeOnOff.selected = (value & 0x0400) >> 10;
    } else if (btnDynLimiter.selected) {
        btnDynModeOnOff.selected = (value & 0x0800) >> 11;
    }
    
    int dynEnable = (value & 0x02) >> 1;
    if (dynEnabled[currentChannel] != dynEnable) {
        dynEnabled[currentChannel] = dynEnable;
        valueChanged = YES;
    }
    
    int dynGateEnable = (value & 0x0100) >> 8;
    if (dynGateEnabled[currentChannel] != dynGateEnable) {
        dynGateEnabled[currentChannel] = dynGateEnable;
        valueChanged = YES;
    }
    
    int dynExpEnable = (value & 0x0200) >> 9;
    if (dynExpEnabled[currentChannel] != dynExpEnable) {
        dynExpEnabled[currentChannel] = dynExpEnable;
        valueChanged = YES;
    }
    
    int dynCompEnable = (value & 0x0400) >> 10;
    if (dynCompEnabled[currentChannel] != dynCompEnable) {
        dynCompEnabled[currentChannel] = dynCompEnable;
        valueChanged = YES;
    }
    
    int dynLimitEnable = (value & 0x0800) >> 11;
    if (dynLimitEnabled[currentChannel] != dynLimitEnable) {
        dynLimitEnabled[currentChannel] = dynLimitEnable;
        valueChanged = YES;
    }
    
    [self updateDynPlot:dynEnable];
    [self updateThresPlot:dynEnable];
    
    if (valueChanged) {
        NSLog(@"updateDynStatus: value changed!");
        [self updateDynFrame];
    }
    
    return valueChanged;
}

- (void)processReply:(DynPagePacket *)pkt
{
    _ch1Ctrl = pkt->ch_1_ctrl;
    _ch2Ctrl = pkt->ch_2_ctrl;
    _ch3Ctrl = pkt->ch_3_ctrl;
    _ch4Ctrl = pkt->ch_4_ctrl;
    _ch5Ctrl = pkt->ch_5_ctrl;
    _ch6Ctrl = pkt->ch_6_ctrl;
    _ch7Ctrl = pkt->ch_7_ctrl;
    _ch8Ctrl = pkt->ch_8_ctrl;
    _ch9Ctrl = pkt->ch_9_ctrl;
    _ch10Ctrl = pkt->ch_10_ctrl;
    _ch11Ctrl = pkt->ch_11_ctrl;
    _ch12Ctrl = pkt->ch_12_ctrl;
    _ch13Ctrl = pkt->ch_13_ctrl;
    _ch14Ctrl = pkt->ch_14_ctrl;
    _ch15Ctrl = pkt->ch_15_ctrl;
    _ch16Ctrl = pkt->ch_16_ctrl;
    _ch17Ctrl = pkt->ch_17_ctrl;
    _ch18Ctrl = pkt->ch_18_ctrl;
    _multi1Ctrl = pkt->multi_1_ctrl;
    _multi2Ctrl = pkt->multi_2_ctrl;
    _multi3Ctrl = pkt->multi_3_ctrl;
    _multi4Ctrl = pkt->multi_4_ctrl;
    _mainCtrl = pkt->main_l_r_ctrl;
    _chOnOff = pkt->ch_on_off;
    _soloCtrl = pkt->solo_ctrl;
    _chMeterPrePost = pkt->ch_meter_pre_post;
    _ch17Setting = pkt->ch_17_setting;
    _ch18Setting = pkt->ch_18_setting;
    _multiMeter = pkt->multi_meter_pre_post;
    _mainMeter = pkt->main_meter_pre_post;
    
    switch (currentChannel) {
        case CHANNEL_CH_1:
            if (knobGateThreshold.value != pkt->ch_1_gate_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH1_GATE_THRESHOLD dspId:DSP_1]) {
                knobGateThreshold.value = pkt->ch_1_gate_threshold+THRESHOLD_MAX;
                [self knobGateThresholdDidChange:nil];
            }
            if (knobGateRange.value != pkt->ch_1_gate_range+RANGE_MAX &&
                ![viewController sendCommandExists:CMD_CH1_GATE_RANGE dspId:DSP_1]) {
                knobGateRange.value = pkt->ch_1_gate_range+RANGE_MAX;
                [self knobGateRangeDidChange:nil];
            }
            if (knobGateHold.value != pkt->ch_1_gate_hold  &&
                ![viewController sendCommandExists:CMD_CH1_GATE_HOLD dspId:DSP_1]) {
                knobGateHold.value = pkt->ch_1_gate_hold;
                [self knobGateHoldDidChange:nil];
            }
            if (knobGateAttack.value != pkt->ch_1_gate_attack &&
                ![viewController sendCommandExists:CMD_CH1_GATE_ATTACK dspId:DSP_1]) {
                knobGateAttack.value = pkt->ch_1_gate_attack;
                [self knobGateAttackDidChange:nil];
            }
            if (knobGateRelease.value != pkt->ch_1_gate_release &&
                ![viewController sendCommandExists:CMD_CH1_GATE_RELEASE dspId:DSP_1]) {
                knobGateRelease.value = pkt->ch_1_gate_release;
                [self knobGateReleaseDidChange:nil];
            }
            if (knobExpanderThreshold.value != pkt->ch_1_expand_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH1_EXPAND_THRESHOLD dspId:DSP_1]) {
                knobExpanderThreshold.value = pkt->ch_1_expand_threshold+THRESHOLD_MAX;
                [self knobExpanderThresholdDidChange:nil];
            }
            if (knobExpanderRatio.value != pkt->ch_1_expand_ratio &&
                ![viewController sendCommandExists:CMD_CH1_EXPAND_RATIO dspId:DSP_1]) {
                knobExpanderRatio.value = pkt->ch_1_expand_ratio;
                [self knobExpanderRatioDidChange:nil];
            }
            if (knobExpanderAttack.value != pkt->ch_1_expand_attack &&
                ![viewController sendCommandExists:CMD_CH1_EXPAND_ATTACK dspId:DSP_1]) {
                knobExpanderAttack.value = pkt->ch_1_expand_attack;
                [self knobExpanderAttackDidChange:nil];
            }
            if (knobExpanderRelease.value != pkt->ch_1_expand_release &&
                ![viewController sendCommandExists:CMD_CH1_EXPAND_RELEASE dspId:DSP_1]) {
                knobExpanderRelease.value = pkt->ch_1_expand_release;
                [self knobExpanderReleaseDidChange:nil];
            }
            if (knobCompThreshold.value != pkt->ch_1_compressor_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH1_COMPRESSOR_THRESHOLD dspId:DSP_1]) {
                knobCompThreshold.value = pkt->ch_1_compressor_threshold+THRESHOLD_MAX;
                [self knobCompThresholdDidChange:nil];
            }
            if (knobCompRatio.value != pkt->ch_1_compressor_ratio &&
                ![viewController sendCommandExists:CMD_CH1_COMPRESSOR_RATIO dspId:DSP_1]) {
                knobCompRatio.value = pkt->ch_1_compressor_ratio;
                [self knobCompRatioDidChange:nil];
            }
            if (knobCompOutputGain.value != pkt->ch_1_compressor_out_gain &&
                ![viewController sendCommandExists:CMD_CH1_COMPRESSOR_OUT_GAIN dspId:DSP_1]) {
                knobCompOutputGain.value = pkt->ch_1_compressor_out_gain;
                [self knobCompOutputGainDidChange:nil];
            }
            if (knobCompAttack.value != pkt->ch_1_compressor_attack &&
                ![viewController sendCommandExists:CMD_CH1_COMPRESSOR_ATTACK dspId:DSP_1]) {
                knobCompAttack.value = pkt->ch_1_compressor_attack;
                [self knobCompAttackDidChange:nil];
            }
            if (knobCompRelease.value != pkt->ch_1_compressor_release &&
                ![viewController sendCommandExists:CMD_CH1_COMPRESSOR_RELEASE dspId:DSP_1]) {
                knobCompRelease.value = pkt->ch_1_compressor_release;
                [self knobCompReleaseDidChange:nil];
            }
            if (knobLimiterThreshold.value != pkt->ch_1_limiter_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH1_LIMITER_THRESHOLD dspId:DSP_1]) {
                knobLimiterThreshold.value = pkt->ch_1_limiter_threshold+THRESHOLD_MAX;
                [self knobLimiterThresholdDidChange:nil];
            }
            if (knobLimiterAttack.value != pkt->ch_1_limiter_attack &&
                ![viewController sendCommandExists:CMD_CH1_LIMITER_ATTACK dspId:DSP_1]) {
                knobLimiterAttack.value = pkt->ch_1_limiter_attack;
                [self knobLimiterAttackDidChange:nil];
            }
            if (knobLimiterRelease.value != pkt->ch_1_limiter_release &&
                ![viewController sendCommandExists:CMD_CH1_LIMITER_RELEASE dspId:DSP_1]) {
                knobLimiterRelease.value = pkt->ch_1_limiter_release;
                [self knobLimiterReleaseDidChange:nil];
            }
            if (pkt->ch_1_fader >= 0.0 && pkt->ch_1_fader <= FADER_MAX_VALUE) {
                slider.value = pkt->ch_1_fader;
                [self onSliderAction:nil];
            }
            
            if (![viewController sendCommandExists:CMD_SOLO_CTRL dspId:DSP_5]) {
                btnSolo.selected = _soloCtrl & 0x01;
            }
            if (![viewController sendCommandExists:CMD_CH_ON_OFF dspId:DSP_5]) {
                btnOn.selected = pkt->ch_on_off & 0x01;
            }
            if (![viewController sendCommandExists:CMD_CH_METER dspId:DSP_5]) {
                btnMeterPrePost.selected = _chMeterPrePost & 0x01;
            }
            
            if (btnDynGate.selected) {
                meterDynOutR.progress = ((float)pkt->ch_1_meter_noise_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_1_noise_gain/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynExpander.selected) {
                meterDynOutR.progress = ((float)pkt->ch_1_meter_expand_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_1_meter_expand_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynComp.selected) {
                meterDynOutR.progress = ((float)pkt->ch_1_meter_compressor_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_1_meter_compressor_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynLimiter.selected) {
                meterDynOutR.progress = ((float)pkt->ch_1_meter_limit_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_1_meter_limit_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            }
            if (![viewController sendCommandExists:CMD_CH1_CTRL dspId:DSP_1]) {
                [self updateDynStatus:pkt->ch_1_ctrl];
            }
            break;
        case CHANNEL_CH_2:
            if (knobGateThreshold.value != pkt->ch_2_gate_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH2_GATE_THRESHOLD dspId:DSP_1]) {
                knobGateThreshold.value = pkt->ch_2_gate_threshold+THRESHOLD_MAX;
                [self knobGateThresholdDidChange:nil];
            }
            if (knobGateRange.value != pkt->ch_2_gate_range+RANGE_MAX &&
                ![viewController sendCommandExists:CMD_CH2_GATE_RANGE dspId:DSP_1]) {
                knobGateRange.value = pkt->ch_2_gate_range+RANGE_MAX;
                [self knobGateRangeDidChange:nil];
            }
            if (knobGateHold.value != pkt->ch_2_gate_hold &&
                ![viewController sendCommandExists:CMD_CH2_GATE_HOLD dspId:DSP_1]) {
                knobGateHold.value = pkt->ch_2_gate_hold;
                [self knobGateHoldDidChange:nil];
            }
            if (knobGateAttack.value != pkt->ch_2_gate_attack &&
                ![viewController sendCommandExists:CMD_CH2_GATE_ATTACK dspId:DSP_1]) {
                knobGateAttack.value = pkt->ch_2_gate_attack;
                [self knobGateAttackDidChange:nil];
            }
            if (knobGateRelease.value != pkt->ch_2_gate_release &&
                ![viewController sendCommandExists:CMD_CH2_GATE_RELEASE dspId:DSP_1]) {
                knobGateRelease.value = pkt->ch_2_gate_release;
                [self knobGateReleaseDidChange:nil];
            }
            if (knobExpanderThreshold.value != pkt->ch_2_expand_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH2_EXPAND_THRESHOLD dspId:DSP_1]) {
                knobExpanderThreshold.value = pkt->ch_2_expand_threshold+THRESHOLD_MAX;
                [self knobExpanderThresholdDidChange:nil];
            }
            if (knobExpanderRatio.value != pkt->ch_2_expand_ratio &&
                ![viewController sendCommandExists:CMD_CH2_EXPAND_RATIO dspId:DSP_1]) {
                knobExpanderRatio.value = pkt->ch_2_expand_ratio;
                [self knobExpanderRatioDidChange:nil];
            }
            if (knobExpanderAttack.value != pkt->ch_2_expand_attack &&
                ![viewController sendCommandExists:CMD_CH2_EXPAND_ATTACK dspId:DSP_1]) {
                knobExpanderAttack.value = pkt->ch_2_expand_attack;
                [self knobExpanderAttackDidChange:nil];
            }
            if (knobExpanderRelease.value != pkt->ch_2_expand_release &&
                ![viewController sendCommandExists:CMD_CH2_EXPAND_RELEASE dspId:DSP_1]) {
                knobExpanderRelease.value = pkt->ch_2_expand_release;
                [self knobExpanderReleaseDidChange:nil];
            }
            if (knobCompThreshold.value != pkt->ch_2_compressor_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH2_COMPRESSOR_THRESHOLD dspId:DSP_1]) {
                knobCompThreshold.value = pkt->ch_2_compressor_threshold+THRESHOLD_MAX;
                [self knobCompThresholdDidChange:nil];
            }
            if (knobCompRatio.value != pkt->ch_2_compressor_ratio &&
                ![viewController sendCommandExists:CMD_CH2_COMPRESSOR_RATIO dspId:DSP_1]) {
                knobCompRatio.value = pkt->ch_2_compressor_ratio;
                [self knobCompRatioDidChange:nil];
            }
            if (knobCompOutputGain.value != pkt->ch_2_compressor_out_gain &&
                ![viewController sendCommandExists:CMD_CH2_COMPRESSOR_OUT_GAIN dspId:DSP_1]) {
                knobCompOutputGain.value = pkt->ch_2_compressor_out_gain;
                [self knobCompOutputGainDidChange:nil];
            }
            if (knobCompAttack.value != pkt->ch_2_compressor_attack &&
                ![viewController sendCommandExists:CMD_CH2_COMPRESSOR_ATTACK dspId:DSP_1]) {
                knobCompAttack.value = pkt->ch_2_compressor_attack;
                [self knobCompAttackDidChange:nil];
            }
            if (knobCompRelease.value != pkt->ch_2_compressor_release &&
                ![viewController sendCommandExists:CMD_CH2_COMPRESSOR_RELEASE dspId:DSP_1]) {
                knobCompRelease.value = pkt->ch_2_compressor_release;
                [self knobCompReleaseDidChange:nil];
            }
            if (knobLimiterThreshold.value != pkt->ch_2_limiter_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH2_LIMITER_THRESHOLD dspId:DSP_1]) {
                knobLimiterThreshold.value = pkt->ch_2_limiter_threshold+THRESHOLD_MAX;
                [self knobLimiterThresholdDidChange:nil];
            }
            if (knobLimiterAttack.value != pkt->ch_2_limiter_attack &&
                ![viewController sendCommandExists:CMD_CH2_LIMITER_ATTACK dspId:DSP_1]) {
                knobLimiterAttack.value = pkt->ch_2_limiter_attack;
                [self knobLimiterAttackDidChange:nil];
            }
            if (knobLimiterRelease.value != pkt->ch_2_limiter_release &&
                ![viewController sendCommandExists:CMD_CH2_LIMITER_RELEASE dspId:DSP_1]) {
                knobLimiterRelease.value = pkt->ch_2_limiter_release;
                [self knobLimiterReleaseDidChange:nil];
            }
            if (pkt->ch_2_fader >= 0.0 && pkt->ch_2_fader <= FADER_MAX_VALUE) {
                slider.value = pkt->ch_2_fader;
                [self onSliderAction:nil];
            }
            
            if (![viewController sendCommandExists:CMD_SOLO_CTRL dspId:DSP_5]) {
                btnSolo.selected = (_soloCtrl & 0x02) >> 1;
            }
            if (![viewController sendCommandExists:CMD_CH_ON_OFF dspId:DSP_5]) {
                btnOn.selected = (pkt->ch_on_off & 0x02) >> 1;
            }
            if (![viewController sendCommandExists:CMD_CH_METER dspId:DSP_5]) {
                btnMeterPrePost.selected = (_chMeterPrePost & 0x02) >> 1;
            }
            
            if (btnDynGate.selected) {
                meterDynOutR.progress = ((float)pkt->ch_2_meter_noise_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_2_noise_gain/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynExpander.selected) {
                meterDynOutR.progress = ((float)pkt->ch_2_meter_expand_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_2_meter_expand_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynComp.selected) {
                meterDynOutR.progress = ((float)pkt->ch_2_meter_compressor_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_2_meter_compressor_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynLimiter.selected) {
                meterDynOutR.progress = ((float)pkt->ch_2_meter_limit_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_2_meter_limit_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            }
            if (![viewController sendCommandExists:CMD_CH2_CTRL dspId:DSP_1]) {
                [self updateDynStatus:pkt->ch_2_ctrl];
            }
            break;
        case CHANNEL_CH_3:
            if (knobGateThreshold.value != pkt->ch_3_gate_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH3_GATE_THRESHOLD dspId:DSP_1]) {
                knobGateThreshold.value = pkt->ch_3_gate_threshold+THRESHOLD_MAX;
                [self knobGateThresholdDidChange:nil];
            }
            if (knobGateRange.value != pkt->ch_3_gate_range+RANGE_MAX &&
                ![viewController sendCommandExists:CMD_CH3_GATE_RANGE dspId:DSP_1]) {
                knobGateRange.value = pkt->ch_3_gate_range+RANGE_MAX;
                [self knobGateRangeDidChange:nil];
            }
            if (knobGateHold.value != pkt->ch_3_gate_hold &&
                ![viewController sendCommandExists:CMD_CH3_GATE_HOLD dspId:DSP_1]) {
                knobGateHold.value = pkt->ch_3_gate_hold;
                [self knobGateHoldDidChange:nil];
            }
            if (knobGateAttack.value != pkt->ch_3_gate_attack &&
                ![viewController sendCommandExists:CMD_CH3_GATE_ATTACK dspId:DSP_1]) {
                knobGateAttack.value = pkt->ch_3_gate_attack;
                [self knobGateAttackDidChange:nil];
            }
            if (knobGateRelease.value != pkt->ch_3_gate_release &&
                ![viewController sendCommandExists:CMD_CH3_GATE_RELEASE dspId:DSP_1]) {
                knobGateRelease.value = pkt->ch_3_gate_release;
                [self knobGateReleaseDidChange:nil];
            }
            if (knobExpanderThreshold.value != pkt->ch_3_expand_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH3_EXPAND_THRESHOLD dspId:DSP_1]) {
                knobExpanderThreshold.value = pkt->ch_3_expand_threshold+THRESHOLD_MAX;
                [self knobExpanderThresholdDidChange:nil];
            }
            if (knobExpanderRatio.value != pkt->ch_3_expand_ratio &&
                ![viewController sendCommandExists:CMD_CH3_EXPAND_RATIO dspId:DSP_1]) {
                knobExpanderRatio.value = pkt->ch_3_expand_ratio;
                [self knobExpanderRatioDidChange:nil];
            }
            if (knobExpanderAttack.value != pkt->ch_3_expand_attack &&
                ![viewController sendCommandExists:CMD_CH3_EXPAND_ATTACK dspId:DSP_1]) {
                knobExpanderAttack.value = pkt->ch_3_expand_attack;
                [self knobExpanderAttackDidChange:nil];
            }
            if (knobExpanderRelease.value != pkt->ch_3_expand_release &&
                ![viewController sendCommandExists:CMD_CH3_EXPAND_RELEASE dspId:DSP_1]) {
                knobExpanderRelease.value = pkt->ch_3_expand_release;
                [self knobExpanderReleaseDidChange:nil];
            }
            if (knobCompThreshold.value != pkt->ch_3_compressor_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH3_COMPRESSOR_THRESHOLD dspId:DSP_1]) {
                knobCompThreshold.value = pkt->ch_3_compressor_threshold+THRESHOLD_MAX;
                [self knobCompThresholdDidChange:nil];
            }
            if (knobCompRatio.value != pkt->ch_3_compressor_ratio &&
                ![viewController sendCommandExists:CMD_CH3_COMPRESSOR_RATIO dspId:DSP_1]) {
                knobCompRatio.value = pkt->ch_3_compressor_ratio;
                [self knobCompRatioDidChange:nil];
            }
            if (knobCompOutputGain.value != pkt->ch_3_compressor_out_gain &&
                ![viewController sendCommandExists:CMD_CH3_COMPRESSOR_OUT_GAIN dspId:DSP_1]) {
                knobCompOutputGain.value = pkt->ch_3_compressor_out_gain;
                [self knobCompOutputGainDidChange:nil];
            }
            if (knobCompAttack.value != pkt->ch_3_compressor_attack &&
                ![viewController sendCommandExists:CMD_CH3_COMPRESSOR_ATTACK dspId:DSP_1]) {
                knobCompAttack.value = pkt->ch_3_compressor_attack;
                [self knobCompAttackDidChange:nil];
            }
            if (knobCompRelease.value != pkt->ch_3_compressor_release &&
                ![viewController sendCommandExists:CMD_CH3_COMPRESSOR_RELEASE dspId:DSP_1]) {
                knobCompRelease.value = pkt->ch_3_compressor_release;
                [self knobCompReleaseDidChange:nil];
            }
            if (knobLimiterThreshold.value != pkt->ch_3_limiter_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH3_LIMITER_THRESHOLD dspId:DSP_1]) {
                knobLimiterThreshold.value = pkt->ch_3_limiter_threshold+THRESHOLD_MAX;
                [self knobLimiterThresholdDidChange:nil];
            }
            if (knobLimiterAttack.value != pkt->ch_3_limiter_attack &&
                ![viewController sendCommandExists:CMD_CH3_LIMITER_ATTACK dspId:DSP_1]) {
                knobLimiterAttack.value = pkt->ch_3_limiter_attack;
                [self knobLimiterAttackDidChange:nil];
            }
            if (knobLimiterRelease.value != pkt->ch_3_limiter_release &&
                ![viewController sendCommandExists:CMD_CH3_LIMITER_RELEASE dspId:DSP_1]) {
                knobLimiterRelease.value = pkt->ch_3_limiter_release;
                [self knobLimiterReleaseDidChange:nil];
            }
            if (pkt->ch_3_fader >= 0.0 && pkt->ch_3_fader <= FADER_MAX_VALUE) {
                slider.value = pkt->ch_3_fader;
                [self onSliderAction:nil];
            }
            
            if (![viewController sendCommandExists:CMD_SOLO_CTRL dspId:DSP_5]) {
                btnSolo.selected = (_soloCtrl & 0x04) >> 2;
            }
            if (![viewController sendCommandExists:CMD_CH_ON_OFF dspId:DSP_5]) {
                btnOn.selected = (pkt->ch_on_off & 0x04) >> 2;
            }
            if (![viewController sendCommandExists:CMD_CH_METER dspId:DSP_5]) {
                btnMeterPrePost.selected = (_chMeterPrePost & 0x04) >> 2;
            }

            if (btnDynGate.selected) {
                meterDynOutR.progress = ((float)pkt->ch_3_meter_noise_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_3_noise_gain/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynExpander.selected) {
                meterDynOutR.progress = ((float)pkt->ch_3_meter_expand_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_3_meter_expand_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynComp.selected) {
                meterDynOutR.progress = ((float)pkt->ch_3_meter_compressor_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_3_meter_compressor_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynLimiter.selected) {
                meterDynOutR.progress = ((float)pkt->ch_3_meter_limit_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_3_meter_limit_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            }
            if (![viewController sendCommandExists:CMD_CH3_CTRL dspId:DSP_1]) {
                [self updateDynStatus:pkt->ch_3_ctrl];
            }
            break;
        case CHANNEL_CH_4:
            if (knobGateThreshold.value != pkt->ch_4_gate_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH4_GATE_THRESHOLD dspId:DSP_1]) {
                knobGateThreshold.value = pkt->ch_4_gate_threshold+THRESHOLD_MAX;
                [self knobGateThresholdDidChange:nil];
            }
            if (knobGateRange.value != pkt->ch_4_gate_range+RANGE_MAX &&
                ![viewController sendCommandExists:CMD_CH4_GATE_RANGE dspId:DSP_1]) {
                knobGateRange.value = pkt->ch_4_gate_range+RANGE_MAX;
                [self knobGateRangeDidChange:nil];
            }
            if (knobGateHold.value != pkt->ch_4_gate_hold &&
                ![viewController sendCommandExists:CMD_CH4_GATE_HOLD dspId:DSP_1]) {
                knobGateHold.value = pkt->ch_4_gate_hold;
                [self knobGateHoldDidChange:nil];
            }
            if (knobGateAttack.value != pkt->ch_4_gate_attack &&
                ![viewController sendCommandExists:CMD_CH4_GATE_ATTACK dspId:DSP_1]) {
                knobGateAttack.value = pkt->ch_4_gate_attack;
                [self knobGateAttackDidChange:nil];
            }
            if (knobGateRelease.value != pkt->ch_4_gate_release &&
                ![viewController sendCommandExists:CMD_CH4_GATE_RELEASE dspId:DSP_1]) {
                knobGateRelease.value = pkt->ch_4_gate_release;
                [self knobGateReleaseDidChange:nil];
            }
            if (knobExpanderThreshold.value != pkt->ch_4_expand_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH4_EXPAND_THRESHOLD dspId:DSP_1]) {
                knobExpanderThreshold.value = pkt->ch_4_expand_threshold+THRESHOLD_MAX;
                [self knobExpanderThresholdDidChange:nil];
            }
            if (knobExpanderRatio.value != pkt->ch_4_expand_ratio &&
                ![viewController sendCommandExists:CMD_CH4_EXPAND_RATIO dspId:DSP_1]) {
                knobExpanderRatio.value = pkt->ch_4_expand_ratio;
                [self knobExpanderRatioDidChange:nil];
            }
            if (knobExpanderAttack.value != pkt->ch_4_expand_attack &&
                ![viewController sendCommandExists:CMD_CH4_EXPAND_ATTACK dspId:DSP_1]) {
                knobExpanderAttack.value = pkt->ch_4_expand_attack;
                [self knobExpanderAttackDidChange:nil];
            }
            if (knobExpanderRelease.value != pkt->ch_4_expand_release &&
                ![viewController sendCommandExists:CMD_CH4_EXPAND_RELEASE dspId:DSP_1]) {
                knobExpanderRelease.value = pkt->ch_4_expand_release;
                [self knobExpanderReleaseDidChange:nil];
            }
            if (knobCompThreshold.value != pkt->ch_4_compressor_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH4_COMPRESSOR_THRESHOLD dspId:DSP_1]) {
                knobCompThreshold.value = pkt->ch_4_compressor_threshold+THRESHOLD_MAX;
                [self knobCompThresholdDidChange:nil];
            }
            if (knobCompRatio.value != pkt->ch_4_compressor_ratio &&
                ![viewController sendCommandExists:CMD_CH4_COMPRESSOR_RATIO dspId:DSP_1]) {
                knobCompRatio.value = pkt->ch_4_compressor_ratio;
                [self knobCompRatioDidChange:nil];
            }
            if (knobCompOutputGain.value != pkt->ch_4_compressor_out_gain &&
                ![viewController sendCommandExists:CMD_CH4_COMPRESSOR_OUT_GAIN dspId:DSP_1]) {
                knobCompOutputGain.value = pkt->ch_4_compressor_out_gain;
                [self knobCompOutputGainDidChange:nil];
            }
            if (knobCompAttack.value != pkt->ch_4_compressor_attack &&
                ![viewController sendCommandExists:CMD_CH4_COMPRESSOR_ATTACK dspId:DSP_1]) {
                knobCompAttack.value = pkt->ch_4_compressor_attack;
                [self knobCompAttackDidChange:nil];
            }
            if (knobCompRelease.value != pkt->ch_4_compressor_release &&
                ![viewController sendCommandExists:CMD_CH4_COMPRESSOR_RELEASE dspId:DSP_1]) {
                knobCompRelease.value = pkt->ch_4_compressor_release;
                [self knobCompReleaseDidChange:nil];
            }
            if (knobLimiterThreshold.value != pkt->ch_4_limiter_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH4_LIMITER_THRESHOLD dspId:DSP_1]) {
                knobLimiterThreshold.value = pkt->ch_4_limiter_threshold+THRESHOLD_MAX;
                [self knobLimiterThresholdDidChange:nil];
            }
            if (knobLimiterAttack.value != pkt->ch_4_limiter_attack &&
                ![viewController sendCommandExists:CMD_CH4_LIMITER_ATTACK dspId:DSP_1]) {
                knobLimiterAttack.value = pkt->ch_4_limiter_attack;
                [self knobLimiterAttackDidChange:nil];
            }
            if (knobLimiterRelease.value != pkt->ch_4_limiter_release &&
                ![viewController sendCommandExists:CMD_CH4_LIMITER_RELEASE dspId:DSP_1]) {
                knobLimiterRelease.value = pkt->ch_4_limiter_release;
                [self knobLimiterReleaseDidChange:nil];
            }
            if (pkt->ch_4_fader >= 0.0 && pkt->ch_4_fader <= FADER_MAX_VALUE) {
                slider.value = pkt->ch_4_fader;
                [self onSliderAction:nil];
            }
            
            if (![viewController sendCommandExists:CMD_SOLO_CTRL dspId:DSP_5]) {
                btnSolo.selected = (_soloCtrl & 0x08) >> 3;
            }
            if (![viewController sendCommandExists:CMD_CH_ON_OFF dspId:DSP_5]) {
                btnOn.selected = (pkt->ch_on_off & 0x08) >> 3;
            }
            if (![viewController sendCommandExists:CMD_CH_METER dspId:DSP_5]) {
                btnMeterPrePost.selected = (_chMeterPrePost & 0x08) >> 3;
            }
            
            if (btnDynGate.selected) {
                meterDynOutR.progress = ((float)pkt->ch_4_meter_noise_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_4_noise_gain/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynExpander.selected) {
                meterDynOutR.progress = ((float)pkt->ch_4_meter_expand_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_4_meter_expand_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynComp.selected) {
                meterDynOutR.progress = ((float)pkt->ch_4_meter_compressor_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_4_meter_compressor_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynLimiter.selected) {
                meterDynOutR.progress = ((float)pkt->ch_4_meter_limit_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_4_meter_limit_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            }
            if (![viewController sendCommandExists:CMD_CH4_CTRL dspId:DSP_1]) {
                [self updateDynStatus:pkt->ch_4_ctrl];
            }
            break;
        case CHANNEL_CH_5:
            if (knobGateThreshold.value != pkt->ch_5_gate_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH1_GATE_THRESHOLD dspId:DSP_2]) {
                knobGateThreshold.value = pkt->ch_5_gate_threshold+THRESHOLD_MAX;
                [self knobGateThresholdDidChange:nil];
            }
            if (knobGateRange.value != pkt->ch_5_gate_range+RANGE_MAX &&
                ![viewController sendCommandExists:CMD_CH1_GATE_RANGE dspId:DSP_2]) {
                knobGateRange.value = pkt->ch_5_gate_range+RANGE_MAX;
                [self knobGateRangeDidChange:nil];
            }
            if (knobGateHold.value != pkt->ch_5_gate_hold &&
                ![viewController sendCommandExists:CMD_CH1_GATE_HOLD dspId:DSP_2]) {
                knobGateHold.value = pkt->ch_5_gate_hold;
                [self knobGateHoldDidChange:nil];
            }
            if (knobGateAttack.value != pkt->ch_5_gate_attack &&
                ![viewController sendCommandExists:CMD_CH1_GATE_ATTACK dspId:DSP_2]) {
                knobGateAttack.value = pkt->ch_5_gate_attack;
                [self knobGateAttackDidChange:nil];
            }
            if (knobGateRelease.value != pkt->ch_5_gate_release &&
                ![viewController sendCommandExists:CMD_CH1_GATE_RELEASE dspId:DSP_2]) {
                knobGateRelease.value = pkt->ch_5_gate_release;
                [self knobGateReleaseDidChange:nil];
            }
            if (knobExpanderThreshold.value != pkt->ch_5_expand_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH1_EXPAND_THRESHOLD dspId:DSP_2]) {
                knobExpanderThreshold.value = pkt->ch_5_expand_threshold+THRESHOLD_MAX;
                [self knobExpanderThresholdDidChange:nil];
            }
            if (knobExpanderRatio.value != pkt->ch_5_expand_ratio &&
                ![viewController sendCommandExists:CMD_CH1_EXPAND_RATIO dspId:DSP_2]) {
                knobExpanderRatio.value = pkt->ch_5_expand_ratio;
                [self knobExpanderRatioDidChange:nil];
            }
            if (knobExpanderAttack.value != pkt->ch_5_expand_attack &&
                ![viewController sendCommandExists:CMD_CH1_EXPAND_ATTACK dspId:DSP_2]) {
                knobExpanderAttack.value = pkt->ch_5_expand_attack;
                [self knobExpanderAttackDidChange:nil];
            }
            if (knobExpanderRelease.value != pkt->ch_5_expand_release &&
                ![viewController sendCommandExists:CMD_CH1_EXPAND_RELEASE dspId:DSP_2]) {
                knobExpanderRelease.value = pkt->ch_5_expand_release;
                [self knobExpanderReleaseDidChange:nil];
            }
            if (knobCompThreshold.value != pkt->ch_5_compressor_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH1_COMPRESSOR_THRESHOLD dspId:DSP_2]) {
                knobCompThreshold.value = pkt->ch_5_compressor_threshold+THRESHOLD_MAX;
                [self knobCompThresholdDidChange:nil];
            }
            if (knobCompRatio.value != pkt->ch_5_compressor_ratio &&
                ![viewController sendCommandExists:CMD_CH1_COMPRESSOR_RATIO dspId:DSP_2]) {
                knobCompRatio.value = pkt->ch_5_compressor_ratio;
                [self knobCompRatioDidChange:nil];
            }
            if (knobCompOutputGain.value != pkt->ch_5_compressor_out_gain &&
                ![viewController sendCommandExists:CMD_CH1_COMPRESSOR_OUT_GAIN dspId:DSP_2]) {
                knobCompOutputGain.value = pkt->ch_5_compressor_out_gain;
                [self knobCompOutputGainDidChange:nil];
            }
            if (knobCompAttack.value != pkt->ch_5_compressor_attack &&
                ![viewController sendCommandExists:CMD_CH1_COMPRESSOR_ATTACK dspId:DSP_2]) {
                knobCompAttack.value = pkt->ch_5_compressor_attack;
                [self knobCompAttackDidChange:nil];
            }
            if (knobCompRelease.value != pkt->ch_5_compressor_release &&
                ![viewController sendCommandExists:CMD_CH1_COMPRESSOR_RELEASE dspId:DSP_2]) {
                knobCompRelease.value = pkt->ch_5_compressor_release;
                [self knobCompReleaseDidChange:nil];
            }
            if (knobLimiterThreshold.value != pkt->ch_5_limiter_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH1_LIMITER_THRESHOLD dspId:DSP_2]) {
                knobLimiterThreshold.value = pkt->ch_5_limiter_threshold+THRESHOLD_MAX;
                [self knobLimiterThresholdDidChange:nil];
            }
            if (knobLimiterAttack.value != pkt->ch_5_limiter_attack &&
                ![viewController sendCommandExists:CMD_CH1_LIMITER_ATTACK dspId:DSP_2]) {
                knobLimiterAttack.value = pkt->ch_5_limiter_attack;
                [self knobLimiterAttackDidChange:nil];
            }
            if (knobLimiterRelease.value != pkt->ch_5_limiter_release &&
                ![viewController sendCommandExists:CMD_CH1_LIMITER_RELEASE dspId:DSP_2]) {
                knobLimiterRelease.value = pkt->ch_5_limiter_release;
                [self knobLimiterReleaseDidChange:nil];
            }
            if (pkt->ch_5_fader >= 0.0 && pkt->ch_5_fader <= FADER_MAX_VALUE) {
                slider.value = pkt->ch_5_fader;
                [self onSliderAction:nil];
            }
            
            if (![viewController sendCommandExists:CMD_SOLO_CTRL dspId:DSP_5]) {
                btnSolo.selected = (_soloCtrl & 0x10) >> 4;
            }
            if (![viewController sendCommandExists:CMD_CH_ON_OFF dspId:DSP_5]) {
                btnOn.selected = (pkt->ch_on_off & 0x10) >> 4;
            }
            if (![viewController sendCommandExists:CMD_CH_METER dspId:DSP_5]) {
                btnMeterPrePost.selected = (_chMeterPrePost & 0x10) >> 4;
            }
            
            if (btnDynGate.selected) {
                meterDynOutR.progress = ((float)pkt->ch_5_meter_noise_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_5_noise_gain/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynExpander.selected) {
                meterDynOutR.progress = ((float)pkt->ch_5_meter_expand_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_5_meter_expand_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynComp.selected) {
                meterDynOutR.progress = ((float)pkt->ch_5_meter_compressor_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_5_meter_compressor_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynLimiter.selected) {
                meterDynOutR.progress = ((float)pkt->ch_5_meter_limit_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_5_meter_limit_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            }
            if (![viewController sendCommandExists:CMD_CH5_CTRL dspId:DSP_2]) {
                [self updateDynStatus:pkt->ch_5_ctrl];
            }
            break;
        case CHANNEL_CH_6:
            if (knobGateThreshold.value != pkt->ch_6_gate_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH2_GATE_THRESHOLD dspId:DSP_2]) {
                knobGateThreshold.value = pkt->ch_6_gate_threshold+THRESHOLD_MAX;
                [self knobGateThresholdDidChange:nil];
            }
            if (knobGateRange.value != pkt->ch_6_gate_range+RANGE_MAX &&
                ![viewController sendCommandExists:CMD_CH2_GATE_RANGE dspId:DSP_2]) {
                knobGateRange.value = pkt->ch_6_gate_range+RANGE_MAX;
                [self knobGateRangeDidChange:nil];
            }
            if (knobGateHold.value != pkt->ch_6_gate_hold &&
                ![viewController sendCommandExists:CMD_CH2_GATE_HOLD dspId:DSP_2]) {
                knobGateHold.value = pkt->ch_6_gate_hold;
                [self knobGateHoldDidChange:nil];
            }
            if (knobGateAttack.value != pkt->ch_6_gate_attack &&
                ![viewController sendCommandExists:CMD_CH2_GATE_ATTACK dspId:DSP_2]) {
                knobGateAttack.value = pkt->ch_6_gate_attack;
                [self knobGateAttackDidChange:nil];
            }
            if (knobGateRelease.value != pkt->ch_6_gate_release &&
                ![viewController sendCommandExists:CMD_CH2_GATE_RELEASE dspId:DSP_2]) {
                knobGateRelease.value = pkt->ch_6_gate_release;
                [self knobGateReleaseDidChange:nil];
            }
            if (knobExpanderThreshold.value != pkt->ch_6_expand_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH2_EXPAND_THRESHOLD dspId:DSP_2]) {
                knobExpanderThreshold.value = pkt->ch_6_expand_threshold+THRESHOLD_MAX;
                [self knobExpanderThresholdDidChange:nil];
            }
            if (knobExpanderRatio.value != pkt->ch_6_expand_ratio &&
                ![viewController sendCommandExists:CMD_CH2_EXPAND_RATIO dspId:DSP_2]) {
                knobExpanderRatio.value = pkt->ch_6_expand_ratio;
                [self knobExpanderRatioDidChange:nil];
            }
            if (knobExpanderAttack.value != pkt->ch_6_expand_attack &&
                ![viewController sendCommandExists:CMD_CH2_EXPAND_ATTACK dspId:DSP_2]) {
                knobExpanderAttack.value = pkt->ch_6_expand_attack;
                [self knobExpanderAttackDidChange:nil];
            }
            if (knobExpanderRelease.value != pkt->ch_6_expand_release &&
                ![viewController sendCommandExists:CMD_CH2_EXPAND_RELEASE dspId:DSP_2]) {
                knobExpanderRelease.value = pkt->ch_6_expand_release;
                [self knobExpanderReleaseDidChange:nil];
            }
            if (knobCompThreshold.value != pkt->ch_6_compressor_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH2_COMPRESSOR_THRESHOLD dspId:DSP_2]) {
                knobCompThreshold.value = pkt->ch_6_compressor_threshold+THRESHOLD_MAX;
                [self knobCompThresholdDidChange:nil];
            }
            if (knobCompRatio.value != pkt->ch_6_compressor_ratio &&
                ![viewController sendCommandExists:CMD_CH2_COMPRESSOR_RATIO dspId:DSP_2]) {
                knobCompRatio.value = pkt->ch_6_compressor_ratio;
                [self knobCompRatioDidChange:nil];
            }
            if (knobCompOutputGain.value != pkt->ch_6_compressor_out_gain &&
                ![viewController sendCommandExists:CMD_CH2_COMPRESSOR_OUT_GAIN dspId:DSP_2]) {
                knobCompOutputGain.value = pkt->ch_6_compressor_out_gain;
                [self knobCompOutputGainDidChange:nil];
            }
            if (knobCompAttack.value != pkt->ch_6_compressor_attack &&
                ![viewController sendCommandExists:CMD_CH2_COMPRESSOR_ATTACK dspId:DSP_2]) {
                knobCompAttack.value = pkt->ch_6_compressor_attack;
                [self knobCompAttackDidChange:nil];
            }
            if (knobCompRelease.value != pkt->ch_6_compressor_release &&
                ![viewController sendCommandExists:CMD_CH2_COMPRESSOR_RELEASE dspId:DSP_2]) {
                knobCompRelease.value = pkt->ch_6_compressor_release;
                [self knobCompReleaseDidChange:nil];
            }
            if (knobLimiterThreshold.value != pkt->ch_6_limiter_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH2_LIMITER_THRESHOLD dspId:DSP_2]) {
                knobLimiterThreshold.value = pkt->ch_6_limiter_threshold+THRESHOLD_MAX;
                [self knobLimiterThresholdDidChange:nil];
            }
            if (knobLimiterAttack.value != pkt->ch_6_limiter_attack &&
                ![viewController sendCommandExists:CMD_CH2_LIMITER_ATTACK dspId:DSP_2]) {
                knobLimiterAttack.value = pkt->ch_6_limiter_attack;
                [self knobLimiterAttackDidChange:nil];
            }
            if (knobLimiterRelease.value != pkt->ch_6_limiter_release &&
                ![viewController sendCommandExists:CMD_CH2_LIMITER_RELEASE dspId:DSP_2]) {
                knobLimiterRelease.value = pkt->ch_6_limiter_release;
                [self knobLimiterReleaseDidChange:nil];
            }
            if (pkt->ch_6_fader >= 0.0 && pkt->ch_6_fader <= FADER_MAX_VALUE) {
                slider.value = pkt->ch_6_fader;
                [self onSliderAction:nil];
            }
            
            if (![viewController sendCommandExists:CMD_SOLO_CTRL dspId:DSP_5]) {
                btnSolo.selected = (_soloCtrl & 0x20) >> 5;
            }
            if (![viewController sendCommandExists:CMD_CH_ON_OFF dspId:DSP_5]) {
                btnOn.selected = (pkt->ch_on_off & 0x20) >> 5;
            }
            if (![viewController sendCommandExists:CMD_CH_METER dspId:DSP_5]) {
                btnMeterPrePost.selected = (_chMeterPrePost & 0x20) >> 5;
            }
            
            if (btnDynGate.selected) {
                meterDynOutR.progress = ((float)pkt->ch_6_meter_noise_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_6_noise_gain/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynExpander.selected) {
                meterDynOutR.progress = ((float)pkt->ch_6_meter_expand_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_6_meter_expand_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynComp.selected) {
                meterDynOutR.progress = ((float)pkt->ch_6_meter_compressor_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_6_meter_compressor_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynLimiter.selected) {
                meterDynOutR.progress = ((float)pkt->ch_6_meter_limit_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_6_meter_limit_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            }
            if (![viewController sendCommandExists:CMD_CH6_CTRL dspId:DSP_2]) {
                [self updateDynStatus:pkt->ch_6_ctrl];
            }
            break;
        case CHANNEL_CH_7:
            if (knobGateThreshold.value != pkt->ch_7_gate_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH3_GATE_THRESHOLD dspId:DSP_2]) {
                knobGateThreshold.value = pkt->ch_7_gate_threshold+THRESHOLD_MAX;
                [self knobGateThresholdDidChange:nil];
            }
            if (knobGateRange.value != pkt->ch_7_gate_range+RANGE_MAX &&
                ![viewController sendCommandExists:CMD_CH3_GATE_RANGE dspId:DSP_2]) {
                knobGateRange.value = pkt->ch_7_gate_range+RANGE_MAX;
                [self knobGateRangeDidChange:nil];
            }
            if (knobGateHold.value != pkt->ch_7_gate_hold &&
                ![viewController sendCommandExists:CMD_CH3_GATE_HOLD dspId:DSP_2]) {
                knobGateHold.value = pkt->ch_7_gate_hold;
                [self knobGateHoldDidChange:nil];
            }
            if (knobGateAttack.value != pkt->ch_7_gate_attack &&
                ![viewController sendCommandExists:CMD_CH3_GATE_ATTACK dspId:DSP_2]) {
                knobGateAttack.value = pkt->ch_7_gate_attack;
                [self knobGateAttackDidChange:nil];
            }
            if (knobGateRelease.value != pkt->ch_7_gate_release &&
                ![viewController sendCommandExists:CMD_CH3_GATE_RELEASE dspId:DSP_2]) {
                knobGateRelease.value = pkt->ch_7_gate_release;
                [self knobGateReleaseDidChange:nil];
            }
            if (knobExpanderThreshold.value != pkt->ch_7_expand_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH3_EXPAND_THRESHOLD dspId:DSP_2]) {
                knobExpanderThreshold.value = pkt->ch_7_expand_threshold+THRESHOLD_MAX;
                [self knobExpanderThresholdDidChange:nil];
            }
            if (knobExpanderRatio.value != pkt->ch_7_expand_ratio &&
                ![viewController sendCommandExists:CMD_CH3_EXPAND_RATIO dspId:DSP_2]) {
                knobExpanderRatio.value = pkt->ch_7_expand_ratio;
                [self knobExpanderRatioDidChange:nil];
            }
            if (knobExpanderAttack.value != pkt->ch_7_expand_attack &&
                ![viewController sendCommandExists:CMD_CH3_EXPAND_ATTACK dspId:DSP_2]) {
                knobExpanderAttack.value = pkt->ch_7_expand_attack;
                [self knobExpanderAttackDidChange:nil];
            }
            if (knobExpanderRelease.value != pkt->ch_7_expand_release &&
                ![viewController sendCommandExists:CMD_CH3_EXPAND_RELEASE dspId:DSP_2]) {
                knobExpanderRelease.value = pkt->ch_7_expand_release;
                [self knobExpanderReleaseDidChange:nil];
            }
            if (knobCompThreshold.value != pkt->ch_7_compressor_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH3_COMPRESSOR_THRESHOLD dspId:DSP_2]) {
                knobCompThreshold.value = pkt->ch_7_compressor_threshold+THRESHOLD_MAX;
                [self knobCompThresholdDidChange:nil];
            }
            if (knobCompRatio.value != pkt->ch_7_compressor_ratio &&
                ![viewController sendCommandExists:CMD_CH3_COMPRESSOR_RATIO dspId:DSP_2]) {
                knobCompRatio.value = pkt->ch_7_compressor_ratio;
                [self knobCompRatioDidChange:nil];
            }
            if (knobCompOutputGain.value != pkt->ch_7_compressor_out_gain &&
                ![viewController sendCommandExists:CMD_CH3_COMPRESSOR_OUT_GAIN dspId:DSP_2]) {
                knobCompOutputGain.value = pkt->ch_7_compressor_out_gain;
                [self knobCompOutputGainDidChange:nil];
            }
            if (knobCompAttack.value != pkt->ch_7_compressor_attack &&
                ![viewController sendCommandExists:CMD_CH3_COMPRESSOR_ATTACK dspId:DSP_2]) {
                knobCompAttack.value = pkt->ch_7_compressor_attack;
                [self knobCompAttackDidChange:nil];
            }
            if (knobCompRelease.value != pkt->ch_7_compressor_release &&
                ![viewController sendCommandExists:CMD_CH3_COMPRESSOR_RELEASE dspId:DSP_2]) {
                knobCompRelease.value = pkt->ch_7_compressor_release;
                [self knobCompReleaseDidChange:nil];
            }
            if (knobLimiterThreshold.value != pkt->ch_7_limiter_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH3_LIMITER_THRESHOLD dspId:DSP_2]) {
                knobLimiterThreshold.value = pkt->ch_7_limiter_threshold+THRESHOLD_MAX;
                [self knobLimiterThresholdDidChange:nil];
            }
            if (knobLimiterAttack.value != pkt->ch_7_limiter_attack &&
                ![viewController sendCommandExists:CMD_CH3_LIMITER_ATTACK dspId:DSP_2]) {
                knobLimiterAttack.value = pkt->ch_7_limiter_attack;
                [self knobLimiterAttackDidChange:nil];
            }
            if (knobLimiterRelease.value != pkt->ch_7_limiter_release &&
                ![viewController sendCommandExists:CMD_CH3_LIMITER_RELEASE dspId:DSP_2]) {
                knobLimiterRelease.value = pkt->ch_7_limiter_release;
                [self knobLimiterReleaseDidChange:nil];
            }
            if (pkt->ch_7_fader >= 0.0 && pkt->ch_7_fader <= FADER_MAX_VALUE) {
                slider.value = pkt->ch_7_fader;
                [self onSliderAction:nil];
            }
            
            if (![viewController sendCommandExists:CMD_SOLO_CTRL dspId:DSP_5]) {
                btnSolo.selected = (_soloCtrl & 0x40) >> 6;
            }
            if (![viewController sendCommandExists:CMD_CH_ON_OFF dspId:DSP_5]) {
                btnOn.selected = (pkt->ch_on_off & 0x40) >> 6;
            }
            if (![viewController sendCommandExists:CMD_CH_METER dspId:DSP_5]) {
                btnMeterPrePost.selected = (_chMeterPrePost & 0x40) >> 6;
            }

            if (btnDynGate.selected) {
                meterDynOutR.progress = ((float)pkt->ch_7_meter_noise_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_7_noise_gain/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynExpander.selected) {
                meterDynOutR.progress = ((float)pkt->ch_7_meter_expand_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_7_meter_expand_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynComp.selected) {
                meterDynOutR.progress = ((float)pkt->ch_7_meter_compressor_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_7_meter_compressor_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynLimiter.selected) {
                meterDynOutR.progress = ((float)pkt->ch_7_meter_limit_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_7_meter_limit_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            }
            if (![viewController sendCommandExists:CMD_CH7_CTRL dspId:DSP_2]) {
                [self updateDynStatus:pkt->ch_7_ctrl];
            }
            break;
        case CHANNEL_CH_8:
            if (knobGateThreshold.value != pkt->ch_8_gate_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH4_GATE_THRESHOLD dspId:DSP_2]) {
                knobGateThreshold.value = pkt->ch_8_gate_threshold+THRESHOLD_MAX;
                [self knobGateThresholdDidChange:nil];
            }
            if (knobGateRange.value != pkt->ch_8_gate_range+RANGE_MAX &&
                ![viewController sendCommandExists:CMD_CH4_GATE_RANGE dspId:DSP_2]) {
                knobGateRange.value = pkt->ch_8_gate_range+RANGE_MAX;
                [self knobGateRangeDidChange:nil];
            }
            if (knobGateHold.value != pkt->ch_8_gate_hold &&
                ![viewController sendCommandExists:CMD_CH4_GATE_HOLD dspId:DSP_2]) {
                knobGateHold.value = pkt->ch_8_gate_hold;
                [self knobGateHoldDidChange:nil];
            }
            if (knobGateAttack.value != pkt->ch_8_gate_attack &&
                ![viewController sendCommandExists:CMD_CH4_GATE_ATTACK dspId:DSP_2]) {
                knobGateAttack.value = pkt->ch_8_gate_attack;
                [self knobGateAttackDidChange:nil];
            }
            if (knobGateRelease.value != pkt->ch_8_gate_release &&
                ![viewController sendCommandExists:CMD_CH4_GATE_RELEASE dspId:DSP_2]) {
                knobGateRelease.value = pkt->ch_8_gate_release;
                [self knobGateReleaseDidChange:nil];
            }
            if (knobExpanderThreshold.value != pkt->ch_8_expand_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH4_EXPAND_THRESHOLD dspId:DSP_2]) {
                knobExpanderThreshold.value = pkt->ch_8_expand_threshold+THRESHOLD_MAX;
                [self knobExpanderThresholdDidChange:nil];
            }
            if (knobExpanderRatio.value != pkt->ch_8_expand_ratio &&
                ![viewController sendCommandExists:CMD_CH4_EXPAND_RATIO dspId:DSP_2]) {
                knobExpanderRatio.value = pkt->ch_8_expand_ratio;
                [self knobExpanderRatioDidChange:nil];
            }
            if (knobExpanderAttack.value != pkt->ch_8_expand_attack &&
                ![viewController sendCommandExists:CMD_CH4_EXPAND_ATTACK dspId:DSP_2]) {
                knobExpanderAttack.value = pkt->ch_8_expand_attack;
                [self knobExpanderAttackDidChange:nil];
            }
            if (knobExpanderRelease.value != pkt->ch_8_expand_release &&
                ![viewController sendCommandExists:CMD_CH4_EXPAND_RELEASE dspId:DSP_2]) {
                knobExpanderRelease.value = pkt->ch_8_expand_release;
                [self knobExpanderReleaseDidChange:nil];
            }
            if (knobCompThreshold.value != pkt->ch_8_compressor_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH4_COMPRESSOR_THRESHOLD dspId:DSP_2]) {
                knobCompThreshold.value = pkt->ch_8_compressor_threshold+THRESHOLD_MAX;
                [self knobCompThresholdDidChange:nil];
            }
            if (knobCompRatio.value != pkt->ch_8_compressor_ratio &&
                ![viewController sendCommandExists:CMD_CH4_COMPRESSOR_RATIO dspId:DSP_2]) {
                knobCompRatio.value = pkt->ch_8_compressor_ratio;
                [self knobCompRatioDidChange:nil];
            }
            if (knobCompOutputGain.value != pkt->ch_8_compressor_out_gain &&
                ![viewController sendCommandExists:CMD_CH4_COMPRESSOR_OUT_GAIN dspId:DSP_2]) {
                knobCompOutputGain.value = pkt->ch_8_compressor_out_gain;
                [self knobCompOutputGainDidChange:nil];
            }
            if (knobCompAttack.value != pkt->ch_8_compressor_attack &&
                ![viewController sendCommandExists:CMD_CH4_COMPRESSOR_ATTACK dspId:DSP_2]) {
                knobCompAttack.value = pkt->ch_8_compressor_attack;
                [self knobCompAttackDidChange:nil];
            }
            if (knobCompRelease.value != pkt->ch_8_compressor_release &&
                ![viewController sendCommandExists:CMD_CH4_COMPRESSOR_RELEASE dspId:DSP_2]) {
                knobCompRelease.value = pkt->ch_8_compressor_release;
                [self knobCompReleaseDidChange:nil];
            }
            if (knobLimiterThreshold.value != pkt->ch_8_limiter_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH4_LIMITER_THRESHOLD dspId:DSP_2]) {
                knobLimiterThreshold.value = pkt->ch_8_limiter_threshold+THRESHOLD_MAX;
                [self knobLimiterThresholdDidChange:nil];
            }
            if (knobLimiterAttack.value != pkt->ch_8_limiter_attack &&
                ![viewController sendCommandExists:CMD_CH4_LIMITER_ATTACK dspId:DSP_2]) {
                knobLimiterAttack.value = pkt->ch_8_limiter_attack;
                [self knobLimiterAttackDidChange:nil];
            }
            if (knobLimiterRelease.value != pkt->ch_8_limiter_release &&
                ![viewController sendCommandExists:CMD_CH4_LIMITER_RELEASE dspId:DSP_2]) {
                knobLimiterRelease.value = pkt->ch_8_limiter_release;
                [self knobLimiterReleaseDidChange:nil];
            }
            if (pkt->ch_8_fader >= 0.0 && pkt->ch_8_fader <= FADER_MAX_VALUE) {
                slider.value = pkt->ch_8_fader;
                [self onSliderAction:nil];
            }
            
            if (![viewController sendCommandExists:CMD_SOLO_CTRL dspId:DSP_5]) {
                btnSolo.selected = (_soloCtrl & 0x80) >> 7;
            }
            if (![viewController sendCommandExists:CMD_CH_ON_OFF dspId:DSP_5]) {
                btnOn.selected = (pkt->ch_on_off & 0x80) >> 7;
            }
            if (![viewController sendCommandExists:CMD_CH_METER dspId:DSP_5]) {
                btnMeterPrePost.selected = (_chMeterPrePost & 0x80) >> 7;
            }

            if (btnDynGate.selected) {
                meterDynOutR.progress = ((float)pkt->ch_8_meter_noise_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_8_noise_gain/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynExpander.selected) {
                meterDynOutR.progress = ((float)pkt->ch_8_meter_expand_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_8_meter_expand_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynComp.selected) {
                meterDynOutR.progress = ((float)pkt->ch_8_meter_compressor_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_8_meter_compressor_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynLimiter.selected) {
                meterDynOutR.progress = ((float)pkt->ch_8_meter_limit_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_8_meter_limit_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            }
            if (![viewController sendCommandExists:CMD_CH8_CTRL dspId:DSP_2]) {
                [self updateDynStatus:pkt->ch_8_ctrl];
            }
            break;
        case CHANNEL_CH_9:
            if (knobGateThreshold.value != pkt->ch_9_gate_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH1_GATE_THRESHOLD dspId:DSP_3]) {
                knobGateThreshold.value = pkt->ch_9_gate_threshold+THRESHOLD_MAX;
                [self knobGateThresholdDidChange:nil];
            }
            if (knobGateRange.value != pkt->ch_9_gate_range+RANGE_MAX &&
                ![viewController sendCommandExists:CMD_CH1_GATE_RANGE dspId:DSP_3]) {
                knobGateRange.value = pkt->ch_9_gate_range+RANGE_MAX;
                [self knobGateRangeDidChange:nil];
            }
            if (knobGateHold.value != pkt->ch_9_gate_hold &&
                ![viewController sendCommandExists:CMD_CH1_GATE_HOLD dspId:DSP_3]) {
                knobGateHold.value = pkt->ch_9_gate_hold;
                [self knobGateHoldDidChange:nil];
            }
            if (knobGateAttack.value != pkt->ch_9_gate_attack &&
                ![viewController sendCommandExists:CMD_CH1_GATE_ATTACK dspId:DSP_3]) {
                knobGateAttack.value = pkt->ch_9_gate_attack;
                [self knobGateAttackDidChange:nil];
            }
            if (knobGateRelease.value != pkt->ch_9_gate_release &&
                ![viewController sendCommandExists:CMD_CH1_GATE_RELEASE dspId:DSP_3]) {
                knobGateRelease.value = pkt->ch_9_gate_release;
                [self knobGateReleaseDidChange:nil];
            }
            if (knobExpanderThreshold.value != pkt->ch_9_expand_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH1_EXPAND_THRESHOLD dspId:DSP_3]) {
                knobExpanderThreshold.value = pkt->ch_9_expand_threshold+THRESHOLD_MAX;
                [self knobExpanderThresholdDidChange:nil];
            }
            if (knobExpanderRatio.value != pkt->ch_9_expand_ratio &&
                ![viewController sendCommandExists:CMD_CH1_EXPAND_RATIO dspId:DSP_3]) {
                knobExpanderRatio.value = pkt->ch_9_expand_ratio;
                [self knobExpanderRatioDidChange:nil];
            }
            if (knobExpanderAttack.value != pkt->ch_9_expand_attack &&
                ![viewController sendCommandExists:CMD_CH1_EXPAND_ATTACK dspId:DSP_3]) {
                knobExpanderAttack.value = pkt->ch_9_expand_attack;
                [self knobExpanderAttackDidChange:nil];
            }
            if (knobExpanderRelease.value != pkt->ch_9_expand_release &&
                ![viewController sendCommandExists:CMD_CH1_EXPAND_RELEASE dspId:DSP_3]) {
                knobExpanderRelease.value = pkt->ch_9_expand_release;
                [self knobExpanderReleaseDidChange:nil];
            }
            if (knobCompThreshold.value != pkt->ch_9_compressor_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH1_COMPRESSOR_THRESHOLD dspId:DSP_3]) {
                knobCompThreshold.value = pkt->ch_9_compressor_threshold+THRESHOLD_MAX;
                [self knobCompThresholdDidChange:nil];
            }
            if (knobCompRatio.value != pkt->ch_9_compressor_ratio &&
                ![viewController sendCommandExists:CMD_CH1_COMPRESSOR_RATIO dspId:DSP_3]) {
                knobCompRatio.value = pkt->ch_9_compressor_ratio;
                [self knobCompRatioDidChange:nil];
            }
            if (knobCompOutputGain.value != pkt->ch_9_compressor_out_gain &&
                ![viewController sendCommandExists:CMD_CH1_COMPRESSOR_OUT_GAIN dspId:DSP_3]) {
                knobCompOutputGain.value = pkt->ch_9_compressor_out_gain;
                [self knobCompOutputGainDidChange:nil];
            }
            if (knobCompAttack.value != pkt->ch_9_compressor_attack &&
                ![viewController sendCommandExists:CMD_CH1_COMPRESSOR_ATTACK dspId:DSP_3]) {
                knobCompAttack.value = pkt->ch_9_compressor_attack;
                [self knobCompAttackDidChange:nil];
            }
            if (knobCompRelease.value != pkt->ch_9_compressor_release &&
                ![viewController sendCommandExists:CMD_CH1_COMPRESSOR_RELEASE dspId:DSP_3]) {
                knobCompRelease.value = pkt->ch_9_compressor_release;
                [self knobCompReleaseDidChange:nil];
            }
            if (knobLimiterThreshold.value != pkt->ch_9_limiter_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH1_LIMITER_THRESHOLD dspId:DSP_3]) {
                knobLimiterThreshold.value = pkt->ch_9_limiter_threshold+THRESHOLD_MAX;
                [self knobLimiterThresholdDidChange:nil];
            }
            if (knobLimiterAttack.value != pkt->ch_9_limiter_attack &&
                ![viewController sendCommandExists:CMD_CH1_LIMITER_ATTACK dspId:DSP_3]) {
                knobLimiterAttack.value = pkt->ch_9_limiter_attack;
                [self knobLimiterAttackDidChange:nil];
            }
            if (knobLimiterRelease.value != pkt->ch_9_limiter_release &&
                ![viewController sendCommandExists:CMD_CH1_LIMITER_RELEASE dspId:DSP_3]) {
                knobLimiterRelease.value = pkt->ch_9_limiter_release;
                [self knobLimiterReleaseDidChange:nil];
            }
            if (pkt->ch_9_fader >= 0.0 && pkt->ch_9_fader <= FADER_MAX_VALUE) {
                slider.value = pkt->ch_9_fader;
                [self onSliderAction:nil];
            }
            
            if (![viewController sendCommandExists:CMD_SOLO_CTRL dspId:DSP_5]) {
                btnSolo.selected = (_soloCtrl & 0x0100) >> 8;
            }
            if (![viewController sendCommandExists:CMD_CH_ON_OFF dspId:DSP_5]) {
                btnOn.selected = (pkt->ch_on_off & 0x0100) >> 8;
            }
            if (![viewController sendCommandExists:CMD_CH_METER dspId:DSP_5]) {
                btnMeterPrePost.selected = (_chMeterPrePost & 0x0100) >> 8;
            }

            if (btnDynGate.selected) {
                meterDynOutR.progress = ((float)pkt->ch_9_meter_noise_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_9_noise_gain/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynExpander.selected) {
                meterDynOutR.progress = ((float)pkt->ch_9_meter_expand_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_9_meter_expand_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynComp.selected) {
                meterDynOutR.progress = ((float)pkt->ch_9_meter_compressor_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_9_meter_compressor_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynLimiter.selected) {
                meterDynOutR.progress = ((float)pkt->ch_9_meter_limit_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_9_meter_limit_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            }
            if (![viewController sendCommandExists:CMD_CH9_CTRL dspId:DSP_3]) {
                [self updateDynStatus:pkt->ch_9_ctrl];
            }
            break;
        case CHANNEL_CH_10:
            if (knobGateThreshold.value != pkt->ch_10_gate_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH2_GATE_THRESHOLD dspId:DSP_3]) {
                knobGateThreshold.value = pkt->ch_10_gate_threshold+THRESHOLD_MAX;
                [self knobGateThresholdDidChange:nil];
            }
            if (knobGateRange.value != pkt->ch_10_gate_range+RANGE_MAX &&
                ![viewController sendCommandExists:CMD_CH2_GATE_RANGE dspId:DSP_3]) {
                knobGateRange.value = pkt->ch_10_gate_range+RANGE_MAX;
                [self knobGateRangeDidChange:nil];
            }
            if (knobGateHold.value != pkt->ch_10_gate_hold &&
                ![viewController sendCommandExists:CMD_CH2_GATE_HOLD dspId:DSP_3]) {
                knobGateHold.value = pkt->ch_10_gate_hold;
                [self knobGateHoldDidChange:nil];
            }
            if (knobGateAttack.value != pkt->ch_10_gate_attack &&
                ![viewController sendCommandExists:CMD_CH2_GATE_ATTACK dspId:DSP_3]) {
                knobGateAttack.value = pkt->ch_10_gate_attack;
                [self knobGateAttackDidChange:nil];
            }
            if (knobGateRelease.value != pkt->ch_10_gate_release &&
                ![viewController sendCommandExists:CMD_CH2_GATE_RELEASE dspId:DSP_3]) {
                knobGateRelease.value = pkt->ch_10_gate_release;
                [self knobGateReleaseDidChange:nil];
            }
            if (knobExpanderThreshold.value != pkt->ch_10_expand_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH2_EXPAND_THRESHOLD dspId:DSP_3]) {
                knobExpanderThreshold.value = pkt->ch_10_expand_threshold+THRESHOLD_MAX;
                [self knobExpanderThresholdDidChange:nil];
            }
            if (knobExpanderRatio.value != pkt->ch_10_expand_ratio &&
                ![viewController sendCommandExists:CMD_CH2_EXPAND_RATIO dspId:DSP_3]) {
                knobExpanderRatio.value = pkt->ch_10_expand_ratio;
                [self knobExpanderRatioDidChange:nil];
            }
            if (knobExpanderAttack.value != pkt->ch_10_expand_attack &&
                ![viewController sendCommandExists:CMD_CH2_EXPAND_ATTACK dspId:DSP_3]) {
                knobExpanderAttack.value = pkt->ch_10_expand_attack;
                [self knobExpanderAttackDidChange:nil];
            }
            if (knobExpanderRelease.value != pkt->ch_10_expand_release &&
                ![viewController sendCommandExists:CMD_CH2_EXPAND_RELEASE dspId:DSP_3]) {
                knobExpanderRelease.value = pkt->ch_10_expand_release;
                [self knobExpanderReleaseDidChange:nil];
            }
            if (knobCompThreshold.value != pkt->ch_10_compressor_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH2_COMPRESSOR_THRESHOLD dspId:DSP_3]) {
                knobCompThreshold.value = pkt->ch_10_compressor_threshold+THRESHOLD_MAX;
                [self knobCompThresholdDidChange:nil];
            }
            if (knobCompRatio.value != pkt->ch_10_compressor_ratio &&
                ![viewController sendCommandExists:CMD_CH2_COMPRESSOR_RATIO dspId:DSP_3]) {
                knobCompRatio.value = pkt->ch_10_compressor_ratio;
                [self knobCompRatioDidChange:nil];
            }
            if (knobCompOutputGain.value != pkt->ch_10_compressor_out_gain &&
                ![viewController sendCommandExists:CMD_CH2_COMPRESSOR_OUT_GAIN dspId:DSP_3]) {
                knobCompOutputGain.value = pkt->ch_10_compressor_out_gain;
                [self knobCompOutputGainDidChange:nil];
            }
            if (knobCompAttack.value != pkt->ch_10_compressor_attack &&
                ![viewController sendCommandExists:CMD_CH2_COMPRESSOR_ATTACK dspId:DSP_3]) {
                knobCompAttack.value = pkt->ch_10_compressor_attack;
                [self knobCompAttackDidChange:nil];
            }
            if (knobCompRelease.value != pkt->ch_10_compressor_release &&
                ![viewController sendCommandExists:CMD_CH2_COMPRESSOR_RELEASE dspId:DSP_3]) {
                knobCompRelease.value = pkt->ch_10_compressor_release;
                [self knobCompReleaseDidChange:nil];
            }
            if (knobLimiterThreshold.value != pkt->ch_10_limiter_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH2_LIMITER_THRESHOLD dspId:DSP_3]) {
                knobLimiterThreshold.value = pkt->ch_10_limiter_threshold+THRESHOLD_MAX;
                [self knobLimiterThresholdDidChange:nil];
            }
            if (knobLimiterAttack.value != pkt->ch_10_limiter_attack &&
                ![viewController sendCommandExists:CMD_CH2_LIMITER_ATTACK dspId:DSP_3]) {
                knobLimiterAttack.value = pkt->ch_10_limiter_attack;
                [self knobLimiterAttackDidChange:nil];
            }
            if (knobLimiterRelease.value != pkt->ch_10_limiter_release &&
                ![viewController sendCommandExists:CMD_CH2_LIMITER_RELEASE dspId:DSP_3]) {
                knobLimiterRelease.value = pkt->ch_10_limiter_release;
                [self knobLimiterReleaseDidChange:nil];
            }
            if (pkt->ch_10_fader >= 0.0 && pkt->ch_10_fader <= FADER_MAX_VALUE) {
                slider.value = pkt->ch_10_fader;
                [self onSliderAction:nil];
            }
            
            if (![viewController sendCommandExists:CMD_SOLO_CTRL dspId:DSP_5]) {
                btnSolo.selected = (_soloCtrl & 0x0200) >> 9;
            }
            if (![viewController sendCommandExists:CMD_CH_ON_OFF dspId:DSP_5]) {
                btnOn.selected = (pkt->ch_on_off & 0x0200) >> 9;
            }
            if (![viewController sendCommandExists:CMD_CH_METER dspId:DSP_5]) {
                btnMeterPrePost.selected = (_chMeterPrePost & 0x0200) >> 9;
            }

            if (btnDynGate.selected) {
                meterDynOutR.progress = ((float)pkt->ch_10_meter_noise_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_10_noise_gain/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynExpander.selected) {
                meterDynOutR.progress = ((float)pkt->ch_10_meter_expand_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_10_meter_expand_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynComp.selected) {
                meterDynOutR.progress = ((float)pkt->ch_10_meter_compressor_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_10_meter_compressor_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynLimiter.selected) {
                meterDynOutR.progress = ((float)pkt->ch_10_meter_limit_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_10_meter_limit_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            }
            if (![viewController sendCommandExists:CMD_CH10_CTRL dspId:DSP_3]) {
                [self updateDynStatus:pkt->ch_10_ctrl];
            }
            break;
        case CHANNEL_CH_11:
            if (knobGateThreshold.value != pkt->ch_11_gate_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH3_GATE_THRESHOLD dspId:DSP_3]) {
                knobGateThreshold.value = pkt->ch_11_gate_threshold+THRESHOLD_MAX;
                [self knobGateThresholdDidChange:nil];
            }
            if (knobGateRange.value != pkt->ch_11_gate_range+RANGE_MAX &&
                ![viewController sendCommandExists:CMD_CH3_GATE_RANGE dspId:DSP_3]) {
                knobGateRange.value = pkt->ch_11_gate_range+RANGE_MAX;
                [self knobGateRangeDidChange:nil];
            }
            if (knobGateHold.value != pkt->ch_11_gate_hold &&
                ![viewController sendCommandExists:CMD_CH3_GATE_HOLD dspId:DSP_3]) {
                knobGateHold.value = pkt->ch_11_gate_hold;
                [self knobGateHoldDidChange:nil];
            }
            if (knobGateAttack.value != pkt->ch_11_gate_attack &&
                ![viewController sendCommandExists:CMD_CH3_GATE_ATTACK dspId:DSP_3]) {
                knobGateAttack.value = pkt->ch_11_gate_attack;
                [self knobGateAttackDidChange:nil];
            }
            if (knobGateRelease.value != pkt->ch_11_gate_release &&
                ![viewController sendCommandExists:CMD_CH3_GATE_RELEASE dspId:DSP_3]) {
                knobGateRelease.value = pkt->ch_11_gate_release;
                [self knobGateReleaseDidChange:nil];
            }
            if (knobExpanderThreshold.value != pkt->ch_11_expand_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH3_EXPAND_THRESHOLD dspId:DSP_3]) {
                knobExpanderThreshold.value = pkt->ch_11_expand_threshold+THRESHOLD_MAX;
                [self knobExpanderThresholdDidChange:nil];
            }
            if (knobExpanderRatio.value != pkt->ch_11_expand_ratio &&
                ![viewController sendCommandExists:CMD_CH3_EXPAND_RATIO dspId:DSP_3]) {
                knobExpanderRatio.value = pkt->ch_11_expand_ratio;
                [self knobExpanderRatioDidChange:nil];
            }
            if (knobExpanderAttack.value != pkt->ch_11_expand_attack &&
                ![viewController sendCommandExists:CMD_CH3_EXPAND_ATTACK dspId:DSP_3]) {
                knobExpanderAttack.value = pkt->ch_11_expand_attack;
                [self knobExpanderAttackDidChange:nil];
            }
            if (knobExpanderRelease.value != pkt->ch_11_expand_release &&
                ![viewController sendCommandExists:CMD_CH3_EXPAND_RELEASE dspId:DSP_3]) {
                knobExpanderRelease.value = pkt->ch_11_expand_release;
                [self knobExpanderReleaseDidChange:nil];
            }
            if (knobCompThreshold.value != pkt->ch_11_compressor_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH3_COMPRESSOR_THRESHOLD dspId:DSP_3]) {
                knobCompThreshold.value = pkt->ch_11_compressor_threshold+THRESHOLD_MAX;
                [self knobCompThresholdDidChange:nil];
            }
            if (knobCompRatio.value != pkt->ch_11_compressor_ratio &&
                ![viewController sendCommandExists:CMD_CH3_COMPRESSOR_RATIO dspId:DSP_3]) {
                knobCompRatio.value = pkt->ch_11_compressor_ratio;
                [self knobCompRatioDidChange:nil];
            }
            if (knobCompOutputGain.value != pkt->ch_11_compressor_out_gain &&
                ![viewController sendCommandExists:CMD_CH3_COMPRESSOR_OUT_GAIN dspId:DSP_3]) {
                knobCompOutputGain.value = pkt->ch_11_compressor_out_gain;
                [self knobCompOutputGainDidChange:nil];
            }
            if (knobCompAttack.value != pkt->ch_11_compressor_attack &&
                ![viewController sendCommandExists:CMD_CH3_COMPRESSOR_ATTACK dspId:DSP_3]) {
                knobCompAttack.value = pkt->ch_11_compressor_attack;
                [self knobCompAttackDidChange:nil];
            }
            if (knobCompRelease.value != pkt->ch_11_compressor_release &&
                ![viewController sendCommandExists:CMD_CH3_COMPRESSOR_RELEASE dspId:DSP_3]) {
                knobCompRelease.value = pkt->ch_11_compressor_release;
                [self knobCompReleaseDidChange:nil];
            }
            if (knobLimiterThreshold.value != pkt->ch_11_limiter_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH3_LIMITER_THRESHOLD dspId:DSP_3]) {
                knobLimiterThreshold.value = pkt->ch_11_limiter_threshold+THRESHOLD_MAX;
                [self knobLimiterThresholdDidChange:nil];
            }
            if (knobLimiterAttack.value != pkt->ch_11_limiter_attack &&
                ![viewController sendCommandExists:CMD_CH3_LIMITER_ATTACK dspId:DSP_3]) {
                knobLimiterAttack.value = pkt->ch_11_limiter_attack;
                [self knobLimiterAttackDidChange:nil];
            }
            if (knobLimiterRelease.value != pkt->ch_11_limiter_release &&
                ![viewController sendCommandExists:CMD_CH3_LIMITER_RELEASE dspId:DSP_3]) {
                knobLimiterRelease.value = pkt->ch_11_limiter_release;
                [self knobLimiterReleaseDidChange:nil];
            }
            if (pkt->ch_11_fader >= 0.0 && pkt->ch_11_fader <= FADER_MAX_VALUE) {
                slider.value = pkt->ch_11_fader;
                [self onSliderAction:nil];
            }
            
            if (![viewController sendCommandExists:CMD_SOLO_CTRL dspId:DSP_5]) {
                btnSolo.selected = (_soloCtrl & 0x0400) >> 10;
            }
            if (![viewController sendCommandExists:CMD_CH_ON_OFF dspId:DSP_5]) {
                btnOn.selected = (pkt->ch_on_off & 0x0400) >> 10;
            }
            if (![viewController sendCommandExists:CMD_CH_METER dspId:DSP_5]) {
                btnMeterPrePost.selected = (_chMeterPrePost & 0x0400) >> 10;
            }
            
            if (btnDynGate.selected) {
                meterDynOutR.progress = ((float)pkt->ch_11_meter_noise_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_11_noise_gain/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynExpander.selected) {
                meterDynOutR.progress = ((float)pkt->ch_11_meter_expand_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_11_meter_expand_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynComp.selected) {
                meterDynOutR.progress = ((float)pkt->ch_11_meter_compressor_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_11_meter_compressor_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynLimiter.selected) {
                meterDynOutR.progress = ((float)pkt->ch_11_meter_limit_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_11_meter_limit_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            }
            if (![viewController sendCommandExists:CMD_CH11_CTRL dspId:DSP_3]) {
                [self updateDynStatus:pkt->ch_11_ctrl];
            }
            break;
        case CHANNEL_CH_12:
            if (knobGateThreshold.value != pkt->ch_12_gate_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH4_GATE_THRESHOLD dspId:DSP_3]) {
                knobGateThreshold.value = pkt->ch_12_gate_threshold+THRESHOLD_MAX;
                [self knobGateThresholdDidChange:nil];
            }
            if (knobGateRange.value != pkt->ch_12_gate_range+RANGE_MAX &&
                ![viewController sendCommandExists:CMD_CH4_GATE_RANGE dspId:DSP_3]) {
                knobGateRange.value = pkt->ch_12_gate_range+RANGE_MAX;
                [self knobGateRangeDidChange:nil];
            }
            if (knobGateHold.value != pkt->ch_12_gate_hold &&
                ![viewController sendCommandExists:CMD_CH4_GATE_HOLD dspId:DSP_3]) {
                knobGateHold.value = pkt->ch_12_gate_hold;
                [self knobGateHoldDidChange:nil];
            }
            if (knobGateAttack.value != pkt->ch_12_gate_attack &&
                ![viewController sendCommandExists:CMD_CH4_GATE_ATTACK dspId:DSP_3]) {
                knobGateAttack.value = pkt->ch_12_gate_attack;
                [self knobGateAttackDidChange:nil];
            }
            if (knobGateRelease.value != pkt->ch_12_gate_release &&
                ![viewController sendCommandExists:CMD_CH4_GATE_RELEASE dspId:DSP_3]) {
                knobGateRelease.value = pkt->ch_12_gate_release;
                [self knobGateReleaseDidChange:nil];
            }
            if (knobExpanderThreshold.value != pkt->ch_12_expand_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH4_EXPAND_THRESHOLD dspId:DSP_3]) {
                knobExpanderThreshold.value = pkt->ch_12_expand_threshold+THRESHOLD_MAX;
                [self knobExpanderThresholdDidChange:nil];
            }
            if (knobExpanderRatio.value != pkt->ch_12_expand_ratio &&
                ![viewController sendCommandExists:CMD_CH4_EXPAND_RATIO dspId:DSP_3]) {
                knobExpanderRatio.value = pkt->ch_12_expand_ratio;
                [self knobExpanderRatioDidChange:nil];
            }
            if (knobExpanderAttack.value != pkt->ch_12_expand_attack &&
                ![viewController sendCommandExists:CMD_CH4_EXPAND_ATTACK dspId:DSP_3]) {
                knobExpanderAttack.value = pkt->ch_12_expand_attack;
                [self knobExpanderAttackDidChange:nil];
            }
            if (knobExpanderRelease.value != pkt->ch_12_expand_release &&
                ![viewController sendCommandExists:CMD_CH4_EXPAND_RELEASE dspId:DSP_3]) {
                knobExpanderRelease.value = pkt->ch_12_expand_release;
                [self knobExpanderReleaseDidChange:nil];
            }
            if (knobCompThreshold.value != pkt->ch_12_compressor_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH4_COMPRESSOR_THRESHOLD dspId:DSP_3]) {
                knobCompThreshold.value = pkt->ch_12_compressor_threshold+THRESHOLD_MAX;
                [self knobCompThresholdDidChange:nil];
            }
            if (knobCompRatio.value != pkt->ch_12_compressor_ratio &&
                ![viewController sendCommandExists:CMD_CH4_COMPRESSOR_RATIO dspId:DSP_3]) {
                knobCompRatio.value = pkt->ch_12_compressor_ratio;
                [self knobCompRatioDidChange:nil];
            }
            if (knobCompOutputGain.value != pkt->ch_12_compressor_out_gain &&
                ![viewController sendCommandExists:CMD_CH4_COMPRESSOR_OUT_GAIN dspId:DSP_3]) {
                knobCompOutputGain.value = pkt->ch_12_compressor_out_gain;
                [self knobCompOutputGainDidChange:nil];
            }
            if (knobCompAttack.value != pkt->ch_12_compressor_attack &&
                ![viewController sendCommandExists:CMD_CH4_COMPRESSOR_ATTACK dspId:DSP_3]) {
                knobCompAttack.value = pkt->ch_12_compressor_attack;
                [self knobCompAttackDidChange:nil];
            }
            if (knobCompRelease.value != pkt->ch_12_compressor_release &&
                ![viewController sendCommandExists:CMD_CH4_COMPRESSOR_RELEASE dspId:DSP_3]) {
                knobCompRelease.value = pkt->ch_12_compressor_release;
                [self knobCompReleaseDidChange:nil];
            }
            if (knobLimiterThreshold.value != pkt->ch_12_limiter_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH4_LIMITER_THRESHOLD dspId:DSP_3]) {
                knobLimiterThreshold.value = pkt->ch_12_limiter_threshold+THRESHOLD_MAX;
                [self knobLimiterThresholdDidChange:nil];
            }
            if (knobLimiterAttack.value != pkt->ch_12_limiter_attack &&
                ![viewController sendCommandExists:CMD_CH4_LIMITER_ATTACK dspId:DSP_3]) {
                knobLimiterAttack.value = pkt->ch_12_limiter_attack;
                [self knobLimiterAttackDidChange:nil];
            }
            if (knobLimiterRelease.value != pkt->ch_12_limiter_release &&
                ![viewController sendCommandExists:CMD_CH4_LIMITER_RELEASE dspId:DSP_3]) {
                knobLimiterRelease.value = pkt->ch_12_limiter_release;
                [self knobLimiterReleaseDidChange:nil];
            }
            if (pkt->ch_12_fader >= 0.0 && pkt->ch_12_fader <= FADER_MAX_VALUE) {
                slider.value = pkt->ch_12_fader;
                [self onSliderAction:nil];
            }
            
            if (![viewController sendCommandExists:CMD_SOLO_CTRL dspId:DSP_5]) {
                btnSolo.selected = (_soloCtrl & 0x0800) >> 11;
            }
            if (![viewController sendCommandExists:CMD_CH_ON_OFF dspId:DSP_5]) {
                btnOn.selected = (pkt->ch_on_off & 0x0800) >> 11;
            }
            if (![viewController sendCommandExists:CMD_CH_METER dspId:DSP_5]) {
                btnMeterPrePost.selected = (_chMeterPrePost & 0x0800) >> 11;
            }

            if (btnDynGate.selected) {
                meterDynOutR.progress = ((float)pkt->ch_12_meter_noise_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_12_noise_gain/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynExpander.selected) {
                meterDynOutR.progress = ((float)pkt->ch_12_meter_expand_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_12_meter_expand_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynComp.selected) {
                meterDynOutR.progress = ((float)pkt->ch_12_meter_compressor_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_12_meter_compressor_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynLimiter.selected) {
                meterDynOutR.progress = ((float)pkt->ch_12_meter_limit_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_12_meter_limit_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            }
            if (![viewController sendCommandExists:CMD_CH12_CTRL dspId:DSP_3]) {
                [self updateDynStatus:pkt->ch_12_ctrl];
            }
            break;
        case CHANNEL_CH_13:
            if (knobGateThreshold.value != pkt->ch_13_gate_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH1_GATE_THRESHOLD dspId:DSP_4]) {
                knobGateThreshold.value = pkt->ch_13_gate_threshold+THRESHOLD_MAX;
                [self knobGateThresholdDidChange:nil];
            }
            if (knobGateRange.value != pkt->ch_13_gate_range+RANGE_MAX &&
                ![viewController sendCommandExists:CMD_CH1_GATE_RANGE dspId:DSP_4]) {
                knobGateRange.value = pkt->ch_13_gate_range+RANGE_MAX;
                [self knobGateRangeDidChange:nil];
            }
            if (knobGateHold.value != pkt->ch_13_gate_hold &&
                ![viewController sendCommandExists:CMD_CH1_GATE_HOLD dspId:DSP_4]) {
                knobGateHold.value = pkt->ch_13_gate_hold;
                [self knobGateHoldDidChange:nil];
            }
            if (knobGateAttack.value != pkt->ch_13_gate_attack &&
                ![viewController sendCommandExists:CMD_CH1_GATE_ATTACK dspId:DSP_4]) {
                knobGateAttack.value = pkt->ch_13_gate_attack;
                [self knobGateAttackDidChange:nil];
            }
            if (knobGateRelease.value != pkt->ch_13_gate_release &&
                ![viewController sendCommandExists:CMD_CH1_GATE_RELEASE dspId:DSP_4]) {
                knobGateRelease.value = pkt->ch_13_gate_release;
                [self knobGateReleaseDidChange:nil];
            }
            if (knobExpanderThreshold.value != pkt->ch_13_expand_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH1_EXPAND_THRESHOLD dspId:DSP_4]) {
                knobExpanderThreshold.value = pkt->ch_13_expand_threshold+THRESHOLD_MAX;
                [self knobExpanderThresholdDidChange:nil];
            }
            if (knobExpanderRatio.value != pkt->ch_13_expand_ratio &&
                ![viewController sendCommandExists:CMD_CH1_EXPAND_RATIO dspId:DSP_4]) {
                knobExpanderRatio.value = pkt->ch_13_expand_ratio;
                [self knobExpanderRatioDidChange:nil];
            }
            if (knobExpanderAttack.value != pkt->ch_13_expand_attack &&
                ![viewController sendCommandExists:CMD_CH1_EXPAND_ATTACK dspId:DSP_4]) {
                knobExpanderAttack.value = pkt->ch_13_expand_attack;
                [self knobExpanderAttackDidChange:nil];
            }
            if (knobExpanderRelease.value != pkt->ch_13_expand_release &&
                ![viewController sendCommandExists:CMD_CH1_EXPAND_RELEASE dspId:DSP_4]) {
                knobExpanderRelease.value = pkt->ch_13_expand_release;
                [self knobExpanderReleaseDidChange:nil];
            }
            if (knobCompThreshold.value != pkt->ch_13_compressor_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH1_COMPRESSOR_THRESHOLD dspId:DSP_4]) {
                knobCompThreshold.value = pkt->ch_13_compressor_threshold+THRESHOLD_MAX;
                [self knobCompThresholdDidChange:nil];
            }
            if (knobCompRatio.value != pkt->ch_13_compressor_ratio &&
                ![viewController sendCommandExists:CMD_CH1_COMPRESSOR_RATIO dspId:DSP_4]) {
                knobCompRatio.value = pkt->ch_13_compressor_ratio;
                [self knobCompRatioDidChange:nil];
            }
            if (knobCompOutputGain.value != pkt->ch_13_compressor_out_gain &&
                ![viewController sendCommandExists:CMD_CH1_COMPRESSOR_OUT_GAIN dspId:DSP_4]) {
                knobCompOutputGain.value = pkt->ch_13_compressor_out_gain;
                [self knobCompOutputGainDidChange:nil];
            }
            if (knobCompAttack.value != pkt->ch_13_compressor_attack &&
                ![viewController sendCommandExists:CMD_CH1_COMPRESSOR_ATTACK dspId:DSP_4]) {
                knobCompAttack.value = pkt->ch_13_compressor_attack;
                [self knobCompAttackDidChange:nil];
            }
            if (knobCompRelease.value != pkt->ch_13_compressor_release &&
                ![viewController sendCommandExists:CMD_CH1_COMPRESSOR_RELEASE dspId:DSP_4]) {
                knobCompRelease.value = pkt->ch_13_compressor_release;
                [self knobCompReleaseDidChange:nil];
            }
            if (knobLimiterThreshold.value != pkt->ch_13_limiter_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH1_LIMITER_THRESHOLD dspId:DSP_4]) {
                knobLimiterThreshold.value = pkt->ch_13_limiter_threshold+THRESHOLD_MAX;
                [self knobLimiterThresholdDidChange:nil];
            }
            if (knobLimiterAttack.value != pkt->ch_13_limiter_attack &&
                ![viewController sendCommandExists:CMD_CH1_LIMITER_ATTACK dspId:DSP_4]) {
                knobLimiterAttack.value = pkt->ch_13_limiter_attack;
                [self knobLimiterAttackDidChange:nil];
            }
            if (knobLimiterRelease.value != pkt->ch_13_limiter_release &&
                ![viewController sendCommandExists:CMD_CH1_LIMITER_RELEASE dspId:DSP_4]) {
                knobLimiterRelease.value = pkt->ch_13_limiter_release;
                [self knobLimiterReleaseDidChange:nil];
            }
            if (pkt->ch_13_fader >= 0.0 && pkt->ch_13_fader <= FADER_MAX_VALUE) {
                slider.value = pkt->ch_13_fader;
                [self onSliderAction:nil];
            }
            
            if (![viewController sendCommandExists:CMD_SOLO_CTRL dspId:DSP_5]) {
                btnSolo.selected = (_soloCtrl & 0x1000) >> 12;
            }
            if (![viewController sendCommandExists:CMD_CH_ON_OFF dspId:DSP_5]) {
                btnOn.selected = (pkt->ch_on_off & 0x1000) >> 12;
            }
            if (![viewController sendCommandExists:CMD_CH_METER dspId:DSP_5]) {
                btnMeterPrePost.selected = (_chMeterPrePost & 0x1000) >> 12;
            }
            
            if (btnDynGate.selected) {
                meterDynOutR.progress = ((float)pkt->ch_13_meter_noise_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_13_noise_gain/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynExpander.selected) {
                meterDynOutR.progress = ((float)pkt->ch_13_meter_expand_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_13_meter_expand_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynComp.selected) {
                meterDynOutR.progress = ((float)pkt->ch_13_meter_compressor_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_13_meter_compressor_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynLimiter.selected) {
                meterDynOutR.progress = ((float)pkt->ch_13_meter_limit_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_13_meter_limit_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            }
            if (![viewController sendCommandExists:CMD_CH13_CTRL dspId:DSP_4]) {
                [self updateDynStatus:pkt->ch_13_ctrl];
            }
            break;
        case CHANNEL_CH_14:
            if (knobGateThreshold.value != pkt->ch_14_gate_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH2_GATE_THRESHOLD dspId:DSP_4]) {
                knobGateThreshold.value = pkt->ch_14_gate_threshold+THRESHOLD_MAX;
                [self knobGateThresholdDidChange:nil];
            }
            if (knobGateRange.value != pkt->ch_14_gate_range+RANGE_MAX &&
                ![viewController sendCommandExists:CMD_CH2_GATE_RANGE dspId:DSP_4]) {
                knobGateRange.value = pkt->ch_14_gate_range+RANGE_MAX;
                [self knobGateRangeDidChange:nil];
            }
            if (knobGateHold.value != pkt->ch_14_gate_hold &&
                ![viewController sendCommandExists:CMD_CH2_GATE_HOLD dspId:DSP_4]) {
                knobGateHold.value = pkt->ch_14_gate_hold;
                [self knobGateHoldDidChange:nil];
            }
            if (knobGateAttack.value != pkt->ch_14_gate_attack &&
                ![viewController sendCommandExists:CMD_CH2_GATE_ATTACK dspId:DSP_4]) {
                knobGateAttack.value = pkt->ch_14_gate_attack;
                [self knobGateAttackDidChange:nil];
            }
            if (knobGateRelease.value != pkt->ch_14_gate_release &&
                ![viewController sendCommandExists:CMD_CH2_GATE_RELEASE dspId:DSP_4]) {
                knobGateRelease.value = pkt->ch_14_gate_release;
                [self knobGateReleaseDidChange:nil];
            }
            if (knobExpanderThreshold.value != pkt->ch_14_expand_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH2_EXPAND_THRESHOLD dspId:DSP_4]) {
                knobExpanderThreshold.value = pkt->ch_14_expand_threshold+THRESHOLD_MAX;
                [self knobExpanderThresholdDidChange:nil];
            }
            if (knobExpanderRatio.value != pkt->ch_14_expand_ratio &&
                ![viewController sendCommandExists:CMD_CH2_EXPAND_RATIO dspId:DSP_4]) {
                knobExpanderRatio.value = pkt->ch_14_expand_ratio;
                [self knobExpanderRatioDidChange:nil];
            }
            if (knobExpanderAttack.value != pkt->ch_14_expand_attack &&
                ![viewController sendCommandExists:CMD_CH2_EXPAND_ATTACK dspId:DSP_4]) {
                knobExpanderAttack.value = pkt->ch_14_expand_attack;
                [self knobExpanderAttackDidChange:nil];
            }
            if (knobExpanderRelease.value != pkt->ch_14_expand_release &&
                ![viewController sendCommandExists:CMD_CH2_EXPAND_RELEASE dspId:DSP_4]) {
                knobExpanderRelease.value = pkt->ch_14_expand_release;
                [self knobExpanderReleaseDidChange:nil];
            }
            if (knobCompThreshold.value != pkt->ch_14_compressor_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH2_COMPRESSOR_THRESHOLD dspId:DSP_4]) {
                knobCompThreshold.value = pkt->ch_14_compressor_threshold+THRESHOLD_MAX;
                [self knobCompThresholdDidChange:nil];
            }
            if (knobCompRatio.value != pkt->ch_14_compressor_ratio &&
                ![viewController sendCommandExists:CMD_CH2_COMPRESSOR_RATIO dspId:DSP_4]) {
                knobCompRatio.value = pkt->ch_14_compressor_ratio;
                [self knobCompRatioDidChange:nil];
            }
            if (knobCompOutputGain.value != pkt->ch_14_compressor_out_gain &&
                ![viewController sendCommandExists:CMD_CH2_COMPRESSOR_OUT_GAIN dspId:DSP_4]) {
                knobCompOutputGain.value = pkt->ch_14_compressor_out_gain;
                [self knobCompOutputGainDidChange:nil];
            }
            if (knobCompAttack.value != pkt->ch_14_compressor_attack &&
                ![viewController sendCommandExists:CMD_CH2_COMPRESSOR_ATTACK dspId:DSP_4]) {
                knobCompAttack.value = pkt->ch_14_compressor_attack;
                [self knobCompAttackDidChange:nil];
            }
            if (knobCompRelease.value != pkt->ch_14_compressor_release &&
                ![viewController sendCommandExists:CMD_CH2_COMPRESSOR_RELEASE dspId:DSP_4]) {
                knobCompRelease.value = pkt->ch_14_compressor_release;
                [self knobCompReleaseDidChange:nil];
            }
            if (knobLimiterThreshold.value != pkt->ch_14_limiter_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH2_LIMITER_THRESHOLD dspId:DSP_4]) {
                knobLimiterThreshold.value = pkt->ch_14_limiter_threshold+THRESHOLD_MAX;
                [self knobLimiterThresholdDidChange:nil];
            }
            if (knobLimiterAttack.value != pkt->ch_14_limiter_attack &&
                ![viewController sendCommandExists:CMD_CH2_LIMITER_ATTACK dspId:DSP_4]) {
                knobLimiterAttack.value = pkt->ch_14_limiter_attack;
                [self knobLimiterAttackDidChange:nil];
            }
            if (knobLimiterRelease.value != pkt->ch_14_limiter_release &&
                ![viewController sendCommandExists:CMD_CH2_LIMITER_RELEASE dspId:DSP_4]) {
                knobLimiterRelease.value = pkt->ch_14_limiter_release;
                [self knobLimiterReleaseDidChange:nil];
            }
            if (pkt->ch_14_fader >= 0.0 && pkt->ch_14_fader <= FADER_MAX_VALUE) {
                slider.value = pkt->ch_14_fader;
                [self onSliderAction:nil];
            }
            
            if (![viewController sendCommandExists:CMD_SOLO_CTRL dspId:DSP_5]) {
                btnSolo.selected = (_soloCtrl & 0x2000) >> 13;
            }
            if (![viewController sendCommandExists:CMD_CH_ON_OFF dspId:DSP_5]) {
                btnOn.selected = (pkt->ch_on_off & 0x2000) >> 13;
            }
            if (![viewController sendCommandExists:CMD_CH_METER dspId:DSP_5]) {
                btnMeterPrePost.selected = (_chMeterPrePost & 0x2000) >> 13;
            }

            if (btnDynGate.selected) {
                meterDynOutR.progress = ((float)pkt->ch_14_meter_noise_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_14_noise_gain/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynExpander.selected) {
                meterDynOutR.progress = ((float)pkt->ch_14_meter_expand_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_14_meter_expand_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynComp.selected) {
                meterDynOutR.progress = ((float)pkt->ch_14_meter_compressor_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_14_meter_compressor_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynLimiter.selected) {
                meterDynOutR.progress = ((float)pkt->ch_14_meter_limit_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_4_meter_limit_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            }
            if (![viewController sendCommandExists:CMD_CH14_CTRL dspId:DSP_4]) {
                [self updateDynStatus:pkt->ch_14_ctrl];
            }
            break;
        case CHANNEL_CH_15:
            if (knobGateThreshold.value != pkt->ch_15_gate_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH3_GATE_THRESHOLD dspId:DSP_4]) {
                knobGateThreshold.value = pkt->ch_15_gate_threshold+THRESHOLD_MAX;
                [self knobGateThresholdDidChange:nil];
            }
            if (knobGateRange.value != pkt->ch_15_gate_range+RANGE_MAX &&
                ![viewController sendCommandExists:CMD_CH3_GATE_RANGE dspId:DSP_4]) {
                knobGateRange.value = pkt->ch_15_gate_range+RANGE_MAX;
                [self knobGateRangeDidChange:nil];
            }
            if (knobGateHold.value != pkt->ch_15_gate_hold &&
                ![viewController sendCommandExists:CMD_CH3_GATE_HOLD dspId:DSP_4]) {
                knobGateHold.value = pkt->ch_15_gate_hold;
                [self knobGateHoldDidChange:nil];
            }
            if (knobGateAttack.value != pkt->ch_15_gate_attack &&
                ![viewController sendCommandExists:CMD_CH3_GATE_ATTACK dspId:DSP_4]) {
                knobGateAttack.value = pkt->ch_15_gate_attack;
                [self knobGateAttackDidChange:nil];
            }
            if (knobGateRelease.value != pkt->ch_15_gate_release &&
                ![viewController sendCommandExists:CMD_CH3_GATE_RELEASE dspId:DSP_4]) {
                knobGateRelease.value = pkt->ch_15_gate_release;
                [self knobGateReleaseDidChange:nil];
            }
            if (knobExpanderThreshold.value != pkt->ch_15_expand_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH3_EXPAND_THRESHOLD dspId:DSP_4]) {
                knobExpanderThreshold.value = pkt->ch_15_expand_threshold+THRESHOLD_MAX;
                [self knobExpanderThresholdDidChange:nil];
            }
            if (knobExpanderRatio.value != pkt->ch_15_expand_ratio &&
                ![viewController sendCommandExists:CMD_CH3_EXPAND_RATIO dspId:DSP_4]) {
                knobExpanderRatio.value = pkt->ch_15_expand_ratio;
                [self knobExpanderRatioDidChange:nil];
            }
            if (knobExpanderAttack.value != pkt->ch_15_expand_attack &&
                ![viewController sendCommandExists:CMD_CH3_EXPAND_ATTACK dspId:DSP_4]) {
                knobExpanderAttack.value = pkt->ch_15_expand_attack;
                [self knobExpanderAttackDidChange:nil];
            }
            if (knobExpanderRelease.value != pkt->ch_15_expand_release &&
                ![viewController sendCommandExists:CMD_CH3_EXPAND_RELEASE dspId:DSP_4]) {
                knobExpanderRelease.value = pkt->ch_15_expand_release;
                [self knobExpanderReleaseDidChange:nil];
            }
            if (knobCompThreshold.value != pkt->ch_15_compressor_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH3_COMPRESSOR_THRESHOLD dspId:DSP_4]) {
                knobCompThreshold.value = pkt->ch_15_compressor_threshold+THRESHOLD_MAX;
                [self knobCompThresholdDidChange:nil];
            }
            if (knobCompRatio.value != pkt->ch_15_compressor_ratio &&
                ![viewController sendCommandExists:CMD_CH3_COMPRESSOR_RATIO dspId:DSP_4]) {
                knobCompRatio.value = pkt->ch_15_compressor_ratio;
                [self knobCompRatioDidChange:nil];
            }
            if (knobCompOutputGain.value != pkt->ch_15_compressor_out_gain &&
                ![viewController sendCommandExists:CMD_CH3_COMPRESSOR_OUT_GAIN dspId:DSP_4]) {
                knobCompOutputGain.value = pkt->ch_15_compressor_out_gain;
                [self knobCompOutputGainDidChange:nil];
            }
            if (knobCompAttack.value != pkt->ch_15_compressor_attack &&
                ![viewController sendCommandExists:CMD_CH3_COMPRESSOR_ATTACK dspId:DSP_4]) {
                knobCompAttack.value = pkt->ch_15_compressor_attack;
                [self knobCompAttackDidChange:nil];
            }
            if (knobCompRelease.value != pkt->ch_15_compressor_release &&
                ![viewController sendCommandExists:CMD_CH3_COMPRESSOR_RELEASE dspId:DSP_4]) {
                knobCompRelease.value = pkt->ch_15_compressor_release;
                [self knobCompReleaseDidChange:nil];
            }
            if (knobLimiterThreshold.value != pkt->ch_15_limiter_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH3_LIMITER_THRESHOLD dspId:DSP_4]) {
                knobLimiterThreshold.value = pkt->ch_15_limiter_threshold+THRESHOLD_MAX;
                [self knobLimiterThresholdDidChange:nil];
            }
            if (knobLimiterAttack.value != pkt->ch_15_limiter_attack &&
                ![viewController sendCommandExists:CMD_CH3_LIMITER_ATTACK dspId:DSP_4]) {
                knobLimiterAttack.value = pkt->ch_15_limiter_attack;
                [self knobLimiterAttackDidChange:nil];
            }
            if (knobLimiterRelease.value != pkt->ch_15_limiter_release &&
                ![viewController sendCommandExists:CMD_CH3_LIMITER_RELEASE dspId:DSP_4]) {
                knobLimiterRelease.value = pkt->ch_15_limiter_release;
                [self knobLimiterReleaseDidChange:nil];
            }
            if (pkt->ch_15_fader >= 0.0 && pkt->ch_15_fader <= FADER_MAX_VALUE) {
                slider.value = pkt->ch_15_fader;
                [self onSliderAction:nil];
            }
            
            if (![viewController sendCommandExists:CMD_SOLO_CTRL dspId:DSP_5]) {
                btnSolo.selected = (_soloCtrl & 0x4000) >> 14;
            }
            if (![viewController sendCommandExists:CMD_CH_ON_OFF dspId:DSP_5]) {
                btnOn.selected = (pkt->ch_on_off & 0x4000) >> 14;
            }
            if (![viewController sendCommandExists:CMD_CH_METER dspId:DSP_5]) {
                btnMeterPrePost.selected = (_chMeterPrePost & 0x4000) >> 14;
            }

            if (btnDynGate.selected) {
                meterDynOutR.progress = ((float)pkt->ch_15_meter_noise_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_15_noise_gain/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynExpander.selected) {
                meterDynOutR.progress = ((float)pkt->ch_15_meter_expand_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_15_meter_expand_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynComp.selected) {
                meterDynOutR.progress = ((float)pkt->ch_15_meter_compressor_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_15_meter_compressor_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynLimiter.selected) {
                meterDynOutR.progress = ((float)pkt->ch_15_meter_limit_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_15_meter_limit_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            }
            if (![viewController sendCommandExists:CMD_CH15_CTRL dspId:DSP_4]) {
                [self updateDynStatus:pkt->ch_15_ctrl];
            }
            break;
        case CHANNEL_CH_16:
            if (knobGateThreshold.value != pkt->ch_16_gate_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH4_GATE_THRESHOLD dspId:DSP_4]) {
                knobGateThreshold.value = pkt->ch_16_gate_threshold+THRESHOLD_MAX;
                [self knobGateThresholdDidChange:nil];
            }
            if (knobGateRange.value != pkt->ch_16_gate_range+RANGE_MAX &&
                ![viewController sendCommandExists:CMD_CH4_GATE_RANGE dspId:DSP_4]) {
                knobGateRange.value = pkt->ch_16_gate_range+RANGE_MAX;
                [self knobGateRangeDidChange:nil];
            }
            if (knobGateHold.value != pkt->ch_16_gate_hold &&
                ![viewController sendCommandExists:CMD_CH4_GATE_HOLD dspId:DSP_4]) {
                knobGateHold.value = pkt->ch_16_gate_hold;
                [self knobGateHoldDidChange:nil];
            }
            if (knobGateAttack.value != pkt->ch_16_gate_attack &&
                ![viewController sendCommandExists:CMD_CH4_GATE_ATTACK dspId:DSP_4]) {
                knobGateAttack.value = pkt->ch_16_gate_attack;
                [self knobGateAttackDidChange:nil];
            }
            if (knobGateRelease.value != pkt->ch_16_gate_release &&
                ![viewController sendCommandExists:CMD_CH4_GATE_RELEASE dspId:DSP_4]) {
                knobGateRelease.value = pkt->ch_16_gate_release;
                [self knobGateReleaseDidChange:nil];
            }
            if (knobExpanderThreshold.value != pkt->ch_16_expand_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH4_EXPAND_THRESHOLD dspId:DSP_4]) {
                knobExpanderThreshold.value = pkt->ch_16_expand_threshold+THRESHOLD_MAX;
                [self knobExpanderThresholdDidChange:nil];
            }
            if (knobExpanderRatio.value != pkt->ch_16_expand_ratio &&
                ![viewController sendCommandExists:CMD_CH4_EXPAND_RATIO dspId:DSP_4]) {
                knobExpanderRatio.value = pkt->ch_16_expand_ratio;
                [self knobExpanderRatioDidChange:nil];
            }
            if (knobExpanderAttack.value != pkt->ch_16_expand_attack &&
                ![viewController sendCommandExists:CMD_CH4_EXPAND_ATTACK dspId:DSP_4]) {
                knobExpanderAttack.value = pkt->ch_16_expand_attack;
                [self knobExpanderAttackDidChange:nil];
            }
            if (knobExpanderRelease.value != pkt->ch_16_expand_release &&
                ![viewController sendCommandExists:CMD_CH4_EXPAND_RELEASE dspId:DSP_4]) {
                knobExpanderRelease.value = pkt->ch_16_expand_release;
                [self knobExpanderReleaseDidChange:nil];
            }
            if (knobCompThreshold.value != pkt->ch_16_compressor_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH4_COMPRESSOR_THRESHOLD dspId:DSP_4]) {
                knobCompThreshold.value = pkt->ch_16_compressor_threshold+THRESHOLD_MAX;
                [self knobCompThresholdDidChange:nil];
            }
            if (knobCompRatio.value != pkt->ch_16_compressor_ratio &&
                ![viewController sendCommandExists:CMD_CH4_COMPRESSOR_RATIO dspId:DSP_4]) {
                knobCompRatio.value = pkt->ch_16_compressor_ratio;
                [self knobCompRatioDidChange:nil];
            }
            if (knobCompOutputGain.value != pkt->ch_16_compressor_out_gain &&
                ![viewController sendCommandExists:CMD_CH4_COMPRESSOR_OUT_GAIN dspId:DSP_4]) {
                knobCompOutputGain.value = pkt->ch_16_compressor_out_gain;
                [self knobCompOutputGainDidChange:nil];
            }
            if (knobCompAttack.value != pkt->ch_16_compressor_attack &&
                ![viewController sendCommandExists:CMD_CH4_COMPRESSOR_ATTACK dspId:DSP_4]) {
                knobCompAttack.value = pkt->ch_16_compressor_attack;
                [self knobCompAttackDidChange:nil];
            }
            if (knobCompRelease.value != pkt->ch_16_compressor_release &&
                ![viewController sendCommandExists:CMD_CH4_COMPRESSOR_RELEASE dspId:DSP_4]) {
                knobCompRelease.value = pkt->ch_16_compressor_release;
                [self knobCompReleaseDidChange:nil];
            }
            if (knobLimiterThreshold.value != pkt->ch_16_limiter_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH4_LIMITER_THRESHOLD dspId:DSP_4]) {
                knobLimiterThreshold.value = pkt->ch_16_limiter_threshold+THRESHOLD_MAX;
                [self knobLimiterThresholdDidChange:nil];
            }
            if (knobLimiterAttack.value != pkt->ch_16_limiter_attack &&
                ![viewController sendCommandExists:CMD_CH4_LIMITER_ATTACK dspId:DSP_4]) {
                knobLimiterAttack.value = pkt->ch_16_limiter_attack;
                [self knobLimiterAttackDidChange:nil];
            }
            if (knobLimiterRelease.value != pkt->ch_16_limiter_release &&
                ![viewController sendCommandExists:CMD_CH4_LIMITER_RELEASE dspId:DSP_4]) {
                knobLimiterRelease.value = pkt->ch_16_limiter_release;
                [self knobLimiterReleaseDidChange:nil];
            }
            if (pkt->ch_16_fader >= 0.0 && pkt->ch_16_fader <= FADER_MAX_VALUE) {
                slider.value = pkt->ch_16_fader;
                [self onSliderAction:nil];
            }
            
            if (![viewController sendCommandExists:CMD_SOLO_CTRL dspId:DSP_5]) {
                btnSolo.selected = (_soloCtrl & 0x8000) >> 15;
            }
            if (![viewController sendCommandExists:CMD_CH_ON_OFF dspId:DSP_5]) {
                btnOn.selected = (pkt->ch_on_off & 0x8000) >> 15;
            }
            if (![viewController sendCommandExists:CMD_CH_METER dspId:DSP_5]) {
                btnMeterPrePost.selected = (_chMeterPrePost & 0x8000) >> 15;
            }

            if (btnDynGate.selected) {
                meterDynOutR.progress = ((float)pkt->ch_16_meter_noise_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_16_noise_gain/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynExpander.selected) {
                meterDynOutR.progress = ((float)pkt->ch_16_meter_expand_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_16_meter_expand_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynComp.selected) {
                meterDynOutR.progress = ((float)pkt->ch_16_meter_compressor_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_16_meter_compressor_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynLimiter.selected) {
                meterDynOutR.progress = ((float)pkt->ch_16_meter_limit_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_16_meter_limit_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            }
            if (![viewController sendCommandExists:CMD_CH16_CTRL dspId:DSP_4]) {
                [self updateDynStatus:pkt->ch_16_ctrl];
            }
            break;
        case CHANNEL_CH_17:
            if (knobGateThreshold.value != pkt->ch_17_gate_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH17_GATE_THRESHOLD dspId:DSP_6]) {
                knobGateThreshold.value = pkt->ch_17_gate_threshold+THRESHOLD_MAX;
                [self knobGateThresholdDidChange:nil];
            }
            if (knobGateRange.value != pkt->ch_17_gate_range+RANGE_MAX &&
                ![viewController sendCommandExists:CMD_CH17_GATE_RANGE dspId:DSP_6]) {
                knobGateRange.value = pkt->ch_17_gate_range+RANGE_MAX;
                [self knobGateRangeDidChange:nil];
            }
            if (knobGateHold.value != pkt->ch_17_gate_hold &&
                ![viewController sendCommandExists:CMD_CH17_GATE_HOLD dspId:DSP_6]) {
                knobGateHold.value = pkt->ch_17_gate_hold;
                [self knobGateHoldDidChange:nil];
            }
            if (knobGateAttack.value != pkt->ch_17_gate_attack &&
                ![viewController sendCommandExists:CMD_CH17_GATE_ATTACK dspId:DSP_6]) {
                knobGateAttack.value = pkt->ch_17_gate_attack;
                [self knobGateAttackDidChange:nil];
            }
            if (knobGateRelease.value != pkt->ch_17_gate_release &&
                ![viewController sendCommandExists:CMD_CH17_GATE_RELEASE dspId:DSP_6]) {
                knobGateRelease.value = pkt->ch_17_gate_release;
                [self knobGateReleaseDidChange:nil];
            }
            if (knobExpanderThreshold.value != pkt->ch_17_expand_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH17_EXPAND_THRESHOLD dspId:DSP_6]) {
                knobExpanderThreshold.value = pkt->ch_17_expand_threshold+THRESHOLD_MAX;
                [self knobExpanderThresholdDidChange:nil];
            }
            if (knobExpanderRatio.value != pkt->ch_17_expand_ratio &&
                ![viewController sendCommandExists:CMD_CH17_EXPAND_RATIO dspId:DSP_6]) {
                knobExpanderRatio.value = pkt->ch_17_expand_ratio;
                [self knobExpanderRatioDidChange:nil];
            }
            if (knobExpanderAttack.value != pkt->ch_17_expand_attack &&
                ![viewController sendCommandExists:CMD_CH17_EXPAND_ATTACK dspId:DSP_6]) {
                knobExpanderAttack.value = pkt->ch_17_expand_attack;
                [self knobExpanderAttackDidChange:nil];
            }
            if (knobExpanderRelease.value != pkt->ch_17_expand_release &&
                ![viewController sendCommandExists:CMD_CH17_EXPAND_RELEASE dspId:DSP_6]) {
                knobExpanderRelease.value = pkt->ch_17_expand_release;
                [self knobExpanderReleaseDidChange:nil];
            }
            if (knobCompThreshold.value != pkt->ch_17_compressor_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH17_COMPRESSOR_THRESHOLD dspId:DSP_6]) {
                knobCompThreshold.value = pkt->ch_17_compressor_threshold+THRESHOLD_MAX;
                [self knobCompThresholdDidChange:nil];
            }
            if (knobCompRatio.value != pkt->ch_17_compressor_ratio &&
                ![viewController sendCommandExists:CMD_CH17_COMPRESSOR_RATIO dspId:DSP_6]) {
                knobCompRatio.value = pkt->ch_17_compressor_ratio;
                [self knobCompRatioDidChange:nil];
            }
            if (knobCompOutputGain.value != pkt->ch_17_compressor_out_gain &&
                ![viewController sendCommandExists:CMD_CH17_COMPRESSOR_OUT_GAIN dspId:DSP_6]) {
                knobCompOutputGain.value = pkt->ch_17_compressor_out_gain;
                [self knobCompOutputGainDidChange:nil];
            }
            if (knobCompAttack.value != pkt->ch_17_compressor_attack &&
                ![viewController sendCommandExists:CMD_CH17_COMPRESSOR_ATTACK dspId:DSP_6]) {
                knobCompAttack.value = pkt->ch_17_compressor_attack;
                [self knobCompAttackDidChange:nil];
            }
            if (knobCompRelease.value != pkt->ch_17_compressor_release &&
                ![viewController sendCommandExists:CMD_CH17_COMPRESSOR_RELEASE dspId:DSP_6]) {
                knobCompRelease.value = pkt->ch_17_compressor_release;
                [self knobCompReleaseDidChange:nil];
            }
            if (knobLimiterThreshold.value != pkt->ch_17_limiter_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH17_LIMITER_THRESHOLD dspId:DSP_6]) {
                knobLimiterThreshold.value = pkt->ch_17_limiter_threshold+THRESHOLD_MAX;
                [self knobLimiterThresholdDidChange:nil];
            }
            if (knobLimiterAttack.value != pkt->ch_17_limiter_attack &&
                ![viewController sendCommandExists:CMD_CH17_LIMITER_ATTACK dspId:DSP_6]) {
                knobLimiterAttack.value = pkt->ch_17_limiter_attack;
                [self knobLimiterAttackDidChange:nil];
            }
            if (knobLimiterRelease.value != pkt->ch_17_limiter_release &&
                ![viewController sendCommandExists:CMD_CH17_LIMITER_ATTACK dspId:DSP_6]) {
                knobLimiterRelease.value = pkt->ch_17_limiter_release;
                [self knobLimiterReleaseDidChange:nil];
            }
            if (pkt->ch_17_fader >= 0.0 && pkt->ch_17_fader <= FADER_MAX_VALUE) {
                slider.value = pkt->ch_17_fader;
                [self onSliderAction:nil];
            }
            
            if (![viewController sendCommandExists:CMD_CH17_USB_AUDIO_SETTING dspId:DSP_5]) {
                btnSolo.selected = (_ch17Setting & 0x0400) >> 10;
                BOOL soloSafe = (_ch17Setting & 0x0800) >> 11;
                if (soloSafe) {
                    [btnSolo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
                    [btnSolo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
                } else {
                    [btnSolo setTitle:@"SOLO" forState:UIControlStateNormal];
                    [btnSolo setTitle:@"SOLO" forState:UIControlStateSelected];
                }
                btnOn.selected = (_ch17Setting & 0x0100) >> 8;
                btnMeterPrePost.selected = (_ch17Setting & 0x8000) >> 15;
            }

            if (btnDynGate.selected) {
                meterDynOutR.progress = ((float)pkt->ch_17_meter_noise_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_17_noise_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynExpander.selected) {
                meterDynOutR.progress = ((float)pkt->ch_17_meter_expand_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_17_meter_expand_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynComp.selected) {
                meterDynOutR.progress = ((float)pkt->ch_17_meter_compressor_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_17_meter_compressor_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynLimiter.selected) {
                meterDynOutR.progress = ((float)pkt->ch_17_meter_limit_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_17_meter_limit_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            }
            if (![viewController sendCommandExists:CMD_CH17_CTRL dspId:DSP_6]) {
                [self updateDynStatus:pkt->ch_17_ctrl];
            }
            break;
        case CHANNEL_CH_18:
            if (knobGateThreshold.value != pkt->ch_18_gate_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH18_GATE_THRESHOLD dspId:DSP_6]) {
                knobGateThreshold.value = pkt->ch_18_gate_threshold+THRESHOLD_MAX;
                [self knobGateThresholdDidChange:nil];
            }
            if (knobGateRange.value != pkt->ch_18_gate_range+RANGE_MAX &&
                ![viewController sendCommandExists:CMD_CH18_GATE_RANGE dspId:DSP_6]) {
                knobGateRange.value = pkt->ch_18_gate_range+RANGE_MAX;
                [self knobGateRangeDidChange:nil];
            }
            if (knobGateHold.value != pkt->ch_18_gate_hold &&
                ![viewController sendCommandExists:CMD_CH18_GATE_HOLD dspId:DSP_6]) {
                knobGateHold.value = pkt->ch_18_gate_hold;
                [self knobGateHoldDidChange:nil];
            }
            if (knobGateAttack.value != pkt->ch_18_gate_attack &&
                ![viewController sendCommandExists:CMD_CH18_GATE_ATTACK dspId:DSP_6]) {
                knobGateAttack.value = pkt->ch_18_gate_attack;
                [self knobGateAttackDidChange:nil];
            }
            if (knobGateRelease.value != pkt->ch_18_gate_release &&
                ![viewController sendCommandExists:CMD_CH18_GATE_RELEASE dspId:DSP_6]) {
                knobGateRelease.value = pkt->ch_18_gate_release;
                [self knobGateReleaseDidChange:nil];
            }
            if (knobExpanderThreshold.value != pkt->ch_18_expand_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH18_EXPAND_THRESHOLD dspId:DSP_6]) {
                knobExpanderThreshold.value = pkt->ch_18_expand_threshold+THRESHOLD_MAX;
                [self knobExpanderThresholdDidChange:nil];
            }
            if (knobExpanderRatio.value != pkt->ch_18_expand_ratio &&
                ![viewController sendCommandExists:CMD_CH18_EXPAND_RATIO dspId:DSP_6]) {
                knobExpanderRatio.value = pkt->ch_18_expand_ratio;
                [self knobExpanderRatioDidChange:nil];
            }
            if (knobExpanderAttack.value != pkt->ch_18_expand_attack &&
                ![viewController sendCommandExists:CMD_CH18_EXPAND_ATTACK dspId:DSP_6]) {
                knobExpanderAttack.value = pkt->ch_18_expand_attack;
                [self knobExpanderAttackDidChange:nil];
            }
            if (knobExpanderRelease.value != pkt->ch_18_expand_release &&
                ![viewController sendCommandExists:CMD_CH18_EXPAND_RELEASE dspId:DSP_6]) {
                knobExpanderRelease.value = pkt->ch_18_expand_release;
                [self knobExpanderReleaseDidChange:nil];
            }
            if (knobCompThreshold.value != pkt->ch_18_compressor_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH18_COMPRESSOR_THRESHOLD dspId:DSP_6]) {
                knobCompThreshold.value = pkt->ch_18_compressor_threshold+THRESHOLD_MAX;
                [self knobCompThresholdDidChange:nil];
            }
            if (knobCompRatio.value != pkt->ch_18_compressor_ratio &&
                ![viewController sendCommandExists:CMD_CH18_COMPRESSOR_RATIO dspId:DSP_6]) {
                knobCompRatio.value = pkt->ch_18_compressor_ratio;
                [self knobCompRatioDidChange:nil];
            }
            if (knobCompOutputGain.value != pkt->ch_18_compressor_out_gain &&
                ![viewController sendCommandExists:CMD_CH18_COMPRESSOR_OUT_GAIN dspId:DSP_6]) {
                knobCompOutputGain.value = pkt->ch_18_compressor_out_gain;
                [self knobCompOutputGainDidChange:nil];
            }
            if (knobCompAttack.value != pkt->ch_18_compressor_attack &&
                ![viewController sendCommandExists:CMD_CH18_COMPRESSOR_ATTACK dspId:DSP_6]) {
                knobCompAttack.value = pkt->ch_18_compressor_attack;
                [self knobCompAttackDidChange:nil];
            }
            if (knobCompRelease.value != pkt->ch_18_compressor_release &&
                ![viewController sendCommandExists:CMD_CH18_COMPRESSOR_RELEASE dspId:DSP_6]) {
                knobCompRelease.value = pkt->ch_18_compressor_release;
                [self knobCompReleaseDidChange:nil];
            }
            if (knobLimiterThreshold.value != pkt->ch_18_limiter_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_CH18_LIMITER_THRESHOLD dspId:DSP_6]) {
                knobLimiterThreshold.value = pkt->ch_18_limiter_threshold+THRESHOLD_MAX;
                [self knobLimiterThresholdDidChange:nil];
            }
            if (knobLimiterAttack.value != pkt->ch_18_limiter_attack &&
                ![viewController sendCommandExists:CMD_CH18_LIMITER_ATTACK dspId:DSP_6]) {
                knobLimiterAttack.value = pkt->ch_18_limiter_attack;
                [self knobLimiterAttackDidChange:nil];
            }
            if (knobLimiterRelease.value != pkt->ch_18_limiter_release &&
                ![viewController sendCommandExists:CMD_CH18_LIMITER_RELEASE dspId:DSP_6]) {
                knobLimiterRelease.value = pkt->ch_18_limiter_release;
                [self knobLimiterReleaseDidChange:nil];
            }
            if (pkt->ch_18_fader >= 0.0 && pkt->ch_18_fader <= FADER_MAX_VALUE) {
                slider.value = pkt->ch_18_fader;
                [self onSliderAction:nil];
            }
            
            if (![viewController sendCommandExists:CMD_CH18_USB_AUDIO_SETTING dspId:DSP_5]) {
                btnSolo.selected = (_ch18Setting & 0x0400) >> 10;
                BOOL soloSafe = (_ch18Setting & 0x0800) >> 11;
                if (soloSafe) {
                    [btnSolo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
                    [btnSolo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
                } else {
                    [btnSolo setTitle:@"SOLO" forState:UIControlStateNormal];
                    [btnSolo setTitle:@"SOLO" forState:UIControlStateSelected];
                }
                btnOn.selected = (_ch18Setting & 0x0100) >> 8;
                btnMeterPrePost.selected = (_ch18Setting & 0x8000) >> 15;
            }
            
            if (btnDynGate.selected) {
                meterDynOutR.progress = ((float)pkt->ch_18_meter_noise_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_18_noise_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynExpander.selected) {
                meterDynOutR.progress = ((float)pkt->ch_18_meter_expand_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_18_meter_expand_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynComp.selected) {
                meterDynOutR.progress = ((float)pkt->ch_18_meter_compressor_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_18_meter_compressor_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynLimiter.selected) {
                meterDynOutR.progress = ((float)pkt->ch_18_meter_limit_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->ch_18_meter_limit_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            }
            if (![viewController sendCommandExists:CMD_CH18_CTRL dspId:DSP_6]) {
                [self updateDynStatus:pkt->ch_18_ctrl];
            }
            break;
        case CHANNEL_MULTI_1:
            if (knobGateThreshold.value != pkt->multi_1_gate_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_MULTI1_GATE_THRESHOLD dspId:DSP_7]) {
                knobGateThreshold.value = pkt->multi_1_gate_threshold+THRESHOLD_MAX;
                [self knobGateThresholdDidChange:nil];
            }
            if (knobGateRange.value != pkt->multi_1_gate_range+RANGE_MAX &&
                ![viewController sendCommandExists:CMD_MULTI1_GATE_RANGE dspId:DSP_7]) {
                knobGateRange.value = pkt->multi_1_gate_range+RANGE_MAX;
                [self knobGateRangeDidChange:nil];
            }
            if (knobGateHold.value != pkt->multi_1_gate_hold &&
                ![viewController sendCommandExists:CMD_MULTI1_GATE_HOLD dspId:DSP_7]) {
                knobGateHold.value = pkt->multi_1_gate_hold;
                [self knobGateHoldDidChange:nil];
            }
            if (knobGateAttack.value != pkt->multi_1_gate_attack &&
                ![viewController sendCommandExists:CMD_MULTI1_GATE_ATTACK dspId:DSP_7]) {
                knobGateAttack.value = pkt->multi_1_gate_attack;
                [self knobGateAttackDidChange:nil];
            }
            if (knobGateRelease.value != pkt->multi_1_gate_release &&
                ![viewController sendCommandExists:CMD_MULTI1_GATE_RELEASE dspId:DSP_7]) {
                knobGateRelease.value = pkt->multi_1_gate_release;
                [self knobGateReleaseDidChange:nil];
            }
            if (knobExpanderThreshold.value != pkt->multi_1_expand_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_MULTI1_EXPAND_THRESHOLD dspId:DSP_7]) {
                knobExpanderThreshold.value = pkt->multi_1_expand_threshold+THRESHOLD_MAX;
                [self knobExpanderThresholdDidChange:nil];
            }
            if (knobExpanderRatio.value != pkt->multi_1_expand_ratio &&
                ![viewController sendCommandExists:CMD_MULTI1_EXPAND_RATIO dspId:DSP_7]) {
                knobExpanderRatio.value = pkt->multi_1_expand_ratio;
                [self knobExpanderRatioDidChange:nil];
            }
            if (knobExpanderAttack.value != pkt->multi_1_expand_attack &&
                ![viewController sendCommandExists:CMD_MULTI1_EXPAND_ATTACK dspId:DSP_7]) {
                knobExpanderAttack.value = pkt->multi_1_expand_attack;
                [self knobExpanderAttackDidChange:nil];
            }
            if (knobExpanderRelease.value != pkt->multi_1_expand_release &&
                ![viewController sendCommandExists:CMD_MULTI1_EXPAND_RELEASE dspId:DSP_7]) {
                knobExpanderRelease.value = pkt->multi_1_expand_release;
                [self knobExpanderReleaseDidChange:nil];
            }
            if (knobCompThreshold.value != pkt->multi_1_compressor_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_MULTI1_COMPRESSOR_THRESHOLD dspId:DSP_7]) {
                knobCompThreshold.value = pkt->multi_1_compressor_threshold+THRESHOLD_MAX;
                [self knobCompThresholdDidChange:nil];
            }
            if (knobCompRatio.value != pkt->multi_1_compressor_ratio &&
                ![viewController sendCommandExists:CMD_MULTI1_COMPRESSOR_RATIO dspId:DSP_7]) {
                knobCompRatio.value = pkt->multi_1_compressor_ratio;
                [self knobCompRatioDidChange:nil];
            }
            if (knobCompOutputGain.value != pkt->multi_1_compressor_out_gain &&
                ![viewController sendCommandExists:CMD_MULTI1_COMPRESSOR_OUT_GAIN dspId:DSP_7]) {
                knobCompOutputGain.value = pkt->multi_1_compressor_out_gain;
                [self knobCompOutputGainDidChange:nil];
            }
            if (knobCompAttack.value != pkt->multi_1_compressor_attack &&
                ![viewController sendCommandExists:CMD_MULTI1_COMPRESSOR_ATTACK dspId:DSP_7]) {
                knobCompAttack.value = pkt->multi_1_compressor_attack;
                [self knobCompAttackDidChange:nil];
            }
            if (knobCompRelease.value != pkt->multi_1_compressor_release &&
                ![viewController sendCommandExists:CMD_MULTI1_COMPRESSOR_RELEASE dspId:DSP_7]) {
                knobCompRelease.value = pkt->multi_1_compressor_release;
                [self knobCompReleaseDidChange:nil];
            }
            if (knobLimiterThreshold.value != pkt->multi_1_limiter_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_MULTI1_LIMITER_THRESHOLD dspId:DSP_7]) {
                knobLimiterThreshold.value = pkt->multi_1_limiter_threshold+THRESHOLD_MAX;
                [self knobLimiterThresholdDidChange:nil];
            }
            if (knobLimiterAttack.value != pkt->multi_1_limiter_attack &&
                ![viewController sendCommandExists:CMD_MULTI1_LIMITER_ATTACK dspId:DSP_7]) {
                knobLimiterAttack.value = pkt->multi_1_limiter_attack;
                [self knobLimiterAttackDidChange:nil];
            }
            if (knobLimiterRelease.value != pkt->multi_1_limiter_release &&
                ![viewController sendCommandExists:CMD_MULTI1_LIMITER_RELEASE dspId:DSP_7]) {
                knobLimiterRelease.value = pkt->multi_1_limiter_release;
                [self knobLimiterReleaseDidChange:nil];
            }
            if (pkt->multi_1_fader >= 0.0 && pkt->multi_1_fader <= FADER_MAX_VALUE) {
                slider.value = pkt->multi_1_fader;
                [self onSliderAction:nil];
            }
            
            if (![viewController sendCommandExists:CMD_MULTI_1_CTRL dspId:DSP_7]) {
                btnOn.selected = pkt->multi_1_ctrl & 0x01;
            }
            if (![viewController sendCommandExists:CMD_MULTI_METER dspId:DSP_7]) {
                btnMeterPrePost.selected = _multiMeter & 0x01;
            }
            
            if (btnDynGate.selected) {
                meterDynOutR.progress = ((float)pkt->multi_1_meter_noise_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->multi_1_noise_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynExpander.selected) {
                meterDynOutR.progress = ((float)pkt->multi_1_meter_expand_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->multi_1_meter_expand_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynComp.selected) {
                meterDynOutR.progress = ((float)pkt->multi_1_meter_compressor_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->multi_1_meter_compressor_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynLimiter.selected) {
                meterDynOutR.progress = ((float)pkt->multi_1_meter_limit_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->multi_1_meter_limit_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            }
            if (![viewController sendCommandExists:CMD_MULTI_1_CTRL dspId:DSP_7]) {
                [self updateDynStatus:pkt->multi_1_ctrl];
            }
            break;
        case CHANNEL_MULTI_2:
            if (knobGateThreshold.value != pkt->multi_2_gate_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_MULTI2_GATE_THRESHOLD dspId:DSP_7]) {
                knobGateThreshold.value = pkt->multi_2_gate_threshold+THRESHOLD_MAX;
                [self knobGateThresholdDidChange:nil];
            }
            if (knobGateRange.value != pkt->multi_2_gate_range+RANGE_MAX &&
                ![viewController sendCommandExists:CMD_MULTI2_GATE_RANGE dspId:DSP_7]) {
                knobGateRange.value = pkt->multi_2_gate_range+RANGE_MAX;
                [self knobGateRangeDidChange:nil];
            }
            if (knobGateHold.value != pkt->multi_2_gate_hold &&
                ![viewController sendCommandExists:CMD_MULTI2_GATE_HOLD dspId:DSP_7]) {
                knobGateHold.value = pkt->multi_2_gate_hold;
                [self knobGateHoldDidChange:nil];
            }
            if (knobGateAttack.value != pkt->multi_2_gate_attack &&
                ![viewController sendCommandExists:CMD_MULTI2_GATE_ATTACK dspId:DSP_7]) {
                knobGateAttack.value = pkt->multi_2_gate_attack;
                [self knobGateAttackDidChange:nil];
            }
            if (knobGateRelease.value != pkt->multi_2_gate_release &&
                ![viewController sendCommandExists:CMD_MULTI2_GATE_RELEASE dspId:DSP_7]) {
                knobGateRelease.value = pkt->multi_2_gate_release;
                [self knobGateReleaseDidChange:nil];
            }
            if (knobExpanderThreshold.value != pkt->multi_2_expand_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_MULTI2_EXPAND_THRESHOLD dspId:DSP_7]) {
                knobExpanderThreshold.value = pkt->multi_2_expand_threshold+THRESHOLD_MAX;
                [self knobExpanderThresholdDidChange:nil];
            }
            if (knobExpanderRatio.value != pkt->multi_2_expand_ratio &&
                ![viewController sendCommandExists:CMD_MULTI2_EXPAND_RATIO dspId:DSP_7]) {
                knobExpanderRatio.value = pkt->multi_2_expand_ratio;
                [self knobExpanderRatioDidChange:nil];
            }
            if (knobExpanderAttack.value != pkt->multi_2_expand_attack &&
                ![viewController sendCommandExists:CMD_MULTI2_EXPAND_ATTACK dspId:DSP_7]) {
                knobExpanderAttack.value = pkt->multi_2_expand_attack;
                [self knobExpanderAttackDidChange:nil];
            }
            if (knobExpanderRelease.value != pkt->multi_2_expand_release &&
                ![viewController sendCommandExists:CMD_MULTI2_EXPAND_RELEASE dspId:DSP_7]) {
                knobExpanderRelease.value = pkt->multi_2_expand_release;
                [self knobExpanderReleaseDidChange:nil];
            }
            if (knobCompThreshold.value != pkt->multi_2_compressor_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_MULTI2_COMPRESSOR_THRESHOLD dspId:DSP_7]) {
                knobCompThreshold.value = pkt->multi_2_compressor_threshold+THRESHOLD_MAX;
                [self knobCompThresholdDidChange:nil];
            }
            if (knobCompRatio.value != pkt->multi_2_compressor_ratio &&
                ![viewController sendCommandExists:CMD_MULTI2_COMPRESSOR_RATIO dspId:DSP_7]) {
                knobCompRatio.value = pkt->multi_2_compressor_ratio;
                [self knobCompRatioDidChange:nil];
            }
            if (knobCompOutputGain.value != pkt->multi_2_compressor_out_gain &&
                ![viewController sendCommandExists:CMD_MULTI2_COMPRESSOR_OUT_GAIN dspId:DSP_7]) {
                knobCompOutputGain.value = pkt->multi_2_compressor_out_gain;
                [self knobCompOutputGainDidChange:nil];
            }
            if (knobCompAttack.value != pkt->multi_2_compressor_attack &&
                ![viewController sendCommandExists:CMD_MULTI2_COMPRESSOR_ATTACK dspId:DSP_7]) {
                knobCompAttack.value = pkt->multi_2_compressor_attack;
                [self knobCompAttackDidChange:nil];
            }
            if (knobCompRelease.value != pkt->multi_2_compressor_release &&
                ![viewController sendCommandExists:CMD_MULTI2_COMPRESSOR_RELEASE dspId:DSP_7]) {
                knobCompRelease.value = pkt->multi_2_compressor_release;
                [self knobCompReleaseDidChange:nil];
            }
            if (knobLimiterThreshold.value != pkt->multi_2_limiter_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_MULTI2_LIMITER_THRESHOLD dspId:DSP_7]) {
                knobLimiterThreshold.value = pkt->multi_2_limiter_threshold+THRESHOLD_MAX;
                [self knobLimiterThresholdDidChange:nil];
            }
            if (knobLimiterAttack.value != pkt->multi_2_limiter_attack &&
                ![viewController sendCommandExists:CMD_MULTI2_LIMITER_ATTACK dspId:DSP_7]) {
                knobLimiterAttack.value = pkt->multi_2_limiter_attack;
                [self knobLimiterAttackDidChange:nil];
            }
            if (knobLimiterRelease.value != pkt->multi_2_limiter_release &&
                ![viewController sendCommandExists:CMD_MULTI2_LIMITER_RELEASE dspId:DSP_7]) {
                knobLimiterRelease.value = pkt->multi_2_limiter_release;
                [self knobLimiterReleaseDidChange:nil];
            }
            if (pkt->multi_2_fader >= 0.0 && pkt->multi_2_fader <= FADER_MAX_VALUE) {
                slider.value = pkt->multi_2_fader;
                [self onSliderAction:nil];
            }
            
            if (![viewController sendCommandExists:CMD_MULTI_2_CTRL dspId:DSP_7]) {
                btnOn.selected = pkt->multi_2_ctrl & 0x01;
            }
            if (![viewController sendCommandExists:CMD_MULTI_METER dspId:DSP_7]) {
                btnMeterPrePost.selected = (_multiMeter & 0x02) >> 1;
            }
            
            if (btnDynGate.selected) {
                meterDynOutR.progress = ((float)pkt->multi_2_meter_noise_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->multi_2_noise_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynExpander.selected) {
                meterDynOutR.progress = ((float)pkt->multi_2_meter_expand_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->multi_2_meter_expand_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynComp.selected) {
                meterDynOutR.progress = ((float)pkt->multi_2_meter_compressor_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->multi_2_meter_compressor_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynLimiter.selected) {
                meterDynOutR.progress = ((float)pkt->multi_2_meter_limit_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->multi_2_meter_limit_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            }
            if (![viewController sendCommandExists:CMD_MULTI_2_CTRL dspId:DSP_7]) {
                [self updateDynStatus:pkt->multi_2_ctrl];
            }
            break;
        case CHANNEL_MULTI_3:
            if (knobGateThreshold.value != pkt->multi_3_gate_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_MULTI3_GATE_THRESHOLD dspId:DSP_7]) {
                knobGateThreshold.value = pkt->multi_3_gate_threshold+THRESHOLD_MAX;
                [self knobGateThresholdDidChange:nil];
            }
            if (knobGateRange.value != pkt->multi_3_gate_range+RANGE_MAX &&
                ![viewController sendCommandExists:CMD_MULTI3_GATE_RANGE dspId:DSP_7]) {
                knobGateRange.value = pkt->multi_3_gate_range+RANGE_MAX;
                [self knobGateRangeDidChange:nil];
            }
            if (knobGateHold.value != pkt->multi_3_gate_hold &&
                ![viewController sendCommandExists:CMD_MULTI3_GATE_HOLD dspId:DSP_7]) {
                knobGateHold.value = pkt->multi_3_gate_hold;
                [self knobGateHoldDidChange:nil];
            }
            if (knobGateAttack.value != pkt->multi_3_gate_attack &&
                ![viewController sendCommandExists:CMD_MULTI3_GATE_ATTACK dspId:DSP_7]) {
                knobGateAttack.value = pkt->multi_3_gate_attack;
                [self knobGateAttackDidChange:nil];
            }
            if (knobGateRelease.value != pkt->multi_3_gate_release &&
                ![viewController sendCommandExists:CMD_MULTI3_GATE_RELEASE dspId:DSP_7]) {
                knobGateRelease.value = pkt->multi_3_gate_release;
                [self knobGateReleaseDidChange:nil];
            }
            if (knobExpanderThreshold.value != pkt->multi_3_expand_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_MULTI3_EXPAND_THRESHOLD dspId:DSP_7]) {
                knobExpanderThreshold.value = pkt->multi_3_expand_threshold+THRESHOLD_MAX;
                [self knobExpanderThresholdDidChange:nil];
            }
            if (knobExpanderRatio.value != pkt->multi_3_expand_ratio &&
                ![viewController sendCommandExists:CMD_MULTI3_EXPAND_RATIO dspId:DSP_7]) {
                knobExpanderRatio.value = pkt->multi_3_expand_ratio;
                [self knobExpanderRatioDidChange:nil];
            }
            if (knobExpanderAttack.value != pkt->multi_3_expand_attack &&
                ![viewController sendCommandExists:CMD_MULTI3_EXPAND_ATTACK dspId:DSP_7]) {
                knobExpanderAttack.value = pkt->multi_3_expand_attack;
                [self knobExpanderAttackDidChange:nil];
            }
            if (knobExpanderRelease.value != pkt->multi_3_expand_release &&
                ![viewController sendCommandExists:CMD_MULTI3_EXPAND_RELEASE dspId:DSP_7]) {
                knobExpanderRelease.value = pkt->multi_3_expand_release;
                [self knobExpanderReleaseDidChange:nil];
            }
            if (knobCompThreshold.value != pkt->multi_3_compressor_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_MULTI3_COMPRESSOR_THRESHOLD dspId:DSP_7]) {
                knobCompThreshold.value = pkt->multi_3_compressor_threshold+THRESHOLD_MAX;
                [self knobCompThresholdDidChange:nil];
            }
            if (knobCompRatio.value != pkt->multi_3_compressor_ratio &&
                ![viewController sendCommandExists:CMD_MULTI3_COMPRESSOR_RATIO dspId:DSP_7]) {
                knobCompRatio.value = pkt->multi_3_compressor_ratio;
                [self knobCompRatioDidChange:nil];
            }
            if (knobCompOutputGain.value != pkt->multi_3_compressor_out_gain &&
                ![viewController sendCommandExists:CMD_MULTI3_COMPRESSOR_OUT_GAIN dspId:DSP_7]) {
                knobCompOutputGain.value = pkt->multi_3_compressor_out_gain;
                [self knobCompOutputGainDidChange:nil];
            }
            if (knobCompAttack.value != pkt->multi_3_compressor_attack &&
                ![viewController sendCommandExists:CMD_MULTI3_COMPRESSOR_ATTACK dspId:DSP_7]) {
                knobCompAttack.value = pkt->multi_3_compressor_attack;
                [self knobCompAttackDidChange:nil];
            }
            if (knobCompRelease.value != pkt->multi_3_compressor_release &&
                ![viewController sendCommandExists:CMD_MULTI3_COMPRESSOR_RELEASE dspId:DSP_7]) {
                knobCompRelease.value = pkt->multi_3_compressor_release;
                [self knobCompReleaseDidChange:nil];
            }
            if (knobLimiterThreshold.value != pkt->multi_3_limiter_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_MULTI3_LIMITER_THRESHOLD dspId:DSP_7]) {
                knobLimiterThreshold.value = pkt->multi_3_limiter_threshold+THRESHOLD_MAX;
                [self knobLimiterThresholdDidChange:nil];
            }
            if (knobLimiterAttack.value != pkt->multi_3_limiter_attack &&
                ![viewController sendCommandExists:CMD_MULTI3_LIMITER_ATTACK dspId:DSP_7]) {
                knobLimiterAttack.value = pkt->multi_3_limiter_attack;
                [self knobLimiterAttackDidChange:nil];
            }
            if (knobLimiterRelease.value != pkt->multi_3_limiter_release &&
                ![viewController sendCommandExists:CMD_MULTI3_LIMITER_RELEASE dspId:DSP_7]) {
                knobLimiterRelease.value = pkt->multi_3_limiter_release;
                [self knobLimiterReleaseDidChange:nil];
            }
            if (pkt->multi_3_fader >= 0.0 && pkt->multi_3_fader <= FADER_MAX_VALUE) {
                slider.value = pkt->multi_3_fader;
                [self onSliderAction:nil];
            }
            
            if (![viewController sendCommandExists:CMD_MULTI_3_CTRL dspId:DSP_7]) {
                btnOn.selected = pkt->multi_3_ctrl & 0x01;
            }
            if (![viewController sendCommandExists:CMD_MULTI_METER dspId:DSP_7]) {
                btnMeterPrePost.selected = (_multiMeter & 0x04) >> 2;
            }
            
            if (btnDynGate.selected) {
                meterDynOutR.progress = ((float)pkt->multi_3_meter_noise_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->multi_3_noise_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynExpander.selected) {
                meterDynOutR.progress = ((float)pkt->multi_3_meter_expand_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->multi_3_meter_expand_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynComp.selected) {
                meterDynOutR.progress = ((float)pkt->multi_3_meter_compressor_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->multi_3_meter_compressor_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynLimiter.selected) {
                meterDynOutR.progress = ((float)pkt->multi_3_meter_limit_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->multi_3_meter_limit_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            }
            if (![viewController sendCommandExists:CMD_MULTI_3_CTRL dspId:DSP_7]) {
                [self updateDynStatus:pkt->multi_3_ctrl];
            }
            break;
        case CHANNEL_MULTI_4:
            if (knobGateThreshold.value != pkt->multi_4_gate_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_MULTI4_GATE_THRESHOLD dspId:DSP_7]) {
                knobGateThreshold.value = pkt->multi_4_gate_threshold+THRESHOLD_MAX;
                [self knobGateThresholdDidChange:nil];
            }
            if (knobGateRange.value != pkt->multi_4_gate_range+RANGE_MAX &&
                ![viewController sendCommandExists:CMD_MULTI4_GATE_RANGE dspId:DSP_7]) {
                knobGateRange.value = pkt->multi_4_gate_range+RANGE_MAX;
                [self knobGateRangeDidChange:nil];
            }
            if (knobGateHold.value != pkt->multi_4_gate_hold &&
                ![viewController sendCommandExists:CMD_MULTI4_GATE_HOLD dspId:DSP_7]) {
                knobGateHold.value = pkt->multi_4_gate_hold;
                [self knobGateHoldDidChange:nil];
            }
            if (knobGateAttack.value != pkt->multi_4_gate_attack &&
                ![viewController sendCommandExists:CMD_MULTI4_GATE_ATTACK dspId:DSP_7]) {
                knobGateAttack.value = pkt->multi_4_gate_attack;
                [self knobGateAttackDidChange:nil];
            }
            if (knobGateRelease.value != pkt->multi_4_gate_release &&
                ![viewController sendCommandExists:CMD_MULTI4_GATE_RELEASE dspId:DSP_7]) {
                knobGateRelease.value = pkt->multi_4_gate_release;
                [self knobGateReleaseDidChange:nil];
            }
            if (knobExpanderThreshold.value != pkt->multi_4_expand_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_MULTI4_EXPAND_THRESHOLD dspId:DSP_7]) {
                knobExpanderThreshold.value = pkt->multi_4_expand_threshold+THRESHOLD_MAX;
                [self knobExpanderThresholdDidChange:nil];
            }
            if (knobExpanderRatio.value != pkt->multi_4_expand_ratio &&
                ![viewController sendCommandExists:CMD_MULTI4_EXPAND_RATIO dspId:DSP_7]) {
                knobExpanderRatio.value = pkt->multi_4_expand_ratio;
                [self knobExpanderRatioDidChange:nil];
            }
            if (knobExpanderAttack.value != pkt->multi_4_expand_attack &&
                ![viewController sendCommandExists:CMD_MULTI4_EXPAND_ATTACK dspId:DSP_7]) {
                knobExpanderAttack.value = pkt->multi_4_expand_attack;
                [self knobExpanderAttackDidChange:nil];
            }
            if (knobExpanderRelease.value != pkt->multi_4_expand_release &&
                ![viewController sendCommandExists:CMD_MULTI4_EXPAND_RELEASE dspId:DSP_7]) {
                knobExpanderRelease.value = pkt->multi_4_expand_release;
                [self knobExpanderReleaseDidChange:nil];
            }
            if (knobCompThreshold.value != pkt->multi_4_compressor_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_MULTI4_COMPRESSOR_THRESHOLD dspId:DSP_7]) {
                knobCompThreshold.value = pkt->multi_4_compressor_threshold+THRESHOLD_MAX;
                [self knobCompThresholdDidChange:nil];
            }
            if (knobCompRatio.value != pkt->multi_4_compressor_ratio &&
                ![viewController sendCommandExists:CMD_MULTI4_COMPRESSOR_RATIO dspId:DSP_7]) {
                knobCompRatio.value = pkt->multi_4_compressor_ratio;
                [self knobCompRatioDidChange:nil];
            }
            if (knobCompOutputGain.value != pkt->multi_4_compressor_out_gain &&
                ![viewController sendCommandExists:CMD_MULTI4_COMPRESSOR_OUT_GAIN dspId:DSP_7]) {
                knobCompOutputGain.value = pkt->multi_4_compressor_out_gain;
                [self knobCompOutputGainDidChange:nil];
            }
            if (knobCompAttack.value != pkt->multi_4_compressor_attack &&
                ![viewController sendCommandExists:CMD_MULTI4_COMPRESSOR_ATTACK dspId:DSP_7]) {
                knobCompAttack.value = pkt->multi_4_compressor_attack;
                [self knobCompAttackDidChange:nil];
            }
            if (knobCompRelease.value != pkt->multi_4_compressor_release &&
                ![viewController sendCommandExists:CMD_MULTI4_COMPRESSOR_RELEASE dspId:DSP_7]) {
                knobCompRelease.value = pkt->multi_4_compressor_release;
                [self knobCompReleaseDidChange:nil];
            }
            if (knobLimiterThreshold.value != pkt->multi_4_limiter_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_MULTI4_LIMITER_THRESHOLD dspId:DSP_7]) {
                knobLimiterThreshold.value = pkt->multi_4_limiter_threshold+THRESHOLD_MAX;
                [self knobLimiterThresholdDidChange:nil];
            }
            if (knobLimiterAttack.value != pkt->multi_4_limiter_attack &&
                ![viewController sendCommandExists:CMD_MULTI4_LIMITER_ATTACK dspId:DSP_7]) {
                knobLimiterAttack.value = pkt->multi_4_limiter_attack;
                [self knobLimiterAttackDidChange:nil];
            }
            if (knobLimiterRelease.value != pkt->multi_4_limiter_release &&
                ![viewController sendCommandExists:CMD_MULTI4_LIMITER_RELEASE dspId:DSP_7]) {
                knobLimiterRelease.value = pkt->multi_4_limiter_release;
                [self knobLimiterReleaseDidChange:nil];
            }
            if (pkt->multi_4_fader >= 0.0 && pkt->multi_4_fader <= FADER_MAX_VALUE) {
                slider.value = pkt->multi_4_fader;
                [self onSliderAction:nil];
            }
            
            if (![viewController sendCommandExists:CMD_MULTI_4_CTRL dspId:DSP_7]) {
                btnOn.selected = pkt->multi_4_ctrl & 0x01;
            }
            if (![viewController sendCommandExists:CMD_MULTI_METER dspId:DSP_7]) {
                btnMeterPrePost.selected = (_multiMeter & 0x08) >> 3;
            }

            if (btnDynGate.selected) {
                meterDynOutR.progress = ((float)pkt->multi_4_meter_noise_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->multi_4_noise_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynExpander.selected) {
                meterDynOutR.progress = ((float)pkt->multi_4_meter_expand_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->multi_4_meter_expand_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynComp.selected) {
                meterDynOutR.progress = ((float)pkt->multi_4_meter_compressor_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->multi_4_meter_compressor_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynLimiter.selected) {
                meterDynOutR.progress = ((float)pkt->multi_4_meter_limit_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->multi_4_meter_limit_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            }
            if (![viewController sendCommandExists:CMD_MULTI_4_CTRL dspId:DSP_7]) {
                [self updateDynStatus:pkt->multi_4_ctrl];
            }
            break;
        case CHANNEL_MAIN:
            if (knobGateThreshold.value != pkt->main_l_r_gate_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_MAIN_LR_GATE_THRESHOLD dspId:DSP_6]) {
                knobGateThreshold.value = pkt->main_l_r_gate_threshold+THRESHOLD_MAX;
                [self knobGateThresholdDidChange:nil];
            }
            if (knobGateRange.value != pkt->main_l_r_gate_range+RANGE_MAX &&
                ![viewController sendCommandExists:CMD_MAIN_LR_GATE_RANGE dspId:DSP_6]) {
                knobGateRange.value = pkt->main_l_r_gate_range+RANGE_MAX;
                [self knobGateRangeDidChange:nil];
            }
            if (knobGateHold.value != pkt->main_l_r_gate_hold &&
                ![viewController sendCommandExists:CMD_MAIN_LR_GATE_HOLD dspId:DSP_6]) {
                knobGateHold.value = pkt->main_l_r_gate_hold;
                [self knobGateHoldDidChange:nil];
            }
            if (knobGateAttack.value != pkt->main_l_r_gate_attack &&
                ![viewController sendCommandExists:CMD_MAIN_LR_GATE_ATTACK dspId:DSP_6]) {
                knobGateAttack.value = pkt->main_l_r_gate_attack;
                [self knobGateAttackDidChange:nil];
            }
            if (knobGateRelease.value != pkt->main_l_r_gate_release &&
                ![viewController sendCommandExists:CMD_MAIN_LR_GATE_RELEASE dspId:DSP_6]) {
                knobGateRelease.value = pkt->main_l_r_gate_release;
                [self knobGateReleaseDidChange:nil];
            }
            if (knobExpanderThreshold.value != pkt->main_l_r_expand_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_MAIN_LR_EXPAND_THRESHOLD dspId:DSP_6]) {
                knobExpanderThreshold.value = pkt->main_l_r_expand_threshold+THRESHOLD_MAX;
                [self knobExpanderThresholdDidChange:nil];
            }
            if (knobExpanderRatio.value != pkt->main_l_r_expand_ratio &&
                ![viewController sendCommandExists:CMD_MAIN_LR_EXPAND_RATIO dspId:DSP_6]) {
                knobExpanderRatio.value = pkt->main_l_r_expand_ratio;
                [self knobExpanderRatioDidChange:nil];
            }
            if (knobExpanderAttack.value != pkt->main_l_r_expand_attack &&
                ![viewController sendCommandExists:CMD_MAIN_LR_EXPAND_ATTACK dspId:DSP_6]) {
                knobExpanderAttack.value = pkt->main_l_r_expand_attack;
                [self knobExpanderAttackDidChange:nil];
            }
            if (knobExpanderRelease.value != pkt->main_l_r_expand_release &&
                ![viewController sendCommandExists:CMD_MAIN_LR_EXPAND_RELEASE dspId:DSP_6]) {
                knobExpanderRelease.value = pkt->main_l_r_expand_release;
                [self knobExpanderReleaseDidChange:nil];
            }
            if (knobCompThreshold.value != pkt->main_l_r_compressor_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_MAIN_LR_COMPRESSOR_THRESHOLD dspId:DSP_6]) {
                knobCompThreshold.value = pkt->main_l_r_compressor_threshold+THRESHOLD_MAX;
                [self knobCompThresholdDidChange:nil];
            }
            if (knobCompRatio.value != pkt->main_l_r_compressor_ratio &&
                ![viewController sendCommandExists:CMD_MAIN_LR_COMPRESSOR_RATIO dspId:DSP_6]) {
                knobCompRatio.value = pkt->main_l_r_compressor_ratio;
                [self knobCompRatioDidChange:nil];
            }
            if (knobCompOutputGain.value != pkt->main_l_r_compressor_out_gain &&
                ![viewController sendCommandExists:CMD_MAIN_LR_COMPRESSOR_OUT_GAIN dspId:DSP_6]) {
                knobCompOutputGain.value = pkt->main_l_r_compressor_out_gain;
                [self knobCompOutputGainDidChange:nil];
            }
            if (knobCompAttack.value != pkt->main_l_r_compressor_attack &&
                ![viewController sendCommandExists:CMD_MAIN_LR_COMPRESSOR_ATTACK dspId:DSP_6]) {
                knobCompAttack.value = pkt->main_l_r_compressor_attack;
                [self knobCompAttackDidChange:nil];
            }
            if (knobCompRelease.value != pkt->main_l_r_compressor_release &&
                ![viewController sendCommandExists:CMD_MAIN_LR_COMPRESSOR_RELEASE dspId:DSP_6]) {
                knobCompRelease.value = pkt->main_l_r_compressor_release;
                [self knobCompReleaseDidChange:nil];
            }
            if (knobLimiterThreshold.value != pkt->main_l_r_limiter_threshold+THRESHOLD_MAX &&
                ![viewController sendCommandExists:CMD_MAIN_LR_LIMITER_THRESHOLD dspId:DSP_6]) {
                knobLimiterThreshold.value = pkt->main_l_r_limiter_threshold+THRESHOLD_MAX;
                [self knobLimiterThresholdDidChange:nil];
            }
            if (knobLimiterAttack.value != pkt->main_l_r_limiter_attack &&
                ![viewController sendCommandExists:CMD_MAIN_LR_LIMITER_ATTACK dspId:DSP_6]) {
                knobLimiterAttack.value = pkt->main_l_r_limiter_attack;
                [self knobLimiterAttackDidChange:nil];
            }
            if (knobLimiterRelease.value != pkt->main_l_r_limiter_release &&
                ![viewController sendCommandExists:CMD_MAIN_LR_LIMITER_RELEASE dspId:DSP_6]) {
                knobLimiterRelease.value = pkt->main_l_r_limiter_release;
                [self knobLimiterReleaseDidChange:nil];
            }
            if (pkt->main_fader >= 0.0 && pkt->main_fader <= FADER_MAX_VALUE) {
                slider.value = pkt->main_fader;
                [self onSliderAction:nil];
            }
            
            if (![viewController sendCommandExists:CMD_MAIN_METER dspId:DSP_6]) {
                btnSolo.selected = (pkt->main_meter_pre_post & 0x80) >> 7;
            }
            if (![viewController sendCommandExists:CMD_MAIN_LR_CTRL dspId:DSP_6]) {
                btnOn.selected = pkt->main_l_r_ctrl & 0x01;
            }
            if (![viewController sendCommandExists:CMD_MAIN_METER dspId:DSP_6]) {
                btnMeterPrePost.selected = _mainMeter & 0x01;
            }
            
            if (btnDynGate.selected) {
                meterDynOutL.progress = ((float)pkt->main_l_meter_noise_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynOutR.progress = ((float)pkt->main_r_meter_noise_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrL.progress = DYN_GR_OUT_OFFSET - ((float)pkt->main_l_noise_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->main_r_noise_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynExpander.selected) {
                meterDynOutL.progress = ((float)pkt->main_l_meter_expand_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynOutR.progress = ((float)pkt->main_r_meter_expand_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrL.progress = DYN_GR_OUT_OFFSET - ((float)pkt->main_l_meter_expand_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->main_r_meter_expand_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynComp.selected) {
                meterDynOutL.progress = ((float)pkt->main_l_meter_compressor_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynOutR.progress = ((float)pkt->main_r_meter_compressor_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrL.progress = DYN_GR_OUT_OFFSET - ((float)pkt->main_l_meter_compressor_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->main_r_meter_compressor_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            } else if (btnDynLimiter.selected) {
                meterDynOutL.progress = ((float)pkt->main_l_meter_limit_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynOutR.progress = ((float)pkt->main_r_meter_limit_out/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrL.progress = DYN_GR_OUT_OFFSET - ((float)pkt->main_l_meter_limit_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
                meterDynGrR.progress = DYN_GR_OUT_OFFSET - ((float)pkt->main_r_meter_limit_gain_reduce/128.0 + DYN_METER_MAX_VALUE)/DYN_METER_MAX_VALUE;
            }
            if (![viewController sendCommandExists:CMD_MAIN_LR_CTRL dspId:DSP_6]) {
                [self updateDynStatus:pkt->main_l_r_ctrl];
            }
            break;
        default:
            break;
    }
}

- (void)processChannel1_8PageReply:(ChannelCh1_8PagePacket *)pkt {
    
    BOOL valueChanged = NO;
    
    _ch1Ctrl = pkt->ch_1_ctrl;
    _ch2Ctrl = pkt->ch_2_ctrl;
    _ch3Ctrl = pkt->ch_3_ctrl;
    _ch4Ctrl = pkt->ch_4_ctrl;
    _ch5Ctrl = pkt->ch_5_ctrl;
    _ch6Ctrl = pkt->ch_6_ctrl;
    _ch7Ctrl = pkt->ch_7_ctrl;
    _ch8Ctrl = pkt->ch_8_ctrl;
    _mainCtrl = pkt->main_l_r_ctrl;
    
    // Only handle when DYN view is hidden
    if (!self.view.hidden) {
        NSLog(@"processChannel1_8PageReply: view is visible!");
        return;
    }
    
    // CHANNEL_CH_1
    currentChannel = CHANNEL_CH_1;
    if (dynGateThreshold[currentChannel] != pkt->ch_1_gate_threshold) {
        dynGateThreshold[currentChannel] = pkt->ch_1_gate_threshold;
        valueChanged = YES;
    }
    if (dynGateRange[currentChannel] != pkt->ch_1_gate_range) {
        dynGateRange[currentChannel] = pkt->ch_1_gate_range;
        valueChanged = YES;
    }
    if (dynExpThreshold[currentChannel] != pkt->ch_1_expand_threshold) {
        dynExpThreshold[currentChannel] = pkt->ch_1_expand_threshold;
        valueChanged = YES;
    }
    if (dynExpRatio[currentChannel] != [Global getRatioValue:pkt->ch_1_expand_ratio]) {
        dynExpRatio[currentChannel] = [Global getRatioValue:pkt->ch_1_expand_ratio];
        valueChanged = YES;
    }
    if (dynCompThreshold[currentChannel] != pkt->ch_1_compressor_threshold) {
        dynCompThreshold[currentChannel] = pkt->ch_1_compressor_threshold;
        valueChanged = YES;
    }
    if (dynCompRatio[currentChannel] != [Global getRatioCompValue:pkt->ch_1_compressor_ratio]) {
        dynCompRatio[currentChannel] = [Global getRatioCompValue:pkt->ch_1_compressor_ratio];
        valueChanged = YES;
    }
    if (dynLimitThreshold[currentChannel] != pkt->ch_1_limiter_threshold) {
        dynLimitThreshold[currentChannel] = pkt->ch_1_limiter_threshold;
        valueChanged = YES;
    }
    [self updateDynStatus:pkt->ch_1_ctrl];
    
    if (valueChanged) {
        [self calculateDyn];
        [self updatePreviewImageForChannel:currentChannel];
        valueChanged = NO;
    }
    
    // CHANNEL_CH_2
    currentChannel = CHANNEL_CH_2;
    if (dynGateThreshold[currentChannel] != pkt->ch_2_gate_threshold) {
        dynGateThreshold[currentChannel] = pkt->ch_2_gate_threshold;
        valueChanged = YES;
    }
    if (dynGateRange[currentChannel] != pkt->ch_2_gate_range) {
        dynGateRange[currentChannel] = pkt->ch_2_gate_range;
        valueChanged = YES;
    }
    if (dynExpThreshold[currentChannel] != pkt->ch_2_expand_threshold) {
        dynExpThreshold[currentChannel] = pkt->ch_2_expand_threshold;
        valueChanged = YES;
    }
    if (dynExpRatio[currentChannel] != [Global getRatioValue:pkt->ch_2_expand_ratio]) {
        dynExpRatio[currentChannel] = [Global getRatioValue:pkt->ch_2_expand_ratio];
        valueChanged = YES;
    }
    if (dynCompThreshold[currentChannel] != pkt->ch_2_compressor_threshold) {
        dynCompThreshold[currentChannel] = pkt->ch_2_compressor_threshold;
        valueChanged = YES;
    }
    if (dynCompRatio[currentChannel] != [Global getRatioCompValue:pkt->ch_2_compressor_ratio]) {
        dynCompRatio[currentChannel] = [Global getRatioCompValue:pkt->ch_2_compressor_ratio];
        valueChanged = YES;
    }
    if (dynLimitThreshold[currentChannel] != pkt->ch_2_limiter_threshold) {
        dynLimitThreshold[currentChannel] = pkt->ch_2_limiter_threshold;
        valueChanged = YES;
    }
    [self updateDynStatus:pkt->ch_2_ctrl];
    
    if (valueChanged) {
        [self calculateDyn];
        [self updatePreviewImageForChannel:currentChannel];
        valueChanged = NO;
    }
    
    // CHANNEL_CH_3
    currentChannel = CHANNEL_CH_3;
    if (dynGateThreshold[currentChannel] != pkt->ch_3_gate_threshold) {
        dynGateThreshold[currentChannel] = pkt->ch_3_gate_threshold;
        valueChanged = YES;
    }
    if (dynGateRange[currentChannel] != pkt->ch_3_gate_range) {
        dynGateRange[currentChannel] = pkt->ch_3_gate_range;
        valueChanged = YES;
    }
    if (dynExpThreshold[currentChannel] != pkt->ch_3_expand_threshold) {
        dynExpThreshold[currentChannel] = pkt->ch_3_expand_threshold;
        valueChanged = YES;
    }
    if (dynExpRatio[currentChannel] != [Global getRatioValue:pkt->ch_3_expand_ratio]) {
        dynExpRatio[currentChannel] = [Global getRatioValue:pkt->ch_3_expand_ratio];
        valueChanged = YES;
    }
    if (dynCompThreshold[currentChannel] != pkt->ch_3_compressor_threshold) {
        dynCompThreshold[currentChannel] = pkt->ch_3_compressor_threshold;
        valueChanged = YES;
    }
    if (dynCompRatio[currentChannel] != [Global getRatioCompValue:pkt->ch_3_compressor_ratio]) {
        dynCompRatio[currentChannel] = [Global getRatioCompValue:pkt->ch_3_compressor_ratio];
        valueChanged = YES;
    }
    if (dynLimitThreshold[currentChannel] != pkt->ch_3_limiter_threshold) {
        dynLimitThreshold[currentChannel] = pkt->ch_3_limiter_threshold;
        valueChanged = YES;
    }
    [self updateDynStatus:pkt->ch_3_ctrl];
    
    if (valueChanged) {
        [self calculateDyn];
        [self updatePreviewImageForChannel:currentChannel];
        valueChanged = NO;
    }
    
    // CHANNEL_CH_4
    currentChannel = CHANNEL_CH_4;
    if (dynGateThreshold[currentChannel] != pkt->ch_4_gate_threshold) {
        dynGateThreshold[currentChannel] = pkt->ch_4_gate_threshold;
        valueChanged = YES;
    }
    if (dynGateRange[currentChannel] != pkt->ch_4_gate_range) {
        dynGateRange[currentChannel] = pkt->ch_4_gate_range;
        valueChanged = YES;
    }
    if (dynExpThreshold[currentChannel] != pkt->ch_4_expand_threshold) {
        dynExpThreshold[currentChannel] = pkt->ch_4_expand_threshold;
        valueChanged = YES;
    }
    if (dynExpRatio[currentChannel] != [Global getRatioValue:pkt->ch_4_expand_ratio]) {
        dynExpRatio[currentChannel] = [Global getRatioValue:pkt->ch_4_expand_ratio];
        valueChanged = YES;
    }
    if (dynCompThreshold[currentChannel] != pkt->ch_4_compressor_threshold) {
        dynCompThreshold[currentChannel] = pkt->ch_4_compressor_threshold;
        valueChanged = YES;
    }
    if (dynCompRatio[currentChannel] != [Global getRatioCompValue:pkt->ch_4_compressor_ratio]) {
        dynCompRatio[currentChannel] = [Global getRatioCompValue:pkt->ch_4_compressor_ratio];
        valueChanged = YES;
    }
    if (dynLimitThreshold[currentChannel] != pkt->ch_4_limiter_threshold) {
        dynLimitThreshold[currentChannel] = pkt->ch_4_limiter_threshold;
        valueChanged = YES;
    }
    [self updateDynStatus:pkt->ch_4_ctrl];
    
    if (valueChanged) {
        [self calculateDyn];
        [self updatePreviewImageForChannel:currentChannel];
        valueChanged = NO;
    }
    
    // CHANNEL_CH_5
    currentChannel = CHANNEL_CH_5;
    if (dynGateThreshold[currentChannel] != pkt->ch_5_gate_threshold) {
        dynGateThreshold[currentChannel] = pkt->ch_5_gate_threshold;
        valueChanged = YES;
    }
    if (dynGateRange[currentChannel] != pkt->ch_5_gate_range) {
        dynGateRange[currentChannel] = pkt->ch_5_gate_range;
        valueChanged = YES;
    }
    if (dynExpThreshold[currentChannel] != pkt->ch_5_expand_threshold) {
        dynExpThreshold[currentChannel] = pkt->ch_5_expand_threshold;
        valueChanged = YES;
    }
    if (dynExpRatio[currentChannel] != [Global getRatioValue:pkt->ch_5_expand_ratio]) {
        dynExpRatio[currentChannel] = [Global getRatioValue:pkt->ch_5_expand_ratio];
        valueChanged = YES;
    }
    if (dynCompThreshold[currentChannel] != pkt->ch_5_compressor_threshold) {
        dynCompThreshold[currentChannel] = pkt->ch_5_compressor_threshold;
        valueChanged = YES;
    }
    if (dynCompRatio[currentChannel] != [Global getRatioCompValue:pkt->ch_5_compressor_ratio]) {
        dynCompRatio[currentChannel] = [Global getRatioCompValue:pkt->ch_5_compressor_ratio];
        valueChanged = YES;
    }
    if (dynLimitThreshold[currentChannel] != pkt->ch_5_limiter_threshold) {
        dynLimitThreshold[currentChannel] = pkt->ch_5_limiter_threshold;
        valueChanged = YES;
    }
    [self updateDynStatus:pkt->ch_5_ctrl];
    
    if (valueChanged) {
        [self calculateDyn];
        [self updatePreviewImageForChannel:currentChannel];
        valueChanged = NO;
    }
    
    // CHANNEL_CH_6
    currentChannel = CHANNEL_CH_6;
    if (dynGateThreshold[currentChannel] != pkt->ch_6_gate_threshold) {
        dynGateThreshold[currentChannel] = pkt->ch_6_gate_threshold;
        valueChanged = YES;
    }
    if (dynGateRange[currentChannel] != pkt->ch_6_gate_range) {
        dynGateRange[currentChannel] = pkt->ch_6_gate_range;
        valueChanged = YES;
    }
    if (dynExpThreshold[currentChannel] != pkt->ch_6_expand_threshold) {
        dynExpThreshold[currentChannel] = pkt->ch_6_expand_threshold;
        valueChanged = YES;
    }
    if (dynExpRatio[currentChannel] != [Global getRatioValue:pkt->ch_6_expand_ratio]) {
        dynExpRatio[currentChannel] = [Global getRatioValue:pkt->ch_6_expand_ratio];
        valueChanged = YES;
    }
    if (dynCompThreshold[currentChannel] != pkt->ch_6_compressor_threshold) {
        dynCompThreshold[currentChannel] = pkt->ch_6_compressor_threshold;
        valueChanged = YES;
    }
    if (dynCompRatio[currentChannel] != [Global getRatioCompValue:pkt->ch_6_compressor_ratio]) {
        dynCompRatio[currentChannel] = [Global getRatioCompValue:pkt->ch_6_compressor_ratio];
        valueChanged = YES;
    }
    if (dynLimitThreshold[currentChannel] != pkt->ch_6_limiter_threshold) {
        dynLimitThreshold[currentChannel] = pkt->ch_6_limiter_threshold;
        valueChanged = YES;
    }
    [self updateDynStatus:pkt->ch_6_ctrl];
    
    if (valueChanged) {
        [self calculateDyn];
        [self updatePreviewImageForChannel:currentChannel];
        valueChanged = NO;
    }
    
    // CHANNEL_CH_7
    currentChannel = CHANNEL_CH_7;
    if (dynGateThreshold[currentChannel] != pkt->ch_7_gate_threshold) {
        dynGateThreshold[currentChannel] = pkt->ch_7_gate_threshold;
        valueChanged = YES;
    }
    if (dynGateRange[currentChannel] != pkt->ch_7_gate_range) {
        dynGateRange[currentChannel] = pkt->ch_7_gate_range;
        valueChanged = YES;
    }
    if (dynExpThreshold[currentChannel] != pkt->ch_7_expand_threshold) {
        dynExpThreshold[currentChannel] = pkt->ch_7_expand_threshold;
        valueChanged = YES;
    }
    if (dynExpRatio[currentChannel] != [Global getRatioValue:pkt->ch_7_expand_ratio]) {
        dynExpRatio[currentChannel] = [Global getRatioValue:pkt->ch_7_expand_ratio];
        valueChanged = YES;
    }
    if (dynCompThreshold[currentChannel] != pkt->ch_7_compressor_threshold) {
        dynCompThreshold[currentChannel] = pkt->ch_7_compressor_threshold;
        valueChanged = YES;
    }
    if (dynCompRatio[currentChannel] != [Global getRatioCompValue:pkt->ch_7_compressor_ratio]) {
        dynCompRatio[currentChannel] = [Global getRatioCompValue:pkt->ch_7_compressor_ratio];
        valueChanged = YES;
    }
    if (dynLimitThreshold[currentChannel] != pkt->ch_7_limiter_threshold) {
        dynLimitThreshold[currentChannel] = pkt->ch_7_limiter_threshold;
        valueChanged = YES;
    }
    [self updateDynStatus:pkt->ch_7_ctrl];
    
    if (valueChanged) {
        [self calculateDyn];
        [self updatePreviewImageForChannel:currentChannel];
        valueChanged = NO;
    }
    
    // CHANNEL_CH_8
    currentChannel = CHANNEL_CH_8;
    if (dynGateThreshold[currentChannel] != pkt->ch_8_gate_threshold) {
        dynGateThreshold[currentChannel] = pkt->ch_8_gate_threshold;
        valueChanged = YES;
    }
    if (dynGateRange[currentChannel] != pkt->ch_8_gate_range) {
        dynGateRange[currentChannel] = pkt->ch_8_gate_range;
        valueChanged = YES;
    }
    if (dynExpThreshold[currentChannel] != pkt->ch_8_expand_threshold) {
        dynExpThreshold[currentChannel] = pkt->ch_8_expand_threshold;
        valueChanged = YES;
    }
    if (dynExpRatio[currentChannel] != [Global getRatioValue:pkt->ch_8_expand_ratio]) {
        dynExpRatio[currentChannel] = [Global getRatioValue:pkt->ch_8_expand_ratio];
        valueChanged = YES;
    }
    if (dynCompThreshold[currentChannel] != pkt->ch_8_compressor_threshold) {
        dynCompThreshold[currentChannel] = pkt->ch_8_compressor_threshold;
        valueChanged = YES;
    }
    if (dynCompRatio[currentChannel] != [Global getRatioCompValue:pkt->ch_8_compressor_ratio]) {
        dynCompRatio[currentChannel] = [Global getRatioCompValue:pkt->ch_8_compressor_ratio];
        valueChanged = YES;
    }
    if (dynLimitThreshold[currentChannel] != pkt->ch_8_limiter_threshold) {
        dynLimitThreshold[currentChannel] = pkt->ch_8_limiter_threshold;
        valueChanged = YES;
    }
    [self updateDynStatus:pkt->ch_8_ctrl];
    
    if (valueChanged) {
        [self calculateDyn];
        [self updatePreviewImageForChannel:currentChannel];
        valueChanged = NO;
    }
    
    // CHANNEL_MAIN
    currentChannel = CHANNEL_MAIN;
    if (dynGateThreshold[currentChannel] != pkt->main_l_r_gate_threshold) {
        dynGateThreshold[currentChannel] = pkt->main_l_r_gate_threshold;
        valueChanged = YES;
    }
    if (dynGateRange[currentChannel] != pkt->main_l_r_gate_range) {
        dynGateRange[currentChannel] = pkt->main_l_r_gate_range;
        valueChanged = YES;
    }
    if (dynExpThreshold[currentChannel] != pkt->main_l_r_expand_threshold) {
        dynExpThreshold[currentChannel] = pkt->main_l_r_expand_threshold;
        valueChanged = YES;
    }
    if (dynExpRatio[currentChannel] != [Global getRatioValue:pkt->main_l_r_expand_ratio]) {
        dynExpRatio[currentChannel] = [Global getRatioValue:pkt->main_l_r_expand_ratio];
        valueChanged = YES;
    }
    if (dynCompThreshold[currentChannel] != pkt->main_l_r_compressor_threshold) {
        dynCompThreshold[currentChannel] = pkt->main_l_r_compressor_threshold;
        valueChanged = YES;
    }
    if (dynCompRatio[currentChannel] != [Global getRatioCompValue:pkt->main_l_r_compressor_ratio]) {
        dynCompRatio[currentChannel] = [Global getRatioCompValue:pkt->main_l_r_compressor_ratio];
        valueChanged = YES;
    }
    if (dynLimitThreshold[currentChannel] != pkt->main_l_r_limiter_threshold) {
        dynLimitThreshold[currentChannel] = pkt->main_l_r_limiter_threshold;
        valueChanged = YES;
    }
    [self updateDynStatus:pkt->main_l_r_ctrl];
    
    if (valueChanged) {
        [self calculateDyn];
        [self updatePreviewImageForChannel:currentChannel];
    }
}

- (void)processChannel9_16PageReply:(ChannelCh9_16PagePacket *)pkt {
    
    BOOL valueChanged = NO;
    
    _ch9Ctrl = pkt->ch_9_ctrl;
    _ch10Ctrl = pkt->ch_10_ctrl;
    _ch11Ctrl = pkt->ch_11_ctrl;
    _ch12Ctrl = pkt->ch_12_ctrl;
    _ch13Ctrl = pkt->ch_13_ctrl;
    _ch14Ctrl = pkt->ch_14_ctrl;
    _ch15Ctrl = pkt->ch_15_ctrl;
    _ch16Ctrl = pkt->ch_16_ctrl;
    _mainCtrl = pkt->main_l_r_ctrl;
    
    // Only handle when DYN view is hidden
    if (!self.view.hidden) {
        NSLog(@"processChannel9_16PageReply: view is visible!");
        return;
    }
    
    // CHANNEL_CH_9
    currentChannel = CHANNEL_CH_9;
    if (dynGateThreshold[currentChannel] != pkt->ch_9_gate_threshold) {
        dynGateThreshold[currentChannel] = pkt->ch_9_gate_threshold;
        valueChanged = YES;
    }
    if (dynGateRange[currentChannel] != pkt->ch_9_gate_range) {
        dynGateRange[currentChannel] = pkt->ch_9_gate_range;
        valueChanged = YES;
    }
    if (dynExpThreshold[currentChannel] != pkt->ch_9_expand_threshold) {
        dynExpThreshold[currentChannel] = pkt->ch_9_expand_threshold;
        valueChanged = YES;
    }
    if (dynExpRatio[currentChannel] != [Global getRatioValue:pkt->ch_9_expand_ratio]) {
        dynExpRatio[currentChannel] = [Global getRatioValue:pkt->ch_9_expand_ratio];
        valueChanged = YES;
    }
    if (dynCompThreshold[currentChannel] != pkt->ch_9_compressor_threshold) {
        dynCompThreshold[currentChannel] = pkt->ch_9_compressor_threshold;
        valueChanged = YES;
    }
    if (dynCompRatio[currentChannel] != [Global getRatioCompValue:pkt->ch_9_compressor_ratio]) {
        dynCompRatio[currentChannel] = [Global getRatioCompValue:pkt->ch_9_compressor_ratio];
        valueChanged = YES;
    }
    if (dynLimitThreshold[currentChannel] != pkt->ch_9_limiter_threshold) {
        dynLimitThreshold[currentChannel] = pkt->ch_9_limiter_threshold;
        valueChanged = YES;
    }
    [self updateDynStatus:pkt->ch_9_ctrl];
    
    if (valueChanged) {
        [self calculateDyn];
        [self updatePreviewImageForChannel:currentChannel];
        valueChanged = NO;
    }
    
    // CHANNEL_CH_10
    currentChannel = CHANNEL_CH_10;
    if (dynGateThreshold[currentChannel] != pkt->ch_10_gate_threshold) {
        dynGateThreshold[currentChannel] = pkt->ch_10_gate_threshold;
        valueChanged = YES;
    }
    if (dynGateRange[currentChannel] != pkt->ch_10_gate_range) {
        dynGateRange[currentChannel] = pkt->ch_10_gate_range;
        valueChanged = YES;
    }
    if (dynExpThreshold[currentChannel] != pkt->ch_10_expand_threshold) {
        dynExpThreshold[currentChannel] = pkt->ch_10_expand_threshold;
        valueChanged = YES;
    }
    if (dynExpRatio[currentChannel] != [Global getRatioValue:pkt->ch_10_expand_ratio]) {
        dynExpRatio[currentChannel] = [Global getRatioValue:pkt->ch_10_expand_ratio];
        valueChanged = YES;
    }
    if (dynCompThreshold[currentChannel] != pkt->ch_10_compressor_threshold) {
        dynCompThreshold[currentChannel] = pkt->ch_10_compressor_threshold;
        valueChanged = YES;
    }
    if (dynCompRatio[currentChannel] != [Global getRatioCompValue:pkt->ch_10_compressor_ratio]) {
        dynCompRatio[currentChannel] = [Global getRatioCompValue:pkt->ch_10_compressor_ratio];
        valueChanged = YES;
    }
    if (dynLimitThreshold[currentChannel] != pkt->ch_10_limiter_threshold) {
        dynLimitThreshold[currentChannel] = pkt->ch_10_limiter_threshold;
        valueChanged = YES;
    }
    [self updateDynStatus:pkt->ch_10_ctrl];
    
    if (valueChanged) {
        [self calculateDyn];
        [self updatePreviewImageForChannel:currentChannel];
        valueChanged = NO;
    }
    
    // CHANNEL_CH_11
    currentChannel = CHANNEL_CH_11;
    if (dynGateThreshold[currentChannel] != pkt->ch_11_gate_threshold) {
        dynGateThreshold[currentChannel] = pkt->ch_11_gate_threshold;
        valueChanged = YES;
    }
    if (dynGateRange[currentChannel] != pkt->ch_11_gate_range) {
        dynGateRange[currentChannel] = pkt->ch_11_gate_range;
        valueChanged = YES;
    }
    if (dynExpThreshold[currentChannel] != pkt->ch_11_expand_threshold) {
        dynExpThreshold[currentChannel] = pkt->ch_11_expand_threshold;
        valueChanged = YES;
    }
    if (dynExpRatio[currentChannel] != [Global getRatioValue:pkt->ch_11_expand_ratio]) {
        dynExpRatio[currentChannel] = [Global getRatioValue:pkt->ch_11_expand_ratio];
        valueChanged = YES;
    }
    if (dynCompThreshold[currentChannel] != pkt->ch_11_compressor_threshold) {
        dynCompThreshold[currentChannel] = pkt->ch_11_compressor_threshold;
        valueChanged = YES;
    }
    if (dynCompRatio[currentChannel] != [Global getRatioCompValue:pkt->ch_11_compressor_ratio]) {
        dynCompRatio[currentChannel] = [Global getRatioCompValue:pkt->ch_11_compressor_ratio];
        valueChanged = YES;
    }
    if (dynLimitThreshold[currentChannel] != pkt->ch_11_limiter_threshold) {
        dynLimitThreshold[currentChannel] = pkt->ch_11_limiter_threshold;
        valueChanged = YES;
    }
    [self updateDynStatus:pkt->ch_11_ctrl];
    
    if (valueChanged) {
        [self calculateDyn];
        [self updatePreviewImageForChannel:currentChannel];
        valueChanged = NO;
    }
    
    // CHANNEL_CH_12
    currentChannel = CHANNEL_CH_12;
    if (dynGateThreshold[currentChannel] != pkt->ch_12_gate_threshold) {
        dynGateThreshold[currentChannel] = pkt->ch_12_gate_threshold;
        valueChanged = YES;
    }
    if (dynGateRange[currentChannel] != pkt->ch_12_gate_range) {
        dynGateRange[currentChannel] = pkt->ch_12_gate_range;
        valueChanged = YES;
    }
    if (dynExpThreshold[currentChannel] != pkt->ch_12_expand_threshold) {
        dynExpThreshold[currentChannel] = pkt->ch_12_expand_threshold;
        valueChanged = YES;
    }
    if (dynExpRatio[currentChannel] != [Global getRatioValue:pkt->ch_12_expand_ratio]) {
        dynExpRatio[currentChannel] = [Global getRatioValue:pkt->ch_12_expand_ratio];
        valueChanged = YES;
    }
    if (dynCompThreshold[currentChannel] != pkt->ch_12_compressor_threshold) {
        dynCompThreshold[currentChannel] = pkt->ch_12_compressor_threshold;
        valueChanged = YES;
    }
    if (dynCompRatio[currentChannel] != [Global getRatioCompValue:pkt->ch_12_compressor_ratio]) {
        dynCompRatio[currentChannel] = [Global getRatioCompValue:pkt->ch_12_compressor_ratio];
        valueChanged = YES;
    }
    if (dynLimitThreshold[currentChannel] != pkt->ch_12_limiter_threshold) {
        dynLimitThreshold[currentChannel] = pkt->ch_12_limiter_threshold;
        valueChanged = YES;
    }
    [self updateDynStatus:pkt->ch_12_ctrl];
    
    if (valueChanged) {
        [self calculateDyn];
        [self updatePreviewImageForChannel:currentChannel];
        valueChanged = NO;
    }
    
    // CHANNEL_CH_13
    currentChannel = CHANNEL_CH_13;
    if (dynGateThreshold[currentChannel] != pkt->ch_13_gate_threshold) {
        dynGateThreshold[currentChannel] = pkt->ch_13_gate_threshold;
        valueChanged = YES;
    }
    if (dynGateRange[currentChannel] != pkt->ch_13_gate_range) {
        dynGateRange[currentChannel] = pkt->ch_13_gate_range;
        valueChanged = YES;
    }
    if (dynExpThreshold[currentChannel] != pkt->ch_13_expand_threshold) {
        dynExpThreshold[currentChannel] = pkt->ch_13_expand_threshold;
        valueChanged = YES;
    }
    if (dynExpRatio[currentChannel] != [Global getRatioValue:pkt->ch_13_expand_ratio]) {
        dynExpRatio[currentChannel] = [Global getRatioValue:pkt->ch_13_expand_ratio];
        valueChanged = YES;
    }
    if (dynCompThreshold[currentChannel] != pkt->ch_13_compressor_threshold) {
        dynCompThreshold[currentChannel] = pkt->ch_13_compressor_threshold;
        valueChanged = YES;
    }
    if (dynCompRatio[currentChannel] != [Global getRatioCompValue:pkt->ch_13_compressor_ratio]) {
        dynCompRatio[currentChannel] = [Global getRatioCompValue:pkt->ch_13_compressor_ratio];
        valueChanged = YES;
    }
    if (dynLimitThreshold[currentChannel] != pkt->ch_13_limiter_threshold) {
        dynLimitThreshold[currentChannel] = pkt->ch_13_limiter_threshold;
        valueChanged = YES;
    }
    [self updateDynStatus:pkt->ch_13_ctrl];
    
    if (valueChanged) {
        [self calculateDyn];
        [self updatePreviewImageForChannel:currentChannel];
        valueChanged = NO;
    }
    
    // CHANNEL_CH_14
    currentChannel = CHANNEL_CH_14;
    if (dynGateThreshold[currentChannel] != pkt->ch_14_gate_threshold) {
        dynGateThreshold[currentChannel] = pkt->ch_14_gate_threshold;
        valueChanged = YES;
    }
    if (dynGateRange[currentChannel] != pkt->ch_14_gate_range) {
        dynGateRange[currentChannel] = pkt->ch_14_gate_range;
        valueChanged = YES;
    }
    if (dynExpThreshold[currentChannel] != pkt->ch_14_expand_threshold) {
        dynExpThreshold[currentChannel] = pkt->ch_14_expand_threshold;
        valueChanged = YES;
    }
    if (dynExpRatio[currentChannel] != [Global getRatioValue:pkt->ch_14_expand_ratio]) {
        dynExpRatio[currentChannel] = [Global getRatioValue:pkt->ch_14_expand_ratio];
        valueChanged = YES;
    }
    if (dynCompThreshold[currentChannel] != pkt->ch_14_compressor_threshold) {
        dynCompThreshold[currentChannel] = pkt->ch_14_compressor_threshold;
        valueChanged = YES;
    }
    if (dynCompRatio[currentChannel] != [Global getRatioCompValue:pkt->ch_14_compressor_ratio]) {
        dynCompRatio[currentChannel] = [Global getRatioCompValue:pkt->ch_14_compressor_ratio];
        valueChanged = YES;
    }
    if (dynLimitThreshold[currentChannel] != pkt->ch_14_limiter_threshold) {
        dynLimitThreshold[currentChannel] = pkt->ch_14_limiter_threshold;
        valueChanged = YES;
    }
    [self updateDynStatus:pkt->ch_14_ctrl];
    
    if (valueChanged) {
        [self calculateDyn];
        [self updatePreviewImageForChannel:currentChannel];
        valueChanged = NO;
    }
    
    // CHANNEL_CH_15
    currentChannel = CHANNEL_CH_15;
    if (dynGateThreshold[currentChannel] != pkt->ch_15_gate_threshold) {
        dynGateThreshold[currentChannel] = pkt->ch_15_gate_threshold;
        valueChanged = YES;
    }
    if (dynGateRange[currentChannel] != pkt->ch_15_gate_range) {
        dynGateRange[currentChannel] = pkt->ch_15_gate_range;
        valueChanged = YES;
    }
    if (dynExpThreshold[currentChannel] != pkt->ch_15_expand_threshold) {
        dynExpThreshold[currentChannel] = pkt->ch_15_expand_threshold;
        valueChanged = YES;
    }
    if (dynExpRatio[currentChannel] != [Global getRatioValue:pkt->ch_15_expand_ratio]) {
        dynExpRatio[currentChannel] = [Global getRatioValue:pkt->ch_15_expand_ratio];
        valueChanged = YES;
    }
    if (dynCompThreshold[currentChannel] != pkt->ch_15_compressor_threshold) {
        dynCompThreshold[currentChannel] = pkt->ch_15_compressor_threshold;
        valueChanged = YES;
    }
    if (dynCompRatio[currentChannel] != [Global getRatioCompValue:pkt->ch_15_compressor_ratio]) {
        dynCompRatio[currentChannel] = [Global getRatioCompValue:pkt->ch_15_compressor_ratio];
        valueChanged = YES;
    }
    if (dynLimitThreshold[currentChannel] != pkt->ch_15_limiter_threshold) {
        dynLimitThreshold[currentChannel] = pkt->ch_15_limiter_threshold;
        valueChanged = YES;
    }
    [self updateDynStatus:pkt->ch_15_ctrl];
    
    if (valueChanged) {
        [self calculateDyn];
        [self updatePreviewImageForChannel:currentChannel];
        valueChanged = NO;
    }
    
    // CHANNEL_CH_16
    currentChannel = CHANNEL_CH_16;
    if (dynGateThreshold[currentChannel] != pkt->ch_16_gate_threshold) {
        dynGateThreshold[currentChannel] = pkt->ch_16_gate_threshold;
        valueChanged = YES;
    }
    if (dynGateRange[currentChannel] != pkt->ch_16_gate_range) {
        dynGateRange[currentChannel] = pkt->ch_16_gate_range;
        valueChanged = YES;
    }
    if (dynExpThreshold[currentChannel] != pkt->ch_16_expand_threshold) {
        dynExpThreshold[currentChannel] = pkt->ch_16_expand_threshold;
        valueChanged = YES;
    }
    if (dynExpRatio[currentChannel] != [Global getRatioValue:pkt->ch_16_expand_ratio]) {
        dynExpRatio[currentChannel] = [Global getRatioValue:pkt->ch_16_expand_ratio];
        valueChanged = YES;
    }
    if (dynCompThreshold[currentChannel] != pkt->ch_16_compressor_threshold) {
        dynCompThreshold[currentChannel] = pkt->ch_16_compressor_threshold;
        valueChanged = YES;
    }
    if (dynCompRatio[currentChannel] != [Global getRatioCompValue:pkt->ch_16_compressor_ratio]) {
        dynCompRatio[currentChannel] = [Global getRatioCompValue:pkt->ch_16_compressor_ratio];
        valueChanged = YES;
    }
    if (dynLimitThreshold[currentChannel] != pkt->ch_16_limiter_threshold) {
        dynLimitThreshold[currentChannel] = pkt->ch_16_limiter_threshold;
        valueChanged = YES;
    }
    [self updateDynStatus:pkt->ch_16_ctrl];
    
    if (valueChanged) {
        [self calculateDyn];
        [self updatePreviewImageForChannel:currentChannel];
        valueChanged = NO;
    }
    
    // CHANNEL_MAIN
    currentChannel = CHANNEL_MAIN;
    if (dynGateThreshold[currentChannel] != pkt->main_l_r_gate_threshold) {
        dynGateThreshold[currentChannel] = pkt->main_l_r_gate_threshold;
        valueChanged = YES;
    }
    if (dynGateRange[currentChannel] != pkt->main_l_r_gate_range) {
        dynGateRange[currentChannel] = pkt->main_l_r_gate_range;
        valueChanged = YES;
    }
    if (dynExpThreshold[currentChannel] != pkt->main_l_r_expand_threshold) {
        dynExpThreshold[currentChannel] = pkt->main_l_r_expand_threshold;
        valueChanged = YES;
    }
    if (dynExpRatio[currentChannel] != [Global getRatioValue:pkt->main_l_r_expand_ratio]) {
        dynExpRatio[currentChannel] = [Global getRatioValue:pkt->main_l_r_expand_ratio];
        valueChanged = YES;
    }
    if (dynCompThreshold[currentChannel] != pkt->main_l_r_compressor_threshold) {
        dynCompThreshold[currentChannel] = pkt->main_l_r_compressor_threshold;
        valueChanged = YES;
    }
    if (dynCompRatio[currentChannel] != [Global getRatioCompValue:pkt->main_l_r_compressor_ratio]) {
        dynCompRatio[currentChannel] = [Global getRatioCompValue:pkt->main_l_r_compressor_ratio];
        valueChanged = YES;
    }
    if (dynLimitThreshold[currentChannel] != pkt->main_l_r_limiter_threshold) {
        dynLimitThreshold[currentChannel] = pkt->main_l_r_limiter_threshold;
        valueChanged = YES;
    }
    [self updateDynStatus:pkt->main_l_r_ctrl];
    
    if (valueChanged) {
        [self calculateDyn];
        [self updatePreviewImageForChannel:currentChannel];
    }
}

- (void)processChannel17_18PageReply:(ChannelCh17_18PagePacket *)pkt {
    
    BOOL valueChanged = NO;
    
    _ch17Ctrl = pkt->ch_17_ctrl;
    _ch18Ctrl = pkt->ch_18_ctrl;
    _mainCtrl = pkt->main_l_r_ctrl;
    
    // Only handle when DYN view is hidden
    if (!self.view.hidden) {
        NSLog(@"processChannel17_18PageReply: view is visible!");
        return;
    }
    
    // CHANNEL_CH_17
    currentChannel = CHANNEL_CH_17;
    if (dynGateThreshold[currentChannel] != pkt->ch_17_gate_threshold) {
        dynGateThreshold[currentChannel] = pkt->ch_17_gate_threshold;
        valueChanged = YES;
    }
    if (dynGateRange[currentChannel] != pkt->ch_17_gate_range) {
        dynGateRange[currentChannel] = pkt->ch_17_gate_range;
        valueChanged = YES;
    }
    if (dynExpThreshold[currentChannel] != pkt->ch_17_expand_threshold) {
        dynExpThreshold[currentChannel] = pkt->ch_17_expand_threshold;
        valueChanged = YES;
    }
    if (dynExpRatio[currentChannel] != [Global getRatioValue:pkt->ch_17_expand_ratio]) {
        dynExpRatio[currentChannel] = [Global getRatioValue:pkt->ch_17_expand_ratio];
        valueChanged = YES;
    }
    if (dynCompThreshold[currentChannel] != pkt->ch_17_compressor_threshold) {
        dynCompThreshold[currentChannel] = pkt->ch_17_compressor_threshold;
        valueChanged = YES;
    }
    if (dynCompRatio[currentChannel] != [Global getRatioCompValue:pkt->ch_17_compressor_ratio]) {
        dynCompRatio[currentChannel] = [Global getRatioCompValue:pkt->ch_17_compressor_ratio];
        valueChanged = YES;
    }
    if (dynLimitThreshold[currentChannel] != pkt->ch_17_limiter_threshold) {
        dynLimitThreshold[currentChannel] = pkt->ch_17_limiter_threshold;
        valueChanged = YES;
    }
    [self updateDynStatus:pkt->ch_17_ctrl];
    
    if (valueChanged) {
        [self calculateDyn];
        [self updatePreviewImageForChannel:currentChannel];
        valueChanged = NO;
    }
    
    // CHANNEL_CH_18
    currentChannel = CHANNEL_CH_18;
    if (dynGateThreshold[currentChannel] != pkt->ch_18_gate_threshold) {
        dynGateThreshold[currentChannel] = pkt->ch_18_gate_threshold;
        valueChanged = YES;
    }
    if (dynGateRange[currentChannel] != pkt->ch_18_gate_range) {
        dynGateRange[currentChannel] = pkt->ch_18_gate_range;
        valueChanged = YES;
    }
    if (dynExpThreshold[currentChannel] != pkt->ch_18_expand_threshold) {
        dynExpThreshold[currentChannel] = pkt->ch_18_expand_threshold;
        valueChanged = YES;
    }
    if (dynExpRatio[currentChannel] != [Global getRatioValue:pkt->ch_18_expand_ratio]) {
        dynExpRatio[currentChannel] = [Global getRatioValue:pkt->ch_18_expand_ratio];
        valueChanged = YES;
    }
    if (dynCompThreshold[currentChannel] != pkt->ch_18_compressor_threshold) {
        dynCompThreshold[currentChannel] = pkt->ch_18_compressor_threshold;
        valueChanged = YES;
    }
    if (dynCompRatio[currentChannel] != [Global getRatioCompValue:pkt->ch_18_compressor_ratio]) {
        dynCompRatio[currentChannel] = [Global getRatioCompValue:pkt->ch_18_compressor_ratio];
        valueChanged = YES;
    }
    if (dynLimitThreshold[currentChannel] != pkt->ch_18_limiter_threshold) {
        dynLimitThreshold[currentChannel] = pkt->ch_18_limiter_threshold;
        valueChanged = YES;
    }
    [self updateDynStatus:pkt->ch_18_ctrl];
    
    if (valueChanged) {
        [self calculateDyn];
        [self updatePreviewImageForChannel:currentChannel];
        valueChanged = NO;
    }
    
    // CHANNEL_MAIN
    currentChannel = CHANNEL_MAIN;
    if (dynGateThreshold[currentChannel] != pkt->main_l_r_gate_threshold) {
        dynGateThreshold[currentChannel] = pkt->main_l_r_gate_threshold;
        valueChanged = YES;
    }
    if (dynGateRange[currentChannel] != pkt->main_l_r_gate_range) {
        dynGateRange[currentChannel] = pkt->main_l_r_gate_range;
        valueChanged = YES;
    }
    if (dynExpThreshold[currentChannel] != pkt->main_l_r_expand_threshold) {
        dynExpThreshold[currentChannel] = pkt->main_l_r_expand_threshold;
        valueChanged = YES;
    }
    if (dynExpRatio[currentChannel] != [Global getRatioValue:pkt->main_l_r_expand_ratio]) {
        dynExpRatio[currentChannel] = [Global getRatioValue:pkt->main_l_r_expand_ratio];
        valueChanged = YES;
    }
    if (dynCompThreshold[currentChannel] != pkt->main_l_r_compressor_threshold) {
        dynCompThreshold[currentChannel] = pkt->main_l_r_compressor_threshold;
        valueChanged = YES;
    }
    if (dynCompRatio[currentChannel] != [Global getRatioCompValue:pkt->main_l_r_compressor_ratio]) {
        dynCompRatio[currentChannel] = [Global getRatioCompValue:pkt->main_l_r_compressor_ratio];
        valueChanged = YES;
    }
    if (dynLimitThreshold[currentChannel] != pkt->main_l_r_limiter_threshold) {
        dynLimitThreshold[currentChannel] = pkt->main_l_r_limiter_threshold;
        valueChanged = YES;
    }
    [self updateDynStatus:pkt->main_l_r_ctrl];
    
    if (valueChanged) {
        [self calculateDyn];
        [self updatePreviewImageForChannel:currentChannel];
    }
}

- (void)processMulti1_4PageReply:(ChannelMulti1_4PagePacket *)pkt {
    
    BOOL valueChanged = NO;
    
    _multi1Ctrl = pkt->multi_1_ctrl;
    _multi2Ctrl = pkt->multi_2_ctrl;
    _multi3Ctrl = pkt->multi_3_ctrl;
    _multi4Ctrl = pkt->multi_4_ctrl;
    _mainCtrl = pkt->main_l_r_ctrl;
    
    // Only handle when DYN view is hidden
    if (!self.view.hidden) {
        NSLog(@"processMulti1_4PageReply: view is visible!");
        return;
    }
    
    // CHANNEL_MULTI_1
    currentChannel = CHANNEL_MULTI_1;
    if (dynGateThreshold[currentChannel] != pkt->multi_1_gate_threshold) {
        dynGateThreshold[currentChannel] = pkt->multi_1_gate_threshold;
        valueChanged = YES;
    }
    if (dynGateRange[currentChannel] != pkt->multi_1_gate_range) {
        dynGateRange[currentChannel] = pkt->multi_1_gate_range;
        valueChanged = YES;
    }
    if (dynExpThreshold[currentChannel] != pkt->multi_1_expand_threshold) {
        dynExpThreshold[currentChannel] = pkt->multi_1_expand_threshold;
        valueChanged = YES;
    }
    if (dynExpRatio[currentChannel] != [Global getRatioValue:pkt->multi_1_expand_ratio]) {
        dynExpRatio[currentChannel] = [Global getRatioValue:pkt->multi_1_expand_ratio];
        valueChanged = YES;
    }
    if (dynCompThreshold[currentChannel] != pkt->multi_1_compressor_threshold) {
        dynCompThreshold[currentChannel] = pkt->multi_1_compressor_threshold;
        valueChanged = YES;
    }
    if (dynCompRatio[currentChannel] != [Global getRatioCompValue:pkt->multi_1_compressor_ratio]) {
        dynCompRatio[currentChannel] = [Global getRatioCompValue:pkt->multi_1_compressor_ratio];
        valueChanged = YES;
    }
    if (dynLimitThreshold[currentChannel] != pkt->multi_1_limiter_threshold) {
        dynLimitThreshold[currentChannel] = pkt->multi_1_limiter_threshold;
        valueChanged = YES;
    }
    [self updateDynStatus:pkt->multi_1_ctrl];
    
    if (valueChanged) {
        [self calculateDyn];
        [self updatePreviewImageForChannel:currentChannel];
        valueChanged = NO;
    }
    
    // CHANNEL_MULTI_2
    currentChannel = CHANNEL_MULTI_2;
    if (dynGateThreshold[currentChannel] != pkt->multi_2_gate_threshold) {
        dynGateThreshold[currentChannel] = pkt->multi_2_gate_threshold;
        valueChanged = YES;
    }
    if (dynGateRange[currentChannel] != pkt->multi_2_gate_range) {
        dynGateRange[currentChannel] = pkt->multi_2_gate_range;
        valueChanged = YES;
    }
    if (dynExpThreshold[currentChannel] != pkt->multi_2_expand_threshold) {
        dynExpThreshold[currentChannel] = pkt->multi_2_expand_threshold;
        valueChanged = YES;
    }
    if (dynExpRatio[currentChannel] != [Global getRatioValue:pkt->multi_2_expand_ratio]) {
        dynExpRatio[currentChannel] = [Global getRatioValue:pkt->multi_2_expand_ratio];
        valueChanged = YES;
    }
    if (dynCompThreshold[currentChannel] != pkt->multi_2_compressor_threshold) {
        dynCompThreshold[currentChannel] = pkt->multi_2_compressor_threshold;
        valueChanged = YES;
    }
    if (dynCompRatio[currentChannel] != [Global getRatioCompValue:pkt->multi_2_compressor_ratio]) {
        dynCompRatio[currentChannel] = [Global getRatioCompValue:pkt->multi_2_compressor_ratio];
        valueChanged = YES;
    }
    if (dynLimitThreshold[currentChannel] != pkt->multi_2_limiter_threshold) {
        dynLimitThreshold[currentChannel] = pkt->multi_2_limiter_threshold;
        valueChanged = YES;
    }
    [self updateDynStatus:pkt->multi_2_ctrl];
    
    if (valueChanged) {
        [self calculateDyn];
        [self updatePreviewImageForChannel:currentChannel];
        valueChanged = NO;
    }
    
    // CHANNEL_MULTI_3
    currentChannel = CHANNEL_MULTI_3;
    if (dynGateThreshold[currentChannel] != pkt->multi_3_gate_threshold) {
        dynGateThreshold[currentChannel] = pkt->multi_3_gate_threshold;
        valueChanged = YES;
    }
    if (dynGateRange[currentChannel] != pkt->multi_3_gate_range) {
        dynGateRange[currentChannel] = pkt->multi_3_gate_range;
        valueChanged = YES;
    }
    if (dynExpThreshold[currentChannel] != pkt->multi_3_expand_threshold) {
        dynExpThreshold[currentChannel] = pkt->multi_3_expand_threshold;
        valueChanged = YES;
    }
    if (dynExpRatio[currentChannel] != [Global getRatioValue:pkt->multi_3_expand_ratio]) {
        dynExpRatio[currentChannel] = [Global getRatioValue:pkt->multi_3_expand_ratio];
        valueChanged = YES;
    }
    if (dynCompThreshold[currentChannel] != pkt->multi_3_compressor_threshold) {
        dynCompThreshold[currentChannel] = pkt->multi_3_compressor_threshold;
        valueChanged = YES;
    }
    if (dynCompRatio[currentChannel] != [Global getRatioCompValue:pkt->multi_3_compressor_ratio]) {
        dynCompRatio[currentChannel] = [Global getRatioCompValue:pkt->multi_3_compressor_ratio];
        valueChanged = YES;
    }
    if (dynLimitThreshold[currentChannel] != pkt->multi_3_limiter_threshold) {
        dynLimitThreshold[currentChannel] = pkt->multi_3_limiter_threshold;
        valueChanged = YES;
    }
    [self updateDynStatus:pkt->multi_3_ctrl];
    
    if (valueChanged) {
        [self calculateDyn];
        [self updatePreviewImageForChannel:currentChannel];
        valueChanged = NO;
    }
    
    // CHANNEL_MULTI_4
    currentChannel = CHANNEL_MULTI_4;
    if (dynGateThreshold[currentChannel] != pkt->multi_4_gate_threshold) {
        dynGateThreshold[currentChannel] = pkt->multi_4_gate_threshold;
        valueChanged = YES;
    }
    if (dynGateRange[currentChannel] != pkt->multi_4_gate_range) {
        dynGateRange[currentChannel] = pkt->multi_4_gate_range;
        valueChanged = YES;
    }
    if (dynExpThreshold[currentChannel] != pkt->multi_4_expand_threshold) {
        dynExpThreshold[currentChannel] = pkt->multi_4_expand_threshold;
        valueChanged = YES;
    }
    if (dynExpRatio[currentChannel] != [Global getRatioValue:pkt->multi_4_expand_ratio]) {
        dynExpRatio[currentChannel] = [Global getRatioValue:pkt->multi_4_expand_ratio];
        valueChanged = YES;
    }
    if (dynCompThreshold[currentChannel] != pkt->multi_4_compressor_threshold) {
        dynCompThreshold[currentChannel] = pkt->multi_4_compressor_threshold;
        valueChanged = YES;
    }
    if (dynCompRatio[currentChannel] != [Global getRatioCompValue:pkt->multi_4_compressor_ratio]) {
        dynCompRatio[currentChannel] = [Global getRatioCompValue:pkt->multi_4_compressor_ratio];
        valueChanged = YES;
    }
    if (dynLimitThreshold[currentChannel] != pkt->multi_4_limiter_threshold) {
        dynLimitThreshold[currentChannel] = pkt->multi_4_limiter_threshold;
        valueChanged = YES;
    }
    [self updateDynStatus:pkt->multi_4_ctrl];
    
    if (valueChanged) {
        [self calculateDyn];
        [self updatePreviewImageForChannel:currentChannel];
        valueChanged = NO;
    }
    
    // CHANNEL_MAIN
    currentChannel = CHANNEL_MAIN;
    if (dynGateThreshold[currentChannel] != pkt->main_l_r_gate_threshold) {
        dynGateThreshold[currentChannel] = pkt->main_l_r_gate_threshold;
        valueChanged = YES;
    }
    if (dynGateRange[currentChannel] != pkt->main_l_r_gate_range) {
        dynGateRange[currentChannel] = pkt->main_l_r_gate_range;
        valueChanged = YES;
    }
    if (dynExpThreshold[currentChannel] != pkt->main_l_r_expand_threshold) {
        dynExpThreshold[currentChannel] = pkt->main_l_r_expand_threshold;
        valueChanged = YES;
    }
    if (dynExpRatio[currentChannel] != [Global getRatioValue:pkt->main_l_r_expand_ratio]) {
        dynExpRatio[currentChannel] = [Global getRatioValue:pkt->main_l_r_expand_ratio];
        valueChanged = YES;
    }
    if (dynCompThreshold[currentChannel] != pkt->main_l_r_compressor_threshold) {
        dynCompThreshold[currentChannel] = pkt->main_l_r_compressor_threshold;
        valueChanged = YES;
    }
    if (dynCompRatio[currentChannel] != [Global getRatioCompValue:pkt->main_l_r_compressor_ratio]) {
        dynCompRatio[currentChannel] = [Global getRatioCompValue:pkt->main_l_r_compressor_ratio];
        valueChanged = YES;
    }
    if (dynLimitThreshold[currentChannel] != pkt->main_l_r_limiter_threshold) {
        dynLimitThreshold[currentChannel] = pkt->main_l_r_limiter_threshold;
        valueChanged = YES;
    }
    [self updateDynStatus:pkt->main_l_r_ctrl];
    
    if (valueChanged) {
        [self calculateDyn];
        [self updatePreviewImageForChannel:currentChannel];
    }
}

- (void)processChannelViewPage:(ViewPageChannelPacket *)pkt {

    BOOL valueChanged = NO;
     
    _ch1Ctrl = pkt->channel_1_ctrl;
    _ch2Ctrl = pkt->channel_2_ctrl;
    _ch3Ctrl = pkt->channel_3_ctrl;
    _ch4Ctrl = pkt->channel_4_ctrl;
    _ch5Ctrl = pkt->channel_5_ctrl;
    _ch6Ctrl = pkt->channel_6_ctrl;
    _ch7Ctrl = pkt->channel_7_ctrl;
    _ch8Ctrl = pkt->channel_8_ctrl;
    _ch9Ctrl = pkt->channel_9_ctrl;
    _ch10Ctrl = pkt->channel_10_ctrl;
    _ch11Ctrl = pkt->channel_11_ctrl;
    _ch12Ctrl = pkt->channel_12_ctrl;
    _ch13Ctrl = pkt->channel_13_ctrl;
    _ch14Ctrl = pkt->channel_14_ctrl;
    _ch15Ctrl = pkt->channel_15_ctrl;
    _ch16Ctrl = pkt->channel_16_ctrl;
    
    // Only handle when DYN view is hidden
    if (!self.view.hidden) {
        NSLog(@"processChannelViewPage: view is visible!");
        return;
    }
     
    // CHANNEL_CH_1
    currentChannel = CHANNEL_CH_1;
    if (dynGateThreshold[currentChannel] != pkt->ch_1_gate_threshold) {
        dynGateThreshold[currentChannel] = pkt->ch_1_gate_threshold;
        valueChanged = YES;
    }
    if (dynGateRange[currentChannel] != pkt->ch_1_gate_range) {
        dynGateRange[currentChannel] = pkt->ch_1_gate_range;
        valueChanged = YES;
    }
    if (dynExpThreshold[currentChannel] != pkt->ch_1_expand_threshold) {
        dynExpThreshold[currentChannel] = pkt->ch_1_expand_threshold;
        valueChanged = YES;
    }
    if (dynExpRatio[currentChannel] != [Global getRatioValue:pkt->ch_1_expand_ratio]) {
        dynExpRatio[currentChannel] = [Global getRatioValue:pkt->ch_1_expand_ratio];
        valueChanged = YES;
    }
    if (dynCompThreshold[currentChannel] != pkt->ch_1_compressor_threshold) {
        dynCompThreshold[currentChannel] = pkt->ch_1_compressor_threshold;
        valueChanged = YES;
    }
    if (dynCompRatio[currentChannel] != [Global getRatioCompValue:pkt->ch_1_compressor_ratio]) {
        dynCompRatio[currentChannel] = [Global getRatioCompValue:pkt->ch_1_compressor_ratio];
        valueChanged = YES;
    }
    if (dynLimitThreshold[currentChannel] != pkt->ch_1_limiter_threshold) {
        dynLimitThreshold[currentChannel] = pkt->ch_1_limiter_threshold;
        valueChanged = YES;
    }
    if ([self updateDynStatus:pkt->channel_1_ctrl]) {
        valueChanged = YES;
    }

    if (valueChanged) {
        [self calculateDyn];
        [self updatePreviewImageForChannel:currentChannel];
        valueChanged = NO;
    }

    // CHANNEL_CH_2
    currentChannel = CHANNEL_CH_2;
    if (dynGateThreshold[currentChannel] != pkt->ch_2_gate_threshold) {
        dynGateThreshold[currentChannel] = pkt->ch_2_gate_threshold;
        valueChanged = YES;
    }
    if (dynGateRange[currentChannel] != pkt->ch_2_gate_range) {
        dynGateRange[currentChannel] = pkt->ch_2_gate_range;
        valueChanged = YES;
    }
    if (dynExpThreshold[currentChannel] != pkt->ch_2_expand_threshold) {
        dynExpThreshold[currentChannel] = pkt->ch_2_expand_threshold;
        valueChanged = YES;
    }
    if (dynExpRatio[currentChannel] != [Global getRatioValue:pkt->ch_2_expand_ratio]) {
        dynExpRatio[currentChannel] = [Global getRatioValue:pkt->ch_2_expand_ratio];
        valueChanged = YES;
    }
    if (dynCompThreshold[currentChannel] != pkt->ch_2_compressor_threshold) {
        dynCompThreshold[currentChannel] = pkt->ch_2_compressor_threshold;
        valueChanged = YES;
    }
    if (dynCompRatio[currentChannel] != [Global getRatioCompValue:pkt->ch_2_compressor_ratio]) {
        dynCompRatio[currentChannel] = [Global getRatioCompValue:pkt->ch_2_compressor_ratio];
        valueChanged = YES;
    }
    if (dynLimitThreshold[currentChannel] != pkt->ch_2_limiter_threshold) {
        dynLimitThreshold[currentChannel] = pkt->ch_2_limiter_threshold;
        valueChanged = YES;
    }
    if ([self updateDynStatus:pkt->channel_2_ctrl]) {
        valueChanged = YES;
    }

    if (valueChanged) {
        [self calculateDyn];
        [self updatePreviewImageForChannel:currentChannel];
        valueChanged = NO;
    }
    
    // CHANNEL_CH_3
    currentChannel = CHANNEL_CH_3;
    if (dynGateThreshold[currentChannel] != pkt->ch_3_gate_threshold) {
        dynGateThreshold[currentChannel] = pkt->ch_3_gate_threshold;
        valueChanged = YES;
    }
    if (dynGateRange[currentChannel] != pkt->ch_3_gate_range) {
        dynGateRange[currentChannel] = pkt->ch_3_gate_range;
        valueChanged = YES;
    }
    if (dynExpThreshold[currentChannel] != pkt->ch_3_expand_threshold) {
        dynExpThreshold[currentChannel] = pkt->ch_3_expand_threshold;
        valueChanged = YES;
    }
    if (dynExpRatio[currentChannel] != [Global getRatioValue:pkt->ch_3_expand_ratio]) {
        dynExpRatio[currentChannel] = [Global getRatioValue:pkt->ch_3_expand_ratio];
        valueChanged = YES;
    }
    if (dynCompThreshold[currentChannel] != pkt->ch_3_compressor_threshold) {
        dynCompThreshold[currentChannel] = pkt->ch_3_compressor_threshold;
        valueChanged = YES;
    }
    if (dynCompRatio[currentChannel] != [Global getRatioCompValue:pkt->ch_3_compressor_ratio]) {
        dynCompRatio[currentChannel] = [Global getRatioCompValue:pkt->ch_3_compressor_ratio];
        valueChanged = YES;
    }
    if (dynLimitThreshold[currentChannel] != pkt->ch_3_limiter_threshold) {
        dynLimitThreshold[currentChannel] = pkt->ch_3_limiter_threshold;
        valueChanged = YES;
    }
    if ([self updateDynStatus:pkt->channel_3_ctrl]) {
        valueChanged = YES;
    }

    if (valueChanged) {
        [self calculateDyn];
        [self updatePreviewImageForChannel:currentChannel];
        valueChanged = NO;
    }

    // CHANNEL_CH_4
    currentChannel = CHANNEL_CH_4;
    if (dynGateThreshold[currentChannel] != pkt->ch_4_gate_threshold) {
        dynGateThreshold[currentChannel] = pkt->ch_4_gate_threshold;
        valueChanged = YES;
    }
    if (dynGateRange[currentChannel] != pkt->ch_4_gate_range) {
        dynGateRange[currentChannel] = pkt->ch_4_gate_range;
        valueChanged = YES;
    }
    if (dynExpThreshold[currentChannel] != pkt->ch_4_expand_threshold) {
        dynExpThreshold[currentChannel] = pkt->ch_4_expand_threshold;
        valueChanged = YES;
    }
    if (dynExpRatio[currentChannel] != [Global getRatioValue:pkt->ch_4_expand_ratio]) {
        dynExpRatio[currentChannel] = [Global getRatioValue:pkt->ch_4_expand_ratio];
        valueChanged = YES;
    }
    if (dynCompThreshold[currentChannel] != pkt->ch_4_compressor_threshold) {
        dynCompThreshold[currentChannel] = pkt->ch_4_compressor_threshold;
        valueChanged = YES;
    }
    if (dynCompRatio[currentChannel] != [Global getRatioCompValue:pkt->ch_4_compressor_ratio]) {
        dynCompRatio[currentChannel] = [Global getRatioCompValue:pkt->ch_4_compressor_ratio];
        valueChanged = YES;
    }
    if (dynLimitThreshold[currentChannel] != pkt->ch_4_limiter_threshold) {
        dynLimitThreshold[currentChannel] = pkt->ch_4_limiter_threshold;
        valueChanged = YES;
    }
    if ([self updateDynStatus:pkt->channel_4_ctrl]) {
        valueChanged = YES;
    }

    if (valueChanged) {
        [self calculateDyn];
        [self updatePreviewImageForChannel:currentChannel];
        valueChanged = NO;
    }

    // CHANNEL_CH_5
    currentChannel = CHANNEL_CH_5;
    if (dynGateThreshold[currentChannel] != pkt->ch_5_gate_threshold) {
        dynGateThreshold[currentChannel] = pkt->ch_5_gate_threshold;
        valueChanged = YES;
    }
    if (dynGateRange[currentChannel] != pkt->ch_5_gate_range) {
        dynGateRange[currentChannel] = pkt->ch_5_gate_range;
        valueChanged = YES;
    }
    if (dynExpThreshold[currentChannel] != pkt->ch_5_expand_threshold) {
        dynExpThreshold[currentChannel] = pkt->ch_5_expand_threshold;
        valueChanged = YES;
    }
    if (dynExpRatio[currentChannel] != [Global getRatioValue:pkt->ch_5_expand_ratio]) {
        dynExpRatio[currentChannel] = [Global getRatioValue:pkt->ch_5_expand_ratio];
        valueChanged = YES;
    }
    if (dynCompThreshold[currentChannel] != pkt->ch_5_compressor_threshold) {
        dynCompThreshold[currentChannel] = pkt->ch_5_compressor_threshold;
        valueChanged = YES;
    }
    if (dynCompRatio[currentChannel] != [Global getRatioCompValue:pkt->ch_5_compressor_ratio]) {
        dynCompRatio[currentChannel] = [Global getRatioCompValue:pkt->ch_5_compressor_ratio];
        valueChanged = YES;
    }
    if (dynLimitThreshold[currentChannel] != pkt->ch_5_limiter_threshold) {
        dynLimitThreshold[currentChannel] = pkt->ch_5_limiter_threshold;
        valueChanged = YES;
    }
    if ([self updateDynStatus:pkt->channel_5_ctrl]) {
        valueChanged = YES;
    }

    if (valueChanged) {
        [self calculateDyn];
        [self updatePreviewImageForChannel:currentChannel];
        valueChanged = NO;
    }

    // CHANNEL_CH_6
    currentChannel = CHANNEL_CH_6;
    if (dynGateThreshold[currentChannel] != pkt->ch_6_gate_threshold) {
        dynGateThreshold[currentChannel] = pkt->ch_6_gate_threshold;
        valueChanged = YES;
    }
    if (dynGateRange[currentChannel] != pkt->ch_6_gate_range) {
        dynGateRange[currentChannel] = pkt->ch_6_gate_range;
        valueChanged = YES;
    }
    if (dynExpThreshold[currentChannel] != pkt->ch_6_expand_threshold) {
        dynExpThreshold[currentChannel] = pkt->ch_6_expand_threshold;
        valueChanged = YES;
    }
    if (dynExpRatio[currentChannel] != [Global getRatioValue:pkt->ch_6_expand_ratio]) {
        dynExpRatio[currentChannel] = [Global getRatioValue:pkt->ch_6_expand_ratio];
        valueChanged = YES;
    }
    if (dynCompThreshold[currentChannel] != pkt->ch_6_compressor_threshold) {
        dynCompThreshold[currentChannel] = pkt->ch_6_compressor_threshold;
        valueChanged = YES;
    }
    if (dynCompRatio[currentChannel] != [Global getRatioCompValue:pkt->ch_6_compressor_ratio]) {
        dynCompRatio[currentChannel] = [Global getRatioCompValue:pkt->ch_6_compressor_ratio];
        valueChanged = YES;
    }
    if (dynLimitThreshold[currentChannel] != pkt->ch_6_limiter_threshold) {
        dynLimitThreshold[currentChannel] = pkt->ch_6_limiter_threshold;
        valueChanged = YES;
    }
    if ([self updateDynStatus:pkt->channel_6_ctrl]) {
        valueChanged = YES;
    }

    if (valueChanged) {
        [self calculateDyn];
        [self updatePreviewImageForChannel:currentChannel];
        valueChanged = NO;
    }

    // CHANNEL_CH_7
    currentChannel = CHANNEL_CH_7;
    if (dynGateThreshold[currentChannel] != pkt->ch_7_gate_threshold) {
        dynGateThreshold[currentChannel] = pkt->ch_7_gate_threshold;
        valueChanged = YES;
    }
    if (dynGateRange[currentChannel] != pkt->ch_7_gate_range) {
        dynGateRange[currentChannel] = pkt->ch_7_gate_range;
        valueChanged = YES;
    }
    if (dynExpThreshold[currentChannel] != pkt->ch_7_expand_threshold) {
        dynExpThreshold[currentChannel] = pkt->ch_7_expand_threshold;
        valueChanged = YES;
    }
    if (dynExpRatio[currentChannel] != [Global getRatioValue:pkt->ch_7_expand_ratio]) {
        dynExpRatio[currentChannel] = [Global getRatioValue:pkt->ch_7_expand_ratio];
        valueChanged = YES;
    }
    if (dynCompThreshold[currentChannel] != pkt->ch_7_compressor_threshold) {
        dynCompThreshold[currentChannel] = pkt->ch_7_compressor_threshold;
        valueChanged = YES;
    }
    if (dynCompRatio[currentChannel] != [Global getRatioCompValue:pkt->ch_7_compressor_ratio]) {
        dynCompRatio[currentChannel] = [Global getRatioCompValue:pkt->ch_7_compressor_ratio];
        valueChanged = YES;
    }
    if (dynLimitThreshold[currentChannel] != pkt->ch_7_limiter_threshold) {
        dynLimitThreshold[currentChannel] = pkt->ch_7_limiter_threshold;
        valueChanged = YES;
    }
    if ([self updateDynStatus:pkt->channel_7_ctrl]) {
        valueChanged = YES;
    }

    if (valueChanged) {
        [self calculateDyn];
        [self updatePreviewImageForChannel:currentChannel];
        valueChanged = NO;
    }

    // CHANNEL_CH_8
    currentChannel = CHANNEL_CH_8;
    if (dynGateThreshold[currentChannel] != pkt->ch_8_gate_threshold) {
        dynGateThreshold[currentChannel] = pkt->ch_8_gate_threshold;
        valueChanged = YES;
    }
    if (dynGateRange[currentChannel] != pkt->ch_8_gate_range) {
        dynGateRange[currentChannel] = pkt->ch_8_gate_range;
        valueChanged = YES;
    }
    if (dynExpThreshold[currentChannel] != pkt->ch_8_expand_threshold) {
        dynExpThreshold[currentChannel] = pkt->ch_8_expand_threshold;
        valueChanged = YES;
    }
    if (dynExpRatio[currentChannel] != [Global getRatioValue:pkt->ch_8_expand_ratio]) {
        dynExpRatio[currentChannel] = [Global getRatioValue:pkt->ch_8_expand_ratio];
        valueChanged = YES;
    }
    if (dynCompThreshold[currentChannel] != pkt->ch_8_compressor_threshold) {
        dynCompThreshold[currentChannel] = pkt->ch_8_compressor_threshold;
        valueChanged = YES;
    }
    if (dynCompRatio[currentChannel] != [Global getRatioCompValue:pkt->ch_8_compressor_ratio]) {
        dynCompRatio[currentChannel] = [Global getRatioCompValue:pkt->ch_8_compressor_ratio];
        valueChanged = YES;
    }
    if (dynLimitThreshold[currentChannel] != pkt->ch_8_limiter_threshold) {
        dynLimitThreshold[currentChannel] = pkt->ch_8_limiter_threshold;
        valueChanged = YES;
    }
    if ([self updateDynStatus:pkt->channel_8_ctrl]) {
        valueChanged = YES;
    }

    if (valueChanged) {
        [self calculateDyn];
        [self updatePreviewImageForChannel:currentChannel];
        valueChanged = NO;
    }
    
     // CHANNEL_CH_9
     currentChannel = CHANNEL_CH_9;
     if (dynGateThreshold[currentChannel] != pkt->ch_9_gate_threshold) {
         dynGateThreshold[currentChannel] = pkt->ch_9_gate_threshold;
         valueChanged = YES;
     }
     if (dynGateRange[currentChannel] != pkt->ch_9_gate_range) {
         dynGateRange[currentChannel] = pkt->ch_9_gate_range;
         valueChanged = YES;
     }
     if (dynExpThreshold[currentChannel] != pkt->ch_9_expand_threshold) {
         dynExpThreshold[currentChannel] = pkt->ch_9_expand_threshold;
         valueChanged = YES;
     }
     if (dynExpRatio[currentChannel] != [Global getRatioValue:pkt->ch_9_expand_ratio]) {
         dynExpRatio[currentChannel] = [Global getRatioValue:pkt->ch_9_expand_ratio];
         valueChanged = YES;
     }
     if (dynCompThreshold[currentChannel] != pkt->ch_9_compressor_threshold) {
         dynCompThreshold[currentChannel] = pkt->ch_9_compressor_threshold;
         valueChanged = YES;
     }
     if (dynCompRatio[currentChannel] != [Global getRatioCompValue:pkt->ch_9_compressor_ratio]) {
         dynCompRatio[currentChannel] = [Global getRatioCompValue:pkt->ch_9_compressor_ratio];
         valueChanged = YES;
     }
     if (dynLimitThreshold[currentChannel] != pkt->ch_9_limiter_threshold) {
         dynLimitThreshold[currentChannel] = pkt->ch_9_limiter_threshold;
         valueChanged = YES;
     }
     if ([self updateDynStatus:pkt->channel_9_ctrl]) {
        valueChanged = YES;
     }
    
     if (valueChanged) {
         [self calculateDyn];
         [self updatePreviewImageForChannel:currentChannel];
         valueChanged = NO;
     }
     
     // CHANNEL_CH_10
     currentChannel = CHANNEL_CH_10;
     if (dynGateThreshold[currentChannel] != pkt->ch_10_gate_threshold) {
         dynGateThreshold[currentChannel] = pkt->ch_10_gate_threshold;
         valueChanged = YES;
     }
     if (dynGateRange[currentChannel] != pkt->ch_10_gate_range) {
         dynGateRange[currentChannel] = pkt->ch_10_gate_range;
         valueChanged = YES;
     }
     if (dynExpThreshold[currentChannel] != pkt->ch_10_expand_threshold) {
         dynExpThreshold[currentChannel] = pkt->ch_10_expand_threshold;
         valueChanged = YES;
     }
     if (dynExpRatio[currentChannel] != [Global getRatioValue:pkt->ch_10_expand_ratio]) {
         dynExpRatio[currentChannel] = [Global getRatioValue:pkt->ch_10_expand_ratio];
         valueChanged = YES;
     }
     if (dynCompThreshold[currentChannel] != pkt->ch_10_compressor_threshold) {
         dynCompThreshold[currentChannel] = pkt->ch_10_compressor_threshold;
         valueChanged = YES;
     }
     if (dynCompRatio[currentChannel] != [Global getRatioCompValue:pkt->ch_10_compressor_ratio]) {
         dynCompRatio[currentChannel] = [Global getRatioCompValue:pkt->ch_10_compressor_ratio];
         valueChanged = YES;
     }
     if (dynLimitThreshold[currentChannel] != pkt->ch_10_limiter_threshold) {
         dynLimitThreshold[currentChannel] = pkt->ch_10_limiter_threshold;
         valueChanged = YES;
     }
     if ([self updateDynStatus:pkt->channel_10_ctrl]) {
         valueChanged = YES;
     }
    
     if (valueChanged) {
         [self calculateDyn];
         [self updatePreviewImageForChannel:currentChannel];
         valueChanged = NO;
     }
     
     // CHANNEL_CH_11
     currentChannel = CHANNEL_CH_11;
     if (dynGateThreshold[currentChannel] != pkt->ch_11_gate_threshold) {
         dynGateThreshold[currentChannel] = pkt->ch_11_gate_threshold;
         valueChanged = YES;
     }
     if (dynGateRange[currentChannel] != pkt->ch_11_gate_range) {
         dynGateRange[currentChannel] = pkt->ch_11_gate_range;
         valueChanged = YES;
     }
     if (dynExpThreshold[currentChannel] != pkt->ch_11_expand_threshold) {
         dynExpThreshold[currentChannel] = pkt->ch_11_expand_threshold;
         valueChanged = YES;
     }
     if (dynExpRatio[currentChannel] != [Global getRatioValue:pkt->ch_11_expand_ratio]) {
         dynExpRatio[currentChannel] = [Global getRatioValue:pkt->ch_11_expand_ratio];
         valueChanged = YES;
     }
     if (dynCompThreshold[currentChannel] != pkt->ch_11_compressor_threshold) {
         dynCompThreshold[currentChannel] = pkt->ch_11_compressor_threshold;
         valueChanged = YES;
     }
     if (dynCompRatio[currentChannel] != [Global getRatioCompValue:pkt->ch_11_compressor_ratio]) {
         dynCompRatio[currentChannel] = [Global getRatioCompValue:pkt->ch_11_compressor_ratio];
         valueChanged = YES;
     }
     if (dynLimitThreshold[currentChannel] != pkt->ch_11_limiter_threshold) {
         dynLimitThreshold[currentChannel] = pkt->ch_11_limiter_threshold;
         valueChanged = YES;
     }
     if ([self updateDynStatus:pkt->channel_11_ctrl]) {
         valueChanged = YES;
     }

     if (valueChanged) {
         [self calculateDyn];
         [self updatePreviewImageForChannel:currentChannel];
         valueChanged = NO;
     }
     
     // CHANNEL_CH_12
     currentChannel = CHANNEL_CH_12;
     if (dynGateThreshold[currentChannel] != pkt->ch_12_gate_threshold) {
         dynGateThreshold[currentChannel] = pkt->ch_12_gate_threshold;
         valueChanged = YES;
     }
     if (dynGateRange[currentChannel] != pkt->ch_12_gate_range) {
         dynGateRange[currentChannel] = pkt->ch_12_gate_range;
         valueChanged = YES;
     }
     if (dynExpThreshold[currentChannel] != pkt->ch_12_expand_threshold) {
         dynExpThreshold[currentChannel] = pkt->ch_12_expand_threshold;
         valueChanged = YES;
     }
     if (dynExpRatio[currentChannel] != [Global getRatioValue:pkt->ch_12_expand_ratio]) {
         dynExpRatio[currentChannel] = [Global getRatioValue:pkt->ch_12_expand_ratio];
         valueChanged = YES;
     }
     if (dynCompThreshold[currentChannel] != pkt->ch_12_compressor_threshold) {
         dynCompThreshold[currentChannel] = pkt->ch_12_compressor_threshold;
         valueChanged = YES;
     }
     if (dynCompRatio[currentChannel] != [Global getRatioCompValue:pkt->ch_12_compressor_ratio]) {
         dynCompRatio[currentChannel] = [Global getRatioCompValue:pkt->ch_12_compressor_ratio];
         valueChanged = YES;
     }
     if (dynLimitThreshold[currentChannel] != pkt->ch_12_limiter_threshold) {
         dynLimitThreshold[currentChannel] = pkt->ch_12_limiter_threshold;
         valueChanged = YES;
     }
     if ([self updateDynStatus:pkt->channel_12_ctrl]) {
         valueChanged = YES;
     }
    
     if (valueChanged) {
         [self calculateDyn];
         [self updatePreviewImageForChannel:currentChannel];
         valueChanged = NO;
     }
     
     // CHANNEL_CH_13
     currentChannel = CHANNEL_CH_13;
     if (dynGateThreshold[currentChannel] != pkt->ch_13_gate_threshold) {
         dynGateThreshold[currentChannel] = pkt->ch_13_gate_threshold;
         valueChanged = YES;
     }
     if (dynGateRange[currentChannel] != pkt->ch_13_gate_range) {
         dynGateRange[currentChannel] = pkt->ch_13_gate_range;
         valueChanged = YES;
     }
     if (dynExpThreshold[currentChannel] != pkt->ch_13_expand_threshold) {
         dynExpThreshold[currentChannel] = pkt->ch_13_expand_threshold;
         valueChanged = YES;
     }
     if (dynExpRatio[currentChannel] != [Global getRatioValue:pkt->ch_13_expand_ratio]) {
         dynExpRatio[currentChannel] = [Global getRatioValue:pkt->ch_13_expand_ratio];
         valueChanged = YES;
     }
     if (dynCompThreshold[currentChannel] != pkt->ch_13_compressor_threshold) {
         dynCompThreshold[currentChannel] = pkt->ch_13_compressor_threshold;
         valueChanged = YES;
     }
     if (dynCompRatio[currentChannel] != [Global getRatioCompValue:pkt->ch_13_compressor_ratio]) {
         dynCompRatio[currentChannel] = [Global getRatioCompValue:pkt->ch_13_compressor_ratio];
         valueChanged = YES;
     }
     if (dynLimitThreshold[currentChannel] != pkt->ch_13_limiter_threshold) {
         dynLimitThreshold[currentChannel] = pkt->ch_13_limiter_threshold;
         valueChanged = YES;
     }
     if ([self updateDynStatus:pkt->channel_13_ctrl]) {
         valueChanged = YES;
     }
    
     if (valueChanged) {
         [self calculateDyn];
         [self updatePreviewImageForChannel:currentChannel];
         valueChanged = NO;
     }
     
     // CHANNEL_CH_14
     currentChannel = CHANNEL_CH_14;
     if (dynGateThreshold[currentChannel] != pkt->ch_14_gate_threshold) {
         dynGateThreshold[currentChannel] = pkt->ch_14_gate_threshold;
         valueChanged = YES;
     }
     if (dynGateRange[currentChannel] != pkt->ch_14_gate_range) {
         dynGateRange[currentChannel] = pkt->ch_14_gate_range;
         valueChanged = YES;
     }
     if (dynExpThreshold[currentChannel] != pkt->ch_14_expand_threshold) {
         dynExpThreshold[currentChannel] = pkt->ch_14_expand_threshold;
         valueChanged = YES;
     }
     if (dynExpRatio[currentChannel] != [Global getRatioValue:pkt->ch_14_expand_ratio]) {
         dynExpRatio[currentChannel] = [Global getRatioValue:pkt->ch_14_expand_ratio];
         valueChanged = YES;
     }
     if (dynCompThreshold[currentChannel] != pkt->ch_14_compressor_threshold) {
         dynCompThreshold[currentChannel] = pkt->ch_14_compressor_threshold;
         valueChanged = YES;
     }
     if (dynCompRatio[currentChannel] != [Global getRatioCompValue:pkt->ch_14_compressor_ratio]) {
         dynCompRatio[currentChannel] = [Global getRatioCompValue:pkt->ch_14_compressor_ratio];
         valueChanged = YES;
     }
     if (dynLimitThreshold[currentChannel] != pkt->ch_14_limiter_threshold) {
         dynLimitThreshold[currentChannel] = pkt->ch_14_limiter_threshold;
         valueChanged = YES;
     }
     if ([self updateDynStatus:pkt->channel_14_ctrl]) {
         valueChanged = YES;
     }
    
     if (valueChanged) {
         [self calculateDyn];
         [self updatePreviewImageForChannel:currentChannel];
         valueChanged = NO;
     }
     
     // CHANNEL_CH_15
     currentChannel = CHANNEL_CH_15;
     if (dynGateThreshold[currentChannel] != pkt->ch_15_gate_threshold) {
         dynGateThreshold[currentChannel] = pkt->ch_15_gate_threshold;
         valueChanged = YES;
     }
     if (dynGateRange[currentChannel] != pkt->ch_15_gate_range) {
         dynGateRange[currentChannel] = pkt->ch_15_gate_range;
         valueChanged = YES;
     }
     if (dynExpThreshold[currentChannel] != pkt->ch_15_expand_threshold) {
         dynExpThreshold[currentChannel] = pkt->ch_15_expand_threshold;
         valueChanged = YES;
     }
     if (dynExpRatio[currentChannel] != [Global getRatioValue:pkt->ch_15_expand_ratio]) {
         dynExpRatio[currentChannel] = [Global getRatioValue:pkt->ch_15_expand_ratio];
         valueChanged = YES;
     }
     if (dynCompThreshold[currentChannel] != pkt->ch_15_compressor_threshold) {
         dynCompThreshold[currentChannel] = pkt->ch_15_compressor_threshold;
         valueChanged = YES;
     }
     if (dynCompRatio[currentChannel] != [Global getRatioCompValue:pkt->ch_15_compressor_ratio]) {
         dynCompRatio[currentChannel] = [Global getRatioCompValue:pkt->ch_15_compressor_ratio];
         valueChanged = YES;
     }
     if (dynLimitThreshold[currentChannel] != pkt->ch_15_limiter_threshold) {
         dynLimitThreshold[currentChannel] = pkt->ch_15_limiter_threshold;
         valueChanged = YES;
     }
     if ([self updateDynStatus:pkt->channel_15_ctrl]) {
         valueChanged = YES;
     }
    
     if (valueChanged) {
         [self calculateDyn];
         [self updatePreviewImageForChannel:currentChannel];
         valueChanged = NO;
     }
     
     // CHANNEL_CH_16
     currentChannel = CHANNEL_CH_16;
     if (dynGateThreshold[currentChannel] != pkt->ch_16_gate_threshold) {
         dynGateThreshold[currentChannel] = pkt->ch_16_gate_threshold;
         valueChanged = YES;
     }
     if (dynGateRange[currentChannel] != pkt->ch_16_gate_range) {
         dynGateRange[currentChannel] = pkt->ch_16_gate_range;
         valueChanged = YES;
     }
     if (dynExpThreshold[currentChannel] != pkt->ch_16_expand_threshold) {
         dynExpThreshold[currentChannel] = pkt->ch_16_expand_threshold;
         valueChanged = YES;
     }
     if (dynExpRatio[currentChannel] != [Global getRatioValue:pkt->ch_16_expand_ratio]) {
         dynExpRatio[currentChannel] = [Global getRatioValue:pkt->ch_16_expand_ratio];
         valueChanged = YES;
     }
     if (dynCompThreshold[currentChannel] != pkt->ch_16_compressor_threshold) {
         dynCompThreshold[currentChannel] = pkt->ch_16_compressor_threshold;
         valueChanged = YES;
     }
     if (dynCompRatio[currentChannel] != [Global getRatioCompValue:pkt->ch_16_compressor_ratio]) {
         dynCompRatio[currentChannel] = [Global getRatioCompValue:pkt->ch_16_compressor_ratio];
         valueChanged = YES;
     }
     if (dynLimitThreshold[currentChannel] != pkt->ch_16_limiter_threshold) {
         dynLimitThreshold[currentChannel] = pkt->ch_16_limiter_threshold;
         valueChanged = YES;
     }
     if ([self updateDynStatus:pkt->channel_16_ctrl]) {
         valueChanged = YES;
     }
    
     if (valueChanged) {
         [self calculateDyn];
         [self updatePreviewImageForChannel:currentChannel];
         valueChanged = NO;
     }
}

- (void)processUsbAudioViewPage:(ViewPageUsbAudioPacket *)pkt {

     BOOL valueChanged = NO;
     
     _ch17Ctrl = pkt->ch_17_ctrl;
     _ch18Ctrl = pkt->ch_18_ctrl;
     
     // Only handle when DYN view is hidden
     if (!self.view.hidden) {
         NSLog(@"processUsbAudioViewPage: view is visible!");
         return;
     }
     
     // CHANNEL_CH_17
     currentChannel = CHANNEL_CH_17;
     if (dynGateThreshold[currentChannel] != pkt->ch_17_gate_threshold) {
         dynGateThreshold[currentChannel] = pkt->ch_17_gate_threshold;
         valueChanged = YES;
     }
     if (dynGateRange[currentChannel] != pkt->ch_17_gate_range) {
         dynGateRange[currentChannel] = pkt->ch_17_gate_range;
         valueChanged = YES;
     }
     if (dynExpThreshold[currentChannel] != pkt->ch_17_expand_threshold) {
         dynExpThreshold[currentChannel] = pkt->ch_17_expand_threshold;
         valueChanged = YES;
     }
     if (dynExpRatio[currentChannel] != [Global getRatioValue:pkt->ch_17_expand_ratio]) {
         dynExpRatio[currentChannel] = [Global getRatioValue:pkt->ch_17_expand_ratio];
         valueChanged = YES;
     }
     if (dynCompThreshold[currentChannel] != pkt->ch_17_compressor_threshold) {
         dynCompThreshold[currentChannel] = pkt->ch_17_compressor_threshold;
         valueChanged = YES;
     }
     if (dynCompRatio[currentChannel] != [Global getRatioCompValue:pkt->ch_17_compressor_ratio]) {
         dynCompRatio[currentChannel] = [Global getRatioCompValue:pkt->ch_17_compressor_ratio];
         valueChanged = YES;
     }
     if (dynLimitThreshold[currentChannel] != pkt->ch_17_limiter_threshold) {
         dynLimitThreshold[currentChannel] = pkt->ch_17_limiter_threshold;
         valueChanged = YES;
     }
     if ([self updateDynStatus:pkt->ch_17_ctrl]) {
         valueChanged = YES;
     }
    
     if (valueChanged) {
         [self calculateDyn];
         [self updatePreviewImageForChannel:currentChannel];
         valueChanged = NO;
     }
     
     // CHANNEL_CH_18
     currentChannel = CHANNEL_CH_18;
     if (dynGateThreshold[currentChannel] != pkt->ch_18_gate_threshold) {
         dynGateThreshold[currentChannel] = pkt->ch_18_gate_threshold;
         valueChanged = YES;
     }
     if (dynGateRange[currentChannel] != pkt->ch_18_gate_range) {
         dynGateRange[currentChannel] = pkt->ch_18_gate_range;
         valueChanged = YES;
     }
     if (dynExpThreshold[currentChannel] != pkt->ch_18_expand_threshold) {
         dynExpThreshold[currentChannel] = pkt->ch_18_expand_threshold;
         valueChanged = YES;
     }
     if (dynExpRatio[currentChannel] != [Global getRatioValue:pkt->ch_18_expand_ratio]) {
         dynExpRatio[currentChannel] = [Global getRatioValue:pkt->ch_18_expand_ratio];
         valueChanged = YES;
     }
     if (dynCompThreshold[currentChannel] != pkt->ch_18_compressor_threshold) {
         dynCompThreshold[currentChannel] = pkt->ch_18_compressor_threshold;
         valueChanged = YES;
     }
     if (dynCompRatio[currentChannel] != [Global getRatioCompValue:pkt->ch_18_compressor_ratio]) {
         dynCompRatio[currentChannel] = [Global getRatioCompValue:pkt->ch_18_compressor_ratio];
         valueChanged = YES;
     }
     if (dynLimitThreshold[currentChannel] != pkt->ch_18_limiter_threshold) {
         dynLimitThreshold[currentChannel] = pkt->ch_18_limiter_threshold;
         valueChanged = YES;
     }
     if ([self updateDynStatus:pkt->ch_18_ctrl]) {
         valueChanged = YES;
     }
    
     if (valueChanged) {
         [self calculateDyn];
         [self updatePreviewImageForChannel:currentChannel];
         valueChanged = NO;
     }
}

- (void)processMultiViewPage:(ViewPageMultiPacket *)pkt {

    BOOL valueChanged = NO;
     
     _multi1Ctrl = pkt->multi_1_ctrl;
     _multi2Ctrl = pkt->multi_2_ctrl;
     _multi3Ctrl = pkt->multi_3_ctrl;
     _multi4Ctrl = pkt->multi_4_ctrl;
     
     // Only handle when DYN view is hidden
     if (!self.view.hidden) {
         NSLog(@"processMultiViewPage: view is visible!");
         return;
     }
     
     // CHANNEL_MULTI_1
     currentChannel = CHANNEL_MULTI_1;
     if (dynGateThreshold[currentChannel] != pkt->multi_1_gate_threshold) {
         dynGateThreshold[currentChannel] = pkt->multi_1_gate_threshold;
         valueChanged = YES;
     }
     if (dynGateRange[currentChannel] != pkt->multi_1_gate_range) {
         dynGateRange[currentChannel] = pkt->multi_1_gate_range;
         valueChanged = YES;
     }
     if (dynExpThreshold[currentChannel] != pkt->multi_1_expand_threshold) {
         dynExpThreshold[currentChannel] = pkt->multi_1_expand_threshold;
         valueChanged = YES;
     }
     if (dynExpRatio[currentChannel] != [Global getRatioValue:pkt->multi_1_expand_ratio]) {
         dynExpRatio[currentChannel] = [Global getRatioValue:pkt->multi_1_expand_ratio];
         valueChanged = YES;
     }
     if (dynCompThreshold[currentChannel] != pkt->multi_1_compressor_threshold) {
         dynCompThreshold[currentChannel] = pkt->multi_1_compressor_threshold;
         valueChanged = YES;
     }
     if (dynCompRatio[currentChannel] != [Global getRatioCompValue:pkt->multi_1_compressor_ratio]) {
         dynCompRatio[currentChannel] = [Global getRatioCompValue:pkt->multi_1_compressor_ratio];
         valueChanged = YES;
     }
     if (dynLimitThreshold[currentChannel] != pkt->multi_1_limiter_threshold) {
         dynLimitThreshold[currentChannel] = pkt->multi_1_limiter_threshold;
         valueChanged = YES;
     }
     if ([self updateDynStatus:pkt->multi_1_ctrl]) {
         valueChanged = YES;
     }
 
     if (valueChanged) {
         [self calculateDyn];
         [self updatePreviewImageForChannel:currentChannel];
         valueChanged = NO;
     }
     
     // CHANNEL_MULTI_2
     currentChannel = CHANNEL_MULTI_2;
     if (dynGateThreshold[currentChannel] != pkt->multi_2_gate_threshold) {
         dynGateThreshold[currentChannel] = pkt->multi_2_gate_threshold;
         valueChanged = YES;
     }
     if (dynGateRange[currentChannel] != pkt->multi_2_gate_range) {
         dynGateRange[currentChannel] = pkt->multi_2_gate_range;
         valueChanged = YES;
     }
     if (dynExpThreshold[currentChannel] != pkt->multi_2_expand_threshold) {
         dynExpThreshold[currentChannel] = pkt->multi_2_expand_threshold;
         valueChanged = YES;
     }
     if (dynExpRatio[currentChannel] != [Global getRatioValue:pkt->multi_2_expand_ratio]) {
         dynExpRatio[currentChannel] = [Global getRatioValue:pkt->multi_2_expand_ratio];
         valueChanged = YES;
     }
     if (dynCompThreshold[currentChannel] != pkt->multi_2_compressor_threshold) {
         dynCompThreshold[currentChannel] = pkt->multi_2_compressor_threshold;
         valueChanged = YES;
     }
     if (dynCompRatio[currentChannel] != [Global getRatioCompValue:pkt->multi_2_compressor_ratio]) {
         dynCompRatio[currentChannel] = [Global getRatioCompValue:pkt->multi_2_compressor_ratio];
         valueChanged = YES;
     }
     if (dynLimitThreshold[currentChannel] != pkt->multi_2_limiter_threshold) {
         dynLimitThreshold[currentChannel] = pkt->multi_2_limiter_threshold;
         valueChanged = YES;
     }
     if ([self updateDynStatus:pkt->multi_2_ctrl]) {
         valueChanged = YES;
     }
    
     if (valueChanged) {
         [self calculateDyn];
         [self updatePreviewImageForChannel:currentChannel];
         valueChanged = NO;
     }
     
     // CHANNEL_MULTI_3
     currentChannel = CHANNEL_MULTI_3;
     if (dynGateThreshold[currentChannel] != pkt->multi_3_gate_threshold) {
         dynGateThreshold[currentChannel] = pkt->multi_3_gate_threshold;
         valueChanged = YES;
     }
     if (dynGateRange[currentChannel] != pkt->multi_3_gate_range) {
         dynGateRange[currentChannel] = pkt->multi_3_gate_range;
         valueChanged = YES;
     }
     if (dynExpThreshold[currentChannel] != pkt->multi_3_expand_threshold) {
         dynExpThreshold[currentChannel] = pkt->multi_3_expand_threshold;
         valueChanged = YES;
     }
     if (dynExpRatio[currentChannel] != [Global getRatioValue:pkt->multi_3_expand_ratio]) {
         dynExpRatio[currentChannel] = [Global getRatioValue:pkt->multi_3_expand_ratio];
         valueChanged = YES;
     }
     if (dynCompThreshold[currentChannel] != pkt->multi_3_compressor_threshold) {
         dynCompThreshold[currentChannel] = pkt->multi_3_compressor_threshold;
         valueChanged = YES;
     }
     if (dynCompRatio[currentChannel] != [Global getRatioCompValue:pkt->multi_3_compressor_ratio]) {
         dynCompRatio[currentChannel] = [Global getRatioCompValue:pkt->multi_3_compressor_ratio];
         valueChanged = YES;
     }
     if (dynLimitThreshold[currentChannel] != pkt->multi_3_limiter_threshold) {
         dynLimitThreshold[currentChannel] = pkt->multi_3_limiter_threshold;
         valueChanged = YES;
     }
     if ([self updateDynStatus:pkt->multi_3_ctrl]) {
         valueChanged = YES;
     }
    
     if (valueChanged) {
         [self calculateDyn];
         [self updatePreviewImageForChannel:currentChannel];
         valueChanged = NO;
     }
     
     // CHANNEL_MULTI_4
     currentChannel = CHANNEL_MULTI_4;
     if (dynGateThreshold[currentChannel] != pkt->multi_4_gate_threshold) {
         dynGateThreshold[currentChannel] = pkt->multi_4_gate_threshold;
         valueChanged = YES;
     }
     if (dynGateRange[currentChannel] != pkt->multi_4_gate_range) {
         dynGateRange[currentChannel] = pkt->multi_4_gate_range;
         valueChanged = YES;
     }
     if (dynExpThreshold[currentChannel] != pkt->multi_4_expand_threshold) {
         dynExpThreshold[currentChannel] = pkt->multi_4_expand_threshold;
         valueChanged = YES;
     }
     if (dynExpRatio[currentChannel] != [Global getRatioValue:pkt->multi_4_expand_ratio]) {
         dynExpRatio[currentChannel] = [Global getRatioValue:pkt->multi_4_expand_ratio];
         valueChanged = YES;
     }
     if (dynCompThreshold[currentChannel] != pkt->multi_4_compressor_threshold) {
         dynCompThreshold[currentChannel] = pkt->multi_4_compressor_threshold;
         valueChanged = YES;
     }
     if (dynCompRatio[currentChannel] != [Global getRatioCompValue:pkt->multi_4_compressor_ratio]) {
         dynCompRatio[currentChannel] = [Global getRatioCompValue:pkt->multi_4_compressor_ratio];
         valueChanged = YES;
     }
     if (dynLimitThreshold[currentChannel] != pkt->multi_4_limiter_threshold) {
         dynLimitThreshold[currentChannel] = pkt->multi_4_limiter_threshold;
         valueChanged = YES;
     }
     if ([self updateDynStatus:pkt->multi_4_ctrl]) {
         valueChanged = YES;
     }
    
     if (valueChanged) {
         [self calculateDyn];
         [self updatePreviewImageForChannel:currentChannel];
         valueChanged = NO;
     }
}

- (void)processMainViewPage:(ViewPageMainPacket *)pkt {

    BOOL valueChanged = NO;
     
     _mainCtrl = pkt->main_l_r_ctrl;
     
     // Only handle when DYN view is hidden
     if (!self.view.hidden) {
         NSLog(@"processMainViewPage: view is visible!");
         return;
     }
     
     // CHANNEL_MAIN
     currentChannel = CHANNEL_MAIN;
     if (dynGateThreshold[currentChannel] != pkt->main_l_r_gate_threshold) {
         dynGateThreshold[currentChannel] = pkt->main_l_r_gate_threshold;
         valueChanged = YES;
     }
     if (dynGateRange[currentChannel] != pkt->main_l_r_gate_range) {
         dynGateRange[currentChannel] = pkt->main_l_r_gate_range;
         valueChanged = YES;
     }
     if (dynExpThreshold[currentChannel] != pkt->main_l_r_expand_threshold) {
         dynExpThreshold[currentChannel] = pkt->main_l_r_expand_threshold;
         valueChanged = YES;
     }
     if (dynExpRatio[currentChannel] != [Global getRatioValue:pkt->main_l_r_expand_ratio]) {
         dynExpRatio[currentChannel] = [Global getRatioValue:pkt->main_l_r_expand_ratio];
         valueChanged = YES;
     }
     if (dynCompThreshold[currentChannel] != pkt->main_l_r_compressor_threshold) {
         dynCompThreshold[currentChannel] = pkt->main_l_r_compressor_threshold;
         valueChanged = YES;
     }
     if (dynCompRatio[currentChannel] != [Global getRatioCompValue:pkt->main_l_r_compressor_ratio]) {
         dynCompRatio[currentChannel] = [Global getRatioCompValue:pkt->main_l_r_compressor_ratio];
         valueChanged = YES;
     }
     if (dynLimitThreshold[currentChannel] != pkt->main_l_r_limiter_threshold) {
         dynLimitThreshold[currentChannel] = pkt->main_l_r_limiter_threshold;
         valueChanged = YES;
     }
     if ([self updateDynStatus:pkt->main_l_r_ctrl]) {
         valueChanged = YES;
     }
    
     if (valueChanged) {
         [self calculateDyn];
         [self updatePreviewImageForChannel:currentChannel];
     }
}

- (void)processChannelLabel:(AllMeterValueRxPacket *)pkt
{
    switch (currentChannel) {
        case CHANNEL_CH_1:
            txtPage.text = [NSString stringWithUTF8String:(const char *)pkt->channel_1_audio_name];
            meterR.progress = ((float)pkt->channel_1_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case CHANNEL_CH_2:
            txtPage.text = [NSString stringWithUTF8String:(const char *)pkt->channel_2_audio_name];
            meterR.progress = ((float)pkt->channel_2_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case CHANNEL_CH_3:
            txtPage.text = [NSString stringWithUTF8String:(const char *)pkt->channel_3_audio_name];
            meterR.progress = ((float)pkt->channel_3_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case CHANNEL_CH_4:
            txtPage.text = [NSString stringWithUTF8String:(const char *)pkt->channel_4_audio_name];
            meterR.progress = ((float)pkt->channel_4_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case CHANNEL_CH_5:
            txtPage.text = [NSString stringWithUTF8String:(const char *)pkt->channel_5_audio_name];
            meterR.progress = ((float)pkt->channel_5_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case CHANNEL_CH_6:
            txtPage.text = [NSString stringWithUTF8String:(const char *)pkt->channel_6_audio_name];
            meterR.progress = ((float)pkt->channel_6_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case CHANNEL_CH_7:
            txtPage.text = [NSString stringWithUTF8String:(const char *)pkt->channel_7_audio_name];
            meterR.progress = ((float)pkt->channel_7_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case CHANNEL_CH_8:
            txtPage.text = [NSString stringWithUTF8String:(const char *)pkt->channel_8_audio_name];
            meterR.progress = ((float)pkt->channel_8_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case CHANNEL_CH_9:
            txtPage.text = [NSString stringWithUTF8String:(const char *)pkt->channel_9_audio_name];
            meterR.progress = ((float)pkt->channel_9_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case CHANNEL_CH_10:
            txtPage.text = [NSString stringWithUTF8String:(const char *)pkt->channel_10_audio_name];
            meterR.progress = ((float)pkt->channel_10_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case CHANNEL_CH_11:
            txtPage.text = [NSString stringWithUTF8String:(const char *)pkt->channel_11_audio_name];
            meterR.progress = ((float)pkt->channel_11_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case CHANNEL_CH_12:
            txtPage.text = [NSString stringWithUTF8String:(const char *)pkt->channel_12_audio_name];
            meterR.progress = ((float)pkt->channel_12_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case CHANNEL_CH_13:
            txtPage.text = [NSString stringWithUTF8String:(const char *)pkt->channel_13_audio_name];
            meterR.progress = ((float)pkt->channel_13_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case CHANNEL_CH_14:
            txtPage.text = [NSString stringWithUTF8String:(const char *)pkt->channel_14_audio_name];
            meterR.progress = ((float)pkt->channel_14_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case CHANNEL_CH_15:
            txtPage.text = [NSString stringWithUTF8String:(const char *)pkt->channel_15_audio_name];
            meterR.progress = ((float)pkt->channel_15_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case CHANNEL_CH_16:
            txtPage.text = [NSString stringWithUTF8String:(const char *)pkt->channel_16_audio_name];
            meterR.progress = ((float)pkt->channel_16_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case CHANNEL_CH_17:
            txtPage.text = [NSString stringWithUTF8String:(const char *)pkt->channel_17_audio_name];
            meterR.progress = ((float)pkt->channel_17_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case CHANNEL_CH_18:
            txtPage.text = [NSString stringWithUTF8String:(const char *)pkt->channel_18_audio_name];
            meterR.progress = ((float)pkt->channel_18_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case CHANNEL_MULTI_1:
            txtPage.text = [NSString stringWithUTF8String:(const char *)pkt->multi_1_audio_name];
            meterR.progress = ((float)pkt->multi_1_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case CHANNEL_MULTI_2:
            txtPage.text = [NSString stringWithUTF8String:(const char *)pkt->multi_2_audio_name];
            meterR.progress = ((float)pkt->multi_2_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case CHANNEL_MULTI_3:
            txtPage.text = [NSString stringWithUTF8String:(const char *)pkt->multi_3_audio_name];
            meterR.progress = ((float)pkt->multi_3_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case CHANNEL_MULTI_4:
            txtPage.text = [NSString stringWithUTF8String:(const char *)pkt->multi_4_audio_name];
            meterR.progress = ((float)pkt->multi_4_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case CHANNEL_MAIN:
            txtPage.text = [NSString stringWithUTF8String:(const char *)pkt->main_audio_name];
            meterL.progress = ((float)pkt->main_l_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterR.progress = ((float)pkt->main_r_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        default:
            break;
    }
}

#pragma mark - Chart behavior

-(void)initPlot {
    [self configureHost];
    [self configureGraph];
    [self configurePlots];
    [self configureAxes];
}

-(void)configureHost {
    self.hostView = [(CPTGraphHostingView *)[CPTGraphHostingView alloc] initWithFrame:CGRectMake(271, 135, 274, 274)];
    self.hostView.allowPinchScaling = NO;
    self.hostView.backgroundColor = [UIColor clearColor];
    [self.view insertSubview:self.hostView belowSubview:self.viewOutput];
}

-(void)configureGraph {
    // Create the graph
    CPTGraph *graph = [[CPTXYGraph alloc] initWithFrame:self.hostView.frame];
    self.hostView.hostedGraph = graph;
    
    // Set padding for plot area
    [graph.plotAreaFrame setPaddingLeft:0.0f];
    [graph.plotAreaFrame setPaddingBottom:0.0f];
    
    // Enable user interactions for plot space
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *) graph.defaultPlotSpace;
    plotSpace.allowsUserInteraction = NO;
}

-(void)configurePlots {
    // Get graph and plot space
    CPTGraph *graph = self.hostView.hostedGraph;
    graph.axisSet = nil;
    graph.paddingLeft = 0.0;
    graph.paddingTop = 0.0;
    graph.paddingRight = 0.0;
    graph.paddingBottom = 0.0;
    CPTXYPlotSpace *plotSpace = (CPTXYPlotSpace *) graph.defaultPlotSpace;
    
    // Create threshold plot
    CPTScatterPlot *thresPlot = [[CPTScatterPlot alloc] init];
    thresPlot.dataSource = self;
    thresPlot.identifier = @"THRESHOLD";
    CPTColor *thresColor = [CPTColor blackColor];
    [graph addPlot:thresPlot toPlotSpace:plotSpace];
    
    // Create DYN plot
    CPTScatterPlot *dynPlot = [[CPTScatterPlot alloc] init];
    dynPlot.dataSource = self;
    dynPlot.identifier = @"DYN";
    CPTColor *dynColor = [CPTColor grayColor];
    [graph addPlot:dynPlot toPlotSpace:plotSpace];
    
    // Set up plot space
    CGFloat xMin = 0.0f;
    CGFloat xMax = 50.0f;
    CGFloat yMin = -50.0f;
    CGFloat yMax = 0.0f;
    plotSpace.xRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(xMin) length:CPTDecimalFromFloat(xMax-xMin)];
    plotSpace.yRange = [CPTPlotRange plotRangeWithLocation:CPTDecimalFromFloat(yMin) length:CPTDecimalFromFloat(yMax-yMin)];
    
    // Create styles and symbols
    CPTMutableLineStyle *dynLineStyle = [dynPlot.dataLineStyle mutableCopy];
    dynLineStyle.lineWidth = DYN_PLOT_WIDTH;
    dynLineStyle.lineColor = dynColor;
    dynPlot.dataLineStyle = dynLineStyle;
    dynPlot.plotSymbol = nil;
    dynPlot.interpolation = CPTScatterPlotInterpolationLinear;
    
    CPTMutableLineStyle *thresLineStyle = [thresPlot.dataLineStyle mutableCopy];
    thresLineStyle.lineWidth = THRESHOLD_PLOT_WIDTH;
    thresLineStyle.lineColor = thresColor;
    thresPlot.dataLineStyle = thresLineStyle;
    thresPlot.plotSymbol = nil;
    thresPlot.interpolation = CPTScatterPlotInterpolationLinear;
}

-(void)configureAxes {
}

#pragma mark - CPTPlotDataSource methods

-(NSUInteger)numberOfRecordsForPlot:(CPTPlot *)plot {
    if ([plot.identifier isEqual:@"DYN"]) {
        return [dynData count];
    } else {
        return [thresData count];
    }
}

-(NSNumber *)numberForPlot:(CPTPlot *)plot field:(NSUInteger)fieldEnum recordIndex:(NSUInteger)index {
    if ([plot.identifier isEqual:@"DYN"]) {
        NSInteger valueCount = [dynData count];
        switch (fieldEnum) {
            case CPTScatterPlotFieldX:
                if (index < valueCount) {
                    return [NSNumber numberWithUnsignedInteger:index];
                }
                break;
                
            case CPTScatterPlotFieldY:
                if (index < valueCount) {
                    return (NSNumber *)[dynData objectAtIndex:index];
                }
                break;
        }
        return [NSDecimalNumber zero];
    } else {
        NSDictionary *sample = [thresData objectAtIndex:index];
        if (fieldEnum == CPTScatterPlotFieldX)
            return [sample objectForKey:X_VAL];
        else
            return [sample objectForKey:Y_VAL];
    }
}

#pragma mark - Main view event handler methods

- (IBAction)onBtnDyn:(id)sender {
    btnDyn.selected = !btnDyn.selected;
    //dynEnabled[currentChannel] = btnDyn.selected;
    [self updateDynPlot:btnDyn.selected];
    [self updateThresPlot:btnDyn.selected];
    switch (currentChannel) {
        case CHANNEL_CH_1:
            [Global setClearBit:&_ch1Ctrl bitValue:btnDyn.selected bitOrder:1];
            [viewController sendData:CMD_CH1_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch1Ctrl];
            break;
        case CHANNEL_CH_2:
            [Global setClearBit:&_ch2Ctrl bitValue:btnDyn.selected bitOrder:1];
            [viewController sendData:CMD_CH2_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch2Ctrl];
            break;
        case CHANNEL_CH_3:
            [Global setClearBit:&_ch3Ctrl bitValue:btnDyn.selected bitOrder:1];
            [viewController sendData:CMD_CH3_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch3Ctrl];
            break;
        case CHANNEL_CH_4:
            [Global setClearBit:&_ch4Ctrl bitValue:btnDyn.selected bitOrder:1];
            [viewController sendData:CMD_CH4_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch4Ctrl];
            break;
        case CHANNEL_CH_5:
            [Global setClearBit:&_ch5Ctrl bitValue:btnDyn.selected bitOrder:1];
            [viewController sendData:CMD_CH5_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch5Ctrl];
            break;
        case CHANNEL_CH_6:
            [Global setClearBit:&_ch6Ctrl bitValue:btnDyn.selected bitOrder:1];
            [viewController sendData:CMD_CH6_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch6Ctrl];
            break;
        case CHANNEL_CH_7:
            [Global setClearBit:&_ch7Ctrl bitValue:btnDyn.selected bitOrder:1];
            [viewController sendData:CMD_CH7_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch7Ctrl];
            break;
        case CHANNEL_CH_8:
            [Global setClearBit:&_ch8Ctrl bitValue:btnDyn.selected bitOrder:1];
            [viewController sendData:CMD_CH8_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch8Ctrl];
            break;
        case CHANNEL_CH_9:
            [Global setClearBit:&_ch9Ctrl bitValue:btnDyn.selected bitOrder:1];
            [viewController sendData:CMD_CH9_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch9Ctrl];
            break;
        case CHANNEL_CH_10:
            [Global setClearBit:&_ch10Ctrl bitValue:btnDyn.selected bitOrder:1];
            [viewController sendData:CMD_CH10_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch10Ctrl];
            break;
        case CHANNEL_CH_11:
            [Global setClearBit:&_ch11Ctrl bitValue:btnDyn.selected bitOrder:1];
            [viewController sendData:CMD_CH11_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch11Ctrl];
            break;
        case CHANNEL_CH_12:
            [Global setClearBit:&_ch12Ctrl bitValue:btnDyn.selected bitOrder:1];
            [viewController sendData:CMD_CH12_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch12Ctrl];
            break;
        case CHANNEL_CH_13:
            [Global setClearBit:&_ch13Ctrl bitValue:btnDyn.selected bitOrder:1];
            [viewController sendData:CMD_CH13_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch13Ctrl];
            break;
        case CHANNEL_CH_14:
            [Global setClearBit:&_ch14Ctrl bitValue:btnDyn.selected bitOrder:1];
            [viewController sendData:CMD_CH14_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch14Ctrl];
            break;
        case CHANNEL_CH_15:
            [Global setClearBit:&_ch15Ctrl bitValue:btnDyn.selected bitOrder:1];
            [viewController sendData:CMD_CH15_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch15Ctrl];
            break;
        case CHANNEL_CH_16:
            [Global setClearBit:&_ch16Ctrl bitValue:btnDyn.selected bitOrder:1];
            [viewController sendData:CMD_CH16_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch16Ctrl];
            break;
        case CHANNEL_CH_17:
            [Global setClearBit:&_ch17Ctrl bitValue:btnDyn.selected bitOrder:1];
            [viewController sendData:CMD_CH17_CTRL commandType:DSP_WRITE dspId:DSP_6 value:_ch17Ctrl];
            break;
        case CHANNEL_CH_18:
            [Global setClearBit:&_ch18Ctrl bitValue:btnDyn.selected bitOrder:1];
            [viewController sendData:CMD_CH18_CTRL commandType:DSP_WRITE dspId:DSP_6 value:_ch18Ctrl];
            break;
        case CHANNEL_MULTI_1:
            [Global setClearBit:&_multi1Ctrl bitValue:btnDyn.selected bitOrder:1];
            [viewController sendData:CMD_MULTI_1_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi1Ctrl];
            break;
        case CHANNEL_MULTI_2:
            [Global setClearBit:&_multi2Ctrl bitValue:btnDyn.selected bitOrder:1];
            [viewController sendData:CMD_MULTI_2_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi2Ctrl];
            break;
        case CHANNEL_MULTI_3:
            [Global setClearBit:&_multi3Ctrl bitValue:btnDyn.selected bitOrder:1];
            [viewController sendData:CMD_MULTI_3_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi3Ctrl];
            break;
        case CHANNEL_MULTI_4:
            [Global setClearBit:&_multi4Ctrl bitValue:btnDyn.selected bitOrder:1];
            [viewController sendData:CMD_MULTI_4_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi4Ctrl];
            break;
        case CHANNEL_MAIN:
            [Global setClearBit:&_mainCtrl bitValue:btnDyn.selected bitOrder:1];
            [viewController sendData:CMD_MAIN_LR_CTRL commandType:DSP_WRITE dspId:DSP_6 value:_mainCtrl];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnGate:(id)sender {
    btnDynGate.selected = YES;
    btnDynExpander.selected = NO;
    btnDynComp.selected = NO;
    btnDynLimiter.selected = NO;
    [viewGate setHidden:NO];
    [viewExpander setHidden:YES];
    [viewComp setHidden:YES];
    [viewLimiter setHidden:YES];
    if (currentChannel == MAIN) {
        [imgBackground setImage:[UIImage imageNamed:@"basemap-37.png"]];
    } else {
        [imgBackground setImage:[UIImage imageNamed:@"basemap-38.png"]];
    }
    [btnBallRatio setHidden:YES];
    btnDynModeOnOff.selected = dynGateEnabled[currentChannel];
    [self setFileSceneMode];
    [self updateDynFrame];
}

- (IBAction)onBtnExpander:(id)sender {
    btnDynGate.selected = NO;
    btnDynExpander.selected = YES;
    btnDynComp.selected = NO;
    btnDynLimiter.selected = NO;
    [viewGate setHidden:YES];
    [viewExpander setHidden:NO];
    [viewComp setHidden:YES];
    [viewLimiter setHidden:YES];
    if (currentChannel == MAIN) {
        [imgBackground setImage:[UIImage imageNamed:@"basemap-37.png"]];
    } else {
        [imgBackground setImage:[UIImage imageNamed:@"basemap-38.png"]];
    }
    [btnBallRatio setHidden:NO];
    btnDynModeOnOff.selected = dynExpEnabled[currentChannel];
    [self setFileSceneMode];
    [self updateDynFrame];
}

- (IBAction)onBtnComp:(id)sender {
    btnDynGate.selected = NO;
    btnDynExpander.selected = NO;
    btnDynComp.selected = YES;
    btnDynLimiter.selected = NO;
    [viewGate setHidden:YES];
    [viewExpander setHidden:YES];
    [viewComp setHidden:NO];
    [viewLimiter setHidden:YES];
    if (currentChannel == MAIN) {
        [imgBackground setImage:[UIImage imageNamed:@"basemap-37.png"]];
    } else {
        [imgBackground setImage:[UIImage imageNamed:@"basemap-38.png"]];
    }
    [btnBallRatio setHidden:NO];
    btnDynModeOnOff.selected = dynCompEnabled[currentChannel];
    [self setFileSceneMode];
    [self updateDynFrame];
}

- (IBAction)onBtnLimiter:(id)sender {
    btnDynGate.selected = NO;
    btnDynExpander.selected = NO;
    btnDynComp.selected = NO;
    btnDynLimiter.selected = YES;
    [viewGate setHidden:YES];
    [viewExpander setHidden:YES];
    [viewComp setHidden:YES];
    [viewLimiter setHidden:NO];
    [imgBackground setImage:[UIImage imageNamed:@"basemap-42.png"]];
    [btnBallRatio setHidden:YES];
    btnDynModeOnOff.selected = dynLimitEnabled[currentChannel];
    [self setFileSceneMode];
    [self updateDynFrame];
}

- (IBAction)onBtnDynOnOff:(id)sender {
    btnDynModeOnOff.selected = !btnDynModeOnOff.selected;
    
    int bitOrder;
    if (btnDynGate.selected) {
        bitOrder = 8;
        dynGateEnabled[currentChannel] = btnDynModeOnOff.selected;
    } else if (btnDynExpander.selected) {
        bitOrder = 9;
        dynExpEnabled[currentChannel] = btnDynModeOnOff.selected;
    } else if (btnDynComp.selected) {
        bitOrder = 10;
        dynCompEnabled[currentChannel] = btnDynModeOnOff.selected;
    } else {
        bitOrder = 11;
        dynLimitEnabled[currentChannel] = btnDynModeOnOff.selected;
    }
    
    [self updateDynFrame];
    
    switch (currentChannel) {
        case CHANNEL_CH_1:
            [Global setClearBit:&_ch1Ctrl bitValue:btnDynModeOnOff.selected bitOrder:bitOrder];
            [viewController sendData:CMD_CH1_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch1Ctrl];
            break;
        case CHANNEL_CH_2:
            [Global setClearBit:&_ch2Ctrl bitValue:btnDynModeOnOff.selected bitOrder:bitOrder];
            [viewController sendData:CMD_CH2_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch2Ctrl];
            break;
        case CHANNEL_CH_3:
            [Global setClearBit:&_ch3Ctrl bitValue:btnDynModeOnOff.selected bitOrder:bitOrder];
            [viewController sendData:CMD_CH3_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch3Ctrl];
            break;
        case CHANNEL_CH_4:
            [Global setClearBit:&_ch4Ctrl bitValue:btnDynModeOnOff.selected bitOrder:bitOrder];
            [viewController sendData:CMD_CH4_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch4Ctrl];
            break;
        case CHANNEL_CH_5:
            [Global setClearBit:&_ch5Ctrl bitValue:btnDynModeOnOff.selected bitOrder:bitOrder];
            [viewController sendData:CMD_CH5_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch5Ctrl];
            break;
        case CHANNEL_CH_6:
            [Global setClearBit:&_ch6Ctrl bitValue:btnDynModeOnOff.selected bitOrder:bitOrder];
            [viewController sendData:CMD_CH6_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch6Ctrl];
            break;
        case CHANNEL_CH_7:
            [Global setClearBit:&_ch7Ctrl bitValue:btnDynModeOnOff.selected bitOrder:bitOrder];
            [viewController sendData:CMD_CH7_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch7Ctrl];
            break;
        case CHANNEL_CH_8:
            [Global setClearBit:&_ch8Ctrl bitValue:btnDynModeOnOff.selected bitOrder:bitOrder];
            [viewController sendData:CMD_CH8_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch8Ctrl];
            break;
        case CHANNEL_CH_9:
            [Global setClearBit:&_ch9Ctrl bitValue:btnDynModeOnOff.selected bitOrder:bitOrder];
            [viewController sendData:CMD_CH9_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch9Ctrl];
            break;
        case CHANNEL_CH_10:
            [Global setClearBit:&_ch10Ctrl bitValue:btnDynModeOnOff.selected bitOrder:bitOrder];
            [viewController sendData:CMD_CH10_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch10Ctrl];
            break;
        case CHANNEL_CH_11:
            [Global setClearBit:&_ch11Ctrl bitValue:btnDynModeOnOff.selected bitOrder:bitOrder];
            [viewController sendData:CMD_CH11_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch11Ctrl];
            break;
        case CHANNEL_CH_12:
            [Global setClearBit:&_ch12Ctrl bitValue:btnDynModeOnOff.selected bitOrder:bitOrder];
            [viewController sendData:CMD_CH12_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch12Ctrl];
            break;
        case CHANNEL_CH_13:
            [Global setClearBit:&_ch13Ctrl bitValue:btnDynModeOnOff.selected bitOrder:bitOrder];
            [viewController sendData:CMD_CH13_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch13Ctrl];
            break;
        case CHANNEL_CH_14:
            [Global setClearBit:&_ch14Ctrl bitValue:btnDynModeOnOff.selected bitOrder:bitOrder];
            [viewController sendData:CMD_CH14_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch14Ctrl];
            break;
        case CHANNEL_CH_15:
            [Global setClearBit:&_ch15Ctrl bitValue:btnDynModeOnOff.selected bitOrder:bitOrder];
            [viewController sendData:CMD_CH15_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch15Ctrl];
            break;
        case CHANNEL_CH_16:
            [Global setClearBit:&_ch16Ctrl bitValue:btnDynModeOnOff.selected bitOrder:bitOrder];
            [viewController sendData:CMD_CH16_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch16Ctrl];
            break;
        case CHANNEL_CH_17:
            [Global setClearBit:&_ch17Ctrl bitValue:btnDynModeOnOff.selected bitOrder:bitOrder];
            [viewController sendData:CMD_CH17_CTRL commandType:DSP_WRITE dspId:DSP_6 value:_ch17Ctrl];
            break;
        case CHANNEL_CH_18:
            [Global setClearBit:&_ch18Ctrl bitValue:btnDynModeOnOff.selected bitOrder:bitOrder];
            [viewController sendData:CMD_CH18_CTRL commandType:DSP_WRITE dspId:DSP_6 value:_ch18Ctrl];
            break;
        case CHANNEL_MULTI_1:
            [Global setClearBit:&_multi1Ctrl bitValue:btnDynModeOnOff.selected bitOrder:bitOrder];
            [viewController sendData:CMD_MULTI_1_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi1Ctrl];
            break;
        case CHANNEL_MULTI_2:
            [Global setClearBit:&_multi2Ctrl bitValue:btnDynModeOnOff.selected bitOrder:bitOrder];
            [viewController sendData:CMD_MULTI_2_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi2Ctrl];
            break;
        case CHANNEL_MULTI_3:
            [Global setClearBit:&_multi3Ctrl bitValue:btnDynModeOnOff.selected bitOrder:bitOrder];
            [viewController sendData:CMD_MULTI_3_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi3Ctrl];
            break;
        case CHANNEL_MULTI_4:
            [Global setClearBit:&_multi4Ctrl bitValue:btnDynModeOnOff.selected bitOrder:bitOrder];
            [viewController sendData:CMD_MULTI_4_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi4Ctrl];
            break;
        case CHANNEL_MAIN:
            [Global setClearBit:&_mainCtrl bitValue:btnDynModeOnOff.selected bitOrder:bitOrder];
            [viewController sendData:CMD_MAIN_LR_CTRL commandType:DSP_WRITE dspId:DSP_6 value:_mainCtrl];
            break;
        default:
            break;
    }
}

- (void)setFileSceneMode {
    int sceneChannel;
    switch (currentChannel) {
        case CHANNEL_CH_17:
            sceneChannel = SCENE_CHANNEL_CH_17;
            break;
        case CHANNEL_CH_18:
            sceneChannel = SCENE_CHANNEL_CH_18;
            break;
        case CHANNEL_MULTI_1:
            sceneChannel = SCENE_CHANNEL_MULTI_1;
            break;
        case CHANNEL_MULTI_2:
            sceneChannel = SCENE_CHANNEL_MULTI_2;
            break;
        case CHANNEL_MULTI_3:
            sceneChannel = SCENE_CHANNEL_MULTI_3;
            break;
        case CHANNEL_MULTI_4:
            sceneChannel = SCENE_CHANNEL_MULTI_4;
            break;
        case CHANNEL_MAIN:
            sceneChannel = SCENE_CHANNEL_MAIN;
            break;
        default:
            sceneChannel = currentChannel;
            break;
    }
    
    if (btnDynGate.selected) {
        [viewController setFileSceneMode:SCENE_MODE_DYN_GATE sceneChannel:sceneChannel];
    } else if (btnDynExpander.selected) {
        [viewController setFileSceneMode:SCENE_MODE_DYN_EXP sceneChannel:sceneChannel];
    } else if (btnDynComp.selected) {
        [viewController setFileSceneMode:SCENE_MODE_DYN_COMP sceneChannel:sceneChannel];
    } else {
        [viewController setFileSceneMode:SCENE_MODE_DYN_LIM sceneChannel:sceneChannel];
    }
}

- (IBAction)onBtnFile:(id)sender {
    [self setFileSceneMode];
    [viewController.view bringSubviewToFront:viewController.viewScenes];
    [viewController.viewScenes setHidden:NO];
}

- (IBAction)onBtnReset:(id)sender {
    [self resetDynDefault];
    [self updatePreviewImage];
}

- (IBAction)onBtnBack:(id)sender {
    [self.delegate didFinishEditingDyn];
    [self.view setHidden:YES];
    [viewController restoreSendPage];
}

- (void)updateChannelLabels
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [meterL setHidden:YES];
    [meterDynOutL setHidden:YES];
    [meterDynGrL setHidden:YES];
    currentChannelLabel = currentChannel;
    
    [btnSolo setTitle:@"SOLO" forState:UIControlStateNormal];
    [btnSolo setBackgroundImage:[UIImage imageNamed:@"button-2.png"] forState:UIControlStateNormal];
    [btnSolo setTitle:@"SOLO" forState:UIControlStateSelected];
    [btnSolo setBackgroundImage:[UIImage imageNamed:@"button-4.png"] forState:UIControlStateSelected];
    [btnMeterPrePost setTitle:@"METER INPUT" forState:UIControlStateNormal];
    [btnMeterPrePost setBackgroundImage:[UIImage imageNamed:@"button-6.png"] forState:UIControlStateNormal];
    
    if (btnDynGate.selected) {
        [imgBackground setImage:[UIImage imageNamed:@"basemap-38.png"]];
    } else if (btnDynExpander.selected) {
        [imgBackground setImage:[UIImage imageNamed:@"basemap-40.png"]];
    } else if (btnDynComp.selected) {
        [imgBackground setImage:[UIImage imageNamed:@"basemap-38.png"]];
    } else {
        [imgBackground setImage:[UIImage imageNamed:@"basemap-42.png"]];
    }
    
    switch (currentChannel) {
        case CHANNEL_CH_1:
            if ([userDefaults objectForKey:@"Ch1"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch1"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch1", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch1", @"Acapela", nil)];
            [btnSolo setHidden:NO];
            [slider setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            break;
        case CHANNEL_CH_2:
            if ([userDefaults objectForKey:@"Ch2"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch2"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch2", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch2", @"Acapela", nil)];
            [btnSolo setHidden:NO];
            [slider setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            break;
        case CHANNEL_CH_3:
            if ([userDefaults objectForKey:@"Ch3"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch3"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch3", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch3", @"Acapela", nil)];
            [btnSolo setHidden:NO];
            [slider setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            break;
        case CHANNEL_CH_4:
            if ([userDefaults objectForKey:@"Ch4"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch4"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch4", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch4", @"Acapela", nil)];
            [btnSolo setHidden:NO];
            [slider setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            break;
        case CHANNEL_CH_5:
            if ([userDefaults objectForKey:@"Ch5"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch5"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch5", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch5", @"Acapela", nil)];
            [btnSolo setHidden:NO];
            [slider setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            break;
        case CHANNEL_CH_6:
            if ([userDefaults objectForKey:@"Ch6"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch6"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch6", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch6", @"Acapela", nil)];
            [btnSolo setHidden:NO];
            [slider setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            break;
        case CHANNEL_CH_7:
            if ([userDefaults objectForKey:@"Ch7"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch7"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch7", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch7", @"Acapela", nil)];
            [btnSolo setHidden:NO];
            [slider setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            break;
        case CHANNEL_CH_8:
            if ([userDefaults objectForKey:@"Ch8"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch8"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch8", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch8", @"Acapela", nil)];
            [btnSolo setHidden:NO];
            [slider setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            break;
        case CHANNEL_CH_9:
            if ([userDefaults objectForKey:@"Ch9"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch9"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch9", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch9", @"Acapela", nil)];
            [btnSolo setHidden:NO];
            [slider setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            break;
        case CHANNEL_CH_10:
            if ([userDefaults objectForKey:@"Ch10"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch10"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch10", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch10", @"Acapela", nil)];
            [btnSolo setHidden:NO];
            [slider setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            break;
        case CHANNEL_CH_11:
            if ([userDefaults objectForKey:@"Ch11"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch11"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch11", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch11", @"Acapela", nil)];
            [btnSolo setHidden:NO];
            [slider setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            break;
        case CHANNEL_CH_12:
            if ([userDefaults objectForKey:@"Ch12"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch12"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch12", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch12", @"Acapela", nil)];
            [btnSolo setHidden:NO];
            [slider setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            break;
        case CHANNEL_CH_13:
            if ([userDefaults objectForKey:@"Ch13"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch13"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch13", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch13", @"Acapela", nil)];
            [btnSolo setHidden:NO];
            [slider setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            break;
        case CHANNEL_CH_14:
            if ([userDefaults objectForKey:@"Ch14"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch14"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch14", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch14", @"Acapela", nil)];
            [btnSolo setHidden:NO];
            [slider setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            break;
        case CHANNEL_CH_15:
            if ([userDefaults objectForKey:@"Ch15"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch15"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch15", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch15", @"Acapela", nil)];
            [btnSolo setHidden:NO];
            [slider setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            break;
        case CHANNEL_CH_16:
            if ([userDefaults objectForKey:@"Ch16"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch16"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch16", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch16", @"Acapela", nil)];
            [btnSolo setHidden:NO];
            [slider setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            break;
        case CHANNEL_CH_17:
            if ([userDefaults objectForKey:@"Ch17"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch17"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch17", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch17", @"Acapela", nil)];
            [btnSolo setHidden:NO];
            [slider setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            break;
        case CHANNEL_CH_18:
            if ([userDefaults objectForKey:@"Ch18"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch18"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch18", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch18", @"Acapela", nil)];
            [btnSolo setHidden:NO];
            [slider setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            break;
        case CHANNEL_MULTI_1:
            if ([userDefaults objectForKey:@"Multi1"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Multi1"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Multi1", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Multi1", @"Acapela", nil)];
            [btnSolo setHidden:YES];
            [slider setThumbImage:[UIImage imageNamed:@"fader-4.png"] forState:UIControlStateNormal];
            [btnMeterPrePost setTitle:@"METER PRE" forState:UIControlStateNormal];
            [btnMeterPrePost setBackgroundImage:[UIImage imageNamed:@"button-3.png"] forState:UIControlStateNormal];
            break;
        case CHANNEL_MULTI_2:
            if ([userDefaults objectForKey:@"Multi2"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Multi2"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Multi2", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Multi2", @"Acapela", nil)];
            [btnSolo setHidden:YES];
            [slider setThumbImage:[UIImage imageNamed:@"fader-4.png"] forState:UIControlStateNormal];
            [btnMeterPrePost setTitle:@"METER PRE" forState:UIControlStateNormal];
            [btnMeterPrePost setBackgroundImage:[UIImage imageNamed:@"button-3.png"] forState:UIControlStateNormal];
            break;
        case CHANNEL_MULTI_3:
            if ([userDefaults objectForKey:@"Multi3"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Multi3"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Multi3", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Multi3", @"Acapela", nil)];
            [btnSolo setHidden:YES];
            [slider setThumbImage:[UIImage imageNamed:@"fader-4.png"] forState:UIControlStateNormal];
            [btnMeterPrePost setTitle:@"METER PRE" forState:UIControlStateNormal];
            [btnMeterPrePost setBackgroundImage:[UIImage imageNamed:@"button-3.png"] forState:UIControlStateNormal];
            break;
        case CHANNEL_MULTI_4:
            if ([userDefaults objectForKey:@"Multi4"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Multi4"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Multi4", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Multi4", @"Acapela", nil)];
            [btnSolo setHidden:YES];
            [slider setThumbImage:[UIImage imageNamed:@"fader-4.png"] forState:UIControlStateNormal];
            [btnMeterPrePost setTitle:@"METER PRE" forState:UIControlStateNormal];
            [btnMeterPrePost setBackgroundImage:[UIImage imageNamed:@"button-3.png"] forState:UIControlStateNormal];
            break;
        case CHANNEL_MAIN:
            if ([userDefaults objectForKey:@"Main"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Main"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Main", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Main", @"Acapela", nil)];
            [btnSolo setHidden:NO];
            [btnSolo setTitle:@"STEREO" forState:UIControlStateNormal];
            [btnSolo setBackgroundImage:[UIImage imageNamed:@"button-4.png"] forState:UIControlStateNormal];
            [btnSolo setTitle:@"MONO" forState:UIControlStateSelected];
            [btnSolo setBackgroundImage:[UIImage imageNamed:@"button-5.png"] forState:UIControlStateSelected];
            [slider setThumbImage:[UIImage imageNamed:@"fader-3.png"] forState:UIControlStateNormal];
            [meterL setHidden:NO];
            [meterDynOutL setHidden:NO];
            [meterDynGrL setHidden:NO];
            [btnMeterPrePost setTitle:@"METER PRE" forState:UIControlStateNormal];
            [btnMeterPrePost setBackgroundImage:[UIImage imageNamed:@"button-3.png"] forState:UIControlStateNormal];
            
            if (btnDynGate.selected) {
                [imgBackground setImage:[UIImage imageNamed:@"basemap-37.png"]];
            } else if (btnDynExpander.selected) {
                [imgBackground setImage:[UIImage imageNamed:@"basemap-39.png"]];
            } else if (btnDynComp.selected) {
                [imgBackground setImage:[UIImage imageNamed:@"basemap-37.png"]];
            } else {
                [imgBackground setImage:[UIImage imageNamed:@"basemap-41.png"]];
            }
            break;
        default:
            break;
    }
}

- (IBAction)onBtnSelectDown:(id)sender {
    if (currentChannel > CHANNEL_CH_1) {
        currentChannel--;
        currentChannelLabel = currentChannel;
    }
    [self updateChannelLabels];
}

- (IBAction)onBtnSelectUp:(id)sender {
    if (currentChannel < CHANNEL_MAIN) {
        currentChannel++;
        currentChannelLabel = currentChannel;
    }
    [self updateChannelLabels];
}

- (IBAction)onBtnOn:(id)sender {
    btnOn.selected = !btnOn.selected;
    switch (currentChannel) {
        case CHANNEL_CH_1:
            [Global setClearBit:&_chOnOff bitValue:btnOn.selected bitOrder:0];
            [viewController sendData:CMD_CH_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_chOnOff];
            break;
        case CHANNEL_CH_2:
            [Global setClearBit:&_chOnOff bitValue:btnOn.selected bitOrder:1];
            [viewController sendData:CMD_CH_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_chOnOff];
            break;
        case CHANNEL_CH_3:
            [Global setClearBit:&_chOnOff bitValue:btnOn.selected bitOrder:2];
            [viewController sendData:CMD_CH_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_chOnOff];
            break;
        case CHANNEL_CH_4:
            [Global setClearBit:&_chOnOff bitValue:btnOn.selected bitOrder:3];
            [viewController sendData:CMD_CH_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_chOnOff];
            break;
        case CHANNEL_CH_5:
            [Global setClearBit:&_chOnOff bitValue:btnOn.selected bitOrder:4];
            [viewController sendData:CMD_CH_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_chOnOff];
            break;
        case CHANNEL_CH_6:
            [Global setClearBit:&_chOnOff bitValue:btnOn.selected bitOrder:5];
            [viewController sendData:CMD_CH_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_chOnOff];
            break;
        case CHANNEL_CH_7:
            [Global setClearBit:&_chOnOff bitValue:btnOn.selected bitOrder:6];
            [viewController sendData:CMD_CH_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_chOnOff];
            break;
        case CHANNEL_CH_8:
            [Global setClearBit:&_chOnOff bitValue:btnOn.selected bitOrder:7];
            [viewController sendData:CMD_CH_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_chOnOff];
            break;
        case CHANNEL_CH_9:
            [Global setClearBit:&_chOnOff bitValue:btnOn.selected bitOrder:8];
            [viewController sendData:CMD_CH_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_chOnOff];
            break;
        case CHANNEL_CH_10:
            [Global setClearBit:&_chOnOff bitValue:btnOn.selected bitOrder:9];
            [viewController sendData:CMD_CH_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_chOnOff];
            break;
        case CHANNEL_CH_11:
            [Global setClearBit:&_chOnOff bitValue:btnOn.selected bitOrder:10];
            [viewController sendData:CMD_CH_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_chOnOff];
            break;
        case CHANNEL_CH_12:
            [Global setClearBit:&_chOnOff bitValue:btnOn.selected bitOrder:11];
            [viewController sendData:CMD_CH_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_chOnOff];
            break;
        case CHANNEL_CH_13:
            [Global setClearBit:&_chOnOff bitValue:btnOn.selected bitOrder:12];
            [viewController sendData:CMD_CH_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_chOnOff];
            break;
        case CHANNEL_CH_14:
            [Global setClearBit:&_chOnOff bitValue:btnOn.selected bitOrder:13];
            [viewController sendData:CMD_CH_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_chOnOff];
            break;
        case CHANNEL_CH_15:
            [Global setClearBit:&_chOnOff bitValue:btnOn.selected bitOrder:14];
            [viewController sendData:CMD_CH_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_chOnOff];
            break;
        case CHANNEL_CH_16:
            [Global setClearBit:&_chOnOff bitValue:btnOn.selected bitOrder:15];
            [viewController sendData:CMD_CH_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_chOnOff];
            break;
        case CHANNEL_CH_17:
            [Global setClearBit:&_ch17Setting bitValue:btnOn.selected bitOrder:8];
            [viewController sendData:CMD_CH17_USB_AUDIO_SETTING commandType:DSP_WRITE dspId:DSP_5 value:_ch17Setting];
            break;
        case CHANNEL_CH_18:
            [Global setClearBit:&_ch18Setting bitValue:btnOn.selected bitOrder:8];
            [viewController sendData:CMD_CH18_USB_AUDIO_SETTING commandType:DSP_WRITE dspId:DSP_5 value:_ch18Setting];
            break;
        case CHANNEL_MULTI_1:
            [Global setClearBit:&_multi1Ctrl bitValue:btnOn.selected bitOrder:0];
            [viewController sendData:CMD_MULTI_1_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi1Ctrl];
            break;
        case CHANNEL_MULTI_2:
            [Global setClearBit:&_multi2Ctrl bitValue:btnOn.selected bitOrder:0];
            [viewController sendData:CMD_MULTI_2_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi2Ctrl];
            break;
        case CHANNEL_MULTI_3:
            [Global setClearBit:&_multi3Ctrl bitValue:btnOn.selected bitOrder:0];
            [viewController sendData:CMD_MULTI_3_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi3Ctrl];
            break;
        case CHANNEL_MULTI_4:
            [Global setClearBit:&_multi4Ctrl bitValue:btnOn.selected bitOrder:0];
            [viewController sendData:CMD_MULTI_4_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi4Ctrl];
            break;
        case CHANNEL_MAIN:
            [Global setClearBit:&_mainCtrl bitValue:btnOn.selected bitOrder:0];
            [viewController sendData:CMD_MAIN_LR_CTRL commandType:DSP_WRITE dspId:DSP_6 value:_mainCtrl];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnMeterPrePost:(id)sender {
    btnMeterPrePost.selected = !btnMeterPrePost.selected;
    switch (currentChannel) {
        case CHANNEL_CH_1:
            [Global setClearBit:&_chMeterPrePost bitValue:btnMeterPrePost.selected bitOrder:0];
            [viewController sendData:CMD_CH_METER commandType:DSP_WRITE dspId:DSP_5 value:_chMeterPrePost];
            break;
        case CHANNEL_CH_2:
            [Global setClearBit:&_chMeterPrePost bitValue:btnMeterPrePost.selected bitOrder:1];
            [viewController sendData:CMD_CH_METER commandType:DSP_WRITE dspId:DSP_5 value:_chMeterPrePost];
            break;
        case CHANNEL_CH_3:
            [Global setClearBit:&_chMeterPrePost bitValue:btnMeterPrePost.selected bitOrder:2];
            [viewController sendData:CMD_CH_METER commandType:DSP_WRITE dspId:DSP_5 value:_chMeterPrePost];
            break;
        case CHANNEL_CH_4:
            [Global setClearBit:&_chMeterPrePost bitValue:btnMeterPrePost.selected bitOrder:3];
            [viewController sendData:CMD_CH_METER commandType:DSP_WRITE dspId:DSP_5 value:_chMeterPrePost];
            break;
        case CHANNEL_CH_5:
            [Global setClearBit:&_chMeterPrePost bitValue:btnMeterPrePost.selected bitOrder:4];
            [viewController sendData:CMD_CH_METER commandType:DSP_WRITE dspId:DSP_5 value:_chMeterPrePost];
            break;
        case CHANNEL_CH_6:
            [Global setClearBit:&_chMeterPrePost bitValue:btnMeterPrePost.selected bitOrder:5];
            [viewController sendData:CMD_CH_METER commandType:DSP_WRITE dspId:DSP_5 value:_chMeterPrePost];
            break;
        case CHANNEL_CH_7:
            [Global setClearBit:&_chMeterPrePost bitValue:btnMeterPrePost.selected bitOrder:6];
            [viewController sendData:CMD_CH_METER commandType:DSP_WRITE dspId:DSP_5 value:_chMeterPrePost];
            break;
        case CHANNEL_CH_8:
            [Global setClearBit:&_chMeterPrePost bitValue:btnMeterPrePost.selected bitOrder:7];
            [viewController sendData:CMD_CH_METER commandType:DSP_WRITE dspId:DSP_5 value:_chMeterPrePost];
            break;
        case CHANNEL_CH_9:
            [Global setClearBit:&_chMeterPrePost bitValue:btnMeterPrePost.selected bitOrder:8];
            [viewController sendData:CMD_CH_METER commandType:DSP_WRITE dspId:DSP_5 value:_chMeterPrePost];
            break;
        case CHANNEL_CH_10:
            [Global setClearBit:&_chMeterPrePost bitValue:btnMeterPrePost.selected bitOrder:9];
            [viewController sendData:CMD_CH_METER commandType:DSP_WRITE dspId:DSP_5 value:_chMeterPrePost];
            break;
        case CHANNEL_CH_11:
            [Global setClearBit:&_chMeterPrePost bitValue:btnMeterPrePost.selected bitOrder:10];
            [viewController sendData:CMD_CH_METER commandType:DSP_WRITE dspId:DSP_5 value:_chMeterPrePost];
            break;
        case CHANNEL_CH_12:
            [Global setClearBit:&_chMeterPrePost bitValue:btnMeterPrePost.selected bitOrder:11];
            [viewController sendData:CMD_CH_METER commandType:DSP_WRITE dspId:DSP_5 value:_chMeterPrePost];
            break;
        case CHANNEL_CH_13:
            [Global setClearBit:&_chMeterPrePost bitValue:btnMeterPrePost.selected bitOrder:12];
            [viewController sendData:CMD_CH_METER commandType:DSP_WRITE dspId:DSP_5 value:_chMeterPrePost];
            break;
        case CHANNEL_CH_14:
            [Global setClearBit:&_chMeterPrePost bitValue:btnMeterPrePost.selected bitOrder:13];
            [viewController sendData:CMD_CH_METER commandType:DSP_WRITE dspId:DSP_5 value:_chMeterPrePost];
            break;
        case CHANNEL_CH_15:
            [Global setClearBit:&_chMeterPrePost bitValue:btnMeterPrePost.selected bitOrder:14];
            [viewController sendData:CMD_CH_METER commandType:DSP_WRITE dspId:DSP_5 value:_chMeterPrePost];
            break;
        case CHANNEL_CH_16:
            [Global setClearBit:&_chMeterPrePost bitValue:btnMeterPrePost.selected bitOrder:15];
            [viewController sendData:CMD_CH_METER commandType:DSP_WRITE dspId:DSP_5 value:_chMeterPrePost];
            break;
        case CHANNEL_CH_17:
            [Global setClearBit:&_ch17Setting bitValue:btnMeterPrePost.selected bitOrder:15];
            [viewController sendData:CMD_CH17_USB_AUDIO_SETTING commandType:DSP_WRITE dspId:DSP_5 value:_ch17Setting];
            break;
        case CHANNEL_CH_18:
            [Global setClearBit:&_ch18Setting bitValue:btnMeterPrePost.selected bitOrder:15];
            [viewController sendData:CMD_CH18_USB_AUDIO_SETTING commandType:DSP_WRITE dspId:DSP_5 value:_ch18Setting];
            break;
        case CHANNEL_MULTI_1:
            [Global setClearBit:&_multiMeter bitValue:btnMeterPrePost.selected bitOrder:0];
            [viewController sendData:CMD_MULTI_METER commandType:DSP_WRITE dspId:DSP_7 value:_multiMeter];
            break;
        case CHANNEL_MULTI_2:
            [Global setClearBit:&_multiMeter bitValue:btnMeterPrePost.selected bitOrder:1];
            [viewController sendData:CMD_MULTI_METER commandType:DSP_WRITE dspId:DSP_7 value:_multiMeter];
            break;
        case CHANNEL_MULTI_3:
            [Global setClearBit:&_multiMeter bitValue:btnMeterPrePost.selected bitOrder:2];
            [viewController sendData:CMD_MULTI_METER commandType:DSP_WRITE dspId:DSP_7 value:_multiMeter];
            break;
        case CHANNEL_MULTI_4:
            [Global setClearBit:&_multiMeter bitValue:btnMeterPrePost.selected bitOrder:3];
            [viewController sendData:CMD_MULTI_METER commandType:DSP_WRITE dspId:DSP_7 value:_multiMeter];
            break;
        case CHANNEL_MAIN:
            [Global setClearBit:&_mainMeter bitValue:btnMeterPrePost.selected bitOrder:0];
            [viewController sendData:CMD_MAIN_METER commandType:DSP_WRITE dspId:DSP_6 value:_mainMeter];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnSolo:(id)sender {
    btnSolo.selected = !btnSolo.selected;
    switch (currentChannel) {
        case CHANNEL_CH_1:
            [Global setClearBit:&_soloCtrl bitValue:btnSolo.selected bitOrder:0];
            [viewController sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloCtrl];
            break;
        case CHANNEL_CH_2:
            [Global setClearBit:&_soloCtrl bitValue:btnSolo.selected bitOrder:1];
            [viewController sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloCtrl];
            break;
        case CHANNEL_CH_3:
            [Global setClearBit:&_soloCtrl bitValue:btnSolo.selected bitOrder:2];
            [viewController sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloCtrl];
            break;
        case CHANNEL_CH_4:
            [Global setClearBit:&_soloCtrl bitValue:btnSolo.selected bitOrder:3];
            [viewController sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloCtrl];
            break;
        case CHANNEL_CH_5:
            [Global setClearBit:&_soloCtrl bitValue:btnSolo.selected bitOrder:4];
            [viewController sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloCtrl];
            break;
        case CHANNEL_CH_6:
            [Global setClearBit:&_soloCtrl bitValue:btnSolo.selected bitOrder:5];
            [viewController sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloCtrl];
            break;
        case CHANNEL_CH_7:
            [Global setClearBit:&_soloCtrl bitValue:btnSolo.selected bitOrder:6];
            [viewController sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloCtrl];
            break;
        case CHANNEL_CH_8:
            [Global setClearBit:&_soloCtrl bitValue:btnSolo.selected bitOrder:7];
            [viewController sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloCtrl];
            break;
        case CHANNEL_CH_9:
            [Global setClearBit:&_soloCtrl bitValue:btnSolo.selected bitOrder:8];
            [viewController sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloCtrl];
            break;
        case CHANNEL_CH_10:
            [Global setClearBit:&_soloCtrl bitValue:btnSolo.selected bitOrder:9];
            [viewController sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloCtrl];
            break;
        case CHANNEL_CH_11:
            [Global setClearBit:&_soloCtrl bitValue:btnSolo.selected bitOrder:10];
            [viewController sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloCtrl];
            break;
        case CHANNEL_CH_12:
            [Global setClearBit:&_soloCtrl bitValue:btnSolo.selected bitOrder:11];
            [viewController sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloCtrl];
            break;
        case CHANNEL_CH_13:
            [Global setClearBit:&_soloCtrl bitValue:btnSolo.selected bitOrder:12];
            [viewController sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloCtrl];
            break;
        case CHANNEL_CH_14:
            [Global setClearBit:&_soloCtrl bitValue:btnSolo.selected bitOrder:13];
            [viewController sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloCtrl];
            break;
        case CHANNEL_CH_15:
            [Global setClearBit:&_soloCtrl bitValue:btnSolo.selected bitOrder:14];
            [viewController sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloCtrl];
            break;
        case CHANNEL_CH_16:
            [Global setClearBit:&_soloCtrl bitValue:btnSolo.selected bitOrder:15];
            [viewController sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloCtrl];
            break;
        case CHANNEL_CH_17:
            [Global setClearBit:&_ch17Setting bitValue:btnSolo.selected bitOrder:10];
            [viewController sendData:CMD_CH17_USB_AUDIO_SETTING commandType:DSP_WRITE dspId:DSP_5 value:_ch17Setting];
            break;
        case CHANNEL_CH_18:
            [Global setClearBit:&_ch18Setting bitValue:btnSolo.selected bitOrder:10];
            [viewController sendData:CMD_CH18_USB_AUDIO_SETTING commandType:DSP_WRITE dspId:DSP_5 value:_ch18Setting];
            break;
        case CHANNEL_MAIN:
            [Global setClearBit:&_mainMeter bitValue:btnSolo.selected bitOrder:7];
            [viewController sendData:CMD_MAIN_METER commandType:DSP_WRITE dspId:DSP_6 value:_mainMeter];
            break;
        default:
            break;
    }
}

- (IBAction)onSliderBegin:(id)sender {
    _sliderLock = YES;
}

- (IBAction)onSliderEnd:(id)sender {
    _sliderLock = NO;
}

- (IBAction)onSliderAction:(id)sender {
    [lblSliderValue setText:[Global getSliderGain:slider.value appendDb:YES]];
    if (sender != nil) {
        switch (currentChannel) {
            case CHANNEL_CH_1:
                [viewController sendData:CMD_CH1_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:slider.value];
                break;
            case CHANNEL_CH_2:
                [viewController sendData:CMD_CH2_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:slider.value];
                break;
            case CHANNEL_CH_3:
                [viewController sendData:CMD_CH3_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:slider.value];
                break;
            case CHANNEL_CH_4:
                [viewController sendData:CMD_CH4_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:slider.value];
                break;
            case CHANNEL_CH_5:
                [viewController sendData:CMD_CH5_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:slider.value];
                break;
            case CHANNEL_CH_6:
                [viewController sendData:CMD_CH6_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:slider.value];
                break;
            case CHANNEL_CH_7:
                [viewController sendData:CMD_CH7_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:slider.value];
                break;
            case CHANNEL_CH_8:
                [viewController sendData:CMD_CH8_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:slider.value];
                break;
            case CHANNEL_CH_9:
                [viewController sendData:CMD_CH9_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:slider.value];
                break;
            case CHANNEL_CH_10:
                [viewController sendData:CMD_CH10_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:slider.value];
                break;
            case CHANNEL_CH_11:
                [viewController sendData:CMD_CH11_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:slider.value];
                break;
            case CHANNEL_CH_12:
                [viewController sendData:CMD_CH12_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:slider.value];
                break;
            case CHANNEL_CH_13:
                [viewController sendData:CMD_CH13_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:slider.value];
                break;
            case CHANNEL_CH_14:
                [viewController sendData:CMD_CH14_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:slider.value];
                break;
            case CHANNEL_CH_15:
                [viewController sendData:CMD_CH15_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:slider.value];
                break;
            case CHANNEL_CH_16:
                [viewController sendData:CMD_CH16_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:slider.value];
                break;
            case CHANNEL_CH_17:
                [viewController sendData:CMD_CH17_USB_AUDIO_FADER commandType:DSP_WRITE dspId:DSP_5 value:slider.value];
                break;
            case CHANNEL_CH_18:
                [viewController sendData:CMD_CH18_USB_AUDIO_FADER commandType:DSP_WRITE dspId:DSP_5 value:slider.value];
                break;
            case CHANNEL_MULTI_1:
                [viewController sendData:CMD_MULTI1_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_7 value:slider.value];
                break;
            case CHANNEL_MULTI_2:
                [viewController sendData:CMD_MULTI2_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_7 value:slider.value];
                break;
            case CHANNEL_MULTI_3:
                [viewController sendData:CMD_MULTI3_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_7 value:slider.value];
                break;
            case CHANNEL_MULTI_4:
                [viewController sendData:CMD_MULTI4_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_7 value:slider.value];
                break;
            case CHANNEL_MAIN:
                [viewController sendData:CMD_MAIN_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_6 value:slider.value];
                break;
            default:
                break;
        }
    }
}

#pragma mark -
#pragma mark Gate view event handler methods

- (IBAction)knobGateThresholdDidChange:(id)sender
{
    lblGateThreshold.text = [NSString stringWithFormat:@"%d dB", (int)knobGateThreshold.value-THRESHOLD_MAX];
    cpvGateThreshold.progress = knobGateThreshold.value/THRESHOLD_MAX;
    dynGateThreshold[currentChannel] = knobGateThreshold.value-THRESHOLD_MAX;
    [self updateDynFrame];
    if (sender != nil) {
        switch (currentChannel) {
            case CHANNEL_CH_1:
                [viewController sendData:CMD_CH1_GATE_THRESHOLD commandType:DSP_WRITE dspId:DSP_1 value:knobGateThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_2:
                [viewController sendData:CMD_CH2_GATE_THRESHOLD commandType:DSP_WRITE dspId:DSP_1 value:knobGateThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_3:
                [viewController sendData:CMD_CH3_GATE_THRESHOLD commandType:DSP_WRITE dspId:DSP_1 value:knobGateThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_4:
                [viewController sendData:CMD_CH4_GATE_THRESHOLD commandType:DSP_WRITE dspId:DSP_1 value:knobGateThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_5:
                [viewController sendData:CMD_CH1_GATE_THRESHOLD commandType:DSP_WRITE dspId:DSP_2 value:knobGateThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_6:
                [viewController sendData:CMD_CH2_GATE_THRESHOLD commandType:DSP_WRITE dspId:DSP_2 value:knobGateThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_7:
                [viewController sendData:CMD_CH3_GATE_THRESHOLD commandType:DSP_WRITE dspId:DSP_2 value:knobGateThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_8:
                [viewController sendData:CMD_CH4_GATE_THRESHOLD commandType:DSP_WRITE dspId:DSP_2 value:knobGateThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_9:
                [viewController sendData:CMD_CH1_GATE_THRESHOLD commandType:DSP_WRITE dspId:DSP_3 value:knobGateThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_10:
                [viewController sendData:CMD_CH2_GATE_THRESHOLD commandType:DSP_WRITE dspId:DSP_3 value:knobGateThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_11:
                [viewController sendData:CMD_CH3_GATE_THRESHOLD commandType:DSP_WRITE dspId:DSP_3 value:knobGateThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_12:
                [viewController sendData:CMD_CH4_GATE_THRESHOLD commandType:DSP_WRITE dspId:DSP_3 value:knobGateThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_13:
                [viewController sendData:CMD_CH1_GATE_THRESHOLD commandType:DSP_WRITE dspId:DSP_4 value:knobGateThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_14:
                [viewController sendData:CMD_CH2_GATE_THRESHOLD commandType:DSP_WRITE dspId:DSP_4 value:knobGateThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_15:
                [viewController sendData:CMD_CH3_GATE_THRESHOLD commandType:DSP_WRITE dspId:DSP_4 value:knobGateThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_16:
                [viewController sendData:CMD_CH4_GATE_THRESHOLD commandType:DSP_WRITE dspId:DSP_4 value:knobGateThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_17:
                [viewController sendData:CMD_CH17_GATE_THRESHOLD commandType:DSP_WRITE dspId:DSP_6 value:knobGateThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_18:
                [viewController sendData:CMD_CH18_GATE_THRESHOLD commandType:DSP_WRITE dspId:DSP_6 value:knobGateThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_MULTI_1:
                [viewController sendData:CMD_MULTI1_GATE_THRESHOLD commandType:DSP_WRITE dspId:DSP_7 value:knobGateThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_MULTI_2:
                [viewController sendData:CMD_MULTI2_GATE_THRESHOLD commandType:DSP_WRITE dspId:DSP_7 value:knobGateThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_MULTI_3:
                [viewController sendData:CMD_MULTI3_GATE_THRESHOLD commandType:DSP_WRITE dspId:DSP_7 value:knobGateThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_MULTI_4:
                [viewController sendData:CMD_MULTI4_GATE_THRESHOLD commandType:DSP_WRITE dspId:DSP_7 value:knobGateThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_MAIN:
                [viewController sendData:CMD_MAIN_LR_GATE_THRESHOLD commandType:DSP_WRITE dspId:DSP_6 value:knobGateThreshold.value-THRESHOLD_MAX];
                break;
            default:
                break;
        }
    }
}

- (IBAction)knobGateRangeDidChange:(id)sender
{
    lblGateRange.text = [NSString stringWithFormat:@"%d dB", (int)knobGateRange.value-RANGE_MAX];
    cpvGateRange.progress = knobGateRange.value/RANGE_MAX;
    dynGateRange[currentChannel] = knobGateRange.value-RANGE_MAX;
    [self updateDynFrame];
    if (sender != nil) {
        switch (currentChannel) {
            case CHANNEL_CH_1:
                [viewController sendData:CMD_CH1_GATE_RANGE commandType:DSP_WRITE dspId:DSP_1 value:knobGateRange.value-RANGE_MAX];
                break;
            case CHANNEL_CH_2:
                [viewController sendData:CMD_CH2_GATE_RANGE commandType:DSP_WRITE dspId:DSP_1 value:knobGateRange.value-RANGE_MAX];
                break;
            case CHANNEL_CH_3:
                [viewController sendData:CMD_CH3_GATE_RANGE commandType:DSP_WRITE dspId:DSP_1 value:knobGateRange.value-RANGE_MAX];
                break;
            case CHANNEL_CH_4:
                [viewController sendData:CMD_CH4_GATE_RANGE commandType:DSP_WRITE dspId:DSP_1 value:knobGateRange.value-RANGE_MAX];
                break;
            case CHANNEL_CH_5:
                [viewController sendData:CMD_CH1_GATE_RANGE commandType:DSP_WRITE dspId:DSP_2 value:knobGateRange.value-RANGE_MAX];
                break;
            case CHANNEL_CH_6:
                [viewController sendData:CMD_CH2_GATE_RANGE commandType:DSP_WRITE dspId:DSP_2 value:knobGateRange.value-RANGE_MAX];
                break;
            case CHANNEL_CH_7:
                [viewController sendData:CMD_CH3_GATE_RANGE commandType:DSP_WRITE dspId:DSP_2 value:knobGateRange.value-RANGE_MAX];
                break;
            case CHANNEL_CH_8:
                [viewController sendData:CMD_CH4_GATE_RANGE commandType:DSP_WRITE dspId:DSP_2 value:knobGateRange.value-RANGE_MAX];
                break;
            case CHANNEL_CH_9:
                [viewController sendData:CMD_CH1_GATE_RANGE commandType:DSP_WRITE dspId:DSP_3 value:knobGateRange.value-RANGE_MAX];
                break;
            case CHANNEL_CH_10:
                [viewController sendData:CMD_CH2_GATE_RANGE commandType:DSP_WRITE dspId:DSP_3 value:knobGateRange.value-RANGE_MAX];
                break;
            case CHANNEL_CH_11:
                [viewController sendData:CMD_CH3_GATE_RANGE commandType:DSP_WRITE dspId:DSP_3 value:knobGateRange.value-RANGE_MAX];
                break;
            case CHANNEL_CH_12:
                [viewController sendData:CMD_CH4_GATE_RANGE commandType:DSP_WRITE dspId:DSP_3 value:knobGateRange.value-RANGE_MAX];
                break;
            case CHANNEL_CH_13:
                [viewController sendData:CMD_CH1_GATE_RANGE commandType:DSP_WRITE dspId:DSP_4 value:knobGateRange.value-RANGE_MAX];
                break;
            case CHANNEL_CH_14:
                [viewController sendData:CMD_CH2_GATE_RANGE commandType:DSP_WRITE dspId:DSP_4 value:knobGateRange.value-RANGE_MAX];
                break;
            case CHANNEL_CH_15:
                [viewController sendData:CMD_CH3_GATE_RANGE commandType:DSP_WRITE dspId:DSP_4 value:knobGateRange.value-RANGE_MAX];
                break;
            case CHANNEL_CH_16:
                [viewController sendData:CMD_CH4_GATE_RANGE commandType:DSP_WRITE dspId:DSP_4 value:knobGateRange.value-RANGE_MAX];
                break;
            case CHANNEL_CH_17:
                [viewController sendData:CMD_CH17_GATE_RANGE commandType:DSP_WRITE dspId:DSP_6 value:knobGateRange.value-RANGE_MAX];
                break;
            case CHANNEL_CH_18:
                [viewController sendData:CMD_CH18_GATE_RANGE commandType:DSP_WRITE dspId:DSP_6 value:knobGateRange.value-RANGE_MAX];
                break;
            case CHANNEL_MULTI_1:
                [viewController sendData:CMD_MULTI1_GATE_RANGE commandType:DSP_WRITE dspId:DSP_7 value:knobGateRange.value-RANGE_MAX];
                break;
            case CHANNEL_MULTI_2:
                [viewController sendData:CMD_MULTI2_GATE_RANGE commandType:DSP_WRITE dspId:DSP_7 value:knobGateRange.value-RANGE_MAX];
                break;
            case CHANNEL_MULTI_3:
                [viewController sendData:CMD_MULTI3_GATE_RANGE commandType:DSP_WRITE dspId:DSP_7 value:knobGateRange.value-RANGE_MAX];
                break;
            case CHANNEL_MULTI_4:
                [viewController sendData:CMD_MULTI4_GATE_RANGE commandType:DSP_WRITE dspId:DSP_7 value:knobGateRange.value-RANGE_MAX];
                break;
            case CHANNEL_MAIN:
                [viewController sendData:CMD_MAIN_LR_GATE_RANGE commandType:DSP_WRITE dspId:DSP_6 value:knobGateRange.value-RANGE_MAX];
                break;
            default:
                break;
        }
    }
}

- (IBAction)knobGateAttackDidChange:(id)sender
{
    lblGateAttack.text = [Global getTimeString:knobGateAttack.value];
    cpvGateAttack.progress = knobGateAttack.value/ATTACK_RELEASE_MAX;
    if (sender != nil) {
        switch (currentChannel) {
            case CHANNEL_CH_1:
                [viewController sendData:CMD_CH1_GATE_ATTACK commandType:DSP_WRITE dspId:DSP_1 value:knobGateAttack.value];
                break;
            case CHANNEL_CH_2:
                [viewController sendData:CMD_CH2_GATE_ATTACK commandType:DSP_WRITE dspId:DSP_1 value:knobGateAttack.value];
                break;
            case CHANNEL_CH_3:
                [viewController sendData:CMD_CH3_GATE_ATTACK commandType:DSP_WRITE dspId:DSP_1 value:knobGateAttack.value];
                break;
            case CHANNEL_CH_4:
                [viewController sendData:CMD_CH4_GATE_ATTACK commandType:DSP_WRITE dspId:DSP_1 value:knobGateAttack.value];
                break;
            case CHANNEL_CH_5:
                [viewController sendData:CMD_CH1_GATE_ATTACK commandType:DSP_WRITE dspId:DSP_2 value:knobGateAttack.value];
                break;
            case CHANNEL_CH_6:
                [viewController sendData:CMD_CH2_GATE_ATTACK commandType:DSP_WRITE dspId:DSP_2 value:knobGateAttack.value];
                break;
            case CHANNEL_CH_7:
                [viewController sendData:CMD_CH3_GATE_ATTACK commandType:DSP_WRITE dspId:DSP_2 value:knobGateAttack.value];
                break;
            case CHANNEL_CH_8:
                [viewController sendData:CMD_CH4_GATE_ATTACK commandType:DSP_WRITE dspId:DSP_2 value:knobGateAttack.value];
                break;
            case CHANNEL_CH_9:
                [viewController sendData:CMD_CH1_GATE_ATTACK commandType:DSP_WRITE dspId:DSP_3 value:knobGateAttack.value];
                break;
            case CHANNEL_CH_10:
                [viewController sendData:CMD_CH2_GATE_ATTACK commandType:DSP_WRITE dspId:DSP_3 value:knobGateAttack.value];
                break;
            case CHANNEL_CH_11:
                [viewController sendData:CMD_CH3_GATE_ATTACK commandType:DSP_WRITE dspId:DSP_3 value:knobGateAttack.value];
                break;
            case CHANNEL_CH_12:
                [viewController sendData:CMD_CH4_GATE_ATTACK commandType:DSP_WRITE dspId:DSP_3 value:knobGateAttack.value];
                break;
            case CHANNEL_CH_13:
                [viewController sendData:CMD_CH1_GATE_ATTACK commandType:DSP_WRITE dspId:DSP_4 value:knobGateAttack.value];
                break;
            case CHANNEL_CH_14:
                [viewController sendData:CMD_CH2_GATE_ATTACK commandType:DSP_WRITE dspId:DSP_4 value:knobGateAttack.value];
                break;
            case CHANNEL_CH_15:
                [viewController sendData:CMD_CH3_GATE_ATTACK commandType:DSP_WRITE dspId:DSP_4 value:knobGateAttack.value];
                break;
            case CHANNEL_CH_16:
                [viewController sendData:CMD_CH4_GATE_ATTACK commandType:DSP_WRITE dspId:DSP_4 value:knobGateAttack.value];
                break;
            case CHANNEL_CH_17:
                [viewController sendData:CMD_CH17_GATE_ATTACK commandType:DSP_WRITE dspId:DSP_6 value:knobGateAttack.value];
                break;
            case CHANNEL_CH_18:
                [viewController sendData:CMD_CH18_GATE_ATTACK commandType:DSP_WRITE dspId:DSP_6 value:knobGateAttack.value];
                break;
            case CHANNEL_MULTI_1:
                [viewController sendData:CMD_MULTI1_GATE_ATTACK commandType:DSP_WRITE dspId:DSP_7 value:knobGateAttack.value];
                break;
            case CHANNEL_MULTI_2:
                [viewController sendData:CMD_MULTI2_GATE_ATTACK commandType:DSP_WRITE dspId:DSP_7 value:knobGateAttack.value];
                break;
            case CHANNEL_MULTI_3:
                [viewController sendData:CMD_MULTI3_GATE_ATTACK commandType:DSP_WRITE dspId:DSP_7 value:knobGateAttack.value];
                break;
            case CHANNEL_MULTI_4:
                [viewController sendData:CMD_MULTI4_GATE_ATTACK commandType:DSP_WRITE dspId:DSP_7 value:knobGateAttack.value];
                break;
            case CHANNEL_MAIN:
                [viewController sendData:CMD_MAIN_LR_GATE_ATTACK commandType:DSP_WRITE dspId:DSP_6 value:knobGateAttack.value];
                break;
            default:
                break;
        }
    }
}

- (IBAction)knobGateHoldDidChange:(id)sender
{
    lblGateHold.text = [Global getTimeString:knobGateHold.value];
    cpvGateHold.progress = knobGateHold.value/HOLD_MAX;
    if (sender != nil) {
        switch (currentChannel) {
            case CHANNEL_CH_1:
                [viewController sendData:CMD_CH1_GATE_HOLD commandType:DSP_WRITE dspId:DSP_1 value:knobGateHold.value];
                break;
            case CHANNEL_CH_2:
                [viewController sendData:CMD_CH2_GATE_HOLD commandType:DSP_WRITE dspId:DSP_1 value:knobGateHold.value];
                break;
            case CHANNEL_CH_3:
                [viewController sendData:CMD_CH3_GATE_HOLD commandType:DSP_WRITE dspId:DSP_1 value:knobGateHold.value];
                break;
            case CHANNEL_CH_4:
                [viewController sendData:CMD_CH4_GATE_HOLD commandType:DSP_WRITE dspId:DSP_1 value:knobGateHold.value];
                break;
            case CHANNEL_CH_5:
                [viewController sendData:CMD_CH1_GATE_HOLD commandType:DSP_WRITE dspId:DSP_2 value:knobGateHold.value];
                break;
            case CHANNEL_CH_6:
                [viewController sendData:CMD_CH2_GATE_HOLD commandType:DSP_WRITE dspId:DSP_2 value:knobGateHold.value];
                break;
            case CHANNEL_CH_7:
                [viewController sendData:CMD_CH3_GATE_HOLD commandType:DSP_WRITE dspId:DSP_2 value:knobGateHold.value];
                break;
            case CHANNEL_CH_8:
                [viewController sendData:CMD_CH4_GATE_HOLD commandType:DSP_WRITE dspId:DSP_2 value:knobGateHold.value];
                break;
            case CHANNEL_CH_9:
                [viewController sendData:CMD_CH1_GATE_HOLD commandType:DSP_WRITE dspId:DSP_3 value:knobGateHold.value];
                break;
            case CHANNEL_CH_10:
                [viewController sendData:CMD_CH2_GATE_HOLD commandType:DSP_WRITE dspId:DSP_3 value:knobGateHold.value];
                break;
            case CHANNEL_CH_11:
                [viewController sendData:CMD_CH3_GATE_HOLD commandType:DSP_WRITE dspId:DSP_3 value:knobGateHold.value];
                break;
            case CHANNEL_CH_12:
                [viewController sendData:CMD_CH4_GATE_HOLD commandType:DSP_WRITE dspId:DSP_3 value:knobGateHold.value];
                break;
            case CHANNEL_CH_13:
                [viewController sendData:CMD_CH1_GATE_HOLD commandType:DSP_WRITE dspId:DSP_4 value:knobGateHold.value];
                break;
            case CHANNEL_CH_14:
                [viewController sendData:CMD_CH2_GATE_HOLD commandType:DSP_WRITE dspId:DSP_4 value:knobGateHold.value];
                break;
            case CHANNEL_CH_15:
                [viewController sendData:CMD_CH3_GATE_HOLD commandType:DSP_WRITE dspId:DSP_4 value:knobGateHold.value];
                break;
            case CHANNEL_CH_16:
                [viewController sendData:CMD_CH4_GATE_HOLD commandType:DSP_WRITE dspId:DSP_4 value:knobGateHold.value];
                break;
            case CHANNEL_CH_17:
                [viewController sendData:CMD_CH17_GATE_HOLD commandType:DSP_WRITE dspId:DSP_6 value:knobGateHold.value];
                break;
            case CHANNEL_CH_18:
                [viewController sendData:CMD_CH18_GATE_HOLD commandType:DSP_WRITE dspId:DSP_6 value:knobGateHold.value];
                break;
            case CHANNEL_MULTI_1:
                [viewController sendData:CMD_MULTI1_GATE_HOLD commandType:DSP_WRITE dspId:DSP_7 value:knobGateHold.value];
                break;
            case CHANNEL_MULTI_2:
                [viewController sendData:CMD_MULTI2_GATE_HOLD commandType:DSP_WRITE dspId:DSP_7 value:knobGateHold.value];
                break;
            case CHANNEL_MULTI_3:
                [viewController sendData:CMD_MULTI3_GATE_HOLD commandType:DSP_WRITE dspId:DSP_7 value:knobGateHold.value];
                break;
            case CHANNEL_MULTI_4:
                [viewController sendData:CMD_MULTI4_GATE_HOLD commandType:DSP_WRITE dspId:DSP_7 value:knobGateHold.value];
                break;
            case CHANNEL_MAIN:
                [viewController sendData:CMD_MAIN_LR_GATE_HOLD commandType:DSP_WRITE dspId:DSP_6 value:knobGateHold.value];
                break;
            default:
                break;
        }
    }
}

- (IBAction)knobGateReleaseDidChange:(id)sender
{
    lblGateRelease.text = [Global getTimeString:knobGateRelease.value];
    cpvGateRelease.progress = knobGateRelease.value/ATTACK_RELEASE_MAX;
    if (sender != nil) {
        switch (currentChannel) {
            case CHANNEL_CH_1:
                [viewController sendData:CMD_CH1_GATE_RELEASE commandType:DSP_WRITE dspId:DSP_1 value:knobGateRelease.value];
                break;
            case CHANNEL_CH_2:
                [viewController sendData:CMD_CH2_GATE_RELEASE commandType:DSP_WRITE dspId:DSP_1 value:knobGateRelease.value];
                break;
            case CHANNEL_CH_3:
                [viewController sendData:CMD_CH3_GATE_RELEASE commandType:DSP_WRITE dspId:DSP_1 value:knobGateRelease.value];
                break;
            case CHANNEL_CH_4:
                [viewController sendData:CMD_CH4_GATE_RELEASE commandType:DSP_WRITE dspId:DSP_1 value:knobGateRelease.value];
                break;
            case CHANNEL_CH_5:
                [viewController sendData:CMD_CH1_GATE_RELEASE commandType:DSP_WRITE dspId:DSP_2 value:knobGateRelease.value];
                break;
            case CHANNEL_CH_6:
                [viewController sendData:CMD_CH2_GATE_RELEASE commandType:DSP_WRITE dspId:DSP_2 value:knobGateRelease.value];
                break;
            case CHANNEL_CH_7:
                [viewController sendData:CMD_CH3_GATE_RELEASE commandType:DSP_WRITE dspId:DSP_2 value:knobGateRelease.value];
                break;
            case CHANNEL_CH_8:
                [viewController sendData:CMD_CH4_GATE_RELEASE commandType:DSP_WRITE dspId:DSP_2 value:knobGateRelease.value];
                break;
            case CHANNEL_CH_9:
                [viewController sendData:CMD_CH1_GATE_RELEASE commandType:DSP_WRITE dspId:DSP_3 value:knobGateRelease.value];
                break;
            case CHANNEL_CH_10:
                [viewController sendData:CMD_CH2_GATE_RELEASE commandType:DSP_WRITE dspId:DSP_3 value:knobGateRelease.value];
                break;
            case CHANNEL_CH_11:
                [viewController sendData:CMD_CH3_GATE_RELEASE commandType:DSP_WRITE dspId:DSP_3 value:knobGateRelease.value];
                break;
            case CHANNEL_CH_12:
                [viewController sendData:CMD_CH4_GATE_RELEASE commandType:DSP_WRITE dspId:DSP_3 value:knobGateRelease.value];
                break;
            case CHANNEL_CH_13:
                [viewController sendData:CMD_CH1_GATE_RELEASE commandType:DSP_WRITE dspId:DSP_4 value:knobGateRelease.value];
                break;
            case CHANNEL_CH_14:
                [viewController sendData:CMD_CH2_GATE_RELEASE commandType:DSP_WRITE dspId:DSP_4 value:knobGateRelease.value];
                break;
            case CHANNEL_CH_15:
                [viewController sendData:CMD_CH3_GATE_RELEASE commandType:DSP_WRITE dspId:DSP_4 value:knobGateRelease.value];
                break;
            case CHANNEL_CH_16:
                [viewController sendData:CMD_CH4_GATE_RELEASE commandType:DSP_WRITE dspId:DSP_4 value:knobGateRelease.value];
                break;
            case CHANNEL_CH_17:
                [viewController sendData:CMD_CH17_GATE_RELEASE commandType:DSP_WRITE dspId:DSP_6 value:knobGateRelease.value];
                break;
            case CHANNEL_CH_18:
                [viewController sendData:CMD_CH18_GATE_RELEASE commandType:DSP_WRITE dspId:DSP_6 value:knobGateRelease.value];
                break;
            case CHANNEL_MULTI_1:
                [viewController sendData:CMD_MULTI1_GATE_RELEASE commandType:DSP_WRITE dspId:DSP_7 value:knobGateRelease.value];
                break;
            case CHANNEL_MULTI_2:
                [viewController sendData:CMD_MULTI2_GATE_RELEASE commandType:DSP_WRITE dspId:DSP_7 value:knobGateRelease.value];
                break;
            case CHANNEL_MULTI_3:
                [viewController sendData:CMD_MULTI3_GATE_RELEASE commandType:DSP_WRITE dspId:DSP_7 value:knobGateRelease.value];
                break;
            case CHANNEL_MULTI_4:
                [viewController sendData:CMD_MULTI4_GATE_RELEASE commandType:DSP_WRITE dspId:DSP_7 value:knobGateRelease.value];
                break;
            case CHANNEL_MAIN:
                [viewController sendData:CMD_MAIN_LR_GATE_RELEASE commandType:DSP_WRITE dspId:DSP_6 value:knobGateRelease.value];
                break;
            default:
                break;
        }
    }
}

#pragma mark -
#pragma mark Expander view event handler methods

- (IBAction)knobExpanderThresholdDidChange:(id)sender
{
    lblExpanderThreshold.text = [NSString stringWithFormat:@"%d dB", (int)knobExpanderThreshold.value-THRESHOLD_MAX];
    cpvExpanderThreshold.progress = knobExpanderThreshold.value/THRESHOLD_MAX;
    dynExpThreshold[currentChannel] = knobExpanderThreshold.value-THRESHOLD_MAX;
    [self updateDynFrame];
    if (sender != nil) {
        switch (currentChannel) {
            case CHANNEL_CH_1:
                [viewController sendData:CMD_CH1_EXPAND_THRESHOLD commandType:DSP_WRITE dspId:DSP_1 value:knobExpanderThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_2:
                [viewController sendData:CMD_CH2_EXPAND_THRESHOLD commandType:DSP_WRITE dspId:DSP_1 value:knobExpanderThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_3:
                [viewController sendData:CMD_CH3_EXPAND_THRESHOLD commandType:DSP_WRITE dspId:DSP_1 value:knobExpanderThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_4:
                [viewController sendData:CMD_CH4_EXPAND_THRESHOLD commandType:DSP_WRITE dspId:DSP_1 value:knobExpanderThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_5:
                [viewController sendData:CMD_CH1_EXPAND_THRESHOLD commandType:DSP_WRITE dspId:DSP_2 value:knobExpanderThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_6:
                [viewController sendData:CMD_CH2_EXPAND_THRESHOLD commandType:DSP_WRITE dspId:DSP_2 value:knobExpanderThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_7:
                [viewController sendData:CMD_CH3_EXPAND_THRESHOLD commandType:DSP_WRITE dspId:DSP_2 value:knobExpanderThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_8:
                [viewController sendData:CMD_CH4_EXPAND_THRESHOLD commandType:DSP_WRITE dspId:DSP_2 value:knobExpanderThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_9:
                [viewController sendData:CMD_CH1_EXPAND_THRESHOLD commandType:DSP_WRITE dspId:DSP_3 value:knobExpanderThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_10:
                [viewController sendData:CMD_CH2_EXPAND_THRESHOLD commandType:DSP_WRITE dspId:DSP_3 value:knobExpanderThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_11:
                [viewController sendData:CMD_CH3_EXPAND_THRESHOLD commandType:DSP_WRITE dspId:DSP_3 value:knobExpanderThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_12:
                [viewController sendData:CMD_CH4_EXPAND_THRESHOLD commandType:DSP_WRITE dspId:DSP_3 value:knobExpanderThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_13:
                [viewController sendData:CMD_CH1_EXPAND_THRESHOLD commandType:DSP_WRITE dspId:DSP_4 value:knobExpanderThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_14:
                [viewController sendData:CMD_CH2_EXPAND_THRESHOLD commandType:DSP_WRITE dspId:DSP_4 value:knobExpanderThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_15:
                [viewController sendData:CMD_CH3_EXPAND_THRESHOLD commandType:DSP_WRITE dspId:DSP_4 value:knobExpanderThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_16:
                [viewController sendData:CMD_CH4_EXPAND_THRESHOLD commandType:DSP_WRITE dspId:DSP_4 value:knobExpanderThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_17:
                [viewController sendData:CMD_CH17_EXPAND_THRESHOLD commandType:DSP_WRITE dspId:DSP_6 value:knobExpanderThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_18:
                [viewController sendData:CMD_CH18_EXPAND_THRESHOLD commandType:DSP_WRITE dspId:DSP_6 value:knobExpanderThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_MULTI_1:
                [viewController sendData:CMD_MULTI1_EXPAND_THRESHOLD commandType:DSP_WRITE dspId:DSP_7 value:knobExpanderThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_MULTI_2:
                [viewController sendData:CMD_MULTI2_EXPAND_THRESHOLD commandType:DSP_WRITE dspId:DSP_7 value:knobExpanderThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_MULTI_3:
                [viewController sendData:CMD_MULTI3_EXPAND_THRESHOLD commandType:DSP_WRITE dspId:DSP_7 value:knobExpanderThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_MULTI_4:
                [viewController sendData:CMD_MULTI4_EXPAND_THRESHOLD commandType:DSP_WRITE dspId:DSP_7 value:knobExpanderThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_MAIN:
                [viewController sendData:CMD_MAIN_LR_EXPAND_THRESHOLD commandType:DSP_WRITE dspId:DSP_6 value:knobExpanderThreshold.value-THRESHOLD_MAX];
                break;
            default:
                break;
        }
    }
}

- (IBAction)knobExpanderRatioDidChange:(id)sender
{
    lblExpanderRatio.text = [Global getRatioString:knobExpanderRatio.value];
    cpvExpanderRatio.progress = knobExpanderRatio.value/RATIO_MAX;
    dynExpRatio[currentChannel] = [Global getRatioValue:knobExpanderRatio.value];
    [self updateDynFrame];
    if (sender != nil) {
        switch (currentChannel) {
            case CHANNEL_CH_1:
                [viewController sendData:CMD_CH1_EXPAND_RATIO commandType:DSP_WRITE dspId:DSP_1 value:knobExpanderRatio.value];
                break;
            case CHANNEL_CH_2:
                [viewController sendData:CMD_CH2_EXPAND_RATIO commandType:DSP_WRITE dspId:DSP_1 value:knobExpanderRatio.value];
                break;
            case CHANNEL_CH_3:
                [viewController sendData:CMD_CH3_EXPAND_RATIO commandType:DSP_WRITE dspId:DSP_1 value:knobExpanderRatio.value];
                break;
            case CHANNEL_CH_4:
                [viewController sendData:CMD_CH4_EXPAND_RATIO commandType:DSP_WRITE dspId:DSP_1 value:knobExpanderRatio.value];
                break;
            case CHANNEL_CH_5:
                [viewController sendData:CMD_CH1_EXPAND_RATIO commandType:DSP_WRITE dspId:DSP_2 value:knobExpanderRatio.value];
                break;
            case CHANNEL_CH_6:
                [viewController sendData:CMD_CH2_EXPAND_RATIO commandType:DSP_WRITE dspId:DSP_2 value:knobExpanderRatio.value];
                break;
            case CHANNEL_CH_7:
                [viewController sendData:CMD_CH3_EXPAND_RATIO commandType:DSP_WRITE dspId:DSP_2 value:knobExpanderRatio.value];
                break;
            case CHANNEL_CH_8:
                [viewController sendData:CMD_CH4_EXPAND_RATIO commandType:DSP_WRITE dspId:DSP_2 value:knobExpanderRatio.value];
                break;
            case CHANNEL_CH_9:
                [viewController sendData:CMD_CH1_EXPAND_RATIO commandType:DSP_WRITE dspId:DSP_3 value:knobExpanderRatio.value];
                break;
            case CHANNEL_CH_10:
                [viewController sendData:CMD_CH2_EXPAND_RATIO commandType:DSP_WRITE dspId:DSP_3 value:knobExpanderRatio.value];
                break;
            case CHANNEL_CH_11:
                [viewController sendData:CMD_CH3_EXPAND_RATIO commandType:DSP_WRITE dspId:DSP_3 value:knobExpanderRatio.value];
                break;
            case CHANNEL_CH_12:
                [viewController sendData:CMD_CH4_EXPAND_RATIO commandType:DSP_WRITE dspId:DSP_3 value:knobExpanderRatio.value];
                break;
            case CHANNEL_CH_13:
                [viewController sendData:CMD_CH1_EXPAND_RATIO commandType:DSP_WRITE dspId:DSP_4 value:knobExpanderRatio.value];
                break;
            case CHANNEL_CH_14:
                [viewController sendData:CMD_CH2_EXPAND_RATIO commandType:DSP_WRITE dspId:DSP_4 value:knobExpanderRatio.value];
                break;
            case CHANNEL_CH_15:
                [viewController sendData:CMD_CH3_EXPAND_RATIO commandType:DSP_WRITE dspId:DSP_4 value:knobExpanderRatio.value];
                break;
            case CHANNEL_CH_16:
                [viewController sendData:CMD_CH4_EXPAND_RATIO commandType:DSP_WRITE dspId:DSP_4 value:knobExpanderRatio.value];
                break;
            case CHANNEL_CH_17:
                [viewController sendData:CMD_CH17_EXPAND_RATIO commandType:DSP_WRITE dspId:DSP_6 value:knobExpanderRatio.value];
                break;
            case CHANNEL_CH_18:
                [viewController sendData:CMD_CH18_EXPAND_RATIO commandType:DSP_WRITE dspId:DSP_6 value:knobExpanderRatio.value];
                break;
            case CHANNEL_MULTI_1:
                [viewController sendData:CMD_MULTI1_EXPAND_RATIO commandType:DSP_WRITE dspId:DSP_7 value:knobExpanderRatio.value];
                break;
            case CHANNEL_MULTI_2:
                [viewController sendData:CMD_MULTI2_EXPAND_RATIO commandType:DSP_WRITE dspId:DSP_7 value:knobExpanderRatio.value];
                break;
            case CHANNEL_MULTI_3:
                [viewController sendData:CMD_MULTI3_EXPAND_RATIO commandType:DSP_WRITE dspId:DSP_7 value:knobExpanderRatio.value];
                break;
            case CHANNEL_MULTI_4:
                [viewController sendData:CMD_MULTI4_EXPAND_RATIO commandType:DSP_WRITE dspId:DSP_7 value:knobExpanderRatio.value];
                break;
            case CHANNEL_MAIN:
                [viewController sendData:CMD_MAIN_LR_EXPAND_RATIO commandType:DSP_WRITE dspId:DSP_6 value:knobExpanderRatio.value];
                break;
            default:
                break;
        }
    }
}

- (IBAction)knobExpanderAttackDidChange:(id)sender
{
    lblExpanderAttack.text = [Global getTimeString:knobExpanderAttack.value];
    cpvExpanderAttack.progress = knobExpanderAttack.value/ATTACK_RELEASE_MAX;
    if (sender != nil) {
        switch (currentChannel) {
            case CHANNEL_CH_1:
                [viewController sendData:CMD_CH1_EXPAND_ATTACK commandType:DSP_WRITE dspId:DSP_1 value:knobExpanderAttack.value];
                break;
            case CHANNEL_CH_2:
                [viewController sendData:CMD_CH2_EXPAND_ATTACK commandType:DSP_WRITE dspId:DSP_1 value:knobExpanderAttack.value];
                break;
            case CHANNEL_CH_3:
                [viewController sendData:CMD_CH3_EXPAND_ATTACK commandType:DSP_WRITE dspId:DSP_1 value:knobExpanderAttack.value];
                break;
            case CHANNEL_CH_4:
                [viewController sendData:CMD_CH4_EXPAND_ATTACK commandType:DSP_WRITE dspId:DSP_1 value:knobExpanderAttack.value];
                break;
            case CHANNEL_CH_5:
                [viewController sendData:CMD_CH1_EXPAND_ATTACK commandType:DSP_WRITE dspId:DSP_2 value:knobExpanderAttack.value];
                break;
            case CHANNEL_CH_6:
                [viewController sendData:CMD_CH2_EXPAND_ATTACK commandType:DSP_WRITE dspId:DSP_2 value:knobExpanderAttack.value];
                break;
            case CHANNEL_CH_7:
                [viewController sendData:CMD_CH3_EXPAND_ATTACK commandType:DSP_WRITE dspId:DSP_2 value:knobExpanderAttack.value];
                break;
            case CHANNEL_CH_8:
                [viewController sendData:CMD_CH4_EXPAND_ATTACK commandType:DSP_WRITE dspId:DSP_2 value:knobExpanderAttack.value];
                break;
            case CHANNEL_CH_9:
                [viewController sendData:CMD_CH1_EXPAND_ATTACK commandType:DSP_WRITE dspId:DSP_3 value:knobExpanderAttack.value];
                break;
            case CHANNEL_CH_10:
                [viewController sendData:CMD_CH2_EXPAND_ATTACK commandType:DSP_WRITE dspId:DSP_3 value:knobExpanderAttack.value];
                break;
            case CHANNEL_CH_11:
                [viewController sendData:CMD_CH3_EXPAND_ATTACK commandType:DSP_WRITE dspId:DSP_3 value:knobExpanderAttack.value];
                break;
            case CHANNEL_CH_12:
                [viewController sendData:CMD_CH4_EXPAND_ATTACK commandType:DSP_WRITE dspId:DSP_3 value:knobExpanderAttack.value];
                break;
            case CHANNEL_CH_13:
                [viewController sendData:CMD_CH1_EXPAND_ATTACK commandType:DSP_WRITE dspId:DSP_4 value:knobExpanderAttack.value];
                break;
            case CHANNEL_CH_14:
                [viewController sendData:CMD_CH2_EXPAND_ATTACK commandType:DSP_WRITE dspId:DSP_4 value:knobExpanderAttack.value];
                break;
            case CHANNEL_CH_15:
                [viewController sendData:CMD_CH3_EXPAND_ATTACK commandType:DSP_WRITE dspId:DSP_4 value:knobExpanderAttack.value];
                break;
            case CHANNEL_CH_16:
                [viewController sendData:CMD_CH4_EXPAND_ATTACK commandType:DSP_WRITE dspId:DSP_4 value:knobExpanderAttack.value];
                break;
            case CHANNEL_CH_17:
                [viewController sendData:CMD_CH17_EXPAND_ATTACK commandType:DSP_WRITE dspId:DSP_6 value:knobExpanderAttack.value];
                break;
            case CHANNEL_CH_18:
                [viewController sendData:CMD_CH18_EXPAND_ATTACK commandType:DSP_WRITE dspId:DSP_6 value:knobExpanderAttack.value];
                break;
            case CHANNEL_MULTI_1:
                [viewController sendData:CMD_MULTI1_EXPAND_ATTACK commandType:DSP_WRITE dspId:DSP_7 value:knobExpanderAttack.value];
                break;
            case CHANNEL_MULTI_2:
                [viewController sendData:CMD_MULTI2_EXPAND_ATTACK commandType:DSP_WRITE dspId:DSP_7 value:knobExpanderAttack.value];
                break;
            case CHANNEL_MULTI_3:
                [viewController sendData:CMD_MULTI3_EXPAND_ATTACK commandType:DSP_WRITE dspId:DSP_7 value:knobExpanderAttack.value];
                break;
            case CHANNEL_MULTI_4:
                [viewController sendData:CMD_MULTI4_EXPAND_ATTACK commandType:DSP_WRITE dspId:DSP_7 value:knobExpanderAttack.value];
                break;
            case CHANNEL_MAIN:
                [viewController sendData:CMD_MAIN_LR_EXPAND_ATTACK commandType:DSP_WRITE dspId:DSP_6 value:knobExpanderAttack.value];
                break;
            default:
                break;
        }
    }
}

- (IBAction)knobExpanderReleaseDidChange:(id)sender
{
    lblExpanderRelease.text = [Global getTimeString:knobExpanderRelease.value];
    cpvExpanderRelease.progress = knobExpanderRelease.value/ATTACK_RELEASE_MAX;
    if (sender != nil) {
        switch (currentChannel) {
            case CHANNEL_CH_1:
                [viewController sendData:CMD_CH1_EXPAND_RELEASE commandType:DSP_WRITE dspId:DSP_1 value:knobExpanderRelease.value];
                break;
            case CHANNEL_CH_2:
                [viewController sendData:CMD_CH2_EXPAND_RELEASE commandType:DSP_WRITE dspId:DSP_1 value:knobExpanderRelease.value];
                break;
            case CHANNEL_CH_3:
                [viewController sendData:CMD_CH3_EXPAND_RELEASE commandType:DSP_WRITE dspId:DSP_1 value:knobExpanderRelease.value];
                break;
            case CHANNEL_CH_4:
                [viewController sendData:CMD_CH4_EXPAND_RELEASE commandType:DSP_WRITE dspId:DSP_1 value:knobExpanderRelease.value];
                break;
            case CHANNEL_CH_5:
                [viewController sendData:CMD_CH1_EXPAND_RELEASE commandType:DSP_WRITE dspId:DSP_2 value:knobExpanderRelease.value];
                break;
            case CHANNEL_CH_6:
                [viewController sendData:CMD_CH2_EXPAND_RELEASE commandType:DSP_WRITE dspId:DSP_2 value:knobExpanderRelease.value];
                break;
            case CHANNEL_CH_7:
                [viewController sendData:CMD_CH3_EXPAND_RELEASE commandType:DSP_WRITE dspId:DSP_2 value:knobExpanderRelease.value];
                break;
            case CHANNEL_CH_8:
                [viewController sendData:CMD_CH4_EXPAND_RELEASE commandType:DSP_WRITE dspId:DSP_2 value:knobExpanderRelease.value];
                break;
            case CHANNEL_CH_9:
                [viewController sendData:CMD_CH1_EXPAND_RELEASE commandType:DSP_WRITE dspId:DSP_3 value:knobExpanderRelease.value];
                break;
            case CHANNEL_CH_10:
                [viewController sendData:CMD_CH2_EXPAND_RELEASE commandType:DSP_WRITE dspId:DSP_3 value:knobExpanderRelease.value];
                break;
            case CHANNEL_CH_11:
                [viewController sendData:CMD_CH3_EXPAND_RELEASE commandType:DSP_WRITE dspId:DSP_3 value:knobExpanderRelease.value];
                break;
            case CHANNEL_CH_12:
                [viewController sendData:CMD_CH4_EXPAND_RELEASE commandType:DSP_WRITE dspId:DSP_3 value:knobExpanderRelease.value];
                break;
            case CHANNEL_CH_13:
                [viewController sendData:CMD_CH1_EXPAND_RELEASE commandType:DSP_WRITE dspId:DSP_4 value:knobExpanderRelease.value];
                break;
            case CHANNEL_CH_14:
                [viewController sendData:CMD_CH2_EXPAND_RELEASE commandType:DSP_WRITE dspId:DSP_4 value:knobExpanderRelease.value];
                break;
            case CHANNEL_CH_15:
                [viewController sendData:CMD_CH3_EXPAND_RELEASE commandType:DSP_WRITE dspId:DSP_4 value:knobExpanderRelease.value];
                break;
            case CHANNEL_CH_16:
                [viewController sendData:CMD_CH4_EXPAND_RELEASE commandType:DSP_WRITE dspId:DSP_4 value:knobExpanderRelease.value];
                break;
            case CHANNEL_CH_17:
                [viewController sendData:CMD_CH17_EXPAND_RELEASE commandType:DSP_WRITE dspId:DSP_6 value:knobExpanderRelease.value];
                break;
            case CHANNEL_CH_18:
                [viewController sendData:CMD_CH18_EXPAND_RELEASE commandType:DSP_WRITE dspId:DSP_6 value:knobExpanderRelease.value];
                break;
            case CHANNEL_MULTI_1:
                [viewController sendData:CMD_MULTI1_EXPAND_RELEASE commandType:DSP_WRITE dspId:DSP_7 value:knobExpanderRelease.value];
                break;
            case CHANNEL_MULTI_2:
                [viewController sendData:CMD_MULTI2_EXPAND_RELEASE commandType:DSP_WRITE dspId:DSP_7 value:knobExpanderRelease.value];
                break;
            case CHANNEL_MULTI_3:
                [viewController sendData:CMD_MULTI3_EXPAND_RELEASE commandType:DSP_WRITE dspId:DSP_7 value:knobExpanderRelease.value];
                break;
            case CHANNEL_MULTI_4:
                [viewController sendData:CMD_MULTI4_EXPAND_RELEASE commandType:DSP_WRITE dspId:DSP_7 value:knobExpanderRelease.value];
                break;
            case CHANNEL_MAIN:
                [viewController sendData:CMD_MAIN_LR_EXPAND_RELEASE commandType:DSP_WRITE dspId:DSP_6 value:knobExpanderRelease.value];
                break;
            default:
                break;
        }
    }
}

#pragma mark -
#pragma mark Comp view event handler methods

- (IBAction)knobCompThresholdDidChange:(id)sender
{
    lblCompThreshold.text = [NSString stringWithFormat:@"%d dB", (int)knobCompThreshold.value-THRESHOLD_MAX];
    cpvCompThreshold.progress = knobCompThreshold.value/THRESHOLD_MAX;
    dynCompThreshold[currentChannel] = knobCompThreshold.value-THRESHOLD_MAX;
    [self updateDynFrame];
    if (sender != nil) {
        switch (currentChannel) {
            case CHANNEL_CH_1:
                [viewController sendData:CMD_CH1_COMPRESSOR_THRESHOLD commandType:DSP_WRITE dspId:DSP_1 value:knobCompThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_2:
                [viewController sendData:CMD_CH2_COMPRESSOR_THRESHOLD commandType:DSP_WRITE dspId:DSP_1 value:knobCompThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_3:
                [viewController sendData:CMD_CH3_COMPRESSOR_THRESHOLD commandType:DSP_WRITE dspId:DSP_1 value:knobCompThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_4:
                [viewController sendData:CMD_CH4_COMPRESSOR_THRESHOLD commandType:DSP_WRITE dspId:DSP_1 value:knobCompThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_5:
                [viewController sendData:CMD_CH1_COMPRESSOR_THRESHOLD commandType:DSP_WRITE dspId:DSP_2 value:knobCompThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_6:
                [viewController sendData:CMD_CH2_COMPRESSOR_THRESHOLD commandType:DSP_WRITE dspId:DSP_2 value:knobCompThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_7:
                [viewController sendData:CMD_CH3_COMPRESSOR_THRESHOLD commandType:DSP_WRITE dspId:DSP_2 value:knobCompThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_8:
                [viewController sendData:CMD_CH4_COMPRESSOR_THRESHOLD commandType:DSP_WRITE dspId:DSP_2 value:knobCompThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_9:
                [viewController sendData:CMD_CH1_COMPRESSOR_THRESHOLD commandType:DSP_WRITE dspId:DSP_3 value:knobCompThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_10:
                [viewController sendData:CMD_CH2_COMPRESSOR_THRESHOLD commandType:DSP_WRITE dspId:DSP_3 value:knobCompThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_11:
                [viewController sendData:CMD_CH3_COMPRESSOR_THRESHOLD commandType:DSP_WRITE dspId:DSP_3 value:knobCompThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_12:
                [viewController sendData:CMD_CH4_COMPRESSOR_THRESHOLD commandType:DSP_WRITE dspId:DSP_3 value:knobCompThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_13:
                [viewController sendData:CMD_CH1_COMPRESSOR_THRESHOLD commandType:DSP_WRITE dspId:DSP_4 value:knobCompThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_14:
                [viewController sendData:CMD_CH2_COMPRESSOR_THRESHOLD commandType:DSP_WRITE dspId:DSP_4 value:knobCompThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_15:
                [viewController sendData:CMD_CH3_COMPRESSOR_THRESHOLD commandType:DSP_WRITE dspId:DSP_4 value:knobCompThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_16:
                [viewController sendData:CMD_CH4_COMPRESSOR_THRESHOLD commandType:DSP_WRITE dspId:DSP_4 value:knobCompThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_17:
                [viewController sendData:CMD_CH17_COMPRESSOR_THRESHOLD commandType:DSP_WRITE dspId:DSP_6 value:knobCompThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_18:
                [viewController sendData:CMD_CH18_COMPRESSOR_THRESHOLD commandType:DSP_WRITE dspId:DSP_6 value:knobCompThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_MULTI_1:
                [viewController sendData:CMD_MULTI1_COMPRESSOR_THRESHOLD commandType:DSP_WRITE dspId:DSP_7 value:knobCompThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_MULTI_2:
                [viewController sendData:CMD_MULTI2_COMPRESSOR_THRESHOLD commandType:DSP_WRITE dspId:DSP_7 value:knobCompThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_MULTI_3:
                [viewController sendData:CMD_MULTI3_COMPRESSOR_THRESHOLD commandType:DSP_WRITE dspId:DSP_7 value:knobCompThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_MULTI_4:
                [viewController sendData:CMD_MULTI4_COMPRESSOR_THRESHOLD commandType:DSP_WRITE dspId:DSP_7 value:knobCompThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_MAIN:
                [viewController sendData:CMD_MAIN_LR_COMPRESSOR_THRESHOLD commandType:DSP_WRITE dspId:DSP_6 value:knobCompThreshold.value-THRESHOLD_MAX];
                break;
            default:
                break;
        }
    }
}

- (IBAction)knobCompRatioDidChange:(id)sender
{
    lblCompRatio.text = [Global getRatioString:knobCompRatio.value];
    cpvCompRatio.progress = knobCompRatio.value/RATIO_MAX;
    dynCompRatio[currentChannel] = [Global getRatioCompValue:knobCompRatio.value];
    [self updateDynFrame];
    if (sender != nil) {
        switch (currentChannel) {
            case CHANNEL_CH_1:
                [viewController sendData:CMD_CH1_COMPRESSOR_RATIO commandType:DSP_WRITE dspId:DSP_1 value:knobCompRatio.value];
                break;
            case CHANNEL_CH_2:
                [viewController sendData:CMD_CH2_COMPRESSOR_RATIO commandType:DSP_WRITE dspId:DSP_1 value:knobCompRatio.value];
                break;
            case CHANNEL_CH_3:
                [viewController sendData:CMD_CH3_COMPRESSOR_RATIO commandType:DSP_WRITE dspId:DSP_1 value:knobCompRatio.value];
                break;
            case CHANNEL_CH_4:
                [viewController sendData:CMD_CH4_COMPRESSOR_RATIO commandType:DSP_WRITE dspId:DSP_1 value:knobCompRatio.value];
                break;
            case CHANNEL_CH_5:
                [viewController sendData:CMD_CH1_COMPRESSOR_RATIO commandType:DSP_WRITE dspId:DSP_2 value:knobCompRatio.value];
                break;
            case CHANNEL_CH_6:
                [viewController sendData:CMD_CH2_COMPRESSOR_RATIO commandType:DSP_WRITE dspId:DSP_2 value:knobCompRatio.value];
                break;
            case CHANNEL_CH_7:
                [viewController sendData:CMD_CH3_COMPRESSOR_RATIO commandType:DSP_WRITE dspId:DSP_2 value:knobCompRatio.value];
                break;
            case CHANNEL_CH_8:
                [viewController sendData:CMD_CH4_COMPRESSOR_RATIO commandType:DSP_WRITE dspId:DSP_2 value:knobCompRatio.value];
                break;
            case CHANNEL_CH_9:
                [viewController sendData:CMD_CH1_COMPRESSOR_RATIO commandType:DSP_WRITE dspId:DSP_3 value:knobCompRatio.value];
                break;
            case CHANNEL_CH_10:
                [viewController sendData:CMD_CH2_COMPRESSOR_RATIO commandType:DSP_WRITE dspId:DSP_3 value:knobCompRatio.value];
                break;
            case CHANNEL_CH_11:
                [viewController sendData:CMD_CH3_COMPRESSOR_RATIO commandType:DSP_WRITE dspId:DSP_3 value:knobCompRatio.value];
                break;
            case CHANNEL_CH_12:
                [viewController sendData:CMD_CH4_COMPRESSOR_RATIO commandType:DSP_WRITE dspId:DSP_3 value:knobCompRatio.value];
                break;
            case CHANNEL_CH_13:
                [viewController sendData:CMD_CH1_COMPRESSOR_RATIO commandType:DSP_WRITE dspId:DSP_4 value:knobCompRatio.value];
                break;
            case CHANNEL_CH_14:
                [viewController sendData:CMD_CH2_COMPRESSOR_RATIO commandType:DSP_WRITE dspId:DSP_4 value:knobCompRatio.value];
                break;
            case CHANNEL_CH_15:
                [viewController sendData:CMD_CH3_COMPRESSOR_RATIO commandType:DSP_WRITE dspId:DSP_4 value:knobCompRatio.value];
                break;
            case CHANNEL_CH_16:
                [viewController sendData:CMD_CH4_COMPRESSOR_RATIO commandType:DSP_WRITE dspId:DSP_4 value:knobCompRatio.value];
                break;
            case CHANNEL_CH_17:
                [viewController sendData:CMD_CH17_COMPRESSOR_RATIO commandType:DSP_WRITE dspId:DSP_6 value:knobCompRatio.value];
                break;
            case CHANNEL_CH_18:
                [viewController sendData:CMD_CH18_COMPRESSOR_RATIO commandType:DSP_WRITE dspId:DSP_6 value:knobCompRatio.value];
                break;
            case CHANNEL_MULTI_1:
                [viewController sendData:CMD_MULTI1_COMPRESSOR_RATIO commandType:DSP_WRITE dspId:DSP_7 value:knobCompRatio.value];
                break;
            case CHANNEL_MULTI_2:
                [viewController sendData:CMD_MULTI2_COMPRESSOR_RATIO commandType:DSP_WRITE dspId:DSP_7 value:knobCompRatio.value];
                break;
            case CHANNEL_MULTI_3:
                [viewController sendData:CMD_MULTI3_COMPRESSOR_RATIO commandType:DSP_WRITE dspId:DSP_7 value:knobCompRatio.value];
                break;
            case CHANNEL_MULTI_4:
                [viewController sendData:CMD_MULTI4_COMPRESSOR_RATIO commandType:DSP_WRITE dspId:DSP_7 value:knobCompRatio.value];
                break;
            case CHANNEL_MAIN:
                [viewController sendData:CMD_MAIN_LR_COMPRESSOR_RATIO commandType:DSP_WRITE dspId:DSP_6 value:knobCompRatio.value];
                break;
            default:
                break;
        }
    }
}

- (IBAction)knobCompOutputGainDidChange:(id)sender
{
    lblCompOutputGain.text = [NSString stringWithFormat:@"%.1f dB", knobCompOutputGain.value/2.0];
    cpvCompOutputGain.progress = knobCompOutputGain.value/OUT_GAIN_MAX;
    if (sender != nil) {
        switch (currentChannel) {
            case CHANNEL_CH_1:
                [viewController sendData:CMD_CH1_COMPRESSOR_OUT_GAIN commandType:DSP_WRITE dspId:DSP_1 value:knobCompOutputGain.value];
                break;
            case CHANNEL_CH_2:
                [viewController sendData:CMD_CH2_COMPRESSOR_OUT_GAIN commandType:DSP_WRITE dspId:DSP_1 value:knobCompOutputGain.value];
                break;
            case CHANNEL_CH_3:
                [viewController sendData:CMD_CH3_COMPRESSOR_OUT_GAIN commandType:DSP_WRITE dspId:DSP_1 value:knobCompOutputGain.value];
                break;
            case CHANNEL_CH_4:
                [viewController sendData:CMD_CH4_COMPRESSOR_OUT_GAIN commandType:DSP_WRITE dspId:DSP_1 value:knobCompOutputGain.value];
                break;
            case CHANNEL_CH_5:
                [viewController sendData:CMD_CH1_COMPRESSOR_OUT_GAIN commandType:DSP_WRITE dspId:DSP_2 value:knobCompOutputGain.value];
                break;
            case CHANNEL_CH_6:
                [viewController sendData:CMD_CH2_COMPRESSOR_OUT_GAIN commandType:DSP_WRITE dspId:DSP_2 value:knobCompOutputGain.value];
                break;
            case CHANNEL_CH_7:
                [viewController sendData:CMD_CH3_COMPRESSOR_OUT_GAIN commandType:DSP_WRITE dspId:DSP_2 value:knobCompOutputGain.value];
                break;
            case CHANNEL_CH_8:
                [viewController sendData:CMD_CH4_COMPRESSOR_OUT_GAIN commandType:DSP_WRITE dspId:DSP_2 value:knobCompOutputGain.value];
                break;
            case CHANNEL_CH_9:
                [viewController sendData:CMD_CH1_COMPRESSOR_OUT_GAIN commandType:DSP_WRITE dspId:DSP_3 value:knobCompOutputGain.value];
                break;
            case CHANNEL_CH_10:
                [viewController sendData:CMD_CH2_COMPRESSOR_OUT_GAIN commandType:DSP_WRITE dspId:DSP_3 value:knobCompOutputGain.value];
                break;
            case CHANNEL_CH_11:
                [viewController sendData:CMD_CH3_COMPRESSOR_OUT_GAIN commandType:DSP_WRITE dspId:DSP_3 value:knobCompOutputGain.value];
                break;
            case CHANNEL_CH_12:
                [viewController sendData:CMD_CH4_COMPRESSOR_OUT_GAIN commandType:DSP_WRITE dspId:DSP_3 value:knobCompOutputGain.value];
                break;
            case CHANNEL_CH_13:
                [viewController sendData:CMD_CH1_COMPRESSOR_OUT_GAIN commandType:DSP_WRITE dspId:DSP_4 value:knobCompOutputGain.value];
                break;
            case CHANNEL_CH_14:
                [viewController sendData:CMD_CH2_COMPRESSOR_OUT_GAIN commandType:DSP_WRITE dspId:DSP_4 value:knobCompOutputGain.value];
                break;
            case CHANNEL_CH_15:
                [viewController sendData:CMD_CH3_COMPRESSOR_OUT_GAIN commandType:DSP_WRITE dspId:DSP_4 value:knobCompOutputGain.value];
                break;
            case CHANNEL_CH_16:
                [viewController sendData:CMD_CH4_COMPRESSOR_OUT_GAIN commandType:DSP_WRITE dspId:DSP_4 value:knobCompOutputGain.value];
                break;
            case CHANNEL_CH_17:
                [viewController sendData:CMD_CH17_COMPRESSOR_OUT_GAIN commandType:DSP_WRITE dspId:DSP_6 value:knobCompOutputGain.value];
                break;
            case CHANNEL_CH_18:
                [viewController sendData:CMD_CH18_COMPRESSOR_OUT_GAIN commandType:DSP_WRITE dspId:DSP_6 value:knobCompOutputGain.value];
                break;
            case CHANNEL_MULTI_1:
                [viewController sendData:CMD_MULTI1_COMPRESSOR_OUT_GAIN commandType:DSP_WRITE dspId:DSP_7 value:knobCompOutputGain.value];
                break;
            case CHANNEL_MULTI_2:
                [viewController sendData:CMD_MULTI2_COMPRESSOR_OUT_GAIN commandType:DSP_WRITE dspId:DSP_7 value:knobCompOutputGain.value];
                break;
            case CHANNEL_MULTI_3:
                [viewController sendData:CMD_MULTI3_COMPRESSOR_OUT_GAIN commandType:DSP_WRITE dspId:DSP_7 value:knobCompOutputGain.value];
                break;
            case CHANNEL_MULTI_4:
                [viewController sendData:CMD_MULTI4_COMPRESSOR_OUT_GAIN commandType:DSP_WRITE dspId:DSP_7 value:knobCompOutputGain.value];
                break;
            case CHANNEL_MAIN:
                [viewController sendData:CMD_MAIN_LR_COMPRESSOR_OUT_GAIN commandType:DSP_WRITE dspId:DSP_6 value:knobCompOutputGain.value];
                break;
            default:
                break;
        }
    }
}

- (IBAction)knobCompAttackDidChange:(id)sender
{
    lblCompAttack.text = [Global getTimeString:knobCompAttack.value];
    cpvCompAttack.progress = knobCompAttack.value/ATTACK_RELEASE_MAX;
    if (sender != nil) {
        switch (currentChannel) {
            case CHANNEL_CH_1:
                [viewController sendData:CMD_CH1_COMPRESSOR_ATTACK commandType:DSP_WRITE dspId:DSP_1 value:knobCompAttack.value];
                break;
            case CHANNEL_CH_2:
                [viewController sendData:CMD_CH2_COMPRESSOR_ATTACK commandType:DSP_WRITE dspId:DSP_1 value:knobCompAttack.value];
                break;
            case CHANNEL_CH_3:
                [viewController sendData:CMD_CH3_COMPRESSOR_ATTACK commandType:DSP_WRITE dspId:DSP_1 value:knobCompAttack.value];
                break;
            case CHANNEL_CH_4:
                [viewController sendData:CMD_CH4_COMPRESSOR_ATTACK commandType:DSP_WRITE dspId:DSP_1 value:knobCompAttack.value];
                break;
            case CHANNEL_CH_5:
                [viewController sendData:CMD_CH1_COMPRESSOR_ATTACK commandType:DSP_WRITE dspId:DSP_2 value:knobCompAttack.value];
                break;
            case CHANNEL_CH_6:
                [viewController sendData:CMD_CH2_COMPRESSOR_ATTACK commandType:DSP_WRITE dspId:DSP_2 value:knobCompAttack.value];
                break;
            case CHANNEL_CH_7:
                [viewController sendData:CMD_CH3_COMPRESSOR_ATTACK commandType:DSP_WRITE dspId:DSP_2 value:knobCompAttack.value];
                break;
            case CHANNEL_CH_8:
                [viewController sendData:CMD_CH4_COMPRESSOR_ATTACK commandType:DSP_WRITE dspId:DSP_2 value:knobCompAttack.value];
                break;
            case CHANNEL_CH_9:
                [viewController sendData:CMD_CH1_COMPRESSOR_ATTACK commandType:DSP_WRITE dspId:DSP_3 value:knobCompAttack.value];
                break;
            case CHANNEL_CH_10:
                [viewController sendData:CMD_CH2_COMPRESSOR_ATTACK commandType:DSP_WRITE dspId:DSP_3 value:knobCompAttack.value];
                break;
            case CHANNEL_CH_11:
                [viewController sendData:CMD_CH3_COMPRESSOR_ATTACK commandType:DSP_WRITE dspId:DSP_3 value:knobCompAttack.value];
                break;
            case CHANNEL_CH_12:
                [viewController sendData:CMD_CH4_COMPRESSOR_ATTACK commandType:DSP_WRITE dspId:DSP_3 value:knobCompAttack.value];
                break;
            case CHANNEL_CH_13:
                [viewController sendData:CMD_CH1_COMPRESSOR_ATTACK commandType:DSP_WRITE dspId:DSP_4 value:knobCompAttack.value];
                break;
            case CHANNEL_CH_14:
                [viewController sendData:CMD_CH2_COMPRESSOR_ATTACK commandType:DSP_WRITE dspId:DSP_4 value:knobCompAttack.value];
                break;
            case CHANNEL_CH_15:
                [viewController sendData:CMD_CH3_COMPRESSOR_ATTACK commandType:DSP_WRITE dspId:DSP_4 value:knobCompAttack.value];
                break;
            case CHANNEL_CH_16:
                [viewController sendData:CMD_CH4_COMPRESSOR_ATTACK commandType:DSP_WRITE dspId:DSP_4 value:knobCompAttack.value];
                break;
            case CHANNEL_CH_17:
                [viewController sendData:CMD_CH17_COMPRESSOR_ATTACK commandType:DSP_WRITE dspId:DSP_6 value:knobCompAttack.value];
                break;
            case CHANNEL_CH_18:
                [viewController sendData:CMD_CH18_COMPRESSOR_ATTACK commandType:DSP_WRITE dspId:DSP_6 value:knobCompAttack.value];
                break;
            case CHANNEL_MULTI_1:
                [viewController sendData:CMD_MULTI1_COMPRESSOR_ATTACK commandType:DSP_WRITE dspId:DSP_7 value:knobCompAttack.value];
                break;
            case CHANNEL_MULTI_2:
                [viewController sendData:CMD_MULTI2_COMPRESSOR_ATTACK commandType:DSP_WRITE dspId:DSP_7 value:knobCompAttack.value];
                break;
            case CHANNEL_MULTI_3:
                [viewController sendData:CMD_MULTI3_COMPRESSOR_ATTACK commandType:DSP_WRITE dspId:DSP_7 value:knobCompAttack.value];
                break;
            case CHANNEL_MULTI_4:
                [viewController sendData:CMD_MULTI4_COMPRESSOR_ATTACK commandType:DSP_WRITE dspId:DSP_7 value:knobCompAttack.value];
                break;
            case CHANNEL_MAIN:
                [viewController sendData:CMD_MAIN_LR_COMPRESSOR_ATTACK commandType:DSP_WRITE dspId:DSP_6 value:knobCompAttack.value];
                break;
            default:
                break;
        }
    }
}

- (IBAction)knobCompReleaseDidChange:(id)sender
{
    lblCompRelease.text = [Global getTimeString:knobCompRelease.value];
    cpvCompRelease.progress = knobCompRelease.value/ATTACK_RELEASE_MAX;
    if (sender != nil) {
        switch (currentChannel) {
            case CHANNEL_CH_1:
                [viewController sendData:CMD_CH1_COMPRESSOR_RELEASE commandType:DSP_WRITE dspId:DSP_1 value:knobCompRelease.value];
                break;
            case CHANNEL_CH_2:
                [viewController sendData:CMD_CH2_COMPRESSOR_RELEASE commandType:DSP_WRITE dspId:DSP_1 value:knobCompRelease.value];
                break;
            case CHANNEL_CH_3:
                [viewController sendData:CMD_CH3_COMPRESSOR_RELEASE commandType:DSP_WRITE dspId:DSP_1 value:knobCompRelease.value];
                break;
            case CHANNEL_CH_4:
                [viewController sendData:CMD_CH4_COMPRESSOR_RELEASE commandType:DSP_WRITE dspId:DSP_1 value:knobCompRelease.value];
                break;
            case CHANNEL_CH_5:
                [viewController sendData:CMD_CH1_COMPRESSOR_RELEASE commandType:DSP_WRITE dspId:DSP_2 value:knobCompRelease.value];
                break;
            case CHANNEL_CH_6:
                [viewController sendData:CMD_CH2_COMPRESSOR_RELEASE commandType:DSP_WRITE dspId:DSP_2 value:knobCompRelease.value];
                break;
            case CHANNEL_CH_7:
                [viewController sendData:CMD_CH3_COMPRESSOR_RELEASE commandType:DSP_WRITE dspId:DSP_2 value:knobCompRelease.value];
                break;
            case CHANNEL_CH_8:
                [viewController sendData:CMD_CH4_COMPRESSOR_RELEASE commandType:DSP_WRITE dspId:DSP_2 value:knobCompRelease.value];
                break;
            case CHANNEL_CH_9:
                [viewController sendData:CMD_CH1_COMPRESSOR_RELEASE commandType:DSP_WRITE dspId:DSP_3 value:knobCompRelease.value];
                break;
            case CHANNEL_CH_10:
                [viewController sendData:CMD_CH2_COMPRESSOR_RELEASE commandType:DSP_WRITE dspId:DSP_3 value:knobCompRelease.value];
                break;
            case CHANNEL_CH_11:
                [viewController sendData:CMD_CH3_COMPRESSOR_RELEASE commandType:DSP_WRITE dspId:DSP_3 value:knobCompRelease.value];
                break;
            case CHANNEL_CH_12:
                [viewController sendData:CMD_CH4_COMPRESSOR_RELEASE commandType:DSP_WRITE dspId:DSP_3 value:knobCompRelease.value];
                break;
            case CHANNEL_CH_13:
                [viewController sendData:CMD_CH1_COMPRESSOR_RELEASE commandType:DSP_WRITE dspId:DSP_4 value:knobCompRelease.value];
                break;
            case CHANNEL_CH_14:
                [viewController sendData:CMD_CH2_COMPRESSOR_RELEASE commandType:DSP_WRITE dspId:DSP_4 value:knobCompRelease.value];
                break;
            case CHANNEL_CH_15:
                [viewController sendData:CMD_CH3_COMPRESSOR_RELEASE commandType:DSP_WRITE dspId:DSP_4 value:knobCompRelease.value];
                break;
            case CHANNEL_CH_16:
                [viewController sendData:CMD_CH4_COMPRESSOR_RELEASE commandType:DSP_WRITE dspId:DSP_4 value:knobCompRelease.value];
                break;
            case CHANNEL_CH_17:
                [viewController sendData:CMD_CH17_COMPRESSOR_RELEASE commandType:DSP_WRITE dspId:DSP_6 value:knobCompRelease.value];
                break;
            case CHANNEL_CH_18:
                [viewController sendData:CMD_CH18_COMPRESSOR_RELEASE commandType:DSP_WRITE dspId:DSP_6 value:knobCompRelease.value];
                break;
            case CHANNEL_MULTI_1:
                [viewController sendData:CMD_MULTI1_COMPRESSOR_RELEASE commandType:DSP_WRITE dspId:DSP_7 value:knobCompRelease.value];
                break;
            case CHANNEL_MULTI_2:
                [viewController sendData:CMD_MULTI2_COMPRESSOR_RELEASE commandType:DSP_WRITE dspId:DSP_7 value:knobCompRelease.value];
                break;
            case CHANNEL_MULTI_3:
                [viewController sendData:CMD_MULTI3_COMPRESSOR_RELEASE commandType:DSP_WRITE dspId:DSP_7 value:knobCompRelease.value];
                break;
            case CHANNEL_MULTI_4:
                [viewController sendData:CMD_MULTI4_COMPRESSOR_RELEASE commandType:DSP_WRITE dspId:DSP_7 value:knobCompRelease.value];
                break;
            case CHANNEL_MAIN:
                [viewController sendData:CMD_MAIN_LR_COMPRESSOR_RELEASE commandType:DSP_WRITE dspId:DSP_6 value:knobCompRelease.value];
                break;
            default:
                break;
        }
    }
}

#pragma mark -
#pragma mark Limiter view event handler methods

- (IBAction)knobLimiterThresholdDidChange:(id)sender
{
    lblLimiterThreshold.text = [NSString stringWithFormat:@"%d dB", (int)knobLimiterThreshold.value-THRESHOLD_MAX];
    cpvLimiterThreshold.progress = knobLimiterThreshold.value/THRESHOLD_MAX;
    dynLimitThreshold[currentChannel] = knobLimiterThreshold.value-THRESHOLD_MAX;
    [self updateDynFrame];
    if (sender != nil) {
        switch (currentChannel) {
            case CHANNEL_CH_1:
                [viewController sendData:CMD_CH1_LIMITER_THRESHOLD commandType:DSP_WRITE dspId:DSP_1 value:knobLimiterThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_2:
                [viewController sendData:CMD_CH2_LIMITER_THRESHOLD commandType:DSP_WRITE dspId:DSP_1 value:knobLimiterThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_3:
                [viewController sendData:CMD_CH3_LIMITER_THRESHOLD commandType:DSP_WRITE dspId:DSP_1 value:knobLimiterThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_4:
                [viewController sendData:CMD_CH4_LIMITER_THRESHOLD commandType:DSP_WRITE dspId:DSP_1 value:knobLimiterThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_5:
                [viewController sendData:CMD_CH1_LIMITER_THRESHOLD commandType:DSP_WRITE dspId:DSP_2 value:knobLimiterThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_6:
                [viewController sendData:CMD_CH2_LIMITER_THRESHOLD commandType:DSP_WRITE dspId:DSP_2 value:knobLimiterThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_7:
                [viewController sendData:CMD_CH3_LIMITER_THRESHOLD commandType:DSP_WRITE dspId:DSP_2 value:knobLimiterThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_8:
                [viewController sendData:CMD_CH4_LIMITER_THRESHOLD commandType:DSP_WRITE dspId:DSP_2 value:knobLimiterThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_9:
                [viewController sendData:CMD_CH1_LIMITER_THRESHOLD commandType:DSP_WRITE dspId:DSP_3 value:knobLimiterThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_10:
                [viewController sendData:CMD_CH2_LIMITER_THRESHOLD commandType:DSP_WRITE dspId:DSP_3 value:knobLimiterThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_11:
                [viewController sendData:CMD_CH3_LIMITER_THRESHOLD commandType:DSP_WRITE dspId:DSP_3 value:knobLimiterThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_12:
                [viewController sendData:CMD_CH4_LIMITER_THRESHOLD commandType:DSP_WRITE dspId:DSP_3 value:knobLimiterThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_13:
                [viewController sendData:CMD_CH1_LIMITER_THRESHOLD commandType:DSP_WRITE dspId:DSP_4 value:knobLimiterThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_14:
                [viewController sendData:CMD_CH2_LIMITER_THRESHOLD commandType:DSP_WRITE dspId:DSP_4 value:knobLimiterThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_15:
                [viewController sendData:CMD_CH3_LIMITER_THRESHOLD commandType:DSP_WRITE dspId:DSP_4 value:knobLimiterThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_16:
                [viewController sendData:CMD_CH4_LIMITER_THRESHOLD commandType:DSP_WRITE dspId:DSP_4 value:knobLimiterThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_17:
                [viewController sendData:CMD_CH17_LIMITER_THRESHOLD commandType:DSP_WRITE dspId:DSP_6 value:knobLimiterThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_CH_18:
                [viewController sendData:CMD_CH18_LIMITER_THRESHOLD commandType:DSP_WRITE dspId:DSP_6 value:knobLimiterThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_MULTI_1:
                [viewController sendData:CMD_MULTI1_LIMITER_THRESHOLD commandType:DSP_WRITE dspId:DSP_7 value:knobLimiterThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_MULTI_2:
                [viewController sendData:CMD_MULTI2_LIMITER_THRESHOLD commandType:DSP_WRITE dspId:DSP_7 value:knobLimiterThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_MULTI_3:
                [viewController sendData:CMD_MULTI3_LIMITER_THRESHOLD commandType:DSP_WRITE dspId:DSP_7 value:knobLimiterThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_MULTI_4:
                [viewController sendData:CMD_MULTI4_LIMITER_THRESHOLD commandType:DSP_WRITE dspId:DSP_7 value:knobLimiterThreshold.value-THRESHOLD_MAX];
                break;
            case CHANNEL_MAIN:
                [viewController sendData:CMD_MAIN_LR_LIMITER_THRESHOLD commandType:DSP_WRITE dspId:DSP_6 value:knobLimiterThreshold.value-THRESHOLD_MAX];
                break;
            default:
                break;
        }
    }
}

- (IBAction)knobLimiterAttackDidChange:(id)sender
{
    lblLimiterAttack.text = [Global getTimeString:knobLimiterAttack.value];
    cpvLimiterAttack.progress = knobLimiterAttack.value/ATTACK_RELEASE_MAX;
    if (sender != nil) {
        switch (currentChannel) {
            case CHANNEL_CH_1:
                [viewController sendData:CMD_CH1_LIMITER_ATTACK commandType:DSP_WRITE dspId:DSP_1 value:knobLimiterAttack.value];
                break;
            case CHANNEL_CH_2:
                [viewController sendData:CMD_CH2_LIMITER_ATTACK commandType:DSP_WRITE dspId:DSP_1 value:knobLimiterAttack.value];
                break;
            case CHANNEL_CH_3:
                [viewController sendData:CMD_CH3_LIMITER_ATTACK commandType:DSP_WRITE dspId:DSP_1 value:knobLimiterAttack.value];
                break;
            case CHANNEL_CH_4:
                [viewController sendData:CMD_CH4_LIMITER_ATTACK commandType:DSP_WRITE dspId:DSP_1 value:knobLimiterAttack.value];
                break;
            case CHANNEL_CH_5:
                [viewController sendData:CMD_CH1_LIMITER_ATTACK commandType:DSP_WRITE dspId:DSP_2 value:knobLimiterAttack.value];
                break;
            case CHANNEL_CH_6:
                [viewController sendData:CMD_CH2_LIMITER_ATTACK commandType:DSP_WRITE dspId:DSP_2 value:knobLimiterAttack.value];
                break;
            case CHANNEL_CH_7:
                [viewController sendData:CMD_CH3_LIMITER_ATTACK commandType:DSP_WRITE dspId:DSP_2 value:knobLimiterAttack.value];
                break;
            case CHANNEL_CH_8:
                [viewController sendData:CMD_CH4_LIMITER_ATTACK commandType:DSP_WRITE dspId:DSP_2 value:knobLimiterAttack.value];
                break;
            case CHANNEL_CH_9:
                [viewController sendData:CMD_CH1_LIMITER_ATTACK commandType:DSP_WRITE dspId:DSP_3 value:knobLimiterAttack.value];
                break;
            case CHANNEL_CH_10:
                [viewController sendData:CMD_CH2_LIMITER_ATTACK commandType:DSP_WRITE dspId:DSP_3 value:knobLimiterAttack.value];
                break;
            case CHANNEL_CH_11:
                [viewController sendData:CMD_CH3_LIMITER_ATTACK commandType:DSP_WRITE dspId:DSP_3 value:knobLimiterAttack.value];
                break;
            case CHANNEL_CH_12:
                [viewController sendData:CMD_CH4_LIMITER_ATTACK commandType:DSP_WRITE dspId:DSP_3 value:knobLimiterAttack.value];
                break;
            case CHANNEL_CH_13:
                [viewController sendData:CMD_CH1_LIMITER_ATTACK commandType:DSP_WRITE dspId:DSP_4 value:knobLimiterAttack.value];
                break;
            case CHANNEL_CH_14:
                [viewController sendData:CMD_CH2_LIMITER_ATTACK commandType:DSP_WRITE dspId:DSP_4 value:knobLimiterAttack.value];
                break;
            case CHANNEL_CH_15:
                [viewController sendData:CMD_CH3_LIMITER_ATTACK commandType:DSP_WRITE dspId:DSP_4 value:knobLimiterAttack.value];
                break;
            case CHANNEL_CH_16:
                [viewController sendData:CMD_CH4_LIMITER_ATTACK commandType:DSP_WRITE dspId:DSP_4 value:knobLimiterAttack.value];
                break;
            case CHANNEL_CH_17:
                [viewController sendData:CMD_CH17_LIMITER_ATTACK commandType:DSP_WRITE dspId:DSP_6 value:knobLimiterAttack.value];
                break;
            case CHANNEL_CH_18:
                [viewController sendData:CMD_CH18_LIMITER_ATTACK commandType:DSP_WRITE dspId:DSP_6 value:knobLimiterAttack.value];
                break;
            case CHANNEL_MULTI_1:
                [viewController sendData:CMD_MULTI1_LIMITER_ATTACK commandType:DSP_WRITE dspId:DSP_7 value:knobLimiterAttack.value];
                break;
            case CHANNEL_MULTI_2:
                [viewController sendData:CMD_MULTI2_LIMITER_ATTACK commandType:DSP_WRITE dspId:DSP_7 value:knobLimiterAttack.value];
                break;
            case CHANNEL_MULTI_3:
                [viewController sendData:CMD_MULTI3_LIMITER_ATTACK commandType:DSP_WRITE dspId:DSP_7 value:knobLimiterAttack.value];
                break;
            case CHANNEL_MULTI_4:
                [viewController sendData:CMD_MULTI4_LIMITER_ATTACK commandType:DSP_WRITE dspId:DSP_7 value:knobLimiterAttack.value];
                break;
            case CHANNEL_MAIN:
                [viewController sendData:CMD_MAIN_LR_LIMITER_ATTACK commandType:DSP_WRITE dspId:DSP_6 value:knobLimiterAttack.value];
                break;
            default:
                break;
        }
    }
}

- (IBAction)knobLimiterReleaseDidChange:(id)sender
{
    lblLimiterRelease.text = [Global getTimeString:knobLimiterRelease.value];
    cpvLimiterRelease.progress = knobLimiterRelease.value/ATTACK_RELEASE_MAX;
    if (sender != nil) {
        switch (currentChannel) {
            case CHANNEL_CH_1:
                [viewController sendData:CMD_CH1_LIMITER_RELEASE commandType:DSP_WRITE dspId:DSP_1 value:knobLimiterRelease.value];
                break;
            case CHANNEL_CH_2:
                [viewController sendData:CMD_CH2_LIMITER_RELEASE commandType:DSP_WRITE dspId:DSP_1 value:knobLimiterRelease.value];
                break;
            case CHANNEL_CH_3:
                [viewController sendData:CMD_CH3_LIMITER_RELEASE commandType:DSP_WRITE dspId:DSP_1 value:knobLimiterRelease.value];
                break;
            case CHANNEL_CH_4:
                [viewController sendData:CMD_CH4_LIMITER_RELEASE commandType:DSP_WRITE dspId:DSP_1 value:knobLimiterRelease.value];
                break;
            case CHANNEL_CH_5:
                [viewController sendData:CMD_CH1_LIMITER_RELEASE commandType:DSP_WRITE dspId:DSP_2 value:knobLimiterRelease.value];
                break;
            case CHANNEL_CH_6:
                [viewController sendData:CMD_CH2_LIMITER_RELEASE commandType:DSP_WRITE dspId:DSP_2 value:knobLimiterRelease.value];
                break;
            case CHANNEL_CH_7:
                [viewController sendData:CMD_CH3_LIMITER_RELEASE commandType:DSP_WRITE dspId:DSP_2 value:knobLimiterRelease.value];
                break;
            case CHANNEL_CH_8:
                [viewController sendData:CMD_CH4_LIMITER_RELEASE commandType:DSP_WRITE dspId:DSP_2 value:knobLimiterRelease.value];
                break;
            case CHANNEL_CH_9:
                [viewController sendData:CMD_CH1_LIMITER_RELEASE commandType:DSP_WRITE dspId:DSP_3 value:knobLimiterRelease.value];
                break;
            case CHANNEL_CH_10:
                [viewController sendData:CMD_CH2_LIMITER_RELEASE commandType:DSP_WRITE dspId:DSP_3 value:knobLimiterRelease.value];
                break;
            case CHANNEL_CH_11:
                [viewController sendData:CMD_CH3_LIMITER_RELEASE commandType:DSP_WRITE dspId:DSP_3 value:knobLimiterRelease.value];
                break;
            case CHANNEL_CH_12:
                [viewController sendData:CMD_CH4_LIMITER_RELEASE commandType:DSP_WRITE dspId:DSP_3 value:knobLimiterRelease.value];
                break;
            case CHANNEL_CH_13:
                [viewController sendData:CMD_CH1_LIMITER_RELEASE commandType:DSP_WRITE dspId:DSP_4 value:knobLimiterRelease.value];
                break;
            case CHANNEL_CH_14:
                [viewController sendData:CMD_CH2_LIMITER_RELEASE commandType:DSP_WRITE dspId:DSP_4 value:knobLimiterRelease.value];
                break;
            case CHANNEL_CH_15:
                [viewController sendData:CMD_CH3_LIMITER_RELEASE commandType:DSP_WRITE dspId:DSP_4 value:knobLimiterRelease.value];
                break;
            case CHANNEL_CH_16:
                [viewController sendData:CMD_CH4_LIMITER_RELEASE commandType:DSP_WRITE dspId:DSP_4 value:knobLimiterRelease.value];
                break;
            case CHANNEL_CH_17:
                [viewController sendData:CMD_CH17_LIMITER_RELEASE commandType:DSP_WRITE dspId:DSP_6 value:knobLimiterRelease.value];
                break;
            case CHANNEL_CH_18:
                [viewController sendData:CMD_CH18_LIMITER_RELEASE commandType:DSP_WRITE dspId:DSP_6 value:knobLimiterRelease.value];
                break;
            case CHANNEL_MULTI_1:
                [viewController sendData:CMD_MULTI1_LIMITER_RELEASE commandType:DSP_WRITE dspId:DSP_7 value:knobLimiterRelease.value];
                break;
            case CHANNEL_MULTI_2:
                [viewController sendData:CMD_MULTI2_LIMITER_RELEASE commandType:DSP_WRITE dspId:DSP_7 value:knobLimiterRelease.value];
                break;
            case CHANNEL_MULTI_3:
                [viewController sendData:CMD_MULTI3_LIMITER_RELEASE commandType:DSP_WRITE dspId:DSP_7 value:knobLimiterRelease.value];
                break;
            case CHANNEL_MULTI_4:
                [viewController sendData:CMD_MULTI4_LIMITER_RELEASE commandType:DSP_WRITE dspId:DSP_7 value:knobLimiterRelease.value];
                break;
            case CHANNEL_MAIN:
                [viewController sendData:CMD_MAIN_LR_LIMITER_RELEASE commandType:DSP_WRITE dspId:DSP_6 value:knobLimiterRelease.value];
                break;
            default:
                break;
        }
    }
}

#pragma mark -
#pragma mark Threshold and Ratio ball event handlers

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    // Detect touches for scene view
    CGPoint point = [[touches anyObject] locationInView:self.viewOutput];
    //NSLog(@"touchesBegan at %.1f, %.1f", point.x, point.y);
    
    if (point.x > -TR_BALL_WIDTH && point.x < self.viewOutput.bounds.size.width + TR_BALL_WIDTH &&
        point.y > -TR_BALL_WIDTH && point.y < self.viewOutput.bounds.size.height + TR_BALL_WIDTH) {
        if (CGRectContainsPoint(self.btnBallThreshold.frame, point)) {
            [self onBtnBallThresholdTouched:self];
        } else if (CGRectContainsPoint(self.btnBallRatio.frame, point)) {
            [self onBtnBallRatioTouched:self];
        }
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    // Detect touches for scene view
    CGPoint point = [[touches anyObject] locationInView:self.viewOutput];
    //NSLog(@"touchesMoved at %.1f, %.1f", point.x, point.y);
    
    if (point.x > -TR_BALL_WIDTH && point.x < self.viewOutput.bounds.size.width + TR_BALL_WIDTH &&
        point.y > -TR_BALL_WIDTH && point.y < self.viewOutput.bounds.size.height + TR_BALL_WIDTH) {
        if (CGRectContainsPoint(self.btnBallThreshold.frame, point)) {
            [self onBtnBallThresholdMoved:self withEvent:event];
        } else if (CGRectContainsPoint(self.btnBallRatio.frame, point)) {
            [self onBtnBallRatioMoved:self withEvent:event];
        }
    }
}

// Convert x-coordinate to threshold gain
- (int)dynXToThreshold:(CGFloat)ptX
{
    return (int)(ptX * THRESHOLD_MAX/DYN_FRAME_WIDTH);
}

// Convert y-coordinate to threshold gain
- (int)dynYToThreshold:(CGFloat)ptY
{
    return (int)(ptY * EQ_GAIN_MAX_VALUE/DYN_FRAME_HEIGHT);
}

// Convert threshold gain to x-coordinate
- (CGFloat)dynThresholdToX:(int)threshold
{
    float value = (threshold + THRESHOLD_MAX) * DYN_FRAME_WIDTH/THRESHOLD_MAX;
    if (value > DYN_FRAME_WIDTH) {
        value = DYN_FRAME_WIDTH;
    }
    return value;
}

// Convert threshold gain to y-coordinate
- (CGFloat)dynThresholdToY:(int)threshold
{
    float value = -threshold * DYN_FRAME_HEIGHT/THRESHOLD_MAX;
    if (value < 0) {
        value = 0;
    }
    return value;
}

// Convert x-coordinate to ratio
- (int)dynXToRatio:(CGFloat)ptX thresholdPoint:(CGFloat)ptThreshold
{
    return (int)(ptX * RATIO_MAX/ptThreshold);
}

// Convert y-coordinate to ratio
- (int)dynYToRatio:(CGFloat)ptY thresholdPoint:(CGFloat)ptThreshold
{
    return (int)(ptY * RATIO_MAX/ptThreshold);
}

// Convert ratio to x-coordinate
- (CGFloat)dynRatioToX:(int)ratio thresholdPoint:(CGFloat)ptThreshold
{
    return ratio * ptThreshold / RATIO_MAX;
}

// Convert ratio to y-coordinate
- (CGFloat)dynRatioToY:(CGFloat)ratio thresholdPoint:(CGFloat)ptThreshold
{
    return ratio * ptThreshold / RATIO_MAX;
}

- (IBAction)onBtnBallThresholdTouched:(id)sender {
    btnBallThreshold.selected = YES;
}

- (IBAction)onBtnBallThresholdMoved:(id)sender withEvent:(UIEvent *)event {
    CGPoint point = [[[event allTouches] anyObject] locationInView:self.viewOutput];
    
    CGFloat ptX, ptY;
    if (point.x < 0.0) {
        ptX = 0.0;
    } else if (point.x > self.viewOutput.frame.size.width) {
        ptX = self.viewOutput.frame.size.width;
    } else {
        ptX = point.x;
    }
    
    if (point.y < 0.0) {
        ptY = 0.0;
    } else if (point.y > self.viewOutput.frame.size.height) {
        ptY = self.viewOutput.frame.size.height;
    } else {
        ptY = point.y;
    }
    
    // Ensure threshold button stays on linear graph
    ptY = self.viewOutput.frame.size.height - ptX;
    
    btnBallThreshold.center = CGPointMake(ptX, ptY);
    
    if (btnDynGate.selected) {
        dynGateThreshold[currentChannel] = knobGateThreshold.value = [self dynXToThreshold:ptX];
        [self knobGateThresholdDidChange:self];
    } else if (btnDynExpander.selected) {
        dynExpThreshold[currentChannel] = knobExpanderThreshold.value = [self dynXToThreshold:ptX];
        [self knobExpanderThresholdDidChange:self];
    } else if (btnDynComp.selected) {
        dynCompThreshold[currentChannel] = knobCompThreshold.value = [self dynXToThreshold:ptX];
        [self knobCompThresholdDidChange:self];
    } else {
        dynLimitThreshold[currentChannel] = knobLimiterThreshold.value = [self dynXToThreshold:ptX];
        [self knobLimiterThresholdDidChange:self];
    }
}

- (IBAction)onBtnBallThresholdReleased:(id)sender {
    btnBallThreshold.selected = NO;
}

- (IBAction)onBtnBallRatioTouched:(id)sender {
    btnBallRatio.selected = YES;
}

- (IBAction)onBtnBallRatioMoved:(id)sender withEvent:(UIEvent *)event {
    CGPoint point = [[[event allTouches] anyObject] locationInView:self.viewOutput];
    
    if (btnDynExpander.selected) {
        // Get current threshold position
        float thresholdPos = [self dynThresholdToX:dynExpThreshold[currentChannel]];
        
        CGFloat ptX;
        if (point.x < 0.0) {
            ptX = 0.0;
        } else if (point.x > thresholdPos) {
            ptX = thresholdPos;
        } else {
            ptX = point.x;
        }
        
        dynExpRatio[currentChannel] = knobExpanderRatio.value = [self dynXToRatio:ptX thresholdPoint:thresholdPos];
        [self knobExpanderRatioDidChange:self];
    } else if (btnDynComp.selected) {
        // Get current threshold position
        float thresholdPos = [self dynThresholdToX:dynCompThreshold[currentChannel]];
        
        CGFloat ptY;
        if (point.y < 0.0) {
            ptY = 0.0;
        } else if (point.y > thresholdPos) {
            ptY = thresholdPos;
        } else {
            ptY = point.y;
        }
        
        dynCompRatio[currentChannel] = knobCompRatio.value = [self dynYToRatio:ptY thresholdPoint:thresholdPos];
        [self knobCompRatioDidChange:self];
    }
}

- (IBAction)onBtnBallRatioReleased:(id)sender {
    btnBallRatio.selected = NO;
}

#pragma mark -
#pragma mark Text label helper methods

- (void)updatecurrentChannelLabel
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    switch (currentChannelLabel) {
        case CHANNEL_CH_1:
            if ([userDefaults objectForKey:@"Ch1"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch1"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch1", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch1", @"Acapela", nil)];
            break;
        case CHANNEL_CH_2:
            if ([userDefaults objectForKey:@"Ch2"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch2"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch2", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch2", @"Acapela", nil)];
            break;
        case CHANNEL_CH_3:
            if ([userDefaults objectForKey:@"Ch3"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch3"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch3", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch3", @"Acapela", nil)];
            break;
        case CHANNEL_CH_4:
            if ([userDefaults objectForKey:@"Ch4"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch4"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch4", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch4", @"Acapela", nil)];
            break;
        case CHANNEL_CH_5:
            if ([userDefaults objectForKey:@"Ch5"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch5"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch5", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch5", @"Acapela", nil)];
            break;
        case CHANNEL_CH_6:
            if ([userDefaults objectForKey:@"Ch6"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch6"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch6", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch6", @"Acapela", nil)];
            break;
        case CHANNEL_CH_7:
            if ([userDefaults objectForKey:@"Ch7"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch7"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch7", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch7", @"Acapela", nil)];
            break;
        case CHANNEL_CH_8:
            if ([userDefaults objectForKey:@"Ch8"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch8"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch8", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch8", @"Acapela", nil)];
            break;
        case CHANNEL_CH_9:
            if ([userDefaults objectForKey:@"Ch9"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch9"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch9", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch9", @"Acapela", nil)];
            break;
        case CHANNEL_CH_10:
            if ([userDefaults objectForKey:@"Ch10"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch10"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch10", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch10", @"Acapela", nil)];
            break;
        case CHANNEL_CH_11:
            if ([userDefaults objectForKey:@"Ch11"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch11"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch11", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch11", @"Acapela", nil)];
            break;
        case CHANNEL_CH_12:
            if ([userDefaults objectForKey:@"Ch12"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch12"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch12", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch12", @"Acapela", nil)];
            break;
        case CHANNEL_CH_13:
            if ([userDefaults objectForKey:@"Ch13"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch13"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch13", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch13", @"Acapela", nil)];
            break;
        case CHANNEL_CH_14:
            if ([userDefaults objectForKey:@"Ch14"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch14"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch14", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch14", @"Acapela", nil)];
            break;
        case CHANNEL_CH_15:
            if ([userDefaults objectForKey:@"Ch15"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch15"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch15", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch15", @"Acapela", nil)];
            break;
        case CHANNEL_CH_16:
            if ([userDefaults objectForKey:@"Ch16"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch16"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch16", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch16", @"Acapela", nil)];
            break;
        case CHANNEL_CH_17:
            if ([userDefaults objectForKey:@"Ch17"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch17"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch17", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch17", @"Acapela", nil)];
            break;
        case CHANNEL_CH_18:
            if ([userDefaults objectForKey:@"Ch18"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Ch18"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Ch18", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Ch18", @"Acapela", nil)];
            break;
        case CHANNEL_MULTI_1:
            if ([userDefaults objectForKey:@"Multi1"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Multi1"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Multi1", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Multi1", @"Acapela", nil)];
            break;
        case CHANNEL_MULTI_2:
            if ([userDefaults objectForKey:@"Multi2"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Multi2"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Multi2", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Multi2", @"Acapela", nil)];
            break;
        case CHANNEL_MULTI_3:
            if ([userDefaults objectForKey:@"Multi3"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Multi3"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Multi3", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Multi3", @"Acapela", nil)];
            break;
        case CHANNEL_MULTI_4:
            if ([userDefaults objectForKey:@"Multi4"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Multi4"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Multi4", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Multi4", @"Acapela", nil)];
            break;
        case CHANNEL_MAIN:
            if ([userDefaults objectForKey:@"Main"] != nil) {
                [txtPage setText:(NSString *)[userDefaults objectForKey:@"Main"]];
            } else {
                [txtPage setText:NSLocalizedStringFromTable(@"Main", @"Acapela", nil)];
            }
            [lblPage setText:NSLocalizedStringFromTable(@"Main", @"Acapela", nil)];
            break;
        default:
            break;
    }
}

- (void)writeToLabel
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    // Update name to original text field
    if (currentChannelLabel == currentChannel) {
        [txtPage setText:txtPage.text];
    }
    
    // Save to user defaults
    switch (currentChannelLabel) {
        case CHANNEL_CH_1:
            [userDefaults setObject:txtPage.text forKey:@"Ch1"];
            [viewController setAudioName:1 channelNum:0 channelName:[txtPage.text UTF8String]];
            break;
        case CHANNEL_CH_2:
            [userDefaults setObject:txtPage.text forKey:@"Ch2"];
            [viewController setAudioName:1 channelNum:1 channelName:[txtPage.text UTF8String]];
            break;
        case CHANNEL_CH_3:
            [userDefaults setObject:txtPage.text forKey:@"Ch3"];
            [viewController setAudioName:1 channelNum:2 channelName:[txtPage.text UTF8String]];
            break;
        case CHANNEL_CH_4:
            [userDefaults setObject:txtPage.text forKey:@"Ch4"];
            [viewController setAudioName:1 channelNum:3 channelName:[txtPage.text UTF8String]];
            break;
        case CHANNEL_CH_5:
            [userDefaults setObject:txtPage.text forKey:@"Ch5"];
            [viewController setAudioName:1 channelNum:4 channelName:[txtPage.text UTF8String]];
            break;
        case CHANNEL_CH_6:
            [userDefaults setObject:txtPage.text forKey:@"Ch6"];
            [viewController setAudioName:1 channelNum:5 channelName:[txtPage.text UTF8String]];
            break;
        case CHANNEL_CH_7:
            [userDefaults setObject:txtPage.text forKey:@"Ch7"];
            [viewController setAudioName:1 channelNum:6 channelName:[txtPage.text UTF8String]];
            break;
        case CHANNEL_CH_8:
            [userDefaults setObject:txtPage.text forKey:@"Ch8"];
            [viewController setAudioName:1 channelNum:7 channelName:[txtPage.text UTF8String]];
            break;
        case CHANNEL_CH_9:
            [userDefaults setObject:txtPage.text forKey:@"Ch9"];
            [viewController setAudioName:1 channelNum:8 channelName:[txtPage.text UTF8String]];
            break;
        case CHANNEL_CH_10:
            [userDefaults setObject:txtPage.text forKey:@"Ch10"];
            [viewController setAudioName:1 channelNum:9 channelName:[txtPage.text UTF8String]];
            break;
        case CHANNEL_CH_11:
            [userDefaults setObject:txtPage.text forKey:@"Ch11"];
            [viewController setAudioName:1 channelNum:10 channelName:[txtPage.text UTF8String]];
            break;
        case CHANNEL_CH_12:
            [userDefaults setObject:txtPage.text forKey:@"Ch12"];
            [viewController setAudioName:1 channelNum:11 channelName:[txtPage.text UTF8String]];
            break;
        case CHANNEL_CH_13:
            [userDefaults setObject:txtPage.text forKey:@"Ch13"];
            [viewController setAudioName:1 channelNum:12 channelName:[txtPage.text UTF8String]];
            break;
        case CHANNEL_CH_14:
            [userDefaults setObject:txtPage.text forKey:@"Ch14"];
            [viewController setAudioName:1 channelNum:13 channelName:[txtPage.text UTF8String]];
            break;
        case CHANNEL_CH_15:
            [userDefaults setObject:txtPage.text forKey:@"Ch15"];
            [viewController setAudioName:1 channelNum:14 channelName:[txtPage.text UTF8String]];
            break;
        case CHANNEL_CH_16:
            [userDefaults setObject:txtPage.text forKey:@"Ch16"];
            [viewController setAudioName:1 channelNum:15 channelName:[txtPage.text UTF8String]];
            break;
        case CHANNEL_CH_17:
            [userDefaults setObject:txtPage.text forKey:@"Ch17"];
            [viewController setAudioName:1 channelNum:16 channelName:[txtPage.text UTF8String]];
            break;
        case CHANNEL_CH_18:
            [userDefaults setObject:txtPage.text forKey:@"Ch18"];
            [viewController setAudioName:1 channelNum:17 channelName:[txtPage.text UTF8String]];
            break;
        case CHANNEL_MULTI_1:
            [userDefaults setObject:txtPage.text forKey:@"Multi1"];
            [viewController setAudioName:4 channelNum:0 channelName:[txtPage.text UTF8String]];
            break;
        case CHANNEL_MULTI_2:
            [userDefaults setObject:txtPage.text forKey:@"Multi2"];
            [viewController setAudioName:4 channelNum:1 channelName:[txtPage.text UTF8String]];
            break;
        case CHANNEL_MULTI_3:
            [userDefaults setObject:txtPage.text forKey:@"Multi3"];
            [viewController setAudioName:4 channelNum:2 channelName:[txtPage.text UTF8String]];
            break;
        case CHANNEL_MULTI_4:
            [userDefaults setObject:txtPage.text forKey:@"Multi4"];
            [viewController setAudioName:4 channelNum:3 channelName:[txtPage.text UTF8String]];
            break;
        case CHANNEL_MAIN:
            [userDefaults setObject:txtPage.text forKey:@"Main"];
            [viewController setAudioName:0 channelNum:0 channelName:[txtPage.text UTF8String]];
            break;
        default:
            break;
    }
    
    [userDefaults synchronize];
}

- (void)setSceneFilename:(NSString*)fileName {
    [btnFile setTitle:fileName forState:UIControlStateNormal];
}

@end
