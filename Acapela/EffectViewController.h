//
//  EffectViewController.h
//  eLive
//
//  Created by Kevin on 13/1/3.
//  Copyright (c) 2013年 Kevin Phua. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MHRotaryKnob.h"
#import "DACircularProgressView.h"
#import "EffectGeq31ViewController.h"
#import "EffectGeq15ViewController.h"
#import "acapela.h"

typedef enum
{
    EFX_1,
    EFX_2
} EFFECT_NUM;

typedef enum
{
    EFFECT_REVERB = 0,
    EFFECT_ECHO_DELAY,
    EFFECT_TAP_DELAY,
    EFFECT_CHORUS,
    EFFECT_FLANGER,
    EFFECT_PHASER,
    EFFECT_VIBRATO,
    EFFECT_TREMOLO,
    EFFECT_AUTO_PAN,
    EFFECT_GEQ_31,
    EFFECT_GEQ_15 = 9
} EFFECT_TYPE;

typedef enum
{
    REVERB_ROOM,
    REVERB_HALL,
    REVERB_PLATE
} REVERB_GROUP;

typedef enum
{
    ROOM_LARGE = 0,
    ROOM_MEDIUM,
    ROOM_SMALL,
    ROOM_LIVE,
    ROOM_BRIGHT,
    ROOM_WOOD,
    ROOM_HEAVY,
    ROOM_OPERA,
    HALL_LARGE,
    HALL_MEDIUM,
    HALL_SMALL,
    HALL_CONCERT,
    HALL_DARK,
    HALL_WONDER,
    HALL_JAZZ,
    HALL_VOCAL,
    PLATE_LARGE,
    PLATE_MEDIUM,
    PLATE_SMALL,
    PLATE_FLAT,
    PLATE_LIGHT,
    PLATE_THIN,
    PLATE_PERC,
    PLATE_INDUSTRIAL
} REVERB_TYPE;

typedef enum
{
    HPF_FREQ = 0,
    LPF_FREQ,
    PRE_DELAY,
    EARLY_DELAY_OUT,
    REVERB_TIME,
    REVERB_LEVEL,
    HI_RATIO,
    DENSITY,
    GATE,
    GATE_THRESHOLD,
    GATE_HOLD_TIME
} REVERB_PARAMETER;

typedef enum
{
    TRIANGLE = 1,
    SINE
} LFO_TYPE;

typedef enum
{
    EFFECT_IN_NULL = 0,
    EFFECT_IN_CH_1,
    EFFECT_IN_CH_2,
    EFFECT_IN_CH_3,
    EFFECT_IN_CH_4,
    EFFECT_IN_CH_5,
    EFFECT_IN_CH_6,
    EFFECT_IN_CH_7,
    EFFECT_IN_CH_8,
    EFFECT_IN_CH_9,
    EFFECT_IN_CH_10,
    EFFECT_IN_CH_11,
    EFFECT_IN_CH_12,
    EFFECT_IN_CH_13,
    EFFECT_IN_CH_14,
    EFFECT_IN_CH_15,
    EFFECT_IN_CH_16,
    EFFECT_IN_GP_1,
    EFFECT_IN_GP_2,
    EFFECT_IN_GP_3,
    EFFECT_IN_GP_4,
    EFFECT_IN_NOP_1,
    EFFECT_IN_NOP_2,
    EFFECT_IN_NOP_3,
    EFFECT_IN_NOP_4,
    EFFECT_IN_AUX_1,
    EFFECT_IN_AUX_2,
    EFFECT_IN_AUX_3,
    EFFECT_IN_AUX_4,
    EFFECT_IN_NOP_5,
    EFFECT_IN_NOP_6,
    EFFECT_IN_NOP_7,
    EFFECT_IN_NOP_8,
    EFFECT_IN_CH_17,
    EFFECT_IN_CH_18,
} EFFECT_IN;

typedef enum
{
    EFFECT_OUT_L_GP_1 = 0,
    EFFECT_OUT_L_GP_3,
    EFFECT_OUT_L_NOP_1,
    EFFECT_OUT_L_NOP_2,
    EFFECT_OUT_L_MULTI_1,
    EFFECT_OUT_L_MULTI_3,
    EFFECT_OUT_L_NOP_3,
    EFFECT_OUT_L_NOP_4,
    EFFECT_OUT_L_MAIN_L
} EFFECT_OUT_L;

typedef enum
{
    EFFECT_OUT_R_GP_2 = 0,
    EFFECT_OUT_R_GP_4,
    EFFECT_OUT_R_NOP_1,
    EFFECT_OUT_R_NOP_2,
    EFFECT_OUT_R_MULTI_2,
    EFFECT_OUT_R_MULTI_4,
    EFFECT_OUT_R_NOP_3,
    EFFECT_OUT_R_NOP_4,
    EFFECT_OUT_R_MAIN_R
} EFFECT_OUT_R;

@interface EffectViewController : UIViewController<Geq31PreviewDelegate, Geq15PreviewDelegate, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *imgBackground;
@property (weak, nonatomic) IBOutlet UIButton *btnFile;
@property (weak, nonatomic) IBOutlet UIButton *btnReset;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectInL;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectInR;
@property (weak, nonatomic) IBOutlet UIButton *btnEffect;
@property (weak, nonatomic) IBOutlet UIButton *btnReverbType;
@property (weak, nonatomic) IBOutlet UISlider *sliderEffectDryWet;
@property (weak, nonatomic) IBOutlet UILabel *lblEffectDryWet;

// Effect In L selection view
@property (weak, nonatomic) IBOutlet UIView *viewEffectInL;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectInLCh1;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectInLCh2;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectInLCh3;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectInLCh4;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectInLCh5;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectInLCh6;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectInLCh7;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectInLCh8;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectInLCh9;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectInLCh10;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectInLCh11;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectInLCh12;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectInLCh13;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectInLCh14;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectInLCh15;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectInLCh16;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectInLCh17;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectInLCh18;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectInLAux1;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectInLAux2;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectInLAux3;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectInLAux4;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectInLGp1;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectInLGp2;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectInLGp3;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectInLGp4;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectInLNull;

// Effect In R selection view
@property (weak, nonatomic) IBOutlet UIView *viewEffectInR;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectInRCh1;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectInRCh2;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectInRCh3;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectInRCh4;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectInRCh5;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectInRCh6;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectInRCh7;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectInRCh8;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectInRCh9;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectInRCh10;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectInRCh11;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectInRCh12;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectInRCh13;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectInRCh14;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectInRCh15;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectInRCh16;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectInRCh17;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectInRCh18;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectInRAux1;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectInRAux2;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectInRAux3;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectInRAux4;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectInRGp1;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectInRGp2;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectInRGp3;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectInRGp4;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectInRNull;

// Effect Out L
@property (weak, nonatomic) IBOutlet UIButton *btnEffectOutLMulti1Status;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectOutLMulti3Status;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectOutLMainLStatus;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectOutLGp1Status;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectOutLGp3Status;

// Effect Out R
@property (weak, nonatomic) IBOutlet UIButton *btnEffectOutRMulti2Status;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectOutRMulti4Status;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectOutRMainRStatus;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectOutRGp2Status;
@property (weak, nonatomic) IBOutlet UIButton *btnEffectOutRGp4Status;

// Effect 1 selection view
@property (weak, nonatomic) IBOutlet UIView *viewEffect1;
@property (weak, nonatomic) IBOutlet UIButton *btnEffect1ReverbRoom;
@property (weak, nonatomic) IBOutlet UIButton *btnEffect1ReverbHall;
@property (weak, nonatomic) IBOutlet UIButton *btnEffect1ReverbPlate;
@property (weak, nonatomic) IBOutlet UIButton *btnEffect1Echo;
@property (weak, nonatomic) IBOutlet UIButton *btnEffect1TapDelay;
@property (weak, nonatomic) IBOutlet UIButton *btnEffect1Chorus;
@property (weak, nonatomic) IBOutlet UIButton *btnEffect1Flanger;
@property (weak, nonatomic) IBOutlet UIButton *btnEffect1Phaser;
@property (weak, nonatomic) IBOutlet UIButton *btnEffect1Tremolo;
@property (weak, nonatomic) IBOutlet UIButton *btnEffect1Vibrato;
@property (weak, nonatomic) IBOutlet UIButton *btnEffect1AutoPan;
@property (weak, nonatomic) IBOutlet UIButton *btnEffect1Geq31;

// Effect 2 selection view
@property (weak, nonatomic) IBOutlet UIView *viewEffect2;
@property (weak, nonatomic) IBOutlet UIButton *btnEffect2Echo;
@property (weak, nonatomic) IBOutlet UIButton *btnEffect2TapDelay;
@property (weak, nonatomic) IBOutlet UIButton *btnEffect2Chorus;
@property (weak, nonatomic) IBOutlet UIButton *btnEffect2Flanger;
@property (weak, nonatomic) IBOutlet UIButton *btnEffect2Phaser;
@property (weak, nonatomic) IBOutlet UIButton *btnEffect2Tremolo;
@property (weak, nonatomic) IBOutlet UIButton *btnEffect2Vibrato;
@property (weak, nonatomic) IBOutlet UIButton *btnEffect2AutoPan;
@property (weak, nonatomic) IBOutlet UIButton *btnEffect2Geq15;

// Room selection view
@property (weak, nonatomic) IBOutlet UIView *viewReverbType;
@property (weak, nonatomic) IBOutlet UIButton *btnReverbType1;
@property (weak, nonatomic) IBOutlet UIButton *btnReverbType2;
@property (weak, nonatomic) IBOutlet UIButton *btnReverbType3;
@property (weak, nonatomic) IBOutlet UIButton *btnReverbType4;
@property (weak, nonatomic) IBOutlet UIButton *btnReverbType5;
@property (weak, nonatomic) IBOutlet UIButton *btnReverbType6;
@property (weak, nonatomic) IBOutlet UIButton *btnReverbType7;
@property (weak, nonatomic) IBOutlet UIButton *btnReverbType8;

// Meters
@property (nonatomic,retain) JEProgressView *meterInL;
@property (nonatomic,retain) JEProgressView *meterInR;
@property (nonatomic,retain) JEProgressView *meterOutL;
@property (nonatomic,retain) JEProgressView *meterOutR;

// Reverb view
@property (weak, nonatomic) IBOutlet UIView *viewReverb;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobHpf;
@property (nonatomic, retain) DACircularProgressView *cpvHpf;
@property (weak, nonatomic) IBOutlet UILabel *lblHpf;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobLpf;
@property (nonatomic, retain) DACircularProgressView *cpvLpf;
@property (weak, nonatomic) IBOutlet UILabel *lblLpf;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobPreDelay;
@property (nonatomic, retain) DACircularProgressView *cpvPreDelay;
@property (weak, nonatomic) IBOutlet UILabel *lblPreDelay;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobEarlyOut;
@property (nonatomic, retain) DACircularProgressView *cpvEarlyOut;
@property (weak, nonatomic) IBOutlet UILabel *lblEarlyOut;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobReverbTime;
@property (nonatomic, retain) DACircularProgressView *cpvReverbTime;
@property (weak, nonatomic) IBOutlet UILabel *lblReverbTime;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobLevel;
@property (nonatomic, retain) DACircularProgressView *cpvLevel;
@property (weak, nonatomic) IBOutlet UILabel *lblLevel;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobHiRatio;
@property (nonatomic, retain) DACircularProgressView *cpvHiRatio;
@property (weak, nonatomic) IBOutlet UILabel *lblHiRatio;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobDensity;
@property (nonatomic, retain) DACircularProgressView *cpvDensity;
@property (weak, nonatomic) IBOutlet UILabel *lblDensity;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobThreshold;
@property (nonatomic, retain) DACircularProgressView *cpvThreshold;
@property (weak, nonatomic) IBOutlet UILabel *lblThreshold;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobHold;
@property (nonatomic, retain) DACircularProgressView *cpvHold;
@property (weak, nonatomic) IBOutlet UILabel *lblHold;
@property (weak, nonatomic) IBOutlet UIButton *btnGate;

// Echo view
@property (weak, nonatomic) IBOutlet UIView *viewEcho;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobEchoTimeL;
@property (nonatomic, retain) DACircularProgressView *cpvEchoTimeL;
@property (weak, nonatomic) IBOutlet UILabel *lblEchoTimeL;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobEchoTimeR;
@property (nonatomic, retain) DACircularProgressView *cpvEchoTimeR;
@property (weak, nonatomic) IBOutlet UILabel *lblEchoTimeR;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobEchoFb1;
@property (nonatomic, retain) DACircularProgressView *cpvEchoFb1;
@property (weak, nonatomic) IBOutlet UILabel *lblEchoFb1;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobEchoFb2;
@property (nonatomic, retain) DACircularProgressView *cpvEchoFb2;
@property (weak, nonatomic) IBOutlet UILabel *lblEchoFb2;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobEchoHpf;
@property (nonatomic, retain) DACircularProgressView *cpvEchoHpf;
@property (weak, nonatomic) IBOutlet UILabel *lblEchoHpf;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobEchoLpf;
@property (nonatomic, retain) DACircularProgressView *cpvEchoLpf;
@property (weak, nonatomic) IBOutlet UILabel *lblEchoLpf;

// Tap Delay view
@property (weak, nonatomic) IBOutlet UIView *viewTapDelay;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobTapFb;
@property (nonatomic, retain) DACircularProgressView *cpvTapFb;
@property (weak, nonatomic) IBOutlet UILabel *lblTapFb;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobTapHpf;
@property (nonatomic, retain) DACircularProgressView *cpvTapHpf;
@property (weak, nonatomic) IBOutlet UILabel *lblTapHpf;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobTapLpf;
@property (nonatomic, retain) DACircularProgressView *cpvTapLpf;
@property (weak, nonatomic) IBOutlet UILabel *lblTapLpf;
@property (weak, nonatomic) IBOutlet UILabel *lblTapDelay;
@property (weak, nonatomic) IBOutlet UIButton *btnTap;
@property (weak, nonatomic) IBOutlet UIImageView *imgTapLed;

// Chorus view
@property (weak, nonatomic) IBOutlet UIView *viewChorus;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobChorusPreDelay;
@property (nonatomic, retain) DACircularProgressView *cpvChorusPreDelay;
@property (weak, nonatomic) IBOutlet UILabel *lblChorusPreDelay;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobChorusDepth;
@property (nonatomic, retain) DACircularProgressView *cpvChorusDepth;
@property (weak, nonatomic) IBOutlet UILabel *lblChorusDepth;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobChorusLpf;
@property (nonatomic, retain) DACircularProgressView *cpvChorusLpf;
@property (weak, nonatomic) IBOutlet UILabel *lblChorusLpf;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobChorusLfoPhase;
@property (nonatomic, retain) DACircularProgressView *cpvChorusLfoPhase;
@property (weak, nonatomic) IBOutlet UILabel *lblChorusLfoPhase;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobChorusLfoFreq;
@property (nonatomic, retain) DACircularProgressView *cpvChorusLfoFreq;
@property (weak, nonatomic) IBOutlet UILabel *lblChorusLfoFreq;
@property (weak, nonatomic) IBOutlet UIButton *btnChorusLfoType;

// Flanger view
@property (weak, nonatomic) IBOutlet UIView *viewFlanger;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobFlangerPreDelay;
@property (nonatomic, retain) DACircularProgressView *cpvFlangerPreDelay;
@property (weak, nonatomic) IBOutlet UILabel *lblFlangerPreDelay;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobFlangerLpf;
@property (nonatomic, retain) DACircularProgressView *cpvFlangerLpf;
@property (weak, nonatomic) IBOutlet UILabel *lblFlangerLpf;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobFlangerDepth;
@property (nonatomic, retain) DACircularProgressView *cpvFlangerDepth;
@property (weak, nonatomic) IBOutlet UILabel *lblFlangerDepth;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobFlangerFb;
@property (nonatomic, retain) DACircularProgressView *cpvFlangerFb;
@property (weak, nonatomic) IBOutlet UILabel *lblFlangerFb;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobFlangerLfoPhase;
@property (nonatomic, retain) DACircularProgressView *cpvFlangerLfoPhase;
@property (weak, nonatomic) IBOutlet UILabel *lblFlangerLfoPhase;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobFlangerLfoFreq;
@property (nonatomic, retain) DACircularProgressView *cpvFlangerLfoFreq;
@property (weak, nonatomic) IBOutlet UILabel *lblFlangerLfoFreq;
@property (weak, nonatomic) IBOutlet UIButton *btnFlangerLfoType;

// Phaser view
@property (weak, nonatomic) IBOutlet UIView *viewPhaser;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobPhaserStageNo;
@property (nonatomic, retain) DACircularProgressView *cpvPhaserStageNo;
@property (weak, nonatomic) IBOutlet UILabel *lblPhaserStageNo;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobPhaserFreq;
@property (nonatomic, retain) DACircularProgressView *cpvPhaserFreq;
@property (weak, nonatomic) IBOutlet UILabel *lblPhaserFreq;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobPhaserDepth;
@property (nonatomic, retain) DACircularProgressView *cpvPhaserDepth;
@property (weak, nonatomic) IBOutlet UILabel *lblPhaserDepth;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobPhaserLfoFreq;
@property (nonatomic, retain) DACircularProgressView *cpvPhaserLfoFreq;
@property (weak, nonatomic) IBOutlet UILabel *lblPhaserLfoFreq;
@property (weak, nonatomic) IBOutlet UIButton *btnPhaserLfoType;

// Tremolo view
@property (weak, nonatomic) IBOutlet UIView *viewTremolo;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobTremoloDepth;
@property (nonatomic, retain) DACircularProgressView *cpvTremoloDepth;
@property (weak, nonatomic) IBOutlet UILabel *lblTremoloDepth;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobTremoloLfoFreq;
@property (nonatomic, retain) DACircularProgressView *cpvTremoloLfoFreq;
@property (weak, nonatomic) IBOutlet UILabel *lblTremoloLfoFreq;
@property (weak, nonatomic) IBOutlet UIButton *btnTremoloLfoType;

// Vibrato view
@property (weak, nonatomic) IBOutlet UIView *viewVibrato;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobVibratoFreq;
@property (nonatomic, retain) DACircularProgressView *cpvVibratoFreq;
@property (weak, nonatomic) IBOutlet UILabel *lblVibratoFreq;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobVibratoDepth;
@property (nonatomic, retain) DACircularProgressView *cpvVibratoDepth;
@property (weak, nonatomic) IBOutlet UILabel *lblVibratoDepth;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobVibratoLfoFreq;
@property (nonatomic, retain) DACircularProgressView *cpvVibratoLfoFreq;
@property (weak, nonatomic) IBOutlet UILabel *lblVibratoLfoFreq;
@property (weak, nonatomic) IBOutlet UIButton *btnVibratoLfoType;

// Auto pan view
@property (weak, nonatomic) IBOutlet UIView *viewAutoPan;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobAutoPanWay;
@property (nonatomic, retain) DACircularProgressView *cpvAutoPanWay;
@property (weak, nonatomic) IBOutlet UILabel *lblAutoPanWay;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobAutoPanDepth;
@property (nonatomic, retain) DACircularProgressView *cpvAutoPanDepth;
@property (weak, nonatomic) IBOutlet UILabel *lblAutoPanDepth;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobAutoPanLfoFreq;
@property (nonatomic, retain) DACircularProgressView *cpvAutoPanLfoFreq;
@property (weak, nonatomic) IBOutlet UILabel *lblAutoPanLfoFreq;
@property (weak, nonatomic) IBOutlet UIButton *btnAutoPanLfoType;

// GEQ31 view
@property (weak, nonatomic) IBOutlet UIView *viewGeq31;
@property (weak, nonatomic) IBOutlet UIImageView *imgGeq31LPreview;
@property (weak, nonatomic) IBOutlet UIImageView *imgGeq31RPreview;
@property (nonatomic,retain) EffectGeq31ViewController *effectGeq31Controller;

// GEQ15 view
@property (weak, nonatomic) IBOutlet UIView *viewGeq15;
@property (weak, nonatomic) IBOutlet UIImageView *imgGeq15LPreview;
@property (weak, nonatomic) IBOutlet UIImageView *imgGeq15RPreview;
@property (nonatomic,retain) EffectGeq15ViewController *effectGeq15Controller;

- (IBAction)onBtnFile:(id)sender;
- (IBAction)onBtnReset:(id)sender;
- (IBAction)onBtnEffectInL:(id)sender;
- (IBAction)onBtnEffectInR:(id)sender;
- (IBAction)onBtnEffect:(id)sender;
- (IBAction)onBtnReverbType:(id)sender;
- (IBAction)onSliderEffectDryWet:(id)sender;

- (IBAction)onBtnEffectInLCh1:(id)sender;
- (IBAction)onBtnEffectInLCh2:(id)sender;
- (IBAction)onBtnEffectInLCh3:(id)sender;
- (IBAction)onBtnEffectInLCh4:(id)sender;
- (IBAction)onBtnEffectInLCh5:(id)sender;
- (IBAction)onBtnEffectInLCh6:(id)sender;
- (IBAction)onBtnEffectInLCh7:(id)sender;
- (IBAction)onBtnEffectInLCh8:(id)sender;
- (IBAction)onBtnEffectInLCh9:(id)sender;
- (IBAction)onBtnEffectInLCh10:(id)sender;
- (IBAction)onBtnEffectInLCh11:(id)sender;
- (IBAction)onBtnEffectInLCh12:(id)sender;
- (IBAction)onBtnEffectInLCh13:(id)sender;
- (IBAction)onBtnEffectInLCh14:(id)sender;
- (IBAction)onBtnEffectInLCh15:(id)sender;
- (IBAction)onBtnEffectInLCh16:(id)sender;
- (IBAction)onBtnEffectInLCh17:(id)sender;
- (IBAction)onBtnEffectInLCh18:(id)sender;
- (IBAction)onBtnEffectInLAux1:(id)sender;
- (IBAction)onBtnEffectInLAux2:(id)sender;
- (IBAction)onBtnEffectInLAux3:(id)sender;
- (IBAction)onBtnEffectInLAux4:(id)sender;
- (IBAction)onBtnEffectInLGp1:(id)sender;
- (IBAction)onBtnEffectInLGp2:(id)sender;
- (IBAction)onBtnEffectInLGp3:(id)sender;
- (IBAction)onBtnEffectInLGp4:(id)sender;
- (IBAction)onBtnEffectInLNull:(id)sender;

- (IBAction)onBtnEffectInRCh1:(id)sender;
- (IBAction)onBtnEffectInRCh2:(id)sender;
- (IBAction)onBtnEffectInRCh3:(id)sender;
- (IBAction)onBtnEffectInRCh4:(id)sender;
- (IBAction)onBtnEffectInRCh5:(id)sender;
- (IBAction)onBtnEffectInRCh6:(id)sender;
- (IBAction)onBtnEffectInRCh7:(id)sender;
- (IBAction)onBtnEffectInRCh8:(id)sender;
- (IBAction)onBtnEffectInRCh9:(id)sender;
- (IBAction)onBtnEffectInRCh10:(id)sender;
- (IBAction)onBtnEffectInRCh11:(id)sender;
- (IBAction)onBtnEffectInRCh12:(id)sender;
- (IBAction)onBtnEffectInRCh13:(id)sender;
- (IBAction)onBtnEffectInRCh14:(id)sender;
- (IBAction)onBtnEffectInRCh15:(id)sender;
- (IBAction)onBtnEffectInRCh16:(id)sender;
- (IBAction)onBtnEffectInRCh17:(id)sender;
- (IBAction)onBtnEffectInRCh18:(id)sender;
- (IBAction)onBtnEffectInRAux1:(id)sender;
- (IBAction)onBtnEffectInRAux2:(id)sender;
- (IBAction)onBtnEffectInRAux3:(id)sender;
- (IBAction)onBtnEffectInRAux4:(id)sender;
- (IBAction)onBtnEffectInRGp1:(id)sender;
- (IBAction)onBtnEffectInRGp2:(id)sender;
- (IBAction)onBtnEffectInRGp3:(id)sender;
- (IBAction)onBtnEffectInRGp4:(id)sender;
- (IBAction)onBtnEffectInRNull:(id)sender;

- (IBAction)onBtnEffectOutLMulti1:(id)sender;
- (IBAction)onBtnEffectOutRMulti2:(id)sender;
- (IBAction)onBtnEffectOutLMulti3:(id)sender;
- (IBAction)onBtnEffectOutRMulti4:(id)sender;
- (IBAction)onBtnEffectOutLGp1:(id)sender;
- (IBAction)onBtnEffectOutRGp2:(id)sender;
- (IBAction)onBtnEffectOutLGp3:(id)sender;
- (IBAction)onBtnEffectOutRGp4:(id)sender;
- (IBAction)onBtnEffectOutLMainL:(id)sender;
- (IBAction)onBtnEffectOutRMainR:(id)sender;

- (IBAction)onBtnEffect1ReverbRoom:(id)sender;
- (IBAction)onBtnEffect1ReverbHall:(id)sender;
- (IBAction)onBtnEffect1ReverbPlate:(id)sender;
- (IBAction)onBtnEffect1Echo:(id)sender;
- (IBAction)onBtnEffect1TapDelay:(id)sender;
- (IBAction)onBtnEffect1Chorus:(id)sender;
- (IBAction)onBtnEffect1Flanger:(id)sender;
- (IBAction)onBtnEffect1Phaser:(id)sender;
- (IBAction)onBtnEffect1Tremolo:(id)sender;
- (IBAction)onBtnEffect1Vibrato:(id)sender;
- (IBAction)onBtnEffect1AutoPan:(id)sender;
- (IBAction)onBtnEffect1Geq31:(id)sender;

- (IBAction)onBtnEffect2Echo:(id)sender;
- (IBAction)onBtnEffect2TapDelay:(id)sender;
- (IBAction)onBtnEffect2Chorus:(id)sender;
- (IBAction)onBtnEffect2Flanger:(id)sender;
- (IBAction)onBtnEffect2Phaser:(id)sender;
- (IBAction)onBtnEffect2Tremolo:(id)sender;
- (IBAction)onBtnEffect2Vibrato:(id)sender;
- (IBAction)onBtnEffect2AutoPan:(id)sender;
- (IBAction)onBtnEffect2Geq15:(id)sender;

- (IBAction)onBtnReverbType1:(id)sender;
- (IBAction)onBtnReverbType2:(id)sender;
- (IBAction)onBtnReverbType3:(id)sender;
- (IBAction)onBtnReverbType4:(id)sender;
- (IBAction)onBtnReverbType5:(id)sender;
- (IBAction)onBtnReverbType6:(id)sender;
- (IBAction)onBtnReverbType7:(id)sender;
- (IBAction)onBtnReverbType8:(id)sender;

// Reverb view
- (IBAction)onBtnGate:(id)sender;

// Tap delay view
- (IBAction)onBtnTap:(id)sender;

// Chorus view
- (IBAction)onBtnLfoType:(id)sender;

// Flanger view
- (IBAction)onBtnFlangerLfoType:(id)sender;

// Phaser view
- (IBAction)onBtnPhaserLfoType:(id)sender;

// Tremolo view
- (IBAction)onBtnTremoloLfoType:(id)sender;

// Vibrato view
- (IBAction)onBtnVibratoLfoType:(id)sender;

// AutoPan view
- (IBAction)onBtnAutoPanLfoType:(id)sender;

- (void)setEffectNum:(EFFECT_NUM)num;
- (void)setEffectSolo:(BOOL)value;
- (void)setEffectOnOff:(BOOL)value;
//- (void)setLinkMode:(int)linkMode effect:(EFFECT_NUM)effect;
- (void)processReply:(EffectPagePacket *)pkt;
- (void)setSceneFilename:(NSString*)fileName;

@end
