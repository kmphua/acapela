//
//  GroupSendViewController.m
//  Acapela
//
//  Created by Kevin on 13/1/3.
//  Copyright (c) 2013年 Kevin Phua. All rights reserved.
//

#import "GroupSendViewController.h"
#import "AppDelegate.h"
#import "ViewController.h"
#import "Global.h"
#import "JEProgressView.h"

@interface GroupSendViewController ()

@end

@implementation GroupSendViewController
{
    BOOL    sliderGroupSendMainLock;
    ViewController* viewController;
    int     ch17AudioSetting;
    int     ch18AudioSetting;
    int     soloCtrl;
    int     soloSafeCtrl;
    int     auxGpMaster;
    int     auxGpMeterMaster;
    int     viewMeterCtrl;
    int     multi1Source;
    int     multi2Source;
    int     multi3Source;
    int     multi4Source;
    int     effectCtrl;
    int     gpToMain;
}

@synthesize currentGroup, currentGroupLabel;
@synthesize btnGroupSendPrePostAll, btnGroupSend1, btnGroupSend2, btnGroupSend3, btnGroupSend4, btnGroupSend5, btnGroupSend6;
@synthesize btnGroupSend7, btnGroupSend8, btnGroupSend9, btnGroupSend10, btnGroupSend11, btnGroupSend12, btnGroupSend13;
@synthesize btnGroupSend14, btnGroupSend15, btnGroupSend16, btnGroupSend17, btnGroupSend18;
@synthesize lblGroupSend1, lblGroupSend2, lblGroupSend3, lblGroupSend4, lblGroupSend5, lblGroupSend6, lblGroupSend7, lblGroupSend8;
@synthesize lblGroupSend9, lblGroupSend10, lblGroupSend11, lblGroupSend12, lblGroupSend13, lblGroupSend14, lblGroupSend15, lblGroupSend16, lblGroupSend17, lblGroupSend18;
@synthesize btnGroupSendSolo, btnGroupSendOn, btnGroupSelectDown, btnGroupSelectUp, btnGroupMeter;
@synthesize txtCurrentGroup, lblCurrentGroup, sliderGroupSendMain, meterGroupSendMain, lblSliderGroupMainValue;
@synthesize btnGroupMulti1, btnGroupMulti2, btnGroupMulti3, btnGroupMulti4;
@synthesize btnGroupEfx1OnOff, btnGroupEfx2OnOff, btnGroupToMain;
@synthesize meterGroupSend1, meterGroupSend2, meterGroupSend3, meterGroupSend4, meterGroupSend5;
@synthesize meterGroupSend6, meterGroupSend7, meterGroupSend8, meterGroupSend9, meterGroupSend10;
@synthesize meterGroupSend11, meterGroupSend12, meterGroupSend13, meterGroupSend14, meterGroupSend15, meterGroupSend16;
@synthesize meterGroupSend17, meterGroupSend18;
@synthesize sliderPan, lblPanValue, imgPan, meterPan;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        currentGroup = GROUP_1;
        currentGroupLabel = currentGroup;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    viewController = (ViewController *)appDelegate.window.rootViewController;

    // Do any additional setup after loading the view from its nib.
    [btnGroupMeter.titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
    [btnGroupMeter.titleLabel setNumberOfLines:0];
    [btnGroupMeter.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [btnGroupMeter setTitle:@"METER PRE" forState:UIControlStateNormal];
    [btnGroupMeter setBackgroundImage:[UIImage imageNamed:@"button-3.png"] forState:UIControlStateNormal];
    [btnGroupMeter setTitle:@"METER POST" forState:UIControlStateSelected];
    [btnGroupMeter setBackgroundImage:[UIImage imageNamed:@"button-5.png"] forState:UIControlStateSelected];
    [btnGroupSendPrePostAll.titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
    [btnGroupSendPrePostAll.titleLabel setNumberOfLines:0];
    [btnGroupSendPrePostAll.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [btnGroupSendPrePostAll setTitle:@"METER PRE" forState:UIControlStateNormal];
    [btnGroupSendPrePostAll setBackgroundImage:[UIImage imageNamed:@"button-3.png"] forState:UIControlStateNormal];
    [btnGroupSendPrePostAll setTitle:@"METER POST" forState:UIControlStateSelected];
    [btnGroupSendPrePostAll setBackgroundImage:[UIImage imageNamed:@"button-5.png"] forState:UIControlStateSelected];

    // Add group send slider and meter
    UIImage *faderGroupImage = [UIImage imageNamed:@"fader-5.png"];
    UIImage *meterImage = [[UIImage imageNamed:@"meter-8.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0,0,0,0)];
    UIImage *progressImage = [[UIImage imageNamed:@"meter-1.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0,0,0,0)];
    
    sliderGroupSendMain = [[UISlider alloc] initWithFrame:CGRectMake(571, 295, 301, 180)];
    sliderGroupSendMain.transform = CGAffineTransformRotate(sliderGroupSendMain.transform, 270.0/180*M_PI);
    [sliderGroupSendMain addTarget:self action:@selector(onSliderGroupSendMainBegin:) forControlEvents:UIControlEventTouchDown];
    [sliderGroupSendMain addTarget:self action:@selector(onSliderGroupSendMainEnd:) forControlEvents:UIControlEventTouchUpInside];
    [sliderGroupSendMain addTarget:self action:@selector(onSliderGroupSendMainEnd:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderGroupSendMain addTarget:self action:@selector(onSliderGroupSendMainAction:) forControlEvents:UIControlEventValueChanged];
    [sliderGroupSendMain setBackgroundColor:[UIColor clearColor]];
    [sliderGroupSendMain setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGroupSendMain setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderGroupSendMain setThumbImage:faderGroupImage forState:UIControlStateNormal];
    sliderGroupSendMain.minimumValue = 0.0;
    sliderGroupSendMain.maximumValue = FADER_MAX_VALUE;
    sliderGroupSendMain.continuous = YES;
    sliderGroupSendMain.value = 0.0;
    [self.view addSubview:sliderGroupSendMain];
    
    meterGroupSendMain = [[JEProgressView alloc] initWithFrame:CGRectMake(536, 372, 275, 4)];
    meterGroupSendMain.transform = CGAffineTransformRotate(meterGroupSendMain.transform, 270.0/180*M_PI);
    [meterGroupSendMain setProgressImage:progressImage];
    [meterGroupSendMain setTrackImage:[UIImage alloc]];
    [meterGroupSendMain setProgress:0.0];
    [self.view addSubview:meterGroupSendMain];
    
    // Add group send meters
    meterGroupSend1 = [[JEProgressView alloc] initWithFrame:CGRectMake(-5, 108, 88, 8)];
    meterGroupSend1.transform = CGAffineTransformRotate(meterGroupSend1.transform, 270.0/180*M_PI);
    [meterGroupSend1 setProgressImage:meterImage];
    [meterGroupSend1 setTrackImage:[UIImage alloc]];
    [meterGroupSend1 setProgress:0.0];
    [self.view addSubview:meterGroupSend1];
    
    meterGroupSend2 = [[JEProgressView alloc] initWithFrame:CGRectMake(145, 108, 88, 8)];
    meterGroupSend2.transform = CGAffineTransformRotate(meterGroupSend2.transform, 270.0/180*M_PI);
    [meterGroupSend2 setProgressImage:meterImage];
    [meterGroupSend2 setTrackImage:[UIImage alloc]];
    [meterGroupSend2 setProgress:0.0];
    [self.view addSubview:meterGroupSend2];
    
    meterGroupSend3 = [[JEProgressView alloc] initWithFrame:CGRectMake(295, 108, 88, 8)];
    meterGroupSend3.transform = CGAffineTransformRotate(meterGroupSend3.transform, 270.0/180*M_PI);
    [meterGroupSend3 setProgressImage:meterImage];
    [meterGroupSend3 setTrackImage:[UIImage alloc]];
    [meterGroupSend3 setProgress:0.0];
    [self.view addSubview:meterGroupSend3];
    
    meterGroupSend4 = [[JEProgressView alloc] initWithFrame:CGRectMake(445, 108, 88, 8)];
    meterGroupSend4.transform = CGAffineTransformRotate(meterGroupSend4.transform, 270.0/180*M_PI);
    [meterGroupSend4 setProgressImage:meterImage];
    [meterGroupSend4 setTrackImage:[UIImage alloc]];
    [meterGroupSend4 setProgress:0.0];
    [self.view addSubview:meterGroupSend4];
    
    meterGroupSend5 = [[JEProgressView alloc] initWithFrame:CGRectMake(-5, 218, 88, 8)];
    meterGroupSend5.transform = CGAffineTransformRotate(meterGroupSend5.transform, 270.0/180*M_PI);
    [meterGroupSend5 setProgressImage:meterImage];
    [meterGroupSend5 setTrackImage:[UIImage alloc]];
    [meterGroupSend5 setProgress:0.0];
    [self.view addSubview:meterGroupSend5];
    
    meterGroupSend6 = [[JEProgressView alloc] initWithFrame:CGRectMake(145, 218, 88, 8)];
    meterGroupSend6.transform = CGAffineTransformRotate(meterGroupSend6.transform, 270.0/180*M_PI);
    [meterGroupSend6 setProgressImage:meterImage];
    [meterGroupSend6 setTrackImage:[UIImage alloc]];
    [meterGroupSend6 setProgress:0.0];
    [self.view addSubview:meterGroupSend6];
    
    meterGroupSend7 = [[JEProgressView alloc] initWithFrame:CGRectMake(295, 218, 88, 8)];
    meterGroupSend7.transform = CGAffineTransformRotate(meterGroupSend7.transform, 270.0/180*M_PI);
    [meterGroupSend7 setProgressImage:meterImage];
    [meterGroupSend7 setTrackImage:[UIImage alloc]];
    [meterGroupSend7 setProgress:0.0];
    [self.view addSubview:meterGroupSend7];
    
    meterGroupSend8 = [[JEProgressView alloc] initWithFrame:CGRectMake(445, 218, 88, 8)];
    meterGroupSend8.transform = CGAffineTransformRotate(meterGroupSend8.transform, 270.0/180*M_PI);
    [meterGroupSend8 setProgressImage:meterImage];
    [meterGroupSend8 setTrackImage:[UIImage alloc]];
    [meterGroupSend8 setProgress:0.0];
    [self.view addSubview:meterGroupSend8];
    
    meterGroupSend9 = [[JEProgressView alloc] initWithFrame:CGRectMake(-5, 328, 88, 8)];
    meterGroupSend9.transform = CGAffineTransformRotate(meterGroupSend9.transform, 270.0/180*M_PI);
    [meterGroupSend9 setProgressImage:meterImage];
    [meterGroupSend9 setTrackImage:[UIImage alloc]];
    [meterGroupSend9 setProgress:0.0];
    [self.view addSubview:meterGroupSend9];
    
    meterGroupSend10 = [[JEProgressView alloc] initWithFrame:CGRectMake(145, 328, 88, 8)];
    meterGroupSend10.transform = CGAffineTransformRotate(meterGroupSend10.transform, 270.0/180*M_PI);
    [meterGroupSend10 setProgressImage:meterImage];
    [meterGroupSend10 setTrackImage:[UIImage alloc]];
    [meterGroupSend10 setProgress:0.0];
    [self.view addSubview:meterGroupSend10];
    
    meterGroupSend11 = [[JEProgressView alloc] initWithFrame:CGRectMake(295, 328, 88, 8)];
    meterGroupSend11.transform = CGAffineTransformRotate(meterGroupSend11.transform, 270.0/180*M_PI);
    [meterGroupSend11 setProgressImage:meterImage];
    [meterGroupSend11 setTrackImage:[UIImage alloc]];
    [meterGroupSend11 setProgress:0.0];
    [self.view addSubview:meterGroupSend11];
    
    meterGroupSend12 = [[JEProgressView alloc] initWithFrame:CGRectMake(445, 328, 88, 8)];
    meterGroupSend12.transform = CGAffineTransformRotate(meterGroupSend12.transform, 270.0/180*M_PI);
    [meterGroupSend12 setProgressImage:meterImage];
    [meterGroupSend12 setTrackImage:[UIImage alloc]];
    [meterGroupSend12 setProgress:0.0];
    [self.view addSubview:meterGroupSend12];
    
    meterGroupSend13 = [[JEProgressView alloc] initWithFrame:CGRectMake(-5, 438, 88, 8)];
    meterGroupSend13.transform = CGAffineTransformRotate(meterGroupSend13.transform, 270.0/180*M_PI);
    [meterGroupSend13 setProgressImage:meterImage];
    [meterGroupSend13 setTrackImage:[UIImage alloc]];
    [meterGroupSend13 setProgress:0.0];
    [self.view addSubview:meterGroupSend13];
    
    meterGroupSend14 = [[JEProgressView alloc] initWithFrame:CGRectMake(145, 438, 88, 8)];
    meterGroupSend14.transform = CGAffineTransformRotate(meterGroupSend14.transform, 270.0/180*M_PI);
    [meterGroupSend14 setProgressImage:meterImage];
    [meterGroupSend14 setTrackImage:[UIImage alloc]];
    [meterGroupSend14 setProgress:0.0];
    [self.view addSubview:meterGroupSend14];
    
    meterGroupSend15 = [[JEProgressView alloc] initWithFrame:CGRectMake(295, 438, 88, 8)];
    meterGroupSend15.transform = CGAffineTransformRotate(meterGroupSend15.transform, 270.0/180*M_PI);
    [meterGroupSend15 setProgressImage:meterImage];
    [meterGroupSend15 setTrackImage:[UIImage alloc]];
    [meterGroupSend15 setProgress:0.0];
    [self.view addSubview:meterGroupSend15];
    
    meterGroupSend16 = [[JEProgressView alloc] initWithFrame:CGRectMake(445, 438, 88, 8)];
    meterGroupSend16.transform = CGAffineTransformRotate(meterGroupSend16.transform, 270.0/180*M_PI);
    [meterGroupSend16 setProgressImage:meterImage];
    [meterGroupSend16 setTrackImage:[UIImage alloc]];
    [meterGroupSend16 setProgress:0.0];
    [self.view addSubview:meterGroupSend16];
    
    meterGroupSend17 = [[JEProgressView alloc] initWithFrame:CGRectMake(-5, 548, 88, 8)];
    meterGroupSend17.transform = CGAffineTransformRotate(meterGroupSend17.transform, 270.0/180*M_PI);
    [meterGroupSend17 setProgressImage:meterImage];
    [meterGroupSend17 setTrackImage:[UIImage alloc]];
    [meterGroupSend17 setProgress:0.0];
    [self.view addSubview:meterGroupSend17];
    
    meterGroupSend18 = [[JEProgressView alloc] initWithFrame:CGRectMake(145, 548, 88, 8)];
    meterGroupSend18.transform = CGAffineTransformRotate(meterGroupSend18.transform, 270.0/180*M_PI);
    [meterGroupSend18 setProgressImage:meterImage];
    [meterGroupSend18 setTrackImage:[UIImage alloc]];
    [meterGroupSend18 setProgress:0.0];
    [self.view addSubview:meterGroupSend18];
    
    // Add slider meter
    UIImage *panProgressImage = [[UIImage imageNamed:@"pan-6.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0,0,0,0)];
    
    meterPan = [[JEProgressView alloc] initWithFrame:CGRectMake(670, 40, 82, 82)];
    [meterPan setTransform:CGAffineTransformMake(-1, 0, 0, 1, 0, 0)];
    [meterPan setProgressImage:panProgressImage];
    [meterPan setTrackImage:[UIImage alloc]];
    [meterPan setProgress:0.0];
    [self.view addSubview:meterPan];

    // Add PAN slider
    UIImage *panImage = [UIImage imageNamed:@"Slider-1.png"];
    
    sliderPan = [[UISlider alloc] initWithFrame:CGRectMake(668, 12, 86, 86)];
    [sliderPan addTarget:self action:@selector(onSliderPanAction:) forControlEvents:UIControlEventValueChanged];
    [sliderPan addTarget:self action:@selector(onSliderPanStart:) forControlEvents:UIControlEventTouchDown];
    [sliderPan addTarget:self action:@selector(onSliderPanEnd:) forControlEvents:UIControlEventTouchUpInside];
    [sliderPan addTarget:self action:@selector(onSliderPanEnd:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderPan setBackgroundColor:[UIColor clearColor]];
    [sliderPan setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderPan setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderPan setThumbImage:panImage forState:UIControlStateNormal];
    sliderPan.minimumValue = 0.0;
    sliderPan.maximumValue = PAN_MAX_VALUE;
    sliderPan.continuous = YES;
    sliderPan.value = PAN_CENTER_VALUE;
    [self.view addSubview:sliderPan];
        
    [self updateCurrentGroupLabel];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)sendGroupChannel {
    SInt16 gpSendValue = btnGroupSend1.selected + (btnGroupSend2.selected << 1) +
                        (btnGroupSend3.selected << 2) + (btnGroupSend4.selected << 3) +
                        (btnGroupSend5.selected << 4) + (btnGroupSend6.selected << 5) +
                        (btnGroupSend7.selected << 6) + (btnGroupSend8.selected << 7) +
                        (btnGroupSend9.selected << 8) + (btnGroupSend10.selected << 9) +
                        (btnGroupSend11.selected << 10) + (btnGroupSend12.selected << 11) +
                        (btnGroupSend13.selected << 12) + (btnGroupSend14.selected << 13) +
                        (btnGroupSend15.selected << 14) + (btnGroupSend16.selected << 15);
    
    switch (currentGroup) {
        case GROUP_1:
            [viewController sendData:CMD_GP1_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:gpSendValue];
            break;
        case GROUP_2:
            [viewController sendData:CMD_GP2_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:gpSendValue];
            break;
        case GROUP_3:
            [viewController sendData:CMD_GP3_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:gpSendValue];
            break;
        case GROUP_4:
            [viewController sendData:CMD_GP4_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:gpSendValue];
            break;
        default:
            break;
    }
}

- (void)refreshPanValues {
    if (sliderPan.value >= 0.0 && sliderPan.value < PAN_CENTER_VALUE) {
        [meterPan setHidden:NO];
        [meterPan setTransform:CGAffineTransformMake(-1, 0, 0, 1, 0, 0)];
        [meterPan setProgress:1.0 - sliderPan.value/PAN_MAX_VALUE];
    } else if (sliderPan.value > PAN_CENTER_VALUE && sliderPan.value <= PAN_MAX_VALUE) {
        [meterPan setHidden:NO];
        [meterPan setTransform:CGAffineTransformMake(1, 0, 0, 1, 0, 0)];
        [meterPan setProgress:sliderPan.value/PAN_MAX_VALUE];
    } else {
        [meterPan setHidden:YES];
        [imgPan setImage:[UIImage imageNamed:@"pan-2.png"]];
    }
    [lblPanValue setText:[Global getPanString:sliderPan.value]];
}

- (void)updateCurrentGroupLabel
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    switch (currentGroup) {
        case GROUP_1:
            if ([userDefaults objectForKey:@"Gp1"] != nil) {
                [txtCurrentGroup setText:(NSString *)[userDefaults objectForKey:@"Gp1"]];
            } else {
                [txtCurrentGroup setText:NSLocalizedStringFromTable(@"Gp1", @"Acapela", nil)];
            }
            [lblCurrentGroup setText:NSLocalizedStringFromTable(@"Gp1", @"Acapela", nil)];
            break;
        case GROUP_2:
            if ([userDefaults objectForKey:@"Gp2"] != nil) {
                [txtCurrentGroup setText:(NSString *)[userDefaults objectForKey:@"Gp2"]];
            } else {
                [txtCurrentGroup setText:NSLocalizedStringFromTable(@"Gp2", @"Acapela", nil)];
            }
            [lblCurrentGroup setText:NSLocalizedStringFromTable(@"Gp2", @"Acapela", nil)];
            break;
        case GROUP_3:
            if ([userDefaults objectForKey:@"Gp3"] != nil) {
                [txtCurrentGroup setText:(NSString *)[userDefaults objectForKey:@"Gp3"]];
            } else {
                [txtCurrentGroup setText:NSLocalizedStringFromTable(@"Gp3", @"Acapela", nil)];
            }
            [lblCurrentGroup setText:NSLocalizedStringFromTable(@"Gp3", @"Acapela", nil)];
            break;
        case GROUP_4:
            if ([userDefaults objectForKey:@"Gp4"] != nil) {
                [txtCurrentGroup setText:(NSString *)[userDefaults objectForKey:@"Gp4"]];
            } else {
                [txtCurrentGroup setText:NSLocalizedStringFromTable(@"Gp4", @"Acapela", nil)];
            }
            [lblCurrentGroup setText:NSLocalizedStringFromTable(@"Gp4", @"Acapela", nil)];
            break;
        default:
            break;
    }
}

- (void)refreshGroupSendButtons1_8:(unsigned char)value
{
    BOOL groupSend1 = value & 0x01;
    if (groupSend1 != btnGroupSend1.selected) {
        btnGroupSend1.selected = groupSend1;
        if (btnGroupSend1.selected) {
            [lblGroupSend1 setTextColor:[UIColor whiteColor]];
            [lblGroupSend1 setText:@"ON"];
        } else {
            [lblGroupSend1 setTextColor:[UIColor blackColor]];
            [lblGroupSend1 setText:@"OFF"];
        }
    }
    
    BOOL groupSend2 = (value & 0x02) >> 1;
    if (groupSend2 != btnGroupSend2.selected) {
        btnGroupSend2.selected = groupSend2;
        if (btnGroupSend2.selected) {
            [lblGroupSend2 setTextColor:[UIColor whiteColor]];
            [lblGroupSend2 setText:@"ON"];
        } else {
            [lblGroupSend2 setTextColor:[UIColor blackColor]];
            [lblGroupSend2 setText:@"OFF"];
        }
    }
    
    BOOL groupSend3 = (value & 0x04) >> 2;
    if (groupSend3 != btnGroupSend3.selected) {
        btnGroupSend3.selected = groupSend3;
        if (btnGroupSend3.selected) {
            [lblGroupSend3 setTextColor:[UIColor whiteColor]];
            [lblGroupSend3 setText:@"ON"];
        } else {
            [lblGroupSend3 setTextColor:[UIColor blackColor]];
            [lblGroupSend3 setText:@"OFF"];
        }
    }
    
    BOOL groupSend4 = (value & 0x08) >> 3;
    if (groupSend4 != btnGroupSend4.selected) {
        btnGroupSend4.selected = groupSend4;
        if (btnGroupSend4.selected) {
            [lblGroupSend4 setTextColor:[UIColor whiteColor]];
            [lblGroupSend4 setText:@"ON"];
        } else {
            [lblGroupSend4 setTextColor:[UIColor blackColor]];
            [lblGroupSend4 setText:@"OFF"];
        }
    }
    
    BOOL groupSend5 = (value & 0x10) >> 4;
    if (groupSend5 != btnGroupSend5.selected) {
        btnGroupSend5.selected = groupSend5;
        if (btnGroupSend5.selected) {
            [lblGroupSend5 setTextColor:[UIColor whiteColor]];
            [lblGroupSend5 setText:@"ON"];
        } else {
            [lblGroupSend5 setTextColor:[UIColor blackColor]];
            [lblGroupSend5 setText:@"OFF"];
        }
    }
    
    BOOL groupSend6 = (value & 0x20) >> 5;
    if (groupSend6 != btnGroupSend6.selected) {
        btnGroupSend6.selected = groupSend6;
        if (btnGroupSend6.selected) {
            [lblGroupSend6 setTextColor:[UIColor whiteColor]];
            [lblGroupSend6 setText:@"ON"];
        } else {
            [lblGroupSend6 setTextColor:[UIColor blackColor]];
            [lblGroupSend6 setText:@"OFF"];
        }
    }
    
    BOOL groupSend7 = (value & 0x40) >> 6;
    if (groupSend7 != btnGroupSend7.selected) {
        btnGroupSend7.selected = groupSend7;
        if (btnGroupSend7.selected) {
            [lblGroupSend7 setTextColor:[UIColor whiteColor]];
            [lblGroupSend7 setText:@"ON"];
        } else {
            [lblGroupSend7 setTextColor:[UIColor blackColor]];
            [lblGroupSend7 setText:@"OFF"];
        }
    }
    
    BOOL groupSend8 = (value & 0x80) >> 7;
    if (groupSend8 != btnGroupSend8.selected) {
        btnGroupSend8.selected = groupSend8;
        if (btnGroupSend8.selected) {
            [lblGroupSend8 setTextColor:[UIColor whiteColor]];
            [lblGroupSend8 setText:@"ON"];
        } else {
            [lblGroupSend8 setTextColor:[UIColor blackColor]];
            [lblGroupSend8 setText:@"OFF"];
        }
    }
}

- (void)refreshGroupSendButtons9_16:(unsigned char)value
{
    BOOL groupSend9 = value & 0x01;
    if (groupSend9 != btnGroupSend9.selected) {
        btnGroupSend9.selected = groupSend9;
        if (btnGroupSend9.selected) {
            [lblGroupSend9 setTextColor:[UIColor whiteColor]];
            [lblGroupSend9 setText:@"ON"];
        } else {
            [lblGroupSend9 setTextColor:[UIColor blackColor]];
            [lblGroupSend9 setText:@"OFF"];
        }
    }
    
    BOOL groupSend10 = (value & 0x02) >> 1;
    if (groupSend10 != btnGroupSend10.selected) {
        btnGroupSend10.selected = groupSend10;
        if (btnGroupSend10.selected) {
            [lblGroupSend10 setTextColor:[UIColor whiteColor]];
            [lblGroupSend10 setText:@"ON"];
        } else {
            [lblGroupSend10 setTextColor:[UIColor blackColor]];
            [lblGroupSend10 setText:@"OFF"];
        }
    }
    
    BOOL groupSend11 = (value & 0x04) >> 2;
    if (groupSend11 != btnGroupSend11.selected) {
        btnGroupSend11.selected = groupSend11;
        if (btnGroupSend11.selected) {
            [lblGroupSend11 setTextColor:[UIColor whiteColor]];
            [lblGroupSend11 setText:@"ON"];
        } else {
            [lblGroupSend11 setTextColor:[UIColor blackColor]];
            [lblGroupSend11 setText:@"OFF"];
        }
    }
    
    BOOL groupSend12 = (value & 0x08) >> 3;
    if (groupSend12 != btnGroupSend12.selected) {
        btnGroupSend12.selected = groupSend12;
        if (btnGroupSend12.selected) {
            [lblGroupSend12 setTextColor:[UIColor whiteColor]];
            [lblGroupSend12 setText:@"ON"];
        } else {
            [lblGroupSend12 setTextColor:[UIColor blackColor]];
            [lblGroupSend12 setText:@"OFF"];
        }
    }
    
    BOOL groupSend13 = (value & 0x10) >> 4;
    if (groupSend13 != btnGroupSend13.selected) {
        btnGroupSend13.selected = groupSend13;
        if (btnGroupSend13.selected) {
            [lblGroupSend13 setTextColor:[UIColor whiteColor]];
            [lblGroupSend13 setText:@"ON"];
        } else {
            [lblGroupSend13 setTextColor:[UIColor blackColor]];
            [lblGroupSend13 setText:@"OFF"];
        }
    }
    
    BOOL groupSend14 = (value & 0x20) >> 5;
    if (groupSend14 != btnGroupSend14.selected) {
        btnGroupSend14.selected = groupSend14;
        if (btnGroupSend14.selected) {
            [lblGroupSend14 setTextColor:[UIColor whiteColor]];
            [lblGroupSend14 setText:@"ON"];
        } else {
            [lblGroupSend14 setTextColor:[UIColor blackColor]];
            [lblGroupSend14 setText:@"OFF"];
        }
    }
    
    BOOL groupSend15 = (value & 0x40) >> 6;
    if (groupSend15 != btnGroupSend15.selected) {
        btnGroupSend15.selected = groupSend15;
        if (btnGroupSend15.selected) {
            [lblGroupSend15 setTextColor:[UIColor whiteColor]];
            [lblGroupSend15 setText:@"ON"];
        } else {
            [lblGroupSend15 setTextColor:[UIColor blackColor]];
            [lblGroupSend15 setText:@"OFF"];
        }
    }
    
    BOOL groupSend16 = (value & 0x80) >> 7;
    if (groupSend16 != btnGroupSend16.selected) {
        btnGroupSend16.selected = groupSend16;
        if (btnGroupSend16.selected) {
            [lblGroupSend16 setTextColor:[UIColor whiteColor]];
            [lblGroupSend16 setText:@"ON"];
        } else {
            [lblGroupSend16 setTextColor:[UIColor blackColor]];
            [lblGroupSend16 setText:@"OFF"];
        }
    }
}

- (void)processReply:(GpPagePacket *)pkt
{
    soloCtrl = pkt->solo_ctrl;
    soloSafeCtrl = pkt->solo_safe_ctrl;;
    auxGpMaster = pkt->aux_gp_master;
    auxGpMeterMaster = pkt->aux_gp_meter_master;
    viewMeterCtrl = pkt->view_meter_control;
    multi1Source = pkt->multi_1_source;
    multi2Source = pkt->multi_2_source;
    multi3Source = pkt->multi_3_source;
    multi4Source = pkt->multi_4_source;
    effectCtrl = pkt->effect_control;
    gpToMain = pkt->gp_to_main;
    ch17AudioSetting = pkt->ch_17_audio_setting;
    ch18AudioSetting = pkt->ch_18_audio_setting;
    
    switch (currentGroup) {
        case GROUP_1:
            meterGroupSend1.progress = ((float)pkt->view_gp_1_ch_1_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend2.progress = ((float)pkt->view_gp_1_ch_2_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend3.progress = ((float)pkt->view_gp_1_ch_3_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend4.progress = ((float)pkt->view_gp_1_ch_4_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend5.progress = ((float)pkt->view_gp_1_ch_5_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend6.progress = ((float)pkt->view_gp_1_ch_6_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend7.progress = ((float)pkt->view_gp_1_ch_7_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend8.progress = ((float)pkt->view_gp_1_ch_8_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend9.progress = ((float)pkt->view_gp_1_ch_9_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend10.progress = ((float)pkt->view_gp_1_ch_10_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend11.progress = ((float)pkt->view_gp_1_ch_11_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend12.progress = ((float)pkt->view_gp_1_ch_12_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend13.progress = ((float)pkt->view_gp_1_ch_13_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend14.progress = ((float)pkt->view_gp_1_ch_14_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend15.progress = ((float)pkt->view_gp_1_ch_15_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend16.progress = ((float)pkt->view_gp_1_ch_16_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend17.progress = ((float)pkt->view_gp_1_ch_17_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend18.progress = ((float)pkt->view_gp_1_ch_18_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            if (![viewController sendCommandExists:CMD_GP1_ASSIGN dspId:DSP_5]) {
                [self refreshGroupSendButtons1_8:(pkt->gp_1_assign & 0x00FF)];
                [self refreshGroupSendButtons9_16:((pkt->gp_1_assign & 0xFF00) >> 8)];
            }
            if (![viewController sendCommandExists:CMD_CH17_USB_AUDIO_SETTING dspId:DSP_5]) {
                btnGroupSend17.selected = pkt->ch_17_audio_setting & 0x01;
                if (btnGroupSend17.selected) {
                    [lblGroupSend17 setTextColor:[UIColor whiteColor]];
                    [lblGroupSend17 setText:@"ON"];
                } else {
                    [lblGroupSend17 setTextColor:[UIColor blackColor]];
                    [lblGroupSend17 setText:@"OFF"];
                }
            }
            if (![viewController sendCommandExists:CMD_CH18_USB_AUDIO_SETTING dspId:DSP_5]) {
                btnGroupSend18.selected = pkt->ch_18_audio_setting & 0x01;
                if (btnGroupSend18.selected) {
                    [lblGroupSend18 setTextColor:[UIColor whiteColor]];
                    [lblGroupSend18 setText:@"ON"];
                } else {
                    [lblGroupSend18 setTextColor:[UIColor blackColor]];
                    [lblGroupSend18 setText:@"OFF"];
                }
            }
            if (![viewController sendCommandExists:CMD_AUXGP_SOLO_CTRL dspId:DSP_5]) {
                btnGroupSendSolo.selected = pkt->solo_ctrl & 0x01;
            }
            BOOL group1SoloSafe = pkt->solo_safe_ctrl & 0x01;
            if (group1SoloSafe) {
                [btnGroupSendSolo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
                [btnGroupSendSolo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
            } else {
                [btnGroupSendSolo setTitle:@"SOLO" forState:UIControlStateNormal];
                [btnGroupSendSolo setTitle:@"SOLO" forState:UIControlStateSelected];
            }
            if (![viewController sendCommandExists:CMD_AUXGP_ON_OFF dspId:DSP_5]) {
                btnGroupSendOn.selected = pkt->aux_gp_master & 0x01;
            }
            if (![viewController sendCommandExists:CMD_AUXGP_METER dspId:DSP_5]) {
                btnGroupMeter.selected = pkt->aux_gp_meter_master & 0x01;
            }
            if (!sliderGroupSendMainLock) {
                sliderGroupSendMain.value = pkt->gp_1_master_fader;
                [self onSliderGroupSendMainAction:nil];
            }
            if (![viewController sendCommandExists:CMD_VIEW_METER_CONTROL dspId:DSP_5]) {
                btnGroupSendPrePostAll.selected = (pkt->view_meter_control & 0x08) >> 3;
            }
            sliderPan.value = pkt->gp_1_master_pan + PAN_CENTER_VALUE;
            [self onSliderPanAction:nil];
            if (![viewController sendCommandExists:CMD_MULTI1_SOURCE dspId:DSP_5]) {
                btnGroupMulti1.selected = pkt->multi_1_source & 0x01;
            }
            if (![viewController sendCommandExists:CMD_MULTI2_SOURCE dspId:DSP_5]) {
                btnGroupMulti2.selected = pkt->multi_2_source & 0x01;
            }
            if (![viewController sendCommandExists:CMD_MULTI3_SOURCE dspId:DSP_5]) {
                btnGroupMulti3.selected = pkt->multi_3_source & 0x01;
            }
            if (![viewController sendCommandExists:CMD_MULTI4_SOURCE dspId:DSP_5]) {
                btnGroupMulti4.selected = pkt->multi_4_source & 0x01;
            }
            btnGroupEfx1OnOff.hidden = !(pkt->effect_1_1_out_source & 0x01);
            btnGroupEfx2OnOff.hidden = !(pkt->effect_2_1_out_source & 0x01);
            if (![viewController sendCommandExists:CMD_EFFECT_CONTROL dspId:DSP_5]) {
                btnGroupEfx1OnOff.selected = pkt->effect_control & 0x01;
                btnGroupEfx2OnOff.selected = (pkt->effect_control & 0x02) >> 1;
            }
            if (![viewController sendCommandExists:CMD_GP_TO_MAIN_CTRL dspId:DSP_5]) {
                btnGroupToMain.selected = pkt->gp_to_main & 0x01;
            }
            break;
            
        case GROUP_2:
            meterGroupSend1.progress = ((float)pkt->view_gp_2_ch_1_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend2.progress = ((float)pkt->view_gp_2_ch_2_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend3.progress = ((float)pkt->view_gp_2_ch_3_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend4.progress = ((float)pkt->view_gp_2_ch_4_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend5.progress = ((float)pkt->view_gp_2_ch_5_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend6.progress = ((float)pkt->view_gp_2_ch_6_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend7.progress = ((float)pkt->view_gp_2_ch_7_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend8.progress = ((float)pkt->view_gp_2_ch_8_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend9.progress = ((float)pkt->view_gp_2_ch_9_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend10.progress = ((float)pkt->view_gp_2_ch_10_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend11.progress = ((float)pkt->view_gp_2_ch_11_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend12.progress = ((float)pkt->view_gp_2_ch_12_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend13.progress = ((float)pkt->view_gp_2_ch_13_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend14.progress = ((float)pkt->view_gp_2_ch_14_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend15.progress = ((float)pkt->view_gp_2_ch_15_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend16.progress = ((float)pkt->view_gp_2_ch_16_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend17.progress = ((float)pkt->view_gp_2_ch_17_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend18.progress = ((float)pkt->view_gp_2_ch_18_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            if (![viewController sendCommandExists:CMD_GP2_ASSIGN dspId:DSP_5]) {
                [self refreshGroupSendButtons1_8:(pkt->gp_2_assign & 0x00FF)];
                [self refreshGroupSendButtons9_16:((pkt->gp_2_assign & 0xFF00) >> 8)];
            }
            if (![viewController sendCommandExists:CMD_CH17_USB_AUDIO_SETTING dspId:DSP_5]) {
                btnGroupSend17.selected = (pkt->ch_17_audio_setting & 0x02) >> 1;
                if (btnGroupSend17.selected) {
                    [lblGroupSend17 setTextColor:[UIColor whiteColor]];
                    [lblGroupSend17 setText:@"ON"];
                } else {
                    [lblGroupSend17 setTextColor:[UIColor blackColor]];
                    [lblGroupSend17 setText:@"OFF"];
                }
            }
            if (![viewController sendCommandExists:CMD_CH18_USB_AUDIO_SETTING dspId:DSP_5]) {
                btnGroupSend18.selected = (pkt->ch_18_audio_setting & 0x02) >> 1;
                if (btnGroupSend18.selected) {
                    [lblGroupSend18 setTextColor:[UIColor whiteColor]];
                    [lblGroupSend18 setText:@"ON"];
                } else {
                    [lblGroupSend18 setTextColor:[UIColor blackColor]];
                    [lblGroupSend18 setText:@"OFF"];
                }
            }
            if (![viewController sendCommandExists:CMD_AUXGP_SOLO_CTRL dspId:DSP_5]) {
                btnGroupSendSolo.selected = (pkt->solo_ctrl & 0x02) >> 1;
            }
            BOOL group2SoloSafe = (pkt->solo_safe_ctrl & 0x02) >> 1;
            if (group2SoloSafe) {
                [btnGroupSendSolo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
                [btnGroupSendSolo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
            } else {
                [btnGroupSendSolo setTitle:@"SOLO" forState:UIControlStateNormal];
                [btnGroupSendSolo setTitle:@"SOLO" forState:UIControlStateSelected];
            }
            if (![viewController sendCommandExists:CMD_AUXGP_ON_OFF dspId:DSP_5]) {
                btnGroupSendOn.selected = (pkt->aux_gp_master & 0x02) >> 1;
            }
            if (![viewController sendCommandExists:CMD_AUXGP_METER dspId:DSP_5]) {
                btnGroupMeter.selected = (pkt->aux_gp_meter_master & 0x02) >> 1;
            }
            if (!sliderGroupSendMainLock) {
                sliderGroupSendMain.value = pkt->gp_2_master_fader;
                [self onSliderGroupSendMainAction:nil];
            }
            if (![viewController sendCommandExists:CMD_VIEW_METER_CONTROL dspId:DSP_5]) {
                btnGroupSendPrePostAll.selected = (pkt->view_meter_control & 0x08) >> 3;
            }
            sliderPan.value = pkt->gp_2_master_pan + PAN_CENTER_VALUE;
            [self onSliderPanAction:nil];
            if (![viewController sendCommandExists:CMD_MULTI1_SOURCE dspId:DSP_5]) {
                btnGroupMulti1.selected = (pkt->multi_1_source & 0x02) >> 1;
            }
            if (![viewController sendCommandExists:CMD_MULTI2_SOURCE dspId:DSP_5]) {
                btnGroupMulti2.selected = (pkt->multi_2_source & 0x02) >> 1;
            }
            if (![viewController sendCommandExists:CMD_MULTI3_SOURCE dspId:DSP_5]) {
                btnGroupMulti3.selected = (pkt->multi_3_source & 0x02) >> 1;
            }
            if (![viewController sendCommandExists:CMD_MULTI4_SOURCE dspId:DSP_5]) {
                btnGroupMulti4.selected = (pkt->multi_4_source & 0x02) >> 1;
            }
            btnGroupEfx1OnOff.hidden = !(pkt->effect_1_2_out_source & 0x01);
            btnGroupEfx2OnOff.hidden = !(pkt->effect_2_2_out_source & 0x01);
            if (![viewController sendCommandExists:CMD_EFFECT_CONTROL dspId:DSP_5]) {
                btnGroupEfx1OnOff.selected = pkt->effect_control & 0x01;
                btnGroupEfx2OnOff.selected = (pkt->effect_control & 0x02) >> 1;
            }
            if (![viewController sendCommandExists:CMD_GP_TO_MAIN_CTRL dspId:DSP_5]) {
                btnGroupToMain.selected = (pkt->gp_to_main & 0x02) >> 1;
            }
            break;
            
        case GROUP_3:
            meterGroupSend1.progress = ((float)pkt->view_gp_3_ch_1_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend2.progress = ((float)pkt->view_gp_3_ch_2_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend3.progress = ((float)pkt->view_gp_3_ch_3_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend4.progress = ((float)pkt->view_gp_3_ch_4_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend5.progress = ((float)pkt->view_gp_3_ch_5_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend6.progress = ((float)pkt->view_gp_3_ch_6_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend7.progress = ((float)pkt->view_gp_3_ch_7_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend8.progress = ((float)pkt->view_gp_3_ch_8_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend9.progress = ((float)pkt->view_gp_3_ch_9_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend10.progress = ((float)pkt->view_gp_3_ch_10_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend11.progress = ((float)pkt->view_gp_3_ch_11_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend12.progress = ((float)pkt->view_gp_3_ch_12_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend13.progress = ((float)pkt->view_gp_3_ch_13_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend14.progress = ((float)pkt->view_gp_3_ch_14_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend15.progress = ((float)pkt->view_gp_3_ch_15_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend16.progress = ((float)pkt->view_gp_3_ch_16_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend17.progress = ((float)pkt->view_gp_3_ch_17_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend18.progress = ((float)pkt->view_gp_3_ch_18_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            if (![viewController sendCommandExists:CMD_GP3_ASSIGN dspId:DSP_5]) {
                [self refreshGroupSendButtons1_8:(pkt->gp_3_assign & 0x00FF)];
                [self refreshGroupSendButtons9_16:((pkt->gp_3_assign & 0xFF00) >> 8)];
            }
            if (![viewController sendCommandExists:CMD_CH17_USB_AUDIO_SETTING dspId:DSP_5]) {
                btnGroupSend17.selected = (pkt->ch_17_audio_setting & 0x04) >> 2;
                if (btnGroupSend17.selected) {
                    [lblGroupSend17 setTextColor:[UIColor whiteColor]];
                    [lblGroupSend17 setText:@"ON"];
                } else {
                    [lblGroupSend17 setTextColor:[UIColor blackColor]];
                    [lblGroupSend17 setText:@"OFF"];
                }
            }
            if (![viewController sendCommandExists:CMD_CH18_USB_AUDIO_SETTING dspId:DSP_5]) {
                btnGroupSend18.selected = (pkt->ch_18_audio_setting & 0x04) >> 2;
                if (btnGroupSend18.selected) {
                    [lblGroupSend18 setTextColor:[UIColor whiteColor]];
                    [lblGroupSend18 setText:@"ON"];
                } else {
                    [lblGroupSend18 setTextColor:[UIColor blackColor]];
                    [lblGroupSend18 setText:@"OFF"];
                }
            }
            if (![viewController sendCommandExists:CMD_AUXGP_SOLO_CTRL dspId:DSP_5]) {
                btnGroupSendSolo.selected = (pkt->solo_ctrl & 0x04) >> 2;
            }
            BOOL group3SoloSafe = (pkt->solo_safe_ctrl & 0x04) >> 2;
            if (group3SoloSafe) {
                [btnGroupSendSolo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
                [btnGroupSendSolo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
            } else {
                [btnGroupSendSolo setTitle:@"SOLO" forState:UIControlStateNormal];
                [btnGroupSendSolo setTitle:@"SOLO" forState:UIControlStateSelected];
            }
            if (![viewController sendCommandExists:CMD_AUXGP_ON_OFF dspId:DSP_5]) {
                btnGroupSendOn.selected = (pkt->aux_gp_master & 0x04) >> 2;
            }
            if (![viewController sendCommandExists:CMD_AUXGP_METER dspId:DSP_5]) {
                btnGroupMeter.selected = (pkt->aux_gp_meter_master & 0x04) >> 2;
            }
            if (!sliderGroupSendMainLock) {
                sliderGroupSendMain.value = pkt->gp_3_master_fader;
                [self onSliderGroupSendMainAction:nil];
            }
            if (![viewController sendCommandExists:CMD_VIEW_METER_CONTROL dspId:DSP_5]) {
                btnGroupSendPrePostAll.selected = (pkt->view_meter_control & 0x08) >> 3;
            }
            sliderPan.value = pkt->gp_3_master_pan + PAN_CENTER_VALUE;
            [self onSliderPanAction:nil];
            if (![viewController sendCommandExists:CMD_MULTI1_SOURCE dspId:DSP_5]) {
                btnGroupMulti1.selected = (pkt->multi_1_source & 0x04) >> 2;
            }
            if (![viewController sendCommandExists:CMD_MULTI2_SOURCE dspId:DSP_5]) {
                btnGroupMulti2.selected = (pkt->multi_2_source & 0x04) >> 2;
            }
            if (![viewController sendCommandExists:CMD_MULTI3_SOURCE dspId:DSP_5]) {
                btnGroupMulti3.selected = (pkt->multi_3_source & 0x04) >> 2;
            }
            if (![viewController sendCommandExists:CMD_MULTI4_SOURCE dspId:DSP_5]) {
                btnGroupMulti4.selected = (pkt->multi_4_source & 0x04) >> 2;
            }
            btnGroupEfx1OnOff.hidden = !((pkt->effect_1_1_out_source & 0x02) >> 1);
            btnGroupEfx2OnOff.hidden = !((pkt->effect_2_1_out_source & 0x02) >> 1);
            if (![viewController sendCommandExists:CMD_EFFECT_CONTROL dspId:DSP_5]) {
                btnGroupEfx1OnOff.selected = pkt->effect_control & 0x01;
                btnGroupEfx2OnOff.selected = (pkt->effect_control & 0x02) >> 1;
            }
            if (![viewController sendCommandExists:CMD_GP_TO_MAIN_CTRL dspId:DSP_5]) {
                btnGroupToMain.selected = (pkt->gp_to_main & 0x04) >> 2;
            }
            break;
            
        case GROUP_4:
            meterGroupSend1.progress = ((float)pkt->view_gp_4_ch_1_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend2.progress = ((float)pkt->view_gp_4_ch_2_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend3.progress = ((float)pkt->view_gp_4_ch_3_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend4.progress = ((float)pkt->view_gp_4_ch_4_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend5.progress = ((float)pkt->view_gp_4_ch_5_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend6.progress = ((float)pkt->view_gp_4_ch_6_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend7.progress = ((float)pkt->view_gp_4_ch_7_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend8.progress = ((float)pkt->view_gp_4_ch_8_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend9.progress = ((float)pkt->view_gp_4_ch_9_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend10.progress = ((float)pkt->view_gp_4_ch_10_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend11.progress = ((float)pkt->view_gp_4_ch_11_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend12.progress = ((float)pkt->view_gp_4_ch_12_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend13.progress = ((float)pkt->view_gp_4_ch_13_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend14.progress = ((float)pkt->view_gp_4_ch_14_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend15.progress = ((float)pkt->view_gp_4_ch_15_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend16.progress = ((float)pkt->view_gp_4_ch_16_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend17.progress = ((float)pkt->view_gp_4_ch_17_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterGroupSend18.progress = ((float)pkt->view_gp_4_ch_18_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            if (![viewController sendCommandExists:CMD_GP4_ASSIGN dspId:DSP_5]) {
                [self refreshGroupSendButtons1_8:(pkt->gp_4_assign & 0x00FF)];
                [self refreshGroupSendButtons9_16:((pkt->gp_4_assign & 0xFF00) >> 8)];
            }
            if (![viewController sendCommandExists:CMD_CH17_USB_AUDIO_SETTING dspId:DSP_5]) {
                btnGroupSend17.selected = (pkt->ch_17_audio_setting & 0x08) >> 3;
                if (btnGroupSend17.selected) {
                    [lblGroupSend17 setTextColor:[UIColor whiteColor]];
                    [lblGroupSend17 setText:@"ON"];
                } else {
                    [lblGroupSend17 setTextColor:[UIColor blackColor]];
                    [lblGroupSend17 setText:@"OFF"];
                }
            }
            if (![viewController sendCommandExists:CMD_CH18_USB_AUDIO_SETTING dspId:DSP_5]) {
                btnGroupSend18.selected = (pkt->ch_18_audio_setting & 0x08) >> 3;
                if (btnGroupSend18.selected) {
                    [lblGroupSend18 setTextColor:[UIColor whiteColor]];
                    [lblGroupSend18 setText:@"ON"];
                } else {
                    [lblGroupSend18 setTextColor:[UIColor blackColor]];
                    [lblGroupSend18 setText:@"OFF"];
                }
            }
            if (![viewController sendCommandExists:CMD_AUXGP_SOLO_CTRL dspId:DSP_5]) {
                btnGroupSendSolo.selected = (pkt->solo_ctrl & 0x08) >> 3;
            }
            BOOL group4SoloSafe = (pkt->solo_safe_ctrl & 0x08) >> 3;
            if (group4SoloSafe) {
                [btnGroupSendSolo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
                [btnGroupSendSolo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
            } else {
                [btnGroupSendSolo setTitle:@"SOLO" forState:UIControlStateNormal];
                [btnGroupSendSolo setTitle:@"SOLO" forState:UIControlStateSelected];
            }
            if (![viewController sendCommandExists:CMD_AUXGP_ON_OFF dspId:DSP_5]) {
                btnGroupSendOn.selected = (pkt->aux_gp_master & 0x08) >> 3;
            }
            if (![viewController sendCommandExists:CMD_AUXGP_METER dspId:DSP_5]) {
                btnGroupMeter.selected = (pkt->aux_gp_meter_master & 0x08) >> 3;
            }
            if (!sliderGroupSendMainLock) {
                sliderGroupSendMain.value = pkt->gp_4_master_fader;
                [self onSliderGroupSendMainAction:nil];
            }
            if (![viewController sendCommandExists:CMD_VIEW_METER_CONTROL dspId:DSP_5]) {
                btnGroupSendPrePostAll.selected = (pkt->view_meter_control & 0x08) >> 3;
            }
            sliderPan.value = pkt->gp_4_master_pan + PAN_CENTER_VALUE;
            [self onSliderPanAction:nil];
            if (![viewController sendCommandExists:CMD_MULTI1_SOURCE dspId:DSP_5]) {
                btnGroupMulti1.selected = (pkt->multi_1_source & 0x08) >> 3;
            }
            if (![viewController sendCommandExists:CMD_MULTI2_SOURCE dspId:DSP_5]) {
                btnGroupMulti2.selected = (pkt->multi_2_source & 0x08) >> 3;
            }
            if (![viewController sendCommandExists:CMD_MULTI3_SOURCE dspId:DSP_5]) {
                btnGroupMulti3.selected = (pkt->multi_3_source & 0x08) >> 3;
            }
            if (![viewController sendCommandExists:CMD_MULTI4_SOURCE dspId:DSP_5]) {
                btnGroupMulti4.selected = (pkt->multi_4_source & 0x08) >> 3;
            }
            btnGroupEfx1OnOff.hidden = !((pkt->effect_1_2_out_source & 0x02) >> 1);
            btnGroupEfx2OnOff.hidden = !((pkt->effect_2_2_out_source & 0x02) >> 1);
            if (![viewController sendCommandExists:CMD_EFFECT_CONTROL dspId:DSP_5]) {
                btnGroupEfx1OnOff.selected = pkt->effect_control & 0x01;
                btnGroupEfx2OnOff.selected = (pkt->effect_control & 0x02) >> 1;
            }
            if (![viewController sendCommandExists:CMD_GP_TO_MAIN_CTRL dspId:DSP_5]) {
                btnGroupToMain.selected = (pkt->gp_to_main & 0x08) >> 3;
            }
            break;
            
        default:
            break;
    }
}

- (void)processMeter:(AllMeterValueRxPacket *)meterValue
{
    switch (currentGroup) {
        case GROUP_1:
            meterGroupSendMain.progress = ((float)meterValue->group_1_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            txtCurrentGroup.text = [NSString stringWithUTF8String:(const char *)meterValue->group_1_audio_name];
            break;
        case GROUP_2:
            meterGroupSendMain.progress = ((float)meterValue->group_2_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            txtCurrentGroup.text = [NSString stringWithUTF8String:(const char *)meterValue->group_2_audio_name];
            break;
        case GROUP_3:
            meterGroupSendMain.progress = ((float)meterValue->group_3_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            txtCurrentGroup.text = [NSString stringWithUTF8String:(const char *)meterValue->group_3_audio_name];
            break;
        case GROUP_4:
            meterGroupSendMain.progress = ((float)meterValue->group_4_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            txtCurrentGroup.text = [NSString stringWithUTF8String:(const char *)meterValue->group_4_audio_name];
            break;
        default:
            break;
    }
}

#pragma mark -
#pragma mark Group sends event handler methods

- (IBAction)onBtnGroupSendPrePostAll:(id)sender {
    btnGroupSendPrePostAll.selected = !btnGroupSendPrePostAll.selected;
    [Global setClearBit:&viewMeterCtrl bitValue:btnGroupSendPrePostAll.selected bitOrder:3];
    [viewController sendData:CMD_VIEW_METER_CONTROL commandType:DSP_WRITE dspId:DSP_5 value:viewMeterCtrl];
}

- (IBAction)onBtnGroupSend1:(id)sender {
    btnGroupSend1.selected = !btnGroupSend1.selected;
    if (btnGroupSend1.selected) {
        [lblGroupSend1 setTextColor:[UIColor whiteColor]];
        [lblGroupSend1 setText:@"ON"];
    } else {
        [lblGroupSend1 setTextColor:[UIColor blackColor]];
        [lblGroupSend1 setText:@"OFF"];
    }
    [self sendGroupChannel];
}

- (IBAction)onBtnGroupSend2:(id)sender {
    btnGroupSend2.selected = !btnGroupSend2.selected;
    if (btnGroupSend2.selected) {
        [lblGroupSend2 setTextColor:[UIColor whiteColor]];
        [lblGroupSend2 setText:@"ON"];
    } else {
        [lblGroupSend2 setTextColor:[UIColor blackColor]];
        [lblGroupSend2 setText:@"OFF"];
    }
    [self sendGroupChannel];
}

- (IBAction)onBtnGroupSend3:(id)sender {
    btnGroupSend3.selected = !btnGroupSend3.selected;
    if (btnGroupSend3.selected) {
        [lblGroupSend3 setTextColor:[UIColor whiteColor]];
        [lblGroupSend3 setText:@"ON"];
    } else {
        [lblGroupSend3 setTextColor:[UIColor blackColor]];
        [lblGroupSend3 setText:@"OFF"];
    }
    [self sendGroupChannel];
}

- (IBAction)onBtnGroupSend4:(id)sender {
    btnGroupSend4.selected = !btnGroupSend4.selected;
    if (btnGroupSend4.selected) {
        [lblGroupSend4 setTextColor:[UIColor whiteColor]];
        [lblGroupSend4 setText:@"ON"];
    } else {
        [lblGroupSend4 setTextColor:[UIColor blackColor]];
        [lblGroupSend4 setText:@"OFF"];
    }
    [self sendGroupChannel];
}

- (IBAction)onBtnGroupSend5:(id)sender {
    btnGroupSend5.selected = !btnGroupSend5.selected;
    if (btnGroupSend5.selected) {
        [lblGroupSend5 setTextColor:[UIColor whiteColor]];
        [lblGroupSend5 setText:@"ON"];
    } else {
        [lblGroupSend5 setTextColor:[UIColor blackColor]];
        [lblGroupSend5 setText:@"OFF"];
    }
    [self sendGroupChannel];
}

- (IBAction)onBtnGroupSend6:(id)sender {
    btnGroupSend6.selected = !btnGroupSend6.selected;
    if (btnGroupSend6.selected) {
        [lblGroupSend6 setTextColor:[UIColor whiteColor]];
        [lblGroupSend6 setText:@"ON"];
    } else {
        [lblGroupSend6 setTextColor:[UIColor blackColor]];
        [lblGroupSend6 setText:@"OFF"];
    }
    [self sendGroupChannel];
}

- (IBAction)onBtnGroupSend7:(id)sender {
    btnGroupSend7.selected = !btnGroupSend7.selected;
    if (btnGroupSend7.selected) {
        [lblGroupSend7 setTextColor:[UIColor whiteColor]];
        [lblGroupSend7 setText:@"ON"];
    } else {
        [lblGroupSend7 setTextColor:[UIColor blackColor]];
        [lblGroupSend7 setText:@"OFF"];
    }
    [self sendGroupChannel];
}

- (IBAction)onBtnGroupSend8:(id)sender {
    btnGroupSend8.selected = !btnGroupSend8.selected;
    if (btnGroupSend8.selected) {
        [lblGroupSend8 setTextColor:[UIColor whiteColor]];
        [lblGroupSend8 setText:@"ON"];
    } else {
        [lblGroupSend8 setTextColor:[UIColor blackColor]];
        [lblGroupSend8 setText:@"OFF"];
    }
    [self sendGroupChannel];
}

- (IBAction)onBtnGroupSend9:(id)sender {
    btnGroupSend9.selected = !btnGroupSend9.selected;
    if (btnGroupSend9.selected) {
        [lblGroupSend9 setTextColor:[UIColor whiteColor]];
        [lblGroupSend9 setText:@"ON"];
    } else {
        [lblGroupSend9 setTextColor:[UIColor blackColor]];
        [lblGroupSend9 setText:@"OFF"];
    }
    [self sendGroupChannel];
}

- (IBAction)onBtnGroupSend10:(id)sender {
    btnGroupSend10.selected = !btnGroupSend10.selected;
    if (btnGroupSend10.selected) {
        [lblGroupSend10 setTextColor:[UIColor whiteColor]];
        [lblGroupSend10 setText:@"ON"];
    } else {
        [lblGroupSend10 setTextColor:[UIColor blackColor]];
        [lblGroupSend10 setText:@"OFF"];
    }
    [self sendGroupChannel];
}

- (IBAction)onBtnGroupSend11:(id)sender {
    btnGroupSend11.selected = !btnGroupSend11.selected;
    if (btnGroupSend11.selected) {
        [lblGroupSend11 setTextColor:[UIColor whiteColor]];
        [lblGroupSend11 setText:@"ON"];
    } else {
        [lblGroupSend11 setTextColor:[UIColor blackColor]];
        [lblGroupSend11 setText:@"OFF"];
    }
    [self sendGroupChannel];
}

- (IBAction)onBtnGroupSend12:(id)sender {
    btnGroupSend12.selected = !btnGroupSend12.selected;
    if (btnGroupSend12.selected) {
        [lblGroupSend12 setTextColor:[UIColor whiteColor]];
        [lblGroupSend12 setText:@"ON"];
    } else {
        [lblGroupSend12 setTextColor:[UIColor blackColor]];
        [lblGroupSend12 setText:@"OFF"];
    }
    [self sendGroupChannel];
}

- (IBAction)onBtnGroupSend13:(id)sender {
    btnGroupSend13.selected = !btnGroupSend13.selected;
    if (btnGroupSend13.selected) {
        [lblGroupSend13 setTextColor:[UIColor whiteColor]];
        [lblGroupSend13 setText:@"ON"];
    } else {
        [lblGroupSend13 setTextColor:[UIColor blackColor]];
        [lblGroupSend13 setText:@"OFF"];
    }
    [self sendGroupChannel];
}

- (IBAction)onBtnGroupSend14:(id)sender {
    btnGroupSend14.selected = !btnGroupSend14.selected;
    if (btnGroupSend14.selected) {
        [lblGroupSend14 setTextColor:[UIColor whiteColor]];
        [lblGroupSend14 setText:@"ON"];
    } else {
        [lblGroupSend14 setTextColor:[UIColor blackColor]];
        [lblGroupSend14 setText:@"OFF"];
    }
    [self sendGroupChannel];
}

- (IBAction)onBtnGroupSend15:(id)sender {
    btnGroupSend15.selected = !btnGroupSend15.selected;
    if (btnGroupSend15.selected) {
        [lblGroupSend15 setTextColor:[UIColor whiteColor]];
        [lblGroupSend15 setText:@"ON"];
    } else {
        [lblGroupSend15 setTextColor:[UIColor blackColor]];
        [lblGroupSend15 setText:@"OFF"];
    }
    [self sendGroupChannel];
}

- (IBAction)onBtnGroupSend16:(id)sender {
    btnGroupSend16.selected = !btnGroupSend16.selected;
    if (btnGroupSend16.selected) {
        [lblGroupSend16 setTextColor:[UIColor whiteColor]];
        [lblGroupSend16 setText:@"ON"];
    } else {
        [lblGroupSend16 setTextColor:[UIColor blackColor]];
        [lblGroupSend16 setText:@"OFF"];
    }
    [self sendGroupChannel];
}

- (IBAction)onBtnGroupSend17:(id)sender {
    btnGroupSend17.selected = !btnGroupSend17.selected;
    if (btnGroupSend17.selected) {
        [lblGroupSend17 setTextColor:[UIColor whiteColor]];
        [lblGroupSend17 setText:@"ON"];
    } else {
        [lblGroupSend17 setTextColor:[UIColor blackColor]];
        [lblGroupSend17 setText:@"OFF"];
    }
    int bitOrder = 0;
    switch (currentGroup) {
        case GROUP_1:
            bitOrder = 0;
            break;
        case GROUP_2:
            bitOrder = 1;
            break;
        case GROUP_3:
            bitOrder = 2;
            break;
        case GROUP_4:
            bitOrder = 3;
            break;
        default:
            break;
    }
    [Global setClearBit:&ch17AudioSetting bitValue:btnGroupSend17.selected bitOrder:bitOrder];
    [viewController sendData:CMD_CH17_USB_AUDIO_SETTING commandType:DSP_WRITE dspId:DSP_5 value:ch17AudioSetting];
}

- (IBAction)onBtnGroupSend18:(id)sender {
    btnGroupSend18.selected = !btnGroupSend18.selected;
    if (btnGroupSend18.selected) {
        [lblGroupSend18 setTextColor:[UIColor whiteColor]];
        [lblGroupSend18 setText:@"ON"];
    } else {
        [lblGroupSend18 setTextColor:[UIColor blackColor]];
        [lblGroupSend18 setText:@"OFF"];
    }
    int bitOrder = 0;
    switch (currentGroup) {
        case GROUP_1:
            bitOrder = 0;
            break;
        case GROUP_2:
            bitOrder = 1;
            break;
        case GROUP_3:
            bitOrder = 2;
            break;
        case GROUP_4:
            bitOrder = 3;
            break;
        default:
            break;
    }
    [Global setClearBit:&ch18AudioSetting bitValue:btnGroupSend18.selected bitOrder:bitOrder];
    [viewController sendData:CMD_CH18_USB_AUDIO_SETTING commandType:DSP_WRITE dspId:DSP_5 value:ch18AudioSetting];
}

- (IBAction)onBtnGroupSolo:(id)sender {
    btnGroupSendSolo.selected = !btnGroupSendSolo.selected;
    switch (currentGroup) {
        case GROUP_1:
            [Global setClearBit:&soloCtrl bitValue:btnGroupSendSolo.selected bitOrder:0];
            [viewController sendData:CMD_AUXGP_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:soloCtrl];
            break;
        case GROUP_2:
            [Global setClearBit:&soloCtrl bitValue:btnGroupSendSolo.selected bitOrder:1];
            [viewController sendData:CMD_AUXGP_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:soloCtrl];
            break;
        case GROUP_3:
            [Global setClearBit:&soloCtrl bitValue:btnGroupSendSolo.selected bitOrder:2];
            [viewController sendData:CMD_AUXGP_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:soloCtrl];
            break;
        case GROUP_4:
            [Global setClearBit:&soloCtrl bitValue:btnGroupSendSolo.selected bitOrder:3];
            [viewController sendData:CMD_AUXGP_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:soloCtrl];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnGroupOn:(id)sender {
    btnGroupSendOn.selected = !btnGroupSendOn.selected;
    switch (currentGroup) {
        case GROUP_1:
            [Global setClearBit:&auxGpMaster bitValue:btnGroupSendOn.selected bitOrder:0];
            [viewController sendData:CMD_AUXGP_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:auxGpMaster];
            break;
        case GROUP_2:
            [Global setClearBit:&auxGpMaster bitValue:btnGroupSendOn.selected bitOrder:1];
            [viewController sendData:CMD_AUXGP_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:auxGpMaster];
            break;
        case GROUP_3:
            [Global setClearBit:&auxGpMaster bitValue:btnGroupSendOn.selected bitOrder:2];
            [viewController sendData:CMD_AUXGP_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:auxGpMaster];
            break;
        case GROUP_4:
            [Global setClearBit:&auxGpMaster bitValue:btnGroupSendOn.selected bitOrder:3];
            [viewController sendData:CMD_AUXGP_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:auxGpMaster];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnGroupSelectDown:(id)sender {
    if (currentGroup > GROUP_1)
        currentGroup--;
    currentGroupLabel = currentGroup;
    [self updateCurrentGroupLabel];
}

- (IBAction)onBtnGroupSelectUp:(id)sender {
    if (currentGroup < GROUP_4)
        currentGroup++;
    currentGroupLabel = currentGroup;
    [self updateCurrentGroupLabel];
}

- (IBAction)onBtnGroupMeter:(id)sender {
    btnGroupMeter.selected = !btnGroupMeter.selected;
    switch (currentGroup) {
        case GROUP_1:
            [Global setClearBit:&auxGpMeterMaster bitValue:btnGroupMeter.selected bitOrder:0];
            [viewController sendData:CMD_AUXGP_METER commandType:DSP_WRITE dspId:DSP_5 value:auxGpMeterMaster];
            break;
        case GROUP_2:
            [Global setClearBit:&auxGpMeterMaster bitValue:btnGroupMeter.selected bitOrder:1];
            [viewController sendData:CMD_AUXGP_METER commandType:DSP_WRITE dspId:DSP_5 value:auxGpMeterMaster];
            break;
        case GROUP_3:
            [Global setClearBit:&auxGpMeterMaster bitValue:btnGroupMeter.selected bitOrder:2];
            [viewController sendData:CMD_AUXGP_METER commandType:DSP_WRITE dspId:DSP_5 value:auxGpMeterMaster];
            break;
        case GROUP_4:
            [Global setClearBit:&auxGpMeterMaster bitValue:btnGroupMeter.selected bitOrder:3];
            [viewController sendData:CMD_AUXGP_METER commandType:DSP_WRITE dspId:DSP_5 value:auxGpMeterMaster];
            break;
        default:
            break;
    }
}

- (IBAction)onSliderGroupSendMainBegin:(id) sender {
    sliderGroupSendMainLock = YES;
}

- (IBAction)onSliderGroupSendMainEnd:(id) sender {
    sliderGroupSendMainLock = NO;
}

- (IBAction)onSliderGroupSendMainAction:(id) sender {
    [lblSliderGroupMainValue setText:[Global getSliderGain:sliderGroupSendMain.value appendDb:YES]];
    if (sender != nil) {
        switch (currentGroup) {
            case GROUP_1:
                [viewController sendData:CMD_GP1_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderGroupSendMain.value];
                break;
            case GROUP_2:
                [viewController sendData:CMD_GP2_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderGroupSendMain.value];
                break;
            case GROUP_3:
                [viewController sendData:CMD_GP3_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderGroupSendMain.value];
                break;
            case GROUP_4:
                [viewController sendData:CMD_GP4_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderGroupSendMain.value];
                break;
            default:
                break;
        }
    }
}

- (IBAction)onBtnGroupMulti1:(id)sender {
    btnGroupMulti1.selected = !btnGroupMulti1.selected;
    multi1Source = 0;
    switch (currentGroup) {
        case GROUP_1:
            [Global setClearBit:&multi1Source bitValue:btnGroupMulti1.selected bitOrder:0];
            [viewController sendData:CMD_MULTI1_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:multi1Source];
            break;
        case GROUP_2:
            [Global setClearBit:&multi1Source bitValue:btnGroupMulti1.selected bitOrder:1];
            [viewController sendData:CMD_MULTI1_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:multi1Source];
            break;
        case GROUP_3:
            [Global setClearBit:&multi1Source bitValue:btnGroupMulti1.selected bitOrder:2];
            [viewController sendData:CMD_MULTI1_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:multi1Source];
            break;
        case GROUP_4:
            [Global setClearBit:&multi1Source bitValue:btnGroupMulti1.selected bitOrder:3];
            [viewController sendData:CMD_MULTI1_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:multi1Source];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnGroupMulti2:(id)sender {
    btnGroupMulti2.selected = !btnGroupMulti2.selected;
    multi2Source = 0;
    switch (currentGroup) {
        case GROUP_1:
            [Global setClearBit:&multi2Source bitValue:btnGroupMulti2.selected bitOrder:0];
            [viewController sendData:CMD_MULTI2_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:multi2Source];
            break;
        case GROUP_2:
            [Global setClearBit:&multi2Source bitValue:btnGroupMulti2.selected bitOrder:1];
            [viewController sendData:CMD_MULTI2_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:multi2Source];
            break;
        case GROUP_3:
            [Global setClearBit:&multi2Source bitValue:btnGroupMulti2.selected bitOrder:2];
            [viewController sendData:CMD_MULTI2_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:multi2Source];
            break;
        case GROUP_4:
            [Global setClearBit:&multi2Source bitValue:btnGroupMulti2.selected bitOrder:3];
            [viewController sendData:CMD_MULTI2_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:multi2Source];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnGroupMulti3:(id)sender {
    btnGroupMulti3.selected = !btnGroupMulti3.selected;
    multi3Source = 0;
    switch (currentGroup) {
        case GROUP_1:
            [Global setClearBit:&multi3Source bitValue:btnGroupMulti3.selected bitOrder:0];
            [viewController sendData:CMD_MULTI3_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:multi3Source];
            break;
        case GROUP_2:
            [Global setClearBit:&multi3Source bitValue:btnGroupMulti3.selected bitOrder:1];
            [viewController sendData:CMD_MULTI3_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:multi3Source];
            break;
        case GROUP_3:
            [Global setClearBit:&multi3Source bitValue:btnGroupMulti3.selected bitOrder:2];
            [viewController sendData:CMD_MULTI3_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:multi3Source];
            break;
        case GROUP_4:
            [Global setClearBit:&multi3Source bitValue:btnGroupMulti3.selected bitOrder:3];
            [viewController sendData:CMD_MULTI3_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:multi3Source];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnGroupMulti4:(id)sender {
    btnGroupMulti4.selected = !btnGroupMulti4.selected;
    multi4Source = 0;
    switch (currentGroup) {
        case GROUP_1:
            [Global setClearBit:&multi4Source bitValue:btnGroupMulti4.selected bitOrder:0];
            [viewController sendData:CMD_MULTI4_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:multi4Source];
            break;
        case GROUP_2:
            [Global setClearBit:&multi4Source bitValue:btnGroupMulti4.selected bitOrder:1];
            [viewController sendData:CMD_MULTI4_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:multi4Source];
            break;
        case GROUP_3:
            [Global setClearBit:&multi4Source bitValue:btnGroupMulti4.selected bitOrder:2];
            [viewController sendData:CMD_MULTI4_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:multi4Source];
            break;
        case GROUP_4:
            [Global setClearBit:&multi4Source bitValue:btnGroupMulti4.selected bitOrder:3];
            [viewController sendData:CMD_MULTI4_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:multi4Source];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnGroupEfx1OnOff:(id)sender {
    btnGroupEfx1OnOff.selected = !btnGroupEfx1OnOff.selected;
    [Global setClearBit:&effectCtrl bitValue:btnGroupEfx1OnOff.selected bitOrder:0];
    [viewController sendData:CMD_EFFECT_CONTROL commandType:DSP_WRITE dspId:DSP_5 value:effectCtrl];
}

- (IBAction)onBtnGroupEfx2OnOff:(id)sender {
    btnGroupEfx2OnOff.selected = !btnGroupEfx2OnOff.selected;
    [Global setClearBit:&effectCtrl bitValue:btnGroupEfx2OnOff.selected bitOrder:1];
    [viewController sendData:CMD_EFFECT_CONTROL commandType:DSP_WRITE dspId:DSP_5 value:effectCtrl];
}

- (IBAction)onBtnGroupToMain:(id)sender {
    btnGroupToMain.selected = !btnGroupToMain.selected;
    switch (currentGroup) {
        case GROUP_1:
            [Global setClearBit:&gpToMain bitValue:btnGroupToMain.selected bitOrder:0];
            [viewController sendData:CMD_GP_TO_MAIN_CTRL commandType:DSP_WRITE dspId:DSP_5 value:gpToMain];
            break;
        case GROUP_2:
            [Global setClearBit:&gpToMain bitValue:btnGroupToMain.selected bitOrder:1];
            [viewController sendData:CMD_GP_TO_MAIN_CTRL commandType:DSP_WRITE dspId:DSP_5 value:gpToMain];
            break;
        case GROUP_3:
            [Global setClearBit:&gpToMain bitValue:btnGroupToMain.selected bitOrder:2];
            [viewController sendData:CMD_GP_TO_MAIN_CTRL commandType:DSP_WRITE dspId:DSP_5 value:gpToMain];
            break;
        case GROUP_4:
            [Global setClearBit:&gpToMain bitValue:btnGroupToMain.selected bitOrder:3];
            [viewController sendData:CMD_GP_TO_MAIN_CTRL commandType:DSP_WRITE dspId:DSP_5 value:gpToMain];
            break;
        default:
            break;
    }
}

#pragma mark -
#pragma mark PAN slider event handler methods

- (IBAction)onSliderPanAction:(id)sender {
    sliderPan.value = floorf(sliderPan.value);
    if (sliderPan.value >= 0.0 && sliderPan.value < PAN_CENTER_VALUE) {
        [meterPan setHidden:NO];
        [meterPan setTransform:CGAffineTransformMake(-1, 0, 0, 1, 0, 0)];
        [meterPan setProgress:1.0 - sliderPan.value/PAN_MAX_VALUE];
    } else if (sliderPan.value > PAN_CENTER_VALUE && sliderPan.value <= PAN_MAX_VALUE) {
        [meterPan setHidden:NO];
        [meterPan setTransform:CGAffineTransformMake(1, 0, 0, 1, 0, 0)];
        [meterPan setProgress:sliderPan.value/PAN_MAX_VALUE];
    } else {
        [meterPan setHidden:YES];
        [imgPan setImage:[UIImage imageNamed:@"pan-2.png"]];
    }
    [lblPanValue setText:[Global getPanString:sliderPan.value]];
    
    if (sender != nil) {
        switch (currentGroup) {
            case GROUP_1:
                [viewController sendData:CMD_GP1_PAN_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderPan.value-PAN_CENTER_VALUE];
                break;
            case GROUP_2:
                [viewController sendData:CMD_GP2_PAN_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderPan.value-PAN_CENTER_VALUE];
                break;
            case GROUP_3:
                [viewController sendData:CMD_GP3_PAN_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderPan.value-PAN_CENTER_VALUE];
                break;
            case GROUP_4:
                [viewController sendData:CMD_GP4_PAN_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderPan.value-PAN_CENTER_VALUE];
                break;
            default:
                break;
        }
    }
}

- (IBAction)onSliderPanStart:(id) sender {
    [sliderPan setThumbImage:[UIImage imageNamed:@"Slider-2.png"] forState:UIControlStateNormal];
}

- (IBAction)onSliderPanEnd:(id) sender {
    [sliderPan setThumbImage:[UIImage imageNamed:@"Slider-1.png"] forState:UIControlStateNormal];
}

@end
