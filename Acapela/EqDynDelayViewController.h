//
//  EqDynDelayViewController.h
//  Acapela
//
//  Created by Kevin Phua on 13/8/31.
//  Copyright (c) 2013年 Kevin Phua. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MHRotaryKnob.h"
#import "DACircularProgressView.h"
#import "acapela.h"
#import "GeqPeqViewController.h"
#import "DynViewController.h"
#import "Global.h"

enum ViewPage {
    VIEW_PAGE_CH_1_8 = 0,
    VIEW_PAGE_CH_9_16,
    VIEW_PAGE_USB_AUDIO,
    VIEW_PAGE_MT_1_4,
    VIEW_PAGE_CTRL_RM,
    VIEW_PAGE_MAIN
};

@interface EqDynDelayViewController : UIViewController <UITextFieldDelegate, PeqPreviewDelegate, DynPreviewDelegate>
{
    // Label edit
    ChannelNum currentChannel;
    ChannelNum currentChannelLabel;
}

@property (nonatomic) ChannelNum currentChannel;
@property (nonatomic) ChannelNum currentChannelLabel;

@property (weak, nonatomic) IBOutlet UIImageView *imgBackground;

@property (weak, nonatomic) IBOutlet UIView *viewTop;
@property (weak, nonatomic) IBOutlet UIView *viewEqPreview;
@property (weak, nonatomic) IBOutlet UIImageView *imgEqPreview;
@property (weak, nonatomic) IBOutlet UIView *viewDynPreview;
@property (weak, nonatomic) IBOutlet UIImageView *imgDynPreview;
@property (weak, nonatomic) IBOutlet UIButton *btnEq;
@property (weak, nonatomic) IBOutlet UIButton *btnDyn;
@property (weak, nonatomic) IBOutlet UIButton *btnDynGate;
@property (weak, nonatomic) IBOutlet UIButton *btnDynExpander;
@property (weak, nonatomic) IBOutlet UIButton *btnDynComp;
@property (weak, nonatomic) IBOutlet UIButton *btnDynLimiter;
@property (weak, nonatomic) IBOutlet UIButton *btnDelay;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobDelay;
@property (nonatomic, retain) DACircularProgressView *cpvDelay;
@property (weak, nonatomic) IBOutlet UILabel *lblDelay;
@property (weak, nonatomic) IBOutlet UIImageView *imgOrder1;
@property (weak, nonatomic) IBOutlet UIImageView *imgOrder2;
@property (weak, nonatomic) IBOutlet UIImageView *imgOrder3;
@property (weak, nonatomic) IBOutlet UIButton *btnInv;
@property (weak, nonatomic) IBOutlet UILabel *lblPan;
@property (nonatomic,retain) UISlider *sliderPan;
@property (nonatomic,retain) JEProgressView *meterPan;
@property (nonatomic,retain) JEProgressView *meterEqOut;
@property (nonatomic,retain) JEProgressView *meterDynOut;
@property (nonatomic,retain) JEProgressView *meterEqOutR;
@property (nonatomic,retain) JEProgressView *meterDynOutR;

@property (weak, nonatomic) IBOutlet UIView *viewBottomChannel;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobAux1;
@property (nonatomic, retain) DACircularProgressView *cpvAux1;
@property (weak, nonatomic) IBOutlet UILabel *lblAux1;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobAux2;
@property (nonatomic, retain) DACircularProgressView *cpvAux2;
@property (weak, nonatomic) IBOutlet UILabel *lblAux2;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobAux3;
@property (nonatomic, retain) DACircularProgressView *cpvAux3;
@property (weak, nonatomic) IBOutlet UILabel *lblAux3;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobAux4;
@property (nonatomic, retain) DACircularProgressView *cpvAux4;
@property (weak, nonatomic) IBOutlet UILabel *lblAux4;
@property (weak, nonatomic) IBOutlet UIButton *btnChannelAux1PostPre;
@property (weak, nonatomic) IBOutlet UIButton *btnChannelAux2PostPre;
@property (weak, nonatomic) IBOutlet UIButton *btnChannelAux3PostPre;
@property (weak, nonatomic) IBOutlet UIButton *btnChannelAux4PostPre;
@property (weak, nonatomic) IBOutlet UIButton *btnChannelGp1OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnChannelGp2OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnChannelGp3OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnChannelGp4OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnChannelToMainOnOff;

@property (weak, nonatomic) IBOutlet UIView *viewBottomMulti;
@property (weak, nonatomic) IBOutlet UIButton *btnMultiAux1OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnMultiAux2OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnMultiAux3OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnMultiAux4OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnMultiGp1OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnMultiGp2OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnMultiGp3OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnMultiGp4OnOff;

@property (weak, nonatomic) IBOutlet UIView *viewBottomMain;
@property (weak, nonatomic) IBOutlet UIButton *btnMainCh1OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnMainCh2OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnMainCh3OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnMainCh4OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnMainCh5OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnMainCh6OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnMainCh7OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnMainCh8OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnMainCh9OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnMainCh10OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnMainCh11OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnMainCh12OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnMainCh13OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnMainCh14OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnMainCh15OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnMainCh16OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnMainCh17OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnMainCh18OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnMainGp1OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnMainGp2OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnMainGp3OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnMainGp4OnOff;

@property (weak, nonatomic) IBOutlet UIView *viewCtrlRm;
@property (weak, nonatomic) IBOutlet UIButton *btnGlobalPfl;
@property (weak, nonatomic) IBOutlet UIButton *btnGlobalAfl;
@property (weak, nonatomic) IBOutlet UIButton *btnGlobalClearAll;
@property (weak, nonatomic) IBOutlet UIButton *btnSoloSafe;
@property (weak, nonatomic) IBOutlet UIButton *btnSolo;
@property (weak, nonatomic) IBOutlet UIButton *btnSoloPflAfl;
@property (weak, nonatomic) IBOutlet UIButton *btnMainPflAfl;
@property (weak, nonatomic) IBOutlet UIImageView *imgMainPflAflArrow;
@property (weak, nonatomic) IBOutlet UIButton *btnChannel1;
@property (weak, nonatomic) IBOutlet UIButton *btnChannel2;
@property (weak, nonatomic) IBOutlet UIButton *btnChannel3;
@property (weak, nonatomic) IBOutlet UIButton *btnChannel4;
@property (weak, nonatomic) IBOutlet UIButton *btnChannel5;
@property (weak, nonatomic) IBOutlet UIButton *btnChannel6;
@property (weak, nonatomic) IBOutlet UIButton *btnChannel7;
@property (weak, nonatomic) IBOutlet UIButton *btnChannel8;
@property (weak, nonatomic) IBOutlet UIButton *btnChannel9;
@property (weak, nonatomic) IBOutlet UIButton *btnChannel10;
@property (weak, nonatomic) IBOutlet UIButton *btnChannel11;
@property (weak, nonatomic) IBOutlet UIButton *btnChannel12;
@property (weak, nonatomic) IBOutlet UIButton *btnChannel13;
@property (weak, nonatomic) IBOutlet UIButton *btnChannel14;
@property (weak, nonatomic) IBOutlet UIButton *btnChannel15;
@property (weak, nonatomic) IBOutlet UIButton *btnChannel16;
@property (weak, nonatomic) IBOutlet UIButton *btnChannel17;
@property (weak, nonatomic) IBOutlet UIButton *btnChannel18;
@property (weak, nonatomic) IBOutlet UIButton *btnEfx1;
@property (weak, nonatomic) IBOutlet UIButton *btnEfx2;
@property (weak, nonatomic) IBOutlet UIButton *btnAux1;
@property (weak, nonatomic) IBOutlet UIButton *btnAux2;
@property (weak, nonatomic) IBOutlet UIButton *btnAux3;
@property (weak, nonatomic) IBOutlet UIButton *btnAux4;
@property (weak, nonatomic) IBOutlet UIButton *btnGp1;
@property (weak, nonatomic) IBOutlet UIButton *btnGp2;
@property (weak, nonatomic) IBOutlet UIButton *btnGp3;
@property (weak, nonatomic) IBOutlet UIButton *btnGp4;
@property (weak, nonatomic) IBOutlet UILabel *lblChannel1Safe;
@property (weak, nonatomic) IBOutlet UILabel *lblChannel2Safe;
@property (weak, nonatomic) IBOutlet UILabel *lblChannel3Safe;
@property (weak, nonatomic) IBOutlet UILabel *lblChannel4Safe;
@property (weak, nonatomic) IBOutlet UILabel *lblChannel5Safe;
@property (weak, nonatomic) IBOutlet UILabel *lblChannel6Safe;
@property (weak, nonatomic) IBOutlet UILabel *lblChannel7Safe;
@property (weak, nonatomic) IBOutlet UILabel *lblChannel8Safe;
@property (weak, nonatomic) IBOutlet UILabel *lblChannel9Safe;
@property (weak, nonatomic) IBOutlet UILabel *lblChannel10Safe;
@property (weak, nonatomic) IBOutlet UILabel *lblChannel11Safe;
@property (weak, nonatomic) IBOutlet UILabel *lblChannel12Safe;
@property (weak, nonatomic) IBOutlet UILabel *lblChannel13Safe;
@property (weak, nonatomic) IBOutlet UILabel *lblChannel14Safe;
@property (weak, nonatomic) IBOutlet UILabel *lblChannel15Safe;
@property (weak, nonatomic) IBOutlet UILabel *lblChannel16Safe;
@property (weak, nonatomic) IBOutlet UILabel *lblChannel17Safe;
@property (weak, nonatomic) IBOutlet UILabel *lblChannel18Safe;
@property (weak, nonatomic) IBOutlet UILabel *lblEfx1Safe;
@property (weak, nonatomic) IBOutlet UILabel *lblEfx2Safe;
@property (weak, nonatomic) IBOutlet UILabel *lblAux1Safe;
@property (weak, nonatomic) IBOutlet UILabel *lblAux2Safe;
@property (weak, nonatomic) IBOutlet UILabel *lblAux3Safe;
@property (weak, nonatomic) IBOutlet UILabel *lblAux4Safe;
@property (weak, nonatomic) IBOutlet UILabel *lblGp1Safe;
@property (weak, nonatomic) IBOutlet UILabel *lblGp2Safe;
@property (weak, nonatomic) IBOutlet UILabel *lblGp3Safe;
@property (weak, nonatomic) IBOutlet UILabel *lblGp4Safe;
@property (weak, nonatomic) IBOutlet UIImageView *imgChannel1Solo;
@property (weak, nonatomic) IBOutlet UIImageView *imgChannel2Solo;
@property (weak, nonatomic) IBOutlet UIImageView *imgChannel3Solo;
@property (weak, nonatomic) IBOutlet UIImageView *imgChannel4Solo;
@property (weak, nonatomic) IBOutlet UIImageView *imgChannel5Solo;
@property (weak, nonatomic) IBOutlet UIImageView *imgChannel6Solo;
@property (weak, nonatomic) IBOutlet UIImageView *imgChannel7Solo;
@property (weak, nonatomic) IBOutlet UIImageView *imgChannel8Solo;
@property (weak, nonatomic) IBOutlet UIImageView *imgChannel9Solo;
@property (weak, nonatomic) IBOutlet UIImageView *imgChannel10Solo;
@property (weak, nonatomic) IBOutlet UIImageView *imgChannel11Solo;
@property (weak, nonatomic) IBOutlet UIImageView *imgChannel12Solo;
@property (weak, nonatomic) IBOutlet UIImageView *imgChannel13Solo;
@property (weak, nonatomic) IBOutlet UIImageView *imgChannel14Solo;
@property (weak, nonatomic) IBOutlet UIImageView *imgChannel15Solo;
@property (weak, nonatomic) IBOutlet UIImageView *imgChannel16Solo;
@property (weak, nonatomic) IBOutlet UIImageView *imgChannel17Solo;
@property (weak, nonatomic) IBOutlet UIImageView *imgChannel18Solo;
@property (weak, nonatomic) IBOutlet UIImageView *imgEfx1Solo;
@property (weak, nonatomic) IBOutlet UIImageView *imgEfx2Solo;
@property (weak, nonatomic) IBOutlet UIImageView *imgAux1Solo;
@property (weak, nonatomic) IBOutlet UIImageView *imgAux2Solo;
@property (weak, nonatomic) IBOutlet UIImageView *imgAux3Solo;
@property (weak, nonatomic) IBOutlet UIImageView *imgAux4Solo;
@property (weak, nonatomic) IBOutlet UIImageView *imgGp1Solo;
@property (weak, nonatomic) IBOutlet UIImageView *imgGp2Solo;
@property (weak, nonatomic) IBOutlet UIImageView *imgGp3Solo;
@property (weak, nonatomic) IBOutlet UIImageView *imgGp4Solo;

// Order view
@property (weak, nonatomic) IBOutlet UIView *viewOrder;
@property (weak, nonatomic) IBOutlet UIButton *btnOrderEqDynDelay;
@property (weak, nonatomic) IBOutlet UIButton *btnOrderEqDelayDyn;
@property (weak, nonatomic) IBOutlet UIButton *btnOrderDynEqDelay;
@property (weak, nonatomic) IBOutlet UIButton *btnOrderDynDelayEq;
@property (weak, nonatomic) IBOutlet UIButton *btnOrderDelayEqDyn;
@property (weak, nonatomic) IBOutlet UIButton *btnOrderDelayDynEq;
@property (weak, nonatomic) IBOutlet UIView *viewOrderMain;
@property (weak, nonatomic) IBOutlet UIButton *btnOrderMainEqDynDelay;
@property (weak, nonatomic) IBOutlet UIButton *btnOrderMainDynEqDelay;
@property (weak, nonatomic) IBOutlet UIButton *btnOrderMainGeqDynDelay;
@property (weak, nonatomic) IBOutlet UIButton *btnOrderMainDynGeqDelay;
@property (weak, nonatomic) IBOutlet UIView *viewOrderMulti;
@property (weak, nonatomic) IBOutlet UIButton *btnOrderMultiEqDynDelay;
@property (weak, nonatomic) IBOutlet UIButton *btnOrderMultiDynEqDelay;

// Right panel
@property (weak, nonatomic) IBOutlet UIButton *btnCurrentSolo;
@property (weak, nonatomic) IBOutlet UIButton *btnCurrentOn;
@property (weak, nonatomic) IBOutlet UIButton *btnChannelSelectDown;
@property (weak, nonatomic) IBOutlet UIButton *btnChannelSelectUp;
@property (weak, nonatomic) IBOutlet UIButton *btnCurrentMeter;
@property (weak, nonatomic) IBOutlet UITextField *txtCurrentChannel;
@property (weak, nonatomic) IBOutlet UILabel *lblCurrentChannel;
@property (weak, nonatomic) IBOutlet UILabel *lblSliderValue;
@property (nonatomic,retain) UISlider *sliderCurrent;
@property (nonatomic,retain) JEProgressView *meterCurrentL;
@property (nonatomic,retain) JEProgressView *meterCurrentR;

- (void)setViewPage:(int)page;
- (void)processChannelViewPage:(ViewPageChannelPacket *)pkt;
- (void)processUsbAudioViewPage:(ViewPageUsbAudioPacket *)pkt;
- (void)processMultiViewPage:(ViewPageMultiPacket *)pkt;
- (void)processMainViewPage:(ViewPageMainPacket *)pkt;
- (void)processCtrlRmPage:(SetupCtrlRmUsbAssignPagePacket *)pkt;
- (void)processChannelLabel:(AllMeterValueRxPacket *)pkt;
- (void)refreshEqPreview;
- (void)refreshDynPreview;

@end
