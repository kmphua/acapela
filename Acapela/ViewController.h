//
//  ViewController.h
//  Acapela
//
//  Created by Kevin on 13/7/15.
//  Copyright (c) 2013年 Kevin Phua. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GCDAsyncSocket.h"
#import "MHRotaryKnob.h"
#import "MBProgressHUD.h"
#import "DACircularProgressView.h"
#import "AuxSendViewController.h"
#import "GroupSendViewController.h"
#import "EffectViewController.h"
#import "GeqPeqViewController.h"
#import "EqDynDelayViewController.h"
#import "DynViewController.h"
#import "Global.h"
#import "JEProgressView.h"

#define MAX_CHANNELS        18
#define MAX_MULTI           4

enum PageName {
    PAGE_CH_1_8 = 0,
    PAGE_CH_9_16,
    PAGE_USB_AUDIO,
    PAGE_AUX_1_4,
    PAGE_GP_1_4,
    PAGE_EFX_1,
    PAGE_EFX_2,
    PAGE_MT_1_4,
    PAGE_USB_AUDIO_OUT,
    PAGE_CTRL_RM,
    PAGE_MAIN
};

enum Mode {
    MODE_PHANTOM_POWER = 0,
    MODE_INV,
    MODE_PAN,
    MODE_EQ,
    MODE_DYN,
    MODE_DELAY,
    MODE_ORDER
};

enum PageType {
    PRIMARY = 0,        // Page displayed when top meter button is pressed
    SECONDARY           // Page displayed when top label button is pressed
};

enum UsbOutput {
    USB_OUTPUT_L,
    USB_OUTPUT_R
};

enum UsbAssign {
    USB_ASSIGN_NULL = 0,
    USB_ASSIGN_CH_1,
    USB_ASSIGN_CH_2,
    USB_ASSIGN_CH_3,
    USB_ASSIGN_CH_4,
    USB_ASSIGN_CH_5,
    USB_ASSIGN_CH_6,
    USB_ASSIGN_CH_7,
    USB_ASSIGN_CH_8,
    USB_ASSIGN_CH_9,
    USB_ASSIGN_CH_10,
    USB_ASSIGN_CH_11,
    USB_ASSIGN_CH_12,
    USB_ASSIGN_CH_13,
    USB_ASSIGN_CH_14,
    USB_ASSIGN_CH_15,
    USB_ASSIGN_CH_16,
    USB_ASSIGN_CH_17,
    USB_ASSIGN_CH_18,
    USB_ASSIGN_GP_1,
    USB_ASSIGN_GP_2,
    USB_ASSIGN_GP_3,
    USB_ASSIGN_GP_4,
    USB_ASSIGN_AUX_1,
    USB_ASSIGN_AUX_2,
    USB_ASSIGN_AUX_3,
    USB_ASSIGN_AUX_4,
    USB_ASSIGN_MAIN_L = 32,
    USB_ASSIGN_MAIN_R,
    USB_ASSIGN_MULTI_1,
    USB_ASSIGN_MULTI_2,
    USB_ASSIGN_MULTI_3,
    USB_ASSIGN_MULTI_4
};

@interface ViewController : UIViewController <NSStreamDelegate, UITextFieldDelegate, UITableViewDataSource, UITableViewDelegate, UIAlertViewDelegate, PeqPreviewDelegate, DynPreviewDelegate, GCDAsyncSocketDelegate, MBProgressHUDDelegate>
{
    GCDAsyncSocket *socket;
    enum PageName currentPage;
    enum PageType currentPageType;
    enum Mode currentMode;
    int orderChannel[MAX_CHANNELS];   // OrderChannel
    int orderMain;                       // OrderMain
    int orderMulti[MAX_MULTI];          // OrderMulti
    ChannelNum currentOrderChannel;
    int invOffOn[MAX_CHANNELS];
    
    // Label edit
    int currentLabel;
    
    // Values
    short ch1Ctrl, ch2Ctrl, ch3Ctrl, ch4Ctrl;
    short ch1DelayTime, ch2DelayTime, ch3DelayTime, ch4DelayTime;
    short ch1MeterEqOut, ch2MeterEqOut, ch3MeterEqOut, ch4MeterEqOut;
    short ch1MeterDynOut, ch2MeterDynOut, ch3MeterDynOut, ch4MeterDynOut;
    short ch1234DynStatus, ch1_16OnOff, ch1_16SoloOnOff, ch1_16MeterPrePost;
    short chToMainCtrl;
    
    MBProgressHUD *HUD;
}

// Left panel
@property (weak, nonatomic) IBOutlet UILabel *lblSceneName;
@property (weak, nonatomic) IBOutlet UILabel *lblClkRate;
@property (weak, nonatomic) IBOutlet UILabel *lblCurrentMode;
@property (weak, nonatomic) IBOutlet UIButton *btnModeSelectDown;
@property (weak, nonatomic) IBOutlet UIButton *btnModeSelectUp;
@property (weak, nonatomic) IBOutlet UIButton *btnSetup;

// Setup panel
@property (weak, nonatomic) IBOutlet UIView *viewSetup;
@property (weak, nonatomic) IBOutlet UIImageView *imgSetupBg;
@property (weak, nonatomic) IBOutlet UIButton *btnSetupPage1;
@property (weak, nonatomic) IBOutlet UIButton *btnSetupPage2;

// Setup page 1
@property (weak, nonatomic) IBOutlet UIView *viewSetupPage1;
@property (weak, nonatomic) IBOutlet UILabel *lblIpAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblOnline;
@property (weak, nonatomic) IBOutlet UIImageView *imgOnline;
@property (weak, nonatomic) IBOutlet UISwitch *switchSetupLocate;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityLoading;
@property (weak, nonatomic) IBOutlet UILabel *lblVersion;
@property (weak, nonatomic) IBOutlet UIButton *btnClkSrc441kHz;
@property (weak, nonatomic) IBOutlet UIButton *btnClkSrc48kHz;
@property (weak, nonatomic) IBOutlet UIButton *btnClkSrcUsbAudio;
@property (weak, nonatomic) IBOutlet UILabel *lblSampleRate;
@property (weak, nonatomic) IBOutlet UILabel *lblUsbMessage;

// Setup page 2
@property (weak, nonatomic) IBOutlet UIView *viewSetupPage2;
@property (weak, nonatomic) IBOutlet UIButton *btnSine100Hz;
@property (weak, nonatomic) IBOutlet UIButton *btnSine1kHz;
@property (weak, nonatomic) IBOutlet UIButton *btnSine10kHz;
@property (weak, nonatomic) IBOutlet UIButton *btnPinkNoise;
@property (weak, nonatomic) IBOutlet UIButton *btnSigGenAux1;
@property (weak, nonatomic) IBOutlet UIButton *btnSigGenAux2;
@property (weak, nonatomic) IBOutlet UIButton *btnSigGenAux3;
@property (weak, nonatomic) IBOutlet UIButton *btnSigGenAux4;
@property (weak, nonatomic) IBOutlet UIButton *btnSigGenGp1;
@property (weak, nonatomic) IBOutlet UIButton *btnSigGenGp2;
@property (weak, nonatomic) IBOutlet UIButton *btnSigGenGp3;
@property (weak, nonatomic) IBOutlet UIButton *btnSigGenGp4;
@property (weak, nonatomic) IBOutlet UIButton *btnSigGenMain;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobSigGen;
@property (nonatomic, retain) DACircularProgressView *cpvSigGen;
@property (weak, nonatomic) IBOutlet UILabel *lblSigGen;
@property (weak, nonatomic) IBOutlet UIButton *btnSigGenOnOff;
@property (weak, nonatomic) IBOutlet UILabel *lblFirmwareVersion;

// Scenes panel
@property (weak, nonatomic) IBOutlet UIView *viewScenes;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewScenesBg;
@property (weak, nonatomic) IBOutlet UIButton *btnViewScenesInit;
@property (weak, nonatomic) IBOutlet UITableView *tableViewScenes;
@property (weak, nonatomic) IBOutlet UIButton *btnSceneFlashRom;
@property (weak, nonatomic) IBOutlet UIButton *btnSceneUsb;

// USB Audio out panel
@property (weak, nonatomic) IBOutlet UIView *viewUsbAudioOut;
@property (weak, nonatomic) IBOutlet UIButton *btnUsbAudioOutputL;
@property (weak, nonatomic) IBOutlet UIButton *btnUsbAudioOutputR;
@property (weak, nonatomic) IBOutlet UIView *viewUsbAudioOutAssign;
@property (weak, nonatomic) IBOutlet UIButton *btnUsbAudioOutCh1;
@property (weak, nonatomic) IBOutlet UIButton *btnUsbAudioOutCh2;
@property (weak, nonatomic) IBOutlet UIButton *btnUsbAudioOutCh3;
@property (weak, nonatomic) IBOutlet UIButton *btnUsbAudioOutCh4;
@property (weak, nonatomic) IBOutlet UIButton *btnUsbAudioOutCh5;
@property (weak, nonatomic) IBOutlet UIButton *btnUsbAudioOutCh6;
@property (weak, nonatomic) IBOutlet UIButton *btnUsbAudioOutCh7;
@property (weak, nonatomic) IBOutlet UIButton *btnUsbAudioOutCh8;
@property (weak, nonatomic) IBOutlet UIButton *btnUsbAudioOutCh9;
@property (weak, nonatomic) IBOutlet UIButton *btnUsbAudioOutCh10;
@property (weak, nonatomic) IBOutlet UIButton *btnUsbAudioOutCh11;
@property (weak, nonatomic) IBOutlet UIButton *btnUsbAudioOutCh12;
@property (weak, nonatomic) IBOutlet UIButton *btnUsbAudioOutCh13;
@property (weak, nonatomic) IBOutlet UIButton *btnUsbAudioOutCh14;
@property (weak, nonatomic) IBOutlet UIButton *btnUsbAudioOutCh15;
@property (weak, nonatomic) IBOutlet UIButton *btnUsbAudioOutCh16;
@property (weak, nonatomic) IBOutlet UIButton *btnUsbAudioOutCh17;
@property (weak, nonatomic) IBOutlet UIButton *btnUsbAudioOutCh18;
@property (weak, nonatomic) IBOutlet UIButton *btnUsbAudioOutAux1;
@property (weak, nonatomic) IBOutlet UIButton *btnUsbAudioOutAux2;
@property (weak, nonatomic) IBOutlet UIButton *btnUsbAudioOutAux3;
@property (weak, nonatomic) IBOutlet UIButton *btnUsbAudioOutAux4;
@property (weak, nonatomic) IBOutlet UIButton *btnUsbAudioOutGp1;
@property (weak, nonatomic) IBOutlet UIButton *btnUsbAudioOutGp2;
@property (weak, nonatomic) IBOutlet UIButton *btnUsbAudioOutGp3;
@property (weak, nonatomic) IBOutlet UIButton *btnUsbAudioOutGp4;
@property (weak, nonatomic) IBOutlet UIButton *btnUsbAudioOutNull;
@property (weak, nonatomic) IBOutlet UIButton *btnUsbAudioOutMulti1;
@property (weak, nonatomic) IBOutlet UIButton *btnUsbAudioOutMulti2;
@property (weak, nonatomic) IBOutlet UIButton *btnUsbAudioOutMulti3;
@property (weak, nonatomic) IBOutlet UIButton *btnUsbAudioOutMulti4;
@property (weak, nonatomic) IBOutlet UIButton *btnUsbAudioOutMainL;
@property (weak, nonatomic) IBOutlet UIButton *btnUsbAudioOutMainR;

// Top panel
@property (weak, nonatomic) IBOutlet UIView *viewTopPanel;
@property (weak, nonatomic) IBOutlet UIButton *btnCh1_8;
@property (weak, nonatomic) IBOutlet UIButton *btnCh9_16;
@property (weak, nonatomic) IBOutlet UIButton *btnUsbAudio;
@property (weak, nonatomic) IBOutlet UIButton *btnAux1_4;
@property (weak, nonatomic) IBOutlet UIButton *btnGp1_4;
@property (weak, nonatomic) IBOutlet UIButton *btnEfx1;
@property (weak, nonatomic) IBOutlet UIButton *btnEfx2;
@property (weak, nonatomic) IBOutlet UIButton *btnMulti1_4;
@property (weak, nonatomic) IBOutlet UIButton *btnUsbAudioOut;
@property (weak, nonatomic) IBOutlet UIButton *btnCtrlRm;
@property (weak, nonatomic) IBOutlet UIButton *btnMain;

@property (weak, nonatomic) IBOutlet UIButton *btnMeterCh1_8;
@property (weak, nonatomic) IBOutlet UIButton *btnMeterCh9_16;
@property (weak, nonatomic) IBOutlet UIButton *btnMeterUsbAudio;
@property (weak, nonatomic) IBOutlet UIButton *btnMeterAux1_4;
@property (weak, nonatomic) IBOutlet UIButton *btnMeterGp1_4;
@property (weak, nonatomic) IBOutlet UIButton *btnMeterEfx1;
@property (weak, nonatomic) IBOutlet UIButton *btnMeterEfx2;
@property (weak, nonatomic) IBOutlet UIButton *btnMeterMulti1_4;
@property (weak, nonatomic) IBOutlet UIButton *btnMeterUsbAudioOut;
@property (weak, nonatomic) IBOutlet UIButton *btnMeterCtrlRm;
@property (weak, nonatomic) IBOutlet UIButton *btnMeterMain;

@property (nonatomic,retain) JEProgressView *pageCh1_8Meter1;
@property (nonatomic,retain) JEProgressView *pageCh1_8Meter2;
@property (nonatomic,retain) JEProgressView *pageCh1_8Meter3;
@property (nonatomic,retain) JEProgressView *pageCh1_8Meter4;
@property (nonatomic,retain) JEProgressView *pageCh1_8Meter5;
@property (nonatomic,retain) JEProgressView *pageCh1_8Meter6;
@property (nonatomic,retain) JEProgressView *pageCh1_8Meter7;
@property (nonatomic,retain) JEProgressView *pageCh1_8Meter8;
@property (nonatomic,retain) JEProgressView *pageCh9_16Meter1;
@property (nonatomic,retain) JEProgressView *pageCh9_16Meter2;
@property (nonatomic,retain) JEProgressView *pageCh9_16Meter3;
@property (nonatomic,retain) JEProgressView *pageCh9_16Meter4;
@property (nonatomic,retain) JEProgressView *pageCh9_16Meter5;
@property (nonatomic,retain) JEProgressView *pageCh9_16Meter6;
@property (nonatomic,retain) JEProgressView *pageCh9_16Meter7;
@property (nonatomic,retain) JEProgressView *pageCh9_16Meter8;
@property (nonatomic,retain) JEProgressView *pageUsbAudioMeterL;
@property (nonatomic,retain) JEProgressView *pageUsbAudioMeterR;
@property (nonatomic,retain) JEProgressView *pageAux1_4Meter1;
@property (nonatomic,retain) JEProgressView *pageAux1_4Meter2;
@property (nonatomic,retain) JEProgressView *pageAux1_4Meter3;
@property (nonatomic,retain) JEProgressView *pageAux1_4Meter4;
@property (nonatomic,retain) JEProgressView *pageGp1_4Meter1;
@property (nonatomic,retain) JEProgressView *pageGp1_4Meter2;
@property (nonatomic,retain) JEProgressView *pageGp1_4Meter3;
@property (nonatomic,retain) JEProgressView *pageGp1_4Meter4;
@property (nonatomic,retain) JEProgressView *pageEfx1MeterL;
@property (nonatomic,retain) JEProgressView *pageEfx1MeterR;
@property (nonatomic,retain) JEProgressView *pageEfx2MeterL;
@property (nonatomic,retain) JEProgressView *pageEfx2MeterR;
@property (nonatomic,retain) JEProgressView *pageMulti1_4Meter1;
@property (nonatomic,retain) JEProgressView *pageMulti1_4Meter2;
@property (nonatomic,retain) JEProgressView *pageMulti1_4Meter3;
@property (nonatomic,retain) JEProgressView *pageMulti1_4Meter4;
@property (nonatomic,retain) JEProgressView *pageUsbAudioOutMeterL;
@property (nonatomic,retain) JEProgressView *pageUsbAudioOutMeterR;
@property (nonatomic,retain) JEProgressView *pageCtrlRmMeterL;
@property (nonatomic,retain) JEProgressView *pageCtrlRmMeterR;
@property (nonatomic,retain) JEProgressView *pageMainMeterL;
@property (nonatomic,retain) JEProgressView *pageMainMeterR;

// Main panel
@property (weak, nonatomic) IBOutlet UIView *viewMainPanel;
@property (weak, nonatomic) IBOutlet UIView *viewSlider1;
@property (weak, nonatomic) IBOutlet UIView *viewSlider2;
@property (weak, nonatomic) IBOutlet UIView *viewSlider3;
@property (weak, nonatomic) IBOutlet UIView *viewSlider4;
@property (weak, nonatomic) IBOutlet UIView *viewSlider5;
@property (weak, nonatomic) IBOutlet UIView *viewSlider6;
@property (weak, nonatomic) IBOutlet UIView *viewSlider7;
@property (weak, nonatomic) IBOutlet UIView *viewSlider8;
@property (weak, nonatomic) IBOutlet UIView *viewSliderMain;

@property (weak, nonatomic) IBOutlet UIButton *btnSliderPanEq1;
@property (weak, nonatomic) IBOutlet UIButton *btnSlider1Solo;
@property (weak, nonatomic) IBOutlet UIButton *btnSlider1On;
@property (weak, nonatomic) IBOutlet UILabel *lblSlider1;
@property (weak, nonatomic) IBOutlet UILabel *lblSlider1Value;
@property (weak, nonatomic) IBOutlet UIButton *btnSlider1Meter;
@property (weak, nonatomic) IBOutlet UITextField *txtSlider1;
@property (weak, nonatomic) IBOutlet UIButton *btnSliderPanEq2;
@property (weak, nonatomic) IBOutlet UIButton *btnSlider2Solo;
@property (weak, nonatomic) IBOutlet UIButton *btnSlider2On;
@property (weak, nonatomic) IBOutlet UILabel *lblSlider2;
@property (weak, nonatomic) IBOutlet UILabel *lblSlider2Value;
@property (weak, nonatomic) IBOutlet UIButton *btnSlider2Meter;
@property (weak, nonatomic) IBOutlet UITextField *txtSlider2;
@property (weak, nonatomic) IBOutlet UIButton *btnSliderPanEq3;
@property (weak, nonatomic) IBOutlet UIButton *btnSlider3Solo;
@property (weak, nonatomic) IBOutlet UIButton *btnSlider3On;
@property (weak, nonatomic) IBOutlet UILabel *lblSlider3;
@property (weak, nonatomic) IBOutlet UILabel *lblSlider3Value;
@property (weak, nonatomic) IBOutlet UIButton *btnSlider3Meter;
@property (weak, nonatomic) IBOutlet UITextField *txtSlider3;
@property (weak, nonatomic) IBOutlet UIButton *btnSliderPanEq4;
@property (weak, nonatomic) IBOutlet UIButton *btnSlider4Solo;
@property (weak, nonatomic) IBOutlet UIButton *btnSlider4On;
@property (weak, nonatomic) IBOutlet UILabel *lblSlider4;
@property (weak, nonatomic) IBOutlet UILabel *lblSlider4Value;
@property (weak, nonatomic) IBOutlet UIButton *btnSlider4Meter;
@property (weak, nonatomic) IBOutlet UITextField *txtSlider4;
@property (weak, nonatomic) IBOutlet UIButton *btnSliderPanEq5;
@property (weak, nonatomic) IBOutlet UIButton *btnSlider5Solo;
@property (weak, nonatomic) IBOutlet UIButton *btnSlider5On;
@property (weak, nonatomic) IBOutlet UILabel *lblSlider5;
@property (weak, nonatomic) IBOutlet UILabel *lblSlider5Value;
@property (weak, nonatomic) IBOutlet UITextField *txtSlider5;
@property (weak, nonatomic) IBOutlet UIButton *btnSlider5Meter;
@property (weak, nonatomic) IBOutlet UIButton *btnSliderPanEq6;
@property (weak, nonatomic) IBOutlet UIButton *btnSlider6Solo;
@property (weak, nonatomic) IBOutlet UIButton *btnSlider6On;
@property (weak, nonatomic) IBOutlet UILabel *lblSlider6;
@property (weak, nonatomic) IBOutlet UILabel *lblSlider6Value;
@property (weak, nonatomic) IBOutlet UIButton *btnSlider6Meter;
@property (weak, nonatomic) IBOutlet UITextField *txtSlider6;
@property (weak, nonatomic) IBOutlet UIButton *btnSliderPanEq7;
@property (weak, nonatomic) IBOutlet UIButton *btnSlider7Solo;
@property (weak, nonatomic) IBOutlet UIButton *btnSlider7On;
@property (weak, nonatomic) IBOutlet UILabel *lblSlider7;
@property (weak, nonatomic) IBOutlet UILabel *lblSlider7Value;
@property (weak, nonatomic) IBOutlet UIButton *btnSlider7Meter;
@property (weak, nonatomic) IBOutlet UITextField *txtSlider7;
@property (weak, nonatomic) IBOutlet UIButton *btnSliderPanEq8;
@property (weak, nonatomic) IBOutlet UIButton *btnSlider8Solo;
@property (weak, nonatomic) IBOutlet UIButton *btnSlider8On;
@property (weak, nonatomic) IBOutlet UIImageView *imgSlider8;
@property (weak, nonatomic) IBOutlet UILabel *lblSlider8;
@property (weak, nonatomic) IBOutlet UILabel *lblSlider8Value;
@property (weak, nonatomic) IBOutlet UIButton *btnSlider8Meter;
@property (weak, nonatomic) IBOutlet UITextField *txtSlider8;
@property (weak, nonatomic) IBOutlet UIButton *btnSliderPanEqMain;
@property (weak, nonatomic) IBOutlet UIButton *btnSliderMainMono;
@property (weak, nonatomic) IBOutlet UIButton *btnSliderMainOn;
@property (weak, nonatomic) IBOutlet UILabel *lblSliderMain;
@property (weak, nonatomic) IBOutlet UILabel *lblSliderMainValue;
@property (weak, nonatomic) IBOutlet UIButton *btnSliderMainMeter;
@property (weak, nonatomic) IBOutlet UITextField *txtSliderMain;

// PAN sliders
@property (nonatomic,retain) UISlider *sliderPan1;
@property (nonatomic,retain) UISlider *sliderPan2;
@property (nonatomic,retain) UISlider *sliderPan3;
@property (nonatomic,retain) UISlider *sliderPan4;
@property (nonatomic,retain) UISlider *sliderPan5;
@property (nonatomic,retain) UISlider *sliderPan6;
@property (nonatomic,retain) UISlider *sliderPan7;
@property (nonatomic,retain) UISlider *sliderPan8;
@property (nonatomic,retain) JEProgressView *meterPan1;
@property (nonatomic,retain) JEProgressView *meterPan2;
@property (nonatomic,retain) JEProgressView *meterPan3;
@property (nonatomic,retain) JEProgressView *meterPan4;
@property (nonatomic,retain) JEProgressView *meterPan5;
@property (nonatomic,retain) JEProgressView *meterPan6;
@property (nonatomic,retain) JEProgressView *meterPan7;
@property (nonatomic,retain) JEProgressView *meterPan8;
@property (weak, nonatomic) IBOutlet UILabel *lblPanValue1;
@property (weak, nonatomic) IBOutlet UILabel *lblPanValue2;
@property (weak, nonatomic) IBOutlet UILabel *lblPanValue3;
@property (weak, nonatomic) IBOutlet UILabel *lblPanValue4;
@property (weak, nonatomic) IBOutlet UILabel *lblPanValue5;
@property (weak, nonatomic) IBOutlet UILabel *lblPanValue6;
@property (weak, nonatomic) IBOutlet UILabel *lblPanValue7;
@property (weak, nonatomic) IBOutlet UILabel *lblPanValue8;

// Mode button
@property (weak, nonatomic) IBOutlet UILabel *lblOrderFirst1;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderSecond1;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderThird1;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderFirst2;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderSecond2;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderThird2;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderFirst3;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderSecond3;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderThird3;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderFirst4;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderSecond4;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderThird4;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderFirst5;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderSecond5;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderThird5;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderFirst6;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderSecond6;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderThird6;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderFirst7;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderSecond7;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderThird7;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderFirst8;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderSecond8;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderThird8;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderFirstMain;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderSecondMain;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderThirdMain;
@property (weak, nonatomic) IBOutlet UILabel *lblDelay1;
@property (weak, nonatomic) IBOutlet UILabel *lblDelay2;
@property (weak, nonatomic) IBOutlet UILabel *lblDelay3;
@property (weak, nonatomic) IBOutlet UILabel *lblDelay4;
@property (weak, nonatomic) IBOutlet UILabel *lblDelay5;
@property (weak, nonatomic) IBOutlet UILabel *lblDelay6;
@property (weak, nonatomic) IBOutlet UILabel *lblDelay7;
@property (weak, nonatomic) IBOutlet UILabel *lblDelay8;
@property (weak, nonatomic) IBOutlet UILabel *lblDelayMain;
@property (weak, nonatomic) IBOutlet UIButton *btnDynG1;
@property (weak, nonatomic) IBOutlet UIButton *btnDynE1;
@property (weak, nonatomic) IBOutlet UIButton *btnDynC1;
@property (weak, nonatomic) IBOutlet UIButton *btnDynL1;
@property (weak, nonatomic) IBOutlet UIButton *btnDynG2;
@property (weak, nonatomic) IBOutlet UIButton *btnDynE2;
@property (weak, nonatomic) IBOutlet UIButton *btnDynC2;
@property (weak, nonatomic) IBOutlet UIButton *btnDynL2;
@property (weak, nonatomic) IBOutlet UIButton *btnDynG3;
@property (weak, nonatomic) IBOutlet UIButton *btnDynE3;
@property (weak, nonatomic) IBOutlet UIButton *btnDynC3;
@property (weak, nonatomic) IBOutlet UIButton *btnDynL3;
@property (weak, nonatomic) IBOutlet UIButton *btnDynG4;
@property (weak, nonatomic) IBOutlet UIButton *btnDynE4;
@property (weak, nonatomic) IBOutlet UIButton *btnDynC4;
@property (weak, nonatomic) IBOutlet UIButton *btnDynL4;
@property (weak, nonatomic) IBOutlet UIButton *btnDynG5;
@property (weak, nonatomic) IBOutlet UIButton *btnDynE5;
@property (weak, nonatomic) IBOutlet UIButton *btnDynC5;
@property (weak, nonatomic) IBOutlet UIButton *btnDynL5;
@property (weak, nonatomic) IBOutlet UIButton *btnDynG6;
@property (weak, nonatomic) IBOutlet UIButton *btnDynE6;
@property (weak, nonatomic) IBOutlet UIButton *btnDynC6;
@property (weak, nonatomic) IBOutlet UIButton *btnDynL6;
@property (weak, nonatomic) IBOutlet UIButton *btnDynG7;
@property (weak, nonatomic) IBOutlet UIButton *btnDynE7;
@property (weak, nonatomic) IBOutlet UIButton *btnDynC7;
@property (weak, nonatomic) IBOutlet UIButton *btnDynL7;
@property (weak, nonatomic) IBOutlet UIButton *btnDynG8;
@property (weak, nonatomic) IBOutlet UIButton *btnDynE8;
@property (weak, nonatomic) IBOutlet UIButton *btnDynC8;
@property (weak, nonatomic) IBOutlet UIButton *btnDynL8;
@property (weak, nonatomic) IBOutlet UIButton *btnDynGMain;
@property (weak, nonatomic) IBOutlet UIButton *btnDynEMain;
@property (weak, nonatomic) IBOutlet UIButton *btnDynCMain;
@property (weak, nonatomic) IBOutlet UIButton *btnDynLMain;

// Phantom Power view
@property (weak, nonatomic) IBOutlet UIView *viewPhantomPower;
@property (weak, nonatomic) IBOutlet UIButton *btnPhantomPowerCh1OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnPhantomPowerCh2OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnPhantomPowerCh3OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnPhantomPowerCh4OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnPhantomPowerCh5OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnPhantomPowerCh6OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnPhantomPowerCh7OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnPhantomPowerCh8OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnPhantomPowerCh9OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnPhantomPowerCh10OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnPhantomPowerCh11OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnPhantomPowerCh12OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnPhantomPowerCh13OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnPhantomPowerCh14OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnPhantomPowerCh15OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnPhantomPowerCh16OnOff;

// Delay view
@property (weak, nonatomic) IBOutlet UIView *viewDelay;
@property (weak, nonatomic) IBOutlet UIImageView *imgDelayBg;
@property (weak, nonatomic) IBOutlet UIButton *btnDelayPage1;
@property (weak, nonatomic) IBOutlet UIButton *btnDelayPage2;
@property (weak, nonatomic) IBOutlet UIButton *btnDelayPage3;

// Delay page 1 view
@property (weak, nonatomic) IBOutlet UIView *viewDelayPage1;
@property (weak, nonatomic) IBOutlet UIButton *btnDelayCh1OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnDelayCh2OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnDelayCh3OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnDelayCh4OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnDelayCh5OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnDelayCh6OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnDelayCh7OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnDelayCh8OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnDelayCh9OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnDelayCh10OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnDelayCh11OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnDelayCh12OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnDelayCh13OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnDelayCh14OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnDelayCh15OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnDelayCh16OnOff;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobDelayCh1;
@property (nonatomic, retain) DACircularProgressView *cpvDelayCh1;
@property (weak, nonatomic) IBOutlet UILabel *lblDelayCh1;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobDelayCh2;
@property (nonatomic, retain) DACircularProgressView *cpvDelayCh2;
@property (weak, nonatomic) IBOutlet UILabel *lblDelayCh2;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobDelayCh3;
@property (nonatomic, retain) DACircularProgressView *cpvDelayCh3;
@property (weak, nonatomic) IBOutlet UILabel *lblDelayCh3;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobDelayCh4;
@property (nonatomic, retain) DACircularProgressView *cpvDelayCh4;
@property (weak, nonatomic) IBOutlet UILabel *lblDelayCh4;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobDelayCh5;
@property (nonatomic, retain) DACircularProgressView *cpvDelayCh5;
@property (weak, nonatomic) IBOutlet UILabel *lblDelayCh5;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobDelayCh6;
@property (nonatomic, retain) DACircularProgressView *cpvDelayCh6;
@property (weak, nonatomic) IBOutlet UILabel *lblDelayCh6;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobDelayCh7;
@property (nonatomic, retain) DACircularProgressView *cpvDelayCh7;
@property (weak, nonatomic) IBOutlet UILabel *lblDelayCh7;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobDelayCh8;
@property (nonatomic, retain) DACircularProgressView *cpvDelayCh8;
@property (weak, nonatomic) IBOutlet UILabel *lblDelayCh8;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobDelayCh9;
@property (nonatomic, retain) DACircularProgressView *cpvDelayCh9;
@property (weak, nonatomic) IBOutlet UILabel *lblDelayCh9;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobDelayCh10;
@property (nonatomic, retain) DACircularProgressView *cpvDelayCh10;
@property (weak, nonatomic) IBOutlet UILabel *lblDelayCh10;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobDelayCh11;
@property (nonatomic, retain) DACircularProgressView *cpvDelayCh11;
@property (weak, nonatomic) IBOutlet UILabel *lblDelayCh11;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobDelayCh12;
@property (nonatomic, retain) DACircularProgressView *cpvDelayCh12;
@property (weak, nonatomic) IBOutlet UILabel *lblDelayCh12;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobDelayCh13;
@property (nonatomic, retain) DACircularProgressView *cpvDelayCh13;
@property (weak, nonatomic) IBOutlet UILabel *lblDelayCh13;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobDelayCh14;
@property (nonatomic, retain) DACircularProgressView *cpvDelayCh14;
@property (weak, nonatomic) IBOutlet UILabel *lblDelayCh14;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobDelayCh15;
@property (nonatomic, retain) DACircularProgressView *cpvDelayCh15;
@property (weak, nonatomic) IBOutlet UILabel *lblDelayCh15;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobDelayCh16;
@property (nonatomic, retain) DACircularProgressView *cpvDelayCh16;
@property (weak, nonatomic) IBOutlet UILabel *lblDelayCh16;

// Delay page 2 view
@property (weak, nonatomic) IBOutlet UIView *viewDelayPage2;
@property (weak, nonatomic) IBOutlet UIButton *btnDelayCh17OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnDelayCh18OnOff;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobDelayCh17;
@property (nonatomic, retain) DACircularProgressView *cpvDelayCh17;
@property (weak, nonatomic) IBOutlet UILabel *lblDelayCh17;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobDelayCh18;
@property (nonatomic, retain) DACircularProgressView *cpvDelayCh18;
@property (weak, nonatomic) IBOutlet UILabel *lblDelayCh18;

// Delay page 3 view
@property (weak, nonatomic) IBOutlet UIView *viewDelayPage3;
@property (weak, nonatomic) IBOutlet UIButton *btnDelayMt1OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnDelayMt2OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnDelayMt3OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnDelayMt4OnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnDelayMainOnOff;
@property (weak, nonatomic) IBOutlet UIButton *btnDelayTime;
@property (weak, nonatomic) IBOutlet UIButton *btnDelayMeter;
@property (weak, nonatomic) IBOutlet UIButton *btnDelayFeet;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobDelayMt1;
@property (nonatomic, retain) DACircularProgressView *cpvDelayMt1;
@property (weak, nonatomic) IBOutlet UILabel *lblDelayMt1;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobDelayMt2;
@property (nonatomic, retain) DACircularProgressView *cpvDelayMt2;
@property (weak, nonatomic) IBOutlet UILabel *lblDelayMt2;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobDelayMt3;
@property (nonatomic, retain) DACircularProgressView *cpvDelayMt3;
@property (weak, nonatomic) IBOutlet UILabel *lblDelayMt3;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobDelayMt4;
@property (nonatomic, retain) DACircularProgressView *cpvDelayMt4;
@property (weak, nonatomic) IBOutlet UILabel *lblDelayMt4;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobDelayMain;
@property (nonatomic, retain) DACircularProgressView *cpvDelayMain;
@property (weak, nonatomic) IBOutlet UILabel *lblDelayMainValue;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobDelayTemp;
@property (nonatomic, retain) DACircularProgressView *cpvDelayTemp;
@property (weak, nonatomic) IBOutlet UILabel *lblDelayTemp;

// Order view
@property (weak, nonatomic) IBOutlet UIView *viewOrder;
@property (weak, nonatomic) IBOutlet UIImageView *imgOrderBg;
@property (weak, nonatomic) IBOutlet UIButton *btnOrderPage1;
@property (weak, nonatomic) IBOutlet UIButton *btnOrderPage2;
@property (weak, nonatomic) IBOutlet UIButton *btnOrderPage3;

// Order picker view
@property (weak, nonatomic) IBOutlet UIView *viewOrderPickerCtrl;
@property (weak, nonatomic) IBOutlet UIView *viewOrderPickerMain;
@property (weak, nonatomic) IBOutlet UIView *viewOrderPickerMulti;

// Order page 1 view
@property (weak, nonatomic) IBOutlet UIView *viewOrderPage1;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderCh1First;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderCh1Second;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderCh1Third;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderCh2First;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderCh2Second;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderCh2Third;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderCh3First;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderCh3Second;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderCh3Third;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderCh4First;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderCh4Second;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderCh4Third;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderCh5First;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderCh5Second;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderCh5Third;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderCh6First;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderCh6Second;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderCh6Third;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderCh7First;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderCh7Second;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderCh7Third;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderCh8First;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderCh8Second;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderCh8Third;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderCh9First;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderCh9Second;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderCh9Third;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderCh10First;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderCh10Second;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderCh10Third;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderCh11First;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderCh11Second;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderCh11Third;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderCh12First;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderCh12Second;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderCh12Third;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderCh13First;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderCh13Second;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderCh13Third;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderCh14First;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderCh14Second;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderCh14Third;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderCh15First;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderCh15Second;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderCh15Third;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderCh16First;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderCh16Second;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderCh16Third;

// Order page 2 view
@property (weak, nonatomic) IBOutlet UIView *viewOrderPage2;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderCh17First;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderCh17Second;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderCh17Third;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderCh18First;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderCh18Second;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderCh18Third;

// Order page 3 view
@property (weak, nonatomic) IBOutlet UIView *viewOrderPage3;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderMt1First;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderMt1Second;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderMt1Third;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderMt2First;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderMt2Second;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderMt2Third;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderMt3First;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderMt3Second;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderMt3Third;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderMt4First;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderMt4Second;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderMt4Third;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderMainFirst;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderMainSecond;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderMainThird;

// Sliders
@property (nonatomic,retain) UISlider *slider1;
@property (nonatomic,retain) UISlider *slider2;
@property (nonatomic,retain) UISlider *slider3;
@property (nonatomic,retain) UISlider *slider4;
@property (nonatomic,retain) UISlider *slider5;
@property (nonatomic,retain) UISlider *slider6;
@property (nonatomic,retain) UISlider *slider7;
@property (nonatomic,retain) UISlider *slider8;
@property (nonatomic,retain) UISlider *sliderMain;

// Meters
@property (nonatomic,retain) JEProgressView *meter1;
@property (nonatomic,retain) JEProgressView *meter2;
@property (nonatomic,retain) JEProgressView *meter3;
@property (nonatomic,retain) JEProgressView *meter4;
@property (nonatomic,retain) JEProgressView *meter5;
@property (nonatomic,retain) JEProgressView *meter6;
@property (nonatomic,retain) JEProgressView *meter7;
@property (nonatomic,retain) JEProgressView *meter8;
@property (nonatomic,retain) JEProgressView *meterCtrlRmL;
@property (nonatomic,retain) JEProgressView *meterCtrlRmR;
@property (nonatomic,retain) JEProgressView *meterMainL;
@property (nonatomic,retain) JEProgressView *meterMainR;

// Aux & Group sends panels
@property (nonatomic,retain) AuxSendViewController *auxSendController;
@property (nonatomic,retain) GroupSendViewController *groupSendController;
@property (nonatomic,retain) EffectViewController *effectController;
@property (nonatomic,retain) GeqPeqViewController *geqPeqController;
@property (nonatomic,retain) EqDynDelayViewController *eqDynDelayController;
@property (nonatomic,retain) DynViewController *dynController;

// Label edit view
@property (weak, nonatomic) IBOutlet UIView *viewLabelEdit;
@property (weak, nonatomic) IBOutlet UILabel *lblLabelName;
@property (weak, nonatomic) IBOutlet UITextField *txtLabelName;

// Actions
- (IBAction)onBtnMeterCh1_8:(id)sender;
- (IBAction)onBtnMeterCh9_16:(id)sender;
- (IBAction)onBtnMeterUsbAudio:(id)sender;
- (IBAction)onBtnMeterAux1_4:(id)sender;
- (IBAction)onBtnMeterGp1_4:(id)sender;
- (IBAction)onBtnMeterEfx1:(id)sender;
- (IBAction)onBtnMeterEfx2:(id)sender;
- (IBAction)onBtnMeterMulti1_4:(id)sender;
- (IBAction)onBtnMeterCtrlRm:(id)sender;
- (IBAction)onBtnMeterMain:(id)sender;
- (IBAction)onBtnCh1_8:(id)sender;
- (IBAction)onBtnCh9_16:(id)sender;
- (IBAction)onBtnUsbAudio:(id)sender;
- (IBAction)onBtnAux1_4:(id)sender;
- (IBAction)onBtnGp1_4:(id)sender;
- (IBAction)onBtnEfx1:(id)sender;
- (IBAction)onBtnEfx2:(id)sender;
- (IBAction)onBtnMulti1_4:(id)sender;
- (IBAction)onBtnUsbAudioOut:(id)sender;
- (IBAction)onBtnCtrlRm:(id)sender;
- (IBAction)onBtnMain:(id)sender;
- (IBAction)onBtnSlider1Solo:(id)sender;
- (IBAction)onBtnSlider1On:(id)sender;
- (IBAction)onBtnSlider1Meter:(id)sender;
- (IBAction)onBtnSlider2Solo:(id)sender;
- (IBAction)onBtnSlider2On:(id)sender;
- (IBAction)onBtnSlider2Meter:(id)sender;
- (IBAction)onBtnSlider3Solo:(id)sender;
- (IBAction)onBtnSlider3On:(id)sender;
- (IBAction)onBtnSlider3Meter:(id)sender;
- (IBAction)onBtnSlider4Solo:(id)sender;
- (IBAction)onBtnSlider4On:(id)sender;
- (IBAction)onBtnSlider4Meter:(id)sender;
- (IBAction)onBtnSlider5Solo:(id)sender;
- (IBAction)onBtnSlider5On:(id)sender;
- (IBAction)onBtnSlider5Meter:(id)sender;
- (IBAction)onBtnSlider6Solo:(id)sender;
- (IBAction)onBtnSlider6On:(id)sender;
- (IBAction)onBtnSlider6Meter:(id)sender;
- (IBAction)onBtnSlider7Solo:(id)sender;
- (IBAction)onBtnSlider7On:(id)sender;
- (IBAction)onBtnSlider7Meter:(id)sender;
- (IBAction)onBtnSlider8Solo:(id)sender;
- (IBAction)onBtnSlider8On:(id)sender;
- (IBAction)onBtnSlider8Meter:(id)sender;
- (IBAction)onBtnSliderMainMono:(id)sender;
- (IBAction)onBtnSliderMainOn:(id)sender;
- (IBAction)onBtnSliderMainMeter:(id)sender;

- (IBAction)onBtnPanEq1:(id)sender;
- (IBAction)onBtnPanEq2:(id)sender;
- (IBAction)onBtnPanEq3:(id)sender;
- (IBAction)onBtnPanEq4:(id)sender;
- (IBAction)onBtnPanEq5:(id)sender;
- (IBAction)onBtnPanEq6:(id)sender;
- (IBAction)onBtnPanEq7:(id)sender;
- (IBAction)onBtnPanEq8:(id)sender;
- (IBAction)onBtnPanEqMain:(id)sender;

- (IBAction)onBtnModeSelectDown:(id)sender;
- (IBAction)onBtnModeSelectUp:(id)sender;
- (IBAction)onBtnSetup:(id)sender;

// Usb Audio Out actions
- (IBAction)onBtnUsbAudioOutputL:(id)sender;
- (IBAction)onBtnUsbAudioOutoutR:(id)sender;
- (IBAction)onBtnUsbAudioOutCh1:(id)sender;
- (IBAction)onBtnUsbAudioOutCh2:(id)sender;
- (IBAction)onBtnUsbAudioOutCh3:(id)sender;
- (IBAction)onBtnUsbAudioOutCh4:(id)sender;
- (IBAction)onBtnUsbAudioOutCh5:(id)sender;
- (IBAction)onBtnUsbAudioOutCh6:(id)sender;
- (IBAction)onBtnUsbAudioOutCh7:(id)sender;
- (IBAction)onBtnUsbAudioOutCh8:(id)sender;
- (IBAction)onBtnUsbAudioOutCh9:(id)sender;
- (IBAction)onBtnUsbAudioOutCh10:(id)sender;
- (IBAction)onBtnUsbAudioOutCh11:(id)sender;
- (IBAction)onBtnUsbAudioOutCh12:(id)sender;
- (IBAction)onBtnUsbAudioOutCh13:(id)sender;
- (IBAction)onBtnUsbAudioOutCh14:(id)sender;
- (IBAction)onBtnUsbAudioOutCh15:(id)sender;
- (IBAction)onBtnUsbAudioOutCh16:(id)sender;
- (IBAction)onBtnUsbAudioOutCh17:(id)sender;
- (IBAction)onBtnUsbAudioOutCh18:(id)sender;
- (IBAction)onBtnUsbAudioOutAux1:(id)sender;
- (IBAction)onBtnUsbAudioOutAux2:(id)sender;
- (IBAction)onBtnUsbAudioOutAux3:(id)sender;
- (IBAction)onBtnUsbAudioOutAux4:(id)sender;
- (IBAction)onBtnUsbAudioOutGp1:(id)sender;
- (IBAction)onBtnUsbAudioOutGp2:(id)sender;
- (IBAction)onBtnUsbAudioOutGp3:(id)sender;
- (IBAction)onBtnUsbAudioOutGp4:(id)sender;
- (IBAction)onBtnUsbAudioOutNull:(id)sender;
- (IBAction)onBtnUsbAudioOutMulti1:(id)sender;
- (IBAction)onBtnUsbAudioOutMulti2:(id)sender;
- (IBAction)onBtnUsbAudioOutMulti3:(id)sender;
- (IBAction)onBtnUsbAudioOutMulti4:(id)sender;
- (IBAction)onBtnUsbAudioOutMainL:(id)sender;
- (IBAction)onBtnUsbAudioOutMainR:(id)sender;

// Setup actions
- (IBAction)onBtnSetupPage1:(id)sender;
- (IBAction)onBtnSetupPage2:(id)sender;
- (IBAction)onSwitchSetupLocate:(id)sender;
- (IBAction)onBtnSetupRefresh:(id)sender;
- (IBAction)onBtnSetupClockEnter:(id)sender;
- (IBAction)onBtnSetupClockSource441kHz:(id)sender;
- (IBAction)onBtnSetupClockSource48kHz:(id)sender;
- (IBAction)onBtnSetupClockSourceUsbAudio:(id)sender;
- (IBAction)onBtnSine100Hz:(id)sender;
- (IBAction)onBtnSine1kHz:(id)sender;
- (IBAction)onBtnSine10kHz:(id)sender;
- (IBAction)onBtnPinkNoise:(id)sender;
- (IBAction)onBtnSigGenAux1:(id)sender;
- (IBAction)onBtnSigGenAux2:(id)sender;
- (IBAction)onBtnSigGenAux3:(id)sender;
- (IBAction)onBtnSigGenAux4:(id)sender;
- (IBAction)onBtnSigGenGp1:(id)sender;
- (IBAction)onBtnSigGenGp2:(id)sender;
- (IBAction)onBtnSigGenGp3:(id)sender;
- (IBAction)onBtnSigGenGp4:(id)sender;
- (IBAction)onBtnSigGenMain:(id)sender;
- (IBAction)onBtnSigGenOnOff:(id)sender;
- (IBAction)onBtnFirmwareUpdate:(id)sender;

// Scene actions
- (IBAction)onBtnScenesDone:(id)sender;
- (IBAction)onBtnScenesLoad:(id)sender;

- (BOOL)isConnected;
- (void)sendData:(int)command commandType:(COMMAND_TYPE)commandType dspId:(int)dspId value:(int)value;
- (void)sendData2:(COMMAND_TYPE)commandType value1:(int)value1 value2:(int)value2;
- (void)sendDataStream:(COMMAND_TYPE)commandType value:(const char*)value;
- (void)setSendPage:(PAGE_NUMBER)pageNumber reserve2:(int)value;
- (void)restoreSendPage;
- (void)setAudioName:(int)audioType channelNum:(int)channelNum channelName:(const char*)channelName;
- (void)setFileAccess:(int)device sceneMode:(int)sceneMode channel:(int)channel accessMode:(int)accessMode fileName:(const char*)fileName newFileName:(const char*)newFileName;
- (void)refreshModeButtons;
- (void)showGeqPeqDialog:(int)channel;
- (void)showDynDialog:(int)channel;
- (void)setFileSceneMode:(SceneMode)sceneMode sceneChannel:(SceneChannel)sceneChannel;
- (void)disconnectNetwork;
- (BOOL)sendCommandExists:(int)command dspId:(int)dspId;

@end