//
//  ViewController.m
//  Acapela
//
//  Created by Kevin on 13/7/15.
//  Copyright (c) 2013年 Kevin Phua. All rights reserved.
//

#import "ViewController.h"
#import "LoginViewController.h"
#import "acapela.h"
#import "DspRead.h"
#import <QuartzCore/QuartzCore.h>
#import <CommonCrypto/CommonDigest.h>
#import "JEProgressView.h"

@interface ViewController ()

@end

#define TAG_HEADER                      1000
#define TAG_DATA                        1001

#define SCENE_FONTSIZE                  28
#define SCENE_NUMBER_FONTSIZE           76
#define SCENE_DEFAULT_FONTSIZE          34
#define TOP_PANEL_FONTSIZE              13
#define SLIDER_BTN_FONTSIZE             13
#define SLIDER_METER_BTN_FONTSIZE       12
#define SLIDER_LABEL_FONTSIZE           20
#define SLIDER_VALUE_FONTSIZE           16
#define LEFT_PANEL_PANEQ_FONTSIZE       22
#define LEFT_PANEL_BTN_FONTSIZE         26
#define AUX_SEND_LABEL_FONTSIZE         23

#define SCENE_VIEW_TAG                  100
#define SCENE_FILE_LENGTH               12

#define degreesToRadians(x)             (M_PI * x / 180.0)
#define BUFFER_SIZE                     1024
#define ACAPELA_PORT                    9999

#define UI_UPDATE_INTERVAL              0.3f
#define READ_CMD_INTERVAL               0.01f   // Interval between successive reads
#define CLK_SRC_INTERVAL                0.5f

#define CONNECTING_TIME                 4

enum ClkSrcTrackingType {
    CLK_SRC_NONE = 0,
    CLK_SRC_44_1K,
    CLK_SRC_48K,
    CLK_SRC_USB
};

typedef enum {
    ACCESS_MODE_SAVE = 0,
    ACCESS_MODE_LOAD,
    ACCESS_MODE_RENAME,
    ACCESS_MODE_DELETE
} AccessMode;

enum FileAlertTag {
    FILE_SAVE = 1000,
    FILE_LOAD,
    FILE_RENAME,
    FILE_DELETE,
    FILE_INIT
};

@implementation ViewController
{
    BOOL    _loginViewShown;
    BOOL    _loginDone;
    BOOL    _isAuthenticated;
    BOOL    _slider1Lock;
    BOOL    _slider2Lock;
    BOOL    _slider3Lock;
    BOOL    _slider4Lock;
    BOOL    _slider5Lock;
    BOOL    _slider6Lock;
    BOOL    _slider7Lock;
    BOOL    _slider8Lock;
    BOOL    _sliderMainLock;
    BOOL    _modeEnabled;
    
    // Login
    NSString *_ipAddress;
    NSString *_username;
    NSString *_password;
    
    // UI update timer
    NSTimer *_timer;
    
    // Clock source tracking timer
    NSTimer *_clkSrcTimer;
    int     _clkSrcTracking;
    
    // Scene files
    int     _sceneCount;
    NSMutableArray *_sceneFiles;
    int     _selectedScene;
    NSString *_selectedSceneFile;
    UIAlertView *_sceneLoadAlertView;
    
    // Read command queue
    NSMutableArray *_readCmdQueue;
    
    // Write ACK queue
    NSMutableArray *_writeAckQueue;
    
    // Write sequence number
    unsigned int _seqNum;
    
    // Values
    int     _chOffOn;
    int     _chSoloCtrl;
    int     _chMeterPrePost;
    int     _auxGpOffOn;
    int     _auxGpSoloCtrl;
    int     _auxGpMeterPrePost;
    int     _multi14MeterPrePost;
    int     _digitalInOutCtrl;
    int     _mainCtrl;
    int     _mainMeterPrePost;
    int     _sgSettingCtrl;
    int     _sgAssign;
    int     _gpToMain;
    int     _effectCtrl;
    int     _dspSetClkSrc;
    int     _ch48vCtrl;
    int     _ch1Ctrl, _ch2Ctrl, _ch3Ctrl, _ch4Ctrl, _ch5Ctrl, _ch6Ctrl, _ch7Ctrl, _ch8Ctrl, _ch9Ctrl;
    int     _ch10Ctrl, _ch11Ctrl, _ch12Ctrl, _ch13Ctrl, _ch14Ctrl, _ch15Ctrl, _ch16Ctrl, _ch17Ctrl, _ch18Ctrl;
    int     _multi1Ctrl, _multi2Ctrl, _multi3Ctrl, _multi4Ctrl, _ch17AudioSetting, _ch18AudioSetting;
    int     _ch1DelayTime, _ch2DelayTime, _ch3DelayTime, _ch4DelayTime, _ch5DelayTime, _ch6DelayTime, _ch7DelayTime, _ch8DelayTime, _ch9DelayTime;
    int     _ch10DelayTime, _ch11DelayTime, _ch12DelayTime, _ch13DelayTime, _ch14DelayTime, _ch15DelayTime, _ch16DelayTime, _ch17DelayTime, _ch18DelayTime;
    int     _multi1DelayTime, _multi2DelayTime, _multi3DelayTime, _multi4DelayTime, _mainDelayTime;
    int     _usbAudioOutL, _usbAudioOutR, _usbSelect;
    int     _delayScale;
    
    // File scene
    BOOL    _sceneAlertShown;
    SceneMode _currentSceneMode;
    SceneChannel _currentSceneChannel;
    NSIndexPath *_selectedIndexPath;
    int     _usbMount;
    
    // Send page
    int     _prevSendPage;
    int     _prevSendPageValue;
    int     _curSendPage;
    int     _curSendPageValue;
}

@synthesize viewTopPanel, viewMainPanel;
@synthesize viewSetup, imgSetupBg, btnSetupPage1, btnSetupPage2;
@synthesize viewSetupPage1, lblIpAddress, lblOnline, imgOnline, switchSetupLocate, activityLoading, lblVersion;
@synthesize btnClkSrc441kHz, btnClkSrc48kHz, btnClkSrcUsbAudio, lblSampleRate, lblUsbMessage;
@synthesize viewSetupPage2, btnSine100Hz, btnSine1kHz, btnSine10kHz, btnPinkNoise;
@synthesize btnSigGenAux1, btnSigGenAux2, btnSigGenAux3, btnSigGenAux4;
@synthesize btnSigGenGp1, btnSigGenGp2, btnSigGenGp3, btnSigGenGp4, btnSigGenMain;
@synthesize knobSigGen, cpvSigGen, lblSigGen, btnSigGenOnOff, lblFirmwareVersion;
@synthesize viewScenes, imgViewScenesBg, btnViewScenesInit, tableViewScenes, btnSceneFlashRom, btnSceneUsb;
@synthesize viewSlider1, viewSlider2, viewSlider3, viewSlider4;
@synthesize viewSlider5, viewSlider6, viewSlider7, viewSlider8, viewSliderMain;
@synthesize lblSceneName, lblClkRate;
@synthesize lblCurrentMode, btnModeSelectDown, btnModeSelectUp, btnSetup;
@synthesize btnCh1_8, btnCh9_16, btnUsbAudio, btnAux1_4, btnGp1_4, btnEfx1, btnEfx2;
@synthesize btnMulti1_4, btnUsbAudioOut, btnCtrlRm, btnMain;
@synthesize btnMeterCh1_8,btnMeterCh9_16,btnMeterUsbAudio,btnMeterAux1_4,btnMeterGp1_4;
@synthesize btnMeterEfx1,btnMeterEfx2,btnMeterMulti1_4,btnMeterUsbAudioOut,btnMeterCtrlRm,btnMeterMain;
@synthesize btnSliderPanEq1, btnSlider1Solo, btnSlider1On, lblSlider1, lblSlider1Value, btnSlider1Meter, txtSlider1;
@synthesize btnSliderPanEq2, btnSlider2Solo, btnSlider2On, lblSlider2, lblSlider2Value, btnSlider2Meter, txtSlider2;
@synthesize btnSliderPanEq3, btnSlider3Solo, btnSlider3On, lblSlider3, lblSlider3Value, btnSlider3Meter, txtSlider3;
@synthesize btnSliderPanEq4, btnSlider4Solo, btnSlider4On, lblSlider4, lblSlider4Value, btnSlider4Meter, txtSlider4;
@synthesize btnSliderPanEq5, btnSlider5Solo, btnSlider5On, lblSlider5, lblSlider5Value, btnSlider5Meter, txtSlider5;
@synthesize btnSliderPanEq6, btnSlider6Solo, btnSlider6On, lblSlider6, lblSlider6Value, btnSlider6Meter, txtSlider6;
@synthesize btnSliderPanEq7, btnSlider7Solo, btnSlider7On, lblSlider7, lblSlider7Value, btnSlider7Meter, txtSlider7;
@synthesize btnSliderPanEq8, btnSlider8Solo, btnSlider8On, lblSlider8, lblSlider8Value, btnSlider8Meter, txtSlider8;
@synthesize btnSliderPanEqMain, btnSliderMainMono, btnSliderMainOn, lblSliderMain, lblSliderMainValue, btnSliderMainMeter, txtSliderMain;
@synthesize sliderPan1, sliderPan2, sliderPan3, sliderPan4, sliderPan5, sliderPan6, sliderPan7, sliderPan8;
@synthesize meterPan1, meterPan2, meterPan3, meterPan4, meterPan5, meterPan6, meterPan7, meterPan8;
@synthesize lblPanValue1, lblPanValue2, lblPanValue3, lblPanValue4, lblPanValue5, lblPanValue6, lblPanValue7, lblPanValue8;
@synthesize lblOrderFirst1,lblOrderSecond1,lblOrderThird1,lblOrderFirst2,lblOrderSecond2,lblOrderThird2;
@synthesize lblOrderFirst3,lblOrderSecond3,lblOrderThird3,lblOrderFirst4,lblOrderSecond4,lblOrderThird4;
@synthesize lblOrderFirst5,lblOrderSecond5,lblOrderThird5,lblOrderFirst6,lblOrderSecond6,lblOrderThird6;
@synthesize lblOrderFirst7,lblOrderSecond7,lblOrderThird7,lblOrderFirst8,lblOrderSecond8,lblOrderThird8;
@synthesize lblOrderFirstMain,lblOrderSecondMain,lblOrderThirdMain;
@synthesize lblDelay1,lblDelay2,lblDelay3,lblDelay4,lblDelay5,lblDelay6,lblDelay7,lblDelay8,lblDelayMain;
@synthesize btnDynG1,btnDynE1,btnDynC1,btnDynL1,btnDynG2,btnDynE2,btnDynC2,btnDynL2;
@synthesize btnDynG3,btnDynE3,btnDynC3,btnDynL3,btnDynG4,btnDynE4,btnDynC4,btnDynL4;
@synthesize btnDynG5,btnDynE5,btnDynC5,btnDynL5,btnDynG6,btnDynE6,btnDynC6,btnDynL6;
@synthesize btnDynG7,btnDynE7,btnDynC7,btnDynL7,btnDynG8,btnDynE8,btnDynC8,btnDynL8;
@synthesize btnDynGMain,btnDynEMain,btnDynCMain,btnDynLMain;
@synthesize slider1, slider2, slider3, slider4, slider5, slider6, slider7, slider8, sliderMain, imgSlider8;
@synthesize meter1, meter2, meter3, meter4, meter5, meter6, meter7, meter8, meterCtrlRmL, meterCtrlRmR, meterMainL, meterMainR;
@synthesize pageCh1_8Meter1, pageCh1_8Meter2, pageCh1_8Meter3, pageCh1_8Meter4, pageCh1_8Meter5, pageCh1_8Meter6, pageCh1_8Meter7, pageCh1_8Meter8;
@synthesize pageCh9_16Meter1, pageCh9_16Meter2, pageCh9_16Meter3, pageCh9_16Meter4, pageCh9_16Meter5, pageCh9_16Meter6, pageCh9_16Meter7, pageCh9_16Meter8;
@synthesize pageUsbAudioMeterL, pageUsbAudioMeterR, pageUsbAudioOutMeterL, pageUsbAudioOutMeterR;
@synthesize pageAux1_4Meter1, pageAux1_4Meter2, pageAux1_4Meter3, pageAux1_4Meter4;
@synthesize pageGp1_4Meter1, pageGp1_4Meter2, pageGp1_4Meter3, pageGp1_4Meter4;
@synthesize pageEfx1MeterL, pageEfx1MeterR, pageEfx2MeterL, pageEfx2MeterR;
@synthesize pageCtrlRmMeterL, pageCtrlRmMeterR, pageMainMeterL, pageMainMeterR;
@synthesize pageMulti1_4Meter1, pageMulti1_4Meter2, pageMulti1_4Meter3, pageMulti1_4Meter4;
@synthesize viewLabelEdit, lblLabelName, txtLabelName;
@synthesize auxSendController, groupSendController, effectController, geqPeqController, eqDynDelayController, dynController;
@synthesize viewUsbAudioOut,btnUsbAudioOutputL,btnUsbAudioOutputR;
@synthesize viewUsbAudioOutAssign,btnUsbAudioOutCh1,btnUsbAudioOutCh2,btnUsbAudioOutCh3,btnUsbAudioOutCh4;
@synthesize btnUsbAudioOutCh5,btnUsbAudioOutCh6,btnUsbAudioOutCh7,btnUsbAudioOutCh8,btnUsbAudioOutCh9;
@synthesize btnUsbAudioOutCh10,btnUsbAudioOutCh11,btnUsbAudioOutCh12,btnUsbAudioOutCh13,btnUsbAudioOutCh14;
@synthesize btnUsbAudioOutCh15,btnUsbAudioOutCh16,btnUsbAudioOutCh17,btnUsbAudioOutCh18,btnUsbAudioOutAux1;
@synthesize btnUsbAudioOutAux2,btnUsbAudioOutAux3,btnUsbAudioOutAux4,btnUsbAudioOutGp1,btnUsbAudioOutGp2;
@synthesize btnUsbAudioOutGp3,btnUsbAudioOutGp4,btnUsbAudioOutNull,btnUsbAudioOutMulti1,btnUsbAudioOutMulti2;
@synthesize btnUsbAudioOutMulti3,btnUsbAudioOutMulti4,btnUsbAudioOutMainL,btnUsbAudioOutMainR;
@synthesize viewPhantomPower,btnPhantomPowerCh1OnOff,btnPhantomPowerCh2OnOff,btnPhantomPowerCh3OnOff;
@synthesize btnPhantomPowerCh4OnOff,btnPhantomPowerCh5OnOff,btnPhantomPowerCh6OnOff,btnPhantomPowerCh7OnOff;
@synthesize btnPhantomPowerCh8OnOff,btnPhantomPowerCh9OnOff,btnPhantomPowerCh10OnOff,btnPhantomPowerCh11OnOff;
@synthesize btnPhantomPowerCh12OnOff,btnPhantomPowerCh13OnOff,btnPhantomPowerCh14OnOff,btnPhantomPowerCh15OnOff,btnPhantomPowerCh16OnOff;
@synthesize viewDelay,imgDelayBg,btnDelayPage1,btnDelayPage2,btnDelayPage3;
@synthesize viewDelayPage1,viewDelayPage2,viewDelayPage3;
@synthesize knobDelayCh1,cpvDelayCh1,lblDelayCh1,knobDelayCh2,cpvDelayCh2,lblDelayCh2,knobDelayCh3,cpvDelayCh3;
@synthesize lblDelayCh3,knobDelayCh4,cpvDelayCh4,lblDelayCh4,knobDelayCh5,cpvDelayCh5,lblDelayCh5,knobDelayCh6;
@synthesize cpvDelayCh6,lblDelayCh6,knobDelayCh7,cpvDelayCh7,lblDelayCh7,knobDelayCh8,cpvDelayCh8,lblDelayCh8;
@synthesize knobDelayCh9,cpvDelayCh9,lblDelayCh9,knobDelayCh10,cpvDelayCh10,lblDelayCh10,knobDelayCh11,cpvDelayCh11;
@synthesize lblDelayCh11,knobDelayCh12,cpvDelayCh12,lblDelayCh12,knobDelayCh13,cpvDelayCh13,lblDelayCh13;
@synthesize knobDelayCh14,cpvDelayCh14,lblDelayCh14,knobDelayCh15,cpvDelayCh15,lblDelayCh15,knobDelayCh16;
@synthesize cpvDelayCh16,lblDelayCh16,knobDelayCh17,cpvDelayCh17,lblDelayCh17,knobDelayCh18,cpvDelayCh18,lblDelayCh18;
@synthesize knobDelayMt1,cpvDelayMt1,lblDelayMt1,knobDelayMt2,cpvDelayMt2,lblDelayMt2,knobDelayMt3,cpvDelayMt3;
@synthesize lblDelayMt3,knobDelayMt4,cpvDelayMt4,lblDelayMt4,knobDelayMain,cpvDelayMain,lblDelayMainValue;
@synthesize knobDelayTemp,cpvDelayTemp,lblDelayTemp;
@synthesize btnDelayCh1OnOff,btnDelayCh2OnOff,btnDelayCh3OnOff,btnDelayCh4OnOff,btnDelayCh5OnOff,btnDelayCh6OnOff;
@synthesize btnDelayCh7OnOff,btnDelayCh8OnOff,btnDelayCh9OnOff,btnDelayCh10OnOff,btnDelayCh11OnOff,btnDelayCh12OnOff;
@synthesize btnDelayCh13OnOff,btnDelayCh14OnOff,btnDelayCh15OnOff,btnDelayCh16OnOff,btnDelayCh17OnOff,btnDelayCh18OnOff;
@synthesize btnDelayMt1OnOff,btnDelayMt2OnOff,btnDelayMt3OnOff,btnDelayMt4OnOff,btnDelayMainOnOff,btnDelayTime;
@synthesize btnDelayMeter,btnDelayFeet;
@synthesize viewOrder,imgOrderBg,btnOrderPage1,btnOrderPage2,btnOrderPage3;
@synthesize viewOrderPickerCtrl,viewOrderPickerMain,viewOrderPickerMulti;
@synthesize viewOrderPage1,lblOrderCh1First,lblOrderCh1Second,lblOrderCh1Third,lblOrderCh2First;
@synthesize lblOrderCh2Second,lblOrderCh2Third,lblOrderCh3First,lblOrderCh3Second,lblOrderCh3Third;
@synthesize lblOrderCh4First,lblOrderCh4Second,lblOrderCh4Third,lblOrderCh5First,lblOrderCh5Second;
@synthesize lblOrderCh5Third,lblOrderCh6First,lblOrderCh6Second,lblOrderCh6Third,lblOrderCh7First;
@synthesize lblOrderCh7Second,lblOrderCh7Third,lblOrderCh8First,lblOrderCh8Second,lblOrderCh8Third,lblOrderCh9First;
@synthesize lblOrderCh9Second,lblOrderCh9Third,lblOrderCh10First,lblOrderCh10Second,lblOrderCh10Third;
@synthesize lblOrderCh11First,lblOrderCh11Second,lblOrderCh11Third,lblOrderCh12First,lblOrderCh12Second;
@synthesize lblOrderCh12Third,lblOrderCh13First,lblOrderCh13Second,lblOrderCh13Third,lblOrderCh14First;
@synthesize lblOrderCh14Second,lblOrderCh14Third,lblOrderCh15First,lblOrderCh15Second,lblOrderCh15Third;
@synthesize lblOrderCh16First,lblOrderCh16Second,lblOrderCh16Third;
@synthesize viewOrderPage2,lblOrderCh17First,lblOrderCh17Second,lblOrderCh17Third,lblOrderCh18First,lblOrderCh18Second,lblOrderCh18Third;
@synthesize viewOrderPage3,lblOrderMt1First,lblOrderMt1Second,lblOrderMt1Third,lblOrderMt2First,lblOrderMt2Second,lblOrderMt2Third;
@synthesize lblOrderMt3First,lblOrderMt3Second,lblOrderMt3Third,lblOrderMt4First,lblOrderMt4Second,lblOrderMt4Third;
@synthesize lblOrderMainFirst,lblOrderMainSecond,lblOrderMainThird;


#pragma mark -
#pragma mark Device communication methods

- (void)connectToDevice:(NSNotification *)notification {
    // User cancelled
    if (notification.userInfo == nil) {
        [switchSetupLocate setOn:NO];
        [lblIpAddress setText:@""];
        [lblOnline setText:@"Offline"];
        return;
    }
    
    _ipAddress = (NSString *)[notification.userInfo valueForKey:@"ipAddress"];
    _username = (NSString *)[notification.userInfo valueForKey:@"username"];
    _password = (NSString *)[notification.userInfo valueForKey:@"password"];
    
    _loginDone = NO;
    _loginViewShown = NO;
    _isAuthenticated = NO;
    [self initNetworkCommunication];
}

- (void)initNetworkCommunication {
    NSLog(@"Init network communication...");
    socket = [[GCDAsyncSocket alloc] initWithDelegate:self delegateQueue:dispatch_get_main_queue()];
    if (socket == nil) {
        NSLog(@"Error creating socket!");
        return;
    }
    
    // Connect to server
    NSError *err = nil;
    if (![socket connectToHost:_ipAddress onPort:ACAPELA_PORT error:&err]) {
        NSLog(@"Error connecting to server at %@:%d", _ipAddress, ACAPELA_PORT);
        return;
    }
}

- (void)closeNetworkCommunication {
    NSLog(@"Closing network communication...");
    if (socket != nil) {
        [socket setDelegate:nil delegateQueue:NULL];
        [socket disconnect];
        socket = nil;
    }
    
    // Stop timer
    if (_timer != nil) {
        [_timer invalidate];
        _timer = nil;
    }
    
    // Clear firmware version
    [lblFirmwareVersion setText:@""];
}

- (void)disconnectNetwork {
    [self closeNetworkCommunication];
    _loginViewShown = NO;
    _loginDone = NO;
    _isAuthenticated = NO;
    [switchSetupLocate setOn:NO];
    [lblIpAddress setText:@""];
    [lblOnline setText:@"Offline"];
    [imgOnline setImage:[UIImage imageNamed:@"offline.png"]];
    [activityLoading stopAnimating];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"ConnectToDevice" object:nil];
}

- (NSString *)getMd5Digest:(NSString *)str {
    const char *cStr = [str UTF8String];
    unsigned char result[CC_MD5_DIGEST_LENGTH];
    CC_MD5(cStr, (CC_LONG)strlen(cStr), result);
    NSMutableString *md5Result = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; ++i) {
        [md5Result appendFormat:@"%02x", result[i]];
    }
    return [NSString stringWithString:md5Result];
}

- (BOOL)isConnected {
    if (socket == nil) {
        return NO;
    } else {
        return [socket isConnected];
    }
}

- (void)queueReadCommand:(int)command dspId:(int)dspId {
    DspRead *readCmd = [[DspRead alloc] init];
    readCmd.command = command;
    readCmd.dspId = dspId;
    [_readCmdQueue addObject:readCmd];
}

- (void)sendNextReadCommand {
    if (_readCmdQueue != nil && _readCmdQueue.count > 0) {
        DspRead *readCmd = [_readCmdQueue objectAtIndex:0];
        if (readCmd != nil) {
            [self sendData:readCmd.command commandType:DSP_READ dspId:readCmd.dspId value:0];
        }
        [_readCmdQueue removeObjectAtIndex:0];
    }
}

- (void)addSendCommand:(int)command dspId:(int)dspId seqNum:(int)seqNum {
    NSString *key = [NSString stringWithFormat:@"%d-%d-%d", dspId, command, seqNum];
    //NSLog(@"addSendCommand for key %@", key);
    [_writeAckQueue addObject:key];
}

- (void)removeSendCommand:(int)command dspId:(int)dspId seqNum:(int)seqNum {
    NSString *key = [NSString stringWithFormat:@"%d-%d-%d", dspId, command, seqNum];
    for (NSString *curKey in _writeAckQueue) {
        if ([curKey compare:key] == NSOrderedSame) {
            [_writeAckQueue removeObject:curKey];
            //NSLog(@"removeSendCommand for key %@", key);
            return;
        }
    }
}

- (BOOL)sendCommandExists:(int)command dspId:(int)dspId {
    NSString *key = [NSString stringWithFormat:@"%d-%d", dspId, command];
    for (NSString *curKey in _writeAckQueue) {
        if ([curKey rangeOfString:key].location != NSNotFound) {
            return YES;
        }
    }
    return NO;
}

- (void)sendData:(int)command commandType:(COMMAND_TYPE)commandType dspId:(int)dspId value:(int)value {
    if (socket == nil) {
        //NSLog(@"sendData: Null socket!");
        return;
    }
    if ([socket isDisconnected]) {
        NSLog(@"sendData: Socket disconnected!");
        return;
    }

    NSMutableData *data;
    switch (commandType) {
        case DSP_READ:
            {
                DspReadPacket pkt;
                int pktLen = sizeof(pkt);
                memset(&pkt, 0, pktLen);
                pkt.start_of_frame = 0xFF;
                pkt.size = (int)(pktLen-FRAME_PROTOCOL_LEN);
                pkt.command = (int)DSP_READ;
                pkt.reserve_1 = dspId;
                pkt.reserve_2 = command;
                pkt.dsp = dspId;
                pkt.address = command;
                
                unsigned char *bytes = (unsigned char *)&pkt;
                pkt.crc = [Global crc:bytes+5 len:pkt.size];
                data = [[NSMutableData alloc] initWithBytes:&pkt length:pktLen];
            }
            break;
        case DSP_WRITE:
            {
                DspWritePacket pkt;
                int pktLen = sizeof(pkt);
                memset(&pkt, 0, pktLen);
                pkt.start_of_frame = 0xFF;
                pkt.size = (int)(pktLen-FRAME_PROTOCOL_LEN);
                pkt.command = (int)DSP_WRITE;
                pkt.reserve_1 = dspId;
                pkt.reserve_2 = command;
                pkt.dsp = dspId;
                pkt.address = command;
                pkt.data = value;
                pkt.seq_num = ++_seqNum;
                
                unsigned char *bytes = (unsigned char *)&pkt;
                pkt.crc = [Global crc:bytes+5 len:pkt.size];
                data = [[NSMutableData alloc] initWithBytes:&pkt length:pktLen];
                
                [self addSendCommand:command dspId:dspId seqNum:_seqNum];
            }
            break;
        case SET_INIT:
            {
                SetInitPacket pkt;
                int pktLen = sizeof(pkt);
                memset(&pkt, 0, pktLen);
                pkt.start_of_frame = 0xFF;
                pkt.size = (int)(pktLen-FRAME_PROTOCOL_LEN);
                pkt.command = (int)SET_INIT;
                
                unsigned char *bytes = (unsigned char *)&pkt;
                pkt.crc = [Global crc:bytes+5 len:pkt.size];
                data = [[NSMutableData alloc] initWithBytes:&pkt length:pktLen];
            }
            break;
        default:
            NSLog(@"sendData: Invalid command type = %d", commandType);
            return;
    }

    [socket writeData:data withTimeout:-1 tag:command];
}

- (void)sendData2:(COMMAND_TYPE)commandType value1:(int)value1 value2:(int)value2 {
    if (socket == nil) {
        //NSLog(@"sendData2: Null socket!");
        return;
    }
    if ([socket isDisconnected]) {
        NSLog(@"sendData2: Socket disconnected!");
        return;
    }
    
    NSMutableData *data;
    switch (commandType) {
        case SET_DELAY:
            {
                SetDelayTxPacket pkt;
                int pktLen = sizeof(pkt);
                memset(&pkt, 0, pktLen);
                pkt.start_of_frame = 0xFF;
                pkt.size = (int)(pktLen-FRAME_PROTOCOL_LEN);
                pkt.command = (int)SET_DELAY;
                pkt.delay_scale = value1;
                pkt.delay_temp = value2;
                
                unsigned char *bytes = (unsigned char *)&pkt;
                pkt.crc = [Global crc:bytes+5 len:pkt.size];
                data = [[NSMutableData alloc] initWithBytes:&pkt length:pktLen];
            }
            break;
        case GET_FILE_LIST:
            {
                GetFileListTxPacket pkt;
                int pktLen = sizeof(pkt);
                memset(&pkt, 0, pktLen);
                pkt.start_of_frame = 0xFF;
                pkt.size = (int)(pktLen-FRAME_PROTOCOL_LEN);
                pkt.command = (int)GET_FILE_LIST;
                pkt.device = value1;
                pkt.scene_mode = value2;
                
                unsigned char *bytes = (unsigned char *)&pkt;
                pkt.crc = [Global crc:bytes+5 len:pkt.size];
                data = [[NSMutableData alloc] initWithBytes:&pkt length:pktLen];
            }
            break;
        case SET_GEQ_LINK_MODE:
            {
                SetGeqLinkModeTxPacket pkt;
                int pktLen = sizeof(pkt);
                memset(&pkt, 0, pktLen);
                pkt.start_of_frame = 0xFF;
                pkt.size = (int)(pktLen-FRAME_PROTOCOL_LEN);
                pkt.command = (int)SET_GEQ_LINK_MODE;
                pkt.link_type = value1;
                pkt.link_mode = value2;
                
                unsigned char *bytes = (unsigned char *)&pkt;
                pkt.crc = [Global crc:bytes+5 len:pkt.size];
                data = [[NSMutableData alloc] initWithBytes:&pkt length:pktLen];
            }
            break;
        default:
            NSLog(@"sendData2: Invalid command type = %d", commandType);
            return;
    }
    
    [socket writeData:data withTimeout:-1 tag:0];
}

- (void)setSendPage:(PAGE_NUMBER)pageNumber reserve2:(int)value {
    _prevSendPage = _curSendPage;
    _prevSendPageValue = _curSendPageValue;
    _curSendPage = pageNumber;
    _curSendPageValue = value;
    [self setSendPage:_curSendPage value:_curSendPageValue];
}

- (void)restoreSendPage {
    _curSendPage = _prevSendPage;
    _curSendPageValue = _prevSendPageValue;
    [self setSendPage:_curSendPage value:_curSendPageValue];
}

- (void)setSendPage:(PAGE_NUMBER)pageNumber value:(int)value {
    if (socket == nil) {
        //NSLog(@"sendData: Null socket!");
        return;
    }
    if ([socket isDisconnected]) {
        NSLog(@"sendData: Socket disconnected!");
        return;
    }
    
    NSMutableData *data;
    SetSendPageTxPacket pkt;
    int pktLen = sizeof(pkt);
    memset(&pkt, 0, pktLen);
    pkt.start_of_frame = 0xFF;
    pkt.size = (int)(pktLen-FRAME_PROTOCOL_LEN);
    pkt.command = (int)SET_SEND_PAGE;
    pkt.page_num = pageNumber;
    pkt.reserve_2 = value;
    
    unsigned char *bytes = (unsigned char *)&pkt;
    pkt.crc = [Global crc:bytes+5 len:pkt.size];
    data = [[NSMutableData alloc] initWithBytes:&pkt length:pktLen];
    [socket writeData:data withTimeout:-1 tag:pkt.command];
}

- (void)sendDataStream:(COMMAND_TYPE)commandType value:(const char*)value {
    if (socket == nil) {
        //NSLog(@"sendDataStream: Null socket!");
        return;
    }
    if ([socket isDisconnected]) {
        NSLog(@"sendDataStream: Socket disconnected!");
        return;
    }
 
    int pktSize = 0;
    switch (commandType) {
        case AUTHORIZED:
            pktSize = 42;
            break;
        default:
            NSLog(@"sendDataStream: Invalid command type = %d", commandType);
            return;
    }
    
    unsigned char bytes[pktSize];
    memset(bytes, 0, pktSize);
    
    bytes[0] = 0xFF;        // Frame header    
    switch (commandType) {
        case AUTHORIZED:
            {
                bytes[1] = 0x24;
                bytes[9] = AUTHORIZED;
                memcpy(bytes+21, value, 20);
                bytes[41] = [Global crc:bytes+5 len:0x24];
            }
            break;
        default:
            break;
    }
    
    NSMutableData *data = [[NSMutableData alloc] initWithBytes:bytes length:pktSize];
    [socket writeData:data withTimeout:-1 tag:0];
}

- (void)setAudioName:(int)audioType channelNum:(int)channelNum channelName:(const char*)channelName {
    if (socket == nil) {
        //NSLog(@"setAudioName: Null socket!");
        return;
    }
    if ([socket isDisconnected]) {
        NSLog(@"setAudioName: Socket disconnected!");
        return;
    }
    
    NSMutableData *data;
    SetAudioNameTxPacket pkt;
    int pktLen = sizeof(pkt);
    memset(&pkt, 0, pktLen);
    pkt.start_of_frame = 0xFF;
    pkt.size = (int)(pktLen-FRAME_PROTOCOL_LEN);
    pkt.command = (int)SET_AUDIO_NAME;
    pkt.audio_type = audioType;
    pkt.channel_num = channelNum;
    if (channelName != NULL && strlen(channelName) > 0) {
        memcpy(&pkt.channel_name, channelName, 10);
    }
    
    unsigned char *bytes = (unsigned char *)&pkt;
    pkt.crc = [Global crc:bytes+5 len:pkt.size];
    data = [[NSMutableData alloc] initWithBytes:&pkt length:pktLen];
    [socket writeData:data withTimeout:-1 tag:0];
}

- (void)setFileAccess:(int)device sceneMode:(int)sceneMode channel:(int)channel accessMode:(int)accessMode fileName:(const char*)fileName newFileName:(const char*)newFileName {
    if (socket == nil) {
        //NSLog(@"setFileAccess: Null socket!");
        return;
    }
    if ([socket isDisconnected]) {
        NSLog(@"setFileAccess: Socket disconnected!");
        return;
    }
    
    NSMutableData *data;
    SetFileAccessTxPacket pkt;
    int pktLen = sizeof(pkt);
    memset(&pkt, 0, pktLen);
    pkt.start_of_frame = 0xFF;
    pkt.size = (int)(pktLen-FRAME_PROTOCOL_LEN);
    pkt.command = (int)SET_FILE_ACCESS;
    pkt.device = device;
    pkt.scene_mode = sceneMode;
    pkt.channel_select = channel;
    pkt.access_mode = accessMode;
    if (fileName != NULL && strlen(fileName) > 0) {
        memcpy(&pkt.file_name, fileName, 10);
    }
    if (newFileName != NULL && strlen(newFileName) > 0) {
        memcpy(&pkt.new_file_name, newFileName, 10);
    }
    
    unsigned char *bytes = (unsigned char *)&pkt;
    pkt.crc = [Global crc:bytes+5 len:pkt.size];
    data = [[NSMutableData alloc] initWithBytes:&pkt length:pktLen];
    [socket writeData:data withTimeout:-1 tag:0];
}

#pragma mark -
#pragma mark GCDAsyncSocketDelegate methods

- (void)socket:(GCDAsyncSocket *)sender didConnectToHost:(NSString *)host port:(UInt16)port {
    NSLog(@"Socket didConnectToHost");
    [lblOnline setText:@"Loading..."];
    [imgOnline setImage:nil];
    [activityLoading startAnimating];
    
    // Instigate the first read
    [socket readDataWithTimeout:-1 tag:0];
    
    // Do authentication
    if (!_loginDone) {
        // Send login info
        char data[20];
        memset(data, 0, 20);
        const char *username = [_username UTF8String];
        const char *password = [_password UTF8String];
        memcpy(data, username, strlen(username));
        memcpy(data+10, password, strlen(password));
        [self sendDataStream:AUTHORIZED value:data];
        _loginDone = YES;
        _isAuthenticated = NO;
    }
}

- (void)socketDidDisconnect:(GCDAsyncSocket *)sock withError:(NSError *)err {
    NSLog(@"Socket didDisconnect");
    if (err != nil) {
        [[[UIAlertView alloc] initWithTitle:@"Network error"
                                    message:err.localizedFailureReason
                                   delegate:nil
                          cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        
        [self disconnectNetwork];
    }
}

- (void)socket:(GCDAsyncSocket *)sock didWriteDataWithTag:(long)tag {
    //NSLog(@"Wrote data with tag %ld", tag);
}

- (void)socket:(GCDAsyncSocket *)sender didReadData:(NSData *)data withTag:(long)tag {
    if (tag == TAG_HEADER) {
        // Read header
        int len = (int)data.length;
        unsigned char buffer[len];
        [data getBytes:buffer length:data.length];
        int dataSize = buffer[1] + (buffer[2] * 256);
        [socket readDataToLength:dataSize+1 withTimeout:-1 tag:TAG_DATA];   // Read CRC as well
    } else if (tag == TAG_DATA) {
        // Read data
        int len = (int)data.length;
        unsigned char buffer[len];
        [data getBytes:buffer length:len];
        [self processData:buffer length:len];
        
        // Setup header reader
        [socket readDataToLength:FRAME_HEADER_LEN withTimeout:-1 tag:TAG_HEADER];
    } else {
        // Handle authorized
        if (!_isAuthenticated) {
            int len = (int)data.length;
            unsigned char buffer[len];
            [data getBytes:buffer range:NSMakeRange(FRAME_HEADER_LEN, len-FRAME_HEADER_LEN)];
            [self processData:buffer length:len-FRAME_HEADER_LEN];
        }
        
        // Setup header reader
        [socket readDataToLength:FRAME_HEADER_LEN withTimeout:-1 tag:TAG_HEADER];
    }
}

- (void)processData:(unsigned char *)buffer length:(int)len {
    int cmdType = buffer[4];
    int dspId = 0;
    int value = 0;
    int cmd = 0;
    int seqNum = (buffer[3] << 24) + (buffer[2] << 16) + (buffer[1] << 8) + buffer[0];
    
    //NSLog(@"Received command %d, dataLen = %d", cmdType, len);
    
    switch (cmdType) {
        case AUTHORIZED:
        case SET_AUDIO_NAME:
        case SET_SEND_PAGE:
        case SET_DELAY:
        case SET_INIT:
        case SET_FILE_ACCESS:
        case SET_GEQ_LINK_MODE:
        case REVERB_WRITE:
            value = buffer[8];
            break;
        case DSP_WRITE:
            dspId = buffer[8];
            cmd = (buffer[13] << 8) + buffer[12];
            value = (buffer[17] << 8) + buffer[16];
            
            // Remove send command
            [self removeSendCommand:cmd dspId:dspId seqNum:seqNum];
            break;
        case DSP_READ:
            dspId = buffer[8];
            cmd = (buffer[13] << 8) + buffer[12];
            value = (buffer[17] << 8) + buffer[16];
            break;
        case ALL_METER_VALUE:
        {
            AllMeterValueRxPacket *meterValue = (AllMeterValueRxPacket *)buffer;
            
            // Check CRC
            if (![Global checkCrc:buffer
                    len:sizeof(AllMeterValueRxPacket)-FRAME_PROTOCOL_LEN pktCrc:meterValue->crc]) {
                NSLog(@"ALL_METER_VALUE invalid crc!");
                return;
            }
            
            // Update top meters
            pageCh1_8Meter1.progress = ((float)meterValue->channel_1_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            pageCh1_8Meter2.progress = ((float)meterValue->channel_2_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            pageCh1_8Meter3.progress = ((float)meterValue->channel_3_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            pageCh1_8Meter4.progress = ((float)meterValue->channel_4_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            pageCh1_8Meter5.progress = ((float)meterValue->channel_5_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            pageCh1_8Meter6.progress = ((float)meterValue->channel_6_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            pageCh1_8Meter7.progress = ((float)meterValue->channel_7_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            pageCh1_8Meter8.progress = ((float)meterValue->channel_8_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            pageCh9_16Meter1.progress = ((float)meterValue->channel_9_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            pageCh9_16Meter2.progress = ((float)meterValue->channel_10_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            pageCh9_16Meter3.progress = ((float)meterValue->channel_11_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            pageCh9_16Meter4.progress = ((float)meterValue->channel_12_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            pageCh9_16Meter5.progress = ((float)meterValue->channel_13_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            pageCh9_16Meter6.progress = ((float)meterValue->channel_14_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            pageCh9_16Meter7.progress = ((float)meterValue->channel_15_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            pageCh9_16Meter8.progress = ((float)meterValue->channel_16_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            pageUsbAudioMeterL.progress = ((float)meterValue->channel_17_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            pageUsbAudioMeterR.progress = ((float)meterValue->channel_18_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            pageAux1_4Meter1.progress = ((float)meterValue->aux_1_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            pageAux1_4Meter2.progress = ((float)meterValue->aux_2_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            pageAux1_4Meter3.progress = ((float)meterValue->aux_3_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            pageAux1_4Meter4.progress = ((float)meterValue->aux_4_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            pageGp1_4Meter1.progress = ((float)meterValue->group_1_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            pageGp1_4Meter2.progress = ((float)meterValue->group_2_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            pageGp1_4Meter3.progress = ((float)meterValue->group_3_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            pageGp1_4Meter4.progress = ((float)meterValue->group_4_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            pageMulti1_4Meter1.progress = ((float)meterValue->multi_1_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            pageMulti1_4Meter2.progress = ((float)meterValue->multi_2_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            pageMulti1_4Meter3.progress = ((float)meterValue->multi_3_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            pageMulti1_4Meter4.progress = ((float)meterValue->multi_4_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            pageMainMeterL.progress = ((float)meterValue->main_l_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            pageMainMeterR.progress = ((float)meterValue->main_r_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            pageCtrlRmMeterL.progress = ((float)meterValue->ctrl_l_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            pageCtrlRmMeterR.progress = ((float)meterValue->ctrl_r_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            pageEfx1MeterL.progress = ((float)meterValue->effect_1_out_l_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            pageEfx1MeterR.progress = ((float)meterValue->effect_1_out_r_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            pageEfx2MeterL.progress = ((float)meterValue->effect_2_out_l_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            pageEfx2MeterR.progress = ((float)meterValue->effect_2_out_r_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            pageUsbAudioOutMeterL.progress = ((float)meterValue->channel_17_out_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            pageUsbAudioOutMeterR.progress = ((float)meterValue->channel_18_out_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            
            // Update main meter
            meterMainL.progress = ((float)meterValue->main_l_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterMainR.progress = ((float)meterValue->main_r_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            
            // Update individual page meters
            if (currentPage == PAGE_CH_1_8) {
                meter1.progress = ((float)meterValue->channel_1_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
                meter2.progress = ((float)meterValue->channel_2_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
                meter3.progress = ((float)meterValue->channel_3_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
                meter4.progress = ((float)meterValue->channel_4_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
                meter5.progress = ((float)meterValue->channel_5_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
                meter6.progress = ((float)meterValue->channel_6_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
                meter7.progress = ((float)meterValue->channel_7_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
                meter8.progress = ((float)meterValue->channel_8_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            } else if (currentPage == PAGE_CH_9_16 ) {
                meter1.progress = ((float)meterValue->channel_9_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
                meter2.progress = ((float)meterValue->channel_10_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
                meter3.progress = ((float)meterValue->channel_11_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
                meter4.progress = ((float)meterValue->channel_12_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
                meter5.progress = ((float)meterValue->channel_13_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
                meter6.progress = ((float)meterValue->channel_14_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
                meter7.progress = ((float)meterValue->channel_15_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
                meter8.progress = ((float)meterValue->channel_16_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            } else if (currentPage == PAGE_USB_AUDIO) {
                meter1.progress = ((float)meterValue->channel_17_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
                meter2.progress = ((float)meterValue->channel_18_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            } else if (currentPage == PAGE_AUX_1_4) {
                meter1.progress = ((float)meterValue->aux_1_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
                meter2.progress = ((float)meterValue->aux_2_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
                meter3.progress = ((float)meterValue->aux_3_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
                meter4.progress = ((float)meterValue->aux_4_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            } else if (currentPage == PAGE_GP_1_4) {
                meter1.progress = ((float)meterValue->group_1_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
                meter2.progress = ((float)meterValue->group_2_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
                meter3.progress = ((float)meterValue->group_3_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
                meter4.progress = ((float)meterValue->group_4_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            } else if (currentPage == PAGE_MT_1_4) {
                meter1.progress = ((float)meterValue->multi_1_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
                meter2.progress = ((float)meterValue->multi_2_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
                meter3.progress = ((float)meterValue->multi_3_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
                meter4.progress = ((float)meterValue->multi_4_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            } else if (currentPage == PAGE_CTRL_RM || currentPage == PAGE_MAIN) {
                meterCtrlRmL.progress = ((float)meterValue->ctrl_l_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
                meterCtrlRmR.progress = ((float)meterValue->ctrl_r_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            } else if (currentPage == PAGE_EFX_1) {
                meterMainL.progress = ((float)meterValue->effect_1_out_l_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
                meterMainR.progress = ((float)meterValue->effect_1_out_r_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            } else if (currentPage == PAGE_EFX_2) {
                meterMainL.progress = ((float)meterValue->effect_2_out_l_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
                meterMainR.progress = ((float)meterValue->effect_2_out_r_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            }
            
            // Update DSP clock rate
            int clockSource = meterValue->dsp_sets_clock_source & 0x07;
            if (clockSource == 0 || clockSource == 1)
                [lblClkRate setTextColor:[UIColor redColor]];
            else if (clockSource == 4)
                [lblClkRate setTextColor:[UIColor whiteColor]];

            BOOL lockStatus = (meterValue->dsp_status_3 & 0x04) >> 2;
            if (lockStatus != 0) {
                [lblClkRate setTextColor:[UIColor redColor]];
                [lblClkRate setText:@"Un-Lock"];
            }
            [lblClkRate setText:[NSString stringWithFormat:@"%.1f kHz", meterValue->dsp_sample_rate/100.0]];
            
            if (!auxSendController.view.hidden) {
                [auxSendController processMeter:meterValue];
            }
            if (!groupSendController.view.hidden) {
                [groupSendController processMeter:meterValue];
            }
            if (!geqPeqController.view.hidden) {
                [geqPeqController processMeter:meterValue];
            }
            
            // Update USB state
            if (_usbMount != meterValue->usb_state) {
                _usbMount = meterValue->usb_state;
                if (_usbMount == 0) {
                    // Unmount
                    [btnSceneFlashRom setSelected:YES];
                    [btnSceneUsb setHidden:YES];
                    [btnSceneUsb setSelected:NO];
                } else {
                    // Mount
                    [btnSceneFlashRom setSelected:NO];
                    [btnSceneUsb setHidden:NO];
                    [btnSceneUsb setSelected:YES];
                }
            }
            
            // Update delay
            if (meterValue->delay_scale == 0) {
                _delayScale = DELAY_TIME;
            } else if (meterValue->delay_scale == 256) {
                _delayScale = DELAY_METER;
            } else if (meterValue->delay_scale == 512) {
                _delayScale = DELAY_FEET;
            }
            switch (_delayScale) {
                case DELAY_TIME:
                    btnDelayTime.selected = YES;
                    btnDelayMeter.selected = NO;
                    btnDelayFeet.selected = NO;
                    break;
                case DELAY_METER:
                    btnDelayTime.selected = NO;
                    btnDelayMeter.selected = YES;
                    btnDelayFeet.selected = NO;
                    break;
                case DELAY_FEET:
                    btnDelayTime.selected = NO;
                    btnDelayMeter.selected = NO;
                    btnDelayFeet.selected = YES;
                    break;
                default:
                    break;
            }
            
            // Update DYN active status, green if ACTIVE, yellow if NOP

            // Main DYN status
            if ((meterValue->main_l_r_ch_17_18_dyn_active & 0x01) ||
                ((meterValue->main_l_r_ch_17_18_dyn_active & 0x10) >> 4)) {
                [btnDynGMain setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
            } else {
                [btnDynGMain setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
            }
            if (((meterValue->main_l_r_ch_17_18_dyn_active & 0x02) >> 1) ||
                ((meterValue->main_l_r_ch_17_18_dyn_active & 0x20) >> 5)) {
                [btnDynEMain setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
            } else {
                [btnDynEMain setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
            }
            if (((meterValue->main_l_r_ch_17_18_dyn_active & 0x04) >> 2) ||
                ((meterValue->main_l_r_ch_17_18_dyn_active & 0x40) >> 6)) {
                [btnDynCMain setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
            } else {
                [btnDynCMain setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
            }
            if (((meterValue->main_l_r_ch_17_18_dyn_active & 0x08) >> 3) ||
                ((meterValue->main_l_r_ch_17_18_dyn_active & 0x80) >> 7)) {
                [btnDynLMain setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
            } else {
                [btnDynLMain setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
            }
            
            switch (currentPage) {
                case PAGE_CH_1_8:
                    // CH_1
                    if (meterValue->channel_1_4_dyn_active & 0x01) {
                        [btnDynG1 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynG1 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->channel_1_4_dyn_active & 0x02) >> 1) {
                        [btnDynE1 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynE1 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->channel_1_4_dyn_active & 0x04) >> 2) {
                        [btnDynC1 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynC1 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    // CH_2
                    if ((meterValue->channel_1_4_dyn_active & 0x08) >> 3) {
                        [btnDynL2 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynL2 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->channel_1_4_dyn_active & 0x10) >> 4) {
                        [btnDynG2 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynG2 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->channel_1_4_dyn_active & 0x20) >> 5) {
                        [btnDynE2 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynE2 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->channel_1_4_dyn_active & 0x40) >> 6) {
                        [btnDynC2 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynC2 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->channel_1_4_dyn_active & 0x80) >> 7) {
                        [btnDynL2 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynL2 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    // CH_3
                    if ((meterValue->channel_1_4_dyn_active & 0x0100) >> 8) {
                        [btnDynG3 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynG3 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->channel_1_4_dyn_active & 0x0200) >> 9) {
                        [btnDynE3 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynE3 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->channel_1_4_dyn_active & 0x0400) >> 10) {
                        [btnDynC3 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynC3 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->channel_1_4_dyn_active & 0x0800) >> 11) {
                        [btnDynL3 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynL3 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    // CH_4
                    if ((meterValue->channel_1_4_dyn_active & 0x1000) >> 12) {
                        [btnDynG4 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynG4 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->channel_1_4_dyn_active & 0x2000) >> 13) {
                        [btnDynE4 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynE4 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->channel_1_4_dyn_active & 0x4000) >> 14) {
                        [btnDynC4 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynC4 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->channel_1_4_dyn_active & 0x8000) >> 15) {
                        [btnDynL4 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynL4 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    // CH_5
                    if (meterValue->channel_5_8_dyn_active & 0x01) {
                        [btnDynG5 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynG5 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->channel_5_8_dyn_active & 0x02) >> 1) {
                        [btnDynE5 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynE5 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->channel_5_8_dyn_active & 0x04) >> 2) {
                        [btnDynC5 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynC5 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->channel_5_8_dyn_active & 0x08) >> 3) {
                        [btnDynL5 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynL5 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    // CH_6
                    if ((meterValue->channel_5_8_dyn_active & 0x10) >> 4) {
                        [btnDynG6 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynG6 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->channel_5_8_dyn_active & 0x20) >> 5) {
                        [btnDynE6 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynE6 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->channel_5_8_dyn_active & 0x40) >> 6) {
                        [btnDynC6 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynC6 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->channel_5_8_dyn_active & 0x80) >> 7) {
                        [btnDynL6 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynL6 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    // CH_7
                    if ((meterValue->channel_5_8_dyn_active & 0x0100) >> 8) {
                        [btnDynG7 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynG7 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->channel_5_8_dyn_active & 0x0200) >> 9) {
                        [btnDynE7 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynE7 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->channel_5_8_dyn_active & 0x0400) >> 10) {
                        [btnDynC7 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynC7 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->channel_5_8_dyn_active & 0x0800) >> 11) {
                        [btnDynL7 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynL7 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    // CH_8
                    if ((meterValue->channel_5_8_dyn_active & 0x1000) >> 12) {
                        [btnDynG8 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynG8 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->channel_5_8_dyn_active & 0x2000) >> 13) {
                        [btnDynE8 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynE8 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->channel_5_8_dyn_active & 0x4000) >> 14) {
                        [btnDynC8 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynC8 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->channel_5_8_dyn_active & 0x8000) >> 15) {
                        [btnDynL8 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynL8 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    break;
                case PAGE_CH_9_16:
                    // CH_9
                    if (meterValue->channel_9_12_dyn_active & 0x01) {
                        [btnDynG1 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynG1 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->channel_9_12_dyn_active & 0x02) >> 1) {
                        [btnDynE1 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynE1 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->channel_9_12_dyn_active & 0x04) >> 2) {
                        [btnDynC1 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynC1 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    // CH_10
                    if ((meterValue->channel_9_12_dyn_active & 0x08) >> 3) {
                        [btnDynL2 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynL2 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->channel_9_12_dyn_active & 0x10) >> 4) {
                        [btnDynG2 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynG2 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->channel_9_12_dyn_active & 0x20) >> 5) {
                        [btnDynE2 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynE2 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->channel_9_12_dyn_active & 0x40) >> 6) {
                        [btnDynC2 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynC2 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->channel_9_12_dyn_active & 0x80) >> 7) {
                        [btnDynL2 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynL2 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    // CH_11
                    if ((meterValue->channel_9_12_dyn_active & 0x0100) >> 8) {
                        [btnDynG3 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynG3 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->channel_9_12_dyn_active & 0x0200) >> 9) {
                        [btnDynE3 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynE3 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->channel_9_12_dyn_active & 0x0400) >> 10) {
                        [btnDynC3 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynC3 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->channel_9_12_dyn_active & 0x0800) >> 11) {
                        [btnDynL3 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynL3 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    // CH_12
                    if ((meterValue->channel_9_12_dyn_active & 0x1000) >> 12) {
                        [btnDynG4 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynG4 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->channel_9_12_dyn_active & 0x2000) >> 13) {
                        [btnDynE4 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynE4 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->channel_9_12_dyn_active & 0x4000) >> 14) {
                        [btnDynC4 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynC4 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->channel_9_12_dyn_active & 0x8000) >> 15) {
                        [btnDynL4 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynL4 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    // CH_13
                    if (meterValue->channel_13_16_dyn_active & 0x01) {
                        [btnDynG5 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynG5 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->channel_13_16_dyn_active & 0x02) >> 1) {
                        [btnDynE5 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynE5 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->channel_13_16_dyn_active & 0x04) >> 2) {
                        [btnDynC5 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynC5 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->channel_13_16_dyn_active & 0x08) >> 3) {
                        [btnDynL5 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynL5 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    // CH_14
                    if ((meterValue->channel_13_16_dyn_active & 0x10) >> 4) {
                        [btnDynG6 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynG6 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->channel_13_16_dyn_active & 0x20) >> 5) {
                        [btnDynE6 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynE6 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->channel_13_16_dyn_active & 0x40) >> 6) {
                        [btnDynC6 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynC6 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->channel_13_16_dyn_active & 0x80) >> 7) {
                        [btnDynL6 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynL6 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    // CH_15
                    if ((meterValue->channel_13_16_dyn_active & 0x0100) >> 8) {
                        [btnDynG7 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynG7 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->channel_13_16_dyn_active & 0x0200) >> 9) {
                        [btnDynE7 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynE7 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->channel_13_16_dyn_active & 0x0400) >> 10) {
                        [btnDynC7 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynC7 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->channel_13_16_dyn_active & 0x0800) >> 11) {
                        [btnDynL7 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynL7 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    // CH_16
                    if ((meterValue->channel_13_16_dyn_active & 0x1000) >> 12) {
                        [btnDynG8 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynG8 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->channel_13_16_dyn_active & 0x2000) >> 13) {
                        [btnDynE8 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynE8 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->channel_13_16_dyn_active & 0x4000) >> 14) {
                        [btnDynC8 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynC8 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->channel_13_16_dyn_active & 0x8000) >> 15) {
                        [btnDynL8 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynL8 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    break;
                case PAGE_USB_AUDIO:
                    // CH_17
                    if ((meterValue->main_l_r_ch_17_18_dyn_active & 0x0100) >> 8) {
                        [btnDynG1 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynG1 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->main_l_r_ch_17_18_dyn_active & 0x0200) >> 9) {
                        [btnDynE1 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynE1 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->main_l_r_ch_17_18_dyn_active & 0x0400) >> 10) {
                        [btnDynC1 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynC1 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->main_l_r_ch_17_18_dyn_active & 0x0800) >> 11) {
                        [btnDynL1 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynL1 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    // CH_18
                    if ((meterValue->main_l_r_ch_17_18_dyn_active & 0x1000) >> 12) {
                        [btnDynG2 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynG2 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->main_l_r_ch_17_18_dyn_active & 0x2000) >> 13) {
                        [btnDynE2 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynE2 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->main_l_r_ch_17_18_dyn_active & 0x4000) >> 14) {
                        [btnDynC2 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynC2 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->main_l_r_ch_17_18_dyn_active & 0x8000) >> 15) {
                        [btnDynL2 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynL2 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    break;
                case PAGE_MT_1_4:
                    // MULTI_1
                    if (meterValue->multi_1_4_dyn_active & 0x01) {
                        [btnDynG1 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynG1 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->multi_1_4_dyn_active & 0x02) >> 1) {
                        [btnDynE1 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynE1 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->multi_1_4_dyn_active & 0x04) >> 2) {
                        [btnDynC1 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynC1 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    // MULTI_2
                    if ((meterValue->multi_1_4_dyn_active & 0x08) >> 3) {
                        [btnDynL2 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynL2 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->multi_1_4_dyn_active & 0x10) >> 4) {
                        [btnDynG2 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynG2 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->multi_1_4_dyn_active & 0x20) >> 5) {
                        [btnDynE2 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynE2 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->multi_1_4_dyn_active & 0x40) >> 6) {
                        [btnDynC2 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynC2 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->multi_1_4_dyn_active & 0x80) >> 7) {
                        [btnDynL2 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynL2 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    // MULTI_3
                    if ((meterValue->multi_1_4_dyn_active & 0x0100) >> 8) {
                        [btnDynG3 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynG3 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->multi_1_4_dyn_active & 0x0200) >> 9) {
                        [btnDynE3 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynE3 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->multi_1_4_dyn_active & 0x0400) >> 10) {
                        [btnDynC3 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynC3 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->multi_1_4_dyn_active & 0x0800) >> 11) {
                        [btnDynL3 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynL3 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    // MULTI_4
                    if ((meterValue->multi_1_4_dyn_active & 0x1000) >> 12) {
                        [btnDynG4 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynG4 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->multi_1_4_dyn_active & 0x2000) >> 13) {
                        [btnDynE4 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynE4 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->multi_1_4_dyn_active & 0x4000) >> 14) {
                        [btnDynC4 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynC4 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    if ((meterValue->multi_1_4_dyn_active & 0x8000) >> 15) {
                        [btnDynL4 setBackgroundImage:[UIImage imageNamed:@"pan-10.png"] forState:UIControlStateSelected];
                    } else {
                        [btnDynL4 setBackgroundImage:[UIImage imageNamed:@"pan-11.png"] forState:UIControlStateSelected];
                    }
                    break;
                default:
                    break;
            }

            /*
            // GEQ link mode
            switch (currentPage) {
                case PAGE_EFX_1:
                    if (effectController != nil) {
                        [effectController setLinkMode:meterValue->effect_1_geq_31 effect:EFX_1];
                    }
                    break;
                case PAGE_EFX_2:
                    if (effectController != nil) {
                        [effectController setLinkMode:meterValue->effect_2_geq_15 effect:EFX_2];
                    }
                    break;
                default:
                    break;
            }
            */
            
            // Set firmware version
            int versionLen = (int)strlen((const char *)meterValue->version);
            if (versionLen > 12) {
                versionLen = 12;
            }
            NSString *version = [[NSString stringWithUTF8String:(const char *)meterValue->version] substringToIndex:versionLen];
            [lblFirmwareVersion setText:version];
            
            // Audio page
            switch (currentPage) {
                case PAGE_CH_1_8:
                    txtSliderMain.text = [NSString stringWithUTF8String:(const char *)meterValue->main_audio_name];
                    txtSlider1.text = [NSString stringWithUTF8String:(const char *)meterValue->channel_1_audio_name];
                    txtSlider2.text = [NSString stringWithUTF8String:(const char *)meterValue->channel_2_audio_name];
                    txtSlider3.text = [NSString stringWithUTF8String:(const char *)meterValue->channel_3_audio_name];
                    txtSlider4.text = [NSString stringWithUTF8String:(const char *)meterValue->channel_4_audio_name];
                    txtSlider5.text = [NSString stringWithUTF8String:(const char *)meterValue->channel_5_audio_name];
                    txtSlider6.text = [NSString stringWithUTF8String:(const char *)meterValue->channel_6_audio_name];
                    txtSlider7.text = [NSString stringWithUTF8String:(const char *)meterValue->channel_7_audio_name];
                    txtSlider8.text = [NSString stringWithUTF8String:(const char *)meterValue->channel_8_audio_name];
                    break;
                case PAGE_CH_9_16:
                    txtSliderMain.text = [NSString stringWithUTF8String:(const char *)meterValue->main_audio_name];
                    txtSlider1.text = [NSString stringWithUTF8String:(const char *)meterValue->channel_9_audio_name];
                    txtSlider2.text = [NSString stringWithUTF8String:(const char *)meterValue->channel_10_audio_name];
                    txtSlider3.text = [NSString stringWithUTF8String:(const char *)meterValue->channel_11_audio_name];
                    txtSlider4.text = [NSString stringWithUTF8String:(const char *)meterValue->channel_12_audio_name];
                    txtSlider5.text = [NSString stringWithUTF8String:(const char *)meterValue->channel_13_audio_name];
                    txtSlider6.text = [NSString stringWithUTF8String:(const char *)meterValue->channel_14_audio_name];
                    txtSlider7.text = [NSString stringWithUTF8String:(const char *)meterValue->channel_15_audio_name];
                    txtSlider8.text = [NSString stringWithUTF8String:(const char *)meterValue->channel_16_audio_name];
                    break;
                case PAGE_USB_AUDIO:
                    txtSliderMain.text = [NSString stringWithUTF8String:(const char *)meterValue->main_audio_name];
                    txtSlider1.text = [NSString stringWithUTF8String:(const char *)meterValue->channel_17_audio_name];
                    txtSlider2.text = [NSString stringWithUTF8String:(const char *)meterValue->channel_18_audio_name];
                    break;
                case PAGE_AUX_1_4:
                    txtSliderMain.text = [NSString stringWithUTF8String:(const char *)meterValue->main_audio_name];
                    txtSlider1.text = [NSString stringWithUTF8String:(const char *)meterValue->aux_1_audio_name];
                    txtSlider2.text = [NSString stringWithUTF8String:(const char *)meterValue->aux_2_audio_name];
                    txtSlider3.text = [NSString stringWithUTF8String:(const char *)meterValue->aux_3_audio_name];
                    txtSlider4.text = [NSString stringWithUTF8String:(const char *)meterValue->aux_4_audio_name];
                    break;
                case PAGE_GP_1_4:
                    txtSliderMain.text = [NSString stringWithUTF8String:(const char *)meterValue->main_audio_name];
                    txtSlider1.text = [NSString stringWithUTF8String:(const char *)meterValue->group_1_audio_name];
                    txtSlider2.text = [NSString stringWithUTF8String:(const char *)meterValue->group_2_audio_name];
                    txtSlider3.text = [NSString stringWithUTF8String:(const char *)meterValue->group_3_audio_name];
                    txtSlider4.text = [NSString stringWithUTF8String:(const char *)meterValue->group_4_audio_name];
                    break;
                case PAGE_MT_1_4:
                    txtSliderMain.text = [NSString stringWithUTF8String:(const char *)meterValue->main_audio_name];
                    txtSlider1.text = [NSString stringWithUTF8String:(const char *)meterValue->multi_1_audio_name];
                    txtSlider2.text = [NSString stringWithUTF8String:(const char *)meterValue->multi_2_audio_name];
                    txtSlider3.text = [NSString stringWithUTF8String:(const char *)meterValue->multi_3_audio_name];
                    txtSlider4.text = [NSString stringWithUTF8String:(const char *)meterValue->multi_4_audio_name];
                    break;
                case PAGE_EFX_1:
                    txtSliderMain.text = [NSString stringWithUTF8String:(const char *)meterValue->effect_1_audio_name];
                    break;
                case PAGE_EFX_2:
                    txtSliderMain.text = [NSString stringWithUTF8String:(const char *)meterValue->effect_2_audio_name];
                    break;
                case PAGE_CTRL_RM:
                case PAGE_MAIN:
                    txtSliderMain.text = [NSString stringWithUTF8String:(const char *)meterValue->main_audio_name];
                    txtSlider8.text = [NSString stringWithUTF8String:(const char *)meterValue->ctrl_room_audio_name];
                    break;
                default:
                    break;
            }
            if (eqDynDelayController && !eqDynDelayController.view.hidden) {
                [eqDynDelayController processChannelLabel:meterValue];
            }
            if (geqPeqController && !geqPeqController.view.hidden) {
                [geqPeqController processChannelLabel:meterValue];
            }
            if (dynController && !dynController.view.hidden) {
                [dynController processChannelLabel:meterValue];
            }
            return;
        }
        case VIEW_PAGE_PARAMETER:
        {
            int pageType = buffer[8];
            if (pageType == 0) {
                // Channel page
                ViewPageChannelPacket *channelPage = (ViewPageChannelPacket *)buffer;
                
                // Check CRC
                if (![Global checkCrc:buffer
                        len:sizeof(ViewPageChannelPacket)-FRAME_PROTOCOL_LEN pktCrc:channelPage->crc]) {
                    NSLog(@"VIEW_PAGE_PARAMETER (Channel) invalid crc!");
                    return;
                }
                
                switch (currentPage) {
                    case PAGE_CH_1_8:
                        if (!_slider1Lock) {
                            slider1.value = channelPage->ch_1_fader_level;
                            [lblSlider1Value setText:[Global getSliderGain:slider1.value appendDb:YES]];
                        }
                        if (!_slider2Lock) {
                            slider2.value = channelPage->ch_2_fader_level;
                            [lblSlider2Value setText:[Global getSliderGain:slider2.value appendDb:YES]];
                        }
                        if (!_slider3Lock) {
                            slider3.value = channelPage->ch_3_fader_level;
                            [lblSlider3Value setText:[Global getSliderGain:slider3.value appendDb:YES]];
                        }
                        if (!_slider4Lock) {
                            slider4.value = channelPage->ch_4_fader_level;
                            [lblSlider4Value setText:[Global getSliderGain:slider4.value appendDb:YES]];
                        }
                        if (!_slider5Lock) {
                            slider5.value = channelPage->ch_5_fader_level;
                            [lblSlider5Value setText:[Global getSliderGain:slider5.value appendDb:YES]];
                        }
                        if (!_slider6Lock) {
                            slider6.value = channelPage->ch_6_fader_level;
                            [lblSlider6Value setText:[Global getSliderGain:slider6.value appendDb:YES]];
                        }
                        if (!_slider7Lock) {
                            slider7.value = channelPage->ch_7_fader_level;
                            [lblSlider7Value setText:[Global getSliderGain:slider7.value appendDb:YES]];
                        }
                        if (!_slider8Lock) {
                            slider8.value = channelPage->ch_8_fader_level;
                            [lblSlider8Value setText:[Global getSliderGain:slider8.value appendDb:YES]];
                        }
                        [self refreshSliderOnButtons:(channelPage->ch_1_16_on_off & 0xFF)];
                        [self refreshSliderSoloButtons:(channelPage->ch_1_16_solo_ctrl_on_off & 0xFF)];
                        [self refreshSliderSoloSafeButtons:(channelPage->ch_1_16_solo_safe_ctrl_on_off & 0xFF)];
                        [self refreshSliderMeterButtons:(channelPage->ch_1_16_meter_pre_post & 0xFF)];
                        if (currentMode == MODE_PAN) {
                            sliderPan1.value = channelPage->channel_1_pan_level + PAN_CENTER_VALUE;
                            sliderPan2.value = channelPage->channel_2_pan_level + PAN_CENTER_VALUE;
                            sliderPan3.value = channelPage->channel_3_pan_level + PAN_CENTER_VALUE;
                            sliderPan4.value = channelPage->channel_4_pan_level + PAN_CENTER_VALUE;
                            sliderPan5.value = channelPage->channel_5_pan_level + PAN_CENTER_VALUE;
                            sliderPan6.value = channelPage->channel_6_pan_level + PAN_CENTER_VALUE;
                            sliderPan7.value = channelPage->channel_7_pan_level + PAN_CENTER_VALUE;
                            sliderPan8.value = channelPage->channel_8_pan_level + PAN_CENTER_VALUE;
                            [self onSliderPan1Action:nil];
                            [self onSliderPan2Action:nil];
                            [self onSliderPan3Action:nil];
                            [self onSliderPan4Action:nil];
                            [self onSliderPan5Action:nil];
                            [self onSliderPan6Action:nil];
                            [self onSliderPan7Action:nil];
                            [self onSliderPan8Action:nil];
                        }
                        if (eqDynDelayController) {
                            [eqDynDelayController processChannelViewPage:channelPage];
                        }
                        if (geqPeqController) {
                            [geqPeqController processChannelViewPage:channelPage];
                        }
                        if (dynController) {
                            [dynController processChannelViewPage:channelPage];
                        }
                        break;
                        
                    case PAGE_CH_9_16:
                        if (!_slider1Lock) {
                            slider1.value = channelPage->ch_9_fader_level;
                            [lblSlider1Value setText:[Global getSliderGain:slider1.value appendDb:YES]];
                        }
                        if (!_slider2Lock) {
                            slider2.value = channelPage->ch_10_fader_level;
                            [lblSlider2Value setText:[Global getSliderGain:slider2.value appendDb:YES]];
                        }
                        if (!_slider3Lock) {
                            slider3.value = channelPage->ch_11_fader_level;
                            [lblSlider3Value setText:[Global getSliderGain:slider3.value appendDb:YES]];
                        }
                        if (!_slider4Lock) {
                            slider4.value = channelPage->ch_12_fader_level;
                            [lblSlider4Value setText:[Global getSliderGain:slider4.value appendDb:YES]];
                        }
                        if (!_slider5Lock) {
                            slider5.value = channelPage->ch_13_fader_level;
                            [lblSlider5Value setText:[Global getSliderGain:slider5.value appendDb:YES]];
                        }
                        if (!_slider6Lock) {
                            slider6.value = channelPage->ch_14_fader_level;
                            [lblSlider6Value setText:[Global getSliderGain:slider6.value appendDb:YES]];
                        }
                        if (!_slider7Lock) {
                            slider7.value = channelPage->ch_15_fader_level;
                            [lblSlider7Value setText:[Global getSliderGain:slider7.value appendDb:YES]];
                        }
                        if (!_slider8Lock) {
                            slider8.value = channelPage->ch_16_fader_level;
                            [lblSlider8Value setText:[Global getSliderGain:slider8.value appendDb:YES]];
                        }
                        [self refreshSliderOnButtons:((channelPage->ch_1_16_on_off & 0xFF00) >> 8)];
                        [self refreshSliderSoloButtons:((channelPage->ch_1_16_solo_ctrl_on_off & 0xFF00) >> 8)];
                        [self refreshSliderSoloSafeButtons:((channelPage->ch_1_16_solo_safe_ctrl_on_off & 0xFF00) >> 8)];
                        [self refreshSliderMeterButtons:((channelPage->ch_1_16_meter_pre_post & 0xFF00) >> 8)];
                        if (currentMode == MODE_PAN) {
                            sliderPan1.value = channelPage->channel_9_pan_level + PAN_CENTER_VALUE;
                            sliderPan2.value = channelPage->channel_10_pan_level + PAN_CENTER_VALUE;
                            sliderPan3.value = channelPage->channel_11_pan_level + PAN_CENTER_VALUE;
                            sliderPan4.value = channelPage->channel_12_pan_level + PAN_CENTER_VALUE;
                            sliderPan5.value = channelPage->channel_13_pan_level + PAN_CENTER_VALUE;
                            sliderPan6.value = channelPage->channel_14_pan_level + PAN_CENTER_VALUE;
                            sliderPan7.value = channelPage->channel_15_pan_level + PAN_CENTER_VALUE;
                            sliderPan8.value = channelPage->channel_16_pan_level + PAN_CENTER_VALUE;
                            [self onSliderPan1Action:nil];
                            [self onSliderPan2Action:nil];
                            [self onSliderPan3Action:nil];
                            [self onSliderPan4Action:nil];
                            [self onSliderPan5Action:nil];
                            [self onSliderPan6Action:nil];
                            [self onSliderPan7Action:nil];
                            [self onSliderPan8Action:nil];
                        }
                        if (eqDynDelayController) {
                            [eqDynDelayController processChannelViewPage:channelPage];
                        }
                        if (geqPeqController) {
                            [geqPeqController processChannelViewPage:channelPage];
                        }
                        if (dynController) {
                            [dynController processChannelViewPage:channelPage];
                        }
                        break;
                        
                    default:
                        break;
                }                    
            } else if (pageType == 1) {
                // Channel 17,18
                ViewPageUsbAudioPacket *usbAudioPage = (ViewPageUsbAudioPacket *)buffer;
                
                // Check CRC
                if (![Global checkCrc:buffer
                                  len:sizeof(ViewPageUsbAudioPacket)-FRAME_PROTOCOL_LEN pktCrc:usbAudioPage->crc]) {
                    NSLog(@"VIEW_PAGE_PARAMETER (UsbAudio) invalid crc!");
                    return;
                }
                
                switch (currentPage) {
                    case PAGE_USB_AUDIO:
                        if (!_slider1Lock) {
                            slider1.value = usbAudioPage->ch_17_fader;
                            [lblSlider1Value setText:[Global getSliderGain:slider1.value appendDb:YES]];
                        }
                        if (!_slider2Lock) {
                            slider2.value = usbAudioPage->ch_18_fader;
                            [lblSlider2Value setText:[Global getSliderGain:slider2.value appendDb:YES]];
                        }
                        BOOL slider1On = (usbAudioPage->ch_17_audio_setting & 0x0100) >> 8;
                        if (slider1On != btnSlider1On.selected && ![self sendCommandExists:CMD_CH17_USB_AUDIO_SETTING dspId:DSP_5]) {
                            btnSlider1On.selected = slider1On;
                        }
                        BOOL slider2On = (usbAudioPage->ch_18_audio_setting & 0x0100) >> 8;
                        if (slider2On != btnSlider2On.selected && ![self sendCommandExists:CMD_CH18_USB_AUDIO_SETTING dspId:DSP_5]) {
                            btnSlider2On.selected = slider2On;
                        }                            
                        BOOL slider1Solo = (usbAudioPage->ch_17_audio_setting & 0x0400) >> 10;
                        if (slider1Solo != btnSlider1Solo.selected && ![self sendCommandExists:CMD_CH17_USB_AUDIO_SETTING dspId:DSP_5]) {
                            btnSlider1Solo.selected = slider1Solo;
                        }
                        BOOL slider2Solo = (usbAudioPage->ch_18_audio_setting & 0x0400) >> 10;
                        if (slider2Solo != btnSlider2Solo.selected && ![self sendCommandExists:CMD_CH18_USB_AUDIO_SETTING dspId:DSP_5]) {
                            btnSlider2Solo.selected = slider2Solo;
                        }
                        BOOL slider1SoloSafe = (usbAudioPage->ch_17_audio_setting & 0x0800) >> 11;
                        if (slider1SoloSafe) {
                            [btnSlider1Solo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
                            [btnSlider1Solo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
                        } else {
                            [btnSlider1Solo setTitle:@"SOLO" forState:UIControlStateNormal];
                            [btnSlider1Solo setTitle:@"SOLO" forState:UIControlStateSelected];
                        }
                        BOOL slider2SoloSafe = (usbAudioPage->ch_18_audio_setting & 0x0800) >> 11;
                        if (slider2SoloSafe) {
                            [btnSlider2Solo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
                            [btnSlider2Solo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
                        } else {
                            [btnSlider2Solo setTitle:@"SOLO" forState:UIControlStateNormal];
                            [btnSlider2Solo setTitle:@"SOLO" forState:UIControlStateSelected];
                        }
                        BOOL slider1Meter = (usbAudioPage->ch_17_audio_setting & 0x8000) >> 15;
                        if (slider1Meter != btnSlider1Meter.selected && ![self sendCommandExists:CMD_CH17_USB_AUDIO_SETTING dspId:DSP_5]) {
                            btnSlider1Meter.selected = slider1Meter;
                        }
                        BOOL slider2Meter = (usbAudioPage->ch_18_audio_setting & 0x8000) >> 15;
                        if (slider2Meter != btnSlider2Meter.selected && ![self sendCommandExists:CMD_CH17_USB_AUDIO_SETTING dspId:DSP_5]) {
                            btnSlider2Meter.selected = slider2Meter;
                        }
                        if (currentMode == MODE_PAN) {
                            sliderPan1.value = usbAudioPage->ch_17_pan + PAN_CENTER_VALUE;
                            sliderPan2.value = usbAudioPage->ch_18_pan + PAN_CENTER_VALUE;
                            [self onSliderPan1Action:nil];
                            [self onSliderPan2Action:nil];
                        }
                        if (eqDynDelayController) {
                            [eqDynDelayController processUsbAudioViewPage:usbAudioPage];
                        }
                        if (geqPeqController) {
                            [geqPeqController processUsbAudioViewPage:usbAudioPage];
                        }
                        if (dynController) {
                            [dynController processUsbAudioViewPage:usbAudioPage];
                        }
                        break;
                    
                    default:
                        break;
                }                    
            } else if (pageType == 2) {
                // Multi
                ViewPageMultiPacket *multiPage = (ViewPageMultiPacket *)buffer;
                
                // Check CRC
                if (![Global checkCrc:buffer
                                  len:sizeof(ViewPageMultiPacket)-FRAME_PROTOCOL_LEN pktCrc:multiPage->crc]) {
                    NSLog(@"VIEW_PAGE_PARAMETER (Multi) invalid crc!");
                    return;
                }
                
                switch (currentPage) {
                    case PAGE_MT_1_4:
                        if (!_slider1Lock) {
                            slider1.value = multiPage->multi_1_fader;
                            [lblSlider1Value setText:[Global getSliderGain:slider1.value appendDb:YES]];
                        }
                        if (!_slider2Lock) {
                            slider2.value = multiPage->multi_2_fader;
                            [lblSlider2Value setText:[Global getSliderGain:slider2.value appendDb:YES]];
                        }
                        if (!_slider3Lock) {
                            slider3.value = multiPage->multi_3_fader;
                            [lblSlider3Value setText:[Global getSliderGain:slider3.value appendDb:YES]];
                        }
                        if (!_slider4Lock) {
                            slider4.value = multiPage->multi_4_fader;
                            [lblSlider4Value setText:[Global getSliderGain:slider4.value appendDb:YES]];
                        }
                        BOOL slider1On = multiPage->multi_1_ctrl & 0x01;
                        if (slider1On != btnSlider1On.selected && ![self sendCommandExists:CMD_MULTI_1_CTRL dspId:DSP_7]) {
                            btnSlider1On.selected = slider1On;
                        }
                        BOOL slider2On = multiPage->multi_2_ctrl & 0x01;
                        if (slider2On != btnSlider2On.selected && ![self sendCommandExists:CMD_MULTI_2_CTRL dspId:DSP_7]) {
                            btnSlider2On.selected = slider2On;
                        }
                        BOOL slider3On = multiPage->multi_3_ctrl & 0x01;
                        if (slider3On != btnSlider3On.selected && ![self sendCommandExists:CMD_MULTI_3_CTRL dspId:DSP_7]) {
                            btnSlider3On.selected = slider3On;
                        }
                        BOOL slider4On = multiPage->multi_4_ctrl & 0x01;
                        if (slider4On != btnSlider4On.selected && ![self sendCommandExists:CMD_MULTI_4_CTRL dspId:DSP_7]) {
                            btnSlider4On.selected = slider4On;
                        }
                        BOOL slider1Meter = multiPage->multi_1_4_meter_pre_post & 0x01;
                        if (slider1Meter != btnSlider1Meter.selected && ![self sendCommandExists:CMD_MULTI_METER dspId:DSP_7]) {
                            btnSlider1Meter.selected = slider1Meter;
                        }
                        BOOL slider2Meter = (multiPage->multi_1_4_meter_pre_post & 0x02) >> 1;
                        if (slider2Meter != btnSlider2Meter.selected && ![self sendCommandExists:CMD_MULTI_METER dspId:DSP_7]) {
                            btnSlider2Meter.selected = slider2Meter;
                        }
                        BOOL slider3Meter = (multiPage->multi_1_4_meter_pre_post & 0x04) >> 2;
                        if (slider3Meter != btnSlider3Meter.selected && ![self sendCommandExists:CMD_MULTI_METER dspId:DSP_7]) {
                            btnSlider3Meter.selected = slider3Meter;
                        }
                        BOOL slider4Meter = (multiPage->multi_1_4_meter_pre_post & 0x08) >> 3;
                        if (slider4Meter != btnSlider4Meter.selected && ![self sendCommandExists:CMD_MULTI_METER dspId:DSP_7]) {
                            btnSlider4Meter.selected = slider4Meter;
                        }
                        if (eqDynDelayController) {
                            [eqDynDelayController processMultiViewPage:multiPage];
                        }
                        if (geqPeqController) {
                            [geqPeqController processMultiViewPage:multiPage];
                        }
                        if (dynController) {
                            [dynController processMultiViewPage:multiPage];
                        }
                        break;
                        
                    default:
                        break;
                }
            } else if (pageType == 3) {
                // Main
                ViewPageMainPacket *mainPage = (ViewPageMainPacket *)buffer;
                
                // Check CRC
                if (![Global checkCrc:buffer
                                  len:sizeof(ViewPageMainPacket)-FRAME_PROTOCOL_LEN pktCrc:mainPage->crc]) {
                    NSLog(@"VIEW_PAGE_PARAMETER (Main) invalid crc!");
                    return;
                }
                
                switch (currentPage) {
                    case PAGE_CH_1_8:
                    case PAGE_CH_9_16:
                    case PAGE_USB_AUDIO:
                    case PAGE_AUX_1_4:
                    case PAGE_GP_1_4:
                    case PAGE_MT_1_4:
                    case PAGE_CTRL_RM:
                    case PAGE_MAIN:
                        if (!_sliderMainLock) {
                            sliderMain.value = mainPage->main_fader;
                            [lblSliderMainValue setText:[Global getSliderGain:sliderMain.value appendDb:YES]];
                        }
                        BOOL sliderMainStereoMono = (mainPage->main_meter_pre_post & 0x80) >> 7;
                        if (sliderMainStereoMono != btnSliderMainMono.selected && ![self sendCommandExists:CMD_MAIN_METER dspId:DSP_6]) {
                            btnSliderMainMono.selected = sliderMainStereoMono;
                        }
                        BOOL sliderMainOnOff = mainPage->main_l_r_ctrl & 0x01;
                        if (sliderMainOnOff != btnSliderMainOn.selected && ![self sendCommandExists:CMD_MAIN_LR_CTRL dspId:DSP_6]) {
                            btnSliderMainOn.selected = sliderMainOnOff;
                        }
                        BOOL sliderMainMeter = mainPage->main_meter_pre_post & 0x01;
                        if (sliderMainMeter != btnSliderMainMeter.selected && ![self sendCommandExists:CMD_MAIN_METER dspId:DSP_6]) {
                            btnSliderMainMeter.selected = sliderMainMeter;
                        }
                        if (eqDynDelayController) {
                            [eqDynDelayController processMainViewPage:mainPage];
                        }
                        if (geqPeqController) {
                            [geqPeqController processMainViewPage:mainPage];
                        }
                        if (dynController) {
                            [dynController processMainViewPage:mainPage];
                        }
                        break;
                    
                    default:
                        break;
                }                   
            }
            //NSLog(@"Received View Page Param, len = %d", len);
            return;
        }
        case CHANNEL_PAGE_PARAMETER:
        {
            int pageType = buffer[8];
            if (pageType == CHANNEL_PAGE_CH_1_8) {
                ChannelCh1_8PagePacket *pkt = (ChannelCh1_8PagePacket *)buffer;
                
                // Check CRC
                if (![Global checkCrc:buffer
                                  len:sizeof(ChannelCh1_8PagePacket)-FRAME_PROTOCOL_LEN pktCrc:pkt->crc]) {
                    NSLog(@"CHANNEL_PAGE_PARAMETER (Ch1-8) invalid crc!");
                    return;
                }
                
                _chOffOn = pkt->ch_on_off;
                _chSoloCtrl = pkt->solo_ctrl;
                _chMeterPrePost = pkt->ch_meter_pre_post;
                _mainCtrl = pkt->main_l_r_ctrl;
                _mainMeterPrePost = pkt->main_meter_pre_post;
                _ch48vCtrl = pkt->ch_48v_ctrl;
                
                if (!_slider1Lock) {
                    slider1.value = pkt->ch_1_fader;
                    [lblSlider1Value setText:[Global getSliderGain:slider1.value appendDb:YES]];
                }
                if (!_slider2Lock) {
                    slider2.value = pkt->ch_2_fader;
                    [lblSlider2Value setText:[Global getSliderGain:slider2.value appendDb:YES]];
                }
                if (!_slider3Lock) {
                    slider3.value = pkt->ch_3_fader;
                    [lblSlider3Value setText:[Global getSliderGain:slider3.value appendDb:YES]];
                }
                if (!_slider4Lock) {
                    slider4.value = pkt->ch_4_fader;
                    [lblSlider4Value setText:[Global getSliderGain:slider4.value appendDb:YES]];
                }
                if (!_slider5Lock) {
                    slider5.value = pkt->ch_5_fader;
                    [lblSlider5Value setText:[Global getSliderGain:slider5.value appendDb:YES]];
                }
                if (!_slider6Lock) {
                    slider6.value = pkt->ch_6_fader;
                    [lblSlider6Value setText:[Global getSliderGain:slider6.value appendDb:YES]];
                }
                if (!_slider7Lock) {
                    slider7.value = pkt->ch_7_fader;
                    [lblSlider7Value setText:[Global getSliderGain:slider7.value appendDb:YES]];
                }
                if (!_slider8Lock) {
                    slider8.value = pkt->ch_8_fader;
                    [lblSlider8Value setText:[Global getSliderGain:slider8.value appendDb:YES]];
                }
                [self refreshSliderOnButtons:(pkt->ch_on_off & 0xFF)];
                [self refreshSliderSoloButtons:(pkt->solo_ctrl & 0xFF)];
                [self refreshSliderSoloSafeButtons:(pkt->solo_safe_ctrl & 0xFF)];
                [self refreshSliderMeterButtons:(pkt->ch_meter_pre_post & 0xFF)];
                if (currentMode == MODE_PAN) {
                    sliderPan1.value = pkt->ch_1_pan + PAN_CENTER_VALUE;
                    sliderPan2.value = pkt->ch_2_pan + PAN_CENTER_VALUE;
                    sliderPan3.value = pkt->ch_3_pan + PAN_CENTER_VALUE;
                    sliderPan4.value = pkt->ch_4_pan + PAN_CENTER_VALUE;
                    sliderPan5.value = pkt->ch_5_pan + PAN_CENTER_VALUE;
                    sliderPan6.value = pkt->ch_6_pan + PAN_CENTER_VALUE;
                    sliderPan7.value = pkt->ch_7_pan + PAN_CENTER_VALUE;
                    sliderPan8.value = pkt->ch_8_pan + PAN_CENTER_VALUE;
                    [self onSliderPan1Action:nil];
                    [self onSliderPan2Action:nil];
                    [self onSliderPan3Action:nil];
                    [self onSliderPan4Action:nil];
                    [self onSliderPan5Action:nil];
                    [self onSliderPan6Action:nil];
                    [self onSliderPan7Action:nil];
                    [self onSliderPan8Action:nil];
                }
                orderChannel[CH_1] = (pkt->ch_1_ctrl & 0xE000) >> 13;
                orderChannel[CH_2] = (pkt->ch_2_ctrl & 0xE000) >> 13;
                orderChannel[CH_3] = (pkt->ch_3_ctrl & 0xE000) >> 13;
                orderChannel[CH_4] = (pkt->ch_4_ctrl & 0xE000) >> 13;
                orderChannel[CH_5] = (pkt->ch_5_ctrl & 0xE000) >> 13;
                orderChannel[CH_6] = (pkt->ch_6_ctrl & 0xE000) >> 13;
                orderChannel[CH_7] = (pkt->ch_7_ctrl & 0xE000) >> 13;
                orderChannel[CH_8] = (pkt->ch_8_ctrl & 0xE000) >> 13;
                orderMain = (pkt->main_l_r_ctrl & 0xE000) >> 13;
                [self updateOrderButtons];                    
                if (!_sliderMainLock) {
                    sliderMain.value = pkt->main_fader;
                    [lblSliderMainValue setText:[Global getSliderGain:sliderMain.value appendDb:YES]];
                }
                BOOL sliderMainStereoMono = (pkt->main_meter_pre_post & 0x80) >> 7;
                if (sliderMainStereoMono != btnSliderMainMono.selected && ![self sendCommandExists:CMD_MAIN_METER dspId:DSP_6]) {
                    btnSliderMainMono.selected = sliderMainStereoMono;
                }
                BOOL sliderMainOnOff = pkt->main_l_r_ctrl & 0x01;
                if (sliderMainOnOff != btnSliderMainOn.selected && ![self sendCommandExists:CMD_MAIN_LR_CTRL dspId:DSP_6]) {
                    btnSliderMainOn.selected = sliderMainOnOff;
                }
                BOOL sliderMainMeter = pkt->main_meter_pre_post & 0x01;
                if (sliderMainMeter != btnSliderMainMeter.selected && ![self sendCommandExists:CMD_MAIN_METER dspId:DSP_6]) {
                    btnSliderMainMeter.selected = sliderMainMeter;
                }
                // Phantom power view
                int phantomPower = pkt->ch_48v_ctrl;
                btnPhantomPowerCh1OnOff.selected = phantomPower & 0x01;
                btnPhantomPowerCh2OnOff.selected = (phantomPower & 0x02) >> 1;
                btnPhantomPowerCh3OnOff.selected = (phantomPower & 0x04) >> 2;
                btnPhantomPowerCh4OnOff.selected = (phantomPower & 0x08) >> 3;
                btnPhantomPowerCh5OnOff.selected = (phantomPower & 0x10) >> 4;
                btnPhantomPowerCh6OnOff.selected = (phantomPower & 0x20) >> 5;
                btnPhantomPowerCh7OnOff.selected = (phantomPower & 0x40) >> 6;
                btnPhantomPowerCh8OnOff.selected = (phantomPower & 0x80) >> 7;
                btnPhantomPowerCh9OnOff.selected = (phantomPower & 0x0100) >> 8;
                btnPhantomPowerCh10OnOff.selected = (phantomPower & 0x0200) >> 9;
                btnPhantomPowerCh11OnOff.selected = (phantomPower & 0x0400) >> 10;
                btnPhantomPowerCh12OnOff.selected = (phantomPower & 0x0800) >> 11;
                btnPhantomPowerCh13OnOff.selected = (phantomPower & 0x1000) >> 12;
                btnPhantomPowerCh14OnOff.selected = (phantomPower & 0x2000) >> 13;
                btnPhantomPowerCh15OnOff.selected = (phantomPower & 0x4000) >> 14;
                btnPhantomPowerCh16OnOff.selected = (phantomPower & 0x8000) >> 15;
                // Inv
                invOffOn[CH_1] = (pkt->ch_1_ctrl & 0x1000) >> 12;
                invOffOn[CH_2] = (pkt->ch_2_ctrl & 0x1000) >> 12;
                invOffOn[CH_3] = (pkt->ch_3_ctrl & 0x1000) >> 12;
                invOffOn[CH_4] = (pkt->ch_4_ctrl & 0x1000) >> 12;
                invOffOn[CH_5] = (pkt->ch_5_ctrl & 0x1000) >> 12;
                invOffOn[CH_6] = (pkt->ch_6_ctrl & 0x1000) >> 12;
                invOffOn[CH_7] = (pkt->ch_7_ctrl & 0x1000) >> 12;
                invOffOn[CH_8] = (pkt->ch_8_ctrl & 0x1000) >> 12;
                // Delay
                btnDelayCh1OnOff.selected = (pkt->ch_1_ctrl & 0x04) >> 2;
                btnDelayCh2OnOff.selected = (pkt->ch_2_ctrl & 0x04) >> 2;
                btnDelayCh3OnOff.selected = (pkt->ch_3_ctrl & 0x04) >> 2;
                btnDelayCh4OnOff.selected = (pkt->ch_4_ctrl & 0x04) >> 2;
                btnDelayCh5OnOff.selected = (pkt->ch_5_ctrl & 0x04) >> 2;
                btnDelayCh6OnOff.selected = (pkt->ch_6_ctrl & 0x04) >> 2;
                btnDelayCh7OnOff.selected = (pkt->ch_7_ctrl & 0x04) >> 2;
                btnDelayCh8OnOff.selected = (pkt->ch_8_ctrl & 0x04) >> 2;
                btnDelayMainOnOff.selected = (pkt->main_l_r_ctrl & 0x04) >> 2;
                knobDelayCh1.value = [Global delayToKnobValue:pkt->ch_1_delay_time];
                [self knobDelayCh1DidChange:nil];
                knobDelayCh2.value = [Global delayToKnobValue:pkt->ch_2_delay_time];
                [self knobDelayCh2DidChange:nil];
                knobDelayCh3.value = [Global delayToKnobValue:pkt->ch_3_delay_time];
                [self knobDelayCh3DidChange:nil];
                knobDelayCh4.value = [Global delayToKnobValue:pkt->ch_4_delay_time];
                [self knobDelayCh4DidChange:nil];
                knobDelayCh5.value = [Global delayToKnobValue:pkt->ch_5_delay_time];
                [self knobDelayCh5DidChange:nil];
                knobDelayCh6.value = [Global delayToKnobValue:pkt->ch_6_delay_time];
                [self knobDelayCh6DidChange:nil];
                knobDelayCh7.value = [Global delayToKnobValue:pkt->ch_7_delay_time];
                [self knobDelayCh7DidChange:nil];
                knobDelayCh8.value = [Global delayToKnobValue:pkt->ch_8_delay_time];
                [self knobDelayCh8DidChange:nil];
                knobDelayMain.value = [Global delayToKnobValue:pkt->main_l_r_delay_time];
                [self knobDelayMainDidChange:nil];
                // DYN
                if ((pkt->ch_1_ctrl & 0x02) >> 1) {
                    [btnDynG1 setSelected:((pkt->ch_1_ctrl & 0x0100) >> 8)];
                    [btnDynE1 setSelected:((pkt->ch_1_ctrl & 0x0200) >> 9)];
                    [btnDynC1 setSelected:((pkt->ch_1_ctrl & 0x0400) >> 10)];
                    [btnDynL1 setSelected:((pkt->ch_1_ctrl & 0x0800) >> 11)];
                } else {
                    [btnDynG1 setSelected:NO];
                    [btnDynE1 setSelected:NO];
                    [btnDynC1 setSelected:NO];
                    [btnDynL1 setSelected:NO];
                }
                if ((pkt->ch_2_ctrl & 0x02) >> 1) {
                    [btnDynG2 setSelected:((pkt->ch_2_ctrl & 0x0100) >> 8)];
                    [btnDynE2 setSelected:((pkt->ch_2_ctrl & 0x0200) >> 9)];
                    [btnDynC2 setSelected:((pkt->ch_2_ctrl & 0x0400) >> 10)];
                    [btnDynL2 setSelected:((pkt->ch_2_ctrl & 0x0800) >> 11)];
                } else {
                    [btnDynG2 setSelected:NO];
                    [btnDynE2 setSelected:NO];
                    [btnDynC2 setSelected:NO];
                    [btnDynL2 setSelected:NO];
                }
                if ((pkt->ch_3_ctrl & 0x02) >> 1) {
                    [btnDynG3 setSelected:((pkt->ch_3_ctrl & 0x0100) >> 8)];
                    [btnDynE3 setSelected:((pkt->ch_3_ctrl & 0x0200) >> 9)];
                    [btnDynC3 setSelected:((pkt->ch_3_ctrl & 0x0400) >> 10)];
                    [btnDynL3 setSelected:((pkt->ch_3_ctrl & 0x0800) >> 11)];
                } else {
                    [btnDynG3 setSelected:NO];
                    [btnDynE3 setSelected:NO];
                    [btnDynC3 setSelected:NO];
                    [btnDynL3 setSelected:NO];
                }
                if ((pkt->ch_4_ctrl & 0x02) >> 1) {
                    [btnDynG4 setSelected:((pkt->ch_4_ctrl & 0x0100) >> 8)];
                    [btnDynE4 setSelected:((pkt->ch_4_ctrl & 0x0200) >> 9)];
                    [btnDynC4 setSelected:((pkt->ch_4_ctrl & 0x0400) >> 10)];
                    [btnDynL4 setSelected:((pkt->ch_4_ctrl & 0x0800) >> 11)];
                } else {
                    [btnDynG4 setSelected:NO];
                    [btnDynE4 setSelected:NO];
                    [btnDynC4 setSelected:NO];
                    [btnDynL4 setSelected:NO];
                }
                if ((pkt->ch_5_ctrl & 0x02) >> 1) {
                    [btnDynG5 setSelected:((pkt->ch_5_ctrl & 0x0100) >> 8)];
                    [btnDynE5 setSelected:((pkt->ch_5_ctrl & 0x0200) >> 9)];
                    [btnDynC5 setSelected:((pkt->ch_5_ctrl & 0x0400) >> 10)];
                    [btnDynL5 setSelected:((pkt->ch_5_ctrl & 0x0800) >> 11)];
                } else {
                    [btnDynG5 setSelected:NO];
                    [btnDynE5 setSelected:NO];
                    [btnDynC5 setSelected:NO];
                    [btnDynL5 setSelected:NO];
                }
                if ((pkt->ch_6_ctrl & 0x02) >> 1) {
                    [btnDynG6 setSelected:((pkt->ch_6_ctrl & 0x0100) >> 8)];
                    [btnDynE6 setSelected:((pkt->ch_6_ctrl & 0x0200) >> 9)];
                    [btnDynC6 setSelected:((pkt->ch_6_ctrl & 0x0400) >> 10)];
                    [btnDynL6 setSelected:((pkt->ch_6_ctrl & 0x0800) >> 11)];
                } else {
                    [btnDynG6 setSelected:NO];
                    [btnDynE6 setSelected:NO];
                    [btnDynC6 setSelected:NO];
                    [btnDynL6 setSelected:NO];
                }
                if ((pkt->ch_7_ctrl & 0x02) >> 1) {
                    [btnDynG7 setSelected:((pkt->ch_7_ctrl & 0x0100) >> 8)];
                    [btnDynE7 setSelected:((pkt->ch_7_ctrl & 0x0200) >> 9)];
                    [btnDynC7 setSelected:((pkt->ch_7_ctrl & 0x0400) >> 10)];
                    [btnDynL7 setSelected:((pkt->ch_7_ctrl & 0x0800) >> 11)];
                } else {
                    [btnDynG7 setSelected:NO];
                    [btnDynE7 setSelected:NO];
                    [btnDynC7 setSelected:NO];
                    [btnDynL7 setSelected:NO];
                }
                if ((pkt->ch_8_ctrl & 0x02) >> 1) {
                    [btnDynG8 setSelected:((pkt->ch_8_ctrl & 0x0100) >> 8)];
                    [btnDynE8 setSelected:((pkt->ch_8_ctrl & 0x0200) >> 9)];
                    [btnDynC8 setSelected:((pkt->ch_8_ctrl & 0x0400) >> 10)];
                    [btnDynL8 setSelected:((pkt->ch_8_ctrl & 0x0800) >> 11)];
                } else {
                    [btnDynG8 setSelected:NO];
                    [btnDynE8 setSelected:NO];
                    [btnDynC8 setSelected:NO];
                    [btnDynL8 setSelected:NO];
                }
                if ((pkt->main_l_r_ctrl & 0x02) >> 1) {
                    [btnDynGMain setSelected:((pkt->main_l_r_ctrl & 0x0100) >> 8)];
                    [btnDynEMain setSelected:((pkt->main_l_r_ctrl & 0x0200) >> 9)];
                    [btnDynCMain setSelected:((pkt->main_l_r_ctrl & 0x0400) >> 10)];
                    [btnDynLMain setSelected:((pkt->main_l_r_ctrl & 0x0800) >> 11)];
                } else {
                    [btnDynGMain setSelected:NO];
                    [btnDynEMain setSelected:NO];
                    [btnDynCMain setSelected:NO];
                    [btnDynLMain setSelected:NO];
                }
                [self refreshModeButtons];
                
                // Update EQ preview
                if (geqPeqController) {
                    [geqPeqController processChannel1_8PageReply:pkt];
                }
                
                // Update DYN preview
                if (dynController) {
                    [dynController processChannel1_8PageReply:pkt];
                }
            } else if (pageType == CHANNEL_PAGE_CH_9_16) {
                ChannelCh9_16PagePacket *pkt = (ChannelCh9_16PagePacket *)buffer;
                
                // Check CRC
                if (![Global checkCrc:buffer
                                  len:sizeof(ChannelCh9_16PagePacket)-FRAME_PROTOCOL_LEN pktCrc:pkt->crc]) {
                    NSLog(@"CHANNEL_PAGE_PARAMETER (Ch9-16) invalid crc!");
                    return;
                }
                
                _chOffOn = pkt->ch_on_off;
                _chSoloCtrl = pkt->solo_ctrl;
                _chMeterPrePost = pkt->ch_meter_pre_post;
                _mainCtrl = pkt->main_l_r_ctrl;
                _mainMeterPrePost = pkt->main_meter_pre_post;
                _ch48vCtrl = pkt->ch_48v_ctrl;

                if (!_slider1Lock) {
                    slider1.value = pkt->ch_9_fader;
                    [lblSlider1Value setText:[Global getSliderGain:slider1.value appendDb:YES]];
                }
                if (!_slider2Lock) {
                    slider2.value = pkt->ch_10_fader;
                    [lblSlider2Value setText:[Global getSliderGain:slider2.value appendDb:YES]];
                }
                if (!_slider3Lock) {
                    slider3.value = pkt->ch_11_fader;
                    [lblSlider3Value setText:[Global getSliderGain:slider3.value appendDb:YES]];
                }
                if (!_slider4Lock) {
                    slider4.value = pkt->ch_12_fader;
                    [lblSlider4Value setText:[Global getSliderGain:slider4.value appendDb:YES]];
                }
                if (!_slider5Lock) {
                    slider5.value = pkt->ch_13_fader;
                    [lblSlider5Value setText:[Global getSliderGain:slider5.value appendDb:YES]];
                }
                if (!_slider6Lock) {
                    slider6.value = pkt->ch_14_fader;
                    [lblSlider6Value setText:[Global getSliderGain:slider6.value appendDb:YES]];
                }
                if (!_slider7Lock) {
                    slider7.value = pkt->ch_15_fader;
                    [lblSlider7Value setText:[Global getSliderGain:slider7.value appendDb:YES]];
                }
                if (!_slider8Lock) {
                    slider8.value = pkt->ch_16_fader;
                    [lblSlider8Value setText:[Global getSliderGain:slider8.value appendDb:YES]];
                }
                [self refreshSliderOnButtons:((pkt->ch_on_off & 0xFF00) >> 8)];
                [self refreshSliderSoloButtons:((pkt->solo_ctrl & 0xFF00) >> 8)];
                [self refreshSliderSoloSafeButtons:((pkt->solo_safe_ctrl & 0xFF00) >> 8)];
                [self refreshSliderMeterButtons:((pkt->ch_meter_pre_post & 0xFF00) >> 8)];
                if (currentMode == MODE_PAN) {
                    sliderPan1.value = pkt->ch_9_pan + PAN_CENTER_VALUE;
                    sliderPan2.value = pkt->ch_10_pan + PAN_CENTER_VALUE;
                    sliderPan3.value = pkt->ch_11_pan + PAN_CENTER_VALUE;
                    sliderPan4.value = pkt->ch_12_pan + PAN_CENTER_VALUE;
                    sliderPan5.value = pkt->ch_13_pan + PAN_CENTER_VALUE;
                    sliderPan6.value = pkt->ch_14_pan + PAN_CENTER_VALUE;
                    sliderPan7.value = pkt->ch_15_pan + PAN_CENTER_VALUE;
                    sliderPan8.value = pkt->ch_16_pan + PAN_CENTER_VALUE;
                    [self onSliderPan1Action:nil];
                    [self onSliderPan2Action:nil];
                    [self onSliderPan3Action:nil];
                    [self onSliderPan4Action:nil];
                    [self onSliderPan5Action:nil];
                    [self onSliderPan6Action:nil];
                    [self onSliderPan7Action:nil];
                    [self onSliderPan8Action:nil];
                }
                orderChannel[CH_9] = (pkt->ch_9_ctrl & 0xE000) >> 13;
                orderChannel[CH_10] = (pkt->ch_10_ctrl & 0xE000) >> 13;
                orderChannel[CH_11] = (pkt->ch_11_ctrl & 0xE000) >> 13;
                orderChannel[CH_12] = (pkt->ch_12_ctrl & 0xE000) >> 13;
                orderChannel[CH_13] = (pkt->ch_13_ctrl & 0xE000) >> 13;
                orderChannel[CH_14] = (pkt->ch_14_ctrl & 0xE000) >> 13;
                orderChannel[CH_15] = (pkt->ch_15_ctrl & 0xE000) >> 13;
                orderChannel[CH_16] = (pkt->ch_16_ctrl & 0xE000) >> 13;
                orderMain = (pkt->main_l_r_ctrl & 0xE000) >> 13;
                [self updateOrderButtons];
                if (!_sliderMainLock) {
                    sliderMain.value = pkt->main_fader;
                    [lblSliderMainValue setText:[Global getSliderGain:sliderMain.value appendDb:YES]];
                }
                BOOL sliderMainStereoMono = (pkt->main_meter_pre_post & 0x80) >> 7;
                if (sliderMainStereoMono != btnSliderMainMono.selected && ![self sendCommandExists:CMD_MAIN_METER dspId:DSP_6]) {
                    btnSliderMainMono.selected = sliderMainStereoMono;
                }
                BOOL sliderMainOnOff = pkt->main_l_r_ctrl & 0x01;
                if (sliderMainOnOff != btnSliderMainOn.selected && ![self sendCommandExists:CMD_MAIN_LR_CTRL dspId:DSP_6]) {
                    btnSliderMainOn.selected = sliderMainOnOff;
                }
                BOOL sliderMainMeter = pkt->main_meter_pre_post & 0x01;
                if (sliderMainMeter != btnSliderMainMeter.selected && ![self sendCommandExists:CMD_MAIN_METER dspId:DSP_6]) {
                    btnSliderMainMeter.selected = sliderMainMeter;
                }
                // Phantom power view
                int phantomPower = pkt->ch_48v_ctrl;
                btnPhantomPowerCh1OnOff.selected = phantomPower & 0x01;
                btnPhantomPowerCh2OnOff.selected = (phantomPower & 0x02) >> 1;
                btnPhantomPowerCh3OnOff.selected = (phantomPower & 0x04) >> 2;
                btnPhantomPowerCh4OnOff.selected = (phantomPower & 0x08) >> 3;
                btnPhantomPowerCh5OnOff.selected = (phantomPower & 0x10) >> 4;
                btnPhantomPowerCh6OnOff.selected = (phantomPower & 0x20) >> 5;
                btnPhantomPowerCh7OnOff.selected = (phantomPower & 0x40) >> 6;
                btnPhantomPowerCh8OnOff.selected = (phantomPower & 0x80) >> 7;
                btnPhantomPowerCh9OnOff.selected = (phantomPower & 0x0100) >> 8;
                btnPhantomPowerCh10OnOff.selected = (phantomPower & 0x0200) >> 9;
                btnPhantomPowerCh11OnOff.selected = (phantomPower & 0x0400) >> 10;
                btnPhantomPowerCh12OnOff.selected = (phantomPower & 0x0800) >> 11;
                btnPhantomPowerCh13OnOff.selected = (phantomPower & 0x1000) >> 12;
                btnPhantomPowerCh14OnOff.selected = (phantomPower & 0x2000) >> 13;
                btnPhantomPowerCh15OnOff.selected = (phantomPower & 0x4000) >> 14;
                btnPhantomPowerCh16OnOff.selected = (phantomPower & 0x8000) >> 15;
                // Inv
                invOffOn[CH_9] = (pkt->ch_9_ctrl & 0x1000) >> 12;
                invOffOn[CH_10] = (pkt->ch_10_ctrl & 0x1000) >> 12;
                invOffOn[CH_11] = (pkt->ch_11_ctrl & 0x1000) >> 12;
                invOffOn[CH_12] = (pkt->ch_12_ctrl & 0x1000) >> 12;
                invOffOn[CH_13] = (pkt->ch_13_ctrl & 0x1000) >> 12;
                invOffOn[CH_14] = (pkt->ch_14_ctrl & 0x1000) >> 12;
                invOffOn[CH_15] = (pkt->ch_15_ctrl & 0x1000) >> 12;
                invOffOn[CH_16] = (pkt->ch_16_ctrl & 0x1000) >> 12;
                // Delay
                btnDelayCh9OnOff.selected = (pkt->ch_9_ctrl & 0x04) >> 2;
                btnDelayCh10OnOff.selected = (pkt->ch_10_ctrl & 0x04) >> 2;
                btnDelayCh11OnOff.selected = (pkt->ch_11_ctrl & 0x04) >> 2;
                btnDelayCh12OnOff.selected = (pkt->ch_12_ctrl & 0x04) >> 2;
                btnDelayCh13OnOff.selected = (pkt->ch_13_ctrl & 0x04) >> 2;
                btnDelayCh14OnOff.selected = (pkt->ch_14_ctrl & 0x04) >> 2;
                btnDelayCh15OnOff.selected = (pkt->ch_15_ctrl & 0x04) >> 2;
                btnDelayCh16OnOff.selected = (pkt->ch_16_ctrl & 0x04) >> 2;
                btnDelayMainOnOff.selected = (pkt->main_l_r_ctrl & 0x04) >> 2;
                knobDelayCh9.value = [Global delayToKnobValue:pkt->ch_9_delay_time];
                [self knobDelayCh9DidChange:nil];
                knobDelayCh10.value = [Global delayToKnobValue:pkt->ch_10_delay_time];
                [self knobDelayCh10DidChange:nil];
                knobDelayCh11.value = [Global delayToKnobValue:pkt->ch_11_delay_time];
                [self knobDelayCh11DidChange:nil];
                knobDelayCh12.value = [Global delayToKnobValue:pkt->ch_12_delay_time];
                [self knobDelayCh12DidChange:nil];
                knobDelayCh13.value = [Global delayToKnobValue:pkt->ch_13_delay_time];
                [self knobDelayCh13DidChange:nil];
                knobDelayCh14.value = [Global delayToKnobValue:pkt->ch_14_delay_time];
                [self knobDelayCh14DidChange:nil];
                knobDelayCh15.value = [Global delayToKnobValue:pkt->ch_15_delay_time];
                [self knobDelayCh15DidChange:nil];
                knobDelayCh16.value = [Global delayToKnobValue:pkt->ch_16_delay_time];
                [self knobDelayCh16DidChange:nil];
                knobDelayMain.value = [Global delayToKnobValue:pkt->main_l_r_delay_time];
                [self knobDelayMainDidChange:nil];
                // DYN
                if ((pkt->ch_9_ctrl & 0x02) >> 1) {
                    [btnDynG1 setSelected:((pkt->ch_9_ctrl & 0x0100) >> 8)];
                    [btnDynE1 setSelected:((pkt->ch_9_ctrl & 0x0200) >> 9)];
                    [btnDynC1 setSelected:((pkt->ch_9_ctrl & 0x0400) >> 10)];
                    [btnDynL1 setSelected:((pkt->ch_9_ctrl & 0x0800) >> 11)];
                } else {
                    [btnDynG1 setSelected:NO];
                    [btnDynE1 setSelected:NO];
                    [btnDynC1 setSelected:NO];
                    [btnDynL1 setSelected:NO];
                }
                if ((pkt->ch_10_ctrl & 0x02) >> 1) {
                    [btnDynG2 setSelected:((pkt->ch_10_ctrl & 0x0100) >> 8)];
                    [btnDynE2 setSelected:((pkt->ch_10_ctrl & 0x0200) >> 9)];
                    [btnDynC2 setSelected:((pkt->ch_10_ctrl & 0x0400) >> 10)];
                    [btnDynL2 setSelected:((pkt->ch_10_ctrl & 0x0800) >> 11)];
                } else {
                    [btnDynG2 setSelected:NO];
                    [btnDynE2 setSelected:NO];
                    [btnDynC2 setSelected:NO];
                    [btnDynL2 setSelected:NO];
                }
                if ((pkt->ch_11_ctrl & 0x02) >> 1) {
                    [btnDynG3 setSelected:((pkt->ch_11_ctrl & 0x0100) >> 8)];
                    [btnDynE3 setSelected:((pkt->ch_11_ctrl & 0x0200) >> 9)];
                    [btnDynC3 setSelected:((pkt->ch_11_ctrl & 0x0400) >> 10)];
                    [btnDynL3 setSelected:((pkt->ch_11_ctrl & 0x0800) >> 11)];
                } else {
                    [btnDynG3 setSelected:NO];
                    [btnDynE3 setSelected:NO];
                    [btnDynC3 setSelected:NO];
                    [btnDynL3 setSelected:NO];
                }
                if ((pkt->ch_12_ctrl & 0x02) >> 1) {
                    [btnDynG4 setSelected:((pkt->ch_12_ctrl & 0x0100) >> 8)];
                    [btnDynE4 setSelected:((pkt->ch_12_ctrl & 0x0200) >> 9)];
                    [btnDynC4 setSelected:((pkt->ch_12_ctrl & 0x0400) >> 10)];
                    [btnDynL4 setSelected:((pkt->ch_12_ctrl & 0x0800) >> 11)];
                } else {
                    [btnDynG4 setSelected:NO];
                    [btnDynE4 setSelected:NO];
                    [btnDynC4 setSelected:NO];
                    [btnDynL4 setSelected:NO];
                }
                if ((pkt->ch_13_ctrl & 0x02) >> 1) {
                    [btnDynG5 setSelected:((pkt->ch_13_ctrl & 0x0100) >> 8)];
                    [btnDynE5 setSelected:((pkt->ch_13_ctrl & 0x0200) >> 9)];
                    [btnDynC5 setSelected:((pkt->ch_13_ctrl & 0x0400) >> 10)];
                    [btnDynL5 setSelected:((pkt->ch_13_ctrl & 0x0800) >> 11)];
                } else {
                    [btnDynG5 setSelected:NO];
                    [btnDynE5 setSelected:NO];
                    [btnDynC5 setSelected:NO];
                    [btnDynL5 setSelected:NO];
                }
                if ((pkt->ch_14_ctrl & 0x02) >> 1) {
                    [btnDynG6 setSelected:((pkt->ch_14_ctrl & 0x0100) >> 8)];
                    [btnDynE6 setSelected:((pkt->ch_14_ctrl & 0x0200) >> 9)];
                    [btnDynC6 setSelected:((pkt->ch_14_ctrl & 0x0400) >> 10)];
                    [btnDynL6 setSelected:((pkt->ch_14_ctrl & 0x0800) >> 11)];
                } else {
                    [btnDynG6 setSelected:NO];
                    [btnDynE6 setSelected:NO];
                    [btnDynC6 setSelected:NO];
                    [btnDynL6 setSelected:NO];
                }
                if ((pkt->ch_15_ctrl & 0x02) >> 1) {
                    [btnDynG7 setSelected:((pkt->ch_15_ctrl & 0x0100) >> 8)];
                    [btnDynE7 setSelected:((pkt->ch_15_ctrl & 0x0200) >> 9)];
                    [btnDynC7 setSelected:((pkt->ch_15_ctrl & 0x0400) >> 10)];
                    [btnDynL7 setSelected:((pkt->ch_15_ctrl & 0x0800) >> 11)];
                } else {
                    [btnDynG7 setSelected:NO];
                    [btnDynE7 setSelected:NO];
                    [btnDynC7 setSelected:NO];
                    [btnDynL7 setSelected:NO];
                }
                if ((pkt->ch_16_ctrl & 0x02) >> 1) {
                    [btnDynG8 setSelected:((pkt->ch_16_ctrl & 0x0100) >> 8)];
                    [btnDynE8 setSelected:((pkt->ch_16_ctrl & 0x0200) >> 9)];
                    [btnDynC8 setSelected:((pkt->ch_16_ctrl & 0x0400) >> 10)];
                    [btnDynL8 setSelected:((pkt->ch_16_ctrl & 0x0800) >> 11)];
                } else {
                    [btnDynG8 setSelected:NO];
                    [btnDynE8 setSelected:NO];
                    [btnDynC8 setSelected:NO];
                    [btnDynL8 setSelected:NO];
                }
                if ((pkt->main_l_r_ctrl & 0x02) >> 1) {
                    [btnDynGMain setSelected:((pkt->main_l_r_ctrl & 0x0100) >> 8)];
                    [btnDynEMain setSelected:((pkt->main_l_r_ctrl & 0x0200) >> 9)];
                    [btnDynCMain setSelected:((pkt->main_l_r_ctrl & 0x0400) >> 10)];
                    [btnDynLMain setSelected:((pkt->main_l_r_ctrl & 0x0800) >> 11)];
                } else {
                    [btnDynGMain setSelected:NO];
                    [btnDynEMain setSelected:NO];
                    [btnDynCMain setSelected:NO];
                    [btnDynLMain setSelected:NO];
                }
                [self refreshModeButtons];
                
                // Update EQ preview
                if (geqPeqController) {
                    [geqPeqController processChannel9_16PageReply:pkt];
                }
                
                // Update DYN preview
                if (dynController) {
                    [dynController processChannel9_16PageReply:pkt];
                }
            } else if (pageType == CHANNEL_PAGE_CH_17_18) {
                ChannelCh17_18PagePacket *pkt = (ChannelCh17_18PagePacket *)buffer;
                
                // Check CRC
                if (![Global checkCrc:buffer
                                  len:sizeof(ChannelCh17_18PagePacket)-FRAME_PROTOCOL_LEN pktCrc:pkt->crc]) {
                    NSLog(@"CHANNEL_PAGE_PARAMETER (Ch17-18) invalid crc!");
                    return;
                }

                _ch17AudioSetting = pkt->ch_17_audio_setting;
                _ch18AudioSetting = pkt->ch_18_audio_setting;
                _ch17Ctrl = pkt->ch_17_ctrl;
                _ch18Ctrl = pkt->ch_18_ctrl;
                _mainCtrl = pkt->main_l_r_ctrl;
                _mainMeterPrePost = pkt->main_meter_pre_post;

                if (!_slider1Lock) {
                    slider1.value = pkt->ch_17_fader;
                    [lblSlider1Value setText:[Global getSliderGain:slider1.value appendDb:YES]];
                }
                if (!_slider2Lock) {
                    slider2.value = pkt->ch_18_fader;
                    [lblSlider2Value setText:[Global getSliderGain:slider2.value appendDb:YES]];
                }
                
                BOOL slider1On = (pkt->ch_17_audio_setting & 0x0100) >> 8;
                if (slider1On != btnSlider1On.selected && ![self sendCommandExists:CMD_CH17_USB_AUDIO_SETTING dspId:DSP_5]) {
                    btnSlider1On.selected = slider1On;
                }
                BOOL slider2On = (pkt->ch_18_audio_setting & 0x0100) >> 8;
                if (slider2On != btnSlider2On.selected && ![self sendCommandExists:CMD_CH18_USB_AUDIO_SETTING dspId:DSP_5]) {
                    btnSlider2On.selected = slider2On;
                }
                BOOL slider1Solo = (pkt->ch_17_audio_setting & 0x0400) >> 10;
                if (slider1Solo != btnSlider1Solo.selected && ![self sendCommandExists:CMD_CH17_USB_AUDIO_SETTING dspId:DSP_5]) {
                    btnSlider1Solo.selected = slider1Solo;
                }
                BOOL slider2Solo = (pkt->ch_18_audio_setting & 0x0400) >> 10;
                if (slider2Solo != btnSlider2Solo.selected && ![self sendCommandExists:CMD_CH18_USB_AUDIO_SETTING dspId:DSP_5]) {
                    btnSlider2Solo.selected = slider2Solo;
                }
                BOOL slider1SoloSafe = (pkt->ch_17_audio_setting & 0x0800) >> 11;
                if (slider1SoloSafe) {
                    [btnSlider1Solo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
                    [btnSlider1Solo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
                } else {
                    [btnSlider1Solo setTitle:@"SOLO" forState:UIControlStateNormal];
                    [btnSlider1Solo setTitle:@"SOLO" forState:UIControlStateSelected];
                }
                BOOL slider2SoloSafe = (pkt->ch_18_audio_setting & 0x0800) >> 11;
                if (slider2SoloSafe) {
                    [btnSlider2Solo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
                    [btnSlider2Solo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
                } else {
                    [btnSlider2Solo setTitle:@"SOLO" forState:UIControlStateNormal];
                    [btnSlider2Solo setTitle:@"SOLO" forState:UIControlStateSelected];
                }
                BOOL slider1Meter = (pkt->ch_17_audio_setting & 0x8000) >> 15;
                if (slider1Meter != btnSlider1Meter.selected && ![self sendCommandExists:CMD_CH17_USB_AUDIO_SETTING dspId:DSP_5]) {
                    btnSlider1Meter.selected = slider1Meter;
                }
                BOOL slider2Meter = (pkt->ch_18_audio_setting & 0x8000) >> 15;
                if (slider2Meter != btnSlider2Meter.selected && ![self sendCommandExists:CMD_CH18_USB_AUDIO_SETTING dspId:DSP_5]) {
                    btnSlider2Meter.selected = slider2Meter;
                }
                if (currentMode == MODE_PAN) {
                    sliderPan1.value = pkt->ch_17_pan + PAN_CENTER_VALUE;
                    sliderPan2.value = pkt->ch_18_pan + PAN_CENTER_VALUE;
                    [self onSliderPan1Action:nil];
                    [self onSliderPan2Action:nil];
                }
                orderChannel[CH_17] = (pkt->ch_17_ctrl & 0xE000) >> 13;
                orderChannel[CH_18] = (pkt->ch_18_ctrl & 0xE000) >> 13;
                orderMain = (pkt->main_l_r_ctrl & 0xE000) >> 13;
                [self updateOrderButtons];
                if (!_sliderMainLock) {
                    sliderMain.value = pkt->main_fader;
                    [lblSliderMainValue setText:[Global getSliderGain:sliderMain.value appendDb:YES]];
                }
                BOOL sliderMainStereoMono = (pkt->main_meter_pre_post & 0x80) >> 7;
                if (sliderMainStereoMono != btnSliderMainMono.selected && ![self sendCommandExists:CMD_MAIN_METER dspId:DSP_6]) {
                    btnSliderMainMono.selected = sliderMainStereoMono;
                }
                BOOL sliderMainOnOff = pkt->main_l_r_ctrl & 0x01;
                if (sliderMainOnOff != btnSliderMainOn.selected && ![self sendCommandExists:CMD_MAIN_LR_CTRL dspId:DSP_6]) {
                    btnSliderMainOn.selected = sliderMainOnOff;
                }
                BOOL sliderMainMeter = pkt->main_meter_pre_post & 0x01;
                if (sliderMainMeter != btnSliderMainMeter.selected && ![self sendCommandExists:CMD_MAIN_METER dspId:DSP_6]) {
                    btnSliderMainMeter.selected = sliderMainMeter;
                }
                // Inv
                invOffOn[CH_17] = (pkt->ch_17_ctrl & 0x1000) >> 12;
                invOffOn[CH_18] = (pkt->ch_18_ctrl & 0x1000) >> 12;
                // Delay
                btnDelayCh17OnOff.selected = (pkt->ch_17_ctrl & 0x04) >> 2;
                btnDelayCh18OnOff.selected = (pkt->ch_18_ctrl & 0x04) >> 2;
                btnDelayMainOnOff.selected = (pkt->main_l_r_ctrl & 0x04) >> 2;
                knobDelayCh17.value = [Global delayToKnobValue:pkt->ch_17_delay_time];
                [self knobDelayCh17DidChange:nil];
                knobDelayCh18.value = [Global delayToKnobValue:pkt->ch_18_delay_time];
                [self knobDelayCh18DidChange:nil];
                knobDelayMain.value = [Global delayToKnobValue:pkt->main_l_r_delay_time];
                [self knobDelayMainDidChange:nil];
                // DYN
                if ((pkt->ch_17_ctrl & 0x02) >> 1) {
                    [btnDynG1 setSelected:((pkt->ch_17_ctrl & 0x0100) >> 8)];
                    [btnDynE1 setSelected:((pkt->ch_17_ctrl & 0x0200) >> 9)];
                    [btnDynC1 setSelected:((pkt->ch_17_ctrl & 0x0400) >> 10)];
                    [btnDynL1 setSelected:((pkt->ch_17_ctrl & 0x0800) >> 11)];
                } else {
                    [btnDynG1 setSelected:NO];
                    [btnDynE1 setSelected:NO];
                    [btnDynC1 setSelected:NO];
                    [btnDynL1 setSelected:NO];
                }
                if ((pkt->ch_18_ctrl & 0x02) >> 1) {
                    [btnDynG2 setSelected:((pkt->ch_18_ctrl & 0x0100) >> 8)];
                    [btnDynE2 setSelected:((pkt->ch_18_ctrl & 0x0200) >> 9)];
                    [btnDynC2 setSelected:((pkt->ch_18_ctrl & 0x0400) >> 10)];
                    [btnDynL2 setSelected:((pkt->ch_18_ctrl & 0x0800) >> 11)];
                } else {
                    [btnDynG2 setSelected:NO];
                    [btnDynE2 setSelected:NO];
                    [btnDynC2 setSelected:NO];
                    [btnDynL2 setSelected:NO];
                }
                if ((pkt->main_l_r_ctrl & 0x02) >> 1) {
                    [btnDynGMain setSelected:((pkt->main_l_r_ctrl & 0x0100) >> 8)];
                    [btnDynEMain setSelected:((pkt->main_l_r_ctrl & 0x0200) >> 9)];
                    [btnDynCMain setSelected:((pkt->main_l_r_ctrl & 0x0400) >> 10)];
                    [btnDynLMain setSelected:((pkt->main_l_r_ctrl & 0x0800) >> 11)];
                } else {
                    [btnDynGMain setSelected:NO];
                    [btnDynEMain setSelected:NO];
                    [btnDynCMain setSelected:NO];
                    [btnDynLMain setSelected:NO];
                }
                [self refreshModeButtons];
                
                // Update EQ preview
                if (geqPeqController) {
                    [geqPeqController processChannel17_18PageReply:pkt];
                }
                
                // Update DYN preview
                if (dynController) {
                    [dynController processChannel17_18PageReply:pkt];
                }
            } else if (pageType == CHANNEL_PAGE_AUX_1_4) {
                ChannelAux1_4PagePacket *pkt = (ChannelAux1_4PagePacket *)buffer;
                
                // Check CRC
                if (![Global checkCrc:buffer
                                  len:sizeof(ChannelAux1_4PagePacket)-FRAME_PROTOCOL_LEN pktCrc:pkt->crc]) {
                    NSLog(@"CHANNEL_PAGE_PARAMETER (Aux) invalid crc!");
                    return;
                }
                
                _auxGpSoloCtrl = pkt->solo_ctrl;
                _auxGpOffOn = pkt->aux_gp_master_on_off;
                _auxGpMeterPrePost = pkt->aux_gp_meter_pre_post;
                _mainCtrl = pkt->main_l_r_ctrl;
                _mainMeterPrePost = pkt->main_meter_pre_post;
                
                if (!_slider1Lock) {
                    slider1.value = pkt->aux_1_fader;
                    [lblSlider1Value setText:[Global getSliderGain:slider1.value appendDb:YES]];
                }
                if (!_slider2Lock) {
                    slider2.value = pkt->aux_2_fader;
                    [lblSlider2Value setText:[Global getSliderGain:slider2.value appendDb:YES]];
                }
                if (!_slider3Lock) {
                    slider3.value = pkt->aux_3_fader;
                    [lblSlider3Value setText:[Global getSliderGain:slider3.value appendDb:YES]];
                }
                if (!_slider4Lock) {
                    slider4.value = pkt->aux_4_fader;
                    [lblSlider4Value setText:[Global getSliderGain:slider4.value appendDb:YES]];
                }
                BOOL slider1Solo = (pkt->solo_ctrl & 0x0100) >> 8;
                if (slider1Solo != btnSlider1Solo.selected && ![self sendCommandExists:CMD_AUXGP_SOLO_CTRL dspId:DSP_5]) {
                    btnSlider1Solo.selected = slider1Solo;
                }
                BOOL slider2Solo = (pkt->solo_ctrl & 0x0200) >> 9;
                if (slider2Solo != btnSlider2Solo.selected && ![self sendCommandExists:CMD_AUXGP_SOLO_CTRL dspId:DSP_5]) {
                    btnSlider2Solo.selected = slider2Solo;
                }
                BOOL slider3Solo = (pkt->solo_ctrl & 0x0400) >> 10;
                if (slider3Solo != btnSlider3Solo.selected && ![self sendCommandExists:CMD_AUXGP_SOLO_CTRL dspId:DSP_5]) {
                    btnSlider3Solo.selected = slider3Solo;
                }
                BOOL slider4Solo = (pkt->solo_ctrl & 0x0800) >> 11;
                if (slider4Solo != btnSlider4Solo.selected && ![self sendCommandExists:CMD_AUXGP_SOLO_CTRL dspId:DSP_5]) {
                    btnSlider4Solo.selected = slider4Solo;
                }
                BOOL slider1SoloSafe = (pkt->solo_safe_ctrl & 0x0100) >> 8;
                if (slider1SoloSafe) {
                    [btnSlider1Solo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
                    [btnSlider1Solo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
                } else {
                    [btnSlider1Solo setTitle:@"SOLO" forState:UIControlStateNormal];
                    [btnSlider1Solo setTitle:@"SOLO" forState:UIControlStateSelected];
                }
                BOOL slider2SoloSafe = (pkt->solo_safe_ctrl & 0x0200) >> 9;
                if (slider2SoloSafe) {
                    [btnSlider2Solo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
                    [btnSlider2Solo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
                } else {
                    [btnSlider2Solo setTitle:@"SOLO" forState:UIControlStateNormal];
                    [btnSlider2Solo setTitle:@"SOLO" forState:UIControlStateSelected];
                }
                BOOL slider3SoloSafe = (pkt->solo_safe_ctrl & 0x0400) >> 10;
                if (slider3SoloSafe) {
                    [btnSlider3Solo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
                    [btnSlider3Solo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
                } else {
                    [btnSlider3Solo setTitle:@"SOLO" forState:UIControlStateNormal];
                    [btnSlider3Solo setTitle:@"SOLO" forState:UIControlStateSelected];
                }
                BOOL slider4SoloSafe = (pkt->solo_safe_ctrl & 0x0800) >> 11;
                if (slider4SoloSafe) {
                    [btnSlider4Solo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
                    [btnSlider4Solo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
                } else {
                    [btnSlider4Solo setTitle:@"SOLO" forState:UIControlStateNormal];
                    [btnSlider4Solo setTitle:@"SOLO" forState:UIControlStateSelected];
                }
                BOOL slider1On = (pkt->aux_gp_master_on_off & 0x0100) >> 8;
                if (slider1On != btnSlider1On.selected && ![self sendCommandExists:CMD_AUXGP_ON_OFF dspId:DSP_5]) {
                    btnSlider1On.selected = slider1On;
                }
                BOOL slider2On = (pkt->aux_gp_master_on_off & 0x0200) >> 9;
                if (slider2On != btnSlider2On.selected && ![self sendCommandExists:CMD_AUXGP_ON_OFF dspId:DSP_5]) {
                    btnSlider2On.selected = slider2On;
                }
                BOOL slider3On = (pkt->aux_gp_master_on_off & 0x0400) >> 10;
                if (slider3On != btnSlider3On.selected && ![self sendCommandExists:CMD_AUXGP_ON_OFF dspId:DSP_5]) {
                    btnSlider3On.selected = slider3On;
                }
                BOOL slider4On = (pkt->aux_gp_master_on_off & 0x0800) >> 11;
                if (slider4On != btnSlider4On.selected && ![self sendCommandExists:CMD_AUXGP_ON_OFF dspId:DSP_5]) {
                    btnSlider4On.selected = slider4On;
                }
                BOOL slider1Meter = (pkt->aux_gp_meter_pre_post & 0x0100) >> 8;
                if (slider1Meter != btnSlider1Meter.selected && ![self sendCommandExists:CMD_AUXGP_METER dspId:DSP_5]) {
                    btnSlider1Meter.selected = slider1Meter;
                }
                BOOL slider2Meter = (pkt->aux_gp_meter_pre_post & 0x0200) >> 9;
                if (slider2Meter != btnSlider2Meter.selected && ![self sendCommandExists:CMD_AUXGP_METER dspId:DSP_5]) {
                    btnSlider2Meter.selected = slider2Meter;
                }
                BOOL slider3Meter = (pkt->aux_gp_meter_pre_post & 0x0400) >> 10;
                if (slider3Meter != btnSlider3Meter.selected && ![self sendCommandExists:CMD_AUXGP_METER dspId:DSP_5]) {
                    btnSlider3Meter.selected = slider3Meter;
                }
                BOOL slider4Meter = (pkt->aux_gp_meter_pre_post & 0x0800) >> 11;
                if (slider4Meter != btnSlider4Meter.selected && ![self sendCommandExists:CMD_AUXGP_METER dspId:DSP_5]) {
                    btnSlider4Meter.selected = slider4Meter;
                }
                orderMain = (pkt->main_l_r_ctrl & 0xE000) >> 13;
                [self updateOrderButtons];
                if (!_sliderMainLock) {
                    sliderMain.value = pkt->main_fader;
                    [lblSliderMainValue setText:[Global getSliderGain:sliderMain.value appendDb:YES]];
                }
                BOOL sliderMainStereoMono = (pkt->main_meter_pre_post & 0x80) >> 7;
                if (sliderMainStereoMono != btnSliderMainMono.selected && ![self sendCommandExists:CMD_MAIN_METER dspId:DSP_6]) {
                    btnSliderMainMono.selected = sliderMainStereoMono;
                }
                BOOL sliderMainOnOff = pkt->main_l_r_ctrl & 0x01;
                if (sliderMainOnOff != btnSliderMainOn.selected && ![self sendCommandExists:CMD_MAIN_LR_CTRL dspId:DSP_6]) {
                    btnSliderMainOn.selected = sliderMainOnOff;
                }
                BOOL sliderMainMeter = pkt->main_meter_pre_post & 0x01;
                if (sliderMainMeter != btnSliderMainMeter.selected && ![self sendCommandExists:CMD_MAIN_METER dspId:DSP_6]) {
                    btnSliderMainMeter.selected = sliderMainMeter;
                }
            } else if (pageType == CHANNEL_PAGE_GP_1_4) {
                ChannelGp1_4PagePacket *pkt = (ChannelGp1_4PagePacket *)buffer;
                
                // Check CRC
                if (![Global checkCrc:buffer
                                  len:sizeof(ChannelGp1_4PagePacket)-FRAME_PROTOCOL_LEN pktCrc:pkt->crc]) {
                    NSLog(@"CHANNEL_PAGE_PARAMETER (Gp) invalid crc!");
                    return;
                }
                
                _auxGpSoloCtrl = pkt->solo_ctrl;
                _auxGpOffOn = pkt->aux_gp_master_on_off;
                _auxGpMeterPrePost = pkt->aux_gp_meter_pre_post;
                _mainCtrl = pkt->main_l_r_ctrl;
                _mainMeterPrePost = pkt->main_meter_pre_post;

                if (!_slider1Lock) {
                    slider1.value = pkt->gp_1_fader;
                    [lblSlider1Value setText:[Global getSliderGain:slider1.value appendDb:YES]];
                }
                if (!_slider2Lock) {
                    slider2.value = pkt->gp_2_fader;
                    [lblSlider2Value setText:[Global getSliderGain:slider2.value appendDb:YES]];
                }
                if (!_slider3Lock) {
                    slider3.value = pkt->gp_3_fader;
                    [lblSlider3Value setText:[Global getSliderGain:slider3.value appendDb:YES]];
                }
                if (!_slider4Lock) {
                    slider4.value = pkt->gp_4_fader;
                    [lblSlider4Value setText:[Global getSliderGain:slider4.value appendDb:YES]];
                }
                BOOL slider1Solo = pkt->solo_ctrl & 0x01;
                if (slider1Solo != btnSlider1Solo.selected && ![self sendCommandExists:CMD_AUXGP_SOLO_CTRL dspId:DSP_5]) {
                    btnSlider1Solo.selected = slider1Solo;
                }
                BOOL slider2Solo = (pkt->solo_ctrl & 0x02) >> 1;
                if (slider2Solo != btnSlider2Solo.selected && ![self sendCommandExists:CMD_AUXGP_SOLO_CTRL dspId:DSP_5]) {
                    btnSlider2Solo.selected = slider2Solo;
                }
                BOOL slider3Solo = (pkt->solo_ctrl & 0x04) >> 2;
                if (slider3Solo != btnSlider3Solo.selected && ![self sendCommandExists:CMD_AUXGP_SOLO_CTRL dspId:DSP_5]) {
                    btnSlider3Solo.selected = slider3Solo;
                }
                BOOL slider4Solo = (pkt->solo_ctrl & 0x08) >> 3;
                if (slider4Solo != btnSlider4Solo.selected && ![self sendCommandExists:CMD_AUXGP_SOLO_CTRL dspId:DSP_5]) {
                    btnSlider4Solo.selected = slider4Solo;
                }
                BOOL slider1SoloSafe = pkt->solo_safe_ctrl & 0x01;
                if (slider1SoloSafe) {
                    [btnSlider1Solo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
                    [btnSlider1Solo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
                } else {
                    [btnSlider1Solo setTitle:@"SOLO" forState:UIControlStateNormal];
                    [btnSlider1Solo setTitle:@"SOLO" forState:UIControlStateSelected];
                }
                BOOL slider2SoloSafe = (pkt->solo_safe_ctrl & 0x02) >> 1;
                if (slider2SoloSafe) {
                    [btnSlider2Solo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
                    [btnSlider2Solo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
                } else {
                    [btnSlider2Solo setTitle:@"SOLO" forState:UIControlStateNormal];
                    [btnSlider2Solo setTitle:@"SOLO" forState:UIControlStateSelected];
                }
                BOOL slider3SoloSafe = (pkt->solo_safe_ctrl & 0x04) >> 2;
                if (slider3SoloSafe) {
                    [btnSlider3Solo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
                    [btnSlider3Solo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
                } else {
                    [btnSlider3Solo setTitle:@"SOLO" forState:UIControlStateNormal];
                    [btnSlider3Solo setTitle:@"SOLO" forState:UIControlStateSelected];
                }
                BOOL slider4SoloSafe = (pkt->solo_safe_ctrl & 0x08) >> 3;
                if (slider4SoloSafe) {
                    [btnSlider4Solo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
                    [btnSlider4Solo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
                } else {
                    [btnSlider4Solo setTitle:@"SOLO" forState:UIControlStateNormal];
                    [btnSlider4Solo setTitle:@"SOLO" forState:UIControlStateSelected];
                }
                BOOL slider1On = pkt->aux_gp_master_on_off & 0x01;
                if (slider1On != btnSlider1On.selected && ![self sendCommandExists:CMD_AUXGP_ON_OFF dspId:DSP_5]) {
                    btnSlider1On.selected = slider1On;
                }
                BOOL slider2On = (pkt->aux_gp_master_on_off & 0x02) >> 1;
                if (slider2On != btnSlider2On.selected && ![self sendCommandExists:CMD_AUXGP_ON_OFF dspId:DSP_5]) {
                    btnSlider2On.selected = slider2On;
                }
                BOOL slider3On = (pkt->aux_gp_master_on_off & 0x04) >> 2;
                if (slider3On != btnSlider3On.selected && ![self sendCommandExists:CMD_AUXGP_ON_OFF dspId:DSP_5]) {
                    btnSlider3On.selected = slider3On;
                }
                BOOL slider4On = (pkt->aux_gp_master_on_off & 0x08) >> 3;
                if (slider4On != btnSlider4On.selected && ![self sendCommandExists:CMD_AUXGP_ON_OFF dspId:DSP_5]) {
                    btnSlider4On.selected = slider4On;
                }
                BOOL slider1Meter = pkt->aux_gp_meter_pre_post & 0x01;
                if (slider1Meter != btnSlider1Meter.selected && ![self sendCommandExists:CMD_AUXGP_METER dspId:DSP_5]) {
                    btnSlider1Meter.selected = slider1Meter;
                }
                BOOL slider2Meter = (pkt->aux_gp_meter_pre_post & 0x02) >> 1;
                if (slider2Meter != btnSlider2Meter.selected && ![self sendCommandExists:CMD_AUXGP_METER dspId:DSP_5]) {
                    btnSlider2Meter.selected = slider2Meter;
                }
                BOOL slider3Meter = (pkt->aux_gp_meter_pre_post & 0x04) >> 2;
                if (slider3Meter != btnSlider3Meter.selected && ![self sendCommandExists:CMD_AUXGP_METER dspId:DSP_5]) {
                    btnSlider3Meter.selected = slider3Meter;
                }
                BOOL slider4Meter = (pkt->aux_gp_meter_pre_post & 0x08) >> 3;
                if (slider4Meter != btnSlider4Meter.selected && ![self sendCommandExists:CMD_AUXGP_METER dspId:DSP_5]) {
                    btnSlider4Meter.selected = slider4Meter;
                }
                if (currentMode == MODE_PAN) {
                    sliderPan1.value = pkt->gp_1_pan + PAN_CENTER_VALUE;
                    sliderPan2.value = pkt->gp_2_pan + PAN_CENTER_VALUE;
                    sliderPan3.value = pkt->gp_3_pan + PAN_CENTER_VALUE;
                    sliderPan4.value = pkt->gp_4_pan + PAN_CENTER_VALUE;
                    [self onSliderPan1Action:nil];
                    [self onSliderPan2Action:nil];
                    [self onSliderPan3Action:nil];
                    [self onSliderPan4Action:nil];
                }
                orderMain = (pkt->main_l_r_ctrl & 0xE000) >> 13;
                [self updateOrderButtons];
                if (!_sliderMainLock) {
                    sliderMain.value = pkt->main_fader;
                    [lblSliderMainValue setText:[Global getSliderGain:sliderMain.value appendDb:YES]];
                }
                BOOL sliderMainStereoMono = (pkt->main_meter_pre_post & 0x80) >> 7;
                if (sliderMainStereoMono != btnSliderMainMono.selected && ![self sendCommandExists:CMD_MAIN_METER dspId:DSP_6]) {
                    btnSliderMainMono.selected = sliderMainStereoMono;
                }
                BOOL sliderMainOnOff = pkt->main_l_r_ctrl & 0x01;
                if (sliderMainOnOff != btnSliderMainOn.selected && ![self sendCommandExists:CMD_MAIN_LR_CTRL dspId:DSP_6]) {
                    btnSliderMainOn.selected = sliderMainOnOff;
                }
                BOOL sliderMainMeter = pkt->main_meter_pre_post & 0x01;
                if (sliderMainMeter != btnSliderMainMeter.selected && ![self sendCommandExists:CMD_MAIN_METER dspId:DSP_6]) {
                    btnSliderMainMeter.selected = sliderMainMeter;
                }
            } else if (pageType == CHANNEL_PAGE_MULTI_1_4) {
                ChannelMulti1_4PagePacket *pkt = (ChannelMulti1_4PagePacket *)buffer;
                
                // Check CRC
                if (![Global checkCrc:buffer
                                  len:sizeof(ChannelMulti1_4PagePacket)-FRAME_PROTOCOL_LEN pktCrc:pkt->crc]) {
                    NSLog(@"CHANNEL_PAGE_PARAMETER (Multi1-4) invalid crc!");
                    return;
                }
                
                _multi1Ctrl = pkt->multi_1_ctrl;
                _multi2Ctrl = pkt->multi_2_ctrl;
                _multi3Ctrl = pkt->multi_3_ctrl;
                _multi4Ctrl = pkt->multi_4_ctrl;
                _multi14MeterPrePost = pkt->multi_1_4_meter_pre_post;
                _mainCtrl = pkt->main_l_r_ctrl;
                _mainMeterPrePost = pkt->main_meter_pre_post;

                if (!_slider1Lock) {
                    slider1.value = pkt->multi_1_fader;
                    [lblSlider1Value setText:[Global getSliderGain:slider1.value appendDb:YES]];
                }
                if (!_slider2Lock) {
                    slider2.value = pkt->multi_2_fader;
                    [lblSlider2Value setText:[Global getSliderGain:slider2.value appendDb:YES]];
                }
                if (!_slider3Lock) {
                    slider3.value = pkt->multi_3_fader;
                    [lblSlider3Value setText:[Global getSliderGain:slider3.value appendDb:YES]];
                }
                if (!_slider4Lock) {
                    slider4.value = pkt->multi_4_fader;
                    [lblSlider4Value setText:[Global getSliderGain:slider4.value appendDb:YES]];
                }
                BOOL slider1On = pkt->multi_1_ctrl & 0x01;
                if (slider1On != btnSlider1On.selected && ![self sendCommandExists:CMD_MULTI_1_CTRL dspId:DSP_7]) {
                    btnSlider1On.selected = slider1On;
                }
                BOOL slider2On = pkt->multi_2_ctrl & 0x01;
                if (slider2On != btnSlider2On.selected && ![self sendCommandExists:CMD_MULTI_2_CTRL dspId:DSP_7]) {
                    btnSlider2On.selected = slider2On;
                }
                BOOL slider3On = pkt->multi_3_ctrl & 0x01;
                if (slider3On != btnSlider3On.selected && ![self sendCommandExists:CMD_MULTI_3_CTRL dspId:DSP_7]) {
                    btnSlider3On.selected = slider3On;
                }
                BOOL slider4On = pkt->multi_4_ctrl & 0x01;
                if (slider4On != btnSlider4On.selected && ![self sendCommandExists:CMD_MULTI_4_CTRL dspId:DSP_7]) {
                    btnSlider4On.selected = slider4On;
                }
                BOOL slider1Meter = pkt->multi_1_4_meter_pre_post & 0x01;
                if (slider1Meter != btnSlider1Meter.selected && ![self sendCommandExists:CMD_MULTI_METER dspId:DSP_7]) {
                    btnSlider1Meter.selected = slider1Meter;
                }
                BOOL slider2Meter = (pkt->multi_1_4_meter_pre_post & 0x02) >> 1;
                if (slider2Meter != btnSlider2Meter.selected && ![self sendCommandExists:CMD_MULTI_METER dspId:DSP_7]) {
                    btnSlider2Meter.selected = slider2Meter;
                }
                BOOL slider3Meter = (pkt->multi_1_4_meter_pre_post & 0x04) >> 2;
                if (slider3Meter != btnSlider3Meter.selected && ![self sendCommandExists:CMD_MULTI_METER dspId:DSP_7]) {
                    btnSlider3Meter.selected = slider3Meter;
                }
                BOOL slider4Meter = (pkt->multi_1_4_meter_pre_post & 0x08) >> 3;
                if (slider4Meter != btnSlider4Meter.selected && ![self sendCommandExists:CMD_MULTI_METER dspId:DSP_7]) {
                    btnSlider4Meter.selected = slider4Meter;
                }
                orderMulti[0] = (pkt->multi_1_ctrl & 0xE000) >> 13;
                orderMulti[1] = (pkt->multi_2_ctrl & 0xE000) >> 13;
                orderMulti[2] = (pkt->multi_3_ctrl & 0xE000) >> 13;
                orderMulti[3] = (pkt->multi_4_ctrl & 0xE000) >> 13;
                orderMain = (pkt->main_l_r_ctrl & 0xE000) >> 13;
                [self updateOrderButtons];
                if (!_sliderMainLock) {
                    sliderMain.value = pkt->main_fader;
                    [lblSliderMainValue setText:[Global getSliderGain:sliderMain.value appendDb:YES]];
                }
                BOOL sliderMainStereoMono = (pkt->main_meter_pre_post & 0x80) >> 7;
                if (sliderMainStereoMono != btnSliderMainMono.selected && ![self sendCommandExists:CMD_MAIN_METER dspId:DSP_6]) {
                    btnSliderMainMono.selected = sliderMainStereoMono;
                }
                BOOL sliderMainOnOff = pkt->main_l_r_ctrl & 0x01;
                if (sliderMainOnOff != btnSliderMainOn.selected && ![self sendCommandExists:CMD_MAIN_LR_CTRL dspId:DSP_6]) {
                    btnSliderMainOn.selected = sliderMainOnOff;
                }
                BOOL sliderMainMeter = pkt->main_meter_pre_post & 0x01;
                if (sliderMainMeter != btnSliderMainMeter.selected && ![self sendCommandExists:CMD_MAIN_METER dspId:DSP_6]) {
                    btnSliderMainMeter.selected = sliderMainMeter;
                }
                // Delay
                btnDelayMt1OnOff.selected = (pkt->multi_1_ctrl & 0x04) >> 2;
                btnDelayMt2OnOff.selected = (pkt->multi_2_ctrl & 0x04) >> 2;
                btnDelayMt3OnOff.selected = (pkt->multi_3_ctrl & 0x04) >> 2;
                btnDelayMt4OnOff.selected = (pkt->multi_4_ctrl & 0x04) >> 2;
                btnDelayMainOnOff.selected = (pkt->main_l_r_ctrl & 0x04) >> 2;
                knobDelayMt1.value = [Global delayToKnobValue:pkt->multi_1_delay_time];
                [self knobDelayMt1DidChange:nil];
                knobDelayMt2.value = [Global delayToKnobValue:pkt->multi_2_delay_time];
                [self knobDelayMt2DidChange:nil];
                knobDelayMt3.value = [Global delayToKnobValue:pkt->multi_3_delay_time];
                [self knobDelayMt3DidChange:nil];
                knobDelayMt4.value = [Global delayToKnobValue:pkt->multi_4_delay_time];
                [self knobDelayMt4DidChange:nil];
                knobDelayMain.value = [Global delayToKnobValue:pkt->main_l_r_delay_time];
                [self knobDelayMainDidChange:nil];
                // DYN
                if ((pkt->multi_1_ctrl & 0x02) >> 1) {
                    [btnDynG1 setSelected:((pkt->multi_1_ctrl & 0x0100) >> 8)];
                    [btnDynE1 setSelected:((pkt->multi_1_ctrl & 0x0200) >> 9)];
                    [btnDynC1 setSelected:((pkt->multi_1_ctrl & 0x0400) >> 10)];
                    [btnDynL1 setSelected:((pkt->multi_1_ctrl & 0x0800) >> 11)];
                } else {
                    [btnDynG1 setSelected:NO];
                    [btnDynE1 setSelected:NO];
                    [btnDynC1 setSelected:NO];
                    [btnDynL1 setSelected:NO];
                }
                if ((pkt->multi_2_ctrl & 0x02) >> 1) {
                    [btnDynG2 setSelected:((pkt->multi_2_ctrl & 0x0100) >> 8)];
                    [btnDynE2 setSelected:((pkt->multi_2_ctrl & 0x0200) >> 9)];
                    [btnDynC2 setSelected:((pkt->multi_2_ctrl & 0x0400) >> 10)];
                    [btnDynL2 setSelected:((pkt->multi_2_ctrl & 0x0800) >> 11)];
                } else {
                    [btnDynG2 setSelected:NO];
                    [btnDynE2 setSelected:NO];
                    [btnDynC2 setSelected:NO];
                    [btnDynL2 setSelected:NO];
                }
                if ((pkt->multi_3_ctrl & 0x02) >> 1) {
                    [btnDynG3 setSelected:((pkt->multi_3_ctrl & 0x0100) >> 8)];
                    [btnDynE3 setSelected:((pkt->multi_3_ctrl & 0x0200) >> 9)];
                    [btnDynC3 setSelected:((pkt->multi_3_ctrl & 0x0400) >> 10)];
                    [btnDynL3 setSelected:((pkt->multi_3_ctrl & 0x0800) >> 11)];
                } else {
                    [btnDynG3 setSelected:NO];
                    [btnDynE3 setSelected:NO];
                    [btnDynC3 setSelected:NO];
                    [btnDynL3 setSelected:NO];
                }
                if ((pkt->multi_4_ctrl & 0x02) >> 1) {
                    [btnDynG4 setSelected:((pkt->multi_4_ctrl & 0x0100) >> 8)];
                    [btnDynE4 setSelected:((pkt->multi_4_ctrl & 0x0200) >> 9)];
                    [btnDynC4 setSelected:((pkt->multi_4_ctrl & 0x0400) >> 10)];
                    [btnDynL4 setSelected:((pkt->multi_4_ctrl & 0x0800) >> 11)];
                } else {
                    [btnDynG4 setSelected:NO];
                    [btnDynE4 setSelected:NO];
                    [btnDynC4 setSelected:NO];
                    [btnDynL4 setSelected:NO];
                }
                if ((pkt->main_l_r_ctrl & 0x02) >> 1) {
                    [btnDynGMain setSelected:((pkt->main_l_r_ctrl & 0x0100) >> 8)];
                    [btnDynEMain setSelected:((pkt->main_l_r_ctrl & 0x0200) >> 9)];
                    [btnDynCMain setSelected:((pkt->main_l_r_ctrl & 0x0400) >> 10)];
                    [btnDynLMain setSelected:((pkt->main_l_r_ctrl & 0x0800) >> 11)];
                } else {
                    [btnDynGMain setSelected:NO];
                    [btnDynEMain setSelected:NO];
                    [btnDynCMain setSelected:NO];
                    [btnDynLMain setSelected:NO];
                }
                [self refreshModeButtons];
                
                // Update EQ preview
                if (geqPeqController) {
                    [geqPeqController processMulti1_4PageReply:pkt];
                }
                
                // Update DYN preview
                if (dynController) {
                    [dynController processMulti1_4PageReply:pkt];
                }
            } else if (pageType == CHANNEL_PAGE_CTRL) {
                ChannelCtrlPagePacket *pkt = (ChannelCtrlPagePacket *)buffer;
                
                // Check CRC
                if (![Global checkCrc:buffer
                                  len:sizeof(ChannelCtrlPagePacket)-FRAME_PROTOCOL_LEN pktCrc:pkt->crc]) {
                    NSLog(@"CHANNEL_PAGE_PARAMETER (CtrlRm) invalid crc!");
                    return;
                }
                
                _digitalInOutCtrl = pkt->digital_in_out_ctrl;
                _mainCtrl = pkt->main_l_r_ctrl;
                _mainMeterPrePost = pkt->main_meter_pre_post;
                
                if (!_slider8Lock) {
                    slider8.value = pkt->control_room_trim_level;
                    [lblSlider8Value setText:[Global getSliderGain:slider8.value appendDb:YES]];
                }
                BOOL slider8Solo = (pkt->digital_in_out_ctrl & 0x04) >> 2;
                if (slider8Solo != btnSlider8Solo.selected && ![self sendCommandExists:CMD_DIGITAL_INOUT_CTRL dspId:DSP_6]) {
                    btnSlider8Solo.selected = slider8Solo;
                }
                orderMain = (pkt->main_l_r_ctrl & 0xE000) >> 13;
                [self updateOrderButtons];
                if (!_sliderMainLock) {
                    sliderMain.value = pkt->main_fader;
                    [lblSliderMainValue setText:[Global getSliderGain:sliderMain.value appendDb:YES]];
                }
                BOOL sliderMainStereoMono = (pkt->main_meter_pre_post & 0x80) >> 7;
                if (sliderMainStereoMono != btnSliderMainMono.selected && ![self sendCommandExists:CMD_MAIN_METER dspId:DSP_6]) {
                    btnSliderMainMono.selected = sliderMainStereoMono;
                }
                BOOL sliderMainOnOff = pkt->main_l_r_ctrl & 0x01;
                if (sliderMainOnOff != btnSliderMainOn.selected && ![self sendCommandExists:CMD_MAIN_LR_CTRL dspId:DSP_6]) {
                    btnSliderMainOn.selected = sliderMainOnOff;
                }
                BOOL sliderMainMeter = pkt->main_meter_pre_post & 0x01;
                if (sliderMainMeter != btnSliderMainMeter.selected && ![self sendCommandExists:CMD_MAIN_METER dspId:DSP_6]) {
                    btnSliderMainMeter.selected = sliderMainMeter;
                }
                // Delay
                btnDelayMainOnOff.selected = (pkt->main_l_r_ctrl & 0x04) >> 2;
                knobDelayMain.value = [Global delayToKnobValue:pkt->main_l_r_delay_time];
                [self knobDelayMainDidChange:nil];
                // DYN
                if ((pkt->main_l_r_ctrl & 0x02) >> 1) {
                    [btnDynGMain setSelected:((pkt->main_l_r_ctrl & 0x0100) >> 8)];
                    [btnDynEMain setSelected:((pkt->main_l_r_ctrl & 0x0200) >> 9)];
                    [btnDynCMain setSelected:((pkt->main_l_r_ctrl & 0x0400) >> 10)];
                    [btnDynLMain setSelected:((pkt->main_l_r_ctrl & 0x0800) >> 11)];
                } else {
                    [btnDynGMain setSelected:NO];
                    [btnDynEMain setSelected:NO];
                    [btnDynCMain setSelected:NO];
                    [btnDynLMain setSelected:NO];
                }
                [self refreshModeButtons];
            }
            return;
        }
        case EFFECT_PAGE_PARAMETER:
        {
            if (!effectController.view.hidden) {
                EffectPagePacket *pkt = (EffectPagePacket *)buffer;
                
                // Check CRC
                if (![Global checkCrc:buffer
                                  len:sizeof(EffectPagePacket)-FRAME_PROTOCOL_LEN pktCrc:pkt->crc]) {
                    NSLog(@"EFFECT_PAGE_PARAMETER invalid crc!");
                    return;
                }
                
                [effectController processReply:pkt];
                
                _effectCtrl = pkt->effect_control;
                
                // Update meter, fader and solo/on buttons
                if (currentPage == PAGE_EFX_1) {
                    if (!_sliderMainLock) {
                        sliderMain.value = pkt->efx_1_level;
                        [lblSliderMainValue setText:[Global getSliderGain:sliderMain.value appendDb:YES]];
                    }
                    BOOL sliderMainStereoMono = (pkt->gp_to_main & 0x0400) >> 10;
                    if (sliderMainStereoMono != btnSliderMainMono.selected && ![self sendCommandExists:CMD_GP_TO_MAIN_CTRL dspId:DSP_5]) {
                        btnSliderMainMono.selected = sliderMainStereoMono;
                    }
                    BOOL effectSoloSafe = (pkt->gp_to_main & 0x1000) >> 12;
                    if (effectSoloSafe) {
                        [btnSliderMainMono setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
                        [btnSliderMainMono setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
                    } else {
                        [btnSliderMainMono setTitle:@"SOLO" forState:UIControlStateNormal];
                        [btnSliderMainMono setTitle:@"SOLO" forState:UIControlStateSelected];
                    }
                    BOOL sliderMainOnOff = pkt->effect_control & 0x01;
                    if (sliderMainOnOff != btnSliderMainOn.selected && ![self sendCommandExists:CMD_EFFECT_CONTROL dspId:DSP_5]) {
                        btnSliderMainOn.selected = sliderMainOnOff;
                    }
                    if (effectController != nil) {
                        [effectController setEffectSolo:btnSliderMainMono.selected];
                        [effectController setEffectOnOff:btnSliderMainOn.selected];
                    }
                    meterMainL.progress = ((float)pkt->effect_1_1_out_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
                    meterMainR.progress = ((float)pkt->effect_1_2_out_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
                } else if (currentPage == PAGE_EFX_2) {
                    if (!_sliderMainLock) {
                        sliderMain.value = pkt->efx_2_level;
                        [lblSliderMainValue setText:[Global getSliderGain:sliderMain.value appendDb:YES]];
                    }
                    BOOL sliderMainStereoMono = (pkt->gp_to_main & 0x0800) >> 11;
                    if (sliderMainStereoMono != btnSliderMainMono.selected && ![self sendCommandExists:CMD_GP_TO_MAIN_CTRL dspId:DSP_5]) {
                        btnSliderMainMono.selected = sliderMainStereoMono;
                    }
                    BOOL effectSoloSafe = (pkt->gp_to_main & 0x2000) >> 13;
                    if (effectSoloSafe) {
                        [btnSliderMainMono setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
                        [btnSliderMainMono setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
                    } else {
                        [btnSliderMainMono setTitle:@"SOLO" forState:UIControlStateNormal];
                        [btnSliderMainMono setTitle:@"SOLO" forState:UIControlStateSelected];
                    }
                    BOOL sliderMainOnOff = (pkt->effect_control & 0x02) >> 1;
                    if (sliderMainOnOff != btnSliderMainOn.selected && ![self sendCommandExists:CMD_EFFECT_CONTROL dspId:DSP_5]) {
                        btnSliderMainOn.selected = sliderMainOnOff;
                    }
                    if (effectController != nil) {
                        [effectController setEffectSolo:btnSliderMainMono.selected];
                        [effectController setEffectOnOff:btnSliderMainOn.selected];
                    }
                    meterMainL.progress = ((float)pkt->effect_2_1_out_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
                    meterMainR.progress = ((float)pkt->effect_2_2_out_meter/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
                }
            }
            return;
        }
        case AUX_GP_PAGE_PARAMETER:
        {
            if (!auxSendController.view.hidden) {
                AuxPagePacket *auxPacket = (AuxPagePacket *)buffer;
                
                // Check CRC
                if (![Global checkCrc:buffer
                                  len:sizeof(AuxPagePacket)-FRAME_PROTOCOL_LEN pktCrc:auxPacket->crc]) {
                    NSLog(@"AUX_GP_PAGE_PARAMETER (Aux) invalid crc!");
                    return;
                }

                [auxSendController processReply:auxPacket];
            }
            if (!groupSendController.view.hidden) {
                GpPagePacket *gpPacket = (GpPagePacket *)buffer;
                
                // Check CRC
                if (![Global checkCrc:buffer
                                  len:sizeof(GpPagePacket)-FRAME_PROTOCOL_LEN pktCrc:gpPacket->crc]) {
                    NSLog(@"AUX_GP_PAGE_PARAMETER (Gp) invalid crc!");
                    return;
                }

                [groupSendController processReply:gpPacket];
            }
            return;
        }
        case EQ_PAGE_PARAMETER:
        {
            int pageType = buffer[8];
            if (pageType == 0) {
                EqChannelPagePacket *eqChannelPkt = (EqChannelPagePacket *)buffer;
                
                // Check CRC
                if (![Global checkCrc:buffer
                                  len:sizeof(EqChannelPagePacket)-FRAME_PROTOCOL_LEN pktCrc:eqChannelPkt->crc]) {
                    NSLog(@"EQ_PAGE_PARAMETER (Channel) invalid crc!");
                    return;
                }
                
                [geqPeqController processChannelReply:eqChannelPkt];
            } else if (pageType == 1) {
                EqMultiPagePacket *eqMultiPkt = (EqMultiPagePacket *)buffer;
                
                // Check CRC
                if (![Global checkCrc:buffer
                                  len:sizeof(EqMultiPagePacket)-FRAME_PROTOCOL_LEN pktCrc:eqMultiPkt->crc]) {
                    NSLog(@"EQ_PAGE_PARAMETER (Multi) invalid crc!");
                    return;
                }

                [geqPeqController processMultiReply:eqMultiPkt];
            } else if (pageType == 2) {
                EqMainPagePacket *eqMainPkt = (EqMainPagePacket *)buffer;
                
                // Check CRC
                if (![Global checkCrc:buffer
                                  len:sizeof(EqMainPagePacket)-FRAME_PROTOCOL_LEN pktCrc:eqMainPkt->crc]) {
                    NSLog(@"EQ_PAGE_PARAMETER (Main) invalid crc!");
                    return;
                }

                [geqPeqController processMainReply:eqMainPkt];
            }
            return;
        }
        case SETUP_CTRLRM_USB_ASSIGN_PAGE_PARAMETER:
        {
            SetupCtrlRmUsbAssignPagePacket *pkt = (SetupCtrlRmUsbAssignPagePacket *)buffer;
            
            // Check CRC
            if (![Global checkCrc:buffer
                              len:sizeof(SetupCtrlRmUsbAssignPagePacket)-FRAME_PROTOCOL_LEN pktCrc:pkt->crc]) {
                NSLog(@"SETUP_CTRLRM_USB_ASSIGN_PAGE_PARAMETER invalid crc!");
                return;
            }
            
            _sgSettingCtrl = pkt->sg_setting_ctrl;
            _sgAssign = pkt->sg_assign;
            _gpToMain = pkt->gp_to_main;
            _dspSetClkSrc = pkt->dsp_sets_clock_source;
            
            if (eqDynDelayController != nil && !eqDynDelayController.view.hidden) {
                [eqDynDelayController processCtrlRmPage:pkt];
            }
                
            int clockSource = pkt->dsp_sets_clock_source & 0x07;
            if (_clkSrcTracking == CLK_SRC_NONE) {
                if (clockSource == 0) {
                    [btnClkSrc441kHz setSelected:YES];
                    [btnClkSrc48kHz setSelected:NO];
                    [btnClkSrcUsbAudio setSelected:NO];
                    [lblUsbMessage setHidden:NO];
                } else if (clockSource == 1) {
                    [btnClkSrc441kHz setSelected:NO];
                    [btnClkSrc48kHz setSelected:YES];
                    [btnClkSrcUsbAudio setSelected:NO];
                    [lblUsbMessage setHidden:YES];
                } else if (clockSource == 4) {
                    [btnClkSrc441kHz setSelected:NO];
                    [btnClkSrc48kHz setSelected:NO];
                    [btnClkSrcUsbAudio setSelected:YES];
                    [lblUsbMessage setHidden:YES];
                    
                    float usbSamplingRate = pkt->usb_sampling_rate * 10.0;
                    [btnClkSrcUsbAudio.titleLabel setText:[NSString stringWithFormat:@"USB AUDIO\n%.1f kHz", usbSamplingRate/1000.0]];
                }
            }

            float dspSamplingRate = pkt->dsp_sampling_rate * 10.0;
            BOOL lockStatus = (pkt->dsp_status_3 & 0x04) >> 2;
            if (lockStatus == 0) {
                NSString *lockStr = [NSString stringWithFormat:@"%.1fkHz Lock", dspSamplingRate/1000.0];
                NSMutableAttributedString *attrLockStr = [[NSMutableAttributedString alloc] initWithString:lockStr];
                [attrLockStr addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(lockStr.length-4, 4)];
                [lblSampleRate setAttributedText:attrLockStr];
            } else {
                NSString *lockStr = [NSString stringWithFormat:@"%.1fkHz Un-Lock", dspSamplingRate/1000.0];
                NSMutableAttributedString *attrLockStr = [[NSMutableAttributedString alloc] initWithString:lockStr];
                [attrLockStr addAttribute:NSForegroundColorAttributeName value:[UIColor redColor] range:NSMakeRange(lockStr.length-7, 7)];
                [lblSampleRate setAttributedText:attrLockStr];
            }
            
            int sigGenType = pkt->sg_setting_ctrl & 0x03;
            switch (sigGenType) {
                case 0:
                    [btnSine100Hz setSelected:YES];
                    [btnSine1kHz setSelected:NO];
                    [btnSine10kHz setSelected:NO];
                    [btnPinkNoise setSelected:NO];
                    break;
                case 1:
                    [btnSine100Hz setSelected:NO];
                    [btnSine1kHz setSelected:YES];
                    [btnSine10kHz setSelected:NO];
                    [btnPinkNoise setSelected:NO];
                    break;
                case 2:
                    [btnSine100Hz setSelected:NO];
                    [btnSine1kHz setSelected:NO];
                    [btnSine10kHz setSelected:YES];
                    [btnPinkNoise setSelected:NO];
                    break;
                case 3:
                    [btnSine100Hz setSelected:NO];
                    [btnSine1kHz setSelected:NO];
                    [btnSine10kHz setSelected:NO];
                    [btnPinkNoise setSelected:YES];
                    break;
                default:
                    break;                    
            }
            
            btnSigGenOnOff.selected = (pkt->sg_setting_ctrl & 0x0100) >> 8;
            knobSigGen.value = pkt->sg_level + SIG_GEN_OFFSET;
            [self knobSigGenDidChange:nil];
            btnSigGenGp1.selected = pkt->sg_assign & 0x01;
            btnSigGenGp2.selected = (pkt->sg_assign & 0x02) >> 1;
            btnSigGenGp3.selected = (pkt->sg_assign & 0x04) >> 2;
            btnSigGenGp4.selected = (pkt->sg_assign & 0x08) >> 3;
            btnSigGenAux1.selected = (pkt->sg_assign & 0x0100) >> 8;
            btnSigGenAux2.selected = (pkt->sg_assign & 0x0200) >> 9;
            btnSigGenAux3.selected = (pkt->sg_assign & 0x0400) >> 10;
            btnSigGenAux4.selected = (pkt->sg_assign & 0x0800) >> 11;
            btnSigGenMain.selected = (pkt->gp_to_main & 0x0100) >> 8;
            
            // Update USB assign
            if (_usbAudioOutL != pkt->usb_audio_l_out_assign) {
                _usbAudioOutL = pkt->usb_audio_l_out_assign;
                switch (_usbAudioOutL) {
                    case USB_ASSIGN_CH_1:
                        [btnUsbAudioOutputL setTitle:@"CH 1" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_CH_2:
                        [btnUsbAudioOutputL setTitle:@"CH 2" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_CH_3:
                        [btnUsbAudioOutputL setTitle:@"CH 3" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_CH_4:
                        [btnUsbAudioOutputL setTitle:@"CH 4" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_CH_5:
                        [btnUsbAudioOutputL setTitle:@"CH 5" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_CH_6:
                        [btnUsbAudioOutputL setTitle:@"CH 6" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_CH_7:
                        [btnUsbAudioOutputL setTitle:@"CH 7" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_CH_8:
                        [btnUsbAudioOutputL setTitle:@"CH 8" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_CH_9:
                        [btnUsbAudioOutputL setTitle:@"CH 9" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_CH_10:
                        [btnUsbAudioOutputL setTitle:@"CH 10" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_CH_11:
                        [btnUsbAudioOutputL setTitle:@"CH 11" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_CH_12:
                        [btnUsbAudioOutputL setTitle:@"CH 12" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_CH_13:
                        [btnUsbAudioOutputL setTitle:@"CH 13" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_CH_14:
                        [btnUsbAudioOutputL setTitle:@"CH 14" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_CH_15:
                        [btnUsbAudioOutputL setTitle:@"CH 15" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_CH_16:
                        [btnUsbAudioOutputL setTitle:@"CH 16" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_CH_17:
                        [btnUsbAudioOutputL setTitle:@"CH 17" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_CH_18:
                        [btnUsbAudioOutputL setTitle:@"CH 18" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_NULL:
                        [btnUsbAudioOutputL setTitle:@"NULL" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_AUX_1:
                        [btnUsbAudioOutputL setTitle:@"AUX 1" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_AUX_2:
                        [btnUsbAudioOutputL setTitle:@"AUX 2" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_AUX_3:
                        [btnUsbAudioOutputL setTitle:@"AUX 3" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_AUX_4:
                        [btnUsbAudioOutputL setTitle:@"AUX 4" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_GP_1:
                        [btnUsbAudioOutputL setTitle:@"GP 1" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_GP_2:
                        [btnUsbAudioOutputL setTitle:@"GP 2" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_GP_3:
                        [btnUsbAudioOutputL setTitle:@"GP 3" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_GP_4:
                        [btnUsbAudioOutputL setTitle:@"GP 4" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_MULTI_1:
                        [btnUsbAudioOutputL setTitle:@"MULTI 1" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_MULTI_2:
                        [btnUsbAudioOutputL setTitle:@"MULTI 2" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_MULTI_3:
                        [btnUsbAudioOutputL setTitle:@"MULTI 3" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_MULTI_4:
                        [btnUsbAudioOutputL setTitle:@"MULTI 4" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_MAIN_L:
                        [btnUsbAudioOutputL setTitle:@"MAIN L" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_MAIN_R:
                        [btnUsbAudioOutputL setTitle:@"MAIN R" forState:UIControlStateNormal];
                        break;
                    default:
                        break;
                }
            }
            
            if (_usbAudioOutR != pkt->usb_audio_r_out_assign) {
                _usbAudioOutR = pkt->usb_audio_r_out_assign;
                switch (_usbAudioOutR) {
                    case USB_ASSIGN_CH_1:
                        [btnUsbAudioOutputR setTitle:@"CH 1" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_CH_2:
                        [btnUsbAudioOutputR setTitle:@"CH 2" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_CH_3:
                        [btnUsbAudioOutputR setTitle:@"CH 3" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_CH_4:
                        [btnUsbAudioOutputR setTitle:@"CH 4" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_CH_5:
                        [btnUsbAudioOutputR setTitle:@"CH 5" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_CH_6:
                        [btnUsbAudioOutputR setTitle:@"CH 6" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_CH_7:
                        [btnUsbAudioOutputR setTitle:@"CH 7" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_CH_8:
                        [btnUsbAudioOutputR setTitle:@"CH 8" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_CH_9:
                        [btnUsbAudioOutputR setTitle:@"CH 9" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_CH_10:
                        [btnUsbAudioOutputR setTitle:@"CH 10" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_CH_11:
                        [btnUsbAudioOutputR setTitle:@"CH 11" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_CH_12:
                        [btnUsbAudioOutputR setTitle:@"CH 12" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_CH_13:
                        [btnUsbAudioOutputR setTitle:@"CH 13" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_CH_14:
                        [btnUsbAudioOutputR setTitle:@"CH 14" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_CH_15:
                        [btnUsbAudioOutputR setTitle:@"CH 15" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_CH_16:
                        [btnUsbAudioOutputR setTitle:@"CH 16" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_CH_17:
                        [btnUsbAudioOutputR setTitle:@"CH 17" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_CH_18:
                        [btnUsbAudioOutputR setTitle:@"CH 18" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_NULL:
                        [btnUsbAudioOutputR setTitle:@"NULL" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_AUX_1:
                        [btnUsbAudioOutputR setTitle:@"AUX 1" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_AUX_2:
                        [btnUsbAudioOutputR setTitle:@"AUX 2" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_AUX_3:
                        [btnUsbAudioOutputR setTitle:@"AUX 3" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_AUX_4:
                        [btnUsbAudioOutputR setTitle:@"AUX 4" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_GP_1:
                        [btnUsbAudioOutputR setTitle:@"GP 1" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_GP_2:
                        [btnUsbAudioOutputR setTitle:@"GP 2" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_GP_3:
                        [btnUsbAudioOutputR setTitle:@"GP 3" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_GP_4:
                        [btnUsbAudioOutputR setTitle:@"GP 4" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_MULTI_1:
                        [btnUsbAudioOutputR setTitle:@"MULTI 1" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_MULTI_2:
                        [btnUsbAudioOutputR setTitle:@"MULTI 2" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_MULTI_3:
                        [btnUsbAudioOutputR setTitle:@"MULTI 3" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_MULTI_4:
                        [btnUsbAudioOutputR setTitle:@"MULTI 4" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_MAIN_L:
                        [btnUsbAudioOutputR setTitle:@"MAIN L" forState:UIControlStateNormal];
                        break;
                    case USB_ASSIGN_MAIN_R:
                        [btnUsbAudioOutputR setTitle:@"MAIN R" forState:UIControlStateNormal];
                        break;
                    default:
                        break;
                }
            }
            
            return;
        }
        case DELAY_48V_ORDER_PAGE_PARAMETER:
        {
            Delay48VOrderPagePacket *pkt = (Delay48VOrderPagePacket *)buffer;
            
            // Check CRC
            if (![Global checkCrc:buffer
                              len:sizeof(Delay48VOrderPagePacket)-FRAME_PROTOCOL_LEN pktCrc:pkt->crc]) {
                NSLog(@"DELAY_48V_ORDER_PAGE_PARAMETER invalid crc!");
                return;
            }
            
            _ch1Ctrl = pkt->channel_1_ctrl;
            _ch2Ctrl = pkt->channel_2_ctrl;
            _ch3Ctrl = pkt->channel_3_ctrl;
            _ch4Ctrl = pkt->channel_4_ctrl;
            _ch5Ctrl = pkt->channel_5_ctrl;
            _ch6Ctrl = pkt->channel_6_ctrl;
            _ch7Ctrl = pkt->channel_7_ctrl;
            _ch8Ctrl = pkt->channel_8_ctrl;
            _ch9Ctrl = pkt->channel_9_ctrl;
            _ch10Ctrl = pkt->channel_10_ctrl;
            _ch11Ctrl = pkt->channel_11_ctrl;
            _ch12Ctrl = pkt->channel_12_ctrl;
            _ch13Ctrl = pkt->channel_13_ctrl;
            _ch14Ctrl = pkt->channel_14_ctrl;
            _ch15Ctrl = pkt->channel_15_ctrl;
            _ch16Ctrl = pkt->channel_16_ctrl;
            _ch17Ctrl = pkt->channel_17_ctrl;
            _ch18Ctrl = pkt->channel_18_ctrl;
            _multi1Ctrl = pkt->multi_1_ctrl;
            _multi2Ctrl = pkt->multi_2_ctrl;
            _multi3Ctrl = pkt->multi_3_ctrl;
            _multi4Ctrl = pkt->multi_4_ctrl;
            _mainCtrl = pkt->main_ctrl;
            _ch1DelayTime = pkt->channel_1_delay_time;
            _ch2DelayTime = pkt->channel_2_delay_time;
            _ch3DelayTime = pkt->channel_3_delay_time;
            _ch4DelayTime = pkt->channel_4_delay_time;
            _ch5DelayTime = pkt->channel_5_delay_time;
            _ch6DelayTime = pkt->channel_6_delay_time;
            _ch7DelayTime = pkt->channel_7_delay_time;
            _ch8DelayTime = pkt->channel_8_delay_time;
            _ch9DelayTime = pkt->channel_9_delay_time;
            _ch10DelayTime = pkt->channel_10_delay_time;
            _ch11DelayTime = pkt->channel_11_delay_time;
            _ch12DelayTime = pkt->channel_12_delay_time;
            _ch13DelayTime = pkt->channel_13_delay_time;
            _ch14DelayTime = pkt->channel_14_delay_time;
            _ch15DelayTime = pkt->channel_15_delay_time;
            _ch16DelayTime = pkt->channel_16_delay_time;
            _ch17DelayTime = pkt->channel_17_delay_time;
            _ch18DelayTime = pkt->channel_18_delay_time;
            _multi1DelayTime = pkt->multi_1_delay_time;
            _multi2DelayTime = pkt->multi_2_delay_time;
            _multi3DelayTime = pkt->multi_3_delay_time;
            _multi4DelayTime = pkt->multi_4_delay_time;
            _mainDelayTime = pkt->main_delay_time;
            _ch48vCtrl = pkt->ch_48v_ctrl;
            
            // Delay view
            if (!viewDelay.hidden) {
                if (!viewDelayPage1.hidden) {
                    btnDelayCh1OnOff.selected = (pkt->channel_1_ctrl & 0x04) >> 2;
                    btnDelayCh2OnOff.selected = (pkt->channel_2_ctrl & 0x04) >> 2;
                    btnDelayCh3OnOff.selected = (pkt->channel_3_ctrl & 0x04) >> 2;
                    btnDelayCh4OnOff.selected = (pkt->channel_4_ctrl & 0x04) >> 2;
                    btnDelayCh5OnOff.selected = (pkt->channel_5_ctrl & 0x04) >> 2;
                    btnDelayCh6OnOff.selected = (pkt->channel_6_ctrl & 0x04) >> 2;
                    btnDelayCh7OnOff.selected = (pkt->channel_7_ctrl & 0x04) >> 2;
                    btnDelayCh8OnOff.selected = (pkt->channel_8_ctrl & 0x04) >> 2;
                    btnDelayCh9OnOff.selected = (pkt->channel_9_ctrl & 0x04) >> 2;
                    btnDelayCh10OnOff.selected = (pkt->channel_10_ctrl & 0x04) >> 2;
                    btnDelayCh11OnOff.selected = (pkt->channel_11_ctrl & 0x04) >> 2;
                    btnDelayCh12OnOff.selected = (pkt->channel_12_ctrl & 0x04) >> 2;
                    btnDelayCh13OnOff.selected = (pkt->channel_13_ctrl & 0x04) >> 2;
                    btnDelayCh14OnOff.selected = (pkt->channel_14_ctrl & 0x04) >> 2;
                    btnDelayCh15OnOff.selected = (pkt->channel_15_ctrl & 0x04) >> 2;
                    btnDelayCh16OnOff.selected = (pkt->channel_16_ctrl & 0x04) >> 2;
                    knobDelayCh1.value = [Global delayToKnobValue:pkt->channel_1_delay_time];
                    [self knobDelayCh1DidChange:nil];
                    knobDelayCh2.value = [Global delayToKnobValue:pkt->channel_2_delay_time];
                    [self knobDelayCh2DidChange:nil];
                    knobDelayCh3.value = [Global delayToKnobValue:pkt->channel_3_delay_time];
                    [self knobDelayCh3DidChange:nil];
                    knobDelayCh4.value = [Global delayToKnobValue:pkt->channel_4_delay_time];
                    [self knobDelayCh4DidChange:nil];
                    knobDelayCh5.value = [Global delayToKnobValue:pkt->channel_5_delay_time];
                    [self knobDelayCh5DidChange:nil];
                    knobDelayCh6.value = [Global delayToKnobValue:pkt->channel_6_delay_time];
                    [self knobDelayCh6DidChange:nil];
                    knobDelayCh7.value = [Global delayToKnobValue:pkt->channel_7_delay_time];
                    [self knobDelayCh7DidChange:nil];
                    knobDelayCh8.value = [Global delayToKnobValue:pkt->channel_8_delay_time];
                    [self knobDelayCh8DidChange:nil];
                    knobDelayCh9.value = [Global delayToKnobValue:pkt->channel_9_delay_time];
                    [self knobDelayCh9DidChange:nil];
                    knobDelayCh10.value = [Global delayToKnobValue:pkt->channel_10_delay_time];
                    [self knobDelayCh10DidChange:nil];
                    knobDelayCh11.value = [Global delayToKnobValue:pkt->channel_11_delay_time];
                    [self knobDelayCh11DidChange:nil];
                    knobDelayCh12.value = [Global delayToKnobValue:pkt->channel_12_delay_time];
                    [self knobDelayCh12DidChange:nil];
                    knobDelayCh13.value = [Global delayToKnobValue:pkt->channel_13_delay_time];
                    [self knobDelayCh13DidChange:nil];
                    knobDelayCh14.value = [Global delayToKnobValue:pkt->channel_14_delay_time];
                    [self knobDelayCh14DidChange:nil];
                    knobDelayCh15.value = [Global delayToKnobValue:pkt->channel_15_delay_time];
                    [self knobDelayCh15DidChange:nil];
                    knobDelayCh16.value = [Global delayToKnobValue:pkt->channel_16_delay_time];
                    [self knobDelayCh16DidChange:nil];
                } else if (!viewDelayPage2.hidden) {
                    btnDelayCh17OnOff.selected = (pkt->channel_17_ctrl & 0x04) >> 2;
                    btnDelayCh18OnOff.selected = (pkt->channel_18_ctrl & 0x04) >> 2;
                    knobDelayCh17.value = [Global delayToKnobValue:pkt->channel_17_delay_time];
                    [self knobDelayCh17DidChange:nil];
                    knobDelayCh18.value = [Global delayToKnobValue:pkt->channel_18_delay_time];
                    [self knobDelayCh18DidChange:nil];
                } else if (!viewDelayPage3.hidden) {
                    btnDelayMt1OnOff.selected = (pkt->multi_1_ctrl & 0x04) >> 2;
                    btnDelayMt2OnOff.selected = (pkt->multi_2_ctrl & 0x04) >> 2;
                    btnDelayMt3OnOff.selected = (pkt->multi_3_ctrl & 0x04) >> 2;
                    btnDelayMt4OnOff.selected = (pkt->multi_4_ctrl & 0x04) >> 2;
                    btnDelayMainOnOff.selected = (pkt->main_ctrl & 0x04) >> 2;
                    knobDelayMt1.value = [Global delayToKnobValue:pkt->multi_1_delay_time];
                    [self knobDelayMt1DidChange:nil];
                    knobDelayMt2.value = [Global delayToKnobValue:pkt->multi_2_delay_time];
                    [self knobDelayMt2DidChange:nil];
                    knobDelayMt3.value = [Global delayToKnobValue:pkt->multi_3_delay_time];
                    [self knobDelayMt3DidChange:nil];
                    knobDelayMt4.value = [Global delayToKnobValue:pkt->multi_4_delay_time];
                    [self knobDelayMt4DidChange:nil];
                    knobDelayMain.value = [Global delayToKnobValue:pkt->main_delay_time];
                    [self knobDelayMainDidChange:nil];
                }
            }
            
            // Phantom power view
            if (!viewPhantomPower.hidden) {
                int phantomPower = pkt->ch_48v_ctrl;
                btnPhantomPowerCh1OnOff.selected = phantomPower & 0x01;
                btnPhantomPowerCh2OnOff.selected = (phantomPower & 0x02) >> 1;
                btnPhantomPowerCh3OnOff.selected = (phantomPower & 0x04) >> 2;
                btnPhantomPowerCh4OnOff.selected = (phantomPower & 0x08) >> 3;
                btnPhantomPowerCh5OnOff.selected = (phantomPower & 0x10) >> 4;
                btnPhantomPowerCh6OnOff.selected = (phantomPower & 0x20) >> 5;
                btnPhantomPowerCh7OnOff.selected = (phantomPower & 0x40) >> 6;
                btnPhantomPowerCh8OnOff.selected = (phantomPower & 0x80) >> 7;
                btnPhantomPowerCh9OnOff.selected = (phantomPower & 0x0100) >> 8;
                btnPhantomPowerCh10OnOff.selected = (phantomPower & 0x0200) >> 9;
                btnPhantomPowerCh11OnOff.selected = (phantomPower & 0x0400) >> 10;
                btnPhantomPowerCh12OnOff.selected = (phantomPower & 0x0800) >> 11;
                btnPhantomPowerCh13OnOff.selected = (phantomPower & 0x1000) >> 12;
                btnPhantomPowerCh14OnOff.selected = (phantomPower & 0x2000) >> 13;
                btnPhantomPowerCh15OnOff.selected = (phantomPower & 0x4000) >> 14;
                btnPhantomPowerCh16OnOff.selected = (phantomPower & 0x8000) >> 15;
            }
            
            // Order view
            if (!viewOrder.hidden) {
                if (!viewOrderPage1.hidden) {
                    int orderCh1 = (pkt->channel_1_ctrl & 0xE000) >> 13;
                    if (orderCh1 != orderChannel[CH_1]) {
                        orderChannel[CH_1] = orderCh1;
                        [self updateOrderLabels:CH_1];
                    }
                    int orderCh2 = (pkt->channel_2_ctrl & 0xE000) >> 13;
                    if (orderCh2 != orderChannel[CH_2]) {
                        orderChannel[CH_2] = orderCh2;
                        [self updateOrderLabels:CH_2];
                    }
                    int orderCh3 = (pkt->channel_3_ctrl & 0xE000) >> 13;
                    if (orderCh3 != orderChannel[CH_3]) {
                        orderChannel[CH_3] = orderCh3;
                        [self updateOrderLabels:CH_3];
                    }
                    int orderCh4 = (pkt->channel_4_ctrl & 0xE000) >> 13;
                    if (orderCh4 != orderChannel[CH_4]) {
                        orderChannel[CH_4] = orderCh4;
                        [self updateOrderLabels:CH_4];
                    }
                    int orderCh5 = (pkt->channel_5_ctrl & 0xE000) >> 13;
                    if (orderCh5 != orderChannel[CH_5]) {
                        orderChannel[CH_5] = orderCh5;
                        [self updateOrderLabels:CH_5];
                    }
                    int orderCh6 = (pkt->channel_6_ctrl & 0xE000) >> 13;
                    if (orderCh6 != orderChannel[CH_6]) {
                        orderChannel[CH_6] = orderCh6;
                        [self updateOrderLabels:CH_6];
                    }
                    int orderCh7 = (pkt->channel_7_ctrl & 0xE000) >> 13;
                    if (orderCh7 != orderChannel[CH_7]) {
                        orderChannel[CH_7] = orderCh7;
                        [self updateOrderLabels:CH_7];
                    }
                    int orderCh8 = (pkt->channel_8_ctrl & 0xE000) >> 13;
                    if (orderCh8 != orderChannel[CH_8]) {
                        orderChannel[CH_8] = orderCh8;
                        [self updateOrderLabels:CH_8];
                    }
                    int orderCh9 = (pkt->channel_9_ctrl & 0xE000) >> 13;
                    if (orderCh9 != orderChannel[CH_9]) {
                        orderChannel[CH_9] = orderCh9;
                        [self updateOrderLabels:CH_9];
                    }
                    int orderCh10 = (pkt->channel_10_ctrl & 0xE000) >> 13;
                    if (orderCh10 != orderChannel[CH_10]) {
                        orderChannel[CH_10] = orderCh10;
                        [self updateOrderLabels:CH_10];
                    }
                    int orderCh11 = (pkt->channel_11_ctrl & 0xE000) >> 13;
                    if (orderCh11 != orderChannel[CH_11]) {
                        orderChannel[CH_11] = orderCh11;
                        [self updateOrderLabels:CH_11];
                    }
                    int orderCh12 = (pkt->channel_12_ctrl & 0xE000) >> 13;
                    if (orderCh12 != orderChannel[CH_12]) {
                        orderChannel[CH_12] = orderCh12;
                        [self updateOrderLabels:CH_12];
                    }
                    int orderCh13 = (pkt->channel_13_ctrl & 0xE000) >> 13;
                    if (orderCh13 != orderChannel[CH_13]) {
                        orderChannel[CH_13] = orderCh13;
                        [self updateOrderLabels:CH_13];
                    }
                    int orderCh14 = (pkt->channel_14_ctrl & 0xE000) >> 13;
                    if (orderCh14 != orderChannel[CH_14]) {
                        orderChannel[CH_14] = orderCh14;
                        [self updateOrderLabels:CH_14];
                    }
                    int orderCh15 = (pkt->channel_15_ctrl & 0xE000) >> 13;
                    if (orderCh15 != orderChannel[CH_15]) {
                        orderChannel[CH_15] = orderCh15;
                        [self updateOrderLabels:CH_15];
                    }
                    int orderCh16 = (pkt->channel_16_ctrl & 0xE000) >> 13;
                    if (orderCh16 != orderChannel[CH_16]) {
                        orderChannel[CH_16] = orderCh16;
                        [self updateOrderLabels:CH_16];
                    }
                } else if (!viewOrderPage2.hidden) {
                    int orderCh17 = (pkt->channel_17_ctrl & 0xE000) >> 13;
                    if (orderCh17 != orderChannel[CH_17]) {
                        orderChannel[CH_17] = orderCh17;
                        [self updateOrderLabels:CH_17];
                    }
                    int orderCh18 = (pkt->channel_18_ctrl & 0xE000) >> 13;
                    if (orderCh18 != orderChannel[CH_18]) {
                        orderChannel[CH_18] = orderCh18;
                        [self updateOrderLabels:CH_18];
                    }
                } else if (!viewOrderPage3.hidden) {
                    int orderMt1 = (pkt->multi_1_ctrl & 0xE000) >> 13;
                    if (orderMt1 != orderMulti[0]) {
                        orderMulti[0] = orderMt1;
                        [self updateOrderLabels:MT_1];
                    }
                    int orderMt2 = (pkt->multi_2_ctrl & 0xE000) >> 13;
                    if (orderMt2 != orderMulti[1]) {
                        orderMulti[1] = orderMt2;
                        [self updateOrderLabels:MT_2];
                    }
                    int orderMt3 = (pkt->multi_3_ctrl & 0xE000) >> 13;
                    if (orderMt3 != orderMulti[2]) {
                        orderMulti[2] = orderMt3;
                        [self updateOrderLabels:MT_3];
                    }
                    int orderMt4 = (pkt->multi_4_ctrl & 0xE000) >> 13;
                    if (orderMt4 != orderMulti[3]) {
                        orderMulti[3] = orderMt4;
                        [self updateOrderLabels:MT_4];
                    }
                    int orderMainCtrl = (pkt->main_ctrl & 0xE000) >> 13;
                    if (orderMainCtrl != orderMain) {
                        orderMain = orderMainCtrl;
                        [self updateOrderLabels:MAIN];
                    }
                }
            }
            return;
        }
        case DYN_PAGE_PARAMETER:
        {
            DynPagePacket *pkt = (DynPagePacket *)buffer;
            
            // Check CRC
            if (![Global checkCrc:buffer len:sizeof(DynPagePacket)-FRAME_PROTOCOL_LEN pktCrc:pkt->crc]) {
                NSLog(@"DYN_PAGE_PARAMETER invalid crc!");
                return;
            }
            
            if (dynController != nil) {
                [dynController processReply:pkt];
            }
            return;
        }
        case CONNECT_MAX:
        {
            /*
            [activityLoading stopAnimating];
            NSLog(@"Too many connections!");
            [[[UIAlertView alloc] initWithTitle:@"Network error"
                                        message:@"Too many connections error"
                                       delegate:nil
                              cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            
            [self closeNetworkCommunication];
            [switchSetupLocate setOn:NO];
            [lblIpAddress setText:@""];
            [lblOnline setText:@"Offline"];
            [imgOnline setImage:[UIImage imageNamed:@"offline.png"]];
            _loginDone = NO;
             */
            return;
        }
        case REVERB_READ:
        {
            NSLog(@"Received Reverb Read, len = %d", len);
            return;
        }
        case GET_FILE_LIST:
        {
            GetFileListRxPacket *fileList = (GetFileListRxPacket *)buffer;
            
            // Check CRC
            if (![Global checkCrc:buffer
                              len:sizeof(GetFileListRxPacket)-FRAME_PROTOCOL_LEN pktCrc:fileList->crc]) {
                NSLog(@"GET_FILE_LIST invalid crc!");
                return;
            }
            
            if (fileList->result == 0) {
                NSLog(@"Get file list failed!");
                return;
            }
            
            if (!viewScenes.hidden) {
                _sceneCount = fileList->file_count;
                [_sceneFiles removeAllObjects];

                for (int i=0; i<_sceneCount; i++) {
                    // Update scene list
                    const void* filePtr = buffer + 20 + i*SCENE_FILE_LENGTH;
                    NSString *fileName = [[NSString alloc] initWithBytes:filePtr length:SCENE_FILE_LENGTH encoding:NSASCIIStringEncoding];
                    //NSLog(@"Scene %d filename = %@", i, fileName);
                    [_sceneFiles addObject:fileName];
                }

                // Update scene list
                [tableViewScenes reloadData];
            }
            return;
        }
        default:
            //NSLog(@"Invalid command: %d", cmdType);
            return;
    }

    [self processReply:cmdType command:cmd dspId:dspId value:value];
}

#pragma mark -
#pragma mark UI update methods

- (void)updateStatus
{
    // File scene list
    if (!viewScenes.hidden) {
        [self sendData2:GET_FILE_LIST value1:[btnSceneUsb isSelected] value2:_currentSceneMode];
    }
    
    // Initiate first send
    [self sendNextReadCommand];
}

- (void)processReply:(int)commandType command:(int)command dspId:(int)dspId value:(int)value
{
    if (commandType == AUTHORIZED) {
        NSLog(@"Login - AUTHORIZED");
        [activityLoading stopAnimating];
        
        if (value == 1) {
            _isAuthenticated = YES;
            
            // Login success
            [lblIpAddress setText:_ipAddress];
            [lblOnline setText:@"Online"];
            [imgOnline setImage:[UIImage imageNamed:@"online.png"]];
            [[NSUserDefaults standardUserDefaults] setObject:_ipAddress forKey:@"DeviceIP"];
            [[NSUserDefaults standardUserDefaults] setObject:_username forKey:@"Username"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            /*
            [[[UIAlertView alloc] initWithTitle:nil
                                        message:@"Connected"
                                       delegate:nil
                              cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            */
            
            HUD = [[MBProgressHUD alloc] initWithView:self.view];
            [self.view addSubview:HUD];
            HUD.delegate = self;
            HUD.labelText = @"Connecting";
            [HUD showWhileExecuting:@selector(connectingTask) onTarget:self withObject:nil animated:YES];

            // Setup EQ preview update
            if (geqPeqController == nil) {
                geqPeqController = (GeqPeqViewController *)[[GeqPeqViewController alloc] initWithNibName:@"GeqPeqView" bundle:nil];
                [geqPeqController.view setFrame:CGRectMake(0, 153, 1024, 615)];
                [self.view addSubview:geqPeqController.view];
                geqPeqController.delegate = self;
            }
            
            // Setup DYN preview update
            if (dynController == nil) {
                dynController = (DynViewController *)[[DynViewController alloc] initWithNibName:@"DynViewController" bundle:nil];
                [dynController.view setFrame:CGRectMake(0, 153, 1024, 615)];
                [self.view addSubview:dynController.view];
                dynController.delegate = self;
            }
            
            // Setup UI update timer
            _timer = [NSTimer timerWithTimeInterval:UI_UPDATE_INTERVAL target:self selector:@selector(updateStatus) userInfo:nil repeats:YES];
            [[NSRunLoop mainRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
            
            // Refresh page
            [self refreshPage];
        } else if (value == 0) {
            // Login failed
            NSLog(@"Login failed!");
            [[[UIAlertView alloc] initWithTitle:@"Network error"
                                        message:@"The IP, username and password combination you entered does not exist. Please verify your network connections and try again."
                                       delegate:nil
                              cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            
            [self disconnectNetwork];
        }
        return;
    } else if (commandType == DSP_WRITE) {
        if (value == 0) {
            NSLog(@"Command %d write failed!", command);
        }
        return;
    } else if (commandType == SET_SEND_PAGE) {
        if (value == 0) {
            NSLog(@"Set send page failed!");
        }
        return;
    } else if (commandType == SET_DELAY) {
        if (value == 0) {
            NSLog(@"Set delay failed!");
        }
        return;
    } else if (commandType == REVERB_WRITE) {
        if (value == 0) {
            NSLog(@"Command %d write failed!", command);
        }
        return;
    } else if (commandType != DSP_READ) {
        NSLog(@"Invalid command type %d!", commandType);
        return;
    }
    
    // Send next read command
    [self performSelector:@selector(sendNextReadCommand) withObject:nil afterDelay:READ_CMD_INTERVAL];
}

- (void)connectingTask {
    // Wait while EQ and DYN preview updates finish
	sleep(CONNECTING_TIME);
}

- (void)hideAllModeButtons:(BOOL)hideFlag
{
    [sliderPan1 setHidden:hideFlag];
    [sliderPan2 setHidden:hideFlag];
    [sliderPan3 setHidden:hideFlag];
    [sliderPan4 setHidden:hideFlag];
    [sliderPan5 setHidden:hideFlag];
    [sliderPan6 setHidden:hideFlag];
    [sliderPan7 setHidden:hideFlag];
    [sliderPan8 setHidden:hideFlag];
    [lblPanValue1 setHidden:hideFlag];
    [lblPanValue2 setHidden:hideFlag];
    [lblPanValue3 setHidden:hideFlag];
    [lblPanValue4 setHidden:hideFlag];
    [lblPanValue5 setHidden:hideFlag];
    [lblPanValue6 setHidden:hideFlag];
    [lblPanValue7 setHidden:hideFlag];
    [lblPanValue8 setHidden:hideFlag];
    [meterPan1 setHidden:hideFlag];
    [meterPan2 setHidden:hideFlag];
    [meterPan3 setHidden:hideFlag];
    [meterPan4 setHidden:hideFlag];
    [meterPan5 setHidden:hideFlag];
    [meterPan6 setHidden:hideFlag];
    [meterPan7 setHidden:hideFlag];
    [meterPan8 setHidden:hideFlag];
    [btnSliderPanEq1 setHidden:hideFlag];
    [btnSliderPanEq2 setHidden:hideFlag];
    [btnSliderPanEq3 setHidden:hideFlag];
    [btnSliderPanEq4 setHidden:hideFlag];
    [btnSliderPanEq5 setHidden:hideFlag];
    [btnSliderPanEq6 setHidden:hideFlag];
    [btnSliderPanEq7 setHidden:hideFlag];
    [btnSliderPanEq8 setHidden:hideFlag];
    [btnSliderPanEqMain setHidden:hideFlag];
    [lblOrderFirst1 setHidden:hideFlag];
    [lblOrderSecond1 setHidden:hideFlag];
    [lblOrderThird1 setHidden:hideFlag];
    [lblOrderFirst2 setHidden:hideFlag];
    [lblOrderSecond2 setHidden:hideFlag];
    [lblOrderThird2 setHidden:hideFlag];
    [lblOrderFirst3 setHidden:hideFlag];
    [lblOrderSecond3 setHidden:hideFlag];
    [lblOrderThird3 setHidden:hideFlag];
    [lblOrderFirst4 setHidden:hideFlag];
    [lblOrderSecond4 setHidden:hideFlag];
    [lblOrderThird4 setHidden:hideFlag];
    [lblOrderFirst5 setHidden:hideFlag];
    [lblOrderSecond5 setHidden:hideFlag];
    [lblOrderThird5 setHidden:hideFlag];
    [lblOrderFirst6 setHidden:hideFlag];
    [lblOrderSecond6 setHidden:hideFlag];
    [lblOrderThird6 setHidden:hideFlag];
    [lblOrderFirst7 setHidden:hideFlag];
    [lblOrderSecond7 setHidden:hideFlag];
    [lblOrderThird7 setHidden:hideFlag];
    [lblOrderFirst8 setHidden:hideFlag];
    [lblOrderSecond8 setHidden:hideFlag];
    [lblOrderThird8 setHidden:hideFlag];
    [lblOrderFirstMain setHidden:hideFlag];
    [lblOrderSecondMain setHidden:hideFlag];
    [lblOrderThirdMain setHidden:hideFlag];
    [lblDelay1 setHidden:hideFlag];
    [lblDelay2 setHidden:hideFlag];
    [lblDelay3 setHidden:hideFlag];
    [lblDelay4 setHidden:hideFlag];
    [lblDelay5 setHidden:hideFlag];
    [lblDelay6 setHidden:hideFlag];
    [lblDelay7 setHidden:hideFlag];
    [lblDelay8 setHidden:hideFlag];
    [lblDelayMain setHidden:hideFlag];
    [btnDynG1 setHidden:hideFlag];
    [btnDynE1 setHidden:hideFlag];
    [btnDynC1 setHidden:hideFlag];
    [btnDynL1 setHidden:hideFlag];
    [btnDynG2 setHidden:hideFlag];
    [btnDynE2 setHidden:hideFlag];
    [btnDynC2 setHidden:hideFlag];
    [btnDynL2 setHidden:hideFlag];
    [btnDynG3 setHidden:hideFlag];
    [btnDynE3 setHidden:hideFlag];
    [btnDynC3 setHidden:hideFlag];
    [btnDynL3 setHidden:hideFlag];
    [btnDynG4 setHidden:hideFlag];
    [btnDynE4 setHidden:hideFlag];
    [btnDynC4 setHidden:hideFlag];
    [btnDynL4 setHidden:hideFlag];
    [btnDynG5 setHidden:hideFlag];
    [btnDynE5 setHidden:hideFlag];
    [btnDynC5 setHidden:hideFlag];
    [btnDynL5 setHidden:hideFlag];
    [btnDynG6 setHidden:hideFlag];
    [btnDynE6 setHidden:hideFlag];
    [btnDynC6 setHidden:hideFlag];
    [btnDynL6 setHidden:hideFlag];
    [btnDynG7 setHidden:hideFlag];
    [btnDynE7 setHidden:hideFlag];
    [btnDynC7 setHidden:hideFlag];
    [btnDynL7 setHidden:hideFlag];
    [btnDynG8 setHidden:hideFlag];
    [btnDynE8 setHidden:hideFlag];
    [btnDynC8 setHidden:hideFlag];
    [btnDynL8 setHidden:hideFlag];    
    [btnDynGMain setHidden:hideFlag];
    [btnDynEMain setHidden:hideFlag];
    [btnDynCMain setHidden:hideFlag];
    [btnDynLMain setHidden:hideFlag];
    
    switch (currentPage) {
        case PAGE_GP_1_4:
        case PAGE_MT_1_4:
            [sliderPan5 setHidden:YES];
            [sliderPan6 setHidden:YES];
            [sliderPan7 setHidden:YES];
            [sliderPan8 setHidden:YES];
            [lblPanValue5 setHidden:YES];
            [lblPanValue6 setHidden:YES];
            [lblPanValue7 setHidden:YES];
            [lblPanValue8 setHidden:YES];
            [meterPan5 setHidden:YES];
            [meterPan6 setHidden:YES];
            [meterPan7 setHidden:YES];
            [meterPan8 setHidden:YES];
            [btnSliderPanEq5 setHidden:YES];
            [btnSliderPanEq6 setHidden:YES];
            [btnSliderPanEq7 setHidden:YES];
            [btnSliderPanEq8 setHidden:YES];            
            [lblOrderFirst5 setHidden:YES];
            [lblOrderSecond5 setHidden:YES];
            [lblOrderThird5 setHidden:YES];
            [lblOrderFirst6 setHidden:YES];
            [lblOrderSecond6 setHidden:YES];
            [lblOrderThird6 setHidden:YES];
            [lblOrderFirst7 setHidden:YES];
            [lblOrderSecond7 setHidden:YES];
            [lblOrderThird7 setHidden:YES];
            [lblOrderFirst8 setHidden:YES];
            [lblOrderSecond8 setHidden:YES];
            [lblOrderThird8 setHidden:YES];
            [lblDelay5 setHidden:YES];
            [lblDelay6 setHidden:YES];
            [lblDelay7 setHidden:YES];
            [lblDelay8 setHidden:YES];
            [btnDynG5 setHidden:YES];
            [btnDynE5 setHidden:YES];
            [btnDynC5 setHidden:YES];
            [btnDynL5 setHidden:YES];
            [btnDynG6 setHidden:YES];
            [btnDynE6 setHidden:YES];
            [btnDynC6 setHidden:YES];
            [btnDynL6 setHidden:YES];
            [btnDynG7 setHidden:YES];
            [btnDynE7 setHidden:YES];
            [btnDynC7 setHidden:YES];
            [btnDynL7 setHidden:YES];
            [btnDynG8 setHidden:YES];
            [btnDynE8 setHidden:YES];
            [btnDynC8 setHidden:YES];
            [btnDynL8 setHidden:YES];
            break;
        default:
            break;
    }
    
    switch (currentMode) {
        case MODE_EQ:
        case MODE_DYN:
        case MODE_DELAY:
        case MODE_ORDER:
            [btnSliderPanEqMain setHidden:NO];
            break;
        default:
            [btnSliderPanEqMain setHidden:YES];
            break;
    }
}

- (void)hideAllSliders:(BOOL)hideFlag
{
    [viewSlider1 setHidden:hideFlag];
    [viewSlider2 setHidden:hideFlag];
    [viewSlider3 setHidden:hideFlag];
    [viewSlider4 setHidden:hideFlag];
    [viewSlider5 setHidden:hideFlag];
    [viewSlider6 setHidden:hideFlag];
    [viewSlider7 setHidden:hideFlag];
    [viewSlider8 setHidden:hideFlag];
    [slider1 setHidden:hideFlag];
    [slider2 setHidden:hideFlag];
    [slider3 setHidden:hideFlag];
    [slider4 setHidden:hideFlag];
    [slider5 setHidden:hideFlag];
    [slider6 setHidden:hideFlag];
    [slider7 setHidden:hideFlag];
    [slider8 setHidden:hideFlag];
    [meter1 setHidden:hideFlag];
    [meter2 setHidden:hideFlag];
    [meter3 setHidden:hideFlag];
    [meter4 setHidden:hideFlag];
    [meter5 setHidden:hideFlag];
    [meter6 setHidden:hideFlag];
    [meter7 setHidden:hideFlag];
    [meter8 setHidden:hideFlag];
}

- (void)hideAllSoloButtons:(BOOL)hideFlag
{
    [btnSlider1Solo setHidden:hideFlag];
    [btnSlider2Solo setHidden:hideFlag];
    [btnSlider3Solo setHidden:hideFlag];
    [btnSlider4Solo setHidden:hideFlag];
    [btnSlider5Solo setHidden:hideFlag];
    [btnSlider6Solo setHidden:hideFlag];
    [btnSlider7Solo setHidden:hideFlag];
    [btnSlider8Solo setHidden:hideFlag];
}

- (void)updateCurrentLabel
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    if (auxSendController && !auxSendController.view.hidden) {
        switch (auxSendController.currentAuxLabel) {
            case AUX_1:
                if ([userDefaults objectForKey:@"Aux1"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Aux1"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Aux1", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Aux1", @"Acapela", nil)];
                break;
            case AUX_2:
                if ([userDefaults objectForKey:@"Aux2"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Aux2"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Aux2", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Aux2", @"Acapela", nil)];
                break;
            case AUX_3:
                if ([userDefaults objectForKey:@"Aux3"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Aux3"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Aux3", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Aux3", @"Acapela", nil)];
                break;
            case AUX_4:
                if ([userDefaults objectForKey:@"Aux4"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Aux4"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Aux4", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Aux4", @"Acapela", nil)];
                break;

            default:
                break;
        }
    }
    else if (groupSendController && !groupSendController.view.hidden) {
        switch (groupSendController.currentGroupLabel) {
            case GROUP_1:
                if ([userDefaults objectForKey:@"Gp1"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Gp1"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Gp1", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Gp1", @"Acapela", nil)];
                break;
            case GROUP_2:
                if ([userDefaults objectForKey:@"Gp2"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Gp2"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Gp2", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Gp2", @"Acapela", nil)];
                break;
            case GROUP_3:
                if ([userDefaults objectForKey:@"Gp3"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Gp3"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Gp3", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Gp3", @"Acapela", nil)];
                break;
            case GROUP_4:
                if ([userDefaults objectForKey:@"Gp4"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Gp4"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Gp4", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Gp4", @"Acapela", nil)];
                break;
            default:
                break;
        }
    }
    else if (eqDynDelayController && !eqDynDelayController.view.hidden) {
        switch (eqDynDelayController.currentChannelLabel) {
            case CH_1:
                if ([userDefaults objectForKey:@"Ch1"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Ch1"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Ch1", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Ch1", @"Acapela", nil)];
                break;
            case CH_2:
                if ([userDefaults objectForKey:@"Ch2"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Ch2"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Ch2", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Ch2", @"Acapela", nil)];
                break;
            case CH_3:
                if ([userDefaults objectForKey:@"Ch3"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Ch3"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Ch3", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Ch3", @"Acapela", nil)];
                break;
            case CH_4:
                if ([userDefaults objectForKey:@"Ch4"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Ch4"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Ch4", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Ch4", @"Acapela", nil)];
                break;
            case CH_5:
                if ([userDefaults objectForKey:@"Ch5"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Ch5"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Ch5", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Ch5", @"Acapela", nil)];
                break;
            case CH_6:
                if ([userDefaults objectForKey:@"Ch6"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Ch6"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Ch6", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Ch6", @"Acapela", nil)];
                break;
            case CH_7:
                if ([userDefaults objectForKey:@"Ch7"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Ch7"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Ch7", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Ch7", @"Acapela", nil)];
                break;
            case CH_8:
                if ([userDefaults objectForKey:@"Ch8"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Ch8"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Ch8", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Ch8", @"Acapela", nil)];
                break;
            case CH_9:
                if ([userDefaults objectForKey:@"Ch9"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Ch9"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Ch9", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Ch9", @"Acapela", nil)];
                break;
            case CH_10:
                if ([userDefaults objectForKey:@"Ch10"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Ch10"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Ch10", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Ch10", @"Acapela", nil)];
                break;
            case CH_11:
                if ([userDefaults objectForKey:@"Ch11"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Ch11"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Ch11", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Ch11", @"Acapela", nil)];
                break;
            case CH_12:
                if ([userDefaults objectForKey:@"Ch12"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Ch12"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Ch12", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Ch12", @"Acapela", nil)];
                break;
            case CH_13:
                if ([userDefaults objectForKey:@"Ch13"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Ch13"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Ch13", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Ch13", @"Acapela", nil)];
                break;
            case CH_14:
                if ([userDefaults objectForKey:@"Ch14"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Ch14"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Ch14", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Ch14", @"Acapela", nil)];
                break;
            case CH_15:
                if ([userDefaults objectForKey:@"Ch15"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Ch15"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Ch15", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Ch15", @"Acapela", nil)];
                break;
            case CH_16:
                if ([userDefaults objectForKey:@"Ch16"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Ch16"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Ch16", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Ch16", @"Acapela", nil)];
                break;
            case CH_17:
                if ([userDefaults objectForKey:@"Ch17"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Ch17"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Ch17", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Ch17", @"Acapela", nil)];
                break;
            case CH_18:
                if ([userDefaults objectForKey:@"Ch18"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Ch18"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Ch18", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Ch18", @"Acapela", nil)];
                break;
            case MT_1:
                if ([userDefaults objectForKey:@"Multi1"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Multi1"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Multi1", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Multi1", @"Acapela", nil)];
                break;
            case MT_2:
                if ([userDefaults objectForKey:@"Multi2"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Multi2"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Multi2", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Multi2", @"Acapela", nil)];
                break;
            case MT_3:
                if ([userDefaults objectForKey:@"Multi3"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Multi3"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Multi3", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Multi3", @"Acapela", nil)];
                break;
            case MT_4:
                if ([userDefaults objectForKey:@"Multi4"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Multi4"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Multi4", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Multi4", @"Acapela", nil)];
                break;
            case CTRL_RM:
                if ([userDefaults objectForKey:@"CtrlRm"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"CtrlRm"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"CtrlRm", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"CtrlRm", @"Acapela", nil)];
                break;
            case MAIN:
                if ([userDefaults objectForKey:@"Main"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Main"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Main", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Main", @"Acapela", nil)];
                break;
            default:
                break;
        }
    }
    else if (dynController && !dynController.view.hidden) {
        switch (dynController.currentChannelLabel) {
            case CH_1:
                if ([userDefaults objectForKey:@"Ch1"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Ch1"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Ch1", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Ch1", @"Acapela", nil)];
                break;
            case CH_2:
                if ([userDefaults objectForKey:@"Ch2"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Ch2"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Ch2", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Ch2", @"Acapela", nil)];
                break;
            case CH_3:
                if ([userDefaults objectForKey:@"Ch3"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Ch3"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Ch3", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Ch3", @"Acapela", nil)];
                break;
            case CH_4:
                if ([userDefaults objectForKey:@"Ch4"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Ch4"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Ch4", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Ch4", @"Acapela", nil)];
                break;
            case CH_5:
                if ([userDefaults objectForKey:@"Ch5"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Ch5"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Ch5", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Ch5", @"Acapela", nil)];
                break;
            case CH_6:
                if ([userDefaults objectForKey:@"Ch6"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Ch6"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Ch6", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Ch6", @"Acapela", nil)];
                break;
            case CH_7:
                if ([userDefaults objectForKey:@"Ch7"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Ch7"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Ch7", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Ch7", @"Acapela", nil)];
                break;
            case CH_8:
                if ([userDefaults objectForKey:@"Ch8"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Ch8"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Ch8", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Ch8", @"Acapela", nil)];
                break;
            case CH_9:
                if ([userDefaults objectForKey:@"Ch9"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Ch9"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Ch9", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Ch9", @"Acapela", nil)];
                break;
            case CH_10:
                if ([userDefaults objectForKey:@"Ch10"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Ch10"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Ch10", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Ch10", @"Acapela", nil)];
                break;
            case CH_11:
                if ([userDefaults objectForKey:@"Ch11"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Ch11"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Ch11", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Ch11", @"Acapela", nil)];
                break;
            case CH_12:
                if ([userDefaults objectForKey:@"Ch12"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Ch12"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Ch12", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Ch12", @"Acapela", nil)];
                break;
            case CH_13:
                if ([userDefaults objectForKey:@"Ch13"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Ch13"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Ch13", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Ch13", @"Acapela", nil)];
                break;
            case CH_14:
                if ([userDefaults objectForKey:@"Ch14"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Ch14"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Ch14", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Ch14", @"Acapela", nil)];
                break;
            case CH_15:
                if ([userDefaults objectForKey:@"Ch15"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Ch15"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Ch15", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Ch15", @"Acapela", nil)];
                break;
            case CH_16:
                if ([userDefaults objectForKey:@"Ch16"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Ch16"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Ch16", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Ch16", @"Acapela", nil)];
                break;
            case CH_17:
                if ([userDefaults objectForKey:@"Ch17"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Ch17"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Ch17", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Ch17", @"Acapela", nil)];
                break;
            case CH_18:
                if ([userDefaults objectForKey:@"Ch18"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Ch18"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Ch18", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Ch18", @"Acapela", nil)];
                break;
            case MT_1:
                if ([userDefaults objectForKey:@"Multi1"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Multi1"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Multi1", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Multi1", @"Acapela", nil)];
                break;
            case MT_2:
                if ([userDefaults objectForKey:@"Multi2"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Multi2"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Multi2", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Multi2", @"Acapela", nil)];
                break;
            case MT_3:
                if ([userDefaults objectForKey:@"Multi3"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Multi3"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Multi3", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Multi3", @"Acapela", nil)];
                break;
            case MT_4:
                if ([userDefaults objectForKey:@"Multi4"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Multi4"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Multi4", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Multi4", @"Acapela", nil)];
                break;
            case CTRL_RM:
                if ([userDefaults objectForKey:@"CtrlRm"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"CtrlRm"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"CtrlRm", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"CtrlRm", @"Acapela", nil)];
                break;
            case MAIN:
                if ([userDefaults objectForKey:@"Main"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Main"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Main", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Main", @"Acapela", nil)];
                break;
            default:
                break;
        }
    }
    else if (geqPeqController && !geqPeqController.view.hidden) {
        switch (geqPeqController.currentChannelLabel) {
            case CHANNEL_CH_1:
                if ([userDefaults objectForKey:@"Ch1"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Ch1"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Ch1", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Ch1", @"Acapela", nil)];
                break;
            case CHANNEL_CH_2:
                if ([userDefaults objectForKey:@"Ch2"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Ch2"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Ch2", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Ch2", @"Acapela", nil)];
                break;
            case CHANNEL_CH_3:
                if ([userDefaults objectForKey:@"Ch3"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Ch3"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Ch3", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Ch3", @"Acapela", nil)];
                break;
            case CHANNEL_CH_4:
                if ([userDefaults objectForKey:@"Ch4"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Ch4"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Ch4", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Ch4", @"Acapela", nil)];
                break;
            case CHANNEL_CH_5:
                if ([userDefaults objectForKey:@"Ch5"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Ch5"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Ch5", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Ch5", @"Acapela", nil)];
                break;
            case CHANNEL_CH_6:
                if ([userDefaults objectForKey:@"Ch6"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Ch6"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Ch6", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Ch6", @"Acapela", nil)];
                break;
            case CHANNEL_CH_7:
                if ([userDefaults objectForKey:@"Ch7"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Ch7"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Ch7", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Ch7", @"Acapela", nil)];
                break;
            case CHANNEL_CH_8:
                if ([userDefaults objectForKey:@"Ch8"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Ch8"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Ch8", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Ch8", @"Acapela", nil)];
                break;
            case CHANNEL_CH_9:
                if ([userDefaults objectForKey:@"Ch9"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Ch9"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Ch9", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Ch9", @"Acapela", nil)];
                break;
            case CHANNEL_CH_10:
                if ([userDefaults objectForKey:@"Ch10"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Ch10"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Ch10", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Ch10", @"Acapela", nil)];
                break;
            case CHANNEL_CH_11:
                if ([userDefaults objectForKey:@"Ch11"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Ch11"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Ch11", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Ch11", @"Acapela", nil)];
                break;
            case CHANNEL_CH_12:
                if ([userDefaults objectForKey:@"Ch12"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Ch12"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Ch12", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Ch12", @"Acapela", nil)];
                break;
            case CHANNEL_CH_13:
                if ([userDefaults objectForKey:@"Ch13"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Ch13"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Ch13", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Ch13", @"Acapela", nil)];
                break;
            case CHANNEL_CH_14:
                if ([userDefaults objectForKey:@"Ch14"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Ch14"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Ch14", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Ch14", @"Acapela", nil)];
                break;
            case CHANNEL_CH_15:
                if ([userDefaults objectForKey:@"Ch15"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Ch15"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Ch15", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Ch15", @"Acapela", nil)];
                break;
            case CHANNEL_CH_16:
                if ([userDefaults objectForKey:@"Ch16"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Ch16"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Ch16", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Ch16", @"Acapela", nil)];
                break;
            case CHANNEL_CH_17:
                if ([userDefaults objectForKey:@"Ch17"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Ch17"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Ch17", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Ch17", @"Acapela", nil)];
                break;
            case CHANNEL_CH_18:
                if ([userDefaults objectForKey:@"Ch18"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Ch18"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Ch18", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Ch18", @"Acapela", nil)];
                break;
            case CHANNEL_MULTI_1:
                if ([userDefaults objectForKey:@"Multi1"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Multi1"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Multi1", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Multi1", @"Acapela", nil)];
                break;
            case CHANNEL_MULTI_2:
                if ([userDefaults objectForKey:@"Multi2"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Multi2"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Multi2", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Multi2", @"Acapela", nil)];
                break;
            case CHANNEL_MULTI_3:
                if ([userDefaults objectForKey:@"Multi3"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Multi3"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Multi3", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Multi3", @"Acapela", nil)];
                break;
            case CHANNEL_MULTI_4:
                if ([userDefaults objectForKey:@"Multi4"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Multi4"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Multi4", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Multi4", @"Acapela", nil)];
                break;
            case CHANNEL_MAIN:
                if ([userDefaults objectForKey:@"Main"] != nil) {
                    [txtLabelName setText:(NSString *)[userDefaults objectForKey:@"Main"]];
                } else {
                    [txtLabelName setText:NSLocalizedStringFromTable(@"Main", @"Acapela", nil)];
                }
                [lblLabelName setText:NSLocalizedStringFromTable(@"Main", @"Acapela", nil)];
                break;
            default:
                break;
        }
    }
    else {
        switch (currentLabel) {
            case 1:
                [lblLabelName setText:lblSlider1.text];
                [txtLabelName setText:txtSlider1.text];
                break;
            case 2:
                [lblLabelName setText:lblSlider2.text];
                [txtLabelName setText:txtSlider2.text];
                break;
            case 3:
                [lblLabelName setText:lblSlider3.text];
                [txtLabelName setText:txtSlider3.text];
                break;
            case 4:
                [lblLabelName setText:lblSlider4.text];
                [txtLabelName setText:txtSlider4.text];
                break;
            case 5:
                [lblLabelName setText:lblSlider5.text];
                [txtLabelName setText:txtSlider5.text];
                break;
            case 6:
                [lblLabelName setText:lblSlider6.text];
                [txtLabelName setText:txtSlider6.text];
                break;
            case 7:
                [lblLabelName setText:lblSlider7.text];
                [txtLabelName setText:txtSlider7.text];
                break;
            case 8:
                [lblLabelName setText:lblSlider8.text];
                [txtLabelName setText:txtSlider8.text];
                break;
            case 9:
                [lblLabelName setText:lblSliderMain.text];
                [txtLabelName setText:txtSliderMain.text];
                break;
            default:
                break;
        }
    }
}

- (void)writeToLabel
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    if (auxSendController && !auxSendController.view.hidden) {
        // Update name to original text field
        if (auxSendController.currentAuxLabel == auxSendController.currentAux) {
            [auxSendController.txtCurrentAux setText:txtLabelName.text];
        }
        
        // Save to user defaults
        switch (auxSendController.currentAuxLabel) {
            case AUX_1:
                [userDefaults setObject:txtLabelName.text forKey:@"Aux1"];
                [self setAudioName:2 channelNum:0 channelName:[txtLabelName.text UTF8String]];
                break;
            case AUX_2:
                [userDefaults setObject:txtLabelName.text forKey:@"Aux2"];
                [self setAudioName:2 channelNum:1 channelName:[txtLabelName.text UTF8String]];
                break;
            case AUX_3:
                [userDefaults setObject:txtLabelName.text forKey:@"Aux3"];
                [self setAudioName:2 channelNum:2 channelName:[txtLabelName.text UTF8String]];
                break;
            case AUX_4:
                [userDefaults setObject:txtLabelName.text forKey:@"Aux4"];
                [self setAudioName:2 channelNum:3 channelName:[txtLabelName.text UTF8String]];
                break;
            default:
                break;
        }
    }
    else if (groupSendController && !groupSendController.view.hidden) {
        // Update name to original text field
        if (groupSendController.currentGroupLabel == groupSendController.currentGroup) {
            [groupSendController.txtCurrentGroup setText:txtLabelName.text];
        }
        
        // Save to user defaults
        switch (groupSendController.currentGroupLabel) {
            case GROUP_1:
                [userDefaults setObject:txtLabelName.text forKey:@"Gp1"];
                [self setAudioName:3 channelNum:0 channelName:[txtLabelName.text UTF8String]];
                break;
            case GROUP_2:
                [userDefaults setObject:txtLabelName.text forKey:@"Gp2"];
                [self setAudioName:3 channelNum:1 channelName:[txtLabelName.text UTF8String]];
                break;
            case GROUP_3:
                [userDefaults setObject:txtLabelName.text forKey:@"Gp3"];
                [self setAudioName:3 channelNum:2 channelName:[txtLabelName.text UTF8String]];
                break;
            case GROUP_4:
                [userDefaults setObject:txtLabelName.text forKey:@"Gp4"];
                [self setAudioName:3 channelNum:3 channelName:[txtLabelName.text UTF8String]];
                break;
            default:
                break;
        }
    }
    else if (eqDynDelayController && !eqDynDelayController.view.hidden) {
        // Update name to original text field
        if (eqDynDelayController.currentChannelLabel == eqDynDelayController.currentChannel) {
            [eqDynDelayController.txtCurrentChannel setText:txtLabelName.text];
        }
        
        // Save to user defaults
        switch (eqDynDelayController.currentChannelLabel) {
            case CH_1:
                [userDefaults setObject:txtLabelName.text forKey:@"Ch1"];
                [self setAudioName:1 channelNum:0 channelName:[txtLabelName.text UTF8String]];
                break;
            case CH_2:
                [userDefaults setObject:txtLabelName.text forKey:@"Ch2"];
                [self setAudioName:1 channelNum:1 channelName:[txtLabelName.text UTF8String]];
                break;
            case CH_3:
                [userDefaults setObject:txtLabelName.text forKey:@"Ch3"];
                [self setAudioName:1 channelNum:2 channelName:[txtLabelName.text UTF8String]];
                break;
            case CH_4:
                [userDefaults setObject:txtLabelName.text forKey:@"Ch4"];
                [self setAudioName:1 channelNum:3 channelName:[txtLabelName.text UTF8String]];
                break;
            case CH_5:
                [userDefaults setObject:txtLabelName.text forKey:@"Ch5"];
                [self setAudioName:1 channelNum:4 channelName:[txtLabelName.text UTF8String]];
                break;
            case CH_6:
                [userDefaults setObject:txtLabelName.text forKey:@"Ch6"];
                [self setAudioName:1 channelNum:5 channelName:[txtLabelName.text UTF8String]];
                break;
            case CH_7:
                [userDefaults setObject:txtLabelName.text forKey:@"Ch7"];
                [self setAudioName:1 channelNum:6 channelName:[txtLabelName.text UTF8String]];
                break;
            case CH_8:
                [userDefaults setObject:txtLabelName.text forKey:@"Ch8"];
                [self setAudioName:1 channelNum:7 channelName:[txtLabelName.text UTF8String]];
                break;
            case CH_9:
                [userDefaults setObject:txtLabelName.text forKey:@"Ch9"];
                [self setAudioName:1 channelNum:8 channelName:[txtLabelName.text UTF8String]];
                break;
            case CH_10:
                [userDefaults setObject:txtLabelName.text forKey:@"Ch10"];
                [self setAudioName:1 channelNum:9 channelName:[txtLabelName.text UTF8String]];
                break;
            case CH_11:
                [userDefaults setObject:txtLabelName.text forKey:@"Ch11"];
                [self setAudioName:1 channelNum:10 channelName:[txtLabelName.text UTF8String]];
                break;
            case CH_12:
                [userDefaults setObject:txtLabelName.text forKey:@"Ch12"];
                [self setAudioName:1 channelNum:11 channelName:[txtLabelName.text UTF8String]];
                break;
            case CH_13:
                [userDefaults setObject:txtLabelName.text forKey:@"Ch13"];
                [self setAudioName:1 channelNum:12 channelName:[txtLabelName.text UTF8String]];
                break;
            case CH_14:
                [userDefaults setObject:txtLabelName.text forKey:@"Ch14"];
                [self setAudioName:1 channelNum:13 channelName:[txtLabelName.text UTF8String]];
                break;
            case CH_15:
                [userDefaults setObject:txtLabelName.text forKey:@"Ch15"];
                [self setAudioName:1 channelNum:14 channelName:[txtLabelName.text UTF8String]];
                break;
            case CH_16:
                [userDefaults setObject:txtLabelName.text forKey:@"Ch16"];
                [self setAudioName:1 channelNum:15 channelName:[txtLabelName.text UTF8String]];
                break;
            case CH_17:
                [userDefaults setObject:txtLabelName.text forKey:@"Ch17"];
                [self setAudioName:1 channelNum:16 channelName:[txtLabelName.text UTF8String]];
                break;
            case CH_18:
                [userDefaults setObject:txtLabelName.text forKey:@"Ch18"];
                [self setAudioName:1 channelNum:17 channelName:[txtLabelName.text UTF8String]];
                break;
            case MT_1:
                [userDefaults setObject:txtLabelName.text forKey:@"Multi1"];
                [self setAudioName:4 channelNum:0 channelName:[txtLabelName.text UTF8String]];
                break;
            case MT_2:
                [userDefaults setObject:txtLabelName.text forKey:@"Multi2"];
                [self setAudioName:4 channelNum:1 channelName:[txtLabelName.text UTF8String]];
                break;
            case MT_3:
                [userDefaults setObject:txtLabelName.text forKey:@"Multi3"];
                [self setAudioName:4 channelNum:2 channelName:[txtLabelName.text UTF8String]];
                break;
            case MT_4:
                [userDefaults setObject:txtLabelName.text forKey:@"Multi4"];
                [self setAudioName:4 channelNum:3 channelName:[txtLabelName.text UTF8String]];
                break;
            case CTRL_RM:
                [userDefaults setObject:txtLabelName.text forKey:@"CtrlRm"];
                [self setAudioName:6 channelNum:0 channelName:[txtLabelName.text UTF8String]];
                break;
            case MAIN:
                [userDefaults setObject:txtLabelName.text forKey:@"Main"];
                [self setAudioName:0 channelNum:0 channelName:[txtLabelName.text UTF8String]];
                break;
            default:
                break;
        }
    }
    else if (dynController && !dynController.view.hidden) {
        // Update name to original text field
        if (dynController.currentChannelLabel == dynController.currentChannel) {
            [dynController.txtPage setText:txtLabelName.text];
        }
        
        // Save to user defaults
        switch (dynController.currentChannelLabel) {
            case CH_1:
                [userDefaults setObject:txtLabelName.text forKey:@"Ch1"];
                [self setAudioName:1 channelNum:0 channelName:[txtLabelName.text UTF8String]];
                break;
            case CH_2:
                [userDefaults setObject:txtLabelName.text forKey:@"Ch2"];
                [self setAudioName:1 channelNum:1 channelName:[txtLabelName.text UTF8String]];
                break;
            case CH_3:
                [userDefaults setObject:txtLabelName.text forKey:@"Ch3"];
                [self setAudioName:1 channelNum:2 channelName:[txtLabelName.text UTF8String]];
                break;
            case CH_4:
                [userDefaults setObject:txtLabelName.text forKey:@"Ch4"];
                [self setAudioName:1 channelNum:3 channelName:[txtLabelName.text UTF8String]];
                break;
            case CH_5:
                [userDefaults setObject:txtLabelName.text forKey:@"Ch5"];
                [self setAudioName:1 channelNum:4 channelName:[txtLabelName.text UTF8String]];
                break;
            case CH_6:
                [userDefaults setObject:txtLabelName.text forKey:@"Ch6"];
                [self setAudioName:1 channelNum:5 channelName:[txtLabelName.text UTF8String]];
                break;
            case CH_7:
                [userDefaults setObject:txtLabelName.text forKey:@"Ch7"];
                [self setAudioName:1 channelNum:6 channelName:[txtLabelName.text UTF8String]];
                break;
            case CH_8:
                [userDefaults setObject:txtLabelName.text forKey:@"Ch8"];
                [self setAudioName:1 channelNum:7 channelName:[txtLabelName.text UTF8String]];
                break;
            case CH_9:
                [userDefaults setObject:txtLabelName.text forKey:@"Ch9"];
                [self setAudioName:1 channelNum:8 channelName:[txtLabelName.text UTF8String]];
                break;
            case CH_10:
                [userDefaults setObject:txtLabelName.text forKey:@"Ch10"];
                [self setAudioName:1 channelNum:9 channelName:[txtLabelName.text UTF8String]];
                break;
            case CH_11:
                [userDefaults setObject:txtLabelName.text forKey:@"Ch11"];
                [self setAudioName:1 channelNum:10 channelName:[txtLabelName.text UTF8String]];
                break;
            case CH_12:
                [userDefaults setObject:txtLabelName.text forKey:@"Ch12"];
                [self setAudioName:1 channelNum:11 channelName:[txtLabelName.text UTF8String]];
                break;
            case CH_13:
                [userDefaults setObject:txtLabelName.text forKey:@"Ch13"];
                [self setAudioName:1 channelNum:12 channelName:[txtLabelName.text UTF8String]];
                break;
            case CH_14:
                [userDefaults setObject:txtLabelName.text forKey:@"Ch14"];
                [self setAudioName:1 channelNum:13 channelName:[txtLabelName.text UTF8String]];
                break;
            case CH_15:
                [userDefaults setObject:txtLabelName.text forKey:@"Ch15"];
                [self setAudioName:1 channelNum:14 channelName:[txtLabelName.text UTF8String]];
                break;
            case CH_16:
                [userDefaults setObject:txtLabelName.text forKey:@"Ch16"];
                [self setAudioName:1 channelNum:15 channelName:[txtLabelName.text UTF8String]];
                break;
            case CH_17:
                [userDefaults setObject:txtLabelName.text forKey:@"Ch17"];
                [self setAudioName:1 channelNum:16 channelName:[txtLabelName.text UTF8String]];
                break;
            case CH_18:
                [userDefaults setObject:txtLabelName.text forKey:@"Ch18"];
                [self setAudioName:1 channelNum:17 channelName:[txtLabelName.text UTF8String]];
                break;
            case MT_1:
                [userDefaults setObject:txtLabelName.text forKey:@"Multi1"];
                [self setAudioName:4 channelNum:0 channelName:[txtLabelName.text UTF8String]];
                break;
            case MT_2:
                [userDefaults setObject:txtLabelName.text forKey:@"Multi2"];
                [self setAudioName:4 channelNum:1 channelName:[txtLabelName.text UTF8String]];
                break;
            case MT_3:
                [userDefaults setObject:txtLabelName.text forKey:@"Multi3"];
                [self setAudioName:4 channelNum:2 channelName:[txtLabelName.text UTF8String]];
                break;
            case MT_4:
                [userDefaults setObject:txtLabelName.text forKey:@"Multi4"];
                [self setAudioName:4 channelNum:3 channelName:[txtLabelName.text UTF8String]];
                break;
            case CTRL_RM:
                [userDefaults setObject:txtLabelName.text forKey:@"CtrlRm"];
                [self setAudioName:6 channelNum:0 channelName:[txtLabelName.text UTF8String]];
                break;
            case MAIN:
                [userDefaults setObject:txtLabelName.text forKey:@"Main"];
                [self setAudioName:0 channelNum:0 channelName:[txtLabelName.text UTF8String]];
                break;
            default:
                break;
        }
    }
    else if (geqPeqController && !geqPeqController.view.hidden) {
        // Update name to original text field
        if (geqPeqController.currentChannelLabel == geqPeqController.currentChannel) {
            [geqPeqController.txtPage setText:txtLabelName.text];
        }
        
        // Save to user defaults
        switch (geqPeqController.currentChannelLabel) {
            case CHANNEL_CH_1:
                [userDefaults setObject:txtLabelName.text forKey:@"Ch1"];
                [self setAudioName:1 channelNum:0 channelName:[txtLabelName.text UTF8String]];
                break;
            case CHANNEL_CH_2:
                [userDefaults setObject:txtLabelName.text forKey:@"Ch2"];
                [self setAudioName:1 channelNum:1 channelName:[txtLabelName.text UTF8String]];
                break;
            case CHANNEL_CH_3:
                [userDefaults setObject:txtLabelName.text forKey:@"Ch3"];
                [self setAudioName:1 channelNum:2 channelName:[txtLabelName.text UTF8String]];
                break;
            case CHANNEL_CH_4:
                [userDefaults setObject:txtLabelName.text forKey:@"Ch4"];
                [self setAudioName:1 channelNum:3 channelName:[txtLabelName.text UTF8String]];
                break;
            case CHANNEL_CH_5:
                [userDefaults setObject:txtLabelName.text forKey:@"Ch5"];
                [self setAudioName:1 channelNum:4 channelName:[txtLabelName.text UTF8String]];
                break;
            case CHANNEL_CH_6:
                [userDefaults setObject:txtLabelName.text forKey:@"Ch6"];
                [self setAudioName:1 channelNum:5 channelName:[txtLabelName.text UTF8String]];
                break;
            case CHANNEL_CH_7:
                [userDefaults setObject:txtLabelName.text forKey:@"Ch7"];
                [self setAudioName:1 channelNum:6 channelName:[txtLabelName.text UTF8String]];
                break;
            case CHANNEL_CH_8:
                [userDefaults setObject:txtLabelName.text forKey:@"Ch8"];
                [self setAudioName:1 channelNum:7 channelName:[txtLabelName.text UTF8String]];
                break;
            case CHANNEL_CH_9:
                [userDefaults setObject:txtLabelName.text forKey:@"Ch9"];
                [self setAudioName:1 channelNum:8 channelName:[txtLabelName.text UTF8String]];
                break;
            case CHANNEL_CH_10:
                [userDefaults setObject:txtLabelName.text forKey:@"Ch10"];
                [self setAudioName:1 channelNum:9 channelName:[txtLabelName.text UTF8String]];
                break;
            case CHANNEL_CH_11:
                [userDefaults setObject:txtLabelName.text forKey:@"Ch11"];
                [self setAudioName:1 channelNum:10 channelName:[txtLabelName.text UTF8String]];
                break;
            case CHANNEL_CH_12:
                [userDefaults setObject:txtLabelName.text forKey:@"Ch12"];
                [self setAudioName:1 channelNum:11 channelName:[txtLabelName.text UTF8String]];
                break;
            case CHANNEL_CH_13:
                [userDefaults setObject:txtLabelName.text forKey:@"Ch13"];
                [self setAudioName:1 channelNum:12 channelName:[txtLabelName.text UTF8String]];
                break;
            case CHANNEL_CH_14:
                [userDefaults setObject:txtLabelName.text forKey:@"Ch14"];
                [self setAudioName:1 channelNum:13 channelName:[txtLabelName.text UTF8String]];
                break;
            case CHANNEL_CH_15:
                [userDefaults setObject:txtLabelName.text forKey:@"Ch15"];
                [self setAudioName:1 channelNum:14 channelName:[txtLabelName.text UTF8String]];
                break;
            case CHANNEL_CH_16:
                [userDefaults setObject:txtLabelName.text forKey:@"Ch16"];
                [self setAudioName:1 channelNum:15 channelName:[txtLabelName.text UTF8String]];
                break;
            case CHANNEL_CH_17:
                [userDefaults setObject:txtLabelName.text forKey:@"Ch17"];
                [self setAudioName:1 channelNum:16 channelName:[txtLabelName.text UTF8String]];
                break;
            case CHANNEL_CH_18:
                [userDefaults setObject:txtLabelName.text forKey:@"Ch18"];
                [self setAudioName:1 channelNum:17 channelName:[txtLabelName.text UTF8String]];
                break;
            case CHANNEL_MULTI_1:
                [userDefaults setObject:txtLabelName.text forKey:@"Multi1"];
                [self setAudioName:4 channelNum:0 channelName:[txtLabelName.text UTF8String]];
                break;
            case CHANNEL_MULTI_2:
                [userDefaults setObject:txtLabelName.text forKey:@"Multi2"];
                [self setAudioName:4 channelNum:1 channelName:[txtLabelName.text UTF8String]];
                break;
            case CHANNEL_MULTI_3:
                [userDefaults setObject:txtLabelName.text forKey:@"Multi3"];
                [self setAudioName:4 channelNum:2 channelName:[txtLabelName.text UTF8String]];
                break;
            case CHANNEL_MULTI_4:
                [userDefaults setObject:txtLabelName.text forKey:@"Multi4"];
                [self setAudioName:4 channelNum:3 channelName:[txtLabelName.text UTF8String]];
                break;
            case CHANNEL_MAIN:
                [userDefaults setObject:txtLabelName.text forKey:@"Main"];
                [self setAudioName:0 channelNum:0 channelName:[txtLabelName.text UTF8String]];
                break;
            default:
                break;
        }
    }
    else
    {
        // Update name to original text field
        switch (currentLabel) {
            case 1:
                [txtSlider1 setText:txtLabelName.text];
                switch (currentPage) {
                    case PAGE_CH_1_8:
                        [userDefaults setObject:txtLabelName.text forKey:@"Ch1"];
                        [self setAudioName:1 channelNum:0 channelName:[txtLabelName.text UTF8String]];
                        break;
                    case PAGE_CH_9_16:
                        [userDefaults setObject:txtLabelName.text forKey:@"Ch9"];
                        [self setAudioName:1 channelNum:8 channelName:[txtLabelName.text UTF8String]];
                        break;
                    case PAGE_USB_AUDIO:
                        [userDefaults setObject:txtLabelName.text forKey:@"Ch17"];
                        [self setAudioName:1 channelNum:16 channelName:[txtLabelName.text UTF8String]];
                        break;
                    case PAGE_AUX_1_4:
                        [userDefaults setObject:txtLabelName.text forKey:@"Aux1"];
                        [self setAudioName:2 channelNum:0 channelName:[txtLabelName.text UTF8String]];
                        break;
                    case PAGE_GP_1_4:
                        [userDefaults setObject:txtLabelName.text forKey:@"Gp1"];
                        [self setAudioName:3 channelNum:0 channelName:[txtLabelName.text UTF8String]];
                        break;
                    case PAGE_MT_1_4:
                        [userDefaults setObject:txtLabelName.text forKey:@"Multi1"];
                        [self setAudioName:4 channelNum:0 channelName:[txtLabelName.text UTF8String]];
                        break;
                    default:
                        break;
                }
                break;
            case 2:
                [txtSlider2 setText:txtLabelName.text];
                switch (currentPage) {
                    case PAGE_CH_1_8:
                        [userDefaults setObject:txtLabelName.text forKey:@"Ch2"];
                        [self setAudioName:1 channelNum:1 channelName:[txtLabelName.text UTF8String]];
                        break;
                    case PAGE_CH_9_16:
                        [userDefaults setObject:txtLabelName.text forKey:@"Ch10"];
                        [self setAudioName:1 channelNum:9 channelName:[txtLabelName.text UTF8String]];
                        break;
                    case PAGE_USB_AUDIO:
                        [userDefaults setObject:txtLabelName.text forKey:@"Ch18"];
                        [self setAudioName:1 channelNum:17 channelName:[txtLabelName.text UTF8String]];
                        break;
                    case PAGE_AUX_1_4:
                        [userDefaults setObject:txtLabelName.text forKey:@"Aux2"];
                        [self setAudioName:2 channelNum:1 channelName:[txtLabelName.text UTF8String]];
                        break;
                    case PAGE_GP_1_4:
                        [userDefaults setObject:txtLabelName.text forKey:@"Gp2"];
                        [self setAudioName:3 channelNum:1 channelName:[txtLabelName.text UTF8String]];
                        break;
                    case PAGE_MT_1_4:
                        [userDefaults setObject:txtLabelName.text forKey:@"Multi2"];
                        [self setAudioName:4 channelNum:1 channelName:[txtLabelName.text UTF8String]];
                        break;
                    default:
                        break;
                }
                break;
            case 3:
                [txtSlider3 setText:txtLabelName.text];
                switch (currentPage) {
                    case PAGE_CH_1_8:
                        [userDefaults setObject:txtLabelName.text forKey:@"Ch3"];
                        [self setAudioName:1 channelNum:2 channelName:[txtLabelName.text UTF8String]];
                        break;
                    case PAGE_CH_9_16:
                        [userDefaults setObject:txtLabelName.text forKey:@"Ch11"];
                        [self setAudioName:1 channelNum:10 channelName:[txtLabelName.text UTF8String]];
                        break;
                    case PAGE_AUX_1_4:
                        [userDefaults setObject:txtLabelName.text forKey:@"Aux3"];
                        [self setAudioName:2 channelNum:2 channelName:[txtLabelName.text UTF8String]];
                        break;
                    case PAGE_GP_1_4:
                        [userDefaults setObject:txtLabelName.text forKey:@"Gp3"];
                        [self setAudioName:3 channelNum:2 channelName:[txtLabelName.text UTF8String]];
                        break;
                    case PAGE_MT_1_4:
                        [userDefaults setObject:txtLabelName.text forKey:@"Multi3"];
                        [self setAudioName:4 channelNum:2 channelName:[txtLabelName.text UTF8String]];
                        break;
                    default:
                        break;
                }
                break;
            case 4:
                [txtSlider4 setText:txtLabelName.text];
                switch (currentPage) {
                    case PAGE_CH_1_8:
                        [userDefaults setObject:txtLabelName.text forKey:@"Ch4"];
                        [self setAudioName:1 channelNum:3 channelName:[txtLabelName.text UTF8String]];
                        break;
                    case PAGE_CH_9_16:
                        [userDefaults setObject:txtLabelName.text forKey:@"Ch12"];
                        [self setAudioName:1 channelNum:11 channelName:[txtLabelName.text UTF8String]];
                        break;
                    case PAGE_AUX_1_4:
                        [userDefaults setObject:txtLabelName.text forKey:@"Aux4"];
                        [self setAudioName:2 channelNum:3 channelName:[txtLabelName.text UTF8String]];
                        break;
                    case PAGE_GP_1_4:
                        [userDefaults setObject:txtLabelName.text forKey:@"Gp4"];
                        [self setAudioName:3 channelNum:3 channelName:[txtLabelName.text UTF8String]];
                        break;
                    case PAGE_MT_1_4:
                        [userDefaults setObject:txtLabelName.text forKey:@"Multi4"];
                        [self setAudioName:4 channelNum:3 channelName:[txtLabelName.text UTF8String]];
                        break;
                    default:
                        break;
                }
                break;
            case 5:
                [txtSlider5 setText:txtLabelName.text];
                switch (currentPage) {
                    case PAGE_CH_1_8:
                        [userDefaults setObject:txtLabelName.text forKey:@"Ch5"];
                        [self setAudioName:1 channelNum:4 channelName:[txtLabelName.text UTF8String]];
                        break;
                    case PAGE_CH_9_16:
                        [userDefaults setObject:txtLabelName.text forKey:@"Ch13"];
                        [self setAudioName:1 channelNum:12 channelName:[txtLabelName.text UTF8String]];
                        break;
                    default:
                        break;
                }
                break;
            case 6:
                [txtSlider6 setText:txtLabelName.text];
                switch (currentPage) {
                    case PAGE_CH_1_8:
                        [userDefaults setObject:txtLabelName.text forKey:@"Ch6"];
                        [self setAudioName:1 channelNum:5 channelName:[txtLabelName.text UTF8String]];
                        break;
                    case PAGE_CH_9_16:
                        [userDefaults setObject:txtLabelName.text forKey:@"Ch14"];
                        [self setAudioName:1 channelNum:13 channelName:[txtLabelName.text UTF8String]];
                        break;
                    default:
                        break;
                }
                break;
            case 7:
                [txtSlider7 setText:txtLabelName.text];
                switch (currentPage) {
                    case PAGE_CH_1_8:
                        [userDefaults setObject:txtLabelName.text forKey:@"Ch7"];
                        [self setAudioName:1 channelNum:6 channelName:[txtLabelName.text UTF8String]];
                        break;
                    case PAGE_CH_9_16:
                        [userDefaults setObject:txtLabelName.text forKey:@"Ch15"];
                        [self setAudioName:1 channelNum:14 channelName:[txtLabelName.text UTF8String]];
                        break;
                    default:
                        break;
                }
                break;
            case 8:
                [txtSlider8 setText:txtLabelName.text];
                switch (currentPage) {
                    case PAGE_CH_1_8:
                        [userDefaults setObject:txtLabelName.text forKey:@"Ch8"];
                        [self setAudioName:1 channelNum:7 channelName:[txtLabelName.text UTF8String]];
                        break;
                    case PAGE_CH_9_16:
                        [userDefaults setObject:txtLabelName.text forKey:@"Ch16"];
                        [self setAudioName:1 channelNum:15 channelName:[txtLabelName.text UTF8String]];
                        break;
                    case PAGE_CTRL_RM:
                    case PAGE_MAIN:
                        [userDefaults setObject:txtLabelName.text forKey:@"CtrlRm"];
                        [self setAudioName:6 channelNum:0 channelName:[txtLabelName.text UTF8String]];
                        break;
                    default:
                        break;
                }
                break;
            case 9:
                [txtSliderMain setText:txtLabelName.text];
                switch (currentPage) {
                    case PAGE_EFX_1:
                        [userDefaults setObject:txtLabelName.text forKey:@"Efx1"];
                        [self setAudioName:5 channelNum:0 channelName:[txtLabelName.text UTF8String]];
                        break;
                    case PAGE_EFX_2:
                        [userDefaults setObject:txtLabelName.text forKey:@"Efx2"];
                        [self setAudioName:5 channelNum:1 channelName:[txtLabelName.text UTF8String]];
                        break;
                    default:
                        [userDefaults setObject:txtLabelName.text forKey:@"Main"];
                        [self setAudioName:0 channelNum:0 channelName:[txtLabelName.text UTF8String]];
                        break;
                }
                break;
            default:
                break;
        }
    }
    
    [userDefaults synchronize];
}

- (void)enableCtrlRmButtons:(BOOL)enable {
    if (!enable) {
        if (currentPage == PAGE_MT_1_4)
            [btnSlider8Solo setHidden:YES];
        else
            [btnSlider8Solo setHidden:NO];
        [btnSlider8Solo setTitle:@"SOLO" forState:UIControlStateNormal];
        [btnSlider8Solo setBackgroundImage:[UIImage imageNamed:@"button-2.png"] forState:UIControlStateNormal];
        [btnSlider8Solo setTitle:@"SOLO" forState:UIControlStateSelected];
        [btnSlider8Solo setBackgroundImage:[UIImage imageNamed:@"button-4.png"] forState:UIControlStateSelected];
        
        switch (currentPage) {
            case PAGE_CH_1_8:
            case PAGE_CH_9_16:
                [btnSlider8Meter setTitle:@"METER POST" forState:UIControlStateSelected];
                [btnSlider8Meter setBackgroundImage:[UIImage imageNamed:@"button-5.png"] forState:UIControlStateSelected];
                break;
            default:
                [btnSlider8Meter setTitle:@"METER PRE" forState:UIControlStateSelected];
                [btnSlider8Meter setBackgroundImage:[UIImage imageNamed:@"button-3.png"] forState:UIControlStateSelected];
                break;
        }
    } else {
        [btnSlider8On setHidden:YES];
        [btnSlider8Solo setTitle:@"STEREO" forState:UIControlStateNormal];
        [btnSlider8Solo setBackgroundImage:[UIImage imageNamed:@"button-4.png"] forState:UIControlStateNormal];
        [btnSlider8Solo setTitle:@"MONO" forState:UIControlStateSelected];
        [btnSlider8Solo setBackgroundImage:[UIImage imageNamed:@"button-5.png"] forState:UIControlStateSelected];
        [btnSlider8Meter setTitle:@"MAIN AFL" forState:UIControlStateSelected];
        [btnSlider8Meter setBackgroundImage:[UIImage imageNamed:@"button-5.png"] forState:UIControlStateSelected];
    }
}

- (void)refreshMeterButtons
{
    switch (currentPage) {
        case PAGE_CH_1_8:
        case PAGE_CH_9_16:
        case PAGE_USB_AUDIO:
            [btnSlider8Meter setHidden:NO];
            [btnSliderMainMeter setHidden:NO];
            [btnSlider1Meter setTitle:@"METER INPUT" forState:UIControlStateNormal];
            [btnSlider1Meter setBackgroundImage:[UIImage imageNamed:@"button-6.png"] forState:UIControlStateNormal];
            [btnSlider2Meter setTitle:@"METER INPUT" forState:UIControlStateNormal];
            [btnSlider2Meter setBackgroundImage:[UIImage imageNamed:@"button-6.png"] forState:UIControlStateNormal];
            [btnSlider3Meter setTitle:@"METER INPUT" forState:UIControlStateNormal];
            [btnSlider3Meter setBackgroundImage:[UIImage imageNamed:@"button-6.png"] forState:UIControlStateNormal];
            [btnSlider4Meter setTitle:@"METER INPUT" forState:UIControlStateNormal];
            [btnSlider4Meter setBackgroundImage:[UIImage imageNamed:@"button-6.png"] forState:UIControlStateNormal];
            [btnSlider5Meter setTitle:@"METER INPUT" forState:UIControlStateNormal];
            [btnSlider5Meter setBackgroundImage:[UIImage imageNamed:@"button-6.png"] forState:UIControlStateNormal];
            [btnSlider6Meter setTitle:@"METER INPUT" forState:UIControlStateNormal];
            [btnSlider6Meter setBackgroundImage:[UIImage imageNamed:@"button-6.png"] forState:UIControlStateNormal];
            [btnSlider7Meter setTitle:@"METER INPUT" forState:UIControlStateNormal];
            [btnSlider7Meter setBackgroundImage:[UIImage imageNamed:@"button-6.png"] forState:UIControlStateNormal];
            [btnSlider8Meter setTitle:@"METER INPUT" forState:UIControlStateNormal];
            [btnSlider8Meter setBackgroundImage:[UIImage imageNamed:@"button-6.png"] forState:UIControlStateNormal];
            break;
        case PAGE_AUX_1_4:
        case PAGE_GP_1_4:
        case PAGE_MT_1_4:
            [btnSlider8Meter setHidden:NO];
            [btnSliderMainMeter setHidden:NO];
            [btnSlider1Meter setTitle:@"METER PRE" forState:UIControlStateNormal];
            [btnSlider1Meter setBackgroundImage:[UIImage imageNamed:@"button-3.png"] forState:UIControlStateNormal];
            [btnSlider2Meter setTitle:@"METER PRE" forState:UIControlStateNormal];
            [btnSlider2Meter setBackgroundImage:[UIImage imageNamed:@"button-3.png"] forState:UIControlStateNormal];
            [btnSlider3Meter setTitle:@"METER PRE" forState:UIControlStateNormal];
            [btnSlider3Meter setBackgroundImage:[UIImage imageNamed:@"button-3.png"] forState:UIControlStateNormal];
            [btnSlider4Meter setTitle:@"METER PRE" forState:UIControlStateNormal];
            [btnSlider4Meter setBackgroundImage:[UIImage imageNamed:@"button-3.png"] forState:UIControlStateNormal];
            break;
        case PAGE_EFX_1:
        case PAGE_EFX_2:
            [btnSlider8Meter setHidden:NO];
            [btnSliderMainMeter setHidden:YES];
            break;
        case PAGE_CTRL_RM:
        case PAGE_MAIN:
            [btnSlider8Meter setHidden:YES];
            [btnSliderMainMeter setHidden:NO];
            break;
        default:
            break;
    }
}

- (void)refreshPeqButtons
{
    if (geqPeqController == nil) {
        return;
    }
    
    UIImage *mainImage = [geqPeqController.peqImages objectAtIndex:CHANNEL_MAIN];
    if (mainImage != nil) {
        [btnSliderPanEqMain setImage:mainImage forState:UIControlStateNormal];
    }
    
    switch (currentPage) {
        case PAGE_CH_1_8:
            {
                UIImage *image = [geqPeqController.peqImages objectAtIndex:CHANNEL_CH_1];
                if (image != nil) {
                    [btnSliderPanEq1 setImage:image forState:UIControlStateNormal];
                }
                image = [geqPeqController.peqImages objectAtIndex:CHANNEL_CH_2];
                if (image != nil) {
                    [btnSliderPanEq2 setImage:image forState:UIControlStateNormal];
                }
                image = [geqPeqController.peqImages objectAtIndex:CHANNEL_CH_3];
                if (image != nil) {
                    [btnSliderPanEq3 setImage:image forState:UIControlStateNormal];
                }
                image = [geqPeqController.peqImages objectAtIndex:CHANNEL_CH_4];
                if (image != nil) {
                    [btnSliderPanEq4 setImage:image forState:UIControlStateNormal];
                }
                image = [geqPeqController.peqImages objectAtIndex:CHANNEL_CH_5];
                if (image != nil) {
                    [btnSliderPanEq5 setImage:image forState:UIControlStateNormal];
                }
                image = [geqPeqController.peqImages objectAtIndex:CHANNEL_CH_6];
                if (image != nil) {
                    [btnSliderPanEq6 setImage:image forState:UIControlStateNormal];
                }
                image = [geqPeqController.peqImages objectAtIndex:CHANNEL_CH_7];
                if (image != nil) {
                    [btnSliderPanEq7 setImage:image forState:UIControlStateNormal];
                }
                image = [geqPeqController.peqImages objectAtIndex:CHANNEL_CH_8];
                if (image != nil) {
                    [btnSliderPanEq8 setImage:image forState:UIControlStateNormal];
                }
            }
            break;
            
        case PAGE_CH_9_16:
            {
                UIImage *image = [geqPeqController.peqImages objectAtIndex:CHANNEL_CH_9];
                if (image != nil) {
                    [btnSliderPanEq1 setImage:image forState:UIControlStateNormal];
                }
                image = [geqPeqController.peqImages objectAtIndex:CHANNEL_CH_10];
                if (image != nil) {
                    [btnSliderPanEq2 setImage:image forState:UIControlStateNormal];
                }
                image = [geqPeqController.peqImages objectAtIndex:CHANNEL_CH_11];
                if (image != nil) {
                    [btnSliderPanEq3 setImage:image forState:UIControlStateNormal];
                }
                image = [geqPeqController.peqImages objectAtIndex:CHANNEL_CH_12];
                if (image != nil) {
                    [btnSliderPanEq4 setImage:image forState:UIControlStateNormal];
                }
                image = [geqPeqController.peqImages objectAtIndex:CHANNEL_CH_13];
                if (image != nil) {
                    [btnSliderPanEq5 setImage:image forState:UIControlStateNormal];
                }
                image = [geqPeqController.peqImages objectAtIndex:CHANNEL_CH_14];
                if (image != nil) {
                    [btnSliderPanEq6 setImage:image forState:UIControlStateNormal];
                }
                image = [geqPeqController.peqImages objectAtIndex:CHANNEL_CH_15];
                if (image != nil) {
                    [btnSliderPanEq7 setImage:image forState:UIControlStateNormal];
                }
                image = [geqPeqController.peqImages objectAtIndex:CHANNEL_CH_16];
                if (image != nil) {
                    [btnSliderPanEq8 setImage:image forState:UIControlStateNormal];
                }
            }
            break;
            
        case PAGE_USB_AUDIO:
            {
                UIImage *image = [geqPeqController.peqImages objectAtIndex:CHANNEL_CH_17];
                if (image != nil) {
                    [btnSliderPanEq1 setImage:image forState:UIControlStateNormal];
                }
                image = [geqPeqController.peqImages objectAtIndex:CHANNEL_CH_18];
                if (image != nil) {
                    [btnSliderPanEq2 setImage:image forState:UIControlStateNormal];
                }
            }
            break;
            
        case PAGE_MT_1_4:
            {
                UIImage *image = [geqPeqController.peqImages objectAtIndex:CHANNEL_MULTI_1];
                if (image != nil) {
                    [btnSliderPanEq1 setImage:image forState:UIControlStateNormal];
                }
                image = [geqPeqController.peqImages objectAtIndex:CHANNEL_MULTI_2];
                if (image != nil) {
                    [btnSliderPanEq2 setImage:image forState:UIControlStateNormal];
                }
                image = [geqPeqController.peqImages objectAtIndex:CHANNEL_MULTI_3];
                if (image != nil) {
                    [btnSliderPanEq3 setImage:image forState:UIControlStateNormal];
                }
                image = [geqPeqController.peqImages objectAtIndex:CHANNEL_MULTI_4];
                if (image != nil) {
                    [btnSliderPanEq4 setImage:image forState:UIControlStateNormal];
                }
            }
            break;
            
        default:
            [btnSliderPanEq1 setImage:[UIImage imageNamed:@"pan-2.png"] forState:UIControlStateNormal];
            [btnSliderPanEq2 setImage:[UIImage imageNamed:@"pan-2.png"] forState:UIControlStateNormal];
            [btnSliderPanEq3 setImage:[UIImage imageNamed:@"pan-2.png"] forState:UIControlStateNormal];
            [btnSliderPanEq4 setImage:[UIImage imageNamed:@"pan-2.png"] forState:UIControlStateNormal];
            [btnSliderPanEq5 setImage:[UIImage imageNamed:@"pan-2.png"] forState:UIControlStateNormal];
            [btnSliderPanEq6 setImage:[UIImage imageNamed:@"pan-2.png"] forState:UIControlStateNormal];
            [btnSliderPanEq7 setImage:[UIImage imageNamed:@"pan-2.png"] forState:UIControlStateNormal];
            [btnSliderPanEq8 setImage:[UIImage imageNamed:@"pan-2.png"] forState:UIControlStateNormal];
            break;
    }
}

- (void)refreshDynButtons
{
    if (dynController == nil) {
        return;
    }
    
    UIImage *mainImage = [dynController.dynImages objectAtIndex:CHANNEL_MAIN];
    if (mainImage != nil) {
        [btnSliderPanEqMain setImage:mainImage forState:UIControlStateNormal];
    }

    switch (currentPage) {
        case PAGE_CH_1_8:
            {
                UIImage *image = [dynController.dynImages objectAtIndex:CHANNEL_CH_1];
                if (image != nil) {
                    [btnSliderPanEq1 setImage:image forState:UIControlStateNormal];
                }
                image = [dynController.dynImages objectAtIndex:CHANNEL_CH_2];
                if (image != nil) {
                    [btnSliderPanEq2 setImage:image forState:UIControlStateNormal];
                }
                image = [dynController.dynImages objectAtIndex:CHANNEL_CH_3];
                if (image != nil) {
                    [btnSliderPanEq3 setImage:image forState:UIControlStateNormal];
                }
                image = [dynController.dynImages objectAtIndex:CHANNEL_CH_4];
                if (image != nil) {
                    [btnSliderPanEq4 setImage:image forState:UIControlStateNormal];
                }
                image = [dynController.dynImages objectAtIndex:CHANNEL_CH_5];
                if (image != nil) {
                    [btnSliderPanEq5 setImage:image forState:UIControlStateNormal];
                }
                image = [dynController.dynImages objectAtIndex:CHANNEL_CH_6];
                if (image != nil) {
                    [btnSliderPanEq6 setImage:image forState:UIControlStateNormal];
                }
                image = [dynController.dynImages objectAtIndex:CHANNEL_CH_7];
                if (image != nil) {
                    [btnSliderPanEq7 setImage:image forState:UIControlStateNormal];
                }
                image = [dynController.dynImages objectAtIndex:CHANNEL_CH_8];
                if (image != nil) {
                    [btnSliderPanEq8 setImage:image forState:UIControlStateNormal];
                }
            }
            break;
            
        case PAGE_CH_9_16:
            {
                UIImage *image = [dynController.dynImages objectAtIndex:CHANNEL_CH_9];
                if (image != nil) {
                    [btnSliderPanEq1 setImage:image forState:UIControlStateNormal];
                }
                image = [dynController.dynImages objectAtIndex:CHANNEL_CH_10];
                if (image != nil) {
                    [btnSliderPanEq2 setImage:image forState:UIControlStateNormal];
                }
                image = [dynController.dynImages objectAtIndex:CHANNEL_CH_11];
                if (image != nil) {
                    [btnSliderPanEq3 setImage:image forState:UIControlStateNormal];
                }
                image = [dynController.dynImages objectAtIndex:CHANNEL_CH_12];
                if (image != nil) {
                    [btnSliderPanEq4 setImage:image forState:UIControlStateNormal];
                }
                image = [dynController.dynImages objectAtIndex:CHANNEL_CH_13];
                if (image != nil) {
                    [btnSliderPanEq5 setImage:image forState:UIControlStateNormal];
                }
                image = [dynController.dynImages objectAtIndex:CHANNEL_CH_14];
                if (image != nil) {
                    [btnSliderPanEq6 setImage:image forState:UIControlStateNormal];
                }
                image = [dynController.dynImages objectAtIndex:CHANNEL_CH_15];
                if (image != nil) {
                    [btnSliderPanEq7 setImage:image forState:UIControlStateNormal];
                }
                image = [dynController.dynImages objectAtIndex:CHANNEL_CH_16];
                if (image != nil) {
                    [btnSliderPanEq8 setImage:image forState:UIControlStateNormal];
                }
            }
            break;
            
        case PAGE_USB_AUDIO:
            {
                UIImage *image = [dynController.dynImages objectAtIndex:CHANNEL_CH_17];
                if (image != nil) {
                    [btnSliderPanEq1 setImage:image forState:UIControlStateNormal];
                }
                image = [dynController.dynImages objectAtIndex:CHANNEL_CH_18];
                if (image != nil) {
                    [btnSliderPanEq2 setImage:image forState:UIControlStateNormal];
                }
            }
            break;
            
        case PAGE_MT_1_4:
            {
                UIImage *image = [dynController.dynImages objectAtIndex:CHANNEL_MULTI_1];
                if (image != nil) {
                    [btnSliderPanEq1 setImage:image forState:UIControlStateNormal];
                }
                image = [dynController.dynImages objectAtIndex:CHANNEL_MULTI_2];
                if (image != nil) {
                    [btnSliderPanEq2 setImage:image forState:UIControlStateNormal];
                }
                image = [dynController.dynImages objectAtIndex:CHANNEL_MULTI_3];
                if (image != nil) {
                    [btnSliderPanEq3 setImage:image forState:UIControlStateNormal];
                }
                image = [dynController.dynImages objectAtIndex:CHANNEL_MULTI_4];
                if (image != nil) {
                    [btnSliderPanEq4 setImage:image forState:UIControlStateNormal];
                }
            }
            break;
            
        default:
            [btnSliderPanEq1 setImage:[UIImage imageNamed:@"pan-8.png"] forState:UIControlStateNormal];
            [btnSliderPanEq2 setImage:[UIImage imageNamed:@"pan-8.png"] forState:UIControlStateNormal];
            [btnSliderPanEq3 setImage:[UIImage imageNamed:@"pan-8.png"] forState:UIControlStateNormal];
            [btnSliderPanEq4 setImage:[UIImage imageNamed:@"pan-8.png"] forState:UIControlStateNormal];
            [btnSliderPanEq5 setImage:[UIImage imageNamed:@"pan-8.png"] forState:UIControlStateNormal];
            [btnSliderPanEq6 setImage:[UIImage imageNamed:@"pan-8.png"] forState:UIControlStateNormal];
            [btnSliderPanEq7 setImage:[UIImage imageNamed:@"pan-8.png"] forState:UIControlStateNormal];
            [btnSliderPanEq8 setImage:[UIImage imageNamed:@"pan-8.png"] forState:UIControlStateNormal];
            break;
    }
}

- (void)refreshModeLabel
{
    switch (currentMode) {
        case MODE_PHANTOM_POWER:
            [lblCurrentMode setText:@"PHANTOM POWER"];
            //[self setSendPage:DELAY_48V_PAGE reserve2:0];
            break;
        case MODE_DELAY:
            [lblCurrentMode setText:@"DELAY"];
            //[self setSendPage:DELAY_48V_PAGE reserve2:0];
            break;
        case MODE_DYN:
            [lblCurrentMode setText:@"DYN"];
            //[self setSendPage:DYN_PAGE reserve2:0];
            break;
        case MODE_EQ:
            [lblCurrentMode setText:@"EQ"];
            //[self setSendPage:EQ_PAGE reserve2:0];
            break;
        case MODE_INV:
            [lblCurrentMode setText:@"INV"];
            //[self setSendPage:INV_PAGE reserve2:0];
            break;
        case MODE_ORDER:
            [lblCurrentMode setText:@"ORDER"];
            //[self setSendPage:ORDER_PAGE reserve2:0];
            break;
        case MODE_PAN:
            [lblCurrentMode setText:@"PAN"];
            //[self setSendPage:PAN_PAGE reserve2:0];
            break;
        default:
            break;
    }
}

- (void)refreshPanValues
{
    if (sliderPan1.value >= 0.0 && sliderPan1.value < PAN_CENTER_VALUE) {
        [meterPan1 setHidden:NO];
        [meterPan1 setTransform:CGAffineTransformMake(-1, 0, 0, 1, 0, 0)];
        [meterPan1 setProgress:0.0 - sliderPan1.value/PAN_MAX_VALUE];
    } else if (sliderPan1.value > PAN_CENTER_VALUE && sliderPan1.value <= PAN_MAX_VALUE) {
        [meterPan1 setHidden:NO];
        [meterPan1 setTransform:CGAffineTransformMake(1, 0, 0, 1, 0, 0)];
        [meterPan1 setProgress:sliderPan1.value/PAN_MAX_VALUE];
    } else {
        [meterPan1 setHidden:YES];
        [btnSliderPanEq1 setBackgroundImage:[UIImage imageNamed:@"pan-2.png"] forState:UIControlStateNormal];
    }
    [lblPanValue1 setText:[Global getPanString:sliderPan1.value]];
    
    if (sliderPan2.value >= 0.0 && sliderPan2.value < PAN_CENTER_VALUE) {
        [meterPan2 setHidden:NO];
        [meterPan2 setTransform:CGAffineTransformMake(-1, 0, 0, 1, 0, 0)];
        [meterPan2 setProgress:0.0 - sliderPan2.value/PAN_MAX_VALUE];
    } else if (sliderPan2.value > PAN_CENTER_VALUE && sliderPan2.value <= PAN_MAX_VALUE) {
        [meterPan2 setHidden:NO];
        [meterPan2 setTransform:CGAffineTransformMake(1, 0, 0, 1, 0, 0)];
        [meterPan2 setProgress:sliderPan2.value/PAN_MAX_VALUE];
    } else {
        [meterPan2 setHidden:YES];
        [btnSliderPanEq2 setBackgroundImage:[UIImage imageNamed:@"pan-2.png"] forState:UIControlStateNormal];
    }
    [lblPanValue2 setText:[Global getPanString:sliderPan2.value]];
    
    if (sliderPan3.value >= 0.0 && sliderPan3.value < PAN_CENTER_VALUE) {
        [meterPan3 setHidden:NO];
        [meterPan3 setTransform:CGAffineTransformMake(-1, 0, 0, 1, 0, 0)];
        [meterPan3 setProgress:0.0 - sliderPan3.value/PAN_MAX_VALUE];
    } else if (sliderPan3.value > PAN_CENTER_VALUE && sliderPan3.value <= PAN_MAX_VALUE) {
        [meterPan3 setHidden:NO];
        [meterPan3 setTransform:CGAffineTransformMake(1, 0, 0, 1, 0, 0)];
        [meterPan3 setProgress:sliderPan3.value/PAN_MAX_VALUE];
    } else {
        [meterPan3 setHidden:YES];
        [btnSliderPanEq3 setBackgroundImage:[UIImage imageNamed:@"pan-2.png"] forState:UIControlStateNormal];
    }
    [lblPanValue3 setText:[Global getPanString:sliderPan3.value]];
    
    if (sliderPan4.value >= 0.0 && sliderPan4.value < PAN_CENTER_VALUE) {
        [meterPan4 setHidden:NO];
        [meterPan4 setTransform:CGAffineTransformMake(-1, 0, 0, 1, 0, 0)];
        [meterPan4 setProgress:0.0 - sliderPan4.value/PAN_MAX_VALUE];
    } else if (sliderPan4.value > PAN_CENTER_VALUE && sliderPan4.value <= PAN_MAX_VALUE) {
        [meterPan4 setHidden:NO];
        [meterPan4 setTransform:CGAffineTransformMake(1, 0, 0, 1, 0, 0)];
        [meterPan4 setProgress:sliderPan4.value/PAN_MAX_VALUE];
    } else {
        [meterPan4 setHidden:YES];
        [btnSliderPanEq4 setBackgroundImage:[UIImage imageNamed:@"pan-2.png"] forState:UIControlStateNormal];
    }
    [lblPanValue4 setText:[Global getPanString:sliderPan4.value]];
    
    if (sliderPan5.value >= 0.0 && sliderPan5.value < PAN_CENTER_VALUE) {
        [meterPan5 setHidden:NO];
        [meterPan5 setTransform:CGAffineTransformMake(-1, 0, 0, 1, 0, 0)];
        [meterPan5 setProgress:0.0 - sliderPan5.value/PAN_MAX_VALUE];
    } else if (sliderPan5.value > PAN_CENTER_VALUE && sliderPan5.value <= PAN_MAX_VALUE) {
        [meterPan5 setHidden:NO];
        [meterPan5 setTransform:CGAffineTransformMake(1, 0, 0, 1, 0, 0)];
        [meterPan5 setProgress:sliderPan5.value/PAN_MAX_VALUE];
    } else {
        [meterPan5 setHidden:YES];
        [btnSliderPanEq5 setBackgroundImage:[UIImage imageNamed:@"pan-2.png"] forState:UIControlStateNormal];
    }
    [lblPanValue5 setText:[Global getPanString:sliderPan5.value]];
    
    if (sliderPan6.value >= 0.0 && sliderPan6.value < PAN_CENTER_VALUE) {
        [meterPan6 setHidden:NO];
        [meterPan6 setTransform:CGAffineTransformMake(-1, 0, 0, 1, 0, 0)];
        [meterPan6 setProgress:0.0 - sliderPan6.value/PAN_MAX_VALUE];
    } else if (sliderPan6.value > PAN_CENTER_VALUE && sliderPan6.value <= PAN_MAX_VALUE) {
        [meterPan6 setHidden:NO];
        [meterPan6 setTransform:CGAffineTransformMake(1, 0, 0, 1, 0, 0)];
        [meterPan6 setProgress:sliderPan6.value/PAN_MAX_VALUE];
    } else {
        [meterPan6 setHidden:YES];
        [btnSliderPanEq6 setBackgroundImage:[UIImage imageNamed:@"pan-2.png"] forState:UIControlStateNormal];
    }
    [lblPanValue6 setText:[Global getPanString:sliderPan6.value]];
    
    if (sliderPan7.value >= 0.0 && sliderPan7.value < PAN_CENTER_VALUE) {
        [meterPan7 setHidden:NO];
        [meterPan7 setTransform:CGAffineTransformMake(-1, 0, 0, 1, 0, 0)];
        [meterPan7 setProgress:0.0 - sliderPan7.value/PAN_MAX_VALUE];
    } else if (sliderPan7.value > PAN_CENTER_VALUE && sliderPan7.value <= PAN_MAX_VALUE) {
        [meterPan7 setHidden:NO];
        [meterPan7 setTransform:CGAffineTransformMake(1, 0, 0, 1, 0, 0)];
        [meterPan7 setProgress:sliderPan7.value/PAN_MAX_VALUE];
    } else {
        [meterPan7 setHidden:YES];
        [btnSliderPanEq7 setBackgroundImage:[UIImage imageNamed:@"pan-2.png"] forState:UIControlStateNormal];
    }
    [lblPanValue7 setText:[Global getPanString:sliderPan7.value]];
    
    if (sliderPan8.value >= 0.0 && sliderPan8.value < PAN_CENTER_VALUE) {
        [meterPan8 setHidden:NO];
        [meterPan8 setTransform:CGAffineTransformMake(-1, 0, 0, 1, 0, 0)];
        [meterPan8 setProgress:0.0 - sliderPan8.value/PAN_MAX_VALUE];
    } else if (sliderPan8.value > PAN_CENTER_VALUE && sliderPan8.value <= PAN_MAX_VALUE) {
        [meterPan8 setHidden:NO];
        [meterPan8 setTransform:CGAffineTransformMake(1, 0, 0, 1, 0, 0)];
        [meterPan8 setProgress:sliderPan8.value/PAN_MAX_VALUE];
    } else {
        [meterPan8 setHidden:YES];
        [btnSliderPanEq8 setBackgroundImage:[UIImage imageNamed:@"pan-2.png"] forState:UIControlStateNormal];
    }
    [lblPanValue8 setText:[Global getPanString:sliderPan8.value]];
}

- (void)clearPanButtonImages
{
    [btnSliderPanEq1 setImage:nil forState:UIControlStateNormal];
    [btnSliderPanEq2 setImage:nil forState:UIControlStateNormal];
    [btnSliderPanEq3 setImage:nil forState:UIControlStateNormal];
    [btnSliderPanEq4 setImage:nil forState:UIControlStateNormal];
    [btnSliderPanEq5 setImage:nil forState:UIControlStateNormal];
    [btnSliderPanEq6 setImage:nil forState:UIControlStateNormal];
    [btnSliderPanEq7 setImage:nil forState:UIControlStateNormal];
    [btnSliderPanEq8 setImage:nil forState:UIControlStateNormal];
    [btnSliderPanEqMain setImage:nil forState:UIControlStateNormal];
}

- (void)updateInvButtons
{
    switch (currentPage) {
        case PAGE_CH_1_8:
            if (invOffOn[CH_1] > 0) {
                [btnSliderPanEq1 setBackgroundImage:[UIImage imageNamed:@"pan-15.png"] forState:UIControlStateNormal];
            } else {
                [btnSliderPanEq1 setBackgroundImage:[UIImage imageNamed:@"pan-14.png"] forState:UIControlStateNormal];
            }
            if (invOffOn[CH_2] > 0) {
                [btnSliderPanEq2 setBackgroundImage:[UIImage imageNamed:@"pan-15.png"] forState:UIControlStateNormal];
            } else {
                [btnSliderPanEq2 setBackgroundImage:[UIImage imageNamed:@"pan-14.png"] forState:UIControlStateNormal];
            }
            if (invOffOn[CH_3] > 0) {
                [btnSliderPanEq3 setBackgroundImage:[UIImage imageNamed:@"pan-15.png"] forState:UIControlStateNormal];
            } else {
                [btnSliderPanEq3 setBackgroundImage:[UIImage imageNamed:@"pan-14.png"] forState:UIControlStateNormal];
            }
            if (invOffOn[CH_4] > 0) {
                [btnSliderPanEq4 setBackgroundImage:[UIImage imageNamed:@"pan-15.png"] forState:UIControlStateNormal];
            } else {
                [btnSliderPanEq4 setBackgroundImage:[UIImage imageNamed:@"pan-14.png"] forState:UIControlStateNormal];
            }
            if (invOffOn[CH_5] > 0) {
                [btnSliderPanEq5 setBackgroundImage:[UIImage imageNamed:@"pan-15.png"] forState:UIControlStateNormal];
            } else {
                [btnSliderPanEq5 setBackgroundImage:[UIImage imageNamed:@"pan-14.png"] forState:UIControlStateNormal];
            }
            if (invOffOn[CH_6] > 0) {
                [btnSliderPanEq6 setBackgroundImage:[UIImage imageNamed:@"pan-15.png"] forState:UIControlStateNormal];
            } else {
                [btnSliderPanEq6 setBackgroundImage:[UIImage imageNamed:@"pan-14.png"] forState:UIControlStateNormal];
            }
            if (invOffOn[CH_7] > 0) {
                [btnSliderPanEq7 setBackgroundImage:[UIImage imageNamed:@"pan-15.png"] forState:UIControlStateNormal];
            } else {
                [btnSliderPanEq7 setBackgroundImage:[UIImage imageNamed:@"pan-14.png"] forState:UIControlStateNormal];
            }
            if (invOffOn[CH_8] > 0) {
                [btnSliderPanEq8 setBackgroundImage:[UIImage imageNamed:@"pan-15.png"] forState:UIControlStateNormal];
            } else {
                [btnSliderPanEq8 setBackgroundImage:[UIImage imageNamed:@"pan-14.png"] forState:UIControlStateNormal];
            }
            break;
        case PAGE_CH_9_16:
            if (invOffOn[CH_9] > 0) {
                [btnSliderPanEq1 setBackgroundImage:[UIImage imageNamed:@"pan-15.png"] forState:UIControlStateNormal];
            } else {
                [btnSliderPanEq1 setBackgroundImage:[UIImage imageNamed:@"pan-14.png"] forState:UIControlStateNormal];
            }
            if (invOffOn[CH_10] > 0) {
                [btnSliderPanEq2 setBackgroundImage:[UIImage imageNamed:@"pan-15.png"] forState:UIControlStateNormal];
            } else {
                [btnSliderPanEq2 setBackgroundImage:[UIImage imageNamed:@"pan-14.png"] forState:UIControlStateNormal];
            }
            if (invOffOn[CH_11] > 0) {
                [btnSliderPanEq3 setBackgroundImage:[UIImage imageNamed:@"pan-15.png"] forState:UIControlStateNormal];
            } else {
                [btnSliderPanEq3 setBackgroundImage:[UIImage imageNamed:@"pan-14.png"] forState:UIControlStateNormal];
            }
            if (invOffOn[CH_12] > 0) {
                [btnSliderPanEq4 setBackgroundImage:[UIImage imageNamed:@"pan-15.png"] forState:UIControlStateNormal];
            } else {
                [btnSliderPanEq4 setBackgroundImage:[UIImage imageNamed:@"pan-14.png"] forState:UIControlStateNormal];
            }
            if (invOffOn[CH_13] > 0) {
                [btnSliderPanEq5 setBackgroundImage:[UIImage imageNamed:@"pan-15.png"] forState:UIControlStateNormal];
            } else {
                [btnSliderPanEq5 setBackgroundImage:[UIImage imageNamed:@"pan-14.png"] forState:UIControlStateNormal];
            }
            if (invOffOn[CH_14] > 0) {
                [btnSliderPanEq6 setBackgroundImage:[UIImage imageNamed:@"pan-15.png"] forState:UIControlStateNormal];
            } else {
                [btnSliderPanEq6 setBackgroundImage:[UIImage imageNamed:@"pan-14.png"] forState:UIControlStateNormal];
            }
            if (invOffOn[CH_15] > 0) {
                [btnSliderPanEq7 setBackgroundImage:[UIImage imageNamed:@"pan-15.png"] forState:UIControlStateNormal];
            } else {
                [btnSliderPanEq7 setBackgroundImage:[UIImage imageNamed:@"pan-14.png"] forState:UIControlStateNormal];
            }
            if (invOffOn[CH_16] > 0) {
                [btnSliderPanEq8 setBackgroundImage:[UIImage imageNamed:@"pan-15.png"] forState:UIControlStateNormal];
            } else {
                [btnSliderPanEq8 setBackgroundImage:[UIImage imageNamed:@"pan-14.png"] forState:UIControlStateNormal];
            }
            break;
        case PAGE_USB_AUDIO:
            if (invOffOn[CH_17] > 0) {
                [btnSliderPanEq1 setBackgroundImage:[UIImage imageNamed:@"pan-15.png"] forState:UIControlStateNormal];
            } else {
                [btnSliderPanEq1 setBackgroundImage:[UIImage imageNamed:@"pan-14.png"] forState:UIControlStateNormal];
            }
            if (invOffOn[CH_18] > 0) {
                [btnSliderPanEq2 setBackgroundImage:[UIImage imageNamed:@"pan-15.png"] forState:UIControlStateNormal];
            } else {
                [btnSliderPanEq2 setBackgroundImage:[UIImage imageNamed:@"pan-14.png"] forState:UIControlStateNormal];
            }
            break;
        default:
            break;
    }
}

- (void)updateDelayButtons
{
    if (btnDelayMainOnOff.selected) {
        [btnSliderPanEqMain setBackgroundImage:[UIImage imageNamed:@"pan-17.png"] forState:UIControlStateNormal];
    } else {
        [btnSliderPanEqMain setBackgroundImage:[UIImage imageNamed:@"pan-16.png"] forState:UIControlStateNormal];
    }
    [lblDelayMain setText:lblDelayMainValue.text];
    
    switch (currentPage) {
        case PAGE_CH_1_8:
            if (btnDelayCh1OnOff.selected) {
                [btnSliderPanEq1 setBackgroundImage:[UIImage imageNamed:@"pan-17.png"] forState:UIControlStateNormal];
            } else {
                [btnSliderPanEq1 setBackgroundImage:[UIImage imageNamed:@"pan-16.png"] forState:UIControlStateNormal];
            }
            if (btnDelayCh2OnOff.selected) {
                [btnSliderPanEq2 setBackgroundImage:[UIImage imageNamed:@"pan-17.png"] forState:UIControlStateNormal];
            } else {
                [btnSliderPanEq2 setBackgroundImage:[UIImage imageNamed:@"pan-16.png"] forState:UIControlStateNormal];
            }
            if (btnDelayCh3OnOff.selected) {
                [btnSliderPanEq3 setBackgroundImage:[UIImage imageNamed:@"pan-17.png"] forState:UIControlStateNormal];
            } else {
                [btnSliderPanEq3 setBackgroundImage:[UIImage imageNamed:@"pan-16.png"] forState:UIControlStateNormal];
            }
            if (btnDelayCh4OnOff.selected) {
                [btnSliderPanEq4 setBackgroundImage:[UIImage imageNamed:@"pan-17.png"] forState:UIControlStateNormal];
            } else {
                [btnSliderPanEq4 setBackgroundImage:[UIImage imageNamed:@"pan-16.png"] forState:UIControlStateNormal];
            }
            if (btnDelayCh5OnOff.selected) {
                [btnSliderPanEq5 setBackgroundImage:[UIImage imageNamed:@"pan-17.png"] forState:UIControlStateNormal];
            } else {
                [btnSliderPanEq5 setBackgroundImage:[UIImage imageNamed:@"pan-16.png"] forState:UIControlStateNormal];
            }
            if (btnDelayCh6OnOff.selected) {
                [btnSliderPanEq6 setBackgroundImage:[UIImage imageNamed:@"pan-17.png"] forState:UIControlStateNormal];
            } else {
                [btnSliderPanEq6 setBackgroundImage:[UIImage imageNamed:@"pan-16.png"] forState:UIControlStateNormal];
            }
            if (btnDelayCh7OnOff.selected) {
                [btnSliderPanEq7 setBackgroundImage:[UIImage imageNamed:@"pan-17.png"] forState:UIControlStateNormal];
            } else {
                [btnSliderPanEq7 setBackgroundImage:[UIImage imageNamed:@"pan-16.png"] forState:UIControlStateNormal];
            }
            if (btnDelayCh8OnOff.selected) {
                [btnSliderPanEq8 setBackgroundImage:[UIImage imageNamed:@"pan-17.png"] forState:UIControlStateNormal];
            } else {
                [btnSliderPanEq8 setBackgroundImage:[UIImage imageNamed:@"pan-16.png"] forState:UIControlStateNormal];
            }            
            [lblDelay1 setText:lblDelayCh1.text];
            [lblDelay2 setText:lblDelayCh2.text];
            [lblDelay3 setText:lblDelayCh3.text];
            [lblDelay4 setText:lblDelayCh4.text];
            [lblDelay5 setText:lblDelayCh5.text];
            [lblDelay6 setText:lblDelayCh6.text];
            [lblDelay7 setText:lblDelayCh7.text];
            [lblDelay8 setText:lblDelayCh8.text];
            break;
        case PAGE_CH_9_16:
            if (btnDelayCh9OnOff.selected) {
                [btnSliderPanEq1 setBackgroundImage:[UIImage imageNamed:@"pan-17.png"] forState:UIControlStateNormal];
            } else {
                [btnSliderPanEq1 setBackgroundImage:[UIImage imageNamed:@"pan-16.png"] forState:UIControlStateNormal];
            }
            if (btnDelayCh10OnOff.selected) {
                [btnSliderPanEq2 setBackgroundImage:[UIImage imageNamed:@"pan-17.png"] forState:UIControlStateNormal];
            } else {
                [btnSliderPanEq2 setBackgroundImage:[UIImage imageNamed:@"pan-16.png"] forState:UIControlStateNormal];
            }
            if (btnDelayCh11OnOff.selected) {
                [btnSliderPanEq3 setBackgroundImage:[UIImage imageNamed:@"pan-17.png"] forState:UIControlStateNormal];
            } else {
                [btnSliderPanEq3 setBackgroundImage:[UIImage imageNamed:@"pan-16.png"] forState:UIControlStateNormal];
            }
            if (btnDelayCh12OnOff.selected) {
                [btnSliderPanEq4 setBackgroundImage:[UIImage imageNamed:@"pan-17.png"] forState:UIControlStateNormal];
            } else {
                [btnSliderPanEq4 setBackgroundImage:[UIImage imageNamed:@"pan-16.png"] forState:UIControlStateNormal];
            }
            if (btnDelayCh13OnOff.selected) {
                [btnSliderPanEq5 setBackgroundImage:[UIImage imageNamed:@"pan-17.png"] forState:UIControlStateNormal];
            } else {
                [btnSliderPanEq5 setBackgroundImage:[UIImage imageNamed:@"pan-16.png"] forState:UIControlStateNormal];
            }
            if (btnDelayCh14OnOff.selected) {
                [btnSliderPanEq6 setBackgroundImage:[UIImage imageNamed:@"pan-17.png"] forState:UIControlStateNormal];
            } else {
                [btnSliderPanEq6 setBackgroundImage:[UIImage imageNamed:@"pan-16.png"] forState:UIControlStateNormal];
            }
            if (btnDelayCh15OnOff.selected) {
                [btnSliderPanEq7 setBackgroundImage:[UIImage imageNamed:@"pan-17.png"] forState:UIControlStateNormal];
            } else {
                [btnSliderPanEq7 setBackgroundImage:[UIImage imageNamed:@"pan-16.png"] forState:UIControlStateNormal];
            }
            if (btnDelayCh16OnOff.selected) {
                [btnSliderPanEq8 setBackgroundImage:[UIImage imageNamed:@"pan-17.png"] forState:UIControlStateNormal];
            } else {
                [btnSliderPanEq8 setBackgroundImage:[UIImage imageNamed:@"pan-16.png"] forState:UIControlStateNormal];
            }
            [lblDelay1 setText:lblDelayCh9.text];
            [lblDelay2 setText:lblDelayCh10.text];
            [lblDelay3 setText:lblDelayCh11.text];
            [lblDelay4 setText:lblDelayCh12.text];
            [lblDelay5 setText:lblDelayCh13.text];
            [lblDelay6 setText:lblDelayCh14.text];
            [lblDelay7 setText:lblDelayCh15.text];
            [lblDelay8 setText:lblDelayCh16.text];
            break;
        case PAGE_USB_AUDIO:
            if (btnDelayCh17OnOff.selected) {
                [btnSliderPanEq1 setBackgroundImage:[UIImage imageNamed:@"pan-17.png"] forState:UIControlStateNormal];
            } else {
                [btnSliderPanEq1 setBackgroundImage:[UIImage imageNamed:@"pan-16.png"] forState:UIControlStateNormal];
            }
            if (btnDelayCh18OnOff.selected) {
                [btnSliderPanEq2 setBackgroundImage:[UIImage imageNamed:@"pan-17.png"] forState:UIControlStateNormal];
            } else {
                [btnSliderPanEq2 setBackgroundImage:[UIImage imageNamed:@"pan-16.png"] forState:UIControlStateNormal];
            }
            [lblDelay1 setText:lblDelayCh17.text];
            [lblDelay2 setText:lblDelayCh18.text];
            break;
        case PAGE_MT_1_4:
            if (btnDelayMt1OnOff.selected) {
                [btnSliderPanEq1 setBackgroundImage:[UIImage imageNamed:@"pan-17.png"] forState:UIControlStateNormal];
            } else {
                [btnSliderPanEq1 setBackgroundImage:[UIImage imageNamed:@"pan-16.png"] forState:UIControlStateNormal];
            }
            if (btnDelayMt2OnOff.selected) {
                [btnSliderPanEq2 setBackgroundImage:[UIImage imageNamed:@"pan-17.png"] forState:UIControlStateNormal];
            } else {
                [btnSliderPanEq2 setBackgroundImage:[UIImage imageNamed:@"pan-16.png"] forState:UIControlStateNormal];
            }
            if (btnDelayMt3OnOff.selected) {
                [btnSliderPanEq3 setBackgroundImage:[UIImage imageNamed:@"pan-17.png"] forState:UIControlStateNormal];
            } else {
                [btnSliderPanEq3 setBackgroundImage:[UIImage imageNamed:@"pan-16.png"] forState:UIControlStateNormal];
            }
            if (btnDelayMt4OnOff.selected) {
                [btnSliderPanEq4 setBackgroundImage:[UIImage imageNamed:@"pan-17.png"] forState:UIControlStateNormal];
            } else {
                [btnSliderPanEq4 setBackgroundImage:[UIImage imageNamed:@"pan-16.png"] forState:UIControlStateNormal];
            }
            [lblDelay1 setText:lblDelayMt1.text];
            [lblDelay2 setText:lblDelayMt2.text];
            [lblDelay3 setText:lblDelayMt3.text];
            [lblDelay4 setText:lblDelayMt4.text];
            break;
        default:
            break;
    }
}

- (void)updateOrderButtons
{
    if (orderMain == ORDER_MAIN_EQ_DYN_DELAY) {
        [lblOrderFirstMain setText:@"EQ"];
        [lblOrderSecondMain setText:@"DYN"];
        [lblOrderThirdMain setText:@"DELAY"];
    } else if (orderMain == ORDER_MAIN_DYN_EQ_DELAY) {
        [lblOrderFirstMain setText:@"DYN"];
        [lblOrderSecondMain setText:@"EQ"];
        [lblOrderThirdMain setText:@"DELAY"];
    } else if (orderMain == ORDER_MAIN_DYN_GEQ_DELAY) {
        [lblOrderFirstMain setText:@"DYN"];
        [lblOrderSecondMain setText:@"GEQ"];
        [lblOrderThirdMain setText:@"DELAY"];
    } else {
        [lblOrderFirstMain setText:@"GEQ"];
        [lblOrderSecondMain setText:@"DYN"];
        [lblOrderThirdMain setText:@"DELAY"];        
    }
    
    switch (currentPage) {
        case PAGE_CH_1_8:
            {
                enum OrderChannel orderCh1 = orderChannel[CH_1];
                if (orderCh1 == ORDER_CH_EQ_DYN_DELAY) {
                    [lblOrderFirst1 setText:@"EQ"];
                    [lblOrderSecond1 setText:@"DYN"];
                    [lblOrderThird1 setText:@"DELAY"];
                } else if (orderCh1 == ORDER_CH_EQ_DELAY_DYN) {
                    [lblOrderFirst1 setText:@"EQ"];
                    [lblOrderSecond1 setText:@"DELAY"];
                    [lblOrderThird1 setText:@"DYN"];
                } else if (orderCh1 == ORDER_CH_DYN_EQ_DELAY) {
                    [lblOrderFirst1 setText:@"DYN"];
                    [lblOrderSecond1 setText:@"EQ"];
                    [lblOrderThird1 setText:@"DELAY"];
                } else if (orderCh1 == ORDER_CH_DYN_DELAY_EQ) {
                    [lblOrderFirst1 setText:@"DYN"];
                    [lblOrderSecond1 setText:@"DELAY"];
                    [lblOrderThird1 setText:@"EQ"];
                } else if (orderCh1 == ORDER_CH_DELAY_DYN_EQ) {
                    [lblOrderFirst1 setText:@"DELAY"];
                    [lblOrderSecond1 setText:@"DYN"];
                    [lblOrderThird1 setText:@"EQ"];
                } else {
                    [lblOrderFirst1 setText:@"DELAY"];
                    [lblOrderSecond1 setText:@"EQ"];
                    [lblOrderThird1 setText:@"DYN"];
                }
                enum OrderChannel orderCh2 = orderChannel[CH_2];
                if (orderCh2 == ORDER_CH_EQ_DYN_DELAY) {
                    [lblOrderFirst2 setText:@"EQ"];
                    [lblOrderSecond2 setText:@"DYN"];
                    [lblOrderThird2 setText:@"DELAY"];
                } else if (orderCh2 == ORDER_CH_EQ_DELAY_DYN) {
                    [lblOrderFirst2 setText:@"EQ"];
                    [lblOrderSecond2 setText:@"DELAY"];
                    [lblOrderThird2 setText:@"DYN"];
                } else if (orderCh2 == ORDER_CH_DYN_EQ_DELAY) {
                    [lblOrderFirst2 setText:@"DYN"];
                    [lblOrderSecond2 setText:@"EQ"];
                    [lblOrderThird2 setText:@"DELAY"];
                } else if (orderCh2 == ORDER_CH_DYN_DELAY_EQ) {
                    [lblOrderFirst2 setText:@"DYN"];
                    [lblOrderSecond2 setText:@"DELAY"];
                    [lblOrderThird2 setText:@"EQ"];
                } else if (orderCh2 == ORDER_CH_DELAY_DYN_EQ) {
                    [lblOrderFirst2 setText:@"DELAY"];
                    [lblOrderSecond2 setText:@"DYN"];
                    [lblOrderThird2 setText:@"EQ"];
                } else {
                    [lblOrderFirst2 setText:@"DELAY"];
                    [lblOrderSecond2 setText:@"EQ"];
                    [lblOrderThird2 setText:@"DYN"];
                }
                enum OrderChannel orderCh3 = orderChannel[CH_3];
                if (orderCh3 == ORDER_CH_EQ_DYN_DELAY) {
                    [lblOrderFirst3 setText:@"EQ"];
                    [lblOrderSecond3 setText:@"DYN"];
                    [lblOrderThird3 setText:@"DELAY"];
                } else if (orderCh3 == ORDER_CH_EQ_DELAY_DYN) {
                    [lblOrderFirst3 setText:@"EQ"];
                    [lblOrderSecond3 setText:@"DELAY"];
                    [lblOrderThird3 setText:@"DYN"];
                } else if (orderCh3 == ORDER_CH_DYN_EQ_DELAY) {
                    [lblOrderFirst3 setText:@"DYN"];
                    [lblOrderSecond3 setText:@"EQ"];
                    [lblOrderThird3 setText:@"DELAY"];
                } else if (orderCh3 == ORDER_CH_DYN_DELAY_EQ) {
                    [lblOrderFirst3 setText:@"DYN"];
                    [lblOrderSecond3 setText:@"DELAY"];
                    [lblOrderThird3 setText:@"EQ"];
                } else if (orderCh3 == ORDER_CH_DELAY_DYN_EQ) {
                    [lblOrderFirst3 setText:@"DELAY"];
                    [lblOrderSecond3 setText:@"DYN"];
                    [lblOrderThird3 setText:@"EQ"];
                } else {
                    [lblOrderFirst3 setText:@"DELAY"];
                    [lblOrderSecond3 setText:@"EQ"];
                    [lblOrderThird3 setText:@"DYN"];
                }
                enum OrderChannel orderCh4 = orderChannel[CH_4];
                if (orderCh4 == ORDER_CH_EQ_DYN_DELAY) {
                    [lblOrderFirst4 setText:@"EQ"];
                    [lblOrderSecond4 setText:@"DYN"];
                    [lblOrderThird4 setText:@"DELAY"];
                } else if (orderCh4 == ORDER_CH_EQ_DELAY_DYN) {
                    [lblOrderFirst4 setText:@"EQ"];
                    [lblOrderSecond4 setText:@"DELAY"];
                    [lblOrderThird4 setText:@"DYN"];
                } else if (orderCh4 == ORDER_CH_DYN_EQ_DELAY) {
                    [lblOrderFirst4 setText:@"DYN"];
                    [lblOrderSecond4 setText:@"EQ"];
                    [lblOrderThird4 setText:@"DELAY"];
                } else if (orderCh4 == ORDER_CH_DYN_DELAY_EQ) {
                    [lblOrderFirst4 setText:@"DYN"];
                    [lblOrderSecond4 setText:@"DELAY"];
                    [lblOrderThird4 setText:@"EQ"];
                } else if (orderCh4 == ORDER_CH_DELAY_DYN_EQ) {
                    [lblOrderFirst4 setText:@"DELAY"];
                    [lblOrderSecond4 setText:@"DYN"];
                    [lblOrderThird4 setText:@"EQ"];
                } else {
                    [lblOrderFirst4 setText:@"DELAY"];
                    [lblOrderSecond4 setText:@"EQ"];
                    [lblOrderThird4 setText:@"DYN"];
                }
                enum OrderChannel orderCh5 = orderChannel[CH_5];
                if (orderCh5 == ORDER_CH_EQ_DYN_DELAY) {
                    [lblOrderFirst5 setText:@"EQ"];
                    [lblOrderSecond5 setText:@"DYN"];
                    [lblOrderThird5 setText:@"DELAY"];
                } else if (orderCh5 == ORDER_CH_EQ_DELAY_DYN) {
                    [lblOrderFirst5 setText:@"EQ"];
                    [lblOrderSecond5 setText:@"DELAY"];
                    [lblOrderThird5 setText:@"DYN"];
                } else if (orderCh5 == ORDER_CH_DYN_EQ_DELAY) {
                    [lblOrderFirst5 setText:@"DYN"];
                    [lblOrderSecond5 setText:@"EQ"];
                    [lblOrderThird5 setText:@"DELAY"];
                } else if (orderCh5 == ORDER_CH_DYN_DELAY_EQ) {
                    [lblOrderFirst5 setText:@"DYN"];
                    [lblOrderSecond5 setText:@"DELAY"];
                    [lblOrderThird5 setText:@"EQ"];
                } else if (orderCh5 == ORDER_CH_DELAY_DYN_EQ) {
                    [lblOrderFirst5 setText:@"DELAY"];
                    [lblOrderSecond5 setText:@"DYN"];
                    [lblOrderThird5 setText:@"EQ"];
                } else {
                    [lblOrderFirst5 setText:@"DELAY"];
                    [lblOrderSecond5 setText:@"EQ"];
                    [lblOrderThird5 setText:@"DYN"];
                }
                enum OrderChannel orderCh6 = orderChannel[CH_6];
                if (orderCh6 == ORDER_CH_EQ_DYN_DELAY) {
                    [lblOrderFirst6 setText:@"EQ"];
                    [lblOrderSecond6 setText:@"DYN"];
                    [lblOrderThird6 setText:@"DELAY"];
                } else if (orderCh6 == ORDER_CH_EQ_DELAY_DYN) {
                    [lblOrderFirst6 setText:@"EQ"];
                    [lblOrderSecond6 setText:@"DELAY"];
                    [lblOrderThird6 setText:@"DYN"];
                } else if (orderCh6 == ORDER_CH_DYN_EQ_DELAY) {
                    [lblOrderFirst6 setText:@"DYN"];
                    [lblOrderSecond6 setText:@"EQ"];
                    [lblOrderThird6 setText:@"DELAY"];
                } else if (orderCh6 == ORDER_CH_DYN_DELAY_EQ) {
                    [lblOrderFirst6 setText:@"DYN"];
                    [lblOrderSecond6 setText:@"DELAY"];
                    [lblOrderThird6 setText:@"EQ"];
                } else if (orderCh6 == ORDER_CH_DELAY_DYN_EQ) {
                    [lblOrderFirst6 setText:@"DELAY"];
                    [lblOrderSecond6 setText:@"DYN"];
                    [lblOrderThird6 setText:@"EQ"];
                } else {
                    [lblOrderFirst6 setText:@"DELAY"];
                    [lblOrderSecond6 setText:@"EQ"];
                    [lblOrderThird6 setText:@"DYN"];
                }
                enum OrderChannel orderCh7 = orderChannel[CH_7];
                if (orderCh7 == ORDER_CH_EQ_DYN_DELAY) {
                    [lblOrderFirst7 setText:@"EQ"];
                    [lblOrderSecond7 setText:@"DYN"];
                    [lblOrderThird7 setText:@"DELAY"];
                } else if (orderCh7 == ORDER_CH_EQ_DELAY_DYN) {
                    [lblOrderFirst7 setText:@"EQ"];
                    [lblOrderSecond7 setText:@"DELAY"];
                    [lblOrderThird7 setText:@"DYN"];
                } else if (orderCh7 == ORDER_CH_DYN_EQ_DELAY) {
                    [lblOrderFirst7 setText:@"DYN"];
                    [lblOrderSecond7 setText:@"EQ"];
                    [lblOrderThird7 setText:@"DELAY"];
                } else if (orderCh7 == ORDER_CH_DYN_DELAY_EQ) {
                    [lblOrderFirst7 setText:@"DYN"];
                    [lblOrderSecond7 setText:@"DELAY"];
                    [lblOrderThird7 setText:@"EQ"];
                } else if (orderCh7 == ORDER_CH_DELAY_DYN_EQ) {
                    [lblOrderFirst7 setText:@"DELAY"];
                    [lblOrderSecond7 setText:@"DYN"];
                    [lblOrderThird7 setText:@"EQ"];
                } else {
                    [lblOrderFirst7 setText:@"DELAY"];
                    [lblOrderSecond7 setText:@"EQ"];
                    [lblOrderThird7 setText:@"DYN"];
                }
                enum OrderChannel orderCh8 = orderChannel[CH_8];
                if (orderCh8 == ORDER_CH_EQ_DYN_DELAY) {
                    [lblOrderFirst8 setText:@"EQ"];
                    [lblOrderSecond8 setText:@"DYN"];
                    [lblOrderThird8 setText:@"DELAY"];
                } else if (orderCh8 == ORDER_CH_EQ_DELAY_DYN) {
                    [lblOrderFirst8 setText:@"EQ"];
                    [lblOrderSecond8 setText:@"DELAY"];
                    [lblOrderThird8 setText:@"DYN"];
                } else if (orderCh8 == ORDER_CH_DYN_EQ_DELAY) {
                    [lblOrderFirst8 setText:@"DYN"];
                    [lblOrderSecond8 setText:@"EQ"];
                    [lblOrderThird8 setText:@"DELAY"];
                } else if (orderCh8 == ORDER_CH_DYN_DELAY_EQ) {
                    [lblOrderFirst8 setText:@"DYN"];
                    [lblOrderSecond8 setText:@"DELAY"];
                    [lblOrderThird8 setText:@"EQ"];
                } else if (orderCh8 == ORDER_CH_DELAY_DYN_EQ) {
                    [lblOrderFirst8 setText:@"DELAY"];
                    [lblOrderSecond8 setText:@"DYN"];
                    [lblOrderThird8 setText:@"EQ"];
                } else {
                    [lblOrderFirst8 setText:@"DELAY"];
                    [lblOrderSecond8 setText:@"EQ"];
                    [lblOrderThird8 setText:@"DYN"];
                }
            }
            break;
        case PAGE_CH_9_16:
            {
                enum OrderChannel orderCh1 = orderChannel[CH_9];
                if (orderCh1 == ORDER_CH_EQ_DYN_DELAY) {
                    [lblOrderFirst1 setText:@"EQ"];
                    [lblOrderSecond1 setText:@"DYN"];
                    [lblOrderThird1 setText:@"DELAY"];
                } else if (orderCh1 == ORDER_CH_EQ_DELAY_DYN) {
                    [lblOrderFirst1 setText:@"EQ"];
                    [lblOrderSecond1 setText:@"DELAY"];
                    [lblOrderThird1 setText:@"DYN"];
                } else if (orderCh1 == ORDER_CH_DYN_EQ_DELAY) {
                    [lblOrderFirst1 setText:@"DYN"];
                    [lblOrderSecond1 setText:@"EQ"];
                    [lblOrderThird1 setText:@"DELAY"];
                } else if (orderCh1 == ORDER_CH_DYN_DELAY_EQ) {
                    [lblOrderFirst1 setText:@"DYN"];
                    [lblOrderSecond1 setText:@"DELAY"];
                    [lblOrderThird1 setText:@"EQ"];
                } else if (orderCh1 == ORDER_CH_DELAY_DYN_EQ) {
                    [lblOrderFirst1 setText:@"DELAY"];
                    [lblOrderSecond1 setText:@"DYN"];
                    [lblOrderThird1 setText:@"EQ"];
                } else {
                    [lblOrderFirst1 setText:@"DELAY"];
                    [lblOrderSecond1 setText:@"EQ"];
                    [lblOrderThird1 setText:@"DYN"];
                }
                enum OrderChannel orderCh2 = orderChannel[CH_10];
                if (orderCh2 == ORDER_CH_EQ_DYN_DELAY) {
                    [lblOrderFirst2 setText:@"EQ"];
                    [lblOrderSecond2 setText:@"DYN"];
                    [lblOrderThird2 setText:@"DELAY"];
                } else if (orderCh2 == ORDER_CH_EQ_DELAY_DYN) {
                    [lblOrderFirst2 setText:@"EQ"];
                    [lblOrderSecond2 setText:@"DELAY"];
                    [lblOrderThird2 setText:@"DYN"];
                } else if (orderCh2 == ORDER_CH_DYN_EQ_DELAY) {
                    [lblOrderFirst2 setText:@"DYN"];
                    [lblOrderSecond2 setText:@"EQ"];
                    [lblOrderThird2 setText:@"DELAY"];
                } else if (orderCh2 == ORDER_CH_DYN_DELAY_EQ) {
                    [lblOrderFirst2 setText:@"DYN"];
                    [lblOrderSecond2 setText:@"DELAY"];
                    [lblOrderThird2 setText:@"EQ"];
                } else if (orderCh2 == ORDER_CH_DELAY_DYN_EQ) {
                    [lblOrderFirst2 setText:@"DELAY"];
                    [lblOrderSecond2 setText:@"DYN"];
                    [lblOrderThird2 setText:@"EQ"];
                } else {
                    [lblOrderFirst2 setText:@"DELAY"];
                    [lblOrderSecond2 setText:@"EQ"];
                    [lblOrderThird2 setText:@"DYN"];
                }
                enum OrderChannel orderCh3 = orderChannel[CH_11];
                if (orderCh3 == ORDER_CH_EQ_DYN_DELAY) {
                    [lblOrderFirst3 setText:@"EQ"];
                    [lblOrderSecond3 setText:@"DYN"];
                    [lblOrderThird3 setText:@"DELAY"];
                } else if (orderCh3 == ORDER_CH_EQ_DELAY_DYN) {
                    [lblOrderFirst3 setText:@"EQ"];
                    [lblOrderSecond3 setText:@"DELAY"];
                    [lblOrderThird3 setText:@"DYN"];
                } else if (orderCh3 == ORDER_CH_DYN_EQ_DELAY) {
                    [lblOrderFirst3 setText:@"DYN"];
                    [lblOrderSecond3 setText:@"EQ"];
                    [lblOrderThird3 setText:@"DELAY"];
                } else if (orderCh3 == ORDER_CH_DYN_DELAY_EQ) {
                    [lblOrderFirst3 setText:@"DYN"];
                    [lblOrderSecond3 setText:@"DELAY"];
                    [lblOrderThird3 setText:@"EQ"];
                } else if (orderCh3 == ORDER_CH_DELAY_DYN_EQ) {
                    [lblOrderFirst3 setText:@"DELAY"];
                    [lblOrderSecond3 setText:@"DYN"];
                    [lblOrderThird3 setText:@"EQ"];
                } else {
                    [lblOrderFirst3 setText:@"DELAY"];
                    [lblOrderSecond3 setText:@"EQ"];
                    [lblOrderThird3 setText:@"DYN"];
                }
                enum OrderChannel orderCh4 = orderChannel[CH_12];
                if (orderCh4 == ORDER_CH_EQ_DYN_DELAY) {
                    [lblOrderFirst4 setText:@"EQ"];
                    [lblOrderSecond4 setText:@"DYN"];
                    [lblOrderThird4 setText:@"DELAY"];
                } else if (orderCh4 == ORDER_CH_EQ_DELAY_DYN) {
                    [lblOrderFirst4 setText:@"EQ"];
                    [lblOrderSecond4 setText:@"DELAY"];
                    [lblOrderThird4 setText:@"DYN"];
                } else if (orderCh4 == ORDER_CH_DYN_EQ_DELAY) {
                    [lblOrderFirst4 setText:@"DYN"];
                    [lblOrderSecond4 setText:@"EQ"];
                    [lblOrderThird4 setText:@"DELAY"];
                } else if (orderCh4 == ORDER_CH_DYN_DELAY_EQ) {
                    [lblOrderFirst4 setText:@"DYN"];
                    [lblOrderSecond4 setText:@"DELAY"];
                    [lblOrderThird4 setText:@"EQ"];
                } else if (orderCh4 == ORDER_CH_DELAY_DYN_EQ) {
                    [lblOrderFirst4 setText:@"DELAY"];
                    [lblOrderSecond4 setText:@"DYN"];
                    [lblOrderThird4 setText:@"EQ"];
                } else {
                    [lblOrderFirst4 setText:@"DELAY"];
                    [lblOrderSecond4 setText:@"EQ"];
                    [lblOrderThird4 setText:@"DYN"];
                }
                enum OrderChannel orderCh5 = orderChannel[CH_13];
                if (orderCh5 == ORDER_CH_EQ_DYN_DELAY) {
                    [lblOrderFirst5 setText:@"EQ"];
                    [lblOrderSecond5 setText:@"DYN"];
                    [lblOrderThird5 setText:@"DELAY"];
                } else if (orderCh5 == ORDER_CH_EQ_DELAY_DYN) {
                    [lblOrderFirst5 setText:@"EQ"];
                    [lblOrderSecond5 setText:@"DELAY"];
                    [lblOrderThird5 setText:@"DYN"];
                } else if (orderCh5 == ORDER_CH_DYN_EQ_DELAY) {
                    [lblOrderFirst5 setText:@"DYN"];
                    [lblOrderSecond5 setText:@"EQ"];
                    [lblOrderThird5 setText:@"DELAY"];
                } else if (orderCh5 == ORDER_CH_DYN_DELAY_EQ) {
                    [lblOrderFirst5 setText:@"DYN"];
                    [lblOrderSecond5 setText:@"DELAY"];
                    [lblOrderThird5 setText:@"EQ"];
                } else if (orderCh5 == ORDER_CH_DELAY_DYN_EQ) {
                    [lblOrderFirst5 setText:@"DELAY"];
                    [lblOrderSecond5 setText:@"DYN"];
                    [lblOrderThird5 setText:@"EQ"];
                } else {
                    [lblOrderFirst5 setText:@"DELAY"];
                    [lblOrderSecond5 setText:@"EQ"];
                    [lblOrderThird5 setText:@"DYN"];
                }
                enum OrderChannel orderCh6 = orderChannel[CH_14];
                if (orderCh6 == ORDER_CH_EQ_DYN_DELAY) {
                    [lblOrderFirst6 setText:@"EQ"];
                    [lblOrderSecond6 setText:@"DYN"];
                    [lblOrderThird6 setText:@"DELAY"];
                } else if (orderCh6 == ORDER_CH_EQ_DELAY_DYN) {
                    [lblOrderFirst6 setText:@"EQ"];
                    [lblOrderSecond6 setText:@"DELAY"];
                    [lblOrderThird6 setText:@"DYN"];
                } else if (orderCh6 == ORDER_CH_DYN_EQ_DELAY) {
                    [lblOrderFirst6 setText:@"DYN"];
                    [lblOrderSecond6 setText:@"EQ"];
                    [lblOrderThird6 setText:@"DELAY"];
                } else if (orderCh6 == ORDER_CH_DYN_DELAY_EQ) {
                    [lblOrderFirst6 setText:@"DYN"];
                    [lblOrderSecond6 setText:@"DELAY"];
                    [lblOrderThird6 setText:@"EQ"];
                } else if (orderCh6 == ORDER_CH_DELAY_DYN_EQ) {
                    [lblOrderFirst6 setText:@"DELAY"];
                    [lblOrderSecond6 setText:@"DYN"];
                    [lblOrderThird6 setText:@"EQ"];
                } else {
                    [lblOrderFirst6 setText:@"DELAY"];
                    [lblOrderSecond6 setText:@"EQ"];
                    [lblOrderThird6 setText:@"DYN"];
                }
                enum OrderChannel orderCh7 = orderChannel[CH_15];
                if (orderCh7 == ORDER_CH_EQ_DYN_DELAY) {
                    [lblOrderFirst7 setText:@"EQ"];
                    [lblOrderSecond7 setText:@"DYN"];
                    [lblOrderThird7 setText:@"DELAY"];
                } else if (orderCh7 == ORDER_CH_EQ_DELAY_DYN) {
                    [lblOrderFirst7 setText:@"EQ"];
                    [lblOrderSecond7 setText:@"DELAY"];
                    [lblOrderThird7 setText:@"DYN"];
                } else if (orderCh7 == ORDER_CH_DYN_EQ_DELAY) {
                    [lblOrderFirst7 setText:@"DYN"];
                    [lblOrderSecond7 setText:@"EQ"];
                    [lblOrderThird7 setText:@"DELAY"];
                } else if (orderCh7 == ORDER_CH_DYN_DELAY_EQ) {
                    [lblOrderFirst7 setText:@"DYN"];
                    [lblOrderSecond7 setText:@"DELAY"];
                    [lblOrderThird7 setText:@"EQ"];
                } else if (orderCh7 == ORDER_CH_DELAY_DYN_EQ) {
                    [lblOrderFirst7 setText:@"DELAY"];
                    [lblOrderSecond7 setText:@"DYN"];
                    [lblOrderThird7 setText:@"EQ"];
                } else {
                    [lblOrderFirst7 setText:@"DELAY"];
                    [lblOrderSecond7 setText:@"EQ"];
                    [lblOrderThird7 setText:@"DYN"];
                }
                enum OrderChannel orderCh8 = orderChannel[CH_16];
                if (orderCh8 == ORDER_CH_EQ_DYN_DELAY) {
                    [lblOrderFirst8 setText:@"EQ"];
                    [lblOrderSecond8 setText:@"DYN"];
                    [lblOrderThird8 setText:@"DELAY"];
                } else if (orderCh8 == ORDER_CH_EQ_DELAY_DYN) {
                    [lblOrderFirst8 setText:@"EQ"];
                    [lblOrderSecond8 setText:@"DELAY"];
                    [lblOrderThird8 setText:@"DYN"];
                } else if (orderCh8 == ORDER_CH_DYN_EQ_DELAY) {
                    [lblOrderFirst8 setText:@"DYN"];
                    [lblOrderSecond8 setText:@"EQ"];
                    [lblOrderThird8 setText:@"DELAY"];
                } else if (orderCh8 == ORDER_CH_DYN_DELAY_EQ) {
                    [lblOrderFirst8 setText:@"DYN"];
                    [lblOrderSecond8 setText:@"DELAY"];
                    [lblOrderThird8 setText:@"EQ"];
                } else if (orderCh8 == ORDER_CH_DELAY_DYN_EQ) {
                    [lblOrderFirst8 setText:@"DELAY"];
                    [lblOrderSecond8 setText:@"DYN"];
                    [lblOrderThird8 setText:@"EQ"];
                } else {
                    [lblOrderFirst8 setText:@"DELAY"];
                    [lblOrderSecond8 setText:@"EQ"];
                    [lblOrderThird8 setText:@"DYN"];
                }
            }
            break;
        case PAGE_USB_AUDIO:
            {
                enum OrderChannel orderCh1 = orderChannel[CH_17];
                if (orderCh1 == ORDER_CH_EQ_DYN_DELAY) {
                    [lblOrderFirst1 setText:@"EQ"];
                    [lblOrderSecond1 setText:@"DYN"];
                    [lblOrderThird1 setText:@"DELAY"];
                } else if (orderCh1 == ORDER_CH_EQ_DELAY_DYN) {
                    [lblOrderFirst1 setText:@"EQ"];
                    [lblOrderSecond1 setText:@"DELAY"];
                    [lblOrderThird1 setText:@"DYN"];
                } else if (orderCh1 == ORDER_CH_DYN_EQ_DELAY) {
                    [lblOrderFirst1 setText:@"DYN"];
                    [lblOrderSecond1 setText:@"EQ"];
                    [lblOrderThird1 setText:@"DELAY"];
                } else if (orderCh1 == ORDER_CH_DYN_DELAY_EQ) {
                    [lblOrderFirst1 setText:@"DYN"];
                    [lblOrderSecond1 setText:@"DELAY"];
                    [lblOrderThird1 setText:@"EQ"];
                } else if (orderCh1 == ORDER_CH_DELAY_DYN_EQ) {
                    [lblOrderFirst1 setText:@"DELAY"];
                    [lblOrderSecond1 setText:@"DYN"];
                    [lblOrderThird1 setText:@"EQ"];
                } else {
                    [lblOrderFirst1 setText:@"DELAY"];
                    [lblOrderSecond1 setText:@"EQ"];
                    [lblOrderThird1 setText:@"DYN"];
                }
                enum OrderChannel orderCh2 = orderChannel[CH_18];
                if (orderCh2 == ORDER_CH_EQ_DYN_DELAY) {
                    [lblOrderFirst2 setText:@"EQ"];
                    [lblOrderSecond2 setText:@"DYN"];
                    [lblOrderThird2 setText:@"DELAY"];
                } else if (orderCh2 == ORDER_CH_EQ_DELAY_DYN) {
                    [lblOrderFirst2 setText:@"EQ"];
                    [lblOrderSecond2 setText:@"DELAY"];
                    [lblOrderThird2 setText:@"DYN"];
                } else if (orderCh2 == ORDER_CH_DYN_EQ_DELAY) {
                    [lblOrderFirst2 setText:@"DYN"];
                    [lblOrderSecond2 setText:@"EQ"];
                    [lblOrderThird2 setText:@"DELAY"];
                } else if (orderCh2 == ORDER_CH_DYN_DELAY_EQ) {
                    [lblOrderFirst2 setText:@"DYN"];
                    [lblOrderSecond2 setText:@"DELAY"];
                    [lblOrderThird2 setText:@"EQ"];
                } else if (orderCh2 == ORDER_CH_DELAY_DYN_EQ) {
                    [lblOrderFirst2 setText:@"DELAY"];
                    [lblOrderSecond2 setText:@"DYN"];
                    [lblOrderThird2 setText:@"EQ"];
                } else {
                    [lblOrderFirst2 setText:@"DELAY"];
                    [lblOrderSecond2 setText:@"EQ"];
                    [lblOrderThird2 setText:@"DYN"];
                }
            }
            break;
        case PAGE_MT_1_4:
            {
                enum OrderMulti orderMt1 = orderMulti[0];
                if (orderMt1 == ORDER_MT_EQ_DYN_DELAY) {
                    [lblOrderFirst1 setText:@"EQ"];
                    [lblOrderSecond1 setText:@"DYN"];
                    [lblOrderThird1 setText:@"DELAY"];
                } else if (orderMt1 == ORDER_MT_DYN_EQ_DELAY) {
                    [lblOrderFirst1 setText:@"DYN"];
                    [lblOrderSecond1 setText:@"EQ"];
                    [lblOrderThird1 setText:@"DELAY"];
                }
                enum OrderMulti orderMt2 = orderMulti[1];
                if (orderMt2 == ORDER_MT_EQ_DYN_DELAY) {
                    [lblOrderFirst2 setText:@"EQ"];
                    [lblOrderSecond2 setText:@"DYN"];
                    [lblOrderThird2 setText:@"DELAY"];
                } else if (orderMt2 == ORDER_MT_DYN_EQ_DELAY) {
                    [lblOrderFirst2 setText:@"DYN"];
                    [lblOrderSecond2 setText:@"EQ"];
                    [lblOrderThird2 setText:@"DELAY"];
                }
                enum OrderMulti orderMt3 = orderMulti[2];
                if (orderMt3 == ORDER_MT_EQ_DYN_DELAY) {
                    [lblOrderFirst3 setText:@"EQ"];
                    [lblOrderSecond3 setText:@"DYN"];
                    [lblOrderThird3 setText:@"DELAY"];
                } else if (orderMt3 == ORDER_MT_DYN_EQ_DELAY) {
                    [lblOrderFirst3 setText:@"DYN"];
                    [lblOrderSecond3 setText:@"EQ"];
                    [lblOrderThird3 setText:@"DELAY"];
                }
                enum OrderMulti orderMt4 = orderMulti[3];
                if (orderMt4 == ORDER_MT_EQ_DYN_DELAY) {
                    [lblOrderFirst4 setText:@"EQ"];
                    [lblOrderSecond4 setText:@"DYN"];
                    [lblOrderThird4 setText:@"DELAY"];
                } else if (orderMt4 == ORDER_MT_DYN_EQ_DELAY) {
                    [lblOrderFirst4 setText:@"DYN"];
                    [lblOrderSecond4 setText:@"EQ"];
                    [lblOrderThird4 setText:@"DELAY"];
                }
            }
            break;
        default:
            break;
    }
}

- (void)enableModeSelector:(BOOL)status
{
    _modeEnabled = status;
    [lblCurrentMode setHidden:!status];
    [btnModeSelectUp setEnabled:status];
    [btnModeSelectDown setEnabled:status];
}

- (void)refreshModeButtons
{
    if (!_modeEnabled) {
        return;
    }
    
    [self hideAllModeButtons:YES];
    
    switch (currentMode) {
        case MODE_EQ:
            switch (currentPage) {
                case PAGE_CH_1_8:
                case PAGE_CH_9_16:
                case PAGE_USB_AUDIO:
                case PAGE_MT_1_4:
                    [btnSliderPanEq1 setHidden:NO];
                    [btnSliderPanEq2 setHidden:NO];
                    [btnSliderPanEq3 setHidden:NO];
                    [btnSliderPanEq4 setHidden:NO];
                    [btnSliderPanEq5 setHidden:NO];
                    [btnSliderPanEq6 setHidden:NO];
                    [btnSliderPanEq7 setHidden:NO];
                    [btnSliderPanEq8 setHidden:NO];
                    [btnSliderPanEqMain setHidden:NO];
                    [btnSliderPanEq1 setBackgroundImage:[UIImage imageNamed:@"pan-2.png"] forState:UIControlStateNormal];
                    [btnSliderPanEq2 setBackgroundImage:[UIImage imageNamed:@"pan-2.png"] forState:UIControlStateNormal];
                    [btnSliderPanEq3 setBackgroundImage:[UIImage imageNamed:@"pan-2.png"] forState:UIControlStateNormal];
                    [btnSliderPanEq4 setBackgroundImage:[UIImage imageNamed:@"pan-2.png"] forState:UIControlStateNormal];
                    [btnSliderPanEq5 setBackgroundImage:[UIImage imageNamed:@"pan-2.png"] forState:UIControlStateNormal];
                    [btnSliderPanEq6 setBackgroundImage:[UIImage imageNamed:@"pan-2.png"] forState:UIControlStateNormal];
                    [btnSliderPanEq7 setBackgroundImage:[UIImage imageNamed:@"pan-2.png"] forState:UIControlStateNormal];
                    [btnSliderPanEq8 setBackgroundImage:[UIImage imageNamed:@"pan-2.png"] forState:UIControlStateNormal];
                    [btnSliderPanEqMain setBackgroundImage:[UIImage imageNamed:@"pan-2.png"] forState:UIControlStateNormal];
                    break;
                case PAGE_AUX_1_4:
                case PAGE_GP_1_4:
                case PAGE_CTRL_RM:
                case PAGE_MAIN:
                    [btnSliderPanEqMain setHidden:NO];
                    [btnSliderPanEqMain setBackgroundImage:[UIImage imageNamed:@"pan-2.png"] forState:UIControlStateNormal];
                default:
                    break;
            }
            [self clearPanButtonImages];
            [self refreshPeqButtons];
            break;
            
        case MODE_PAN:
            switch (currentPage) {
                case PAGE_CH_1_8:
                case PAGE_CH_9_16:
                    [btnSliderPanEq1 setHidden:NO];
                    [btnSliderPanEq2 setHidden:NO];
                    [btnSliderPanEq3 setHidden:NO];
                    [btnSliderPanEq4 setHidden:NO];
                    [btnSliderPanEq5 setHidden:NO];
                    [btnSliderPanEq6 setHidden:NO];
                    [btnSliderPanEq7 setHidden:NO];
                    [btnSliderPanEq8 setHidden:NO];
                    [btnSliderPanEqMain setHidden:YES];
                    [sliderPan1 setHidden:NO];
                    [sliderPan2 setHidden:NO];
                    [sliderPan3 setHidden:NO];
                    [sliderPan4 setHidden:NO];
                    [sliderPan5 setHidden:NO];
                    [sliderPan6 setHidden:NO];
                    [sliderPan7 setHidden:NO];
                    [sliderPan8 setHidden:NO];
                    [lblPanValue1 setHidden:NO];
                    [lblPanValue2 setHidden:NO];
                    [lblPanValue3 setHidden:NO];
                    [lblPanValue4 setHidden:NO];
                    [lblPanValue5 setHidden:NO];
                    [lblPanValue6 setHidden:NO];
                    [lblPanValue7 setHidden:NO];
                    [lblPanValue8 setHidden:NO];
                    [meterPan1 setHidden:NO];
                    [meterPan2 setHidden:NO];
                    [meterPan3 setHidden:NO];
                    [meterPan4 setHidden:NO];
                    [meterPan5 setHidden:NO];
                    [meterPan6 setHidden:NO];
                    [meterPan7 setHidden:NO];
                    [meterPan8 setHidden:NO];
                    [btnSliderPanEq1 setBackgroundImage:[UIImage imageNamed:@"pan-2.png"] forState:UIControlStateNormal];
                    [btnSliderPanEq2 setBackgroundImage:[UIImage imageNamed:@"pan-2.png"] forState:UIControlStateNormal];
                    [btnSliderPanEq3 setBackgroundImage:[UIImage imageNamed:@"pan-2.png"] forState:UIControlStateNormal];
                    [btnSliderPanEq4 setBackgroundImage:[UIImage imageNamed:@"pan-2.png"] forState:UIControlStateNormal];
                    [btnSliderPanEq5 setBackgroundImage:[UIImage imageNamed:@"pan-2.png"] forState:UIControlStateNormal];
                    [btnSliderPanEq6 setBackgroundImage:[UIImage imageNamed:@"pan-2.png"] forState:UIControlStateNormal];
                    [btnSliderPanEq7 setBackgroundImage:[UIImage imageNamed:@"pan-2.png"] forState:UIControlStateNormal];
                    [btnSliderPanEq8 setBackgroundImage:[UIImage imageNamed:@"pan-2.png"] forState:UIControlStateNormal];
                    break;
                case PAGE_USB_AUDIO:
                    [btnSliderPanEq1 setHidden:NO];
                    [btnSliderPanEq2 setHidden:NO];
                    [btnSliderPanEq3 setHidden:YES];
                    [btnSliderPanEq4 setHidden:YES];
                    [btnSliderPanEq5 setHidden:YES];
                    [btnSliderPanEq6 setHidden:YES];
                    [btnSliderPanEq7 setHidden:YES];
                    [btnSliderPanEq8 setHidden:YES];
                    [btnSliderPanEqMain setHidden:YES];
                    [sliderPan1 setHidden:NO];
                    [sliderPan2 setHidden:NO];
                    [sliderPan3 setHidden:YES];
                    [sliderPan4 setHidden:YES];
                    [sliderPan5 setHidden:YES];
                    [sliderPan6 setHidden:YES];
                    [sliderPan7 setHidden:YES];
                    [sliderPan8 setHidden:YES];
                    [lblPanValue1 setHidden:NO];
                    [lblPanValue2 setHidden:NO];
                    [lblPanValue3 setHidden:YES];
                    [lblPanValue4 setHidden:YES];
                    [lblPanValue5 setHidden:YES];
                    [lblPanValue6 setHidden:YES];
                    [lblPanValue7 setHidden:YES];
                    [lblPanValue8 setHidden:YES];
                    [meterPan1 setHidden:NO];
                    [meterPan2 setHidden:NO];
                    [meterPan3 setHidden:YES];
                    [meterPan4 setHidden:YES];
                    [meterPan5 setHidden:YES];
                    [meterPan6 setHidden:YES];
                    [meterPan7 setHidden:YES];
                    [meterPan8 setHidden:YES];
                    [btnSliderPanEq1 setBackgroundImage:[UIImage imageNamed:@"pan-2.png"] forState:UIControlStateNormal];
                    [btnSliderPanEq2 setBackgroundImage:[UIImage imageNamed:@"pan-2.png"] forState:UIControlStateNormal];
                    [btnSliderPanEq3 setBackgroundImage:[UIImage imageNamed:@"pan-2.png"] forState:UIControlStateNormal];
                    [btnSliderPanEq4 setBackgroundImage:[UIImage imageNamed:@"pan-2.png"] forState:UIControlStateNormal];
                    [btnSliderPanEq5 setBackgroundImage:[UIImage imageNamed:@"pan-2.png"] forState:UIControlStateNormal];
                    [btnSliderPanEq6 setBackgroundImage:[UIImage imageNamed:@"pan-2.png"] forState:UIControlStateNormal];
                    [btnSliderPanEq7 setBackgroundImage:[UIImage imageNamed:@"pan-2.png"] forState:UIControlStateNormal];
                    [btnSliderPanEq8 setBackgroundImage:[UIImage imageNamed:@"pan-2.png"] forState:UIControlStateNormal];
                    break;
                case PAGE_GP_1_4:
                    [btnSliderPanEq1 setHidden:NO];
                    [btnSliderPanEq2 setHidden:NO];
                    [btnSliderPanEq3 setHidden:NO];
                    [btnSliderPanEq4 setHidden:NO];
                    [btnSliderPanEq5 setHidden:YES];
                    [btnSliderPanEq6 setHidden:YES];
                    [btnSliderPanEq7 setHidden:YES];
                    [btnSliderPanEq8 setHidden:YES];
                    [btnSliderPanEqMain setHidden:YES];
                    [sliderPan1 setHidden:NO];
                    [sliderPan2 setHidden:NO];
                    [sliderPan3 setHidden:NO];
                    [sliderPan4 setHidden:NO];
                    [sliderPan5 setHidden:YES];
                    [sliderPan6 setHidden:YES];
                    [sliderPan7 setHidden:YES];
                    [sliderPan8 setHidden:YES];
                    [lblPanValue1 setHidden:NO];
                    [lblPanValue2 setHidden:NO];
                    [lblPanValue3 setHidden:NO];
                    [lblPanValue4 setHidden:NO];
                    [lblPanValue5 setHidden:YES];
                    [lblPanValue6 setHidden:YES];
                    [lblPanValue7 setHidden:YES];
                    [lblPanValue8 setHidden:YES];
                    [meterPan1 setHidden:NO];
                    [meterPan2 setHidden:NO];
                    [meterPan3 setHidden:NO];
                    [meterPan4 setHidden:NO];
                    [meterPan5 setHidden:YES];
                    [meterPan6 setHidden:YES];
                    [meterPan7 setHidden:YES];
                    [meterPan8 setHidden:YES];
                    [btnSliderPanEq1 setBackgroundImage:[UIImage imageNamed:@"pan-2.png"] forState:UIControlStateNormal];
                    [btnSliderPanEq2 setBackgroundImage:[UIImage imageNamed:@"pan-2.png"] forState:UIControlStateNormal];
                    [btnSliderPanEq3 setBackgroundImage:[UIImage imageNamed:@"pan-2.png"] forState:UIControlStateNormal];
                    [btnSliderPanEq4 setBackgroundImage:[UIImage imageNamed:@"pan-2.png"] forState:UIControlStateNormal];
                    [btnSliderPanEq5 setBackgroundImage:[UIImage imageNamed:@"pan-2.png"] forState:UIControlStateNormal];
                    [btnSliderPanEq6 setBackgroundImage:[UIImage imageNamed:@"pan-2.png"] forState:UIControlStateNormal];
                    [btnSliderPanEq7 setBackgroundImage:[UIImage imageNamed:@"pan-2.png"] forState:UIControlStateNormal];
                    [btnSliderPanEq8 setBackgroundImage:[UIImage imageNamed:@"pan-2.png"] forState:UIControlStateNormal];
                    break;
                default:
                    break;
            }
            [self clearPanButtonImages];
            break;
            
        case MODE_PHANTOM_POWER:
            switch (currentPage) {
                case PAGE_CH_1_8:
                    [btnSliderPanEq1 setHidden:NO];
                    [btnSliderPanEq2 setHidden:NO];
                    [btnSliderPanEq3 setHidden:NO];
                    [btnSliderPanEq4 setHidden:NO];
                    [btnSliderPanEq5 setHidden:NO];
                    [btnSliderPanEq6 setHidden:NO];
                    [btnSliderPanEq7 setHidden:NO];
                    [btnSliderPanEq8 setHidden:NO];
                    [btnSliderPanEqMain setHidden:YES];
                    if (btnPhantomPowerCh1OnOff.selected) {
                        [btnSliderPanEq1 setBackgroundImage:[UIImage imageNamed:@"pan-13.png"] forState:UIControlStateNormal];
                    } else {
                        [btnSliderPanEq1 setBackgroundImage:[UIImage imageNamed:@"pan-12.png"] forState:UIControlStateNormal];
                    }
                    if (btnPhantomPowerCh2OnOff.selected) {
                        [btnSliderPanEq2 setBackgroundImage:[UIImage imageNamed:@"pan-13.png"] forState:UIControlStateNormal];
                    } else {
                        [btnSliderPanEq2 setBackgroundImage:[UIImage imageNamed:@"pan-12.png"] forState:UIControlStateNormal];
                    }
                    if (btnPhantomPowerCh3OnOff.selected) {
                        [btnSliderPanEq3 setBackgroundImage:[UIImage imageNamed:@"pan-13.png"] forState:UIControlStateNormal];
                    } else {
                        [btnSliderPanEq3 setBackgroundImage:[UIImage imageNamed:@"pan-12.png"] forState:UIControlStateNormal];
                    }
                    if (btnPhantomPowerCh4OnOff.selected) {
                        [btnSliderPanEq4 setBackgroundImage:[UIImage imageNamed:@"pan-13.png"] forState:UIControlStateNormal];
                    } else {
                        [btnSliderPanEq4 setBackgroundImage:[UIImage imageNamed:@"pan-12.png"] forState:UIControlStateNormal];
                    }
                    if (btnPhantomPowerCh5OnOff.selected) {
                        [btnSliderPanEq5 setBackgroundImage:[UIImage imageNamed:@"pan-13.png"] forState:UIControlStateNormal];
                    } else {
                        [btnSliderPanEq5 setBackgroundImage:[UIImage imageNamed:@"pan-12.png"] forState:UIControlStateNormal];
                    }
                    if (btnPhantomPowerCh6OnOff.selected) {
                        [btnSliderPanEq6 setBackgroundImage:[UIImage imageNamed:@"pan-13.png"] forState:UIControlStateNormal];
                    } else {
                        [btnSliderPanEq6 setBackgroundImage:[UIImage imageNamed:@"pan-12.png"] forState:UIControlStateNormal];
                    }
                    if (btnPhantomPowerCh7OnOff.selected) {
                        [btnSliderPanEq7 setBackgroundImage:[UIImage imageNamed:@"pan-13.png"] forState:UIControlStateNormal];
                    } else {
                        [btnSliderPanEq7 setBackgroundImage:[UIImage imageNamed:@"pan-12.png"] forState:UIControlStateNormal];
                    }
                    if (btnPhantomPowerCh8OnOff.selected) {
                        [btnSliderPanEq8 setBackgroundImage:[UIImage imageNamed:@"pan-13.png"] forState:UIControlStateNormal];
                    } else {
                        [btnSliderPanEq8 setBackgroundImage:[UIImage imageNamed:@"pan-12.png"] forState:UIControlStateNormal];
                    }
                    break;
                case PAGE_CH_9_16:
                    [btnSliderPanEq1 setHidden:NO];
                    [btnSliderPanEq2 setHidden:NO];
                    [btnSliderPanEq3 setHidden:NO];
                    [btnSliderPanEq4 setHidden:NO];
                    [btnSliderPanEq5 setHidden:NO];
                    [btnSliderPanEq6 setHidden:NO];
                    [btnSliderPanEq7 setHidden:NO];
                    [btnSliderPanEq8 setHidden:NO];
                    [btnSliderPanEqMain setHidden:YES];
                    if (btnPhantomPowerCh9OnOff.selected) {
                        [btnSliderPanEq1 setBackgroundImage:[UIImage imageNamed:@"pan-13.png"] forState:UIControlStateNormal];
                    } else {
                        [btnSliderPanEq1 setBackgroundImage:[UIImage imageNamed:@"pan-12.png"] forState:UIControlStateNormal];
                    }
                    if (btnPhantomPowerCh10OnOff.selected) {
                        [btnSliderPanEq2 setBackgroundImage:[UIImage imageNamed:@"pan-13.png"] forState:UIControlStateNormal];
                    } else {
                        [btnSliderPanEq2 setBackgroundImage:[UIImage imageNamed:@"pan-12.png"] forState:UIControlStateNormal];
                    }
                    if (btnPhantomPowerCh11OnOff.selected) {
                        [btnSliderPanEq3 setBackgroundImage:[UIImage imageNamed:@"pan-13.png"] forState:UIControlStateNormal];
                    } else {
                        [btnSliderPanEq3 setBackgroundImage:[UIImage imageNamed:@"pan-12.png"] forState:UIControlStateNormal];
                    }
                    if (btnPhantomPowerCh12OnOff.selected) {
                        [btnSliderPanEq4 setBackgroundImage:[UIImage imageNamed:@"pan-13.png"] forState:UIControlStateNormal];
                    } else {
                        [btnSliderPanEq4 setBackgroundImage:[UIImage imageNamed:@"pan-12.png"] forState:UIControlStateNormal];
                    }
                    if (btnPhantomPowerCh13OnOff.selected) {
                        [btnSliderPanEq5 setBackgroundImage:[UIImage imageNamed:@"pan-13.png"] forState:UIControlStateNormal];
                    } else {
                        [btnSliderPanEq5 setBackgroundImage:[UIImage imageNamed:@"pan-12.png"] forState:UIControlStateNormal];
                    }
                    if (btnPhantomPowerCh14OnOff.selected) {
                        [btnSliderPanEq6 setBackgroundImage:[UIImage imageNamed:@"pan-13.png"] forState:UIControlStateNormal];
                    } else {
                        [btnSliderPanEq6 setBackgroundImage:[UIImage imageNamed:@"pan-12.png"] forState:UIControlStateNormal];
                    }
                    if (btnPhantomPowerCh15OnOff.selected) {
                        [btnSliderPanEq7 setBackgroundImage:[UIImage imageNamed:@"pan-13.png"] forState:UIControlStateNormal];
                    } else {
                        [btnSliderPanEq7 setBackgroundImage:[UIImage imageNamed:@"pan-12.png"] forState:UIControlStateNormal];
                    }
                    if (btnPhantomPowerCh16OnOff.selected) {
                        [btnSliderPanEq8 setBackgroundImage:[UIImage imageNamed:@"pan-13.png"] forState:UIControlStateNormal];
                    } else {
                        [btnSliderPanEq8 setBackgroundImage:[UIImage imageNamed:@"pan-12.png"] forState:UIControlStateNormal];
                    }
                    break;
                default:
                    break;
            }
            [self clearPanButtonImages];
            break;

        case MODE_INV:
            switch (currentPage) {
                case PAGE_CH_1_8:
                case PAGE_CH_9_16:
                case PAGE_USB_AUDIO:
                    [btnSliderPanEq1 setHidden:NO];
                    [btnSliderPanEq2 setHidden:NO];
                    [btnSliderPanEq3 setHidden:NO];
                    [btnSliderPanEq4 setHidden:NO];
                    [btnSliderPanEq5 setHidden:NO];
                    [btnSliderPanEq6 setHidden:NO];
                    [btnSliderPanEq7 setHidden:NO];
                    [btnSliderPanEq8 setHidden:NO];
                    [btnSliderPanEqMain setHidden:YES];
                    [self updateInvButtons];
                    break;
                default:
                    break;
            }
            [self clearPanButtonImages];
            break;
            
        case MODE_DELAY:
            switch (currentPage) {
                case PAGE_CH_1_8:
                case PAGE_CH_9_16:
                case PAGE_USB_AUDIO:
                case PAGE_MT_1_4:
                    [btnSliderPanEq1 setHidden:NO];
                    [btnSliderPanEq2 setHidden:NO];
                    [btnSliderPanEq3 setHidden:NO];
                    [btnSliderPanEq4 setHidden:NO];
                    [btnSliderPanEq5 setHidden:NO];
                    [btnSliderPanEq6 setHidden:NO];
                    [btnSliderPanEq7 setHidden:NO];
                    [btnSliderPanEq8 setHidden:NO];
                    [btnSliderPanEqMain setHidden:NO];
                    [lblDelay1 setHidden:NO];
                    [lblDelay2 setHidden:NO];
                    [lblDelay3 setHidden:NO];
                    [lblDelay4 setHidden:NO];
                    [lblDelay5 setHidden:NO];
                    [lblDelay6 setHidden:NO];
                    [lblDelay7 setHidden:NO];
                    [lblDelay8 setHidden:NO];
                    [lblDelayMain setHidden:NO];
                    [self updateDelayButtons];
                    break;
                default:
                    [btnSliderPanEqMain setHidden:NO];
                    [lblDelayMain setHidden:NO];
                    [btnSliderPanEqMain setBackgroundImage:[UIImage imageNamed:@"pan-16.png"] forState:UIControlStateNormal];
                    [self updateDelayButtons];
                    break;
            }
            [self clearPanButtonImages];
            break;

        case MODE_DYN:
            switch (currentPage) {
                case PAGE_CH_1_8:
                case PAGE_CH_9_16:
                case PAGE_USB_AUDIO:
                case PAGE_MT_1_4:
                    [btnSliderPanEq1 setHidden:NO];
                    [btnSliderPanEq2 setHidden:NO];
                    [btnSliderPanEq3 setHidden:NO];
                    [btnSliderPanEq4 setHidden:NO];
                    [btnSliderPanEq5 setHidden:NO];
                    [btnSliderPanEq6 setHidden:NO];
                    [btnSliderPanEq7 setHidden:NO];
                    [btnSliderPanEq8 setHidden:NO];
                    [btnSliderPanEqMain setHidden:NO];
                    [btnDynG1 setHidden:NO];
                    [btnDynG2 setHidden:NO];
                    [btnDynG3 setHidden:NO];
                    [btnDynG4 setHidden:NO];
                    [btnDynG5 setHidden:NO];
                    [btnDynG6 setHidden:NO];
                    [btnDynG7 setHidden:NO];
                    [btnDynG8 setHidden:NO];
                    [btnDynGMain setHidden:NO];
                    [btnDynE1 setHidden:NO];
                    [btnDynE2 setHidden:NO];
                    [btnDynE3 setHidden:NO];
                    [btnDynE4 setHidden:NO];
                    [btnDynE5 setHidden:NO];
                    [btnDynE6 setHidden:NO];
                    [btnDynE7 setHidden:NO];
                    [btnDynE8 setHidden:NO];
                    [btnDynEMain setHidden:NO];
                    [btnDynC1 setHidden:NO];
                    [btnDynC2 setHidden:NO];
                    [btnDynC3 setHidden:NO];
                    [btnDynC4 setHidden:NO];
                    [btnDynC5 setHidden:NO];
                    [btnDynC6 setHidden:NO];
                    [btnDynC7 setHidden:NO];
                    [btnDynC8 setHidden:NO];
                    [btnDynCMain setHidden:NO];
                    [btnDynL1 setHidden:NO];
                    [btnDynL2 setHidden:NO];
                    [btnDynL3 setHidden:NO];
                    [btnDynL4 setHidden:NO];
                    [btnDynL5 setHidden:NO];
                    [btnDynL6 setHidden:NO];
                    [btnDynL7 setHidden:NO];
                    [btnDynL8 setHidden:NO];
                    [btnDynLMain setHidden:NO];
                    [btnSliderPanEq1 setBackgroundImage:[UIImage imageNamed:@"pan-8.png"] forState:UIControlStateNormal];
                    [btnSliderPanEq2 setBackgroundImage:[UIImage imageNamed:@"pan-8.png"] forState:UIControlStateNormal];
                    [btnSliderPanEq3 setBackgroundImage:[UIImage imageNamed:@"pan-8.png"] forState:UIControlStateNormal];
                    [btnSliderPanEq4 setBackgroundImage:[UIImage imageNamed:@"pan-8.png"] forState:UIControlStateNormal];
                    [btnSliderPanEq5 setBackgroundImage:[UIImage imageNamed:@"pan-8.png"] forState:UIControlStateNormal];
                    [btnSliderPanEq6 setBackgroundImage:[UIImage imageNamed:@"pan-8.png"] forState:UIControlStateNormal];
                    [btnSliderPanEq7 setBackgroundImage:[UIImage imageNamed:@"pan-8.png"] forState:UIControlStateNormal];
                    [btnSliderPanEq8 setBackgroundImage:[UIImage imageNamed:@"pan-8.png"] forState:UIControlStateNormal];
                    [btnSliderPanEqMain setBackgroundImage:[UIImage imageNamed:@"pan-8.png"] forState:UIControlStateNormal];
                    break;
                case PAGE_AUX_1_4:
                case PAGE_GP_1_4:
                case PAGE_CTRL_RM:
                case PAGE_MAIN:
                    [btnSliderPanEqMain setHidden:NO];
                    [btnDynGMain setHidden:NO];
                    [btnDynEMain setHidden:NO];
                    [btnDynCMain setHidden:NO];
                    [btnDynLMain setHidden:NO];
                    [btnSliderPanEqMain setBackgroundImage:[UIImage imageNamed:@"pan-8.png"] forState:UIControlStateNormal];
                    break;
                default:
                    [btnSliderPanEqMain setHidden:NO];
                    [btnDynGMain setHidden:NO];
                    [btnDynEMain setHidden:NO];
                    [btnDynCMain setHidden:NO];
                    [btnDynLMain setHidden:NO];
                    [btnSliderPanEqMain setBackgroundImage:[UIImage imageNamed:@"pan-8.png"] forState:UIControlStateNormal];
                    break;
            }
            [self clearPanButtonImages];
            [self refreshDynButtons];
            break;

        case MODE_ORDER:
            switch (currentPage) {
                case PAGE_CH_1_8:
                case PAGE_CH_9_16:
                case PAGE_USB_AUDIO:
                case PAGE_MT_1_4:
                    [btnSliderPanEq1 setHidden:NO];
                    [btnSliderPanEq2 setHidden:NO];
                    [btnSliderPanEq3 setHidden:NO];
                    [btnSliderPanEq4 setHidden:NO];
                    [btnSliderPanEq5 setHidden:NO];
                    [btnSliderPanEq6 setHidden:NO];
                    [btnSliderPanEq7 setHidden:NO];
                    [btnSliderPanEq8 setHidden:NO];
                    [btnSliderPanEqMain setHidden:NO];
                    [lblOrderFirst1 setHidden:NO];
                    [lblOrderFirst2 setHidden:NO];
                    [lblOrderFirst3 setHidden:NO];
                    [lblOrderFirst4 setHidden:NO];
                    [lblOrderFirst5 setHidden:NO];
                    [lblOrderFirst6 setHidden:NO];
                    [lblOrderFirst7 setHidden:NO];
                    [lblOrderFirst8 setHidden:NO];
                    [lblOrderFirstMain setHidden:NO];
                    [lblOrderSecond1 setHidden:NO];
                    [lblOrderSecond2 setHidden:NO];
                    [lblOrderSecond3 setHidden:NO];
                    [lblOrderSecond4 setHidden:NO];
                    [lblOrderSecond5 setHidden:NO];
                    [lblOrderSecond6 setHidden:NO];
                    [lblOrderSecond7 setHidden:NO];
                    [lblOrderSecond8 setHidden:NO];
                    [lblOrderSecondMain setHidden:NO];
                    [lblOrderThird1 setHidden:NO];
                    [lblOrderThird2 setHidden:NO];
                    [lblOrderThird3 setHidden:NO];
                    [lblOrderThird4 setHidden:NO];
                    [lblOrderThird5 setHidden:NO];
                    [lblOrderThird6 setHidden:NO];
                    [lblOrderThird7 setHidden:NO];
                    [lblOrderThird8 setHidden:NO];
                    [lblOrderThirdMain setHidden:NO];
                    [btnSliderPanEq1 setBackgroundImage:[UIImage imageNamed:@"pan-18.png"] forState:UIControlStateNormal];
                    [btnSliderPanEq2 setBackgroundImage:[UIImage imageNamed:@"pan-18.png"] forState:UIControlStateNormal];
                    [btnSliderPanEq3 setBackgroundImage:[UIImage imageNamed:@"pan-18.png"] forState:UIControlStateNormal];
                    [btnSliderPanEq4 setBackgroundImage:[UIImage imageNamed:@"pan-18.png"] forState:UIControlStateNormal];
                    [btnSliderPanEq5 setBackgroundImage:[UIImage imageNamed:@"pan-18.png"] forState:UIControlStateNormal];
                    [btnSliderPanEq6 setBackgroundImage:[UIImage imageNamed:@"pan-18.png"] forState:UIControlStateNormal];
                    [btnSliderPanEq7 setBackgroundImage:[UIImage imageNamed:@"pan-18.png"] forState:UIControlStateNormal];
                    [btnSliderPanEq8 setBackgroundImage:[UIImage imageNamed:@"pan-18.png"] forState:UIControlStateNormal];
                    [btnSliderPanEqMain setBackgroundImage:[UIImage imageNamed:@"pan-18.png"] forState:UIControlStateNormal];
                    [self updateOrderButtons];
                    break;
                default:
                    [btnSliderPanEqMain setHidden:NO];
                    [lblOrderFirstMain setHidden:NO];
                    [lblOrderSecondMain setHidden:NO];
                    [lblOrderThirdMain setHidden:NO];
                    [btnSliderPanEqMain setBackgroundImage:[UIImage imageNamed:@"pan-18.png"] forState:UIControlStateNormal];
                    [self updateOrderButtons];
                    break;
            }
            [self clearPanButtonImages];
            break;
    }
}

- (void)refreshPage
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    // Hide AUX/Group Sends views
    if (auxSendController != nil && !auxSendController.view.hidden) {
        [auxSendController.view setHidden:YES];
    }
    if (groupSendController != nil && !groupSendController.view.hidden) {
        [groupSendController.view setHidden:YES];
    }
    if (geqPeqController != nil && !geqPeqController.view.hidden) {
        [geqPeqController.view setHidden:YES];
    }
    if (eqDynDelayController != nil && !eqDynDelayController.view.hidden) {
        [eqDynDelayController.view setHidden:YES];
    }
    if (dynController != nil && !dynController.view.hidden) {
        [dynController.view setHidden:YES];
    }
    if (!viewScenes.hidden) {
        [viewScenes setHidden:YES];
    }
    if (!viewSetup.hidden) {
        [viewSetup setHidden:YES];
        btnSetup.selected = NO;
        [self enableModeSelector:YES];
    }
    if (!viewUsbAudioOut.hidden) {
        [viewUsbAudioOut setHidden:YES];
    }
    if (!viewDelay.hidden) {
        [viewDelay setHidden:YES];
    }
    if (!viewOrder.hidden) {
        [viewOrder setHidden:YES];
    }
    
    [self refreshMeterButtons];
    [self refreshModeButtons];
    
    // Setup main channel
    [lblSliderMain setText:NSLocalizedStringFromTable(@"Main", @"Acapela", nil)];
    if ([userDefaults objectForKey:@"Main"] != nil) {
        [txtSliderMain setText:(NSString *)[userDefaults objectForKey:@"Main"]];
    } else {
        [txtSliderMain setText:NSLocalizedStringFromTable(@"Main", @"Acapela", nil)];
    }
    [btnSliderMainMono setTitle:@"STEREO" forState:UIControlStateNormal];
    [btnSliderMainMono setBackgroundImage:[UIImage imageNamed:@"button-4.png"] forState:UIControlStateNormal];
    [btnSliderMainMono setTitle:@"MONO" forState:UIControlStateSelected];
    [btnSliderMainMono setBackgroundImage:[UIImage imageNamed:@"button-5.png"] forState:UIControlStateSelected];
    [sliderMain setThumbImage:[UIImage imageNamed:@"fader-3.png"] forState:UIControlStateNormal];
    
    [effectController.view setHidden:YES];
    [effectController.effectGeq31Controller.view setHidden:YES];
    [effectController.effectGeq15Controller.view setHidden:YES];
    
    switch (currentPage) {
        case PAGE_CH_1_8:
            [self setSendPage:CHANNEL_PAGE reserve2:CHANNEL_PAGE_CH_1_8];
            [self hideAllSliders:NO];
            [self hideAllSoloButtons:NO];
            [btnSlider8On setHidden:NO];
            [meterCtrlRmL setHidden:YES];
            [meterCtrlRmR setHidden:YES];
            [imgSlider8 setImage:[UIImage imageNamed:@"frame-4.png"]];
            [lblSlider1 setText:NSLocalizedStringFromTable(@"Ch1", @"Acapela", nil)];
            [lblSlider2 setText:NSLocalizedStringFromTable(@"Ch2", @"Acapela", nil)];
            [lblSlider3 setText:NSLocalizedStringFromTable(@"Ch3", @"Acapela", nil)];
            [lblSlider4 setText:NSLocalizedStringFromTable(@"Ch4", @"Acapela", nil)];
            [lblSlider5 setText:NSLocalizedStringFromTable(@"Ch5", @"Acapela", nil)];
            [lblSlider6 setText:NSLocalizedStringFromTable(@"Ch6", @"Acapela", nil)];
            [lblSlider7 setText:NSLocalizedStringFromTable(@"Ch7", @"Acapela", nil)];
            [lblSlider8 setText:NSLocalizedStringFromTable(@"Ch8", @"Acapela", nil)];
            if ([userDefaults objectForKey:@"Ch1"] != nil) {
                [txtSlider1 setText:(NSString *)[userDefaults objectForKey:@"Ch1"]];
            } else {
                [txtSlider1 setText:NSLocalizedStringFromTable(@"Ch1", @"Acapela", nil)];
            }
            if ([userDefaults objectForKey:@"Ch2"] != nil) {
                [txtSlider2 setText:(NSString *)[userDefaults objectForKey:@"Ch2"]];
            } else {
                [txtSlider2 setText:NSLocalizedStringFromTable(@"Ch2", @"Acapela", nil)];
            }
            if ([userDefaults objectForKey:@"Ch3"] != nil) {
                [txtSlider3 setText:(NSString *)[userDefaults objectForKey:@"Ch3"]];
            } else {
                [txtSlider3 setText:NSLocalizedStringFromTable(@"Ch3", @"Acapela", nil)];
            }
            if ([userDefaults objectForKey:@"Ch4"] != nil) {
                [txtSlider4 setText:(NSString *)[userDefaults objectForKey:@"Ch4"]];
            } else {
                [txtSlider4 setText:NSLocalizedStringFromTable(@"Ch4", @"Acapela", nil)];
            }
            if ([userDefaults objectForKey:@"Ch5"] != nil) {
                [txtSlider5 setText:(NSString *)[userDefaults objectForKey:@"Ch5"]];
            } else {
                [txtSlider5 setText:NSLocalizedStringFromTable(@"Ch5", @"Acapela", nil)];
            }
            if ([userDefaults objectForKey:@"Ch6"] != nil) {
                [txtSlider6 setText:(NSString *)[userDefaults objectForKey:@"Ch6"]];
            } else {
                [txtSlider6 setText:NSLocalizedStringFromTable(@"Ch6", @"Acapela", nil)];
            }
            if ([userDefaults objectForKey:@"Ch7"] != nil) {
                [txtSlider7 setText:(NSString *)[userDefaults objectForKey:@"Ch7"]];
            } else {
                [txtSlider7 setText:NSLocalizedStringFromTable(@"Ch7", @"Acapela", nil)];
            }
            if ([userDefaults objectForKey:@"Ch8"] != nil) {
                [txtSlider8 setText:(NSString *)[userDefaults objectForKey:@"Ch8"]];
            } else {
                [txtSlider8 setText:NSLocalizedStringFromTable(@"Ch8", @"Acapela", nil)];
            }
            [slider1 setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            [slider2 setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            [slider3 setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            [slider4 setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            [slider5 setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            [slider6 setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            [slider7 setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            [slider8 setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            [self enableCtrlRmButtons:NO];
            currentPage = PAGE_CH_1_8;
            break;
            
        case PAGE_CH_9_16:
            [self setSendPage:CHANNEL_PAGE reserve2:CHANNEL_PAGE_CH_9_16];
            [self hideAllSliders:NO];
            [self hideAllSoloButtons:NO];
            [btnSlider8On setHidden:NO];
            [meterCtrlRmL setHidden:YES];
            [meterCtrlRmR setHidden:YES];
            [imgSlider8 setImage:[UIImage imageNamed:@"frame-4.png"]];
            [lblSlider1 setText:NSLocalizedStringFromTable(@"Ch9", @"Acapela", nil)];
            [lblSlider2 setText:NSLocalizedStringFromTable(@"Ch10", @"Acapela", nil)];
            [lblSlider3 setText:NSLocalizedStringFromTable(@"Ch11", @"Acapela", nil)];
            [lblSlider4 setText:NSLocalizedStringFromTable(@"Ch12", @"Acapela", nil)];
            [lblSlider5 setText:NSLocalizedStringFromTable(@"Ch13", @"Acapela", nil)];
            [lblSlider6 setText:NSLocalizedStringFromTable(@"Ch14", @"Acapela", nil)];
            [lblSlider7 setText:NSLocalizedStringFromTable(@"Ch15", @"Acapela", nil)];
            [lblSlider8 setText:NSLocalizedStringFromTable(@"Ch16", @"Acapela", nil)];
            if ([userDefaults objectForKey:@"Ch9"] != nil) {
                [txtSlider1 setText:(NSString *)[userDefaults objectForKey:@"Ch9"]];
            } else {
                [txtSlider1 setText:NSLocalizedStringFromTable(@"Ch9", @"Acapela", nil)];
            }
            if ([userDefaults objectForKey:@"Ch10"] != nil) {
                [txtSlider2 setText:(NSString *)[userDefaults objectForKey:@"Ch10"]];
            } else {
                [txtSlider2 setText:NSLocalizedStringFromTable(@"Ch10", @"Acapela", nil)];
            }
            if ([userDefaults objectForKey:@"Ch11"] != nil) {
                [txtSlider3 setText:(NSString *)[userDefaults objectForKey:@"Ch11"]];
            } else {
                [txtSlider3 setText:NSLocalizedStringFromTable(@"Ch11", @"Acapela", nil)];
            }
            if ([userDefaults objectForKey:@"Ch12"] != nil) {
                [txtSlider4 setText:(NSString *)[userDefaults objectForKey:@"Ch12"]];
            } else {
                [txtSlider4 setText:NSLocalizedStringFromTable(@"Ch12", @"Acapela", nil)];
            }
            if ([userDefaults objectForKey:@"Ch13"] != nil) {
                [txtSlider5 setText:(NSString *)[userDefaults objectForKey:@"Ch13"]];
            } else {
                [txtSlider5 setText:NSLocalizedStringFromTable(@"Ch13", @"Acapela", nil)];
            }
            if ([userDefaults objectForKey:@"Ch14"] != nil) {
                [txtSlider6 setText:(NSString *)[userDefaults objectForKey:@"Ch14"]];
            } else {
                [txtSlider6 setText:NSLocalizedStringFromTable(@"Ch14", @"Acapela", nil)];
            }
            if ([userDefaults objectForKey:@"Ch15"] != nil) {
                [txtSlider7 setText:(NSString *)[userDefaults objectForKey:@"Ch15"]];
            } else {
                [txtSlider7 setText:NSLocalizedStringFromTable(@"Ch15", @"Acapela", nil)];
            }
            if ([userDefaults objectForKey:@"Ch16"] != nil) {
                [txtSlider8 setText:(NSString *)[userDefaults objectForKey:@"Ch16"]];
            } else {
                [txtSlider8 setText:NSLocalizedStringFromTable(@"Ch16", @"Acapela", nil)];
            }
            [slider1 setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            [slider2 setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            [slider3 setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            [slider4 setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            [slider5 setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            [slider6 setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            [slider7 setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            [slider8 setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            [self enableCtrlRmButtons:NO];
            currentPage = PAGE_CH_9_16;
            break;
            
        case PAGE_USB_AUDIO:
            [self setSendPage:CHANNEL_PAGE reserve2:CHANNEL_PAGE_CH_17_18];
            [self hideAllSliders:YES];
            [self hideAllSoloButtons:YES];
            [btnSlider8On setHidden:NO];
            [viewSlider1 setHidden:NO];
            [slider1 setHidden:NO];
            [meter1 setHidden:NO];
            [viewSlider2 setHidden:NO];
            [slider2 setHidden:NO];
            [meter2 setHidden:NO];
            [btnSlider1Solo setHidden:NO];
            [btnSlider2Solo setHidden:NO];
            [meterCtrlRmL setHidden:YES];
            [meterCtrlRmR setHidden:YES];
            [lblSlider1 setText:NSLocalizedStringFromTable(@"Ch17", @"Acapela", nil)];
            [lblSlider2 setText:NSLocalizedStringFromTable(@"Ch18", @"Acapela", nil)];
            if ([userDefaults objectForKey:@"Ch17"] != nil) {
                [txtSlider1 setText:(NSString *)[userDefaults objectForKey:@"Ch17"]];
            } else {
                [txtSlider1 setText:NSLocalizedStringFromTable(@"Ch17", @"Acapela", nil)];
            }
            if ([userDefaults objectForKey:@"Ch18"] != nil) {
                [txtSlider2 setText:(NSString *)[userDefaults objectForKey:@"Ch18"]];
            } else {
                [txtSlider2 setText:NSLocalizedStringFromTable(@"Ch18", @"Acapela", nil)];
            }
            [slider1 setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            [slider2 setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            [self enableCtrlRmButtons:NO];
            currentPage = PAGE_USB_AUDIO;
            break;
            
        case PAGE_AUX_1_4:
            [self setSendPage:CHANNEL_PAGE reserve2:CHANNEL_PAGE_AUX_1_4];
            [self hideAllSliders:YES];
            [self hideAllSoloButtons:YES];
            [btnSlider8On setHidden:NO];
            [viewSlider1 setHidden:NO];
            [slider1 setHidden:NO];
            [meter1 setHidden:NO];
            [btnSlider1Solo setHidden:NO];
            [viewSlider2 setHidden:NO];
            [slider2 setHidden:NO];
            [meter2 setHidden:NO];
            [btnSlider2Solo setHidden:NO];
            [viewSlider3 setHidden:NO];
            [slider3 setHidden:NO];
            [meter3 setHidden:NO];
            [btnSlider3Solo setHidden:NO];
            [viewSlider4 setHidden:NO];
            [slider4 setHidden:NO];
            [meter4 setHidden:NO];
            [btnSlider4Solo setHidden:NO];
            [meterCtrlRmL setHidden:YES];
            [meterCtrlRmR setHidden:YES];
            [lblSlider1 setText:NSLocalizedStringFromTable(@"Aux1", @"Acapela", nil)];
            [lblSlider2 setText:NSLocalizedStringFromTable(@"Aux2", @"Acapela", nil)];
            [lblSlider3 setText:NSLocalizedStringFromTable(@"Aux3", @"Acapela", nil)];
            [lblSlider4 setText:NSLocalizedStringFromTable(@"Aux4", @"Acapela", nil)];
            if ([userDefaults objectForKey:@"Aux1"] != nil) {
                [txtSlider1 setText:(NSString *)[userDefaults objectForKey:@"Aux1"]];
            } else {
                [txtSlider1 setText:NSLocalizedStringFromTable(@"Aux1", @"Acapela", nil)];
            }
            if ([userDefaults objectForKey:@"Aux2"] != nil) {
                [txtSlider2 setText:(NSString *)[userDefaults objectForKey:@"Aux2"]];
            } else {
                [txtSlider2 setText:NSLocalizedStringFromTable(@"Aux2", @"Acapela", nil)];
            }
            if ([userDefaults objectForKey:@"Aux3"] != nil) {
                [txtSlider3 setText:(NSString *)[userDefaults objectForKey:@"Aux3"]];
            } else {
                [txtSlider3 setText:NSLocalizedStringFromTable(@"Aux3", @"Acapela", nil)];
            }
            if ([userDefaults objectForKey:@"Aux4"] != nil) {
                [txtSlider4 setText:(NSString *)[userDefaults objectForKey:@"Aux4"]];
            } else {
                [txtSlider4 setText:NSLocalizedStringFromTable(@"Aux4", @"Acapela", nil)];
            }
            [slider1 setThumbImage:[UIImage imageNamed:@"fader-2.png"] forState:UIControlStateNormal];
            [slider2 setThumbImage:[UIImage imageNamed:@"fader-2.png"] forState:UIControlStateNormal];
            [slider3 setThumbImage:[UIImage imageNamed:@"fader-2.png"] forState:UIControlStateNormal];
            [slider4 setThumbImage:[UIImage imageNamed:@"fader-2.png"] forState:UIControlStateNormal];
            [self enableCtrlRmButtons:NO];
            currentPage = PAGE_AUX_1_4;
            break;
            
        case PAGE_GP_1_4:
            [self setSendPage:CHANNEL_PAGE reserve2:CHANNEL_PAGE_GP_1_4];
            [self hideAllSliders:YES];
            [self hideAllSoloButtons:YES];
            [btnSlider8On setHidden:NO];
            [viewSlider1 setHidden:NO];
            [slider1 setHidden:NO];
            [meter1 setHidden:NO];
            [btnSlider1Solo setHidden:NO];
            [viewSlider2 setHidden:NO];
            [slider2 setHidden:NO];
            [meter2 setHidden:NO];
            [btnSlider2Solo setHidden:NO];
            [viewSlider3 setHidden:NO];
            [slider3 setHidden:NO];
            [meter3 setHidden:NO];
            [btnSlider3Solo setHidden:NO];
            [viewSlider4 setHidden:NO];
            [slider4 setHidden:NO];
            [meter4 setHidden:NO];
            [btnSlider4Solo setHidden:NO];
            [meterCtrlRmL setHidden:YES];
            [meterCtrlRmR setHidden:YES];
            [lblSlider1 setText:NSLocalizedStringFromTable(@"Gp1", @"Acapela", nil)];
            [lblSlider2 setText:NSLocalizedStringFromTable(@"Gp2", @"Acapela", nil)];
            [lblSlider3 setText:NSLocalizedStringFromTable(@"Gp3", @"Acapela", nil)];
            [lblSlider4 setText:NSLocalizedStringFromTable(@"Gp4", @"Acapela", nil)];
            if ([userDefaults objectForKey:@"Gp1"] != nil) {
                [txtSlider1 setText:(NSString *)[userDefaults objectForKey:@"Gp1"]];
            } else {
                [txtSlider1 setText:NSLocalizedStringFromTable(@"Gp1", @"Acapela", nil)];
            }
            if ([userDefaults objectForKey:@"Gp2"] != nil) {
                [txtSlider2 setText:(NSString *)[userDefaults objectForKey:@"Gp2"]];
            } else {
                [txtSlider2 setText:NSLocalizedStringFromTable(@"Gp2", @"Acapela", nil)];
            }
            if ([userDefaults objectForKey:@"Gp3"] != nil) {
                [txtSlider3 setText:(NSString *)[userDefaults objectForKey:@"Gp3"]];
            } else {
                [txtSlider3 setText:NSLocalizedStringFromTable(@"Gp3", @"Acapela", nil)];
            }
            if ([userDefaults objectForKey:@"Gp4"] != nil) {
                [txtSlider4 setText:(NSString *)[userDefaults objectForKey:@"Gp4"]];
            } else {
                [txtSlider4 setText:NSLocalizedStringFromTable(@"Gp4", @"Acapela", nil)];
            }
            [slider1 setThumbImage:[UIImage imageNamed:@"fader-5.png"] forState:UIControlStateNormal];
            [slider2 setThumbImage:[UIImage imageNamed:@"fader-5.png"] forState:UIControlStateNormal];
            [slider3 setThumbImage:[UIImage imageNamed:@"fader-5.png"] forState:UIControlStateNormal];
            [slider4 setThumbImage:[UIImage imageNamed:@"fader-5.png"] forState:UIControlStateNormal];
            [self enableCtrlRmButtons:NO];
            currentPage = PAGE_GP_1_4;
            break;
            
        case PAGE_EFX_1:
            [self setSendPage:EFFECT_PAGE reserve2:0];
            [self hideAllSliders:YES];
            [self hideAllSoloButtons:YES];
            [btnSlider8On setHidden:NO];
            [meterCtrlRmL setHidden:YES];
            [meterCtrlRmR setHidden:YES];
            [effectController setEffectNum:EFX_1];
            [effectController.view setHidden:NO];
            [lblSliderMain setText:NSLocalizedStringFromTable(@"Efx1", @"Acapela", nil)];
            if ([userDefaults objectForKey:@"Efx1"] != nil) {
                [txtSliderMain setText:(NSString *)[userDefaults objectForKey:@"Efx1"]];
            } else {
                [txtSliderMain setText:NSLocalizedStringFromTable(@"Efx1", @"Acapela", nil)];
            }
            [self enableCtrlRmButtons:NO];
            currentPage = PAGE_EFX_1;
            break;
            
        case PAGE_EFX_2:
            [self setSendPage:EFFECT_PAGE reserve2:0];
            [self hideAllSliders:YES];
            [self hideAllSoloButtons:YES];
            [btnSlider8On setHidden:NO];
            [meterCtrlRmL setHidden:YES];
            [meterCtrlRmR setHidden:YES];
            [effectController setEffectNum:EFX_2];
            [effectController.view setHidden:NO];
            [lblSliderMain setText:NSLocalizedStringFromTable(@"Efx2", @"Acapela", nil)];
            if ([userDefaults objectForKey:@"Efx2"] != nil) {
                [txtSliderMain setText:(NSString *)[userDefaults objectForKey:@"Efx2"]];
            } else {
                [txtSliderMain setText:NSLocalizedStringFromTable(@"Efx2", @"Acapela", nil)];
            }
            [self enableCtrlRmButtons:NO];
            currentPage = PAGE_EFX_2;
            break;
        
        case PAGE_MT_1_4:
            [self setSendPage:CHANNEL_PAGE reserve2:CHANNEL_PAGE_MULTI_1_4];
            [self hideAllSliders:YES];
            [self hideAllSoloButtons:YES];
            [btnSlider8On setHidden:NO];
            [viewSlider1 setHidden:NO];
            [slider1 setHidden:NO];
            [meter1 setHidden:NO];
            [viewSlider2 setHidden:NO];
            [slider2 setHidden:NO];
            [meter2 setHidden:NO];
            [viewSlider3 setHidden:NO];
            [slider3 setHidden:NO];
            [meter3 setHidden:NO];
            [viewSlider4 setHidden:NO];
            [slider4 setHidden:NO];
            [meter4 setHidden:NO];
            [meterCtrlRmL setHidden:YES];
            [meterCtrlRmR setHidden:YES];
            [lblSlider1 setText:NSLocalizedStringFromTable(@"Multi1", @"Acapela", nil)];
            [lblSlider2 setText:NSLocalizedStringFromTable(@"Multi2", @"Acapela", nil)];
            [lblSlider3 setText:NSLocalizedStringFromTable(@"Multi3", @"Acapela", nil)];
            [lblSlider4 setText:NSLocalizedStringFromTable(@"Multi4", @"Acapela", nil)];
            if ([userDefaults objectForKey:@"Multi1"] != nil) {
                [txtSlider1 setText:(NSString *)[userDefaults objectForKey:@"Multi1"]];
            } else {
                [txtSlider1 setText:NSLocalizedStringFromTable(@"Multi1", @"Acapela", nil)];
            }
            if ([userDefaults objectForKey:@"Multi2"] != nil) {
                [txtSlider2 setText:(NSString *)[userDefaults objectForKey:@"Multi2"]];
            } else {
                [txtSlider2 setText:NSLocalizedStringFromTable(@"Multi2", @"Acapela", nil)];
            }
            if ([userDefaults objectForKey:@"Multi3"] != nil) {
                [txtSlider3 setText:(NSString *)[userDefaults objectForKey:@"Multi3"]];
            } else {
                [txtSlider3 setText:NSLocalizedStringFromTable(@"Multi3", @"Acapela", nil)];
            }
            if ([userDefaults objectForKey:@"Multi4"] != nil) {
                [txtSlider4 setText:(NSString *)[userDefaults objectForKey:@"Multi4"]];
            } else {
                [txtSlider4 setText:NSLocalizedStringFromTable(@"Multi4", @"Acapela", nil)];
            }
            [slider1 setThumbImage:[UIImage imageNamed:@"fader-4.png"] forState:UIControlStateNormal];
            [slider2 setThumbImage:[UIImage imageNamed:@"fader-4.png"] forState:UIControlStateNormal];
            [slider3 setThumbImage:[UIImage imageNamed:@"fader-4.png"] forState:UIControlStateNormal];
            [slider4 setThumbImage:[UIImage imageNamed:@"fader-4.png"] forState:UIControlStateNormal];
            [self enableCtrlRmButtons:NO];
            currentPage = PAGE_MT_1_4;
            break;
            
        case PAGE_USB_AUDIO_OUT:
            break;
            
        case PAGE_CTRL_RM:
        case PAGE_MAIN:
            [self setSendPage:CHANNEL_PAGE reserve2:CHANNEL_PAGE_CTRL];
            [btnSliderPanEq7 setHidden:YES];
            [btnSliderPanEq8 setHidden:YES];
            [self hideAllSliders:YES];
            [self hideAllSoloButtons:NO];
            [viewSlider8 setHidden:NO];
            [imgSlider8 setImage:[UIImage imageNamed:@"frame-7.png"]];
            [slider8 setHidden:NO];
            [meter8 setHidden:YES];
            [btnSlider8On setHidden:YES];
            [meterCtrlRmL setHidden:NO];
            [meterCtrlRmR setHidden:NO];
            [lblSlider8 setText:NSLocalizedStringFromTable(@"CtrlRm", @"Acapela", nil)];
            if ([userDefaults objectForKey:@"CtrlRm"] != nil) {
                [txtSlider8 setText:(NSString *)[userDefaults objectForKey:@"CtrlRm"]];
            } else {
                [txtSlider8 setText:NSLocalizedStringFromTable(@"CtrlRm", @"Acapela", nil)];
            }
            [self enableCtrlRmButtons:YES];
            currentPage = PAGE_MAIN;
            break;
    }
}

- (void)refreshModePage
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    [viewSetup setHidden:YES];
    [viewScenes setHidden:YES];
    [effectController.view setHidden:YES];
    [effectController.effectGeq31Controller.view setHidden:YES];
    [effectController.effectGeq15Controller.view setHidden:YES];
    [geqPeqController.view setHidden:YES];
    if (auxSendController != nil) {
        [auxSendController.view setHidden:YES];
    }
    if (groupSendController != nil) {
        [groupSendController.view setHidden:YES];
    }
    if (eqDynDelayController != nil) {
        [eqDynDelayController.view setHidden:YES];
    }
    if (dynController != nil) {
        [dynController.view setHidden:YES];
    }
    [viewUsbAudioOut setHidden:YES];
    [viewDelay setHidden:YES];
    [viewOrder setHidden:YES];
    
    [btnSliderMainMono setTitle:@"STEREO" forState:UIControlStateNormal];
    [btnSliderMainMono setBackgroundImage:[UIImage imageNamed:@"button-4.png"] forState:UIControlStateNormal];
    [btnSliderMainMono setTitle:@"MONO" forState:UIControlStateSelected];
    [btnSliderMainMono setBackgroundImage:[UIImage imageNamed:@"button-5.png"] forState:UIControlStateSelected];
    [sliderMain setThumbImage:[UIImage imageNamed:@"fader-3.png"] forState:UIControlStateNormal];
    
    switch (currentPage) {
        case PAGE_CH_1_8:
            [self setSendPage:VIEW_PAGE reserve2:VIEW_PAGE_TYPE_CH_1_16];
            if (eqDynDelayController == nil) {
                eqDynDelayController = (EqDynDelayViewController *)[[EqDynDelayViewController alloc] initWithNibName:@"EqDynDelayViewController" bundle:nil];
                [eqDynDelayController.view setFrame:CGRectMake(0, 153, 1024, 615)];
                [self.view addSubview:eqDynDelayController.view];
                [self.view bringSubviewToFront:viewLabelEdit];
            } else {
                eqDynDelayController.view.hidden = NO;
            }
            [eqDynDelayController setViewPage:VIEW_PAGE_CH_1_8];
            break;
        case PAGE_CH_9_16:
            [self setSendPage:VIEW_PAGE reserve2:VIEW_PAGE_TYPE_CH_1_16];
            if (eqDynDelayController == nil) {
                eqDynDelayController = (EqDynDelayViewController *)[[EqDynDelayViewController alloc] initWithNibName:@"EqDynDelayViewController" bundle:nil];
                [eqDynDelayController.view setFrame:CGRectMake(0, 153, 1024, 615)];
                [self.view addSubview:eqDynDelayController.view];
                [self.view bringSubviewToFront:viewLabelEdit];
            } else {
                eqDynDelayController.view.hidden = NO;
            }
            [eqDynDelayController setViewPage:VIEW_PAGE_CH_9_16];
            break;
        case PAGE_USB_AUDIO:
            [self setSendPage:VIEW_PAGE reserve2:VIEW_PAGE_TYPE_CH_17_18];
            if (eqDynDelayController == nil) {
                eqDynDelayController = (EqDynDelayViewController *)[[EqDynDelayViewController alloc] initWithNibName:@"EqDynDelayViewController" bundle:nil];
                [eqDynDelayController.view setFrame:CGRectMake(0, 153, 1024, 615)];
                [self.view addSubview:eqDynDelayController.view];
                [self.view bringSubviewToFront:viewLabelEdit];
            } else {
                eqDynDelayController.view.hidden = NO;
            }
            [eqDynDelayController setViewPage:VIEW_PAGE_USB_AUDIO];
            break;
        case PAGE_AUX_1_4:
            [self setSendPage:AUX_GP_PAGE reserve2:0];
            if (auxSendController == nil) {
                auxSendController = (AuxSendViewController *)[[AuxSendViewController alloc] initWithNibName:@"AuxSendViewController" bundle:nil];
                [auxSendController.view setFrame:CGRectMake(170, 153, 854, 615)];
                [self.view addSubview:auxSendController.view];
                [self.view bringSubviewToFront:viewLabelEdit];
            } else {
                auxSendController.view.hidden = !auxSendController.view.isHidden;
            }
            break;
        case PAGE_GP_1_4:
            [self setSendPage:AUX_GP_PAGE reserve2:1];
            if (groupSendController == nil) {
                groupSendController = (GroupSendViewController *)[[GroupSendViewController alloc] initWithNibName:@"GroupSendViewController" bundle:nil];
                [groupSendController.view setFrame:CGRectMake(170, 153, 854, 615)];
                [self.view addSubview:groupSendController.view];
                [self.view bringSubviewToFront:viewLabelEdit];
            } else {
                groupSendController.view.hidden = !groupSendController.view.isHidden;
            }
            break;
        case PAGE_EFX_1:
            [self setSendPage:EFFECT_PAGE reserve2:0];
            [self hideAllSliders:YES];
            [self hideAllSoloButtons:YES];
            [meterCtrlRmL setHidden:YES];
            [meterCtrlRmR setHidden:YES];
            [effectController setEffectNum:EFX_1];
            [effectController.view setHidden:NO];
            [lblSliderMain setText:NSLocalizedStringFromTable(@"Efx1", @"Acapela", nil)];
            if ([userDefaults objectForKey:@"Efx1"] != nil) {
                [txtSliderMain setText:(NSString *)[userDefaults objectForKey:@"Efx1"]];
            } else {
                [txtSliderMain setText:NSLocalizedStringFromTable(@"Efx1", @"Acapela", nil)];
            }
            [btnSliderMainMeter setHidden:YES];
            [sliderMain setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            [btnSliderMainMono setTitle:@"SOLO" forState:UIControlStateNormal];
            [btnSliderMainMono setBackgroundImage:[UIImage imageNamed:@"button-2.png"] forState:UIControlStateNormal];
            [btnSliderMainMono setTitle:@"SOLO" forState:UIControlStateSelected];
            [btnSliderMainMono setBackgroundImage:[UIImage imageNamed:@"button-4.png"] forState:UIControlStateSelected];
            [self enableCtrlRmButtons:NO];
            [btnSliderPanEqMain setHidden:YES];
            break;            
        case PAGE_EFX_2:
            [self setSendPage:EFFECT_PAGE reserve2:0];
            [self hideAllSliders:YES];
            [self hideAllSoloButtons:YES];
            [meterCtrlRmL setHidden:YES];
            [meterCtrlRmR setHidden:YES];
            [effectController setEffectNum:EFX_2];
            [effectController.view setHidden:NO];
            [lblSliderMain setText:NSLocalizedStringFromTable(@"Efx2", @"Acapela", nil)];
            if ([userDefaults objectForKey:@"Efx2"] != nil) {
                [txtSliderMain setText:(NSString *)[userDefaults objectForKey:@"Efx2"]];
            } else {
                [txtSliderMain setText:NSLocalizedStringFromTable(@"Efx2", @"Acapela", nil)];
            }
            [btnSliderMainMeter setHidden:YES];
            [sliderMain setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            [btnSliderMainMono setTitle:@"SOLO" forState:UIControlStateNormal];
            [btnSliderMainMono setBackgroundImage:[UIImage imageNamed:@"button-2.png"] forState:UIControlStateNormal];
            [btnSliderMainMono setTitle:@"SOLO" forState:UIControlStateSelected];
            [btnSliderMainMono setBackgroundImage:[UIImage imageNamed:@"button-4.png"] forState:UIControlStateSelected];
            [self enableCtrlRmButtons:NO];
            [btnSliderPanEqMain setHidden:YES];
            break;
        case PAGE_MT_1_4:
            [self setSendPage:VIEW_PAGE reserve2:VIEW_PAGE_TYPE_MULTI];
            if (eqDynDelayController == nil) {
                eqDynDelayController = (EqDynDelayViewController *)[[EqDynDelayViewController alloc] initWithNibName:@"EqDynDelayViewController" bundle:nil];
                [eqDynDelayController.view setFrame:CGRectMake(0, 153, 1024, 615)];
                [self.view addSubview:eqDynDelayController.view];
                [self.view bringSubviewToFront:viewLabelEdit];
            } else {
                eqDynDelayController.view.hidden = NO;
            }
            [eqDynDelayController setViewPage:VIEW_PAGE_MT_1_4];
            break;
        case PAGE_USB_AUDIO_OUT:
            [self setSendPage:SETUP_CTRLRM_USB_ASSIGN_PAGE reserve2:0];
            [viewUsbAudioOut setHidden:NO];
            break;
        case PAGE_CTRL_RM:
            [self setSendPage:SETUP_CTRLRM_USB_ASSIGN_PAGE reserve2:0];
            if (eqDynDelayController == nil) {
                eqDynDelayController = (EqDynDelayViewController *)[[EqDynDelayViewController alloc] initWithNibName:@"EqDynDelayViewController" bundle:nil];
                [eqDynDelayController.view setFrame:CGRectMake(0, 153, 1024, 615)];
                [self.view addSubview:eqDynDelayController.view];
                [self.view bringSubviewToFront:viewLabelEdit];
            } else {
                eqDynDelayController.view.hidden = NO;
            }
            [eqDynDelayController setViewPage:VIEW_PAGE_CTRL_RM];
            break;
        case PAGE_MAIN:
            [self setSendPage:VIEW_PAGE reserve2:VIEW_PAGE_TYPE_MAIN];
            if (eqDynDelayController == nil) {
                eqDynDelayController = (EqDynDelayViewController *)[[EqDynDelayViewController alloc] initWithNibName:@"EqDynDelayViewController" bundle:nil];
                [eqDynDelayController.view setFrame:CGRectMake(0, 153, 1024, 615)];
                [self.view addSubview:eqDynDelayController.view];
                [self.view bringSubviewToFront:viewLabelEdit];
            } else {
                eqDynDelayController.view.hidden = NO;
            }
            [eqDynDelayController setViewPage:VIEW_PAGE_MAIN];
            break;
    }
}

- (void)resetTopPanelButtons
{
    // Reset title colors
    [btnCh1_8 setSelected:NO];
    [btnCh9_16 setSelected:NO];
    [btnUsbAudio setSelected:NO];
    [btnAux1_4 setSelected:NO];
    [btnGp1_4 setSelected:NO];
    [btnEfx1 setSelected:NO];
    [btnEfx2 setSelected:NO];
    [btnMulti1_4 setSelected:NO];
    [btnUsbAudioOut setSelected:NO];
    [btnCtrlRm setSelected:NO];
    [btnMain setSelected:NO];
    [btnCh1_8 setBackgroundImage:[UIImage imageNamed:@"button-0.png"] forState:UIControlStateSelected];
    [btnCh9_16 setBackgroundImage:[UIImage imageNamed:@"button-0.png"] forState:UIControlStateSelected];
    [btnUsbAudio setBackgroundImage:[UIImage imageNamed:@"button-0.png"] forState:UIControlStateSelected];
    [btnAux1_4 setBackgroundImage:[UIImage imageNamed:@"button-0.png"] forState:UIControlStateSelected];
    [btnGp1_4 setBackgroundImage:[UIImage imageNamed:@"button-0.png"] forState:UIControlStateSelected];
    [btnEfx1 setBackgroundImage:[UIImage imageNamed:@"button-0.png"] forState:UIControlStateSelected];
    [btnEfx2 setBackgroundImage:[UIImage imageNamed:@"button-0.png"] forState:UIControlStateSelected];
    [btnMulti1_4 setBackgroundImage:[UIImage imageNamed:@"button-0.png"] forState:UIControlStateSelected];
    [btnUsbAudioOut setBackgroundImage:[UIImage imageNamed:@"button-0.png"] forState:UIControlStateSelected];
    [btnCtrlRm setBackgroundImage:[UIImage imageNamed:@"button-0.png"] forState:UIControlStateSelected];
    [btnMain setBackgroundImage:[UIImage imageNamed:@"button-0.png"] forState:UIControlStateSelected];
    
    // Reset meter buttons
    [btnMeterCh1_8 setSelected:NO];
    [btnMeterCh9_16 setSelected:NO];
    [btnMeterUsbAudio setSelected:NO];
    [btnMeterAux1_4 setSelected:NO];
    [btnMeterGp1_4 setSelected:NO];
    [btnMeterEfx1 setSelected:NO];
    [btnMeterEfx2 setSelected:NO];
    [btnMeterMulti1_4 setSelected:NO];
    [btnMeterUsbAudioOut setSelected:NO];
    [btnMeterCtrlRm setSelected:NO];
    [btnMeterMain setSelected:NO];
}

- (void)refreshSliderOnButtons:(unsigned char)value
{
    // If send command exists, don't update UI
    switch (currentPage) {
        case PAGE_CH_1_8:
        case PAGE_CH_9_16:
            if ([self sendCommandExists:CMD_CH_ON_OFF dspId:DSP_5]) {
                NSLog(@"refreshSliderOnButtons: Send command exists!");
                return;
            }
            break;
        default:
            break;
    }
    
    BOOL slider1On = value & 0x01;
    if (slider1On != btnSlider1On.selected) {
        btnSlider1On.selected = slider1On;
    }
    
    BOOL slider2On = (value & 0x02) >> 1;
    if (slider2On != btnSlider2On.selected) {
        btnSlider2On.selected = slider2On;
    }
    
    BOOL slider3On = (value & 0x04) >> 2;
    if (slider3On != btnSlider3On.selected) {
        btnSlider3On.selected = slider3On;
    }
    
    BOOL slider4On = (value & 0x08) >> 3;
    if (slider4On != btnSlider4On.selected) {
        btnSlider4On.selected = slider4On;
    }
    
    BOOL slider5On = (value & 0x10) >> 4;
    if (slider5On != btnSlider5On.selected) {
        btnSlider5On.selected = slider5On;
    }
    
    BOOL slider6On = (value & 0x20) >> 5;
    if (slider6On != btnSlider6On.selected) {
        btnSlider6On.selected = slider6On;
    }
    
    BOOL slider7On = (value & 0x40) >> 6;
    if (slider7On != btnSlider7On.selected) {
        btnSlider7On.selected = slider7On;
    }
    
    BOOL slider8On = (value & 0x80) >> 7;
    if (slider8On != btnSlider8On.selected) {
        btnSlider8On.selected = slider8On;
    }
}

- (void)refreshEffectButtons:(unsigned char)value
{
    BOOL effect1On = (value & 0x02) >> 1;
    if (effect1On != btnSlider7On.selected) {
        btnSlider7On.selected = effect1On;
    }
    
    BOOL effect2On = (value & 0x04) >> 2;
    if (effect2On != btnSlider8On.selected) {
        btnSlider8On.selected = effect2On;
    }
    
    BOOL effect1Solo = (value & 0x40) >> 6;
    if (effect1Solo != btnSlider7Solo.selected) {
        btnSlider7Solo.selected = effect1Solo;
    }
    
    BOOL effect2Solo = (value & 0x80) >> 7;
    if (effect2Solo != btnSlider8Solo.selected) {
        btnSlider8Solo.selected = effect2Solo;
    }
}

- (void)refreshAesEbuCtrlRmButtons:(unsigned char)value
{
    BOOL aesOn = value & 0x01;
    if (aesOn != btnSlider7On.selected) {
        btnSlider7On.selected = aesOn;
    }
    
    BOOL ctrlRmMonoStereo = (value & 0x10) >> 4;
    if (ctrlRmMonoStereo != btnSlider8On.selected) {
        btnSlider8On.selected = ctrlRmMonoStereo;
    }
    
    BOOL aesSolo = (value & 0x20) >> 5;
    if (aesSolo != btnSlider7Solo.selected) {
        btnSlider7Solo.selected = aesSolo;
    }
}

- (void)refreshSliderSoloButtons:(unsigned char)value
{
    // If send command exists, don't update UI
    switch (currentPage) {
        case PAGE_CH_1_8:
        case PAGE_CH_9_16:
            if ([self sendCommandExists:CMD_SOLO_CTRL dspId:DSP_5]) {
                NSLog(@"refreshSliderSoloButtons: Send command exists!");
                return;
            }
            break;
        default:
            break;
    }
    
    BOOL slider1Solo = value & 0x01;
    if (slider1Solo != btnSlider1Solo.selected) {
        btnSlider1Solo.selected = slider1Solo;
    }
    
    BOOL slider2Solo = (value & 0x02) >> 1;
    if (slider2Solo != btnSlider2Solo.selected) {
        btnSlider2Solo.selected = slider2Solo;
    }
    
    BOOL slider3Solo = (value & 0x04) >> 2;
    if (slider3Solo != btnSlider3Solo.selected) {
        btnSlider3Solo.selected = slider3Solo;
    }
    
    BOOL slider4Solo = (value & 0x08) >> 3;
    if (slider4Solo != btnSlider4Solo.selected) {
        btnSlider4Solo.selected = slider4Solo;
    }
    
    BOOL slider5Solo = (value & 0x10) >> 4;
    if (slider5Solo != btnSlider5Solo.selected) {
        btnSlider5Solo.selected = slider5Solo;
    }
    
    BOOL slider6Solo = (value & 0x20) >> 5;
    if (slider6Solo != btnSlider6Solo.selected) {
        btnSlider6Solo.selected = slider6Solo;
    }
    
    BOOL slider7Solo = (value & 0x40) >> 6;
    if (slider7Solo != btnSlider7Solo.selected) {
        btnSlider7Solo.selected = slider7Solo;
    }
    
    BOOL slider8Solo = (value & 0x80) >> 7;
    if (slider8Solo != btnSlider8Solo.selected) {
        btnSlider8Solo.selected = slider8Solo;
    }
}

- (void)refreshSliderSoloSafeButtons:(unsigned char)value
{
    // If send command exists, don't update UI
    switch (currentPage) {
        case PAGE_CH_1_8:
        case PAGE_CH_9_16:
            if ([self sendCommandExists:CMD_SOLO_SAFE_CTRL dspId:DSP_5]) {
                NSLog(@"refreshSliderSoloSafeButtons: Send command exists!");
                return;
            }
            break;
        default:
            break;
    }
    
    BOOL slider1SoloSafe = value & 0x01;
    if (slider1SoloSafe) {
        [btnSlider1Solo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
        [btnSlider1Solo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
    } else {
        [btnSlider1Solo setTitle:@"SOLO" forState:UIControlStateNormal];
        [btnSlider1Solo setTitle:@"SOLO" forState:UIControlStateSelected];
    }
    
    BOOL slider2SoloSafe = (value & 0x02) >> 1;
    if (slider2SoloSafe) {
        [btnSlider2Solo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
        [btnSlider2Solo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
    } else {
        [btnSlider2Solo setTitle:@"SOLO" forState:UIControlStateNormal];
        [btnSlider2Solo setTitle:@"SOLO" forState:UIControlStateSelected];
    }
    
    BOOL slider3SoloSafe = (value & 0x04) >> 2;
    if (slider3SoloSafe) {
        [btnSlider3Solo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
        [btnSlider3Solo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
    } else {
        [btnSlider3Solo setTitle:@"SOLO" forState:UIControlStateNormal];
        [btnSlider3Solo setTitle:@"SOLO" forState:UIControlStateSelected];
    }
    
    BOOL slider4SoloSafe = (value & 0x08) >> 3;
    if (slider4SoloSafe) {
        [btnSlider4Solo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
        [btnSlider4Solo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
    } else {
        [btnSlider4Solo setTitle:@"SOLO" forState:UIControlStateNormal];
        [btnSlider4Solo setTitle:@"SOLO" forState:UIControlStateSelected];
    }
    
    BOOL slider5SoloSafe = (value & 0x10) >> 4;
    if (slider5SoloSafe) {
        [btnSlider5Solo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
        [btnSlider5Solo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
    } else {
        [btnSlider5Solo setTitle:@"SOLO" forState:UIControlStateNormal];
        [btnSlider5Solo setTitle:@"SOLO" forState:UIControlStateSelected];
    }
    
    BOOL slider6SoloSafe = (value & 0x20) >> 5;
    if (slider6SoloSafe) {
        [btnSlider6Solo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
        [btnSlider6Solo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
    } else {
        [btnSlider6Solo setTitle:@"SOLO" forState:UIControlStateNormal];
        [btnSlider6Solo setTitle:@"SOLO" forState:UIControlStateSelected];
    }
    
    BOOL slider7SoloSafe = (value & 0x40) >> 6;
    if (slider7SoloSafe) {
        [btnSlider7Solo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
        [btnSlider7Solo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
    } else {
        [btnSlider7Solo setTitle:@"SOLO" forState:UIControlStateNormal];
        [btnSlider7Solo setTitle:@"SOLO" forState:UIControlStateSelected];
    }
    
    BOOL slider8SoloSafe = (value & 0x80) >> 7;
    if (slider8SoloSafe) {
        [btnSlider8Solo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
        [btnSlider8Solo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
    } else {
        [btnSlider8Solo setTitle:@"SOLO" forState:UIControlStateNormal];
        [btnSlider8Solo setTitle:@"SOLO" forState:UIControlStateSelected];
    }
}

- (void)refreshSliderMeterButtons:(unsigned char)value
{
    // If send command exists, don't update UI
    switch (currentPage) {
        case PAGE_CH_1_8:
        case PAGE_CH_9_16:
            if ([self sendCommandExists:CMD_CH_METER dspId:DSP_5]) {
                NSLog(@"refreshSliderMeterButtons: Send command exists!");
                return;
            }
            break;
        default:
            break;
    }

    BOOL slider1Meter = value & 0x01;
    if (slider1Meter != btnSlider1Meter.selected) {
        btnSlider1Meter.selected = slider1Meter;
    }
    
    BOOL slider2Meter = (value & 0x02) >> 1;
    if (slider2Meter != btnSlider2Meter.selected) {
        btnSlider2Meter.selected = slider2Meter;
    }
    
    BOOL slider3Meter = (value & 0x04) >> 2;
    if (slider3Meter != btnSlider3Meter.selected) {
        btnSlider3Meter.selected = slider3Meter;
    }
    
    BOOL slider4Meter = (value & 0x08) >> 3;
    if (slider4Meter != btnSlider4Meter.selected) {
        btnSlider4Meter.selected = slider4Meter;
    }
    
    BOOL slider5Meter = (value & 0x10) >> 4;
    if (slider5Meter != btnSlider5Meter.selected) {
        btnSlider5Meter.selected = slider5Meter;
    }
    
    BOOL slider6Meter = (value & 0x20) >> 5;
    if (slider6Meter != btnSlider6Meter.selected) {
        btnSlider6Meter.selected = slider6Meter;
    }
    
    BOOL slider7Meter = (value & 0x40) >> 6;
    if (slider7Meter != btnSlider7Meter.selected) {
        btnSlider7Meter.selected = slider7Meter;
    }
    
    BOOL slider8Meter = (value & 0x80) >> 7;
    if (slider8Meter != btnSlider8Meter.selected) {
        btnSlider8Meter.selected = slider8Meter;
    }
}

- (void)showGeqPeqDialog:(int)channel
{
    if (geqPeqController == nil) {
        geqPeqController = (GeqPeqViewController *)[[GeqPeqViewController alloc] initWithNibName:@"GeqPeqView" bundle:nil];
        [geqPeqController.view setFrame:CGRectMake(0, 153, 1024, 615)];
        [self.view addSubview:geqPeqController.view];
        [self.view bringSubviewToFront:viewLabelEdit];
        geqPeqController.delegate = self;
    } else {
        [self.view bringSubviewToFront:geqPeqController.view];
        [self.view bringSubviewToFront:viewLabelEdit];
        geqPeqController.view.hidden = NO;
    }
    
    geqPeqController.currentChannel = channel;
    [geqPeqController updateChannelLabels];
    [geqPeqController updatePeqFrame];
}

- (void)showDynDialog:(int)channel
{
    if (dynController == nil) {
        dynController = (DynViewController *)[[DynViewController alloc] initWithNibName:@"DynViewController" bundle:nil];
        [dynController.view setFrame:CGRectMake(0, 153, 1024, 615)];
        [self.view addSubview:dynController.view];
        [self.view bringSubviewToFront:viewLabelEdit];
        dynController.delegate = self;
    } else {
        [self.view bringSubviewToFront:dynController.view];
        [self.view bringSubviewToFront:viewLabelEdit];
        dynController.view.hidden = NO;
    }
    
    [dynController setDynChannel:channel];
}

#pragma mark -
#pragma mark ViewController main methods

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	// Do any additional setup after loading the view, typically from a nib.
    _loginViewShown = NO;
    
    // Set version number
    NSString *version = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleVersion"];
    [lblVersion setText:[NSString stringWithFormat:@"%@", version]];
    
    // Register for keyboard notifications
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardDidShow:)
                                                 name:UIKeyboardDidShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
    // Customize labels to be multiple line
    [btnUsbAudio.titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
    [btnUsbAudio.titleLabel setNumberOfLines:0];
    [btnUsbAudio.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [btnUsbAudio setTitle:@"CH 17/18\nUSB AUDIO" forState:UIControlStateNormal];
    [btnUsbAudioOut.titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
    [btnUsbAudioOut.titleLabel setNumberOfLines:0];
    [btnUsbAudioOut.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [btnUsbAudioOut setTitle:@"USB AUDIO\nOUT" forState:UIControlStateNormal];
    [btnSine100Hz.titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
    [btnSine100Hz.titleLabel setNumberOfLines:0];
    [btnSine100Hz.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [btnSine100Hz setTitle:@"SINE\n100Hz" forState:UIControlStateNormal];
    [btnSine1kHz.titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
    [btnSine1kHz.titleLabel setNumberOfLines:0];
    [btnSine1kHz.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [btnSine1kHz setTitle:@"SINE\n1kHz" forState:UIControlStateNormal];
    [btnSine10kHz.titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
    [btnSine10kHz.titleLabel setNumberOfLines:0];
    [btnSine10kHz.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [btnSine10kHz setTitle:@"SINE\n10kHz" forState:UIControlStateNormal];
    [btnPinkNoise.titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
    [btnPinkNoise.titleLabel setNumberOfLines:0];
    [btnPinkNoise.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [btnPinkNoise setTitle:@"PINK\nNOISE" forState:UIControlStateNormal];    
    [btnClkSrc441kHz.titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
    [btnClkSrc441kHz.titleLabel setNumberOfLines:0];
    [btnClkSrc441kHz.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [btnClkSrc441kHz setTitle:@"Int\n44.1 kHz" forState:UIControlStateNormal];
    [btnClkSrc441kHz setTitle:@"Int\n44.1 kHz" forState:UIControlStateSelected];
    [btnClkSrc48kHz.titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
    [btnClkSrc48kHz.titleLabel setNumberOfLines:0];
    [btnClkSrc48kHz.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [btnClkSrc48kHz setTitle:@"Int\n48 kHz" forState:UIControlStateNormal];
    [btnClkSrc48kHz setTitle:@"Int\n48 kHz" forState:UIControlStateSelected];
    [btnClkSrcUsbAudio.titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
    [btnClkSrcUsbAudio.titleLabel setNumberOfLines:0];
    [btnClkSrcUsbAudio.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [btnClkSrcUsbAudio setTitle:@"USB AUDIO\n0.0kHz" forState:UIControlStateNormal];
    [btnClkSrcUsbAudio setTitle:@"USB AUDIO\n0.0kHz" forState:UIControlStateSelected];
    [btnDelayTime.titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
    [btnDelayTime.titleLabel setNumberOfLines:0];
    [btnDelayTime.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [btnDelayTime setTitle:@"TIME\n(mS)" forState:UIControlStateNormal];
    [btnDelayTime setTitle:@"TIME\n(mS)" forState:UIControlStateSelected];
    [btnDelayMeter.titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
    [btnDelayMeter.titleLabel setNumberOfLines:0];
    [btnDelayMeter.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [btnDelayMeter setTitle:@"METER\n(M)" forState:UIControlStateNormal];
    [btnDelayMeter setTitle:@"METER\n(M)" forState:UIControlStateSelected];
    [btnDelayFeet.titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
    [btnDelayFeet.titleLabel setNumberOfLines:0];
    [btnDelayFeet.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [btnDelayFeet setTitle:@"FEET\n(Ft.)" forState:UIControlStateNormal];
    [btnDelayFeet setTitle:@"FEET\n(Ft.)" forState:UIControlStateSelected];
    
    // Add slider meters
    UIImage *panProgressImage = [[UIImage imageNamed:@"pan-6.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0,0,0,0)];
    int meterOffset;
    if (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"7.0")) {
        meterOffset = 47;
    } else {
        meterOffset = 44;
    }

    meterPan1 = [[JEProgressView alloc] initWithFrame:CGRectMake(8, meterOffset, 82, 82)];
    [meterPan1 setTransform:CGAffineTransformMake(-1, 0, 0, 1, 0, 0)];
    [meterPan1 setTintColor:[UIColor clearColor]];
    [meterPan1 setProgressImage:panProgressImage];
    [meterPan1 setTrackImage:[UIImage alloc]];
    [meterPan1 setProgress:0.0];
    [viewMainPanel addSubview:meterPan1];
    
    meterPan2 = [[JEProgressView alloc] initWithFrame:CGRectMake(105, meterOffset, 82, 82)];
    [meterPan2 setTransform:CGAffineTransformMake(-1, 0, 0, 1, 0, 0)];
    [meterPan2 setTintColor:[UIColor clearColor]];
    [meterPan2 setProgressImage:panProgressImage];
    [meterPan2 setTrackImage:[UIImage alloc]];
    [meterPan2 setProgress:0.0];
    [viewMainPanel addSubview:meterPan2];
    
    meterPan3 = [[JEProgressView alloc] initWithFrame:CGRectMake(199, meterOffset, 82, 82)];
    [meterPan3 setTransform:CGAffineTransformMake(-1, 0, 0, 1, 0, 0)];
    [meterPan3 setTintColor:[UIColor clearColor]];
    [meterPan3 setProgressImage:panProgressImage];
    [meterPan3 setTrackImage:[UIImage alloc]];
    [meterPan3 setProgress:0.0];
    [viewMainPanel addSubview:meterPan3];
    
    meterPan4 = [[JEProgressView alloc] initWithFrame:CGRectMake(293, meterOffset, 82, 82)];
    [meterPan4 setTransform:CGAffineTransformMake(-1, 0, 0, 1, 0, 0)];
    [meterPan4 setTintColor:[UIColor clearColor]];
    [meterPan4 setProgressImage:panProgressImage];
    [meterPan4 setTrackImage:[UIImage alloc]];
    [meterPan4 setProgress:0.0];
    [viewMainPanel addSubview:meterPan4];
    
    meterPan5 = [[JEProgressView alloc] initWithFrame:CGRectMake(387, meterOffset, 82, 82)];
    [meterPan5 setTransform:CGAffineTransformMake(-1, 0, 0, 1, 0, 0)];
    [meterPan5 setTintColor:[UIColor clearColor]];
    [meterPan5 setProgressImage:panProgressImage];
    [meterPan5 setTrackImage:[UIImage alloc]];
    [meterPan5 setProgress:0.0];
    [viewMainPanel addSubview:meterPan5];
    
    meterPan6 = [[JEProgressView alloc] initWithFrame:CGRectMake(481, meterOffset, 82, 82)];
    [meterPan6 setTransform:CGAffineTransformMake(-1, 0, 0, 1, 0, 0)];
    [meterPan6 setTintColor:[UIColor clearColor]];
    [meterPan6 setProgressImage:panProgressImage];
    [meterPan6 setTrackImage:[UIImage alloc]];
    [meterPan6 setProgress:0.0];
    [viewMainPanel addSubview:meterPan6];
    
    meterPan7 = [[JEProgressView alloc] initWithFrame:CGRectMake(574, meterOffset, 82, 82)];
    [meterPan7 setTransform:CGAffineTransformMake(-1, 0, 0, 1, 0, 0)];
    [meterPan7 setTintColor:[UIColor clearColor]];
    [meterPan7 setProgressImage:panProgressImage];
    [meterPan7 setTrackImage:[UIImage alloc]];
    [meterPan7 setProgress:0.0];
    [viewMainPanel addSubview:meterPan7];
    
    meterPan8 = [[JEProgressView alloc] initWithFrame:CGRectMake(667, meterOffset, 82, 82)];
    [meterPan8 setTransform:CGAffineTransformMake(-1, 0, 0, 1, 0, 0)];
    [meterPan8 setTintColor:[UIColor clearColor]];
    [meterPan8 setProgressImage:panProgressImage];
    [meterPan8 setTrackImage:[UIImage alloc]];
    [meterPan8 setProgress:0.0];
    [viewMainPanel addSubview:meterPan8];
    
    // Add PAN sliders
    UIImage *panImage = [UIImage imageNamed:@"Slider-1.png"];
    
    sliderPan1 = [[UISlider alloc] initWithFrame:CGRectMake(7, 11, 86, 86)];
    [sliderPan1 addTarget:self action:@selector(onSliderPan1Action:) forControlEvents:UIControlEventValueChanged];
    [sliderPan1 addTarget:self action:@selector(onSliderPan1Start:) forControlEvents:UIControlEventTouchDown];
    [sliderPan1 addTarget:self action:@selector(onSliderPan1End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderPan1 addTarget:self action:@selector(onSliderPan1End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderPan1 setBackgroundColor:[UIColor clearColor]];
    [sliderPan1 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderPan1 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderPan1 setThumbImage:panImage forState:UIControlStateNormal];
    sliderPan1.minimumValue = 0.0;
    sliderPan1.maximumValue = PAN_MAX_VALUE;
    sliderPan1.continuous = YES;
    sliderPan1.value = PAN_CENTER_VALUE;
    [viewMainPanel addSubview:sliderPan1];
    
    sliderPan2 = [[UISlider alloc] initWithFrame:CGRectMake(104, 11, 86, 86)];
    [sliderPan2 addTarget:self action:@selector(onSliderPan2Action:) forControlEvents:UIControlEventValueChanged];
    [sliderPan2 addTarget:self action:@selector(onSliderPan2Start:) forControlEvents:UIControlEventTouchDown];
    [sliderPan2 addTarget:self action:@selector(onSliderPan2End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderPan2 addTarget:self action:@selector(onSliderPan2End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderPan2 setBackgroundColor:[UIColor clearColor]];
    [sliderPan2 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderPan2 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderPan2 setThumbImage:panImage forState:UIControlStateNormal];
    sliderPan2.minimumValue = 0.0;
    sliderPan2.maximumValue = PAN_MAX_VALUE;
    sliderPan2.continuous = YES;
    sliderPan2.value = PAN_CENTER_VALUE;
    [viewMainPanel addSubview:sliderPan2];
    
    sliderPan3 = [[UISlider alloc] initWithFrame:CGRectMake(198, 11, 86, 86)];
    [sliderPan3 addTarget:self action:@selector(onSliderPan3Action:) forControlEvents:UIControlEventValueChanged];
    [sliderPan3 addTarget:self action:@selector(onSliderPan3Start:) forControlEvents:UIControlEventTouchDown];
    [sliderPan3 addTarget:self action:@selector(onSliderPan3End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderPan3 addTarget:self action:@selector(onSliderPan3End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderPan3 setBackgroundColor:[UIColor clearColor]];
    [sliderPan3 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderPan3 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderPan3 setThumbImage:panImage forState:UIControlStateNormal];
    sliderPan3.minimumValue = 0.0;
    sliderPan3.maximumValue = PAN_MAX_VALUE;
    sliderPan3.continuous = YES;
    sliderPan3.value = PAN_CENTER_VALUE;
    [viewMainPanel addSubview:sliderPan3];
    
    sliderPan4 = [[UISlider alloc] initWithFrame:CGRectMake(292, 11, 86, 86)];
    [sliderPan4 addTarget:self action:@selector(onSliderPan4Action:) forControlEvents:UIControlEventValueChanged];
    [sliderPan4 addTarget:self action:@selector(onSliderPan4Start:) forControlEvents:UIControlEventTouchDown];
    [sliderPan4 addTarget:self action:@selector(onSliderPan4End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderPan4 addTarget:self action:@selector(onSliderPan4End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderPan4 setBackgroundColor:[UIColor clearColor]];
    [sliderPan4 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderPan4 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderPan4 setThumbImage:panImage forState:UIControlStateNormal];
    sliderPan4.minimumValue = 0.0;
    sliderPan4.maximumValue = PAN_MAX_VALUE;
    sliderPan4.continuous = YES;
    sliderPan4.value = PAN_CENTER_VALUE;
    [viewMainPanel addSubview:sliderPan4];
    
    sliderPan5 = [[UISlider alloc] initWithFrame:CGRectMake(386, 11, 86, 86)];
    [sliderPan5 addTarget:self action:@selector(onSliderPan5Action:) forControlEvents:UIControlEventValueChanged];
    [sliderPan5 addTarget:self action:@selector(onSliderPan5Start:) forControlEvents:UIControlEventTouchDown];
    [sliderPan5 addTarget:self action:@selector(onSliderPan5End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderPan5 addTarget:self action:@selector(onSliderPan5End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderPan5 setBackgroundColor:[UIColor clearColor]];
    [sliderPan5 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderPan5 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderPan5 setThumbImage:panImage forState:UIControlStateNormal];
    sliderPan5.minimumValue = 0.0;
    sliderPan5.maximumValue = PAN_MAX_VALUE;
    sliderPan5.continuous = YES;
    sliderPan5.value = PAN_CENTER_VALUE;
    [viewMainPanel addSubview:sliderPan5];
    
    sliderPan6 = [[UISlider alloc] initWithFrame:CGRectMake(480, 11, 86, 86)];
    [sliderPan6 addTarget:self action:@selector(onSliderPan6Action:) forControlEvents:UIControlEventValueChanged];
    [sliderPan6 addTarget:self action:@selector(onSliderPan6Start:) forControlEvents:UIControlEventTouchDown];
    [sliderPan6 addTarget:self action:@selector(onSliderPan6End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderPan6 addTarget:self action:@selector(onSliderPan6End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderPan6 setBackgroundColor:[UIColor clearColor]];
    [sliderPan6 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderPan6 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderPan6 setThumbImage:panImage forState:UIControlStateNormal];
    sliderPan6.minimumValue = 0.0;
    sliderPan6.maximumValue = PAN_MAX_VALUE;
    sliderPan6.continuous = YES;
    sliderPan6.value = PAN_CENTER_VALUE;
    [viewMainPanel addSubview:sliderPan6];
    
    sliderPan7 = [[UISlider alloc] initWithFrame:CGRectMake(573, 11, 86, 86)];
    [sliderPan7 addTarget:self action:@selector(onSliderPan7Action:) forControlEvents:UIControlEventValueChanged];
    [sliderPan7 addTarget:self action:@selector(onSliderPan7Start:) forControlEvents:UIControlEventTouchDown];
    [sliderPan7 addTarget:self action:@selector(onSliderPan7End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderPan7 addTarget:self action:@selector(onSliderPan7End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderPan7 setBackgroundColor:[UIColor clearColor]];
    [sliderPan7 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderPan7 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderPan7 setThumbImage:panImage forState:UIControlStateNormal];
    sliderPan7.minimumValue = 0.0;
    sliderPan7.maximumValue = PAN_MAX_VALUE;
    sliderPan7.continuous = YES;
    sliderPan7.value = PAN_CENTER_VALUE;
    [viewMainPanel addSubview:sliderPan7];
    
    sliderPan8 = [[UISlider alloc] initWithFrame:CGRectMake(666, 11, 86, 86)];
    [sliderPan8 addTarget:self action:@selector(onSliderPan8Action:) forControlEvents:UIControlEventValueChanged];
    [sliderPan8 addTarget:self action:@selector(onSliderPan8Start:) forControlEvents:UIControlEventTouchDown];
    [sliderPan8 addTarget:self action:@selector(onSliderPan8End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderPan8 addTarget:self action:@selector(onSliderPan8End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderPan8 setBackgroundColor:[UIColor clearColor]];
    [sliderPan8 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderPan8 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderPan8 setThumbImage:panImage forState:UIControlStateNormal];
    sliderPan8.minimumValue = 0.0;
    sliderPan8.maximumValue = PAN_MAX_VALUE;
    sliderPan8.continuous = YES;
    sliderPan8.value = PAN_CENTER_VALUE;
    [viewMainPanel addSubview:sliderPan8];
    
    // Add vertical sliders
    UIImage *fader1Image = [UIImage imageNamed:@"fader-1.png"];
    UIImage *faderMainImage = [UIImage imageNamed:@"fader-3.png"];
    
    slider1 = [[UISlider alloc] initWithFrame:CGRectMake(-86, 295, 301, 180)];
    slider1.transform = CGAffineTransformRotate(slider1.transform, 270.0/180*M_PI);
    [slider1 addTarget:self action:@selector(onSlider1Begin:) forControlEvents:UIControlEventTouchDown];
    [slider1 addTarget:self action:@selector(onSlider1End:) forControlEvents:UIControlEventTouchUpInside];
    [slider1 addTarget:self action:@selector(onSlider1End:) forControlEvents:UIControlEventTouchUpOutside];
    [slider1 addTarget:self action:@selector(onSlider1Action:) forControlEvents:UIControlEventValueChanged];
    [slider1 setBackgroundColor:[UIColor clearColor]];
    [slider1 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [slider1 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [slider1 setThumbImage:fader1Image forState:UIControlStateNormal];
    slider1.minimumValue = 0.0;
    slider1.maximumValue = FADER_MAX_VALUE;
    slider1.continuous = YES;
    slider1.value = FADER_DEFAULT_VALUE;
    [viewMainPanel addSubview:slider1];
    
    slider2 = [[UISlider alloc] initWithFrame:CGRectMake(7, 295, 301, 180)];
    slider2.transform = CGAffineTransformRotate(slider2.transform, 270.0/180*M_PI);
    [slider2 addTarget:self action:@selector(onSlider2Begin:) forControlEvents:UIControlEventTouchDown];
    [slider2 addTarget:self action:@selector(onSlider2End:) forControlEvents:UIControlEventTouchUpInside];
    [slider2 addTarget:self action:@selector(onSlider2End:) forControlEvents:UIControlEventTouchUpOutside];
    [slider2 addTarget:self action:@selector(onSlider2Action:) forControlEvents:UIControlEventValueChanged];
    [slider2 setBackgroundColor:[UIColor clearColor]];
    [slider2 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [slider2 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [slider2 setThumbImage:fader1Image forState:UIControlStateNormal];
    slider2.minimumValue = 0.0;
    slider2.maximumValue = FADER_MAX_VALUE;
    slider2.continuous = YES;
    slider2.value = FADER_DEFAULT_VALUE;
    [viewMainPanel addSubview:slider2];
    
    slider3 = [[UISlider alloc] initWithFrame:CGRectMake(101, 295, 301, 180)];
    slider3.transform = CGAffineTransformRotate(slider3.transform, 270.0/180*M_PI);
    [slider3 addTarget:self action:@selector(onSlider3Begin:) forControlEvents:UIControlEventTouchDown];
    [slider3 addTarget:self action:@selector(onSlider3End:) forControlEvents:UIControlEventTouchUpInside];
    [slider3 addTarget:self action:@selector(onSlider3End:) forControlEvents:UIControlEventTouchUpOutside];
    [slider3 addTarget:self action:@selector(onSlider3Action:) forControlEvents:UIControlEventValueChanged];
    [slider3 setBackgroundColor:[UIColor clearColor]];
    [slider3 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [slider3 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [slider3 setThumbImage:fader1Image forState:UIControlStateNormal];
    slider3.minimumValue = 0.0;
    slider3.maximumValue = FADER_MAX_VALUE;
    slider3.continuous = YES;
    slider3.value = FADER_DEFAULT_VALUE;
    [viewMainPanel addSubview:slider3];
    
    slider4 = [[UISlider alloc] initWithFrame:CGRectMake(195, 295, 301, 180)];
    slider4.transform = CGAffineTransformRotate(slider4.transform, 270.0/180*M_PI);
    [slider4 addTarget:self action:@selector(onSlider4Begin:) forControlEvents:UIControlEventTouchDown];
    [slider4 addTarget:self action:@selector(onSlider4End:) forControlEvents:UIControlEventTouchUpInside];
    [slider4 addTarget:self action:@selector(onSlider4End:) forControlEvents:UIControlEventTouchUpOutside];
    [slider4 addTarget:self action:@selector(onSlider4Action:) forControlEvents:UIControlEventValueChanged];
    [slider4 setBackgroundColor:[UIColor clearColor]];
    [slider4 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [slider4 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [slider4 setThumbImage:fader1Image forState:UIControlStateNormal];
    slider4.minimumValue = 0.0;
    slider4.maximumValue = FADER_MAX_VALUE;
    slider4.continuous = YES;
    slider4.value = FADER_DEFAULT_VALUE;
    [viewMainPanel addSubview:slider4];
    
    slider5 = [[UISlider alloc] initWithFrame:CGRectMake(289, 295, 301, 180)];
    slider5.transform = CGAffineTransformRotate(slider5.transform, 270.0/180*M_PI);
    [slider5 addTarget:self action:@selector(onSlider5Begin:) forControlEvents:UIControlEventTouchDown];
    [slider5 addTarget:self action:@selector(onSlider5End:) forControlEvents:UIControlEventTouchUpInside];
    [slider5 addTarget:self action:@selector(onSlider5End:) forControlEvents:UIControlEventTouchUpOutside];
    [slider5 addTarget:self action:@selector(onSlider5Action:) forControlEvents:UIControlEventValueChanged];
    [slider5 setBackgroundColor:[UIColor clearColor]];
    [slider5 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [slider5 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [slider5 setThumbImage:fader1Image forState:UIControlStateNormal];
    slider5.minimumValue = 0.0;
    slider5.maximumValue = FADER_MAX_VALUE;
    slider5.continuous = YES;
    slider5.value = FADER_DEFAULT_VALUE;
    [viewMainPanel addSubview:slider5];
    
    slider6 = [[UISlider alloc] initWithFrame:CGRectMake(383, 295, 301, 180)];
    slider6.transform = CGAffineTransformRotate(slider6.transform, 270.0/180*M_PI);
    [slider6 addTarget:self action:@selector(onSlider6Begin:) forControlEvents:UIControlEventTouchDown];
    [slider6 addTarget:self action:@selector(onSlider6End:) forControlEvents:UIControlEventTouchUpInside];
    [slider6 addTarget:self action:@selector(onSlider6End:) forControlEvents:UIControlEventTouchUpOutside];
    [slider6 addTarget:self action:@selector(onSlider6Action:) forControlEvents:UIControlEventValueChanged];
    [slider6 setBackgroundColor:[UIColor clearColor]];
    [slider6 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [slider6 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [slider6 setThumbImage:fader1Image forState:UIControlStateNormal];
    slider6.minimumValue = 0.0;
    slider6.maximumValue = FADER_MAX_VALUE;
    slider6.continuous = YES;
    slider6.value = FADER_DEFAULT_VALUE;
    [viewMainPanel addSubview:slider6];
    
    slider7 = [[UISlider alloc] initWithFrame:CGRectMake(477, 295, 301, 180)];
    slider7.transform = CGAffineTransformRotate(slider7.transform, 270.0/180*M_PI);
    [slider7 addTarget:self action:@selector(onSlider7Begin:) forControlEvents:UIControlEventTouchDown];
    [slider7 addTarget:self action:@selector(onSlider7End:) forControlEvents:UIControlEventTouchUpInside];
    [slider7 addTarget:self action:@selector(onSlider7End:) forControlEvents:UIControlEventTouchUpOutside];
    [slider7 addTarget:self action:@selector(onSlider7Action:) forControlEvents:UIControlEventValueChanged];
    [slider7 setBackgroundColor:[UIColor clearColor]];
    [slider7 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [slider7 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [slider7 setThumbImage:fader1Image forState:UIControlStateNormal];
    slider7.minimumValue = 0.0;
    slider7.maximumValue = FADER_MAX_VALUE;
    slider7.continuous = YES;
    slider7.value = FADER_DEFAULT_VALUE;
    [viewMainPanel addSubview:slider7];
    
    slider8 = [[UISlider alloc] initWithFrame:CGRectMake(571, 295, 301, 180)];
    slider8.transform = CGAffineTransformRotate(slider8.transform, 270.0/180*M_PI);
    [slider8 addTarget:self action:@selector(onSlider8Begin:) forControlEvents:UIControlEventTouchDown];
    [slider8 addTarget:self action:@selector(onSlider8End:) forControlEvents:UIControlEventTouchUpInside];
    [slider8 addTarget:self action:@selector(onSlider8End:) forControlEvents:UIControlEventTouchUpOutside];
    [slider8 addTarget:self action:@selector(onSlider8Action:) forControlEvents:UIControlEventValueChanged];
    [slider8 setBackgroundColor:[UIColor clearColor]];
    [slider8 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [slider8 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [slider8 setThumbImage:fader1Image forState:UIControlStateNormal];
    slider8.minimumValue = 0.0;
    slider8.maximumValue = FADER_MAX_VALUE;
    slider8.continuous = YES;
    slider8.value = FADER_DEFAULT_VALUE;
    [viewMainPanel addSubview:slider8];
        
    sliderMain = [[UISlider alloc] initWithFrame:CGRectMake(675, 295, 301, 180)];
    sliderMain.transform = CGAffineTransformRotate(sliderMain.transform, 270.0/180*M_PI);
    [sliderMain addTarget:self action:@selector(onSliderMainBegin:) forControlEvents:UIControlEventTouchDown];
    [sliderMain addTarget:self action:@selector(onSliderMainEnd:) forControlEvents:UIControlEventTouchUpInside];
    [sliderMain addTarget:self action:@selector(onSliderMainEnd:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderMain addTarget:self action:@selector(onSliderMainAction:) forControlEvents:UIControlEventValueChanged];
    [sliderMain setBackgroundColor:[UIColor clearColor]];
    [sliderMain setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderMain setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderMain setThumbImage:faderMainImage forState:UIControlStateNormal];
    sliderMain.minimumValue = 0.0;
    sliderMain.maximumValue = FADER_MAX_VALUE;
    sliderMain.continuous = YES;
    sliderMain.value = FADER_DEFAULT_VALUE;
    [viewMainPanel addSubview:sliderMain];
    
    // Add slider meters
    UIImage *progressImage = [[UIImage imageNamed:@"meter-1.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0,0,0,0)];
    
    meter1 = [[JEProgressView alloc] initWithFrame:CGRectMake(-120, 372, 275, 4)];
    meter1.transform = CGAffineTransformRotate(meter1.transform, 270.0/180*M_PI);
    [meter1 setTintColor:[UIColor clearColor]];
    [meter1 setProgressImage:progressImage];
    [meter1 setTrackImage:[UIImage alloc]];
    [meter1 setProgress:0.0];
    [viewMainPanel addSubview:meter1];
    
    meter2 = [[JEProgressView alloc] initWithFrame:CGRectMake(-27, 372, 275, 4)];
    meter2.transform = CGAffineTransformRotate(meter2.transform, 270.0/180*M_PI);
    [meter2 setTintColor:[UIColor clearColor]];
    [meter2 setProgressImage:progressImage];
    [meter2 setTrackImage:[UIImage alloc]];
    [meter2 setProgress:0.0];
    [viewMainPanel addSubview:meter2];
    
    meter3 = [[JEProgressView alloc] initWithFrame:CGRectMake(67, 372, 275, 4)];
    meter3.transform = CGAffineTransformRotate(meter3.transform, 270.0/180*M_PI);
    [meter3 setTintColor:[UIColor clearColor]];
    [meter3 setProgressImage:progressImage];
    [meter3 setTrackImage:[UIImage alloc]];
    [meter3 setProgress:0.0];
    [viewMainPanel addSubview:meter3];
    
    meter4 = [[JEProgressView alloc] initWithFrame:CGRectMake(160, 372, 275, 4)];
    meter4.transform = CGAffineTransformRotate(meter4.transform, 270.0/180*M_PI);
    [meter4 setTintColor:[UIColor clearColor]];
    [meter4 setProgressImage:progressImage];
    [meter4 setTrackImage:[UIImage alloc]];
    [meter4 setProgress:0.0];
    [viewMainPanel addSubview:meter4];
    
    meter5 = [[JEProgressView alloc] initWithFrame:CGRectMake(255, 372, 275, 4)];
    meter5.transform = CGAffineTransformRotate(meter5.transform, 270.0/180*M_PI);
    [meter5 setTintColor:[UIColor clearColor]];
    [meter5 setProgressImage:progressImage];
    [meter5 setTrackImage:[UIImage alloc]];
    [meter5 setProgress:0.0];
    [viewMainPanel addSubview:meter5];
    
    meter6 = [[JEProgressView alloc] initWithFrame:CGRectMake(348, 372, 275, 4)];
    meter6.transform = CGAffineTransformRotate(meter6.transform, 270.0/180*M_PI);
    [meter6 setTintColor:[UIColor clearColor]];
    [meter6 setProgressImage:progressImage];
    [meter6 setTrackImage:[UIImage alloc]];
    [meter6 setProgress:0.0];
    [viewMainPanel addSubview:meter6];
    
    meter7 = [[JEProgressView alloc] initWithFrame:CGRectMake(442, 372, 275, 4)];
    meter7.transform = CGAffineTransformRotate(meter7.transform, 270.0/180*M_PI);
    [meter7 setTintColor:[UIColor clearColor]];
    [meter7 setProgressImage:progressImage];
    [meter7 setTrackImage:[UIImage alloc]];
    [meter7 setProgress:0.0];
    [viewMainPanel addSubview:meter7];
    
    meter8 = [[JEProgressView alloc] initWithFrame:CGRectMake(536, 372, 275, 4)];
    meter8.transform = CGAffineTransformRotate(meter8.transform, 270.0/180*M_PI);
    [meter8 setTintColor:[UIColor clearColor]];
    [meter8 setProgressImage:progressImage];
    [meter8 setTrackImage:[UIImage alloc]];
    [meter8 setProgress:0.0];
    [viewMainPanel addSubview:meter8];
    
    meterCtrlRmL = [[JEProgressView alloc] initWithFrame:CGRectMake(530, 372, 275, 4)];
    meterCtrlRmL.transform = CGAffineTransformRotate(meterCtrlRmL.transform, 270.0/180*M_PI);
    [meterCtrlRmL setTintColor:[UIColor clearColor]];
    [meterCtrlRmL setProgressImage:progressImage];
    [meterCtrlRmL setTrackImage:[UIImage alloc]];
    [meterCtrlRmL setProgress:0.0];
    [viewMainPanel addSubview:meterCtrlRmL];
    
    meterCtrlRmR = [[JEProgressView alloc] initWithFrame:CGRectMake(540, 372, 275, 4)];
    meterCtrlRmR.transform = CGAffineTransformRotate(meterCtrlRmR.transform, 270.0/180*M_PI);
    [meterCtrlRmR setTintColor:[UIColor clearColor]];
    [meterCtrlRmR setProgressImage:progressImage];
    [meterCtrlRmR setTrackImage:[UIImage alloc]];
    [meterCtrlRmR setProgress:0.0];
    [viewMainPanel addSubview:meterCtrlRmR];
    
    meterMainL = [[JEProgressView alloc] initWithFrame:CGRectMake(632, 372, 275, 4)];
    meterMainL.transform = CGAffineTransformRotate(meterMainL.transform, 270.0/180*M_PI);
    [meterMainL setTintColor:[UIColor clearColor]];
    [meterMainL setProgressImage:progressImage];
    [meterMainL setTrackImage:[UIImage alloc]];
    [meterMainL setProgress:0.0];
    [viewMainPanel addSubview:meterMainL];
    
    meterMainR = [[JEProgressView alloc] initWithFrame:CGRectMake(642, 372, 275, 4)];
    meterMainR.transform = CGAffineTransformRotate(meterMainR.transform, 270.0/180*M_PI);
    [meterMainR setTintColor:[UIColor clearColor]];
    [meterMainR setProgressImage:progressImage];
    [meterMainR setTrackImage:[UIImage alloc]];
    [meterMainR setProgress:0.0];
    [viewMainPanel addSubview:meterMainR];
    
    // Add top panel page meters
    UIImage *pageMeterImage = [[UIImage imageNamed:@"meter-2.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0,0,0,0)];
    
    pageCh1_8Meter1 = [[JEProgressView alloc] initWithFrame:CGRectMake(-28, 53, 84, 2)];
    pageCh1_8Meter1.transform = CGAffineTransformRotate(pageCh1_8Meter1.transform, 270.0/180*M_PI);
    [pageCh1_8Meter1 setTintColor:[UIColor clearColor]];
    [pageCh1_8Meter1 setProgressImage:pageMeterImage];
    [pageCh1_8Meter1 setTrackImage:[UIImage alloc]];
    [pageCh1_8Meter1 setProgress:0.0];
    [pageCh1_8Meter1 setUserInteractionEnabled:NO];
    [viewTopPanel addSubview:pageCh1_8Meter1];
    
    pageCh1_8Meter2 = [[JEProgressView alloc] initWithFrame:CGRectMake(-21, 53, 84, 2)];
    pageCh1_8Meter2.transform = CGAffineTransformRotate(pageCh1_8Meter2.transform, 270.0/180*M_PI);
    [pageCh1_8Meter2 setTintColor:[UIColor clearColor]];
    [pageCh1_8Meter2 setProgressImage:pageMeterImage];
    [pageCh1_8Meter2 setTrackImage:[UIImage alloc]];
    [pageCh1_8Meter2 setProgress:0.0];
    [pageCh1_8Meter2 setUserInteractionEnabled:NO];
    [viewTopPanel addSubview:pageCh1_8Meter2];
    
    pageCh1_8Meter3 = [[JEProgressView alloc] initWithFrame:CGRectMake(-14, 53, 84, 2)];
    pageCh1_8Meter3.transform = CGAffineTransformRotate(pageCh1_8Meter3.transform, 270.0/180*M_PI);
    [pageCh1_8Meter3 setTintColor:[UIColor clearColor]];
    [pageCh1_8Meter3 setProgressImage:pageMeterImage];
    [pageCh1_8Meter3 setTrackImage:[UIImage alloc]];
    [pageCh1_8Meter3 setProgress:0.0];
    [pageCh1_8Meter3 setUserInteractionEnabled:NO];
    [viewTopPanel addSubview:pageCh1_8Meter3];
    
    pageCh1_8Meter4 = [[JEProgressView alloc] initWithFrame:CGRectMake(-7, 53, 84, 2)];
    pageCh1_8Meter4.transform = CGAffineTransformRotate(pageCh1_8Meter4.transform, 270.0/180*M_PI);
    [pageCh1_8Meter4 setTintColor:[UIColor clearColor]];
    [pageCh1_8Meter4 setProgressImage:pageMeterImage];
    [pageCh1_8Meter4 setTrackImage:[UIImage alloc]];
    [pageCh1_8Meter4 setProgress:0.0];
    [pageCh1_8Meter4 setUserInteractionEnabled:NO];
    [viewTopPanel addSubview:pageCh1_8Meter4];
    
    pageCh1_8Meter5 = [[JEProgressView alloc] initWithFrame:CGRectMake(0, 53, 84, 2)];
    pageCh1_8Meter5.transform = CGAffineTransformRotate(pageCh1_8Meter5.transform, 270.0/180*M_PI);
    [pageCh1_8Meter5 setTintColor:[UIColor clearColor]];
    [pageCh1_8Meter5 setProgressImage:pageMeterImage];
    [pageCh1_8Meter5 setTrackImage:[UIImage alloc]];
    [pageCh1_8Meter5 setProgress:0.0];
    [pageCh1_8Meter5 setUserInteractionEnabled:NO];
    [viewTopPanel addSubview:pageCh1_8Meter5];
    
    pageCh1_8Meter6 = [[JEProgressView alloc] initWithFrame:CGRectMake(7, 53, 84, 2)];
    pageCh1_8Meter6.transform = CGAffineTransformRotate(pageCh1_8Meter6.transform, 270.0/180*M_PI);
    [pageCh1_8Meter6 setTintColor:[UIColor clearColor]];
    [pageCh1_8Meter6 setProgressImage:pageMeterImage];
    [pageCh1_8Meter6 setTrackImage:[UIImage alloc]];
    [pageCh1_8Meter6 setProgress:0.0];
    [pageCh1_8Meter6 setUserInteractionEnabled:NO];
    [viewTopPanel addSubview:pageCh1_8Meter6];
    
    pageCh1_8Meter7 = [[JEProgressView alloc] initWithFrame:CGRectMake(13, 53, 84, 2)];
    pageCh1_8Meter7.transform = CGAffineTransformRotate(pageCh1_8Meter7.transform, 270.0/180*M_PI);
    [pageCh1_8Meter7 setTintColor:[UIColor clearColor]];
    [pageCh1_8Meter7 setProgressImage:pageMeterImage];
    [pageCh1_8Meter7 setTrackImage:[UIImage alloc]];
    [pageCh1_8Meter7 setProgress:0.0];
    [pageCh1_8Meter7 setUserInteractionEnabled:NO];
    [viewTopPanel addSubview:pageCh1_8Meter7];
    
    pageCh1_8Meter8 = [[JEProgressView alloc] initWithFrame:CGRectMake(20, 53, 84, 2)];
    pageCh1_8Meter8.transform = CGAffineTransformRotate(pageCh1_8Meter8.transform, 270.0/180*M_PI);
    [pageCh1_8Meter8 setTintColor:[UIColor clearColor]];
    [pageCh1_8Meter8 setProgressImage:pageMeterImage];
    [pageCh1_8Meter8 setTrackImage:[UIImage alloc]];
    [pageCh1_8Meter8 setProgress:0.0];
    [pageCh1_8Meter8 setUserInteractionEnabled:NO];
    [viewTopPanel addSubview:pageCh1_8Meter8];
    
    pageCh9_16Meter1 = [[JEProgressView alloc] initWithFrame:CGRectMake(43, 53, 84, 2)];
    pageCh9_16Meter1.transform = CGAffineTransformRotate(pageCh9_16Meter1.transform, 270.0/180*M_PI);
    [pageCh9_16Meter1 setTintColor:[UIColor clearColor]];
    [pageCh9_16Meter1 setProgressImage:pageMeterImage];
    [pageCh9_16Meter1 setTrackImage:[UIImage alloc]];
    [pageCh9_16Meter1 setProgress:0.0];
    [pageCh9_16Meter1 setUserInteractionEnabled:NO];
    [viewTopPanel addSubview:pageCh9_16Meter1];
    
    pageCh9_16Meter2 = [[JEProgressView alloc] initWithFrame:CGRectMake(50, 53, 84, 2)];
    pageCh9_16Meter2.transform = CGAffineTransformRotate(pageCh9_16Meter2.transform, 270.0/180*M_PI);
    [pageCh9_16Meter2 setTintColor:[UIColor clearColor]];
    [pageCh9_16Meter2 setProgressImage:pageMeterImage];
    [pageCh9_16Meter2 setTrackImage:[UIImage alloc]];
    [pageCh9_16Meter2 setProgress:0.0];
    [pageCh9_16Meter2 setUserInteractionEnabled:NO];
    [viewTopPanel addSubview:pageCh9_16Meter2];
    
    pageCh9_16Meter3 = [[JEProgressView alloc] initWithFrame:CGRectMake(57, 53, 84, 2)];
    pageCh9_16Meter3.transform = CGAffineTransformRotate(pageCh9_16Meter3.transform, 270.0/180*M_PI);
    [pageCh9_16Meter3 setTintColor:[UIColor clearColor]];
    [pageCh9_16Meter3 setProgressImage:pageMeterImage];
    [pageCh9_16Meter3 setTrackImage:[UIImage alloc]];
    [pageCh9_16Meter3 setProgress:0.0];
    [pageCh9_16Meter3 setUserInteractionEnabled:NO];
    [viewTopPanel addSubview:pageCh9_16Meter3];
    
    pageCh9_16Meter4 = [[JEProgressView alloc] initWithFrame:CGRectMake(64, 53, 84, 2)];
    pageCh9_16Meter4.transform = CGAffineTransformRotate(pageCh9_16Meter4.transform, 270.0/180*M_PI);
    [pageCh9_16Meter4 setTintColor:[UIColor clearColor]];
    [pageCh9_16Meter4 setProgressImage:pageMeterImage];
    [pageCh9_16Meter4 setTrackImage:[UIImage alloc]];
    [pageCh9_16Meter4 setProgress:0.0];
    [pageCh9_16Meter4 setUserInteractionEnabled:NO];
    [viewTopPanel addSubview:pageCh9_16Meter4];
    
    pageCh9_16Meter5 = [[JEProgressView alloc] initWithFrame:CGRectMake(71, 53, 84, 2)];
    pageCh9_16Meter5.transform = CGAffineTransformRotate(pageCh9_16Meter5.transform, 270.0/180*M_PI);
    [pageCh9_16Meter5 setTintColor:[UIColor clearColor]];
    [pageCh9_16Meter5 setProgressImage:pageMeterImage];
    [pageCh9_16Meter5 setTrackImage:[UIImage alloc]];
    [pageCh9_16Meter5 setProgress:0.0];
    [pageCh9_16Meter5 setUserInteractionEnabled:NO];
    [viewTopPanel addSubview:pageCh9_16Meter5];
    
    pageCh9_16Meter6 = [[JEProgressView alloc] initWithFrame:CGRectMake(78, 53, 84, 2)];
    pageCh9_16Meter6.transform = CGAffineTransformRotate(pageCh9_16Meter6.transform, 270.0/180*M_PI);
    [pageCh9_16Meter6 setTintColor:[UIColor clearColor]];
    [pageCh9_16Meter6 setProgressImage:pageMeterImage];
    [pageCh9_16Meter6 setTrackImage:[UIImage alloc]];
    [pageCh9_16Meter6 setProgress:0.0];
    [pageCh9_16Meter6 setUserInteractionEnabled:NO];
    [viewTopPanel addSubview:pageCh9_16Meter6];
    
    pageCh9_16Meter7 = [[JEProgressView alloc] initWithFrame:CGRectMake(85, 53, 84, 2)];
    pageCh9_16Meter7.transform = CGAffineTransformRotate(pageCh9_16Meter7.transform, 270.0/180*M_PI);
    [pageCh9_16Meter7 setTintColor:[UIColor clearColor]];
    [pageCh9_16Meter7 setProgressImage:pageMeterImage];
    [pageCh9_16Meter7 setTrackImage:[UIImage alloc]];
    [pageCh9_16Meter7 setProgress:0.0];
    [pageCh9_16Meter7 setUserInteractionEnabled:NO];
    [viewTopPanel addSubview:pageCh9_16Meter7];
    
    pageCh9_16Meter8 = [[JEProgressView alloc] initWithFrame:CGRectMake(92, 53, 84, 2)];
    pageCh9_16Meter8.transform = CGAffineTransformRotate(pageCh9_16Meter8.transform, 270.0/180*M_PI);
    [pageCh9_16Meter8 setTintColor:[UIColor clearColor]];
    [pageCh9_16Meter8 setProgressImage:pageMeterImage];
    [pageCh9_16Meter8 setTrackImage:[UIImage alloc]];
    [pageCh9_16Meter8 setProgress:0.0];
    [pageCh9_16Meter8 setUserInteractionEnabled:NO];
    [viewTopPanel addSubview:pageCh9_16Meter8];
    
    pageUsbAudioMeterL = [[JEProgressView alloc] initWithFrame:CGRectMake(123, 53, 84, 2)];
    pageUsbAudioMeterL.transform = CGAffineTransformRotate(pageUsbAudioMeterL.transform, 270.0/180*M_PI);
    [pageUsbAudioMeterL setTintColor:[UIColor clearColor]];
    [pageUsbAudioMeterL setProgressImage:pageMeterImage];
    [pageUsbAudioMeterL setTrackImage:[UIImage alloc]];
    [pageUsbAudioMeterL setProgress:0.0];
    [pageUsbAudioMeterL setUserInteractionEnabled:NO];
    [viewTopPanel addSubview:pageUsbAudioMeterL];
    
    pageUsbAudioMeterR = [[JEProgressView alloc] initWithFrame:CGRectMake(150, 53, 84, 2)];
    pageUsbAudioMeterR.transform = CGAffineTransformRotate(pageUsbAudioMeterR.transform, 270.0/180*M_PI);
    [pageUsbAudioMeterR setTintColor:[UIColor clearColor]];
    [pageUsbAudioMeterR setProgressImage:pageMeterImage];
    [pageUsbAudioMeterR setTrackImage:[UIImage alloc]];
    [pageUsbAudioMeterR setProgress:0.0];
    [pageUsbAudioMeterR setUserInteractionEnabled:NO];
    [viewTopPanel addSubview:pageUsbAudioMeterR];
    
    pageAux1_4Meter1 = [[JEProgressView alloc] initWithFrame:CGRectMake(185, 53, 84, 2)];
    pageAux1_4Meter1.transform = CGAffineTransformRotate(pageAux1_4Meter1.transform, 270.0/180*M_PI);
    [pageAux1_4Meter1 setTintColor:[UIColor clearColor]];
    [pageAux1_4Meter1 setProgressImage:pageMeterImage];
    [pageAux1_4Meter1 setTrackImage:[UIImage alloc]];
    [pageAux1_4Meter1 setProgress:0.0];
    [pageAux1_4Meter1 setUserInteractionEnabled:NO];
    [viewTopPanel addSubview:pageAux1_4Meter1];
    
    pageAux1_4Meter2 = [[JEProgressView alloc] initWithFrame:CGRectMake(199, 53, 84, 2)];
    pageAux1_4Meter2.transform = CGAffineTransformRotate(pageAux1_4Meter2.transform, 270.0/180*M_PI);
    [pageAux1_4Meter2 setTintColor:[UIColor clearColor]];
    [pageAux1_4Meter2 setProgressImage:pageMeterImage];
    [pageAux1_4Meter2 setTrackImage:[UIImage alloc]];
    [pageAux1_4Meter2 setProgress:0.0];
    [pageAux1_4Meter2 setUserInteractionEnabled:NO];
    [viewTopPanel addSubview:pageAux1_4Meter2];
        
    pageAux1_4Meter3 = [[JEProgressView alloc] initWithFrame:CGRectMake(212, 53, 84, 2)];
    pageAux1_4Meter3.transform = CGAffineTransformRotate(pageAux1_4Meter3.transform, 270.0/180*M_PI);
    [pageAux1_4Meter3 setTintColor:[UIColor clearColor]];
    [pageAux1_4Meter3 setProgressImage:pageMeterImage];
    [pageAux1_4Meter3 setTrackImage:[UIImage alloc]];
    [pageAux1_4Meter3 setProgress:0.0];
    [pageAux1_4Meter3 setUserInteractionEnabled:NO];
    [viewTopPanel addSubview:pageAux1_4Meter3];
    
    pageAux1_4Meter4 = [[JEProgressView alloc] initWithFrame:CGRectMake(226, 53, 84, 2)];
    pageAux1_4Meter4.transform = CGAffineTransformRotate(pageAux1_4Meter4.transform, 270.0/180*M_PI);
    [pageAux1_4Meter4 setTintColor:[UIColor clearColor]];
    [pageAux1_4Meter4 setProgressImage:pageMeterImage];
    [pageAux1_4Meter4 setTrackImage:[UIImage alloc]];
    [pageAux1_4Meter4 setProgress:0.0];
    [pageAux1_4Meter4 setUserInteractionEnabled:NO];
    [viewTopPanel addSubview:pageAux1_4Meter4];
    
    pageGp1_4Meter1 = [[JEProgressView alloc] initWithFrame:CGRectMake(254, 53, 84, 2)];
    pageGp1_4Meter1.transform = CGAffineTransformRotate(pageGp1_4Meter1.transform, 270.0/180*M_PI);
    [pageGp1_4Meter1 setTintColor:[UIColor clearColor]];
    [pageGp1_4Meter1 setProgressImage:pageMeterImage];
    [pageGp1_4Meter1 setTrackImage:[UIImage alloc]];
    [pageGp1_4Meter1 setProgress:0.0];
    [pageGp1_4Meter1 setUserInteractionEnabled:NO];
    [viewTopPanel addSubview:pageGp1_4Meter1];
    
    pageGp1_4Meter2 = [[JEProgressView alloc] initWithFrame:CGRectMake(268, 53, 84, 2)];
    pageGp1_4Meter2.transform = CGAffineTransformRotate(pageGp1_4Meter2.transform, 270.0/180*M_PI);
    [pageGp1_4Meter2 setTintColor:[UIColor clearColor]];
    [pageGp1_4Meter2 setProgressImage:pageMeterImage];
    [pageGp1_4Meter2 setTrackImage:[UIImage alloc]];
    [pageGp1_4Meter2 setProgress:0.0];
    [pageGp1_4Meter2 setUserInteractionEnabled:NO];
    [viewTopPanel addSubview:pageGp1_4Meter2];
    
    pageGp1_4Meter3 = [[JEProgressView alloc] initWithFrame:CGRectMake(281, 53, 84, 2)];
    pageGp1_4Meter3.transform = CGAffineTransformRotate(pageGp1_4Meter3.transform, 270.0/180*M_PI);
    [pageGp1_4Meter3 setTintColor:[UIColor clearColor]];
    [pageGp1_4Meter3 setProgressImage:pageMeterImage];
    [pageGp1_4Meter3 setTrackImage:[UIImage alloc]];
    [pageGp1_4Meter3 setProgress:0.0];
    [pageGp1_4Meter3 setUserInteractionEnabled:NO];
    [viewTopPanel addSubview:pageGp1_4Meter3];
    
    pageGp1_4Meter4 = [[JEProgressView alloc] initWithFrame:CGRectMake(295, 53, 84, 2)];
    pageGp1_4Meter4.transform = CGAffineTransformRotate(pageGp1_4Meter4.transform, 270.0/180*M_PI);
    [pageGp1_4Meter4 setTintColor:[UIColor clearColor]];
    [pageGp1_4Meter4 setProgressImage:pageMeterImage];
    [pageGp1_4Meter4 setTrackImage:[UIImage alloc]];
    [pageGp1_4Meter4 setProgress:0.0];
    [pageGp1_4Meter4 setUserInteractionEnabled:NO];
    [viewTopPanel addSubview:pageGp1_4Meter4];
    
    pageEfx1MeterL = [[JEProgressView alloc] initWithFrame:CGRectMake(413, 53, 84, 2)];
    pageEfx1MeterL.transform = CGAffineTransformRotate(pageEfx1MeterL.transform, 270.0/180*M_PI);
    [pageEfx1MeterL setTintColor:[UIColor clearColor]];
    [pageEfx1MeterL setProgressImage:pageMeterImage];
    [pageEfx1MeterL setTrackImage:[UIImage alloc]];
    [pageEfx1MeterL setProgress:0.0];
    [pageEfx1MeterL setUserInteractionEnabled:NO];
    [viewTopPanel addSubview:pageEfx1MeterL];
    
    pageEfx1MeterR = [[JEProgressView alloc] initWithFrame:CGRectMake(441, 53, 84, 2)];
    pageEfx1MeterR.transform = CGAffineTransformRotate(pageEfx1MeterR.transform, 270.0/180*M_PI);
    [pageEfx1MeterR setTintColor:[UIColor clearColor]];
    [pageEfx1MeterR setProgressImage:pageMeterImage];
    [pageEfx1MeterR setTrackImage:[UIImage alloc]];
    [pageEfx1MeterR setProgress:0.0];
    [pageEfx1MeterR setUserInteractionEnabled:NO];
    [viewTopPanel addSubview:pageEfx1MeterR];
    
    pageEfx2MeterL = [[JEProgressView alloc] initWithFrame:CGRectMake(483, 53, 84, 2)];
    pageEfx2MeterL.transform = CGAffineTransformRotate(pageEfx2MeterL.transform, 270.0/180*M_PI);
    [pageEfx2MeterL setTintColor:[UIColor clearColor]];
    [pageEfx2MeterL setProgressImage:pageMeterImage];
    [pageEfx2MeterL setTrackImage:[UIImage alloc]];
    [pageEfx2MeterL setProgress:0.0];
    [pageEfx2MeterL setUserInteractionEnabled:NO];
    [viewTopPanel addSubview:pageEfx2MeterL];
    
    pageEfx2MeterR = [[JEProgressView alloc] initWithFrame:CGRectMake(510, 53, 84, 2)];
    pageEfx2MeterR.transform = CGAffineTransformRotate(pageEfx2MeterR.transform, 270.0/180*M_PI);
    [pageEfx2MeterR setTintColor:[UIColor clearColor]];
    [pageEfx2MeterR setProgressImage:pageMeterImage];
    [pageEfx2MeterR setTrackImage:[UIImage alloc]];
    [pageEfx2MeterR setProgress:0.0];
    [pageEfx2MeterR setUserInteractionEnabled:NO];
    [viewTopPanel addSubview:pageEfx2MeterR];
    
    pageMulti1_4Meter1 = [[JEProgressView alloc] initWithFrame:CGRectMake(544, 53, 84, 2)];
    pageMulti1_4Meter1.transform = CGAffineTransformRotate(pageMulti1_4Meter1.transform, 270.0/180*M_PI);
    [pageMulti1_4Meter1 setTintColor:[UIColor clearColor]];
    [pageMulti1_4Meter1 setProgressImage:pageMeterImage];
    [pageMulti1_4Meter1 setTrackImage:[UIImage alloc]];
    [pageMulti1_4Meter1 setProgress:0.0];
    [pageMulti1_4Meter1 setUserInteractionEnabled:NO];
    [viewTopPanel addSubview:pageMulti1_4Meter1];
    
    pageMulti1_4Meter2 = [[JEProgressView alloc] initWithFrame:CGRectMake(558, 53, 84, 2)];
    pageMulti1_4Meter2.transform = CGAffineTransformRotate(pageMulti1_4Meter2.transform, 270.0/180*M_PI);
    [pageMulti1_4Meter2 setTintColor:[UIColor clearColor]];
    [pageMulti1_4Meter2 setProgressImage:pageMeterImage];
    [pageMulti1_4Meter2 setTrackImage:[UIImage alloc]];
    [pageMulti1_4Meter2 setProgress:0.0];
    [pageMulti1_4Meter2 setUserInteractionEnabled:NO];
    [viewTopPanel addSubview:pageMulti1_4Meter2];
    
    pageMulti1_4Meter3 = [[JEProgressView alloc] initWithFrame:CGRectMake(572, 53, 84, 2)];
    pageMulti1_4Meter3.transform = CGAffineTransformRotate(pageMulti1_4Meter3.transform, 270.0/180*M_PI);
    [pageMulti1_4Meter3 setTintColor:[UIColor clearColor]];
    [pageMulti1_4Meter3 setProgressImage:pageMeterImage];
    [pageMulti1_4Meter3 setTrackImage:[UIImage alloc]];
    [pageMulti1_4Meter3 setProgress:0.0];
    [pageMulti1_4Meter3 setUserInteractionEnabled:NO];
    [viewTopPanel addSubview:pageMulti1_4Meter3];
    
    pageMulti1_4Meter4 = [[JEProgressView alloc] initWithFrame:CGRectMake(586, 53, 84, 2)];
    pageMulti1_4Meter4.transform = CGAffineTransformRotate(pageMulti1_4Meter4.transform, 270.0/180*M_PI);
    [pageMulti1_4Meter4 setTintColor:[UIColor clearColor]];
    [pageMulti1_4Meter4 setProgressImage:pageMeterImage];
    [pageMulti1_4Meter4 setTrackImage:[UIImage alloc]];
    [pageMulti1_4Meter4 setProgress:0.0];
    [pageMulti1_4Meter4 setUserInteractionEnabled:NO];
    [viewTopPanel addSubview:pageMulti1_4Meter4];
    
    pageUsbAudioOutMeterL = [[JEProgressView alloc] initWithFrame:CGRectMake(620, 53, 84, 2)];
    pageUsbAudioOutMeterL.transform = CGAffineTransformRotate(pageUsbAudioOutMeterL.transform, 270.0/180*M_PI);
    [pageUsbAudioOutMeterL setTintColor:[UIColor clearColor]];
    [pageUsbAudioOutMeterL setProgressImage:pageMeterImage];
    [pageUsbAudioOutMeterL setTrackImage:[UIImage alloc]];
    [pageUsbAudioOutMeterL setProgress:0.0];
    [pageUsbAudioOutMeterL setUserInteractionEnabled:NO];
    [viewTopPanel addSubview:pageUsbAudioOutMeterL];
    
    pageUsbAudioOutMeterR = [[JEProgressView alloc] initWithFrame:CGRectMake(648, 53, 84, 2)];
    pageUsbAudioOutMeterR.transform = CGAffineTransformRotate(pageUsbAudioOutMeterR.transform, 270.0/180*M_PI);
    [pageUsbAudioOutMeterR setTintColor:[UIColor clearColor]];
    [pageUsbAudioOutMeterR setProgressImage:pageMeterImage];
    [pageUsbAudioOutMeterR setTrackImage:[UIImage alloc]];
    [pageUsbAudioOutMeterR setProgress:0.0];
    [pageUsbAudioOutMeterR setUserInteractionEnabled:NO];
    [viewTopPanel addSubview:pageUsbAudioOutMeterR];

    pageCtrlRmMeterL = [[JEProgressView alloc] initWithFrame:CGRectMake(689, 53, 84, 2)];
    pageCtrlRmMeterL.transform = CGAffineTransformRotate(pageCtrlRmMeterL.transform, 270.0/180*M_PI);
    [pageCtrlRmMeterL setTintColor:[UIColor clearColor]];
    [pageCtrlRmMeterL setProgressImage:pageMeterImage];
    [pageCtrlRmMeterL setTrackImage:[UIImage alloc]];
    [pageCtrlRmMeterL setProgress:0.0];
    [pageCtrlRmMeterL setUserInteractionEnabled:NO];
    [viewTopPanel addSubview:pageCtrlRmMeterL];
    
    pageCtrlRmMeterR = [[JEProgressView alloc] initWithFrame:CGRectMake(717, 53, 84, 2)];
    pageCtrlRmMeterR.transform = CGAffineTransformRotate(pageCtrlRmMeterR.transform, 270.0/180*M_PI);
    [pageCtrlRmMeterR setTintColor:[UIColor clearColor]];
    [pageCtrlRmMeterR setProgressImage:pageMeterImage];
    [pageCtrlRmMeterR setTrackImage:[UIImage alloc]];
    [pageCtrlRmMeterR setProgress:0.0];
    [pageCtrlRmMeterR setUserInteractionEnabled:NO];
    [viewTopPanel addSubview:pageCtrlRmMeterR];
    
    pageMainMeterL = [[JEProgressView alloc] initWithFrame:CGRectMake(761, 53, 84, 2)];
    pageMainMeterL.transform = CGAffineTransformRotate(pageMainMeterL.transform, 270.0/180*M_PI);
    [pageMainMeterL setTintColor:[UIColor clearColor]];
    [pageMainMeterL setProgressImage:pageMeterImage];
    [pageMainMeterL setTrackImage:[UIImage alloc]];
    [pageMainMeterL setProgress:0.0];
    [pageMainMeterL setUserInteractionEnabled:NO];
    [viewTopPanel addSubview:pageMainMeterL];
    
    pageMainMeterR = [[JEProgressView alloc] initWithFrame:CGRectMake(788, 53, 84, 2)];
    pageMainMeterR.transform = CGAffineTransformRotate(pageMainMeterR.transform, 270.0/180*M_PI);
    [pageMainMeterR setTintColor:[UIColor clearColor]];
    [pageMainMeterR setProgressImage:pageMeterImage];
    [pageMainMeterR setTrackImage:[UIImage alloc]];
    [pageMainMeterR setProgress:0.0];
    [pageMainMeterR setUserInteractionEnabled:NO];
    [viewTopPanel addSubview:pageMainMeterR];
    
    // Add effects panel
    effectController = (EffectViewController *)[[EffectViewController alloc] initWithNibName:@"EffectViewController" bundle:nil];
    [effectController.view setFrame:CGRectMake(0, 0, 756, 615)];
    [viewMainPanel addSubview:effectController.view];
    [effectController.view setHidden:YES];
    
    // Setup page
    knobSigGen.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobSigGen.scalingFactor = 2.0f;
	knobSigGen.maximumValue = SIG_GEN_MAX_VALUE;
	knobSigGen.minimumValue = SIG_GEN_MIN_VALUE;
	knobSigGen.value = SIG_GEN_MIN_VALUE;
	knobSigGen.defaultValue = knobSigGen.value;
	knobSigGen.resetsToDefault = YES;
	knobSigGen.backgroundColor = [UIColor clearColor];
	[knobSigGen setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobSigGen.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobSigGen addTarget:self action:@selector(knobSigGenDidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvSigGen = [[DACircularProgressView alloc] initWithFrame:CGRectMake(506.0f, 213.0f, 65.0f, 65.0f)];
    [viewSetupPage2 addSubview:cpvSigGen];
    [viewSetupPage2 sendSubviewToBack:cpvSigGen];
    
    // Delay page
    knobDelayCh1.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobDelayCh1.scalingFactor = 2.0f;
	knobDelayCh1.maximumValue = DELAY_MAX_VALUE;
	knobDelayCh1.minimumValue = DELAY_MIN_VALUE;
	knobDelayCh1.value = DELAY_MIN_VALUE;
	knobDelayCh1.defaultValue = knobDelayCh1.value;
	knobDelayCh1.resetsToDefault = YES;
	knobDelayCh1.backgroundColor = [UIColor clearColor];
	[knobDelayCh1 setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobDelayCh1.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobDelayCh1 addTarget:self action:@selector(knobDelayCh1DidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvDelayCh1 = [[DACircularProgressView alloc] initWithFrame:CGRectMake(46.0f, 140.0f, 65.0f, 65.0f)];
    [viewDelayPage1 addSubview:cpvDelayCh1];
    [viewDelayPage1 sendSubviewToBack:cpvDelayCh1];

    knobDelayCh2.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobDelayCh2.scalingFactor = 2.0f;
	knobDelayCh2.maximumValue = DELAY_MAX_VALUE;
	knobDelayCh2.minimumValue = DELAY_MIN_VALUE;
	knobDelayCh2.value = DELAY_MIN_VALUE;
	knobDelayCh2.defaultValue = knobDelayCh2.value;
	knobDelayCh2.resetsToDefault = YES;
	knobDelayCh2.backgroundColor = [UIColor clearColor];
	[knobDelayCh2 setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobDelayCh2.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobDelayCh2 addTarget:self action:@selector(knobDelayCh2DidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvDelayCh2 = [[DACircularProgressView alloc] initWithFrame:CGRectMake(146.0f, 140.0f, 65.0f, 65.0f)];
    [viewDelayPage1 addSubview:cpvDelayCh2];
    [viewDelayPage1 sendSubviewToBack:cpvDelayCh2];

    knobDelayCh3.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobDelayCh3.scalingFactor = 2.0f;
	knobDelayCh3.maximumValue = DELAY_MAX_VALUE;
	knobDelayCh3.minimumValue = DELAY_MIN_VALUE;
	knobDelayCh3.value = DELAY_MIN_VALUE;
	knobDelayCh3.defaultValue = knobDelayCh3.value;
	knobDelayCh3.resetsToDefault = YES;
	knobDelayCh3.backgroundColor = [UIColor clearColor];
	[knobDelayCh3 setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobDelayCh3.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobDelayCh3 addTarget:self action:@selector(knobDelayCh3DidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvDelayCh3 = [[DACircularProgressView alloc] initWithFrame:CGRectMake(246.0f, 140.0f, 65.0f, 65.0f)];
    [viewDelayPage1 addSubview:cpvDelayCh3];
    [viewDelayPage1 sendSubviewToBack:cpvDelayCh3];

    knobDelayCh4.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobDelayCh4.scalingFactor = 2.0f;
	knobDelayCh4.maximumValue = DELAY_MAX_VALUE;
	knobDelayCh4.minimumValue = DELAY_MIN_VALUE;
	knobDelayCh4.value = DELAY_MIN_VALUE;
	knobDelayCh4.defaultValue = knobDelayCh4.value;
	knobDelayCh4.resetsToDefault = YES;
	knobDelayCh4.backgroundColor = [UIColor clearColor];
	[knobDelayCh4 setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobDelayCh4.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobDelayCh4 addTarget:self action:@selector(knobDelayCh4DidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvDelayCh4 = [[DACircularProgressView alloc] initWithFrame:CGRectMake(346.0f, 140.0f, 65.0f, 65.0f)];
    [viewDelayPage1 addSubview:cpvDelayCh4];
    [viewDelayPage1 sendSubviewToBack:cpvDelayCh4];

    knobDelayCh5.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobDelayCh5.scalingFactor = 2.0f;
	knobDelayCh5.maximumValue = DELAY_MAX_VALUE;
	knobDelayCh5.minimumValue = DELAY_MIN_VALUE;
	knobDelayCh5.value = DELAY_MIN_VALUE;
	knobDelayCh5.defaultValue = knobDelayCh5.value;
	knobDelayCh5.resetsToDefault = YES;
	knobDelayCh5.backgroundColor = [UIColor clearColor];
	[knobDelayCh5 setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobDelayCh5.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobDelayCh5 addTarget:self action:@selector(knobDelayCh5DidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvDelayCh5 = [[DACircularProgressView alloc] initWithFrame:CGRectMake(446.0f, 140.0f, 65.0f, 65.0f)];
    [viewDelayPage1 addSubview:cpvDelayCh5];
    [viewDelayPage1 sendSubviewToBack:cpvDelayCh5];

    knobDelayCh6.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobDelayCh6.scalingFactor = 2.0f;
	knobDelayCh6.maximumValue = DELAY_MAX_VALUE;
	knobDelayCh6.minimumValue = DELAY_MIN_VALUE;
	knobDelayCh6.value = DELAY_MIN_VALUE;
	knobDelayCh6.defaultValue = knobDelayCh6.value;
	knobDelayCh6.resetsToDefault = YES;
	knobDelayCh6.backgroundColor = [UIColor clearColor];
	[knobDelayCh6 setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobDelayCh6.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobDelayCh6 addTarget:self action:@selector(knobDelayCh6DidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvDelayCh6 = [[DACircularProgressView alloc] initWithFrame:CGRectMake(546.0f, 140.0f, 65.0f, 65.0f)];
    [viewDelayPage1 addSubview:cpvDelayCh6];
    [viewDelayPage1 sendSubviewToBack:cpvDelayCh6];

    knobDelayCh7.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobDelayCh7.scalingFactor = 2.0f;
	knobDelayCh7.maximumValue = DELAY_MAX_VALUE;
	knobDelayCh7.minimumValue = DELAY_MIN_VALUE;
	knobDelayCh7.value = DELAY_MIN_VALUE;
	knobDelayCh7.defaultValue = knobDelayCh7.value;
	knobDelayCh7.resetsToDefault = YES;
	knobDelayCh7.backgroundColor = [UIColor clearColor];
	[knobDelayCh7 setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobDelayCh7.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobDelayCh7 addTarget:self action:@selector(knobDelayCh7DidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvDelayCh7 = [[DACircularProgressView alloc] initWithFrame:CGRectMake(646.0f, 140.0f, 65.0f, 65.0f)];
    [viewDelayPage1 addSubview:cpvDelayCh7];
    [viewDelayPage1 sendSubviewToBack:cpvDelayCh7];

    knobDelayCh8.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobDelayCh8.scalingFactor = 2.0f;
	knobDelayCh8.maximumValue = DELAY_MAX_VALUE;
	knobDelayCh8.minimumValue = DELAY_MIN_VALUE;
	knobDelayCh8.value = DELAY_MIN_VALUE;
	knobDelayCh8.defaultValue = knobDelayCh8.value;
	knobDelayCh8.resetsToDefault = YES;
	knobDelayCh8.backgroundColor = [UIColor clearColor];
	[knobDelayCh8 setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobDelayCh8.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobDelayCh8 addTarget:self action:@selector(knobDelayCh8DidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvDelayCh8 = [[DACircularProgressView alloc] initWithFrame:CGRectMake(746.0f, 140.0f, 65.0f, 65.0f)];
    [viewDelayPage1 addSubview:cpvDelayCh8];
    [viewDelayPage1 sendSubviewToBack:cpvDelayCh8];

    knobDelayCh9.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobDelayCh9.scalingFactor = 2.0f;
	knobDelayCh9.maximumValue = DELAY_MAX_VALUE;
	knobDelayCh9.minimumValue = DELAY_MIN_VALUE;
	knobDelayCh9.value = DELAY_MIN_VALUE;
	knobDelayCh9.defaultValue = knobDelayCh9.value;
	knobDelayCh9.resetsToDefault = YES;
	knobDelayCh9.backgroundColor = [UIColor clearColor];
	[knobDelayCh9 setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobDelayCh9.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobDelayCh9 addTarget:self action:@selector(knobDelayCh9DidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvDelayCh9 = [[DACircularProgressView alloc] initWithFrame:CGRectMake(46.0f, 343.0f, 65.0f, 65.0f)];
    [viewDelayPage1 addSubview:cpvDelayCh9];
    [viewDelayPage1 sendSubviewToBack:cpvDelayCh9];

    knobDelayCh10.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobDelayCh10.scalingFactor = 2.0f;
	knobDelayCh10.maximumValue = DELAY_MAX_VALUE;
	knobDelayCh10.minimumValue = DELAY_MIN_VALUE;
	knobDelayCh10.value = DELAY_MIN_VALUE;
	knobDelayCh10.defaultValue = knobDelayCh10.value;
	knobDelayCh10.resetsToDefault = YES;
	knobDelayCh10.backgroundColor = [UIColor clearColor];
	[knobDelayCh10 setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobDelayCh10.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobDelayCh10 addTarget:self action:@selector(knobDelayCh10DidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvDelayCh10 = [[DACircularProgressView alloc] initWithFrame:CGRectMake(146.0f, 343, 65.0f, 65.0f)];
    [viewDelayPage1 addSubview:cpvDelayCh10];
    [viewDelayPage1 sendSubviewToBack:cpvDelayCh10];

    knobDelayCh11.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobDelayCh11.scalingFactor = 2.0f;
	knobDelayCh11.maximumValue = DELAY_MAX_VALUE;
	knobDelayCh11.minimumValue = DELAY_MIN_VALUE;
	knobDelayCh11.value = DELAY_MIN_VALUE;
	knobDelayCh11.defaultValue = knobDelayCh11.value;
	knobDelayCh11.resetsToDefault = YES;
	knobDelayCh11.backgroundColor = [UIColor clearColor];
	[knobDelayCh11 setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobDelayCh11.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobDelayCh11 addTarget:self action:@selector(knobDelayCh11DidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvDelayCh11 = [[DACircularProgressView alloc] initWithFrame:CGRectMake(246.0f, 343.0f, 65.0f, 65.0f)];
    [viewDelayPage1 addSubview:cpvDelayCh11];
    [viewDelayPage1 sendSubviewToBack:cpvDelayCh11];

    knobDelayCh12.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobDelayCh12.scalingFactor = 2.0f;
	knobDelayCh12.maximumValue = DELAY_MAX_VALUE;
	knobDelayCh12.minimumValue = DELAY_MIN_VALUE;
	knobDelayCh12.value = DELAY_MIN_VALUE;
	knobDelayCh12.defaultValue = knobDelayCh12.value;
	knobDelayCh12.resetsToDefault = YES;
	knobDelayCh12.backgroundColor = [UIColor clearColor];
	[knobDelayCh12 setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobDelayCh12.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobDelayCh12 addTarget:self action:@selector(knobDelayCh12DidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvDelayCh12 = [[DACircularProgressView alloc] initWithFrame:CGRectMake(346.0f, 343.0f, 65.0f, 65.0f)];
    [viewDelayPage1 addSubview:cpvDelayCh12];
    [viewDelayPage1 sendSubviewToBack:cpvDelayCh12];

    knobDelayCh13.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobDelayCh13.scalingFactor = 2.0f;
	knobDelayCh13.maximumValue = DELAY_MAX_VALUE;
	knobDelayCh13.minimumValue = DELAY_MIN_VALUE;
	knobDelayCh13.value = DELAY_MIN_VALUE;
	knobDelayCh13.defaultValue = knobDelayCh13.value;
	knobDelayCh13.resetsToDefault = YES;
	knobDelayCh13.backgroundColor = [UIColor clearColor];
	[knobDelayCh13 setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobDelayCh13.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobDelayCh13 addTarget:self action:@selector(knobDelayCh13DidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvDelayCh13 = [[DACircularProgressView alloc] initWithFrame:CGRectMake(446.0f, 343.0f, 65.0f, 65.0f)];
    [viewDelayPage1 addSubview:cpvDelayCh13];
    [viewDelayPage1 sendSubviewToBack:cpvDelayCh13];

    knobDelayCh14.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobDelayCh14.scalingFactor = 2.0f;
	knobDelayCh14.maximumValue = DELAY_MAX_VALUE;
	knobDelayCh14.minimumValue = DELAY_MIN_VALUE;
	knobDelayCh14.value = DELAY_MIN_VALUE;
	knobDelayCh14.defaultValue = knobDelayCh14.value;
	knobDelayCh14.resetsToDefault = YES;
	knobDelayCh14.backgroundColor = [UIColor clearColor];
	[knobDelayCh14 setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobDelayCh14.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobDelayCh14 addTarget:self action:@selector(knobDelayCh14DidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvDelayCh14 = [[DACircularProgressView alloc] initWithFrame:CGRectMake(546.0f, 343.0f, 65.0f, 65.0f)];
    [viewDelayPage1 addSubview:cpvDelayCh14];
    [viewDelayPage1 sendSubviewToBack:cpvDelayCh14];

    knobDelayCh15.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobDelayCh15.scalingFactor = 2.0f;
	knobDelayCh15.maximumValue = DELAY_MAX_VALUE;
	knobDelayCh15.minimumValue = DELAY_MIN_VALUE;
	knobDelayCh15.value = DELAY_MIN_VALUE;
	knobDelayCh15.defaultValue = knobDelayCh15.value;
	knobDelayCh15.resetsToDefault = YES;
	knobDelayCh15.backgroundColor = [UIColor clearColor];
	[knobDelayCh15 setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobDelayCh15.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobDelayCh15 addTarget:self action:@selector(knobDelayCh15DidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvDelayCh15 = [[DACircularProgressView alloc] initWithFrame:CGRectMake(646.0f, 343.0f, 65.0f, 65.0f)];
    [viewDelayPage1 addSubview:cpvDelayCh15];
    [viewDelayPage1 sendSubviewToBack:cpvDelayCh15];

    knobDelayCh16.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobDelayCh16.scalingFactor = 2.0f;
	knobDelayCh16.maximumValue = DELAY_MAX_VALUE;
	knobDelayCh16.minimumValue = DELAY_MIN_VALUE;
	knobDelayCh16.value = DELAY_MIN_VALUE;
	knobDelayCh16.defaultValue = knobDelayCh16.value;
	knobDelayCh16.resetsToDefault = YES;
	knobDelayCh16.backgroundColor = [UIColor clearColor];
	[knobDelayCh16 setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobDelayCh16.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobDelayCh16 addTarget:self action:@selector(knobDelayCh16DidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvDelayCh16 = [[DACircularProgressView alloc] initWithFrame:CGRectMake(746.0f, 343.0f, 65.0f, 65.0f)];
    [viewDelayPage1 addSubview:cpvDelayCh16];
    [viewDelayPage1 sendSubviewToBack:cpvDelayCh16];

    knobDelayCh17.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobDelayCh17.scalingFactor = 2.0f;
	knobDelayCh17.maximumValue = DELAY_MAX_VALUE;
	knobDelayCh17.minimumValue = DELAY_MIN_VALUE;
	knobDelayCh17.value = DELAY_MIN_VALUE;
	knobDelayCh17.defaultValue = knobDelayCh17.value;
	knobDelayCh17.resetsToDefault = YES;
	knobDelayCh17.backgroundColor = [UIColor clearColor];
	[knobDelayCh17 setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobDelayCh17.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobDelayCh17 addTarget:self action:@selector(knobDelayCh17DidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvDelayCh17 = [[DACircularProgressView alloc] initWithFrame:CGRectMake(41.0f, 136.0f, 65.0f, 65.0f)];
    [viewDelayPage2 addSubview:cpvDelayCh17];
    [viewDelayPage2 sendSubviewToBack:cpvDelayCh17];

    knobDelayCh18.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobDelayCh18.scalingFactor = 2.0f;
	knobDelayCh18.maximumValue = DELAY_MAX_VALUE;
	knobDelayCh18.minimumValue = DELAY_MIN_VALUE;
	knobDelayCh18.value = DELAY_MIN_VALUE;
	knobDelayCh18.defaultValue = knobDelayCh18.value;
	knobDelayCh18.resetsToDefault = YES;
	knobDelayCh18.backgroundColor = [UIColor clearColor];
	[knobDelayCh18 setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobDelayCh18.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobDelayCh18 addTarget:self action:@selector(knobDelayCh18DidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvDelayCh18 = [[DACircularProgressView alloc] initWithFrame:CGRectMake(141.0f, 136.0f, 65.0f, 65.0f)];
    [viewDelayPage2 addSubview:cpvDelayCh18];
    [viewDelayPage2 sendSubviewToBack:cpvDelayCh18];

    knobDelayMt1.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobDelayMt1.scalingFactor = 2.0f;
	knobDelayMt1.maximumValue = DELAY_MAX_VALUE;
	knobDelayMt1.minimumValue = DELAY_MIN_VALUE;
	knobDelayMt1.value = DELAY_MIN_VALUE;
	knobDelayMt1.defaultValue = knobDelayMt1.value;
	knobDelayMt1.resetsToDefault = YES;
	knobDelayMt1.backgroundColor = [UIColor clearColor];
	[knobDelayMt1 setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobDelayMt1.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobDelayMt1 addTarget:self action:@selector(knobDelayMt1DidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvDelayMt1 = [[DACircularProgressView alloc] initWithFrame:CGRectMake(195.0f, 133.0f, 65.0f, 65.0f)];
    [viewDelayPage3 addSubview:cpvDelayMt1];
    [viewDelayPage3 sendSubviewToBack:cpvDelayMt1];

    knobDelayMt2.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobDelayMt2.scalingFactor = 2.0f;
	knobDelayMt2.maximumValue = DELAY_MAX_VALUE;
	knobDelayMt2.minimumValue = DELAY_MIN_VALUE;
	knobDelayMt2.value = DELAY_MIN_VALUE;
	knobDelayMt2.defaultValue = knobDelayMt2.value;
	knobDelayMt2.resetsToDefault = YES;
	knobDelayMt2.backgroundColor = [UIColor clearColor];
	[knobDelayMt2 setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobDelayMt2.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobDelayMt2 addTarget:self action:@selector(knobDelayMt2DidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvDelayMt2 = [[DACircularProgressView alloc] initWithFrame:CGRectMake(295.0f, 133.0f, 65.0f, 65.0f)];
    [viewDelayPage3 addSubview:cpvDelayMt2];
    [viewDelayPage3 sendSubviewToBack:cpvDelayMt2];

    knobDelayMt3.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobDelayMt3.scalingFactor = 2.0f;
	knobDelayMt3.maximumValue = DELAY_MAX_VALUE;
	knobDelayMt3.minimumValue = DELAY_MIN_VALUE;
	knobDelayMt3.value = DELAY_MIN_VALUE;
	knobDelayMt3.defaultValue = knobDelayMt3.value;
	knobDelayMt3.resetsToDefault = YES;
	knobDelayMt3.backgroundColor = [UIColor clearColor];
	[knobDelayMt3 setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobDelayMt3.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobDelayMt3 addTarget:self action:@selector(knobDelayMt3DidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvDelayMt3 = [[DACircularProgressView alloc] initWithFrame:CGRectMake(395.0f, 133.0f, 65.0f, 65.0f)];
    [viewDelayPage3 addSubview:cpvDelayMt3];
    [viewDelayPage3 sendSubviewToBack:cpvDelayMt3];

    knobDelayMt4.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobDelayMt4.scalingFactor = 2.0f;
	knobDelayMt4.maximumValue = DELAY_MAX_VALUE;
	knobDelayMt4.minimumValue = DELAY_MIN_VALUE;
	knobDelayMt4.value = DELAY_MIN_VALUE;
	knobDelayMt4.defaultValue = knobDelayMt4.value;
	knobDelayMt4.resetsToDefault = YES;
	knobDelayMt4.backgroundColor = [UIColor clearColor];
	[knobDelayMt4 setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobDelayMt4.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobDelayMt4 addTarget:self action:@selector(knobDelayMt4DidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvDelayMt4 = [[DACircularProgressView alloc] initWithFrame:CGRectMake(495.0f, 133.0f, 65.0f, 65.0f)];
    [viewDelayPage3 addSubview:cpvDelayMt4];
    [viewDelayPage3 sendSubviewToBack:cpvDelayMt4];

    knobDelayMain.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobDelayMain.scalingFactor = 2.0f;
	knobDelayMain.maximumValue = DELAY_MAX_VALUE;
	knobDelayMain.minimumValue = DELAY_MIN_VALUE;
	knobDelayMain.value = DELAY_MIN_VALUE;
	knobDelayMain.defaultValue = knobDelayMain.value;
	knobDelayMain.resetsToDefault = YES;
	knobDelayMain.backgroundColor = [UIColor clearColor];
	[knobDelayMain setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobDelayMain.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobDelayMain addTarget:self action:@selector(knobDelayMainDidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvDelayMain = [[DACircularProgressView alloc] initWithFrame:CGRectMake(595.0f, 133.0f, 65.0f, 65.0f)];
    [viewDelayPage3 addSubview:cpvDelayMain];
    [viewDelayPage3 sendSubviewToBack:cpvDelayMain];

    knobDelayTemp.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobDelayTemp.scalingFactor = 2.0f;
	knobDelayTemp.maximumValue = TEMP_MAX_VALUE;
	knobDelayTemp.minimumValue = TEMP_MIN_VALUE;
	knobDelayTemp.value = TEMP_MIN_VALUE;
	knobDelayTemp.defaultValue = knobDelayTemp.value;
	knobDelayTemp.resetsToDefault = YES;
	knobDelayTemp.backgroundColor = [UIColor clearColor];
	[knobDelayTemp setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobDelayTemp.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobDelayTemp addTarget:self action:@selector(knobDelayTempDidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvDelayTemp = [[DACircularProgressView alloc] initWithFrame:CGRectMake(494.0f, 309.0f, 65.0f, 65.0f)];
    [viewDelayPage3 addSubview:cpvDelayTemp];
    [viewDelayPage3 sendSubviewToBack:cpvDelayTemp];

    // Init variables
    currentLabel = 1;
    currentMode = MODE_PHANTOM_POWER;
    _modeEnabled = YES;
    [self refreshModeLabel];
    [self refreshModeButtons];

    [self resetTopPanelButtons];
    currentPage = PAGE_CH_1_8;
    currentPageType = PRIMARY;
    [btnMeterCh1_8 setSelected:YES];
    [btnCh1_8 setSelected:YES];
    [self refreshPage];
    
    btnSetupPage1.selected = YES;
    btnDelayPage1.selected = YES;
    btnOrderPage1.selected = YES;

    _sceneCount = 0;
    _sceneFiles = [[NSMutableArray alloc] init];
    _usbMount = 0;
    
    // Create read command queue
    _readCmdQueue = [[NSMutableArray alloc] init];
    
    // Create write ack queue
    _writeAckQueue = [[NSMutableArray alloc] init];
    
    // Init values
    _chOffOn = 0;
    _chSoloCtrl = 0;
    _chMeterPrePost = 0xFFFF;
    _auxGpOffOn = 0;
    _auxGpSoloCtrl = 0;
    _auxGpMeterPrePost = 0;
    _clkSrcTracking = CLK_SRC_NONE;
    _usbAudioOutL = USB_ASSIGN_NULL;
    _usbAudioOutR = USB_ASSIGN_NULL;
    _currentSceneMode = SCENE_MODE_SCENE;
    
    for (int i=0; i<MAX_CHANNELS; i++) {
        orderChannel[i] = ORDER_CH_EQ_DYN_DELAY;
        invOffOn[i] = NO;
    }
    for (int i=0; i<MAX_MULTI; i++) {
        orderMulti[i] = ORDER_MT_EQ_DYN_DELAY;
    }
    orderMain = ORDER_MAIN_EQ_DYN_DELAY;
    
    btnDelayTime.selected = YES;
    btnDelayMeter.selected = NO;
    btnDelayFeet.selected = NO;
    _delayScale = DELAY_TIME;
    _seqNum = 0;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchesBegan:(NSSet*)touches withEvent:(UIEvent*)event
{
    // Detect touches for scene view
    UITouch *touch = [touches anyObject];
    if (touch.view.tag == SCENE_VIEW_TAG) {
        [self setFileSceneMode:SCENE_MODE_SCENE sceneChannel:0];
        [viewScenes setHidden:NO];
        [self.view bringSubviewToFront:viewScenes];
        // Hide setup view if shown
        if (!viewSetup.isHidden) {
            [viewSetup setHidden:YES];
            [btnSetup setBackgroundImage:[UIImage imageNamed:@"button-8.png"] forState:UIControlStateNormal];
        }
    }
}

#pragma mark -
#pragma mark Button event handler methods

- (IBAction)onBtnPanEq1:(id)sender {
    if (currentMode == MODE_EQ) {
        switch (currentPage) {
            case PAGE_CH_1_8:
                [self setSendPage:EQ_PAGE reserve2:0];
                [self showGeqPeqDialog:CHANNEL_CH_1];
                break;
            case PAGE_CH_9_16:
                [self setSendPage:EQ_PAGE reserve2:0];
                [self showGeqPeqDialog:CHANNEL_CH_9];
                break;
            case PAGE_USB_AUDIO:
                [self setSendPage:EQ_PAGE reserve2:0];
                [self showGeqPeqDialog:CHANNEL_CH_17];
                break;
            case PAGE_MT_1_4:
                [self setSendPage:EQ_PAGE reserve2:1];
                [self showGeqPeqDialog:CHANNEL_MULTI_1];
                break;
            default:
                break;
        }
    } else if (currentMode == MODE_PHANTOM_POWER) {
        [self setSendPage:DELAY_48V_PAGE reserve2:0];
        [viewPhantomPower setHidden:NO];
    } else if (currentMode == MODE_DELAY) {
        [self setSendPage:DELAY_48V_PAGE reserve2:0];
        [viewDelay setHidden:NO];
    } else if (currentMode == MODE_ORDER) {
        [self setSendPage:ORDER_PAGE reserve2:0];
        [viewOrder setHidden:NO];
    } else if (currentMode == MODE_INV) {
        switch (currentPage) {
            case PAGE_CH_1_8:
                invOffOn[CH_1] = !invOffOn[CH_1];
                if (invOffOn[CH_1]) {
                    [btnSliderPanEq1 setBackgroundImage:[UIImage imageNamed:@"pan-15.png"] forState:UIControlStateNormal];
                } else {
                    [btnSliderPanEq1 setBackgroundImage:[UIImage imageNamed:@"pan-14.png"] forState:UIControlStateNormal];
                }
                [Global setClearBit:&_ch1Ctrl bitValue:invOffOn[CH_1] bitOrder:12];
                [self sendData:CMD_CH1_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch1Ctrl];
                break;
            case PAGE_CH_9_16:
                invOffOn[CH_9] = !invOffOn[CH_9];
                if (invOffOn[CH_9]) {
                    [btnSliderPanEq1 setBackgroundImage:[UIImage imageNamed:@"pan-15.png"] forState:UIControlStateNormal];
                } else {
                    [btnSliderPanEq1 setBackgroundImage:[UIImage imageNamed:@"pan-14.png"] forState:UIControlStateNormal];
                }
                [Global setClearBit:&_ch9Ctrl bitValue:invOffOn[CH_9] bitOrder:12];
                [self sendData:CMD_CH9_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch9Ctrl];
                break;
            case PAGE_USB_AUDIO:
                invOffOn[CH_17] = !invOffOn[CH_17];
                if (invOffOn[CH_17]) {
                    [btnSliderPanEq1 setBackgroundImage:[UIImage imageNamed:@"pan-15.png"] forState:UIControlStateNormal];
                } else {
                    [btnSliderPanEq1 setBackgroundImage:[UIImage imageNamed:@"pan-14.png"] forState:UIControlStateNormal];
                }
                [Global setClearBit:&_ch17Ctrl bitValue:invOffOn[CH_17] bitOrder:12];
                [self sendData:CMD_CH17_CTRL commandType:DSP_WRITE dspId:DSP_6 value:_ch17Ctrl];
                break;
            default:
                break;
        }
    } else if (currentMode == MODE_DYN) {
        [self setSendPage:DYN_PAGE reserve2:0];
        int channel = 0;
        switch (currentPage) {
            case PAGE_CH_1_8:
                channel = CHANNEL_CH_1;
                break;
            case PAGE_CH_9_16:
                channel = CHANNEL_CH_9;
                break;
            case PAGE_USB_AUDIO:
                channel = CHANNEL_CH_17;
                break;
            case PAGE_MT_1_4:
                channel = CHANNEL_MULTI_1;
                break;
            default:
                break;
        }
        [self showDynDialog:channel];
    }
}

- (IBAction)onBtnPanEq2:(id)sender {
    if (currentMode == MODE_EQ) {
        switch (currentPage) {
            case PAGE_CH_1_8:
                [self setSendPage:EQ_PAGE reserve2:0];
                [self showGeqPeqDialog:CHANNEL_CH_2];
                break;
            case PAGE_CH_9_16:
                [self setSendPage:EQ_PAGE reserve2:0];
                [self showGeqPeqDialog:CHANNEL_CH_10];
                break;
            case PAGE_USB_AUDIO:
                [self setSendPage:EQ_PAGE reserve2:0];
                [self showGeqPeqDialog:CHANNEL_CH_18];
                break;
            case PAGE_MT_1_4:
                [self setSendPage:EQ_PAGE reserve2:1];
                [self showGeqPeqDialog:CHANNEL_MULTI_2];
                break;
            default:
                break;
        }
    } else if (currentMode == MODE_PHANTOM_POWER) {
        [self setSendPage:DELAY_48V_PAGE reserve2:0];
        [viewPhantomPower setHidden:NO];
    } else if (currentMode == MODE_DELAY) {
        [self setSendPage:DELAY_48V_PAGE reserve2:0];
        [viewDelay setHidden:NO];
    } else if (currentMode == MODE_ORDER) {
        [self setSendPage:ORDER_PAGE reserve2:0];
        [viewOrder setHidden:NO];
    } else if (currentMode == MODE_INV) {
        switch (currentPage) {
            case PAGE_CH_1_8:
                invOffOn[CH_2] = !invOffOn[CH_2];
                if (invOffOn[CH_2]) {
                    [btnSliderPanEq2 setBackgroundImage:[UIImage imageNamed:@"pan-15.png"] forState:UIControlStateNormal];
                } else {
                    [btnSliderPanEq2 setBackgroundImage:[UIImage imageNamed:@"pan-14.png"] forState:UIControlStateNormal];
                }
                [Global setClearBit:&_ch2Ctrl bitValue:invOffOn[CH_2] bitOrder:12];
                [self sendData:CMD_CH2_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch2Ctrl];
                break;
            case PAGE_CH_9_16:
                invOffOn[CH_10] = !invOffOn[CH_10];
                if (invOffOn[CH_10]) {
                    [btnSliderPanEq2 setBackgroundImage:[UIImage imageNamed:@"pan-15.png"] forState:UIControlStateNormal];
                } else {
                    [btnSliderPanEq2 setBackgroundImage:[UIImage imageNamed:@"pan-14.png"] forState:UIControlStateNormal];
                }
                [Global setClearBit:&_ch10Ctrl bitValue:invOffOn[CH_10] bitOrder:12];
                [self sendData:CMD_CH10_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch10Ctrl];
                break;
            case PAGE_USB_AUDIO:
                invOffOn[CH_18] = !invOffOn[CH_18];
                if (invOffOn[CH_18]) {
                    [btnSliderPanEq2 setBackgroundImage:[UIImage imageNamed:@"pan-15.png"] forState:UIControlStateNormal];
                } else {
                    [btnSliderPanEq2 setBackgroundImage:[UIImage imageNamed:@"pan-14.png"] forState:UIControlStateNormal];
                }
                [Global setClearBit:&_ch18Ctrl bitValue:invOffOn[CH_18] bitOrder:12];
                [self sendData:CMD_CH18_CTRL commandType:DSP_WRITE dspId:DSP_6 value:_ch18Ctrl];
                break;
            default:
                break;
        }
    } else if (currentMode == MODE_DYN) {
        [self setSendPage:DYN_PAGE reserve2:0];
        int channel = 0;
        switch (currentPage) {
            case PAGE_CH_1_8:
                channel = CHANNEL_CH_2;
                break;
            case PAGE_CH_9_16:
                channel = CHANNEL_CH_10;
                break;
            case PAGE_USB_AUDIO:
                channel = CHANNEL_CH_18;
                break;
            case PAGE_MT_1_4:
                channel = CHANNEL_MULTI_2;
                break;
            default:
                break;
        }
        [self showDynDialog:channel];
    }
}

- (IBAction)onBtnPanEq3:(id)sender {
    if (currentMode == MODE_EQ) {
        switch (currentPage) {
            case PAGE_CH_1_8:
                [self setSendPage:EQ_PAGE reserve2:0];
                [self showGeqPeqDialog:CHANNEL_CH_3];
                break;
            case PAGE_CH_9_16:
                [self setSendPage:EQ_PAGE reserve2:0];
                [self showGeqPeqDialog:CHANNEL_CH_11];
                break;
            case PAGE_MT_1_4:
                [self setSendPage:EQ_PAGE reserve2:1];
                [self showGeqPeqDialog:CHANNEL_MULTI_3];
                break;
            default:
                break;
        }
    } else if (currentMode == MODE_PHANTOM_POWER) {
        [self setSendPage:DELAY_48V_PAGE reserve2:0];
        [viewPhantomPower setHidden:NO];
    } else if (currentMode == MODE_DELAY) {
        [self setSendPage:DELAY_48V_PAGE reserve2:0];
        [viewDelay setHidden:NO];
    } else if (currentMode == MODE_ORDER) {
        [self setSendPage:ORDER_PAGE reserve2:0];
        [viewOrder setHidden:NO];
    } else if (currentMode == MODE_INV) {
        switch (currentPage) {
            case PAGE_CH_1_8:
                invOffOn[CH_3] = !invOffOn[CH_3];
                if (invOffOn[CH_3]) {
                    [btnSliderPanEq3 setBackgroundImage:[UIImage imageNamed:@"pan-15.png"] forState:UIControlStateNormal];
                } else {
                    [btnSliderPanEq3 setBackgroundImage:[UIImage imageNamed:@"pan-14.png"] forState:UIControlStateNormal];
                }
                [Global setClearBit:&_ch3Ctrl bitValue:invOffOn[CH_3] bitOrder:12];
                [self sendData:CMD_CH3_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch3Ctrl];
                break;
            case PAGE_CH_9_16:
                invOffOn[CH_11] = !invOffOn[CH_11];
                if (invOffOn[CH_11]) {
                    [btnSliderPanEq3 setBackgroundImage:[UIImage imageNamed:@"pan-15.png"] forState:UIControlStateNormal];
                } else {
                    [btnSliderPanEq3 setBackgroundImage:[UIImage imageNamed:@"pan-14.png"] forState:UIControlStateNormal];
                }
                [Global setClearBit:&_ch11Ctrl bitValue:invOffOn[CH_11] bitOrder:12];
                [self sendData:CMD_CH11_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch11Ctrl];
                break;
            default:
                break;
        }
    } else if (currentMode == MODE_DYN) {
        [self setSendPage:DYN_PAGE reserve2:0];
        int channel = 0;
        switch (currentPage) {
            case PAGE_CH_1_8:
                channel = CHANNEL_CH_3;
                break;
            case PAGE_CH_9_16:
                channel = CHANNEL_CH_11;
                break;
            case PAGE_MT_1_4:
                channel = CHANNEL_MULTI_3;
                break;
            default:
                break;
        }
        [self showDynDialog:channel];
    }
}

- (IBAction)onBtnPanEq4:(id)sender {
    if (currentMode == MODE_EQ) {
        switch (currentPage) {
            case PAGE_CH_1_8:
                [self setSendPage:EQ_PAGE reserve2:0];
                [self showGeqPeqDialog:CHANNEL_CH_4];
                break;
            case PAGE_CH_9_16:
                [self setSendPage:EQ_PAGE reserve2:0];
                [self showGeqPeqDialog:CHANNEL_CH_12];
                break;
            case PAGE_MT_1_4:
                [self setSendPage:EQ_PAGE reserve2:1];
                [self showGeqPeqDialog:CHANNEL_MULTI_4];
                break;
            default:
                break;
        }
    } else if (currentMode == MODE_PHANTOM_POWER) {
        [self setSendPage:DELAY_48V_PAGE reserve2:0];
        [viewPhantomPower setHidden:NO];
    } else if (currentMode == MODE_DELAY) {
        [self setSendPage:DELAY_48V_PAGE reserve2:0];
        [viewDelay setHidden:NO];
    } else if (currentMode == MODE_ORDER) {
        [self setSendPage:ORDER_PAGE reserve2:0];
        [viewOrder setHidden:NO];
    } else if (currentMode == MODE_INV) {
        switch (currentPage) {
            case PAGE_CH_1_8:
                invOffOn[CH_4] = !invOffOn[CH_4];
                if (invOffOn[CH_4]) {
                    [btnSliderPanEq4 setBackgroundImage:[UIImage imageNamed:@"pan-15.png"] forState:UIControlStateNormal];
                } else {
                    [btnSliderPanEq4 setBackgroundImage:[UIImage imageNamed:@"pan-14.png"] forState:UIControlStateNormal];
                }
                [Global setClearBit:&_ch4Ctrl bitValue:invOffOn[CH_4] bitOrder:12];
                [self sendData:CMD_CH4_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch4Ctrl];
                break;
            case PAGE_CH_9_16:
                invOffOn[CH_12] = !invOffOn[CH_12];
                if (invOffOn[CH_12]) {
                    [btnSliderPanEq4 setBackgroundImage:[UIImage imageNamed:@"pan-15.png"] forState:UIControlStateNormal];
                } else {
                    [btnSliderPanEq4 setBackgroundImage:[UIImage imageNamed:@"pan-14.png"] forState:UIControlStateNormal];
                }
                [Global setClearBit:&_ch12Ctrl bitValue:invOffOn[CH_12] bitOrder:12];
                [self sendData:CMD_CH12_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch12Ctrl];
                break;
            default:
                break;
        }
    } else if (currentMode == MODE_DYN) {
        [self setSendPage:DYN_PAGE reserve2:0];
        int channel = 0;
        switch (currentPage) {
            case PAGE_CH_1_8:
                channel = CHANNEL_CH_4;
                break;
            case PAGE_CH_9_16:
                channel = CHANNEL_CH_12;
                break;
            case PAGE_MT_1_4:
                channel = CHANNEL_MULTI_4;
                break;
            default:
                break;
        }
        [self showDynDialog:channel];
    }
}

- (IBAction)onBtnPanEq5:(id)sender {
    if (currentMode == MODE_EQ) {
        [self setSendPage:EQ_PAGE reserve2:0];
        switch (currentPage) {
            case PAGE_CH_1_8:
                [self setSendPage:EQ_PAGE reserve2:0];
                [self showGeqPeqDialog:CHANNEL_CH_5];
                break;
            case PAGE_CH_9_16:
                [self setSendPage:EQ_PAGE reserve2:0];
                [self showGeqPeqDialog:CHANNEL_CH_13];
                break;
            default:
                break;
        }
    } else if (currentMode == MODE_PHANTOM_POWER) {
        [self setSendPage:DELAY_48V_PAGE reserve2:0];
        [viewPhantomPower setHidden:NO];
    } else if (currentMode == MODE_DELAY) {
        [self setSendPage:DELAY_48V_PAGE reserve2:0];
        [viewDelay setHidden:NO];
    } else if (currentMode == MODE_ORDER) {
        [self setSendPage:ORDER_PAGE reserve2:0];
        [viewOrder setHidden:NO];
    } else if (currentMode == MODE_INV) {
        switch (currentPage) {
            case PAGE_CH_1_8:
                invOffOn[CH_5] = !invOffOn[CH_5];
                if (invOffOn[CH_5]) {
                    [btnSliderPanEq5 setBackgroundImage:[UIImage imageNamed:@"pan-15.png"] forState:UIControlStateNormal];
                } else {
                    [btnSliderPanEq5 setBackgroundImage:[UIImage imageNamed:@"pan-14.png"] forState:UIControlStateNormal];
                }
                [Global setClearBit:&_ch5Ctrl bitValue:invOffOn[CH_5] bitOrder:12];
                [self sendData:CMD_CH5_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch5Ctrl];
                break;
            case PAGE_CH_9_16:
                invOffOn[CH_13] = !invOffOn[CH_13];
                if (invOffOn[CH_13]) {
                    [btnSliderPanEq5 setBackgroundImage:[UIImage imageNamed:@"pan-15.png"] forState:UIControlStateNormal];
                } else {
                    [btnSliderPanEq5 setBackgroundImage:[UIImage imageNamed:@"pan-14.png"] forState:UIControlStateNormal];
                }
                [Global setClearBit:&_ch13Ctrl bitValue:invOffOn[CH_13] bitOrder:12];
                [self sendData:CMD_CH13_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch13Ctrl];
                break;
            default:
                break;
        }
    } else if (currentMode == MODE_DYN) {
        [self setSendPage:DYN_PAGE reserve2:0];
        int channel = 0;
        switch (currentPage) {
            case PAGE_CH_1_8:
                channel = CHANNEL_CH_5;
                break;
            case PAGE_CH_9_16:
                channel = CHANNEL_CH_13;
                break;
            default:
                break;
        }
        [self showDynDialog:channel];
    }
}

- (IBAction)onBtnPanEq6:(id)sender {
    if (currentMode == MODE_EQ) {
        [self setSendPage:EQ_PAGE reserve2:0];
        switch (currentPage) {
            case PAGE_CH_1_8:
                [self setSendPage:EQ_PAGE reserve2:0];
                [self showGeqPeqDialog:CHANNEL_CH_6];
                break;
            case PAGE_CH_9_16:
                [self setSendPage:EQ_PAGE reserve2:0];
                [self showGeqPeqDialog:CHANNEL_CH_14];
                break;
            default:
                break;
        }
    } else if (currentMode == MODE_PHANTOM_POWER) {
        [self setSendPage:DELAY_48V_PAGE reserve2:0];
        [viewPhantomPower setHidden:NO];
    } else if (currentMode == MODE_DELAY) {
        [self setSendPage:DELAY_48V_PAGE reserve2:0];
        [viewDelay setHidden:NO];
    } else if (currentMode == MODE_ORDER) {
        [self setSendPage:ORDER_PAGE reserve2:0];
        [viewOrder setHidden:NO];
    } else if (currentMode == MODE_INV) {
        switch (currentPage) {
            case PAGE_CH_1_8:
                invOffOn[CH_6] = !invOffOn[CH_6];
                if (invOffOn[CH_6]) {
                    [btnSliderPanEq6 setBackgroundImage:[UIImage imageNamed:@"pan-15.png"] forState:UIControlStateNormal];
                } else {
                    [btnSliderPanEq6 setBackgroundImage:[UIImage imageNamed:@"pan-14.png"] forState:UIControlStateNormal];
                }
                [Global setClearBit:&_ch6Ctrl bitValue:invOffOn[CH_6] bitOrder:12];
                [self sendData:CMD_CH6_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch6Ctrl];
                break;
            case PAGE_CH_9_16:
                invOffOn[CH_14] = !invOffOn[CH_14];
                if (invOffOn[CH_14]) {
                    [btnSliderPanEq6 setBackgroundImage:[UIImage imageNamed:@"pan-15.png"] forState:UIControlStateNormal];
                } else {
                    [btnSliderPanEq6 setBackgroundImage:[UIImage imageNamed:@"pan-14.png"] forState:UIControlStateNormal];
                }
                [Global setClearBit:&_ch14Ctrl bitValue:invOffOn[CH_14] bitOrder:12];
                [self sendData:CMD_CH14_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch14Ctrl];
                break;
            default:
                break;
        }
    } else if (currentMode == MODE_DYN) {
        [self setSendPage:DYN_PAGE reserve2:0];
        int channel = 0;
        switch (currentPage) {
            case PAGE_CH_1_8:
                channel = CHANNEL_CH_6;
                break;
            case PAGE_CH_9_16:
                channel = CHANNEL_CH_14;
                break;
            default:
                break;
        }
        [self showDynDialog:channel];
    }
}

- (IBAction)onBtnPanEq7:(id)sender {
    if (currentMode == MODE_EQ) {
        [self setSendPage:EQ_PAGE reserve2:0];
        switch (currentPage) {
            case PAGE_CH_1_8:
                [self setSendPage:EQ_PAGE reserve2:0];
                [self showGeqPeqDialog:CHANNEL_CH_7];
                break;
            case PAGE_CH_9_16:
                [self setSendPage:EQ_PAGE reserve2:0];
                [self showGeqPeqDialog:CHANNEL_CH_15];
                break;
            default:
                break;
        }
    } else if (currentMode == MODE_PHANTOM_POWER) {
        [self setSendPage:DELAY_48V_PAGE reserve2:0];
        [viewPhantomPower setHidden:NO];
    } else if (currentMode == MODE_DELAY) {
        [self setSendPage:DELAY_48V_PAGE reserve2:0];
        [viewDelay setHidden:NO];
    } else if (currentMode == MODE_ORDER) {
        [self setSendPage:ORDER_PAGE reserve2:0];
        [viewOrder setHidden:NO];
    } else if (currentMode == MODE_INV) {
        switch (currentPage) {
            case PAGE_CH_1_8:
                invOffOn[CH_7] = !invOffOn[CH_7];
                if (invOffOn[CH_7]) {
                    [btnSliderPanEq7 setBackgroundImage:[UIImage imageNamed:@"pan-15.png"] forState:UIControlStateNormal];
                } else {
                    [btnSliderPanEq7 setBackgroundImage:[UIImage imageNamed:@"pan-14.png"] forState:UIControlStateNormal];
                }
                [Global setClearBit:&_ch7Ctrl bitValue:invOffOn[CH_7] bitOrder:12];
                [self sendData:CMD_CH7_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch7Ctrl];
                break;
            case PAGE_CH_9_16:
                invOffOn[CH_15] = !invOffOn[CH_15];
                if (invOffOn[CH_15]) {
                    [btnSliderPanEq7 setBackgroundImage:[UIImage imageNamed:@"pan-15.png"] forState:UIControlStateNormal];
                } else {
                    [btnSliderPanEq7 setBackgroundImage:[UIImage imageNamed:@"pan-14.png"] forState:UIControlStateNormal];
                }
                [Global setClearBit:&_ch15Ctrl bitValue:invOffOn[CH_15] bitOrder:12];
                [self sendData:CMD_CH15_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch15Ctrl];
                break;
            default:
                break;
        }
    } else if (currentMode == MODE_DYN) {
        [self setSendPage:DYN_PAGE reserve2:0];
        int channel = 0;
        switch (currentPage) {
            case PAGE_CH_1_8:
                channel = CHANNEL_CH_7;
                break;
            case PAGE_CH_9_16:
                channel = CHANNEL_CH_15;
                break;
            default:
                break;
        }
        [self showDynDialog:channel];
    }
}

- (IBAction)onBtnPanEq8:(id)sender {
    if (currentMode == MODE_EQ) {
        [self setSendPage:EQ_PAGE reserve2:0];
        switch (currentPage) {
            case PAGE_CH_1_8:
                [self setSendPage:EQ_PAGE reserve2:0];
                [self showGeqPeqDialog:CHANNEL_CH_8];
                break;
            case PAGE_CH_9_16:
                [self setSendPage:EQ_PAGE reserve2:0];
                [self showGeqPeqDialog:CHANNEL_CH_16];
                break;
            default:
                break;
        }
    } else if (currentMode == MODE_PHANTOM_POWER) {
        [self setSendPage:DELAY_48V_PAGE reserve2:0];
        [viewPhantomPower setHidden:NO];
    } else if (currentMode == MODE_DELAY) {
        [self setSendPage:DELAY_48V_PAGE reserve2:0];
        [viewDelay setHidden:NO];
    } else if (currentMode == MODE_ORDER) {
        [self setSendPage:ORDER_PAGE reserve2:0];
        [viewOrder setHidden:NO];
    } else if (currentMode == MODE_INV) {
        switch (currentPage) {
            case PAGE_CH_1_8:
                invOffOn[CH_8] = !invOffOn[CH_8];
                if (invOffOn[CH_8]) {
                    [btnSliderPanEq8 setBackgroundImage:[UIImage imageNamed:@"pan-15.png"] forState:UIControlStateNormal];
                } else {
                    [btnSliderPanEq8 setBackgroundImage:[UIImage imageNamed:@"pan-14.png"] forState:UIControlStateNormal];
                }
                [Global setClearBit:&_ch8Ctrl bitValue:invOffOn[CH_8] bitOrder:12];
                [self sendData:CMD_CH8_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch8Ctrl];
                break;
            case PAGE_CH_9_16:
                invOffOn[CH_16] = !invOffOn[CH_16];
                if (invOffOn[CH_16]) {
                    [btnSliderPanEq8 setBackgroundImage:[UIImage imageNamed:@"pan-15.png"] forState:UIControlStateNormal];
                } else {
                    [btnSliderPanEq8 setBackgroundImage:[UIImage imageNamed:@"pan-14.png"] forState:UIControlStateNormal];
                }
                [Global setClearBit:&_ch16Ctrl bitValue:invOffOn[CH_16] bitOrder:12];
                [self sendData:CMD_CH16_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch16Ctrl];
                break;
            default:
                break;
        }
    } else if (currentMode == MODE_DYN) {
        [self setSendPage:DYN_PAGE reserve2:0];
        int channel = 0;
        switch (currentPage) {
            case PAGE_CH_1_8:
                channel = CHANNEL_CH_8;
                break;
            case PAGE_CH_9_16:
                channel = CHANNEL_CH_16;
                break;
            default:
                break;
        }
        [self showDynDialog:channel];
    }
}

- (IBAction)onBtnPanEqMain:(id)sender {
    if (currentMode == MODE_EQ) {
        [self setSendPage:EQ_PAGE reserve2:2];
        [self showGeqPeqDialog:CHANNEL_MAIN];
    } else if (currentMode == MODE_DELAY) {
        [self setSendPage:DELAY_48V_PAGE reserve2:0];
        [viewDelay setHidden:NO];
    } else if (currentMode == MODE_ORDER) {
        [self setSendPage:ORDER_PAGE reserve2:0];
        [viewOrder setHidden:NO];
    } else if (currentMode == MODE_DYN) {
        [self setSendPage:DYN_PAGE reserve2:0];
        [self showDynDialog:CHANNEL_MAIN];
    }
}

- (IBAction)onBtnModeSelectDown:(id)sender {
    if (currentMode > MODE_PHANTOM_POWER) {
        currentMode--;
    }
    [self refreshModeLabel];
    [self refreshModeButtons];
}

- (IBAction)onBtnModeSelectUp:(id)sender {
    if (currentMode < MODE_ORDER) {
        currentMode++;
    }
    [self refreshModeLabel];
    [self refreshModeButtons];
}

- (IBAction)onBtnSetup:(id)sender {
    viewSetup.hidden = !viewSetup.isHidden;
    if (viewSetup.hidden) {
        [self refreshPage];
        [self enableModeSelector:YES];
        btnSetup.selected = NO;
    } else {
        if (auxSendController != nil && !auxSendController.view.hidden) {
            auxSendController.view.hidden = YES;
        }
        if (groupSendController != nil && !groupSendController.view.hidden) {
            groupSendController.view.hidden = YES;
        }
        [self setSendPage:SETUP_CTRLRM_USB_ASSIGN_PAGE reserve2:0];
        [self enableModeSelector:NO];
        btnSetup.selected = YES;
    }
}

- (IBAction)onBtnSetupPage1:(id)sender {
    btnSetupPage1.selected = YES;
    btnSetupPage2.selected = NO;
    [imgSetupBg setImage:[UIImage imageNamed:@"basemap-24.png"]];
    [viewSetupPage1 setHidden:NO];
    [viewSetupPage2 setHidden:YES];
}

- (IBAction)onBtnSetupPage2:(id)sender {
    btnSetupPage1.selected = NO;
    btnSetupPage2.selected = YES;
    [imgSetupBg setImage:[UIImage imageNamed:@"basemap-25.png"]];
    [viewSetupPage1 setHidden:YES];
    [viewSetupPage2 setHidden:NO];
}

- (IBAction)onBtnSetupRefresh:(id)sender {
    [viewSetup setHidden:YES];
    btnSetup.selected = NO;
    [self enableModeSelector:YES];
    [self restoreSendPage];
}

- (IBAction)onBtnSetupClockEnter:(id)sender {
    if (_clkSrcTracking != CLK_SRC_NONE) {
        // Stop timer
        if (_clkSrcTimer != nil) {
            [_clkSrcTimer invalidate];
            _clkSrcTimer = nil;
        }
        
        switch (_clkSrcTracking) {
            case CLK_SRC_44_1K:
                [Global setClearBit:&_dspSetClkSrc bitValue:0 bitOrder:0];
                [Global setClearBit:&_dspSetClkSrc bitValue:0 bitOrder:1];
                [Global setClearBit:&_dspSetClkSrc bitValue:0 bitOrder:2];
                [btnClkSrc441kHz setBackgroundImage:[UIImage imageNamed:@"button-43.png"] forState:UIControlStateNormal];
                btnClkSrc441kHz.selected = YES;
                btnClkSrc48kHz.selected = NO;
                btnClkSrcUsbAudio.selected = NO;
                [lblUsbMessage setHidden:NO];
                break;
            case CLK_SRC_48K:
                [Global setClearBit:&_dspSetClkSrc bitValue:1 bitOrder:0];
                [Global setClearBit:&_dspSetClkSrc bitValue:0 bitOrder:1];
                [Global setClearBit:&_dspSetClkSrc bitValue:0 bitOrder:2];
                [btnClkSrc48kHz setBackgroundImage:[UIImage imageNamed:@"button-43.png"] forState:UIControlStateNormal];
                btnClkSrc441kHz.selected = NO;
                btnClkSrc48kHz.selected = YES;
                btnClkSrcUsbAudio.selected = NO;
                [lblUsbMessage setHidden:NO];
                break;
            case CLK_SRC_USB:
                [Global setClearBit:&_dspSetClkSrc bitValue:0 bitOrder:0];
                [Global setClearBit:&_dspSetClkSrc bitValue:0 bitOrder:1];
                [Global setClearBit:&_dspSetClkSrc bitValue:1 bitOrder:2];
                [btnClkSrcUsbAudio setBackgroundImage:[UIImage imageNamed:@"button-43.png"] forState:UIControlStateNormal];
                btnClkSrc441kHz.selected = NO;
                btnClkSrc48kHz.selected = NO;
                btnClkSrcUsbAudio.selected = YES;
                [lblUsbMessage setHidden:YES];
                break;
            default:
                break;
        }
        
        [self sendData:CMD_DSP_SET_CLOCK_SOURCE commandType:DSP_WRITE dspId:DSP_6 value:_dspSetClkSrc];
        _clkSrcTracking = CLK_SRC_NONE;
    }
}

- (void)updateClkSrcButton {
    switch (_clkSrcTracking) {
        case CLK_SRC_44_1K:
            btnClkSrc441kHz.selected = !btnClkSrc441kHz.selected;
            break;
        case CLK_SRC_48K:
            btnClkSrc48kHz.selected = !btnClkSrc48kHz.selected;
            break;
        case CLK_SRC_USB:
            btnClkSrcUsbAudio.selected = !btnClkSrcUsbAudio.selected;
            break;
        default:
            break;
    }
}

- (void)startClkSrcFlashing {
    if (_clkSrcTimer != nil) {
        [_clkSrcTimer invalidate];
        _clkSrcTimer = nil;
    }
    _clkSrcTimer = [NSTimer timerWithTimeInterval:CLK_SRC_INTERVAL target:self selector:@selector(updateClkSrcButton) userInfo:nil repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:_clkSrcTimer forMode:NSRunLoopCommonModes];
}

- (IBAction)onBtnSetupClockSource441kHz:(id)sender {
    btnClkSrc441kHz.selected = YES;
    btnClkSrc48kHz.selected = NO;
    btnClkSrcUsbAudio.selected = NO;
    _clkSrcTracking = CLK_SRC_44_1K;
    [btnClkSrc441kHz setBackgroundImage:[UIImage imageNamed:@"button-58.png"] forState:UIControlStateNormal];
    [btnClkSrc48kHz setBackgroundImage:[UIImage imageNamed:@"button-43.png"] forState:UIControlStateNormal];
    [btnClkSrcUsbAudio setBackgroundImage:[UIImage imageNamed:@"button-43.png"] forState:UIControlStateNormal];
    [self startClkSrcFlashing];
}

- (IBAction)onBtnSetupClockSource48kHz:(id)sender {
    btnClkSrc441kHz.selected = NO;
    btnClkSrc48kHz.selected = YES;
    btnClkSrcUsbAudio.selected = NO;
    _clkSrcTracking = CLK_SRC_48K;
    [btnClkSrc441kHz setBackgroundImage:[UIImage imageNamed:@"button-43.png"] forState:UIControlStateNormal];
    [btnClkSrc48kHz setBackgroundImage:[UIImage imageNamed:@"button-58.png"] forState:UIControlStateNormal];
    [btnClkSrcUsbAudio setBackgroundImage:[UIImage imageNamed:@"button-43.png"] forState:UIControlStateNormal];
    [self startClkSrcFlashing];
}

- (IBAction)onBtnSetupClockSourceUsbAudio:(id)sender {
    btnClkSrc441kHz.selected = NO;
    btnClkSrc48kHz.selected = NO;
    btnClkSrcUsbAudio.selected = YES;
    _clkSrcTracking = CLK_SRC_USB;
    [btnClkSrc441kHz setBackgroundImage:[UIImage imageNamed:@"button-43.png"] forState:UIControlStateNormal];
    [btnClkSrc48kHz setBackgroundImage:[UIImage imageNamed:@"button-43.png"] forState:UIControlStateNormal];
    [btnClkSrcUsbAudio setBackgroundImage:[UIImage imageNamed:@"button-58.png"] forState:UIControlStateNormal];
    [self startClkSrcFlashing];
}

- (IBAction)onBtnMeterCh1_8:(id)sender {
    [self resetTopPanelButtons];
    [btnMeterCh1_8 setSelected:YES];
    [btnCh1_8 setSelected:YES];
    currentPage = PAGE_CH_1_8;
    currentPageType = PRIMARY;
    [self enableModeSelector:YES];
    [self refreshPage];
}

- (IBAction)onBtnMeterCh9_16:(id)sender {
    [self resetTopPanelButtons];
    [btnMeterCh9_16 setSelected:YES];
    [btnCh9_16 setSelected:YES];
    currentPage = PAGE_CH_9_16;
    currentPageType = PRIMARY;
    [self enableModeSelector:YES];
    [self refreshPage];
}

- (IBAction)onBtnMeterUsbAudio:(id)sender {
    [self resetTopPanelButtons];
    [btnMeterUsbAudio setSelected:YES];
    [btnUsbAudio setSelected:YES];
    currentPage = PAGE_USB_AUDIO;
    currentPageType = PRIMARY;
    [self enableModeSelector:YES];
    [self refreshPage];
}

- (IBAction)onBtnMeterAux1_4:(id)sender {
    [self resetTopPanelButtons];
    [btnMeterAux1_4 setSelected:YES];
    [btnAux1_4 setSelected:YES];
    currentPage = PAGE_AUX_1_4;
    currentPageType = PRIMARY;
    [self enableModeSelector:YES];
    [self refreshPage];
}

- (IBAction)onBtnMeterGp1_4:(id)sender {
    [self resetTopPanelButtons];
    [btnMeterGp1_4 setSelected:YES];
    [btnGp1_4 setSelected:YES];
    currentPage = PAGE_GP_1_4;
    currentPageType = PRIMARY;
    [self enableModeSelector:YES];
    [self refreshPage];
}

- (IBAction)onBtnMeterEfx1:(id)sender {
    [self resetTopPanelButtons];
    [btnMeterEfx1 setSelected:YES];
    [btnEfx1 setSelected:YES];
    currentPage = PAGE_EFX_1;
    currentPageType = PRIMARY;
    [self refreshPage];
}

- (IBAction)onBtnMeterEfx2:(id)sender {
    [self resetTopPanelButtons];
    [btnMeterEfx2 setSelected:YES];
    [btnEfx2 setSelected:YES];
    currentPage = PAGE_EFX_2;
    currentPageType = PRIMARY;
    [self refreshPage];
}

- (IBAction)onBtnMeterMulti1_4:(id)sender {
    [self resetTopPanelButtons];
    [btnMeterMulti1_4 setSelected:YES];
    [btnMulti1_4 setSelected:YES];
    currentPage = PAGE_MT_1_4;
    currentPageType = PRIMARY;
    [self enableModeSelector:YES];
    [self refreshPage];
}

- (IBAction)onBtnMeterCtrlRm:(id)sender {
    [self resetTopPanelButtons];
    [btnMeterCtrlRm setSelected:YES];
    [btnCtrlRm setSelected:YES];
    currentPage = PAGE_CTRL_RM;
    currentPageType = PRIMARY;
    [self enableModeSelector:YES];
    [self refreshPage];
}

- (IBAction)onBtnMeterMain:(id)sender {
    [self resetTopPanelButtons];
    [btnMeterMain setSelected:YES];
    [btnMain setSelected:YES];
    currentPage = PAGE_MAIN;
    currentPageType = PRIMARY;
    [self enableModeSelector:YES];
    [self refreshPage];
}

- (IBAction)onBtnCh1_8:(id)sender {
    [self resetTopPanelButtons];
    [btnCh1_8 setBackgroundImage:[UIImage imageNamed:@"button-0-1.png"] forState:UIControlStateSelected];
    [btnCh1_8 setSelected:YES];
    currentPage = PAGE_CH_1_8;
    currentPageType = SECONDARY;
    [self refreshModePage];
}

- (IBAction)onBtnCh9_16:(id)sender {
    [self resetTopPanelButtons];
    [btnCh9_16 setBackgroundImage:[UIImage imageNamed:@"button-0-1.png"] forState:UIControlStateSelected];
    [btnCh9_16 setSelected:YES];
    currentPage = PAGE_CH_9_16;
    currentPageType = SECONDARY;
    [self refreshModePage];
}

- (IBAction)onBtnUsbAudio:(id)sender {
    [self resetTopPanelButtons];
    [btnUsbAudio setBackgroundImage:[UIImage imageNamed:@"button-0-1.png"] forState:UIControlStateSelected];
    [btnUsbAudio setSelected:YES];
    currentPage = PAGE_USB_AUDIO;
    currentPageType = SECONDARY;
    [self refreshModePage];
}

- (IBAction)onBtnAux1_4:(id)sender {
    [self resetTopPanelButtons];
    [btnAux1_4 setBackgroundImage:[UIImage imageNamed:@"button-0-1.png"] forState:UIControlStateSelected];
    [btnAux1_4 setSelected:YES];
    currentPage = PAGE_AUX_1_4;
    currentPageType = SECONDARY;
    [self enableModeSelector:NO];
    [self refreshModePage];
}

- (IBAction)onBtnGp1_4:(id)sender {
    [self resetTopPanelButtons];
    [btnGp1_4 setBackgroundImage:[UIImage imageNamed:@"button-0-1.png"] forState:UIControlStateSelected];
    [btnGp1_4 setSelected:YES];
    currentPage = PAGE_GP_1_4;
    currentPageType = SECONDARY;
    [self enableModeSelector:NO];
    [self refreshModePage];
}

- (IBAction)onBtnEfx1:(id)sender {
    [self resetTopPanelButtons];
    [btnEfx1 setBackgroundImage:[UIImage imageNamed:@"button-0-1.png"] forState:UIControlStateSelected];
    [btnEfx1 setSelected:YES];
    currentPage = PAGE_EFX_1;
    currentPageType = SECONDARY;
    [self enableModeSelector:NO];
    [self hideAllModeButtons:YES];
    [self refreshModePage];
}

- (IBAction)onBtnEfx2:(id)sender {
    [self resetTopPanelButtons];
    [btnEfx2 setBackgroundImage:[UIImage imageNamed:@"button-0-1.png"] forState:UIControlStateSelected];
    [btnEfx2 setSelected:YES];
    currentPage = PAGE_EFX_2;
    currentPageType = SECONDARY;
    [self enableModeSelector:NO];
    [self hideAllModeButtons:YES];
    [self refreshModePage];
}

- (IBAction)onBtnMulti1_4:(id)sender {
    [self resetTopPanelButtons];
    [btnMulti1_4 setBackgroundImage:[UIImage imageNamed:@"button-0-1.png"] forState:UIControlStateSelected];
    [btnMulti1_4 setSelected:YES];
    currentPage = PAGE_MT_1_4;
    currentPageType = SECONDARY;
    [self refreshModePage];
}

- (IBAction)onBtnUsbAudioOut:(id)sender {
    [self resetTopPanelButtons];
    [btnUsbAudioOut setBackgroundImage:[UIImage imageNamed:@"button-0-1.png"] forState:UIControlStateSelected];
    [btnUsbAudioOut setSelected:YES];
    currentPage = PAGE_USB_AUDIO_OUT;
    currentPageType = SECONDARY;
    [self enableModeSelector:NO];
    [self refreshModePage];
}

- (IBAction)onBtnCtrlRm:(id)sender {
    [self resetTopPanelButtons];
    [btnCtrlRm setBackgroundImage:[UIImage imageNamed:@"button-0-1.png"] forState:UIControlStateSelected];
    [btnCtrlRm setSelected:YES];
    currentPage = PAGE_CTRL_RM;
    currentPageType = SECONDARY;
    [self refreshModePage];
}

- (IBAction)onBtnMain:(id)sender {
    [self resetTopPanelButtons];
    [btnMain setBackgroundImage:[UIImage imageNamed:@"button-0-1.png"] forState:UIControlStateSelected];
    [btnMain setSelected:YES];
    currentPage = PAGE_MAIN;
    currentPageType = SECONDARY;
    [self refreshModePage];
}

- (IBAction)onBtnSlider1Solo:(id)sender {
    [btnSlider1Solo setSelected:!btnSlider1Solo.selected];
    switch (currentPage) {
        case PAGE_CH_1_8:
            [Global setClearBit:&_chSoloCtrl bitValue:btnSlider1Solo.selected bitOrder:0];
            [self sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_chSoloCtrl];
            break;
        case PAGE_CH_9_16:
            [Global setClearBit:&_chSoloCtrl bitValue:btnSlider1Solo.selected bitOrder:8];
            [self sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_chSoloCtrl];
            break;
        case PAGE_USB_AUDIO:
            [Global setClearBit:&_ch17AudioSetting bitValue:btnSlider1Solo.selected bitOrder:10];
            [self sendData:CMD_CH17_USB_AUDIO_SETTING commandType:DSP_WRITE dspId:DSP_5 value:_ch17AudioSetting];
            break;
        case PAGE_AUX_1_4:
            [Global setClearBit:&_auxGpSoloCtrl bitValue:btnSlider1Solo.selected bitOrder:8];
            [self sendData:CMD_AUXGP_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_auxGpSoloCtrl];
            break;
        case PAGE_GP_1_4:
            [Global setClearBit:&_auxGpSoloCtrl bitValue:btnSlider1Solo.selected bitOrder:0];
            [self sendData:CMD_AUXGP_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_auxGpSoloCtrl];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnSlider1On:(id)sender {
    btnSlider1On.selected = !btnSlider1On.selected;
    switch (currentPage) {
        case PAGE_CH_1_8:
            [Global setClearBit:&_chOffOn bitValue:btnSlider1On.selected bitOrder:0];
            [self sendData:CMD_CH_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_chOffOn];
            break;
        case PAGE_CH_9_16:
            [Global setClearBit:&_chOffOn bitValue:btnSlider1On.selected bitOrder:8];
            [self sendData:CMD_CH_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_chOffOn];
            break;
        case PAGE_USB_AUDIO:
            [Global setClearBit:&_ch17AudioSetting bitValue:btnSlider1On.selected bitOrder:8];
            [self sendData:CMD_CH17_USB_AUDIO_SETTING commandType:DSP_WRITE dspId:DSP_5 value:_ch17AudioSetting];
            break;
        case PAGE_AUX_1_4:
            [Global setClearBit:&_auxGpOffOn bitValue:btnSlider1On.selected bitOrder:8];
            [self sendData:CMD_AUXGP_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_auxGpOffOn];
            break;
        case PAGE_GP_1_4:
            [Global setClearBit:&_auxGpOffOn bitValue:btnSlider1On.selected bitOrder:0];
            [self sendData:CMD_AUXGP_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_auxGpOffOn];
            break;
        case PAGE_MT_1_4:
            [Global setClearBit:&_multi1Ctrl bitValue:btnSlider1On.selected bitOrder:0];
            [self sendData:CMD_MULTI_1_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi1Ctrl];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnSlider1Meter:(id)sender {
    btnSlider1Meter.selected = !btnSlider1Meter.selected;
    switch (currentPage) {
        case PAGE_CH_1_8:
            [Global setClearBit:&_chMeterPrePost bitValue:btnSlider1Meter.selected bitOrder:0];
            [self sendData:CMD_CH_METER commandType:DSP_WRITE dspId:DSP_5 value:_chMeterPrePost];
            break;
        case PAGE_CH_9_16:
            [Global setClearBit:&_chMeterPrePost bitValue:btnSlider1Meter.selected bitOrder:8];
            [self sendData:CMD_CH_METER commandType:DSP_WRITE dspId:DSP_5 value:_chMeterPrePost];
            break;
        case PAGE_USB_AUDIO:
            [Global setClearBit:&_ch17AudioSetting bitValue:btnSlider1Meter.selected bitOrder:15];
            [self sendData:CMD_CH17_USB_AUDIO_SETTING commandType:DSP_WRITE dspId:DSP_5 value:_ch17AudioSetting];
            break;
        case PAGE_AUX_1_4:
            [Global setClearBit:&_auxGpMeterPrePost bitValue:btnSlider1Meter.selected bitOrder:8];
            [self sendData:CMD_AUXGP_METER commandType:DSP_WRITE dspId:DSP_5 value:_auxGpMeterPrePost];
            break;
        case PAGE_GP_1_4:
            [Global setClearBit:&_auxGpMeterPrePost bitValue:btnSlider1Meter.selected bitOrder:0];
            [self sendData:CMD_AUXGP_METER commandType:DSP_WRITE dspId:DSP_5 value:_auxGpMeterPrePost];
            break;
        case PAGE_MT_1_4:
            [Global setClearBit:&_multi14MeterPrePost bitValue:btnSlider1Meter.selected bitOrder:0];
            [self sendData:CMD_MULTI_METER commandType:DSP_WRITE dspId:DSP_7 value:_multi14MeterPrePost];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnSlider2Solo:(id)sender {
    btnSlider2Solo.selected = !btnSlider2Solo.selected;
    switch (currentPage) {
        case PAGE_CH_1_8:
            [Global setClearBit:&_chSoloCtrl bitValue:btnSlider2Solo.selected bitOrder:1];
            [self sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_chSoloCtrl];
            break;
        case PAGE_CH_9_16:
            [Global setClearBit:&_chSoloCtrl bitValue:btnSlider2Solo.selected bitOrder:9];
            [self sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_chSoloCtrl];
            break;
        case PAGE_USB_AUDIO:
            [Global setClearBit:&_ch18AudioSetting bitValue:btnSlider2Solo.selected bitOrder:10];
            [self sendData:CMD_CH18_USB_AUDIO_SETTING commandType:DSP_WRITE dspId:DSP_5 value:_ch18AudioSetting];
            break;
        case PAGE_AUX_1_4:
            [Global setClearBit:&_auxGpSoloCtrl bitValue:btnSlider2Solo.selected bitOrder:9];
            [self sendData:CMD_AUXGP_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_auxGpSoloCtrl];
            break;
        case PAGE_GP_1_4:
            [Global setClearBit:&_auxGpSoloCtrl bitValue:btnSlider2Solo.selected bitOrder:1];
            [self sendData:CMD_AUXGP_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_auxGpSoloCtrl];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnSlider2On:(id)sender {
    btnSlider2On.selected = !btnSlider2On.selected;
    switch (currentPage) {
        case PAGE_CH_1_8:
            [Global setClearBit:&_chOffOn bitValue:btnSlider2On.selected bitOrder:1];
            [self sendData:CMD_CH_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_chOffOn];
            break;
        case PAGE_CH_9_16:
            [Global setClearBit:&_chOffOn bitValue:btnSlider2On.selected bitOrder:9];
            [self sendData:CMD_CH_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_chOffOn];
            break;
        case PAGE_USB_AUDIO:
            [Global setClearBit:&_ch18AudioSetting bitValue:btnSlider2On.selected bitOrder:8];
            [self sendData:CMD_CH18_USB_AUDIO_SETTING commandType:DSP_WRITE dspId:DSP_5 value:_ch18AudioSetting];
            break;
        case PAGE_AUX_1_4:
            [Global setClearBit:&_auxGpOffOn bitValue:btnSlider2On.selected bitOrder:9];
            [self sendData:CMD_AUXGP_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_auxGpOffOn];
            break;
        case PAGE_GP_1_4:
            [Global setClearBit:&_auxGpOffOn bitValue:btnSlider2On.selected bitOrder:1];
            [self sendData:CMD_AUXGP_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_auxGpOffOn];
            break;
        case PAGE_MT_1_4:
            [Global setClearBit:&_multi2Ctrl bitValue:btnSlider2On.selected bitOrder:0];
            [self sendData:CMD_MULTI_2_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi2Ctrl];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnSlider2Meter:(id)sender {
    btnSlider2Meter.selected = !btnSlider2Meter.selected;
    switch (currentPage) {
        case PAGE_CH_1_8:
            [Global setClearBit:&_chMeterPrePost bitValue:btnSlider2Meter.selected bitOrder:1];
            [self sendData:CMD_CH_METER commandType:DSP_WRITE dspId:DSP_5 value:_chMeterPrePost];
            break;
        case PAGE_CH_9_16:
            [Global setClearBit:&_chMeterPrePost bitValue:btnSlider2Meter.selected bitOrder:9];
            [self sendData:CMD_CH_METER commandType:DSP_WRITE dspId:DSP_5 value:_chMeterPrePost];
            break;
        case PAGE_USB_AUDIO:
            [Global setClearBit:&_ch18AudioSetting bitValue:btnSlider2Meter.selected bitOrder:15];
            [self sendData:CMD_CH18_USB_AUDIO_SETTING commandType:DSP_WRITE dspId:DSP_5 value:_ch18AudioSetting];
            break;
        case PAGE_AUX_1_4:
            [Global setClearBit:&_auxGpMeterPrePost bitValue:btnSlider2Meter.selected bitOrder:9];
            [self sendData:CMD_AUXGP_METER commandType:DSP_WRITE dspId:DSP_5 value:_auxGpMeterPrePost];
            break;
        case PAGE_GP_1_4:
            [Global setClearBit:&_auxGpMeterPrePost bitValue:btnSlider2Meter.selected bitOrder:1];
            [self sendData:CMD_AUXGP_METER commandType:DSP_WRITE dspId:DSP_5 value:_auxGpMeterPrePost];
            break;
        case PAGE_MT_1_4:
            [Global setClearBit:&_multi14MeterPrePost bitValue:btnSlider2Meter.selected bitOrder:1];
            [self sendData:CMD_MULTI_METER commandType:DSP_WRITE dspId:DSP_7 value:_multi14MeterPrePost];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnSlider3Solo:(id)sender {
    btnSlider3Solo.selected = !btnSlider3Solo.selected;
    switch (currentPage) {
        case PAGE_CH_1_8:
            [Global setClearBit:&_chSoloCtrl bitValue:btnSlider3Solo.selected bitOrder:2];
            [self sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_chSoloCtrl];
            break;
        case PAGE_CH_9_16:
            [Global setClearBit:&_chSoloCtrl bitValue:btnSlider3Solo.selected bitOrder:10];
            [self sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_chSoloCtrl];
            break;
        case PAGE_AUX_1_4:
            [Global setClearBit:&_auxGpSoloCtrl bitValue:btnSlider3Solo.selected bitOrder:10];
            [self sendData:CMD_AUXGP_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_auxGpSoloCtrl];
            break;
        case PAGE_GP_1_4:
            [Global setClearBit:&_auxGpSoloCtrl bitValue:btnSlider3Solo.selected bitOrder:2];
            [self sendData:CMD_AUXGP_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_auxGpSoloCtrl];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnSlider3On:(id)sender {
    btnSlider3On.selected = !btnSlider3On.selected;
    switch (currentPage) {
        case PAGE_CH_1_8:
            [Global setClearBit:&_chOffOn bitValue:btnSlider3On.selected bitOrder:2];
            [self sendData:CMD_CH_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_chOffOn];
            break;
        case PAGE_CH_9_16:
            [Global setClearBit:&_chOffOn bitValue:btnSlider3On.selected bitOrder:10];
            [self sendData:CMD_CH_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_chOffOn];
            break;
        case PAGE_AUX_1_4:
            [Global setClearBit:&_auxGpOffOn bitValue:btnSlider3On.selected bitOrder:10];
            [self sendData:CMD_AUXGP_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_auxGpOffOn];
            break;
        case PAGE_GP_1_4:
            [Global setClearBit:&_auxGpOffOn bitValue:btnSlider3On.selected bitOrder:2];
            [self sendData:CMD_AUXGP_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_auxGpOffOn];
            break;
        case PAGE_MT_1_4:
            [Global setClearBit:&_multi3Ctrl bitValue:btnSlider3On.selected bitOrder:0];
            [self sendData:CMD_MULTI_3_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi3Ctrl];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnSlider3Meter:(id)sender {
    btnSlider3Meter.selected = !btnSlider3Meter.selected;
    switch (currentPage) {
        case PAGE_CH_1_8:
            [Global setClearBit:&_chMeterPrePost bitValue:btnSlider3Meter.selected bitOrder:2];
            [self sendData:CMD_CH_METER commandType:DSP_WRITE dspId:DSP_5 value:_chMeterPrePost];
            break;
        case PAGE_CH_9_16:
            [Global setClearBit:&_chMeterPrePost bitValue:btnSlider3Meter.selected bitOrder:10];
            [self sendData:CMD_CH_METER commandType:DSP_WRITE dspId:DSP_5 value:_chMeterPrePost];
            break;
        case PAGE_AUX_1_4:
            [Global setClearBit:&_auxGpMeterPrePost bitValue:btnSlider3Meter.selected bitOrder:10];
            [self sendData:CMD_AUXGP_METER commandType:DSP_WRITE dspId:DSP_5 value:_auxGpMeterPrePost];
            break;
        case PAGE_GP_1_4:
            [Global setClearBit:&_auxGpMeterPrePost bitValue:btnSlider3Meter.selected bitOrder:2];
            [self sendData:CMD_AUXGP_METER commandType:DSP_WRITE dspId:DSP_5 value:_auxGpMeterPrePost];
            break;
        case PAGE_MT_1_4:
            [Global setClearBit:&_multi14MeterPrePost bitValue:btnSlider3Meter.selected bitOrder:2];
            [self sendData:CMD_MULTI_METER commandType:DSP_WRITE dspId:DSP_7 value:_multi14MeterPrePost];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnSlider4Solo:(id)sender {
    btnSlider4Solo.selected = !btnSlider4Solo.selected;
    switch (currentPage) {
        case PAGE_CH_1_8:
            [Global setClearBit:&_chSoloCtrl bitValue:btnSlider4Solo.selected bitOrder:3];
            [self sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_chSoloCtrl];
            break;
        case PAGE_CH_9_16:
            [Global setClearBit:&_chSoloCtrl bitValue:btnSlider4Solo.selected bitOrder:11];
            [self sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_chSoloCtrl];
            break;
        case PAGE_AUX_1_4:
            [Global setClearBit:&_auxGpSoloCtrl bitValue:btnSlider4Solo.selected bitOrder:11];
            [self sendData:CMD_AUXGP_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_auxGpSoloCtrl];
            break;
        case PAGE_GP_1_4:
            [Global setClearBit:&_auxGpSoloCtrl bitValue:btnSlider4Solo.selected bitOrder:3];
            [self sendData:CMD_AUXGP_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_auxGpSoloCtrl];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnSlider4On:(id)sender {
    btnSlider4On.selected = !btnSlider4On.selected;
    switch (currentPage) {
        case PAGE_CH_1_8:
            [Global setClearBit:&_chOffOn bitValue:btnSlider4On.selected bitOrder:3];
            [self sendData:CMD_CH_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_chOffOn];
            break;
        case PAGE_CH_9_16:
            [Global setClearBit:&_chOffOn bitValue:btnSlider4On.selected bitOrder:11];
            [self sendData:CMD_CH_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_chOffOn];
            break;
        case PAGE_AUX_1_4:
            [Global setClearBit:&_auxGpOffOn bitValue:btnSlider4On.selected bitOrder:11];
            [self sendData:CMD_AUXGP_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_auxGpOffOn];
            break;
        case PAGE_GP_1_4:
            [Global setClearBit:&_auxGpOffOn bitValue:btnSlider4On.selected bitOrder:3];
            [self sendData:CMD_AUXGP_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_auxGpOffOn];
            break;
        case PAGE_MT_1_4:
            [Global setClearBit:&_multi4Ctrl bitValue:btnSlider4On.selected bitOrder:0];
            [self sendData:CMD_MULTI_4_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi4Ctrl];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnSlider4Meter:(id)sender {
    btnSlider4Meter.selected = !btnSlider4Meter.selected;
    switch (currentPage) {
        case PAGE_CH_1_8:
            [Global setClearBit:&_chMeterPrePost bitValue:btnSlider4Meter.selected bitOrder:3];
            [self sendData:CMD_CH_METER commandType:DSP_WRITE dspId:DSP_5 value:_chMeterPrePost];
            break;
        case PAGE_CH_9_16:
            [Global setClearBit:&_chMeterPrePost bitValue:btnSlider4Meter.selected bitOrder:11];
            [self sendData:CMD_CH_METER commandType:DSP_WRITE dspId:DSP_5 value:_chMeterPrePost];
            break;
        case PAGE_AUX_1_4:
            [Global setClearBit:&_auxGpMeterPrePost bitValue:btnSlider4Meter.selected bitOrder:11];
            [self sendData:CMD_AUXGP_METER commandType:DSP_WRITE dspId:DSP_5 value:_auxGpMeterPrePost];
            break;
        case PAGE_GP_1_4:
            [Global setClearBit:&_auxGpMeterPrePost bitValue:btnSlider4Meter.selected bitOrder:3];
            [self sendData:CMD_AUXGP_METER commandType:DSP_WRITE dspId:DSP_5 value:_auxGpMeterPrePost];
            break;
        case PAGE_MT_1_4:
            [Global setClearBit:&_multi14MeterPrePost bitValue:btnSlider4Meter.selected bitOrder:3];
            [self sendData:CMD_MULTI_METER commandType:DSP_WRITE dspId:DSP_7 value:_multi14MeterPrePost];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnSlider5Solo:(id)sender {
    btnSlider5Solo.selected = !btnSlider5Solo.selected;
    switch (currentPage) {
        case PAGE_CH_1_8:
            [Global setClearBit:&_chSoloCtrl bitValue:btnSlider5Solo.selected bitOrder:4];
            [self sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_chSoloCtrl];
            break;
        case PAGE_CH_9_16:
            [Global setClearBit:&_chSoloCtrl bitValue:btnSlider5Solo.selected bitOrder:12];
            [self sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_chSoloCtrl];
            break;
        case PAGE_AUX_1_4:
            [Global setClearBit:&_auxGpSoloCtrl bitValue:btnSlider5Solo.selected bitOrder:12];
            [self sendData:CMD_AUXGP_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_auxGpSoloCtrl];
            break;
        case PAGE_GP_1_4:
            [Global setClearBit:&_auxGpSoloCtrl bitValue:btnSlider5Solo.selected bitOrder:4];
            [self sendData:CMD_AUXGP_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_auxGpSoloCtrl];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnSlider5On:(id)sender {
    btnSlider5On.selected = !btnSlider5On.selected;
    switch (currentPage) {
        case PAGE_CH_1_8:
            [Global setClearBit:&_chOffOn bitValue:btnSlider5On.selected bitOrder:4];
            [self sendData:CMD_CH_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_chOffOn];
            break;
        case PAGE_CH_9_16:
            [Global setClearBit:&_chOffOn bitValue:btnSlider5On.selected bitOrder:12];
            [self sendData:CMD_CH_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_chOffOn];
            break;
        case PAGE_AUX_1_4:
            [Global setClearBit:&_auxGpOffOn bitValue:btnSlider5On.selected bitOrder:12];
            [self sendData:CMD_AUXGP_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_auxGpOffOn];
            break;
        case PAGE_GP_1_4:
            [Global setClearBit:&_auxGpOffOn bitValue:btnSlider5On.selected bitOrder:4];
            [self sendData:CMD_AUXGP_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_auxGpOffOn];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnSlider5Meter:(id)sender {
    btnSlider5Meter.selected = !btnSlider5Meter.selected;
    switch (currentPage) {
        case PAGE_CH_1_8:
            [Global setClearBit:&_chMeterPrePost bitValue:btnSlider5Meter.selected bitOrder:4];
            [self sendData:CMD_CH_METER commandType:DSP_WRITE dspId:DSP_5 value:_chMeterPrePost];
            break;
        case PAGE_CH_9_16:
            [Global setClearBit:&_chMeterPrePost bitValue:btnSlider5Meter.selected bitOrder:12];
            [self sendData:CMD_CH_METER commandType:DSP_WRITE dspId:DSP_5 value:_chMeterPrePost];
            break;
        case PAGE_AUX_1_4:
            [Global setClearBit:&_auxGpMeterPrePost bitValue:btnSlider5Meter.selected bitOrder:12];
            [self sendData:CMD_AUXGP_METER commandType:DSP_WRITE dspId:DSP_5 value:_auxGpMeterPrePost];
            break;
        case PAGE_GP_1_4:
            [Global setClearBit:&_auxGpMeterPrePost bitValue:btnSlider5Meter.selected bitOrder:4];
            [self sendData:CMD_AUXGP_METER commandType:DSP_WRITE dspId:DSP_5 value:_auxGpMeterPrePost];
            break;
        default:
            break;
    }
}


- (IBAction)onBtnSlider6Solo:(id)sender {
    btnSlider6Solo.selected = !btnSlider6Solo.selected;
    switch (currentPage) {
        case PAGE_CH_1_8:
            [Global setClearBit:&_chSoloCtrl bitValue:btnSlider6Solo.selected bitOrder:5];
            [self sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_chSoloCtrl];
            break;
        case PAGE_CH_9_16:
            [Global setClearBit:&_chSoloCtrl bitValue:btnSlider6Solo.selected bitOrder:13];
            [self sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_chSoloCtrl];
            break;
        case PAGE_AUX_1_4:
            [Global setClearBit:&_auxGpSoloCtrl bitValue:btnSlider6Solo.selected bitOrder:13];
            [self sendData:CMD_AUXGP_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_auxGpSoloCtrl];
            break;
        case PAGE_GP_1_4:
            [Global setClearBit:&_auxGpSoloCtrl bitValue:btnSlider6Solo.selected bitOrder:5];
            [self sendData:CMD_AUXGP_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_auxGpSoloCtrl];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnSlider6On:(id)sender {
    btnSlider6On.selected = !btnSlider6On.selected;
    switch (currentPage) {
        case PAGE_CH_1_8:
            [Global setClearBit:&_chOffOn bitValue:btnSlider6On.selected bitOrder:5];
            [self sendData:CMD_CH_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_chOffOn];
            break;
        case PAGE_CH_9_16:
            [Global setClearBit:&_chOffOn bitValue:btnSlider6On.selected bitOrder:13];
            [self sendData:CMD_CH_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_chOffOn];
            break;
        case PAGE_AUX_1_4:
            [Global setClearBit:&_auxGpOffOn bitValue:btnSlider6On.selected bitOrder:13];
            [self sendData:CMD_AUXGP_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_auxGpOffOn];
            break;
        case PAGE_GP_1_4:
            [Global setClearBit:&_auxGpOffOn bitValue:btnSlider6On.selected bitOrder:5];
            [self sendData:CMD_AUXGP_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_auxGpOffOn];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnSlider6Meter:(id)sender {
    btnSlider6Meter.selected = !btnSlider6Meter.selected;
    switch (currentPage) {
        case PAGE_CH_1_8:
            [Global setClearBit:&_chMeterPrePost bitValue:btnSlider6Meter.selected bitOrder:5];
            [self sendData:CMD_CH_METER commandType:DSP_WRITE dspId:DSP_5 value:_chMeterPrePost];
            break;
        case PAGE_CH_9_16:
            [Global setClearBit:&_chMeterPrePost bitValue:btnSlider6Meter.selected bitOrder:13];
            [self sendData:CMD_CH_METER commandType:DSP_WRITE dspId:DSP_5 value:_chMeterPrePost];
            break;
        case PAGE_AUX_1_4:
            [Global setClearBit:&_auxGpMeterPrePost bitValue:btnSlider6Meter.selected bitOrder:13];
            [self sendData:CMD_AUXGP_METER commandType:DSP_WRITE dspId:DSP_5 value:_auxGpMeterPrePost];
            break;
        case PAGE_GP_1_4:
            [Global setClearBit:&_auxGpMeterPrePost bitValue:btnSlider6Meter.selected bitOrder:5];
            [self sendData:CMD_AUXGP_METER commandType:DSP_WRITE dspId:DSP_5 value:_auxGpMeterPrePost];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnSlider7Solo:(id)sender {
    btnSlider7Solo.selected = !btnSlider7Solo.selected;
    switch (currentPage) {
        case PAGE_CH_1_8:
            [Global setClearBit:&_chSoloCtrl bitValue:btnSlider7Solo.selected bitOrder:6];
            [self sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_chSoloCtrl];
            break;
        case PAGE_CH_9_16:
            [Global setClearBit:&_chSoloCtrl bitValue:btnSlider7Solo.selected bitOrder:14];
            [self sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_chSoloCtrl];
            break;
        case PAGE_AUX_1_4:
            [Global setClearBit:&_auxGpSoloCtrl bitValue:btnSlider7Solo.selected bitOrder:14];
            [self sendData:CMD_AUXGP_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_auxGpSoloCtrl];
            break;
        case PAGE_GP_1_4:
            [Global setClearBit:&_auxGpSoloCtrl bitValue:btnSlider7Solo.selected bitOrder:6];
            [self sendData:CMD_AUXGP_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_auxGpSoloCtrl];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnSlider7On:(id)sender {
    btnSlider7On.selected = !btnSlider7On.selected;
    switch (currentPage) {
        case PAGE_CH_1_8:
            [Global setClearBit:&_chOffOn bitValue:btnSlider7On.selected bitOrder:6];
            [self sendData:CMD_CH_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_chOffOn];
            break;
        case PAGE_CH_9_16:
            [Global setClearBit:&_chOffOn bitValue:btnSlider7On.selected bitOrder:14];
            [self sendData:CMD_CH_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_chOffOn];
            break;
        case PAGE_AUX_1_4:
            [Global setClearBit:&_auxGpOffOn bitValue:btnSlider7On.selected bitOrder:6];
            [self sendData:CMD_AUXGP_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_auxGpOffOn];
            break;
        case PAGE_GP_1_4:
            [Global setClearBit:&_auxGpOffOn bitValue:btnSlider7On.selected bitOrder:14];
            [self sendData:CMD_AUXGP_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_auxGpOffOn];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnSlider7Meter:(id)sender {
    btnSlider7Meter.selected = !btnSlider7Meter.selected;
    switch (currentPage) {
        case PAGE_CH_1_8:
            [Global setClearBit:&_chMeterPrePost bitValue:btnSlider7Meter.selected bitOrder:6];
            [self sendData:CMD_CH_METER commandType:DSP_WRITE dspId:DSP_5 value:_chMeterPrePost];
            break;
        case PAGE_CH_9_16:
            [Global setClearBit:&_chMeterPrePost bitValue:btnSlider7Meter.selected bitOrder:14];
            [self sendData:CMD_CH_METER commandType:DSP_WRITE dspId:DSP_5 value:_chMeterPrePost];
            break;
        case PAGE_AUX_1_4:
            [Global setClearBit:&_auxGpMeterPrePost bitValue:btnSlider7Meter.selected bitOrder:14];
            [self sendData:CMD_AUXGP_METER commandType:DSP_WRITE dspId:DSP_5 value:_auxGpMeterPrePost];
            break;
        case PAGE_GP_1_4:
            [Global setClearBit:&_auxGpMeterPrePost bitValue:btnSlider7Meter.selected bitOrder:6];
            [self sendData:CMD_AUXGP_METER commandType:DSP_WRITE dspId:DSP_5 value:_auxGpMeterPrePost];
            break;
        default:
            break;
    }
}


- (IBAction)onBtnSlider8Solo:(id)sender {
    btnSlider8Solo.selected = !btnSlider8Solo.selected;
    switch (currentPage) {
        case PAGE_CH_1_8:
            [Global setClearBit:&_chSoloCtrl bitValue:btnSlider8Solo.selected bitOrder:7];
            [self sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_chSoloCtrl];
            break;
        case PAGE_CH_9_16:
            [Global setClearBit:&_chSoloCtrl bitValue:btnSlider8Solo.selected bitOrder:15];
            [self sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_chSoloCtrl];
            break;
        case PAGE_AUX_1_4:
            [Global setClearBit:&_auxGpSoloCtrl bitValue:btnSlider8Solo.selected bitOrder:15];
            [self sendData:CMD_AUXGP_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_auxGpSoloCtrl];
            break;
        case PAGE_GP_1_4:
            [Global setClearBit:&_auxGpSoloCtrl bitValue:btnSlider8Solo.selected bitOrder:7];
            [self sendData:CMD_AUXGP_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_auxGpSoloCtrl];
            break;
        case PAGE_CTRL_RM:
        case PAGE_MAIN:
            [Global setClearBit:&_digitalInOutCtrl bitValue:btnSlider8Solo.selected bitOrder:2];
            [self sendData:CMD_DIGITAL_INOUT_CTRL commandType:DSP_WRITE dspId:DSP_6 value:_digitalInOutCtrl];
            break;
            break;
        default:
            break;
    }
}

- (IBAction)onBtnSlider8On:(id)sender {
    btnSlider8On.selected = !btnSlider8On.selected;
    switch (currentPage) {
        case PAGE_CH_1_8:
            [Global setClearBit:&_chOffOn bitValue:btnSlider8On.selected bitOrder:7];
            [self sendData:CMD_CH_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_chOffOn];
            break;
        case PAGE_CH_9_16:
            [Global setClearBit:&_chOffOn bitValue:btnSlider8On.selected bitOrder:15];
            [self sendData:CMD_CH_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_chOffOn];
            break;
        case PAGE_AUX_1_4:
            [Global setClearBit:&_auxGpOffOn bitValue:btnSlider8On.selected bitOrder:15];
            [self sendData:CMD_AUXGP_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_auxGpOffOn];
            break;
        case PAGE_GP_1_4:
            [Global setClearBit:&_auxGpOffOn bitValue:btnSlider8On.selected bitOrder:7];
            [self sendData:CMD_AUXGP_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_auxGpOffOn];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnSlider8Meter:(id)sender {
    btnSlider8Meter.selected = !btnSlider8Meter.selected;
    switch (currentPage) {
        case PAGE_CH_1_8:
            [Global setClearBit:&_chMeterPrePost bitValue:btnSlider8Meter.selected bitOrder:7];
            [self sendData:CMD_CH_METER commandType:DSP_WRITE dspId:DSP_5 value:_chMeterPrePost];
            break;
        case PAGE_CH_9_16:
            [Global setClearBit:&_chMeterPrePost bitValue:btnSlider8Meter.selected bitOrder:15];
            [self sendData:CMD_CH_METER commandType:DSP_WRITE dspId:DSP_5 value:_chMeterPrePost];
            break;
        case PAGE_AUX_1_4:
            [Global setClearBit:&_auxGpMeterPrePost bitValue:btnSlider8Meter.selected bitOrder:15];
            [self sendData:CMD_AUXGP_METER commandType:DSP_WRITE dspId:DSP_5 value:_auxGpMeterPrePost];
            break;
        case PAGE_GP_1_4:
            [Global setClearBit:&_auxGpMeterPrePost bitValue:btnSlider8Meter.selected bitOrder:7];
            [self sendData:CMD_AUXGP_METER commandType:DSP_WRITE dspId:DSP_5 value:_auxGpMeterPrePost];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnSliderMainMono:(id)sender {
    btnSliderMainMono.selected = !btnSliderMainMono.selected;
    switch (currentPage) {
        case PAGE_EFX_1:
            [Global setClearBit:&_gpToMain bitValue:btnSliderMainMono.selected bitOrder:10];
            [self sendData:CMD_GP_TO_MAIN_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_gpToMain];
            if (effectController != nil) {
                [effectController setEffectSolo:btnSliderMainMono.selected];
            }
            break;
        case PAGE_EFX_2:
            [Global setClearBit:&_gpToMain bitValue:btnSliderMainMono.selected bitOrder:11];
            [self sendData:CMD_GP_TO_MAIN_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_gpToMain];
            if (effectController != nil) {
                [effectController setEffectSolo:btnSliderMainMono.selected];
            }
            break;
        default:
            [Global setClearBit:&_mainMeterPrePost bitValue:btnSliderMainMono.selected bitOrder:7];
            [self sendData:CMD_MAIN_METER commandType:DSP_WRITE dspId:DSP_6 value:_mainMeterPrePost];
            break;
    }
}

- (IBAction)onBtnSliderMainOn:(id)sender {
    btnSliderMainOn.selected = !btnSliderMainOn.selected;
    switch (currentPage) {
        case PAGE_EFX_1:
            [Global setClearBit:&_effectCtrl bitValue:btnSliderMainOn.selected bitOrder:0];
            [self sendData:CMD_EFFECT_CONTROL commandType:DSP_WRITE dspId:DSP_5 value:_effectCtrl];
            if (effectController != nil) {
                [effectController setEffectOnOff:btnSliderMainOn.selected];
            }
            break;
        case PAGE_EFX_2:
            [Global setClearBit:&_effectCtrl bitValue:btnSliderMainOn.selected bitOrder:1];
            [self sendData:CMD_EFFECT_CONTROL commandType:DSP_WRITE dspId:DSP_5 value:_effectCtrl];
            if (effectController != nil) {
                [effectController setEffectOnOff:btnSliderMainOn.selected];
            }
            break;
        default:
            [Global setClearBit:&_mainCtrl bitValue:btnSliderMainOn.selected bitOrder:0];
            [self sendData:CMD_MAIN_LR_CTRL commandType:DSP_WRITE dspId:DSP_6 value:_mainCtrl];
            break;
    }
}

- (IBAction)onBtnSliderMainMeter:(id)sender {
    btnSliderMainMeter.selected = !btnSliderMainMeter.selected;
    [Global setClearBit:&_mainMeterPrePost bitValue:btnSliderMainMeter.selected bitOrder:0];
    [self sendData:CMD_MAIN_METER commandType:DSP_WRITE dspId:DSP_6 value:_mainMeterPrePost];
}

- (IBAction)onBtnSetupDone:(id)sender {
    [viewSetup setHidden:YES];
    [btnSetup setBackgroundImage:[UIImage imageNamed:@"button-8.png"] forState:UIControlStateNormal];
    [self refreshPage];
}

- (IBAction)onSwitchSetupLocate:(id)sender {
    if (switchSetupLocate.on) {
        // Popup login modal dialog
        LoginViewController *vc = [[LoginViewController alloc] initWithNibName:@"LoginView" bundle:nil];
        UIView *view = [vc view];
        [view setBackgroundColor:[UIColor whiteColor]];
        [vc setModalPresentationStyle:UIModalPresentationFormSheet];
        [vc setModalTransitionStyle:UIModalTransitionStyleFlipHorizontal];
        [vc setPreferredContentSize:CGSizeMake(400, 300)];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(connectToDevice:) name:@"ConnectToDevice" object:nil];
        [self presentViewController:vc animated:YES completion:nil];
        _loginViewShown = YES;
    } else {
        [self disconnectNetwork];
    }
}

- (IBAction)onBtnSceneFlashRom:(id)sender {
    [btnSceneFlashRom setSelected:YES];
    [btnSceneUsb setSelected:NO];
}

- (IBAction)onBtnSceneUsb:(id)sender {
    [btnSceneFlashRom setSelected:NO];
    [btnSceneUsb setSelected:YES];
}

- (IBAction)onBtnScenesDone:(id)sender {
    [viewScenes setHidden:YES];
    /*
    // Restore previous page
    switch (_currentSceneMode) {
        case FILE_SCENE_FILE_SCENE:
            [self setSendPage:currentPage reserve2:0];
            break;
        
    }
    */
}

- (IBAction)onBtnScenesSave:(id)sender {
    // Popup alert dialog to input name
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                    message:@"Please enter filename"
                                                   delegate:self
                                          cancelButtonTitle:@"OK" otherButtonTitles:@"Cancel",nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    alert.tag = FILE_SAVE;
    _sceneAlertShown = YES;
    [alert show];
}

- (IBAction)onBtnScenesLoad:(id)sender {
    // Popup alert dialog
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                            message:@"This will load the current preset!\nAre you sure?"
                            delegate:self
                            cancelButtonTitle:@"OK" otherButtonTitles:@"Cancel",nil];
    alert.tag = FILE_LOAD;
    [alert show];
}

- (IBAction)onBtnScenesRename:(id)sender {
    // Popup alert dialog to input name
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                    message:@"Please enter new filename"
                                                   delegate:self
                                          cancelButtonTitle:@"OK" otherButtonTitles:@"Cancel",nil];
    alert.alertViewStyle = UIAlertViewStylePlainTextInput;
    [[alert textFieldAtIndex:0] setText:_selectedSceneFile];
    alert.tag = FILE_RENAME;
    _sceneAlertShown = YES;
    [alert show];
}

- (IBAction)onBtnScenesDelete:(id)sender {
    // Popup alert dialog
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                                                    message:@"The preset will be deleted!\nAre you sure?"
                                                   delegate:self
                                          cancelButtonTitle:@"OK" otherButtonTitles:@"Cancel",nil];
    alert.tag = FILE_DELETE;
    [alert show];
}

- (IBAction)onBtnScenesInit:(id)sender {
    // Popup alert dialog
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil
                            message:@"Reinitialize?"
                            delegate:self
                            cancelButtonTitle:@"OK" otherButtonTitles:@"Cancel",nil];
    alert.tag = FILE_INIT;
    [alert show];
}

#pragma mark -
#pragma mark PAN slider event handler methods

- (IBAction)onSliderPan1Action:(id) sender {
    sliderPan1.value = floorf(sliderPan1.value);
    if (sliderPan1.value >= 0.0 && sliderPan1.value < PAN_CENTER_VALUE) {
        [meterPan1 setHidden:NO];
        [meterPan1 setTransform:CGAffineTransformMake(-1, 0, 0, 1, 0, 0)];
        [meterPan1 setProgress:1.0 - sliderPan1.value/PAN_MAX_VALUE];
    } else if (sliderPan1.value > PAN_CENTER_VALUE && sliderPan1.value <= PAN_MAX_VALUE) {
        [meterPan1 setHidden:NO];
        [meterPan1 setTransform:CGAffineTransformMake(1, 0, 0, 1, 0, 0)];
        [meterPan1 setProgress:sliderPan1.value/PAN_MAX_VALUE];
    } else {
        [meterPan1 setHidden:YES];
        [meterPan1 setProgress:sliderPan1.value/PAN_MAX_VALUE];
        [btnSliderPanEq1 setBackgroundImage:[UIImage imageNamed:@"pan-2.png"] forState:UIControlStateNormal];
    }
    [lblPanValue1 setText:[Global getPanString:sliderPan1.value]];
    
    if (sender != nil) {
        switch (currentPage) {
            case PAGE_CH_1_8:
                [self sendData:CMD_CH1_PAN_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderPan1.value-PAN_CENTER_VALUE];
                break;
            case PAGE_CH_9_16:
                [self sendData:CMD_CH9_PAN_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderPan1.value-PAN_CENTER_VALUE];
                break;
            case PAGE_USB_AUDIO:
                [self sendData:CMD_CH17_PAN_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderPan1.value-PAN_CENTER_VALUE];
                break;
            case PAGE_GP_1_4:
                [self sendData:CMD_GP1_PAN_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderPan1.value-PAN_CENTER_VALUE];
                break;
            default:
                NSLog(@"onSliderPan1Action: Invalid page!");
                break;
        }
    }
}

- (IBAction)onSliderPan1Start:(id) sender {
    [sliderPan1 setThumbImage:[UIImage imageNamed:@"Slider-2.png"] forState:UIControlStateNormal];
}

- (IBAction)onSliderPan1End:(id) sender {
    [sliderPan1 setThumbImage:[UIImage imageNamed:@"Slider-1.png"] forState:UIControlStateNormal];
}

- (IBAction)onSliderPan2Action:(id) sender {
    sliderPan2.value = floorf(sliderPan2.value);
    if (sliderPan2.value >= 0.0 && sliderPan2.value < PAN_CENTER_VALUE) {
        [meterPan2 setHidden:NO];
        [meterPan2 setTransform:CGAffineTransformMake(-1, 0, 0, 1, 0, 0)];
        [meterPan2 setProgress:1.0 - sliderPan2.value/PAN_MAX_VALUE];
    } else if (sliderPan2.value > PAN_CENTER_VALUE && sliderPan2.value <= PAN_MAX_VALUE) {
        [meterPan2 setHidden:NO];
        [meterPan2 setTransform:CGAffineTransformMake(1, 0, 0, 1, 0, 0)];
        [meterPan2 setProgress:sliderPan2.value/PAN_MAX_VALUE];
    } else {
        [meterPan2 setHidden:YES];
        [meterPan2 setProgress:sliderPan2.value/PAN_MAX_VALUE];
        [btnSliderPanEq2 setBackgroundImage:[UIImage imageNamed:@"pan-2.png"] forState:UIControlStateNormal];
    }
    [lblPanValue2 setText:[Global getPanString:sliderPan2.value]];
    
    if (sender != nil) {
        switch (currentPage) {
            case PAGE_CH_1_8:
                [self sendData:CMD_CH2_PAN_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderPan2.value-PAN_CENTER_VALUE];
                break;
            case PAGE_CH_9_16:
                [self sendData:CMD_CH10_PAN_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderPan2.value-PAN_CENTER_VALUE];
                break;
            case PAGE_USB_AUDIO:
                [self sendData:CMD_CH18_PAN_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderPan2.value-PAN_CENTER_VALUE];
                break;
            case PAGE_GP_1_4:
                [self sendData:CMD_GP2_PAN_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderPan2.value-PAN_CENTER_VALUE];
                break;
            default:
                NSLog(@"onSliderPan2Action: Invalid page!");
                break;
        }
    }
}

- (IBAction)onSliderPan2Start:(id) sender {
    [sliderPan2 setThumbImage:[UIImage imageNamed:@"Slider-2.png"] forState:UIControlStateNormal];
}

- (IBAction)onSliderPan2End:(id) sender {
    [sliderPan2 setThumbImage:[UIImage imageNamed:@"Slider-1.png"] forState:UIControlStateNormal];
}

- (IBAction)onSliderPan3Action:(id) sender {
    sliderPan3.value = floorf(sliderPan3.value);
    if (sliderPan3.value >= 0.0 && sliderPan3.value < PAN_CENTER_VALUE) {
        [meterPan3 setHidden:NO];
        [meterPan3 setTransform:CGAffineTransformMake(-1, 0, 0, 1, 0, 0)];
        [meterPan3 setProgress:1.0 - sliderPan3.value/PAN_MAX_VALUE];
    } else if (sliderPan3.value > PAN_CENTER_VALUE && sliderPan3.value <= PAN_MAX_VALUE) {
        [meterPan3 setHidden:NO];
        [meterPan3 setTransform:CGAffineTransformMake(1, 0, 0, 1, 0, 0)];
        [meterPan3 setProgress:sliderPan3.value/PAN_MAX_VALUE];
    } else {
        [meterPan3 setHidden:YES];
        [meterPan3 setProgress:sliderPan3.value/PAN_MAX_VALUE];
        [btnSliderPanEq3 setBackgroundImage:[UIImage imageNamed:@"pan-2.png"] forState:UIControlStateNormal];
    }
    [lblPanValue3 setText:[Global getPanString:sliderPan3.value]];
    
    if (sender != nil) {
        switch (currentPage) {
            case PAGE_CH_1_8:
                [self sendData:CMD_CH3_PAN_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderPan3.value-PAN_CENTER_VALUE];
                break;
            case PAGE_CH_9_16:
                [self sendData:CMD_CH11_PAN_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderPan3.value-PAN_CENTER_VALUE];
                break;
            case PAGE_GP_1_4:
                [self sendData:CMD_GP3_PAN_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderPan3.value-PAN_CENTER_VALUE];
                break;
            default:
                NSLog(@"onSliderPan3Action: Invalid page!");
                break;
        }
    }
}

- (IBAction)onSliderPan3Start:(id) sender {
    [sliderPan3 setThumbImage:[UIImage imageNamed:@"Slider-2.png"] forState:UIControlStateNormal];
}

- (IBAction)onSliderPan3End:(id) sender {
    [sliderPan3 setThumbImage:[UIImage imageNamed:@"Slider-1.png"] forState:UIControlStateNormal];
}

- (IBAction)onSliderPan4Action:(id) sender {
    sliderPan4.value = floorf(sliderPan4.value);
    if (sliderPan4.value >= 0.0 && sliderPan4.value < PAN_CENTER_VALUE) {
        [meterPan4 setHidden:NO];
        [meterPan4 setTransform:CGAffineTransformMake(-1, 0, 0, 1, 0, 0)];
        [meterPan4 setProgress:1.0 - sliderPan4.value/PAN_MAX_VALUE];
    } else if (sliderPan4.value > PAN_CENTER_VALUE && sliderPan4.value <= PAN_MAX_VALUE) {
        [meterPan4 setHidden:NO];
        [meterPan4 setTransform:CGAffineTransformMake(1, 0, 0, 1, 0, 0)];
        [meterPan4 setProgress:sliderPan4.value/PAN_MAX_VALUE];
    } else {
        [meterPan4 setHidden:YES];
        [meterPan4 setProgress:sliderPan4.value/PAN_MAX_VALUE];
        [btnSliderPanEq4 setBackgroundImage:[UIImage imageNamed:@"pan-2.png"] forState:UIControlStateNormal];
    }
    [lblPanValue4 setText:[Global getPanString:sliderPan4.value]];
    
    if (sender != nil) {
        switch (currentPage) {
            case PAGE_CH_1_8:
                [self sendData:CMD_CH4_PAN_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderPan4.value-PAN_CENTER_VALUE];
                break;
            case PAGE_CH_9_16:
                [self sendData:CMD_CH12_PAN_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderPan4.value-PAN_CENTER_VALUE];
                break;
            case PAGE_GP_1_4:
                [self sendData:CMD_GP4_PAN_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderPan4.value-PAN_CENTER_VALUE];
                break;
            default:
                NSLog(@"onSliderPan4Action: Invalid page!");
                break;
        }
    }
}

- (IBAction)onSliderPan4Start:(id) sender {
    [sliderPan4 setThumbImage:[UIImage imageNamed:@"Slider-2.png"] forState:UIControlStateNormal];
}

- (IBAction)onSliderPan4End:(id) sender {
    [sliderPan4 setThumbImage:[UIImage imageNamed:@"Slider-1.png"] forState:UIControlStateNormal];
}

- (IBAction)onSliderPan5Action:(id) sender {
    sliderPan5.value = floorf(sliderPan5.value);
    if (sliderPan5.value >= 0.0 && sliderPan5.value < PAN_CENTER_VALUE) {
        [meterPan5 setHidden:NO];
        [meterPan5 setTransform:CGAffineTransformMake(-1, 0, 0, 1, 0, 0)];
        [meterPan5 setProgress:1.0 - sliderPan5.value/PAN_MAX_VALUE];
    } else if (sliderPan5.value > PAN_CENTER_VALUE && sliderPan5.value <= PAN_MAX_VALUE) {
        [meterPan5 setHidden:NO];
        [meterPan5 setTransform:CGAffineTransformMake(1, 0, 0, 1, 0, 0)];
        [meterPan5 setProgress:sliderPan5.value/PAN_MAX_VALUE];
    } else {
        [meterPan5 setHidden:YES];
        [meterPan5 setProgress:sliderPan5.value/PAN_MAX_VALUE];
        [btnSliderPanEq5 setBackgroundImage:[UIImage imageNamed:@"pan-2.png"] forState:UIControlStateNormal];
    }
    [lblPanValue5 setText:[Global getPanString:sliderPan5.value]];
    
    if (sender != nil) {
        switch (currentPage) {
            case PAGE_CH_1_8:
                [self sendData:CMD_CH5_PAN_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderPan5.value-PAN_CENTER_VALUE];
                break;
            case PAGE_CH_9_16:
                [self sendData:CMD_CH13_PAN_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderPan5.value-PAN_CENTER_VALUE];
                break;
            default:
                NSLog(@"onSliderPan5Action: Invalid page!");
                break;
        }
    }
}

- (IBAction)onSliderPan5Start:(id) sender {
    [sliderPan5 setThumbImage:[UIImage imageNamed:@"Slider-2.png"] forState:UIControlStateNormal];
}

- (IBAction)onSliderPan5End:(id) sender {
    [sliderPan5 setThumbImage:[UIImage imageNamed:@"Slider-1.png"] forState:UIControlStateNormal];
}

- (IBAction)onSliderPan6Action:(id) sender {
    sliderPan6.value = floorf(sliderPan6.value);
    if (sliderPan6.value >= 0.0 && sliderPan6.value < PAN_CENTER_VALUE) {
        [meterPan6 setHidden:NO];
        [meterPan6 setTransform:CGAffineTransformMake(-1, 0, 0, 1, 0, 0)];
        [meterPan6 setProgress:1.0 - sliderPan6.value/PAN_MAX_VALUE];
    } else if (sliderPan6.value > PAN_CENTER_VALUE && sliderPan6.value <= PAN_MAX_VALUE) {
        [meterPan6 setHidden:NO];
        [meterPan6 setTransform:CGAffineTransformMake(1, 0, 0, 1, 0, 0)];
        [meterPan6 setProgress:sliderPan6.value/PAN_MAX_VALUE];
    } else {
        [meterPan6 setHidden:YES];
        [meterPan6 setProgress:sliderPan6.value/PAN_MAX_VALUE];
        [btnSliderPanEq6 setBackgroundImage:[UIImage imageNamed:@"pan-2.png"] forState:UIControlStateNormal];
    }
    [lblPanValue6 setText:[Global getPanString:sliderPan6.value]];
    
    if (sender != nil) {
        switch (currentPage) {
            case PAGE_CH_1_8:
                [self sendData:CMD_CH6_PAN_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderPan6.value-PAN_CENTER_VALUE];
                break;
            case PAGE_CH_9_16:
                [self sendData:CMD_CH14_PAN_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderPan6.value-PAN_CENTER_VALUE];
                break;
            default:
                NSLog(@"onSliderPan6Action: Invalid page!");
                break;
        }
    }
}

- (IBAction)onSliderPan6Start:(id) sender {
    [sliderPan6 setThumbImage:[UIImage imageNamed:@"Slider-2.png"] forState:UIControlStateNormal];
}

- (IBAction)onSliderPan6End:(id) sender {
    [sliderPan6 setThumbImage:[UIImage imageNamed:@"Slider-1.png"] forState:UIControlStateNormal];
}

- (IBAction)onSliderPan7Action:(id) sender {
    sliderPan7.value = floorf(sliderPan7.value);
    if (sliderPan7.value >= 0.0 && sliderPan7.value < PAN_CENTER_VALUE) {
        [meterPan7 setHidden:NO];
        [meterPan7 setTransform:CGAffineTransformMake(-1, 0, 0, 1, 0, 0)];
        [meterPan7 setProgress:1.0 - sliderPan7.value/PAN_MAX_VALUE];
    } else if (sliderPan7.value > PAN_CENTER_VALUE && sliderPan7.value <= PAN_MAX_VALUE) {
        [meterPan7 setHidden:NO];
        [meterPan7 setTransform:CGAffineTransformMake(1, 0, 0, 1, 0, 0)];
        [meterPan7 setProgress:sliderPan7.value/PAN_MAX_VALUE];
    } else {
        [meterPan7 setHidden:YES];
        [meterPan7 setProgress:sliderPan7.value/PAN_MAX_VALUE];
        [btnSliderPanEq7 setBackgroundImage:[UIImage imageNamed:@"pan-2.png"] forState:UIControlStateNormal];
    }
    [lblPanValue7 setText:[Global getPanString:sliderPan7.value]];
    
    if (sender != nil) {
        switch (currentPage) {
            case PAGE_CH_1_8:
                [self sendData:CMD_CH7_PAN_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderPan7.value-PAN_CENTER_VALUE];
                break;
            case PAGE_CH_9_16:
                [self sendData:CMD_CH15_PAN_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderPan7.value-PAN_CENTER_VALUE];
                break;
            default:
                NSLog(@"onSliderPan7Action: Invalid page!");
                break;
        }
    }
}

- (IBAction)onSliderPan7Start:(id) sender {
    [sliderPan7 setThumbImage:[UIImage imageNamed:@"Slider-2.png"] forState:UIControlStateNormal];
}

- (IBAction)onSliderPan7End:(id) sender {
    [sliderPan7 setThumbImage:[UIImage imageNamed:@"Slider-1.png"] forState:UIControlStateNormal];
}

- (IBAction)onSliderPan8Action:(id) sender {
    sliderPan8.value = floorf(sliderPan8.value);
    if (sliderPan8.value >= 0.0 && sliderPan8.value < PAN_CENTER_VALUE) {
        [meterPan8 setHidden:NO];
        [meterPan8 setTransform:CGAffineTransformMake(-1, 0, 0, 1, 0, 0)];
        [meterPan8 setProgress:1.0 - sliderPan8.value/PAN_MAX_VALUE];
    } else if (sliderPan8.value > PAN_CENTER_VALUE && sliderPan8.value <= PAN_MAX_VALUE) {
        [meterPan8 setHidden:NO];
        [meterPan8 setTransform:CGAffineTransformMake(1, 0, 0, 1, 0, 0)];
        [meterPan8 setProgress:sliderPan8.value/PAN_MAX_VALUE];
    } else {
        [meterPan8 setHidden:YES];
        [meterPan8 setProgress:sliderPan8.value/PAN_MAX_VALUE];
        [btnSliderPanEq8 setBackgroundImage:[UIImage imageNamed:@"pan-2.png"] forState:UIControlStateNormal];
    }
    [lblPanValue8 setText:[Global getPanString:sliderPan8.value]];
    
    if (sender != nil) {
        switch (currentPage) {
            case PAGE_CH_1_8:
                [self sendData:CMD_CH8_PAN_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderPan8.value-PAN_CENTER_VALUE];
                break;
            case PAGE_CH_9_16:
                [self sendData:CMD_CH16_PAN_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderPan8.value-PAN_CENTER_VALUE];
                break;
            default:
                NSLog(@"onSliderPan8Action: Invalid page!");
                break;
        }
    }
}

- (IBAction)onSliderPan8Start:(id) sender {
    [sliderPan8 setThumbImage:[UIImage imageNamed:@"Slider-2.png"] forState:UIControlStateNormal];
}

- (IBAction)onSliderPan8End:(id) sender {
    [sliderPan8 setThumbImage:[UIImage imageNamed:@"Slider-1.png"] forState:UIControlStateNormal];
}

#pragma mark -
#pragma mark Slider event handler methods

-(IBAction)onSlider1Begin:(id)sender {
    _slider1Lock = YES;
}

-(IBAction)onSlider1End:(id)sender {
    _slider1Lock = NO;
}

-(IBAction)onSlider1Action:(id) sender {
    [lblSlider1Value setText:[Global getSliderGain:slider1.value appendDb:YES]];
    switch (currentPage) {
        case PAGE_CH_1_8:
            [self sendData:CMD_CH1_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:slider1.value];
            break;
        case PAGE_CH_9_16:
            [self sendData:CMD_CH9_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:slider1.value];
            break;
        case PAGE_USB_AUDIO:
            [self sendData:CMD_CH17_USB_AUDIO_FADER commandType:DSP_WRITE dspId:DSP_5 value:slider1.value];
            break;            
        case PAGE_AUX_1_4:
            [self sendData:CMD_AUX1_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:slider1.value];
            break;
        case PAGE_GP_1_4:
            [self sendData:CMD_GP1_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:slider1.value];
            break;
        case PAGE_MT_1_4:
            [self sendData:CMD_MULTI1_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_7 value:slider1.value];
            break;
        default:
            break;
    }
}

-(IBAction)onSlider2Begin:(id)sender {
    _slider2Lock = YES;
}

-(IBAction)onSlider2End:(id)sender {
    _slider2Lock = NO;
}

-(IBAction)onSlider2Action:(id) sender {
    [lblSlider2Value setText:[Global getSliderGain:slider2.value appendDb:YES]];
    switch (currentPage) {
        case PAGE_CH_1_8:
            [self sendData:CMD_CH2_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:slider2.value];
            break;
        case PAGE_CH_9_16:
            [self sendData:CMD_CH10_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:slider2.value];
            break;
        case PAGE_USB_AUDIO:
            [self sendData:CMD_CH18_USB_AUDIO_FADER commandType:DSP_WRITE dspId:DSP_5 value:slider2.value];
            break;
        case PAGE_AUX_1_4:
            [self sendData:CMD_AUX2_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:slider2.value];
            break;
        case PAGE_GP_1_4:
            [self sendData:CMD_GP2_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:slider2.value];
            break;
        case PAGE_MT_1_4:
            [self sendData:CMD_MULTI2_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_7 value:slider2.value];
            break;
        default:
            break;
    }
}

-(IBAction)onSlider3Begin:(id)sender {
    _slider3Lock = YES;
}

-(IBAction)onSlider3End:(id)sender {
    _slider3Lock = NO;
}

-(IBAction)onSlider3Action:(id) sender {
    [lblSlider3Value setText:[Global getSliderGain:slider3.value appendDb:YES]];
    switch (currentPage) {
        case PAGE_CH_1_8:
            [self sendData:CMD_CH3_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:slider3.value];
            break;
        case PAGE_CH_9_16:
            [self sendData:CMD_CH11_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:slider3.value];
            break;
        case PAGE_AUX_1_4:
            [self sendData:CMD_AUX3_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:slider3.value];
            break;
        case PAGE_GP_1_4:
            [self sendData:CMD_GP3_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:slider3.value];
            break;
        case PAGE_MT_1_4:
            [self sendData:CMD_MULTI3_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_7 value:slider3.value];
            break;
        default:
            break;
    }
}

-(IBAction)onSlider4Begin:(id)sender {
    _slider4Lock = YES;
}

-(IBAction)onSlider4End:(id)sender {
    _slider4Lock = NO;
}

-(IBAction)onSlider4Action:(id) sender {
    [lblSlider4Value setText:[Global getSliderGain:slider4.value appendDb:YES]];
    switch (currentPage) {
        case PAGE_CH_1_8:
            [self sendData:CMD_CH4_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:slider4.value];
            break;
        case PAGE_CH_9_16:
            [self sendData:CMD_CH12_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:slider4.value];
            break;
        case PAGE_AUX_1_4:
            [self sendData:CMD_AUX4_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:slider4.value];
            break;
        case PAGE_GP_1_4:
            [self sendData:CMD_GP4_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:slider4.value];
            break;
        case PAGE_MT_1_4:
            [self sendData:CMD_MULTI4_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_7 value:slider4.value];
            break;
        default:
            break;
    }
}

-(IBAction)onSlider5Begin:(id)sender {
    _slider5Lock = YES;
}

-(IBAction)onSlider5End:(id)sender {
    _slider5Lock = NO;
}

-(IBAction)onSlider5Action:(id) sender {
    [lblSlider5Value setText:[Global getSliderGain:slider5.value appendDb:YES]];
    switch (currentPage) {
        case PAGE_CH_1_8:
            [self sendData:CMD_CH5_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:slider5.value];
            break;
        case PAGE_CH_9_16:
            [self sendData:CMD_CH13_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:slider5.value];
            break;
        default:
            break;
    }
}

-(IBAction)onSlider6Begin:(id)sender {
    _slider6Lock = YES;
}

-(IBAction)onSlider6End:(id)sender {
    _slider6Lock = NO;
}

-(IBAction)onSlider6Action:(id) sender {
    [lblSlider6Value setText:[Global getSliderGain:slider6.value appendDb:YES]];
    switch (currentPage) {
        case PAGE_CH_1_8:
            [self sendData:CMD_CH6_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:slider6.value];
            break;
        case PAGE_CH_9_16:
            [self sendData:CMD_CH14_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:slider6.value];
            break;
        default:
            break;
    }
}

-(IBAction)onSlider7Begin:(id)sender {
    _slider7Lock = YES;
}

-(IBAction)onSlider7End:(id)sender {
    _slider7Lock = NO;
}

-(IBAction)onSlider7Action:(id) sender {
    [lblSlider7Value setText:[Global getSliderGain:slider7.value appendDb:YES]];
    switch (currentPage) {
        case PAGE_CH_1_8:
            [self sendData:CMD_CH7_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:slider7.value];
            break;
        case PAGE_CH_9_16:
            [self sendData:CMD_CH15_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:slider7.value];
            break;
        default:
            break;
    }
}

-(IBAction)onSlider8Begin:(id)sender {
    _slider8Lock = YES;
}

-(IBAction)onSlider8End:(id)sender {
    _slider8Lock = NO;
}

-(IBAction)onSlider8Action:(id) sender {
    [lblSlider8Value setText:[Global getSliderGain:slider8.value appendDb:YES]];
    switch (currentPage) {
        case PAGE_CH_1_8:
            [self sendData:CMD_CH8_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:slider8.value];
            break;
        case PAGE_CH_9_16:
            [self sendData:CMD_CH16_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:slider8.value];
            break;
        case PAGE_CTRL_RM:
        case PAGE_MAIN:
            [self sendData:CMD_CTRL_RM_TRIM_LEVEL commandType:DSP_WRITE dspId:DSP_6 value:slider8.value];
            break;
        default:
            break;
    }
}

-(IBAction)onSliderMainBegin:(id)sender {
    _sliderMainLock = YES;
}

-(IBAction)onSliderMainEnd:(id)sender {
    _sliderMainLock = NO;
}

-(IBAction)onSliderMainAction:(id) sender {
    [lblSliderMainValue setText:[Global getSliderGain:sliderMain.value appendDb:YES]];
    switch (currentPage) {
        case PAGE_EFX_1:
            [self sendData:CMD_EFX1_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_8 value:sliderMain.value];
            break;
        case PAGE_EFX_2:
            [self sendData:CMD_EFX2_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderMain.value];
            break;
        default:
            [self sendData:CMD_MAIN_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_6 value:sliderMain.value];
            break;
    }
}

#pragma mark -
#pragma mark Setup page event handler methods

- (IBAction)knobSigGenDidChange:(id)sender {
	lblSigGen.text = [NSString stringWithFormat:@"%d dB", (int)knobSigGen.value - SIG_GEN_OFFSET];
    cpvSigGen.progress = knobSigGen.value/SIG_GEN_MAX_VALUE;
    if (sender) {
        [self sendData:CMD_SG_LEVEL commandType:DSP_WRITE dspId:DSP_8 value:(int)(knobSigGen.value - SIG_GEN_OFFSET)];
    }
}

- (IBAction)onBtnSine100Hz:(id)sender {
    btnSine100Hz.selected = YES;
    btnSine1kHz.selected = NO;
    btnSine10kHz.selected = NO;
    btnPinkNoise.selected = NO;
    [Global setClearBit:&_sgSettingCtrl bitValue:0 bitOrder:0];
    [Global setClearBit:&_sgSettingCtrl bitValue:0 bitOrder:1];
    [Global setClearBit:&_sgSettingCtrl bitValue:0 bitOrder:2];
    [self sendData:CMD_SG_SETTING_CTRL commandType:DSP_WRITE dspId:DSP_8 value:_sgSettingCtrl];
}

- (IBAction)onBtnSine1kHz:(id)sender {
    btnSine100Hz.selected = NO;
    btnSine1kHz.selected = YES;
    btnSine10kHz.selected = NO;
    btnPinkNoise.selected = NO;
    [Global setClearBit:&_sgSettingCtrl bitValue:1 bitOrder:0];
    [Global setClearBit:&_sgSettingCtrl bitValue:0 bitOrder:1];
    [Global setClearBit:&_sgSettingCtrl bitValue:0 bitOrder:2];
    [self sendData:CMD_SG_SETTING_CTRL commandType:DSP_WRITE dspId:DSP_8 value:_sgSettingCtrl];
}

- (IBAction)onBtnSine10kHz:(id)sender {
    btnSine100Hz.selected = NO;
    btnSine1kHz.selected = NO;
    btnSine10kHz.selected = YES;
    btnPinkNoise.selected = NO;
    [Global setClearBit:&_sgSettingCtrl bitValue:0 bitOrder:0];
    [Global setClearBit:&_sgSettingCtrl bitValue:1 bitOrder:1];
    [Global setClearBit:&_sgSettingCtrl bitValue:0 bitOrder:2];
    [self sendData:CMD_SG_SETTING_CTRL commandType:DSP_WRITE dspId:DSP_8 value:_sgSettingCtrl];
}

- (IBAction)onBtnPinkNoise:(id)sender {
    btnSine100Hz.selected = NO;
    btnSine1kHz.selected = NO;
    btnSine10kHz.selected = NO;
    btnPinkNoise.selected = YES;
    [Global setClearBit:&_sgSettingCtrl bitValue:1 bitOrder:0];
    [Global setClearBit:&_sgSettingCtrl bitValue:1 bitOrder:1];
    [Global setClearBit:&_sgSettingCtrl bitValue:0 bitOrder:2];
    [self sendData:CMD_SG_SETTING_CTRL commandType:DSP_WRITE dspId:DSP_8 value:_sgSettingCtrl];
}

- (IBAction)onBtnSigGenAux1:(id)sender {
    btnSigGenAux1.selected = !btnSigGenAux1.selected;
    [Global setClearBit:&_sgAssign bitValue:btnSigGenAux1.selected bitOrder:8];
    [self sendData:CMD_SG_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_sgAssign];
}

- (IBAction)onBtnSigGenAux2:(id)sender {
    btnSigGenAux2.selected = !btnSigGenAux2.selected;
    [Global setClearBit:&_sgAssign bitValue:btnSigGenAux2.selected bitOrder:9];
    [self sendData:CMD_SG_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_sgAssign];
}

- (IBAction)onBtnSigGenAux3:(id)sender {
    btnSigGenAux3.selected = !btnSigGenAux3.selected;
    [Global setClearBit:&_sgAssign bitValue:btnSigGenAux3.selected bitOrder:10];
    [self sendData:CMD_SG_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_sgAssign];
}

- (IBAction)onBtnSigGenAux4:(id)sender {
    btnSigGenAux4.selected = !btnSigGenAux4.selected;
    [Global setClearBit:&_sgAssign bitValue:btnSigGenAux4.selected bitOrder:11];
    [self sendData:CMD_SG_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_sgAssign];
}

- (IBAction)onBtnSigGenGp1:(id)sender {
    btnSigGenGp1.selected = !btnSigGenGp1.selected;
    [Global setClearBit:&_sgAssign bitValue:btnSigGenGp1.selected bitOrder:0];
    [self sendData:CMD_SG_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_sgAssign];
}

- (IBAction)onBtnSigGenGp2:(id)sender {
    btnSigGenGp2.selected = !btnSigGenGp2.selected;
    [Global setClearBit:&_sgAssign bitValue:btnSigGenGp2.selected bitOrder:1];
    [self sendData:CMD_SG_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_sgAssign];
}

- (IBAction)onBtnSigGenGp3:(id)sender {
    btnSigGenGp3.selected = !btnSigGenGp3.selected;
    [Global setClearBit:&_sgAssign bitValue:btnSigGenGp3.selected bitOrder:2];
    [self sendData:CMD_SG_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_sgAssign];
}

- (IBAction)onBtnSigGenGp4:(id)sender {
    btnSigGenGp4.selected = !btnSigGenGp4.selected;
    [Global setClearBit:&_sgAssign bitValue:btnSigGenGp4.selected bitOrder:3];
    [self sendData:CMD_SG_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_sgAssign];
}

- (IBAction)onBtnSigGenMain:(id)sender {
    btnSigGenMain.selected = !btnSigGenMain.selected;
    [Global setClearBit:&_gpToMain bitValue:btnSigGenMain.selected bitOrder:8];
    [self sendData:CMD_GP_TO_MAIN_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_gpToMain];
}

- (IBAction)onBtnSigGenOnOff:(id)sender {
    btnSigGenOnOff.selected = !btnSigGenOnOff.selected;
    [Global setClearBit:&_sgSettingCtrl bitValue:btnSigGenOnOff.selected bitOrder:8];
    [self sendData:CMD_SG_SETTING_CTRL commandType:DSP_WRITE dspId:DSP_8 value:_sgSettingCtrl];
}

- (IBAction)onBtnFirmwareUpdate:(id)sender {
    
}

#pragma mark -
#pragma mark Text field event handler methods

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if (textField == txtSlider1) {
        currentLabel = 1;
    } else if (textField == txtSlider2) {
        currentLabel = 2;
    } else if (textField == txtSlider3) {
        currentLabel = 3;
    } else if (textField == txtSlider4) {
        currentLabel = 4;
    } else if (textField == txtSlider5) {
        currentLabel = 5;
    } else if (textField == txtSlider6) {
        currentLabel = 6;
    } else if (textField == txtSlider7) {
        currentLabel = 7;
    } else if (textField == txtSlider8) {
        currentLabel = 8;
    } else if (textField == txtSliderMain) {
        currentLabel = 9;
    }
    
    [self updateCurrentLabel];
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (textField != txtLabelName)
        return;

    // Check string length
    unsigned long strLen = strlen([textField.text UTF8String]);
    if (strLen > MAX_LABEL_LENGTH) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning"
                                                        message:@"Maximum file name length exceeded.\nPhonic suggests using English letters or numbers when naming files."
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
        return;
    }
    
    [self writeToLabel];
}

/*
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    // Check if string contains Chinese/Japanese characters
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"\\p{Han}|\\p{Bopomofo}|\\p{Katakana}|\\p{Hiragana}" options:NSRegularExpressionCaseInsensitive error:&error];
    NSUInteger numberOfMatchesOrig = [regex numberOfMatchesInString:textField.text options:0 range:NSMakeRange(0, [textField.text length])];
    NSUInteger numberOfMatches = [regex numberOfMatchesInString:string options:0 range:NSMakeRange(0, [string length])];
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    
    //NSLog(@"Matches = %d, New length = %d", numberOfMatches, newLength);
    
    if (numberOfMatchesOrig > 0 || numberOfMatches > 0) {
        return (newLength > MAX_LABEL_LENGTH_CHINESE) ? NO : YES;
    }
    return (newLength > MAX_LABEL_LENGTH) ? NO : YES;
}
*/

#pragma mark -
#pragma mark Keyboard event handler methods

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardDidShow:(NSNotification*)aNotification
{
    // Skip if login view is shown
    if (_loginViewShown || _sceneAlertShown) {
        return;
    }
    
    // Get keyboard size
    CGSize kbSize = [[[aNotification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    CGRect screenRect = [[UIScreen mainScreen] bounds];
    CGFloat screenWidth = screenRect.size.width;
    
    NSLog(@"Keyboard height = %.1f", kbSize.width);
    
    [viewLabelEdit setFrame:CGRectMake(0, screenWidth-kbSize.width-viewLabelEdit.frame.size.height,
                                       viewLabelEdit.frame.size.width, viewLabelEdit.frame.size.height)];
    [viewLabelEdit setHidden:NO];
    [txtLabelName becomeFirstResponder];
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification
{
    [viewLabelEdit setHidden:YES];
    [txtLabelName resignFirstResponder];
}

#pragma mark -
#pragma mark Label edit event handler methods

- (IBAction)onBtnLabelEditDown:(id)sender {
    [self writeToLabel];
    
    if (auxSendController && !auxSendController.view.hidden) {
        if (auxSendController.currentAuxLabel > AUX_1) {
            auxSendController.currentAuxLabel--;
        }
    }
    else if (groupSendController && !groupSendController.view.hidden) {
        if (groupSendController.currentGroupLabel > GROUP_1) {
            groupSendController.currentGroupLabel--;
        }
    }
    else if (eqDynDelayController && !eqDynDelayController.view.hidden) {
        if (eqDynDelayController.currentChannelLabel > CH_1) {
            eqDynDelayController.currentChannelLabel--;
        }
    }
    else if (dynController && !dynController.view.hidden) {
        if (dynController.currentChannelLabel > CH_1) {
            dynController.currentChannelLabel--;
        }
    }
    else if (geqPeqController && !geqPeqController.view.hidden) {
        if (geqPeqController.currentChannelLabel > CHANNEL_CH_1) {
            geqPeqController.currentChannelLabel--;
        }
    }
    else {
        if (currentLabel > 1)
            currentLabel--;
    }
    
    [self updateCurrentLabel];
}

- (IBAction)onBtnLabelEditUp:(id)sender {
    [self writeToLabel];
    
    if (auxSendController && !auxSendController.view.hidden) {
        if (auxSendController.currentAuxLabel < AUX_4) {
            auxSendController.currentAuxLabel++;
        }
    }
    else if (groupSendController && !groupSendController.view.hidden) {
        if (groupSendController.currentGroupLabel < GROUP_4) {
            groupSendController.currentGroupLabel++;
        }
    }
    else if (eqDynDelayController && !eqDynDelayController.view.hidden) {
        if (eqDynDelayController.currentChannelLabel < MAIN) {
            eqDynDelayController.currentChannelLabel++;
        }
    }
    else if (dynController && !dynController.view.hidden) {
        if (dynController.currentChannelLabel < MAIN) {
            dynController.currentChannelLabel++;
        }
    }
    else if (geqPeqController && !geqPeqController.view.hidden) {
        if (geqPeqController.currentChannelLabel < CHANNEL_MAIN) {
            geqPeqController.currentChannelLabel++;
        }
    }
    else {
        if (currentLabel < 9)
            currentLabel++;
    }
    
    [self updateCurrentLabel];
}

- (IBAction)onBtnLabelEditClear:(id)sender {
    [txtLabelName setText:@""];
}

#pragma mark -
#pragma mark Callback from GeqPeqViewController

- (void)didFinishEditing {
    [self refreshPeqButtons];
    //[self refreshPage];
    if (eqDynDelayController != nil) {
        [eqDynDelayController didFinishEditing];
    }
}

#pragma mark -
#pragma mark Callback from DynViewController

- (void)didFinishEditingDyn {
    [self refreshDynButtons];
    //[self refreshPage];
    if (eqDynDelayController != nil) {
        [eqDynDelayController didFinishEditingDyn];
    }
}

#pragma mark -
#pragma mark Table view data source methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _sceneCount;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    cell.textLabel.text = (NSString *)[_sceneFiles objectAtIndex:indexPath.row];
    cell.textLabel.font = [UIFont systemFontOfSize:30.0];
    cell.textLabel.textAlignment = NSTextAlignmentCenter;
    
    if (indexPath.row == _selectedIndexPath.row) {
        [cell setBackgroundColor:[UIColor blueColor]];
        [cell.textLabel setTextColor:[UIColor whiteColor]];
        _selectedSceneFile = cell.textLabel.text;
    } else {
        [cell setBackgroundColor:[UIColor whiteColor]];
        [cell.textLabel setTextColor:[UIColor blackColor]];
    }
    
    return cell;
}


#pragma mark -
#pragma mark - Table view delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60.0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    _selectedSceneFile = (NSString *)[_sceneFiles objectAtIndex:indexPath.row];
    _selectedScene = (int)(indexPath.row + 1);
    _selectedIndexPath = indexPath;
    NSLog(@"Selected file = %@", _selectedSceneFile);
}

#pragma mark -
#pragma mark - Alert view delegate

- (void)closeSceneList {
    [_sceneLoadAlertView dismissWithClickedButtonIndex:0 animated:YES];
    _sceneLoadAlertView = nil;
    [viewScenes setHidden:YES];
}

- (void)updateFilenameTitle:(NSString *)fileName {
    switch (_currentSceneMode) {
        case SCENE_MODE_SCENE:
            [lblSceneName setText:fileName];
            break;
        case SCENE_MODE_DYN_GATE:
        case SCENE_MODE_DYN_EXP:
        case SCENE_MODE_DYN_COMP:
        case SCENE_MODE_DYN_LIM:
            [dynController setSceneFilename:fileName];
            break;
        case SCENE_MODE_EFFECT_1:
        case SCENE_MODE_EFFECT_2:
            [effectController setSceneFilename:fileName];
            break;
        case SCENE_MODE_GEQ:
        case SCENE_MODE_PEQ:
            [geqPeqController setSceneFilename:fileName];
            break;
            break;
        default:
            break;
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    if (alertView.tag == FILE_LOAD) {
        if([title isEqualToString:@"OK"]) {
            // Send selected scene filename to device
            if (_selectedSceneFile == nil) {
                NSLog(@"No scene file selected!");
                return;
            }
            
            // Load preset file
            [self setFileAccess:[btnSceneUsb isSelected] sceneMode:_currentSceneMode channel:_currentSceneChannel accessMode:ACCESS_MODE_LOAD fileName:[_selectedSceneFile UTF8String] newFileName:NULL];
            
            [self updateFilenameTitle:_selectedSceneFile];
            
            _sceneLoadAlertView = [[UIAlertView alloc] initWithTitle:@"Loading preset..."
                                                             message:@"\n"
                                                            delegate:self
                                                   cancelButtonTitle:nil
                                                   otherButtonTitles:nil];
            
            UIActivityIndicatorView *spinner = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
            spinner.center = CGPointMake(139.5, 75.5); // .5 so it doesn't blur
            [_sceneLoadAlertView addSubview:spinner];
            [spinner startAnimating];
            [_sceneLoadAlertView show];
            
            [NSTimer scheduledTimerWithTimeInterval:8.0
                                             target:self
                                           selector:@selector(closeSceneList)
                                           userInfo:nil
                                            repeats:NO];
        }
    } else if (alertView.tag == FILE_SAVE) {
        if([title isEqualToString:@"OK"]) {
            // Save preset file
            NSString *fileName = [alertView textFieldAtIndex:0].text;
            [self setFileAccess:[btnSceneUsb isSelected] sceneMode:_currentSceneMode channel:_currentSceneChannel accessMode:ACCESS_MODE_SAVE fileName:[fileName UTF8String] newFileName:NULL];
            [self updateFilenameTitle:fileName];
            _sceneAlertShown = NO;
            [tableViewScenes reloadData];
        }
    } else if (alertView.tag == FILE_RENAME) {
        if([title isEqualToString:@"OK"]) {
            // Rename preset file
            [self setFileAccess:[btnSceneUsb isSelected] sceneMode:_currentSceneMode channel:_currentSceneChannel accessMode:ACCESS_MODE_RENAME fileName:[_selectedSceneFile UTF8String] newFileName:[[alertView textFieldAtIndex:0].text UTF8String]];
            _sceneAlertShown = NO;
            [tableViewScenes reloadData];
        }
    } else if (alertView.tag == FILE_DELETE) {
        if([title isEqualToString:@"OK"]) {
            // Delete preset file
            [self setFileAccess:[btnSceneUsb isSelected] sceneMode:_currentSceneMode channel:_currentSceneChannel accessMode:ACCESS_MODE_DELETE fileName:[_selectedSceneFile UTF8String] newFileName:NULL];
            [tableViewScenes reloadData];
        }
    } else if (alertView.tag == FILE_INIT) {
        if([title isEqualToString:@"OK"]) {
            // Set init to device
            [self sendData:0 commandType:SET_INIT dspId:0 value:0];
        }
    }
}

#pragma mark -
#pragma mark - Phantom Power view event handlers

- (IBAction)onBtnPhantomPowerReturn:(id)sender {
    [viewPhantomPower setHidden:YES];
    switch (currentPage) {
        case PAGE_CH_1_8:
            [self setSendPage:CHANNEL_PAGE reserve2:CHANNEL_PAGE_CH_1_8];
            break;
        case PAGE_CH_9_16:
            [self setSendPage:CHANNEL_PAGE reserve2:CHANNEL_PAGE_CH_9_16];
            break;
        default:
            break;
    }
    [self refreshModeButtons];
}

- (IBAction)onBtnPhantomPowerCh1OnOff:(id)sender {
    btnPhantomPowerCh1OnOff.selected = !btnPhantomPowerCh1OnOff.selected;
    [Global setClearBit:&_ch48vCtrl bitValue:btnPhantomPowerCh1OnOff.selected bitOrder:0];
    [self sendData:CMD_CH1_16_48V_ONOFF commandType:DSP_WRITE dspId:DSP_6 value:_ch48vCtrl];
}

- (IBAction)onBtnPhantomPowerCh2OnOff:(id)sender {
    btnPhantomPowerCh2OnOff.selected = !btnPhantomPowerCh2OnOff.selected;
    [Global setClearBit:&_ch48vCtrl bitValue:btnPhantomPowerCh2OnOff.selected bitOrder:1];
    [self sendData:CMD_CH1_16_48V_ONOFF commandType:DSP_WRITE dspId:DSP_6 value:_ch48vCtrl];
}

- (IBAction)onBtnPhantomPowerCh3OnOff:(id)sender {
    btnPhantomPowerCh3OnOff.selected = !btnPhantomPowerCh3OnOff.selected;
    [Global setClearBit:&_ch48vCtrl bitValue:btnPhantomPowerCh3OnOff.selected bitOrder:2];
    [self sendData:CMD_CH1_16_48V_ONOFF commandType:DSP_WRITE dspId:DSP_6 value:_ch48vCtrl];
}

- (IBAction)onBtnPhantomPowerCh4OnOff:(id)sender {
    btnPhantomPowerCh4OnOff.selected = !btnPhantomPowerCh4OnOff.selected;
    [Global setClearBit:&_ch48vCtrl bitValue:btnPhantomPowerCh4OnOff.selected bitOrder:3];
    [self sendData:CMD_CH1_16_48V_ONOFF commandType:DSP_WRITE dspId:DSP_6 value:_ch48vCtrl];
}

- (IBAction)onBtnPhantomPowerCh5OnOff:(id)sender {
    btnPhantomPowerCh5OnOff.selected = !btnPhantomPowerCh5OnOff.selected;
    [Global setClearBit:&_ch48vCtrl bitValue:btnPhantomPowerCh5OnOff.selected bitOrder:4];
    [self sendData:CMD_CH1_16_48V_ONOFF commandType:DSP_WRITE dspId:DSP_6 value:_ch48vCtrl];
}

- (IBAction)onBtnPhantomPowerCh6OnOff:(id)sender {
    btnPhantomPowerCh6OnOff.selected = !btnPhantomPowerCh6OnOff.selected;
    [Global setClearBit:&_ch48vCtrl bitValue:btnPhantomPowerCh6OnOff.selected bitOrder:5];
    [self sendData:CMD_CH1_16_48V_ONOFF commandType:DSP_WRITE dspId:DSP_6 value:_ch48vCtrl];
}

- (IBAction)onBtnPhantomPowerCh7OnOff:(id)sender {
    btnPhantomPowerCh7OnOff.selected = !btnPhantomPowerCh7OnOff.selected;
    [Global setClearBit:&_ch48vCtrl bitValue:btnPhantomPowerCh7OnOff.selected bitOrder:6];
    [self sendData:CMD_CH1_16_48V_ONOFF commandType:DSP_WRITE dspId:DSP_6 value:_ch48vCtrl];
}

- (IBAction)onBtnPhantomPowerCh8OnOff:(id)sender {
    btnPhantomPowerCh8OnOff.selected = !btnPhantomPowerCh8OnOff.selected;
    [Global setClearBit:&_ch48vCtrl bitValue:btnPhantomPowerCh8OnOff.selected bitOrder:7];
    [self sendData:CMD_CH1_16_48V_ONOFF commandType:DSP_WRITE dspId:DSP_6 value:_ch48vCtrl];
}

- (IBAction)onBtnPhantomPowerCh9OnOff:(id)sender {
    btnPhantomPowerCh9OnOff.selected = !btnPhantomPowerCh9OnOff.selected;
    [Global setClearBit:&_ch48vCtrl bitValue:btnPhantomPowerCh9OnOff.selected bitOrder:8];
    [self sendData:CMD_CH1_16_48V_ONOFF commandType:DSP_WRITE dspId:DSP_6 value:_ch48vCtrl];
}

- (IBAction)onBtnPhantomPowerCh10OnOff:(id)sender {
    btnPhantomPowerCh10OnOff.selected = !btnPhantomPowerCh10OnOff.selected;
    [Global setClearBit:&_ch48vCtrl bitValue:btnPhantomPowerCh10OnOff.selected bitOrder:9];
    [self sendData:CMD_CH1_16_48V_ONOFF commandType:DSP_WRITE dspId:DSP_6 value:_ch48vCtrl];
}

- (IBAction)onBtnPhantomPowerCh11OnOff:(id)sender {
    btnPhantomPowerCh11OnOff.selected = !btnPhantomPowerCh11OnOff.selected;
    [Global setClearBit:&_ch48vCtrl bitValue:btnPhantomPowerCh11OnOff.selected bitOrder:10];
    [self sendData:CMD_CH1_16_48V_ONOFF commandType:DSP_WRITE dspId:DSP_6 value:_ch48vCtrl];
}

- (IBAction)onBtnPhantomPowerCh12OnOff:(id)sender {
    btnPhantomPowerCh12OnOff.selected = !btnPhantomPowerCh12OnOff.selected;
    [Global setClearBit:&_ch48vCtrl bitValue:btnPhantomPowerCh12OnOff.selected bitOrder:11];
    [self sendData:CMD_CH1_16_48V_ONOFF commandType:DSP_WRITE dspId:DSP_6 value:_ch48vCtrl];
}

- (IBAction)onBtnPhantomPowerCh13OnOff:(id)sender {
    btnPhantomPowerCh13OnOff.selected = !btnPhantomPowerCh13OnOff.selected;
    [Global setClearBit:&_ch48vCtrl bitValue:btnPhantomPowerCh13OnOff.selected bitOrder:12];
    [self sendData:CMD_CH1_16_48V_ONOFF commandType:DSP_WRITE dspId:DSP_6 value:_ch48vCtrl];
}

- (IBAction)onBtnPhantomPowerCh14OnOff:(id)sender {
    btnPhantomPowerCh14OnOff.selected = !btnPhantomPowerCh14OnOff.selected;
    [Global setClearBit:&_ch48vCtrl bitValue:btnPhantomPowerCh14OnOff.selected bitOrder:13];
    [self sendData:CMD_CH1_16_48V_ONOFF commandType:DSP_WRITE dspId:DSP_6 value:_ch48vCtrl];
}

- (IBAction)onBtnPhantomPowerCh15OnOff:(id)sender {
    btnPhantomPowerCh15OnOff.selected = !btnPhantomPowerCh15OnOff.selected;
    [Global setClearBit:&_ch48vCtrl bitValue:btnPhantomPowerCh15OnOff.selected bitOrder:14];
    [self sendData:CMD_CH1_16_48V_ONOFF commandType:DSP_WRITE dspId:DSP_6 value:_ch48vCtrl];
}

- (IBAction)onBtnPhantomPowerCh16OnOff:(id)sender {
    btnPhantomPowerCh16OnOff.selected = !btnPhantomPowerCh16OnOff.selected;
    [Global setClearBit:&_ch48vCtrl bitValue:btnPhantomPowerCh16OnOff.selected bitOrder:15];
    [self sendData:CMD_CH1_16_48V_ONOFF commandType:DSP_WRITE dspId:DSP_6 value:_ch48vCtrl];
}

#pragma mark -
#pragma mark - Delay view event handlers

- (IBAction)onBtnDelayReturn:(id)sender {
    [viewDelay setHidden:YES];
    switch (currentPage) {
        case PAGE_CH_1_8:
            [self setSendPage:CHANNEL_PAGE reserve2:CHANNEL_PAGE_CH_1_8];
            break;
        case PAGE_CH_9_16:
            [self setSendPage:CHANNEL_PAGE reserve2:CHANNEL_PAGE_CH_9_16];
            break;
        case PAGE_USB_AUDIO:
            [self setSendPage:CHANNEL_PAGE reserve2:CHANNEL_PAGE_CH_17_18];
            break;
        case PAGE_MT_1_4:
            [self setSendPage:CHANNEL_PAGE reserve2:CHANNEL_PAGE_MULTI_1_4];
            break;
        case PAGE_CTRL_RM:
        case PAGE_MAIN:
            [self setSendPage:CHANNEL_PAGE reserve2:CHANNEL_PAGE_CTRL];
            break;
        default:
            break;
    }
    [self refreshModeButtons];
}

- (IBAction)onBtnDelayPage1:(id)sender {
    btnDelayPage1.selected = YES;
    btnDelayPage2.selected = NO;
    btnDelayPage3.selected = NO;
    [viewDelayPage1 setHidden:NO];
    [viewDelayPage2 setHidden:YES];
    [viewDelayPage3 setHidden:YES];
    [imgDelayBg setImage:[UIImage imageNamed:@"basemap-29.png"]];
}

- (IBAction)onBtnDelayPage2:(id)sender {
    btnDelayPage1.selected = NO;
    btnDelayPage2.selected = YES;
    btnDelayPage3.selected = NO;
    [viewDelayPage1 setHidden:YES];
    [viewDelayPage2 setHidden:NO];
    [viewDelayPage3 setHidden:YES];
    [imgDelayBg setImage:[UIImage imageNamed:@"basemap-29-1.png"]];
}

- (IBAction)onBtnDelayPage3:(id)sender {
    btnDelayPage1.selected = NO;
    btnDelayPage2.selected = NO;
    btnDelayPage3.selected = YES;
    [viewDelayPage1 setHidden:YES];
    [viewDelayPage2 setHidden:YES];
    [viewDelayPage3 setHidden:NO];
    [imgDelayBg setImage:[UIImage imageNamed:@"basemap-30.png"]];
}

- (IBAction)onBtnDelayCh1OnOff:(id)sender {
    btnDelayCh1OnOff.selected = !btnDelayCh1OnOff.selected;
    [Global setClearBit:&_ch1Ctrl bitValue:btnDelayCh1OnOff.selected bitOrder:2];
    [self sendData:CMD_CH1_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch1Ctrl];
}

- (IBAction)onBtnDelayCh2OnOff:(id)sender {
    btnDelayCh2OnOff.selected = !btnDelayCh2OnOff.selected;
    [Global setClearBit:&_ch2Ctrl bitValue:btnDelayCh2OnOff.selected bitOrder:2];
    [self sendData:CMD_CH2_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch2Ctrl];
}

- (IBAction)onBtnDelayCh3OnOff:(id)sender {
    btnDelayCh3OnOff.selected = !btnDelayCh3OnOff.selected;
    [Global setClearBit:&_ch3Ctrl bitValue:btnDelayCh3OnOff.selected bitOrder:2];
    [self sendData:CMD_CH3_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch3Ctrl];
}

- (IBAction)onBtnDelayCh4OnOff:(id)sender {
    btnDelayCh4OnOff.selected = !btnDelayCh4OnOff.selected;
    [Global setClearBit:&_ch4Ctrl bitValue:btnDelayCh4OnOff.selected bitOrder:2];
    [self sendData:CMD_CH4_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch4Ctrl];
}

- (IBAction)onBtnDelayCh5OnOff:(id)sender {
    btnDelayCh5OnOff.selected = !btnDelayCh5OnOff.selected;
    [Global setClearBit:&_ch5Ctrl bitValue:btnDelayCh5OnOff.selected bitOrder:2];
    [self sendData:CMD_CH5_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch5Ctrl];
}

- (IBAction)onBtnDelayCh6OnOff:(id)sender {
    btnDelayCh6OnOff.selected = !btnDelayCh6OnOff.selected;
    [Global setClearBit:&_ch6Ctrl bitValue:btnDelayCh6OnOff.selected bitOrder:2];
    [self sendData:CMD_CH6_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch6Ctrl];
}

- (IBAction)onBtnDelayCh7OnOff:(id)sender {
    btnDelayCh7OnOff.selected = !btnDelayCh7OnOff.selected;
    [Global setClearBit:&_ch7Ctrl bitValue:btnDelayCh7OnOff.selected bitOrder:2];
    [self sendData:CMD_CH7_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch7Ctrl];
}

- (IBAction)onBtnDelayCh8OnOff:(id)sender {
    btnDelayCh8OnOff.selected = !btnDelayCh8OnOff.selected;
    [Global setClearBit:&_ch8Ctrl bitValue:btnDelayCh8OnOff.selected bitOrder:2];
    [self sendData:CMD_CH8_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch8Ctrl];
}

- (IBAction)onBtnDelayCh9OnOff:(id)sender {
    btnDelayCh9OnOff.selected = !btnDelayCh9OnOff.selected;
    [Global setClearBit:&_ch9Ctrl bitValue:btnDelayCh9OnOff.selected bitOrder:2];
    [self sendData:CMD_CH9_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch9Ctrl];
}

- (IBAction)onBtnDelayCh10OnOff:(id)sender {
    btnDelayCh10OnOff.selected = !btnDelayCh10OnOff.selected;
    [Global setClearBit:&_ch10Ctrl bitValue:btnDelayCh10OnOff.selected bitOrder:2];
    [self sendData:CMD_CH10_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch10Ctrl];
}

- (IBAction)onBtnDelayCh11OnOff:(id)sender {
    btnDelayCh11OnOff.selected = !btnDelayCh11OnOff.selected;
    [Global setClearBit:&_ch11Ctrl bitValue:btnDelayCh11OnOff.selected bitOrder:2];
    [self sendData:CMD_CH11_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch11Ctrl];
}

- (IBAction)onBtnDelayCh12OnOff:(id)sender {
    btnDelayCh12OnOff.selected = !btnDelayCh12OnOff.selected;
    [Global setClearBit:&_ch12Ctrl bitValue:btnDelayCh12OnOff.selected bitOrder:2];
    [self sendData:CMD_CH12_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch12Ctrl];
}

- (IBAction)onBtnDelayCh13OnOff:(id)sender {
    btnDelayCh13OnOff.selected = !btnDelayCh13OnOff.selected;
    [Global setClearBit:&_ch13Ctrl bitValue:btnDelayCh13OnOff.selected bitOrder:2];
    [self sendData:CMD_CH13_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch13Ctrl];
}

- (IBAction)onBtnDelayCh14OnOff:(id)sender {
    btnDelayCh14OnOff.selected = !btnDelayCh14OnOff.selected;
    [Global setClearBit:&_ch14Ctrl bitValue:btnDelayCh14OnOff.selected bitOrder:2];
    [self sendData:CMD_CH14_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch14Ctrl];
}

- (IBAction)onBtnDelayCh15OnOff:(id)sender {
    btnDelayCh15OnOff.selected = !btnDelayCh15OnOff.selected;
    [Global setClearBit:&_ch15Ctrl bitValue:btnDelayCh15OnOff.selected bitOrder:2];
    [self sendData:CMD_CH15_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch15Ctrl];
}

- (IBAction)onBtnDelayCh16OnOff:(id)sender {
    btnDelayCh16OnOff.selected = !btnDelayCh16OnOff.selected;
    [Global setClearBit:&_ch16Ctrl bitValue:btnDelayCh16OnOff.selected bitOrder:2];
    [self sendData:CMD_CH16_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch16Ctrl];
}

- (IBAction)onBtnDelayCh17OnOff:(id)sender {
    btnDelayCh17OnOff.selected = !btnDelayCh17OnOff.selected;
    [Global setClearBit:&_ch17Ctrl bitValue:btnDelayCh17OnOff.selected bitOrder:2];
    [self sendData:CMD_CH17_CTRL commandType:DSP_WRITE dspId:DSP_6 value:_ch17Ctrl];
}

- (IBAction)onBtnDelayCh18OnOff:(id)sender {
    btnDelayCh18OnOff.selected = !btnDelayCh18OnOff.selected;
    [Global setClearBit:&_ch18Ctrl bitValue:btnDelayCh18OnOff.selected bitOrder:2];
    [self sendData:CMD_CH18_CTRL commandType:DSP_WRITE dspId:DSP_6 value:_ch18Ctrl];
}

- (IBAction)onBtnDelayMt1OnOff:(id)sender {
    btnDelayMt1OnOff.selected = !btnDelayMt1OnOff.selected;
    [Global setClearBit:&_multi1Ctrl bitValue:btnDelayMt1OnOff.selected bitOrder:2];
    [self sendData:CMD_MULTI_1_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi1Ctrl];
}

- (IBAction)onBtnDelayMt2OnOff:(id)sender {
    btnDelayMt2OnOff.selected = !btnDelayMt2OnOff.selected;
    [Global setClearBit:&_multi2Ctrl bitValue:btnDelayMt2OnOff.selected bitOrder:2];
    [self sendData:CMD_MULTI_2_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi2Ctrl];
}

- (IBAction)onBtnDelayMt3OnOff:(id)sender {
    btnDelayMt3OnOff.selected = !btnDelayMt3OnOff.selected;
    [Global setClearBit:&_multi3Ctrl bitValue:btnDelayMt3OnOff.selected bitOrder:2];
    [self sendData:CMD_MULTI_3_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi3Ctrl];
}

- (IBAction)onBtnDelayMt4OnOff:(id)sender {
    btnDelayMt4OnOff.selected = !btnDelayMt4OnOff.selected;
    [Global setClearBit:&_multi4Ctrl bitValue:btnDelayMt4OnOff.selected bitOrder:2];
    [self sendData:CMD_MULTI_4_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi4Ctrl];
}

- (IBAction)onBtnDelayMainOnOff:(id)sender {
    btnDelayMainOnOff.selected = !btnDelayMainOnOff.selected;
    [Global setClearBit:&_mainCtrl bitValue:btnDelayMainOnOff.selected bitOrder:2];
    [self sendData:CMD_MAIN_LR_CTRL commandType:DSP_WRITE dspId:DSP_6 value:_mainCtrl];
}

- (IBAction)onBtnDelayTime:(id)sender {
    btnDelayTime.selected = YES;
    btnDelayMeter.selected = NO;
    btnDelayFeet.selected = NO;
    _delayScale = DELAY_TIME;
    [self refreshDelayLabels];
    [self sendData2:SET_DELAY value1:_delayScale value2:knobDelayTemp.value];
}

- (IBAction)onBtnDelayMeter:(id)sender {
    btnDelayTime.selected = NO;
    btnDelayMeter.selected = YES;
    btnDelayFeet.selected = NO;
    _delayScale = DELAY_METER;
    [self refreshDelayLabels];
    [self sendData2:SET_DELAY value1:_delayScale value2:knobDelayTemp.value];
}

- (IBAction)onBtnDelayFeet:(id)sender {
    btnDelayTime.selected = NO;
    btnDelayMeter.selected = NO;
    btnDelayFeet.selected = YES;
    _delayScale = DELAY_FEET;
    [self refreshDelayLabels];
    [self sendData2:SET_DELAY value1:_delayScale value2:knobDelayTemp.value];
}

- (void)refreshDelayLabels {
    [self knobDelayCh1DidChange:nil];
    [self knobDelayCh2DidChange:nil];
    [self knobDelayCh3DidChange:nil];
    [self knobDelayCh4DidChange:nil];
    [self knobDelayCh5DidChange:nil];
    [self knobDelayCh6DidChange:nil];
    [self knobDelayCh7DidChange:nil];
    [self knobDelayCh8DidChange:nil];
    [self knobDelayCh9DidChange:nil];
    [self knobDelayCh10DidChange:nil];
    [self knobDelayCh11DidChange:nil];
    [self knobDelayCh12DidChange:nil];
    [self knobDelayCh13DidChange:nil];
    [self knobDelayCh14DidChange:nil];
    [self knobDelayCh15DidChange:nil];
    [self knobDelayCh16DidChange:nil];
    [self knobDelayCh17DidChange:nil];
    [self knobDelayCh18DidChange:nil];
    [self knobDelayMt1DidChange:nil];
    [self knobDelayMt2DidChange:nil];
    [self knobDelayMt3DidChange:nil];
    [self knobDelayMt4DidChange:nil];
    [self knobDelayMainDidChange:nil];
}

- (IBAction)knobDelayCh1DidChange:(id)sender {
    lblDelayCh1.text = [Global getDelayString:knobDelayCh1.value type:_delayScale temp:knobDelayTemp.value];
    cpvDelayCh1.progress = knobDelayCh1.value/DELAY_MAX_VALUE;
    int delay = [Global knobValueToDelay:knobDelayCh1.value];
    if (sender != nil) {
        [self sendData:CMD_CH1_DELAY_TIME commandType:DSP_WRITE dspId:DSP_1 value:delay];
    }
}

- (IBAction)knobDelayCh2DidChange:(id)sender {
    lblDelayCh2.text = [Global getDelayString:knobDelayCh2.value type:_delayScale  temp:knobDelayTemp.value];
    cpvDelayCh2.progress = knobDelayCh2.value/DELAY_MAX_VALUE;
    int delay = [Global knobValueToDelay:knobDelayCh2.value];
    if (sender != nil) {
        [self sendData:CMD_CH2_DELAY_TIME commandType:DSP_WRITE dspId:DSP_1 value:delay];
    }
}

- (IBAction)knobDelayCh3DidChange:(id)sender {
    lblDelayCh3.text = [Global getDelayString:knobDelayCh3.value type:_delayScale temp:knobDelayTemp.value];
    cpvDelayCh3.progress = knobDelayCh3.value/DELAY_MAX_VALUE;
    int delay = [Global knobValueToDelay:knobDelayCh3.value];
    if (sender != nil) {
        [self sendData:CMD_CH3_DELAY_TIME commandType:DSP_WRITE dspId:DSP_1 value:delay];
    }
}

- (IBAction)knobDelayCh4DidChange:(id)sender {
    lblDelayCh4.text = [Global getDelayString:knobDelayCh4.value type:_delayScale temp:knobDelayTemp.value];
    cpvDelayCh4.progress = knobDelayCh4.value/DELAY_MAX_VALUE;
    int delay = [Global knobValueToDelay:knobDelayCh4.value];
    if (sender != nil) {
        [self sendData:CMD_CH4_DELAY_TIME commandType:DSP_WRITE dspId:DSP_1 value:delay];
    }
}

- (IBAction)knobDelayCh5DidChange:(id)sender {
    lblDelayCh5.text = [Global getDelayString:knobDelayCh5.value type:_delayScale temp:knobDelayTemp.value];
    cpvDelayCh5.progress = knobDelayCh5.value/DELAY_MAX_VALUE;
    int delay = [Global knobValueToDelay:knobDelayCh5.value];
    if (sender != nil) {
        [self sendData:CMD_CH5_DELAY_TIME commandType:DSP_WRITE dspId:DSP_2 value:delay];
    }
}

- (IBAction)knobDelayCh6DidChange:(id)sender {
    lblDelayCh6.text = [Global getDelayString:knobDelayCh6.value type:_delayScale temp:knobDelayTemp.value];
    cpvDelayCh6.progress = knobDelayCh6.value/DELAY_MAX_VALUE;
    int delay = [Global knobValueToDelay:knobDelayCh6.value];
    if (sender != nil) {
        [self sendData:CMD_CH6_DELAY_TIME commandType:DSP_WRITE dspId:DSP_2 value:delay];
    }
}

- (IBAction)knobDelayCh7DidChange:(id)sender {
    lblDelayCh7.text = [Global getDelayString:knobDelayCh7.value type:_delayScale temp:knobDelayTemp.value];
    cpvDelayCh7.progress = knobDelayCh7.value/DELAY_MAX_VALUE;
    int delay = [Global knobValueToDelay:knobDelayCh7.value];
    if (sender != nil) {
        [self sendData:CMD_CH7_DELAY_TIME commandType:DSP_WRITE dspId:DSP_2 value:delay];
    }
}

- (IBAction)knobDelayCh8DidChange:(id)sender {
    lblDelayCh8.text = [Global getDelayString:knobDelayCh8.value type:_delayScale temp:knobDelayTemp.value];
    cpvDelayCh8.progress = knobDelayCh8.value/DELAY_MAX_VALUE;
    int delay = [Global knobValueToDelay:knobDelayCh8.value];
    if (sender != nil) {
        [self sendData:CMD_CH8_DELAY_TIME commandType:DSP_WRITE dspId:DSP_2 value:delay];
    }
}

- (IBAction)knobDelayCh9DidChange:(id)sender {
    lblDelayCh9.text = [Global getDelayString:knobDelayCh9.value type:_delayScale temp:knobDelayTemp.value];
    cpvDelayCh9.progress = knobDelayCh9.value/DELAY_MAX_VALUE;
    int delay = [Global knobValueToDelay:knobDelayCh9.value];
    if (sender != nil) {
        [self sendData:CMD_CH9_DELAY_TIME commandType:DSP_WRITE dspId:DSP_3 value:delay];
    }
}

- (IBAction)knobDelayCh10DidChange:(id)sender {
    lblDelayCh10.text = [Global getDelayString:knobDelayCh10.value type:_delayScale temp:knobDelayTemp.value];
    cpvDelayCh10.progress = knobDelayCh10.value/DELAY_MAX_VALUE;
    int delay = [Global knobValueToDelay:knobDelayCh10.value];
    if (sender != nil) {
        [self sendData:CMD_CH10_DELAY_TIME commandType:DSP_WRITE dspId:DSP_3 value:delay];
    }
}

- (IBAction)knobDelayCh11DidChange:(id)sender {
    lblDelayCh11.text = [Global getDelayString:knobDelayCh11.value type:_delayScale temp:knobDelayTemp.value];
    cpvDelayCh11.progress = knobDelayCh11.value/DELAY_MAX_VALUE;
    int delay = [Global knobValueToDelay:knobDelayCh11.value];
    if (sender != nil) {
        [self sendData:CMD_CH11_DELAY_TIME commandType:DSP_WRITE dspId:DSP_3 value:delay];
    }
}

- (IBAction)knobDelayCh12DidChange:(id)sender {
    lblDelayCh12.text = [Global getDelayString:knobDelayCh12.value type:_delayScale temp:knobDelayTemp.value];
    cpvDelayCh12.progress = knobDelayCh12.value/DELAY_MAX_VALUE;
    int delay = [Global knobValueToDelay:knobDelayCh12.value];
    if (sender != nil) {
        [self sendData:CMD_CH12_DELAY_TIME commandType:DSP_WRITE dspId:DSP_3 value:delay];
    }
}

- (IBAction)knobDelayCh13DidChange:(id)sender {
    lblDelayCh13.text = [Global getDelayString:knobDelayCh13.value type:_delayScale temp:knobDelayTemp.value];
    cpvDelayCh13.progress = knobDelayCh13.value/DELAY_MAX_VALUE;
    int delay = [Global knobValueToDelay:knobDelayCh13.value];
    if (sender != nil) {
        [self sendData:CMD_CH13_DELAY_TIME commandType:DSP_WRITE dspId:DSP_4 value:delay];
    }
}

- (IBAction)knobDelayCh14DidChange:(id)sender {
    lblDelayCh14.text = [Global getDelayString:knobDelayCh14.value type:_delayScale temp:knobDelayTemp.value];
    cpvDelayCh14.progress = knobDelayCh14.value/DELAY_MAX_VALUE;
    int delay = [Global knobValueToDelay:knobDelayCh14.value];
    if (sender != nil) {
        [self sendData:CMD_CH14_DELAY_TIME commandType:DSP_WRITE dspId:DSP_4 value:delay];
    }
}

- (IBAction)knobDelayCh15DidChange:(id)sender {
    lblDelayCh15.text = [Global getDelayString:knobDelayCh15.value type:_delayScale temp:knobDelayTemp.value];
    cpvDelayCh15.progress = knobDelayCh15.value/DELAY_MAX_VALUE;
    int delay = [Global knobValueToDelay:knobDelayCh15.value];
    if (sender != nil) {
        [self sendData:CMD_CH15_DELAY_TIME commandType:DSP_WRITE dspId:DSP_4 value:delay];
    }
}

- (IBAction)knobDelayCh16DidChange:(id)sender {
    lblDelayCh16.text = [Global getDelayString:knobDelayCh16.value type:_delayScale temp:knobDelayTemp.value];
    cpvDelayCh16.progress = knobDelayCh16.value/DELAY_MAX_VALUE;
    int delay = [Global knobValueToDelay:knobDelayCh16.value];
    if (sender != nil) {
        [self sendData:CMD_CH16_DELAY_TIME commandType:DSP_WRITE dspId:DSP_4 value:delay];
    }
}

- (IBAction)knobDelayCh17DidChange:(id)sender {
    lblDelayCh17.text = [Global getDelayString:knobDelayCh17.value type:_delayScale temp:knobDelayTemp.value];
    cpvDelayCh17.progress = knobDelayCh17.value/DELAY_MAX_VALUE;
    int delay = [Global knobValueToDelay:knobDelayCh17.value];
    if (sender != nil) {
        [self sendData:CMD_CH17_DELAY_TIME commandType:DSP_WRITE dspId:DSP_6 value:delay];
    }
}

- (IBAction)knobDelayCh18DidChange:(id)sender {
    lblDelayCh18.text = [Global getDelayString:knobDelayCh18.value type:_delayScale temp:knobDelayTemp.value];
    cpvDelayCh18.progress = knobDelayCh18.value/DELAY_MAX_VALUE;
    int delay = [Global knobValueToDelay:knobDelayCh18.value];
    if (sender != nil) {
        [self sendData:CMD_CH18_DELAY_TIME commandType:DSP_WRITE dspId:DSP_6 value:delay];
    }
}

- (IBAction)knobDelayMt1DidChange:(id)sender {
    lblDelayMt1.text = [Global getDelayString:knobDelayMt1.value type:_delayScale temp:knobDelayTemp.value];
    cpvDelayMt1.progress = knobDelayMt1.value/DELAY_MAX_VALUE;
    int delay = [Global knobValueToDelay:knobDelayMt1.value];
    if (sender != nil) {
        [self sendData:CMD_MULTI1_DELAY_TIME commandType:DSP_WRITE dspId:DSP_7 value:delay];
    }
}

- (IBAction)knobDelayMt2DidChange:(id)sender {
    lblDelayMt2.text = [Global getDelayString:knobDelayMt2.value type:_delayScale temp:knobDelayTemp.value];
    cpvDelayMt2.progress = knobDelayMt2.value/DELAY_MAX_VALUE;
    int delay = [Global knobValueToDelay:knobDelayMt2.value];
    if (sender != nil) {
        [self sendData:CMD_MULTI2_DELAY_TIME commandType:DSP_WRITE dspId:DSP_7 value:delay];
    }
}

- (IBAction)knobDelayMt3DidChange:(id)sender {
    lblDelayMt3.text = [Global getDelayString:knobDelayMt3.value type:_delayScale temp:knobDelayTemp.value];
    cpvDelayMt3.progress = knobDelayMt3.value/DELAY_MAX_VALUE;
    int delay = [Global knobValueToDelay:knobDelayMt3.value];
    if (sender != nil) {
        [self sendData:CMD_MULTI3_DELAY_TIME commandType:DSP_WRITE dspId:DSP_7 value:delay];
    }
}

- (IBAction)knobDelayMt4DidChange:(id)sender {
    lblDelayMt4.text = [Global getDelayString:knobDelayMt4.value type:_delayScale temp:knobDelayTemp.value];
    cpvDelayMt4.progress = knobDelayMt4.value/DELAY_MAX_VALUE;
    int delay = [Global knobValueToDelay:knobDelayMt4.value];
    if (sender != nil) {
        [self sendData:CMD_MULTI4_DELAY_TIME commandType:DSP_WRITE dspId:DSP_7 value:delay];
    }
}

- (IBAction)knobDelayMainDidChange:(id)sender {
    lblDelayMainValue.text = [Global getDelayString:knobDelayMain.value type:_delayScale temp:knobDelayTemp.value];
    cpvDelayMain.progress = knobDelayMain.value/DELAY_MAX_VALUE;
    int delay = [Global knobValueToDelay:knobDelayMain.value];
    if (sender != nil) {
        [self sendData:CMD_MAIN_LR_DELAY_TIME commandType:DSP_WRITE dspId:DSP_6 value:delay];
    }
}

- (IBAction)knobDelayTempDidChange:(id)sender {
    lblDelayTemp.text = [Global getTempString:knobDelayTemp.value];
    cpvDelayTemp.progress = knobDelayTemp.value/TEMP_MAX_VALUE;
    if (sender != nil) {
        [self sendData2:SET_DELAY value1:_delayScale value2:knobDelayTemp.value];
        [self refreshDelayLabels];
    }
}

#pragma mark -
#pragma mark - Order view event handlers

- (IBAction)onBtnOrderReturn:(id)sender {
    [viewOrder setHidden:YES];
    [self refreshModeButtons];
}

- (IBAction)onBtnOrderPage1:(id)sender {
    btnOrderPage1.selected = YES;
    btnOrderPage2.selected = NO;
    btnOrderPage3.selected = NO;
    [viewOrderPage1 setHidden:NO];
    [viewOrderPage2 setHidden:YES];
    [viewOrderPage3 setHidden:YES];
    [imgOrderBg setImage:[UIImage imageNamed:@"basemap-26.png"]];
    [viewOrderPickerCtrl setHidden:YES];
    [viewOrderPickerMain setHidden:YES];
    [viewOrderPickerMulti setHidden:YES];
}

- (IBAction)onBtnOrderPage2:(id)sender {
    btnOrderPage1.selected = NO;
    btnOrderPage2.selected = YES;
    btnOrderPage3.selected = NO;
    [viewOrderPage1 setHidden:YES];
    [viewOrderPage2 setHidden:NO];
    [viewOrderPage3 setHidden:YES];
    [imgOrderBg setImage:[UIImage imageNamed:@"basemap-17-1.png"]];
    [viewOrderPickerCtrl setHidden:YES];
    [viewOrderPickerMain setHidden:YES];
    [viewOrderPickerMulti setHidden:YES];
}

- (IBAction)onBtnOrderPage3:(id)sender {
    btnOrderPage1.selected = NO;
    btnOrderPage2.selected = NO;
    btnOrderPage3.selected = YES;
    [viewOrderPage1 setHidden:YES];
    [viewOrderPage2 setHidden:YES];
    [viewOrderPage3 setHidden:NO];
    [imgOrderBg setImage:[UIImage imageNamed:@"basemap-27.png"]];
    [viewOrderPickerCtrl setHidden:YES];
    [viewOrderPickerMain setHidden:YES];
    [viewOrderPickerMulti setHidden:YES];
}

- (IBAction)onBtnOrderCh1:(id)sender {
    [viewOrderPickerCtrl setFrame:CGRectMake(0.0, 208.0, 224.0, 334.0)];
    [viewOrderPickerCtrl setHidden:NO];
    currentOrderChannel = CH_1;
}

- (IBAction)onBtnOrderCh2:(id)sender {
    [viewOrderPickerCtrl setFrame:CGRectMake(75.0, 208.0, 224.0, 334.0)];
    [viewOrderPickerCtrl setHidden:NO];
    currentOrderChannel = CH_2;
}

- (IBAction)onBtnOrderCh3:(id)sender {
    [viewOrderPickerCtrl setFrame:CGRectMake(175.0, 208.0, 224.0, 334.0)];
    [viewOrderPickerCtrl setHidden:NO];
    currentOrderChannel = CH_3;
}

- (IBAction)onBtnOrderCh4:(id)sender {
    [viewOrderPickerCtrl setFrame:CGRectMake(275.0, 208.0, 224.0, 334.0)];
    [viewOrderPickerCtrl setHidden:NO];
    currentOrderChannel = CH_4;
}

- (IBAction)onBtnOrderCh5:(id)sender {
    [viewOrderPickerCtrl setFrame:CGRectMake(375.0, 208.0, 224.0, 334.0)];
    [viewOrderPickerCtrl setHidden:NO];
    currentOrderChannel = CH_5;
}

- (IBAction)onBtnOrderCh6:(id)sender {
    [viewOrderPickerCtrl setFrame:CGRectMake(475.0, 208.0, 224.0, 334.0)];
    [viewOrderPickerCtrl setHidden:NO];
    currentOrderChannel = CH_6;
}

- (IBAction)onBtnOrderCh7:(id)sender {
    [viewOrderPickerCtrl setFrame:CGRectMake(575.0, 208.0, 224.0, 334.0)];
    [viewOrderPickerCtrl setHidden:NO];
    currentOrderChannel = CH_7;
}

- (IBAction)onBtnOrderCh8:(id)sender {
    [viewOrderPickerCtrl setFrame:CGRectMake(655.0, 208.0, 224.0, 334.0)];
    [viewOrderPickerCtrl setHidden:NO];
    currentOrderChannel = CH_8;
}

- (IBAction)onBtnOrderCh9:(id)sender {
    [viewOrderPickerCtrl setFrame:CGRectMake(0.0, 320.0, 224.0, 334.0)];
    [viewOrderPickerCtrl setHidden:NO];
    currentOrderChannel = CH_9;
}

- (IBAction)onBtnOrderCh10:(id)sender {
    [viewOrderPickerCtrl setFrame:CGRectMake(75.0, 320.0, 224.0, 334.0)];
    [viewOrderPickerCtrl setHidden:NO];
    currentOrderChannel = CH_10;
}

- (IBAction)onBtnOrderCh11:(id)sender {
    [viewOrderPickerCtrl setFrame:CGRectMake(175.0, 320.0, 224.0, 334.0)];
    [viewOrderPickerCtrl setHidden:NO];
    currentOrderChannel = CH_11;
}

- (IBAction)onBtnOrderCh12:(id)sender {
    [viewOrderPickerCtrl setFrame:CGRectMake(275.0, 320.0, 224.0, 334.0)];
    [viewOrderPickerCtrl setHidden:NO];
    currentOrderChannel = CH_12;
}

- (IBAction)onBtnOrderCh13:(id)sender {
    [viewOrderPickerCtrl setFrame:CGRectMake(375.0, 320.0, 224.0, 334.0)];
    [viewOrderPickerCtrl setHidden:NO];
    currentOrderChannel = CH_13;
}

- (IBAction)onBtnOrderCh14:(id)sender {
    [viewOrderPickerCtrl setFrame:CGRectMake(475.0, 320.0, 224.0, 334.0)];
    [viewOrderPickerCtrl setHidden:NO];
    currentOrderChannel = CH_14;
}

- (IBAction)onBtnOrderCh15:(id)sender {
    [viewOrderPickerCtrl setFrame:CGRectMake(575.0, 320.0, 224.0, 334.0)];
    [viewOrderPickerCtrl setHidden:NO];
    currentOrderChannel = CH_15;
}

- (IBAction)onBtnOrderCh16:(id)sender {
    [viewOrderPickerCtrl setFrame:CGRectMake(655.0, 320.0, 224.0, 334.0)];
    [viewOrderPickerCtrl setHidden:NO];
    currentOrderChannel = CH_16;
}

- (IBAction)onBtnOrderCh17:(id)sender {
    [viewOrderPickerCtrl setFrame:CGRectMake(0.0, 208.0, 224.0, 334.0)];
    [viewOrderPickerCtrl setHidden:NO];
    currentOrderChannel = CH_17;
}

- (IBAction)onBtnOrderCh18:(id)sender {
    [viewOrderPickerCtrl setFrame:CGRectMake(75.0, 208.0, 224.0, 334.0)];
    [viewOrderPickerCtrl setHidden:NO];
    currentOrderChannel = CH_18;
}

- (IBAction)onBtnOrderMt1:(id)sender {
    [viewOrderPickerMulti setFrame:CGRectMake(130.0, 210.0, 224.0, 148.0)];
    [viewOrderPickerMulti setHidden:NO];
    [viewOrderPickerMain setHidden:YES];
    currentOrderChannel = MT_1;
}

- (IBAction)onBtnOrderMt2:(id)sender {
    [viewOrderPickerMulti setFrame:CGRectMake(230.0, 210.0, 224.0, 148.0)];
    [viewOrderPickerMulti setHidden:NO];
    [viewOrderPickerMain setHidden:YES];
    currentOrderChannel = MT_2;
}

- (IBAction)onBtnOrderMt3:(id)sender {
    [viewOrderPickerMulti setFrame:CGRectMake(330.0, 210.0, 224.0, 148.0)];
    [viewOrderPickerMulti setHidden:NO];
    [viewOrderPickerMain setHidden:YES];
    currentOrderChannel = MT_3;
}

- (IBAction)onBtnOrderMt4:(id)sender {
    [viewOrderPickerMulti setFrame:CGRectMake(430.0, 210.0, 224.0, 148.0)];
    [viewOrderPickerMulti setHidden:NO];
    [viewOrderPickerMain setHidden:YES];
    currentOrderChannel = MT_4;
}

- (IBAction)onBtnOrderMain:(id)sender {
    [viewOrderPickerMain setFrame:CGRectMake(530.0, 210.0, 224.0, 241.0)];
    [viewOrderPickerMain setHidden:NO];
    [viewOrderPickerMulti setHidden:YES];
    currentOrderChannel = MAIN;
}

- (void)updateOrderLabels:(ChannelNum)channel {
    enum OrderChannel order = orderChannel[channel];
    switch (channel) {
        case CH_1:
            {
                if (order == ORDER_CH_EQ_DYN_DELAY) {
                    [lblOrderCh1First setText:@"EQ"];
                    [lblOrderCh1First setTextColor:[UIColor blackColor]];
                    [lblOrderCh1Second setText:@"DYN"];
                    [lblOrderCh1Second setTextColor:[UIColor blueColor]];
                    [lblOrderCh1Third setText:@"DELAY"];
                    [lblOrderCh1Third setTextColor:[UIColor orangeColor]];
                } else if (order == ORDER_CH_EQ_DELAY_DYN) {
                    [lblOrderCh1First setText:@"EQ"];
                    [lblOrderCh1First setTextColor:[UIColor blackColor]];
                    [lblOrderCh1Second setText:@"DELAY"];
                    [lblOrderCh1Second setTextColor:[UIColor orangeColor]];
                    [lblOrderCh1Third setText:@"DYN"];
                    [lblOrderCh1Third setTextColor:[UIColor blueColor]];
                } else if (order == ORDER_CH_DYN_EQ_DELAY) {
                    [lblOrderCh1First setText:@"DYN"];
                    [lblOrderCh1First setTextColor:[UIColor blueColor]];
                    [lblOrderCh1Second setText:@"EQ"];
                    [lblOrderCh1Second setTextColor:[UIColor blackColor]];
                    [lblOrderCh1Third setText:@"DELAY"];
                    [lblOrderCh1Third setTextColor:[UIColor orangeColor]];
                } else if (order == ORDER_CH_DYN_DELAY_EQ) {
                    [lblOrderCh1First setText:@"DYN"];
                    [lblOrderCh1First setTextColor:[UIColor blueColor]];
                    [lblOrderCh1Second setText:@"DELAY"];
                    [lblOrderCh1Second setTextColor:[UIColor orangeColor]];
                    [lblOrderCh1Third setText:@"EQ"];
                    [lblOrderCh1Third setTextColor:[UIColor blackColor]];
                } else if (order == ORDER_CH_DELAY_DYN_EQ) {
                    [lblOrderCh1First setText:@"DELAY"];
                    [lblOrderCh1First setTextColor:[UIColor orangeColor]];
                    [lblOrderCh1Second setText:@"DYN"];
                    [lblOrderCh1Second setTextColor:[UIColor blueColor]];
                    [lblOrderCh1Third setText:@"EQ"];
                    [lblOrderCh1Third setTextColor:[UIColor blackColor]];
                } else {
                    [lblOrderCh1First setText:@"DELAY"];
                    [lblOrderCh1First setTextColor:[UIColor orangeColor]];
                    [lblOrderCh1Second setText:@"EQ"];
                    [lblOrderCh1Second setTextColor:[UIColor blackColor]];
                    [lblOrderCh1Third setText:@"DYN"];
                    [lblOrderCh1Third setTextColor:[UIColor blueColor]];
                }
                [Global setOrder:&_ch1Ctrl value:order];
                [self sendData:CMD_CH1_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch1Ctrl];
            }
            break;
        case CH_2:
            {
                if (order == ORDER_CH_EQ_DYN_DELAY) {
                    [lblOrderCh2First setText:@"EQ"];
                    [lblOrderCh2First setTextColor:[UIColor blackColor]];
                    [lblOrderCh2Second setText:@"DYN"];
                    [lblOrderCh2Second setTextColor:[UIColor blueColor]];
                    [lblOrderCh2Third setText:@"DELAY"];
                    [lblOrderCh2Third setTextColor:[UIColor orangeColor]];
                } else if (order == ORDER_CH_EQ_DELAY_DYN) {
                    [lblOrderCh2First setText:@"EQ"];
                    [lblOrderCh2First setTextColor:[UIColor blackColor]];
                    [lblOrderCh2Second setText:@"DELAY"];
                    [lblOrderCh2Second setTextColor:[UIColor orangeColor]];
                    [lblOrderCh2Third setText:@"DYN"];
                    [lblOrderCh2Third setTextColor:[UIColor blueColor]];
                } else if (order == ORDER_CH_DYN_EQ_DELAY) {
                    [lblOrderCh2First setText:@"DYN"];
                    [lblOrderCh2First setTextColor:[UIColor blueColor]];
                    [lblOrderCh2Second setText:@"EQ"];
                    [lblOrderCh2Second setTextColor:[UIColor blackColor]];
                    [lblOrderCh2Third setText:@"DELAY"];
                    [lblOrderCh2Third setTextColor:[UIColor orangeColor]];
                } else if (order == ORDER_CH_DYN_DELAY_EQ) {
                    [lblOrderCh2First setText:@"DYN"];
                    [lblOrderCh2First setTextColor:[UIColor blueColor]];
                    [lblOrderCh2Second setText:@"DELAY"];
                    [lblOrderCh2Second setTextColor:[UIColor orangeColor]];
                    [lblOrderCh2Third setText:@"EQ"];
                    [lblOrderCh2Third setTextColor:[UIColor blackColor]];
                } else if (order == ORDER_CH_DELAY_DYN_EQ) {
                    [lblOrderCh2First setText:@"DELAY"];
                    [lblOrderCh2First setTextColor:[UIColor orangeColor]];
                    [lblOrderCh2Second setText:@"DYN"];
                    [lblOrderCh2Second setTextColor:[UIColor blueColor]];
                    [lblOrderCh2Third setText:@"EQ"];
                    [lblOrderCh2Third setTextColor:[UIColor blackColor]];
                } else {
                    [lblOrderCh2First setText:@"DELAY"];
                    [lblOrderCh2First setTextColor:[UIColor orangeColor]];
                    [lblOrderCh2Second setText:@"EQ"];
                    [lblOrderCh2Second setTextColor:[UIColor blackColor]];
                    [lblOrderCh2Third setText:@"DYN"];
                    [lblOrderCh2Third setTextColor:[UIColor blueColor]];
                }
                [Global setOrder:&_ch2Ctrl value:order];
                [self sendData:CMD_CH2_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch2Ctrl];
            }
            break;
        case CH_3:
            {
                if (order == ORDER_CH_EQ_DYN_DELAY) {
                    [lblOrderCh3First setText:@"EQ"];
                    [lblOrderCh3First setTextColor:[UIColor blackColor]];
                    [lblOrderCh3Second setText:@"DYN"];
                    [lblOrderCh3Second setTextColor:[UIColor blueColor]];
                    [lblOrderCh3Third setText:@"DELAY"];
                    [lblOrderCh3Third setTextColor:[UIColor orangeColor]];
                } else if (order == ORDER_CH_EQ_DELAY_DYN) {
                    [lblOrderCh3First setText:@"EQ"];
                    [lblOrderCh3First setTextColor:[UIColor blackColor]];
                    [lblOrderCh3Second setText:@"DELAY"];
                    [lblOrderCh3Second setTextColor:[UIColor orangeColor]];
                    [lblOrderCh3Third setText:@"DYN"];
                    [lblOrderCh3Third setTextColor:[UIColor blueColor]];
                } else if (order == ORDER_CH_DYN_EQ_DELAY) {
                    [lblOrderCh3First setText:@"DYN"];
                    [lblOrderCh3First setTextColor:[UIColor blueColor]];
                    [lblOrderCh3Second setText:@"EQ"];
                    [lblOrderCh3Second setTextColor:[UIColor blackColor]];
                    [lblOrderCh3Third setText:@"DELAY"];
                    [lblOrderCh3Third setTextColor:[UIColor orangeColor]];
                } else if (order == ORDER_CH_DYN_DELAY_EQ) {
                    [lblOrderCh3First setText:@"DYN"];
                    [lblOrderCh3First setTextColor:[UIColor blueColor]];
                    [lblOrderCh3Second setText:@"DELAY"];
                    [lblOrderCh3Second setTextColor:[UIColor orangeColor]];
                    [lblOrderCh3Third setText:@"EQ"];
                    [lblOrderCh3Third setTextColor:[UIColor blackColor]];
                } else if (order == ORDER_CH_DELAY_DYN_EQ) {
                    [lblOrderCh3First setText:@"DELAY"];
                    [lblOrderCh3First setTextColor:[UIColor orangeColor]];
                    [lblOrderCh3Second setText:@"DYN"];
                    [lblOrderCh3Second setTextColor:[UIColor blueColor]];
                    [lblOrderCh3Third setText:@"EQ"];
                    [lblOrderCh3Third setTextColor:[UIColor blackColor]];
                } else {
                    [lblOrderCh3First setText:@"DELAY"];
                    [lblOrderCh3First setTextColor:[UIColor orangeColor]];
                    [lblOrderCh3Second setText:@"EQ"];
                    [lblOrderCh3Second setTextColor:[UIColor blackColor]];
                    [lblOrderCh3Third setText:@"DYN"];
                    [lblOrderCh3Third setTextColor:[UIColor blueColor]];
                }
                [Global setOrder:&_ch3Ctrl value:order];
                [self sendData:CMD_CH3_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch3Ctrl];
            }
            break;
        case CH_4:
            {
                if (order == ORDER_CH_EQ_DYN_DELAY) {
                    [lblOrderCh4First setText:@"EQ"];
                    [lblOrderCh4First setTextColor:[UIColor blackColor]];
                    [lblOrderCh4Second setText:@"DYN"];
                    [lblOrderCh4Second setTextColor:[UIColor blueColor]];
                    [lblOrderCh4Third setText:@"DELAY"];
                    [lblOrderCh4Third setTextColor:[UIColor orangeColor]];
                } else if (order == ORDER_CH_EQ_DELAY_DYN) {
                    [lblOrderCh4First setText:@"EQ"];
                    [lblOrderCh4First setTextColor:[UIColor blackColor]];
                    [lblOrderCh4Second setText:@"DELAY"];
                    [lblOrderCh4Second setTextColor:[UIColor orangeColor]];
                    [lblOrderCh4Third setText:@"DYN"];
                    [lblOrderCh4Third setTextColor:[UIColor blueColor]];
                } else if (order == ORDER_CH_DYN_EQ_DELAY) {
                    [lblOrderCh4First setText:@"DYN"];
                    [lblOrderCh4First setTextColor:[UIColor blueColor]];
                    [lblOrderCh4Second setText:@"EQ"];
                    [lblOrderCh4Second setTextColor:[UIColor blackColor]];
                    [lblOrderCh4Third setText:@"DELAY"];
                    [lblOrderCh4Third setTextColor:[UIColor orangeColor]];
                } else if (order == ORDER_CH_DYN_DELAY_EQ) {
                    [lblOrderCh4First setText:@"DYN"];
                    [lblOrderCh4First setTextColor:[UIColor blueColor]];
                    [lblOrderCh4Second setText:@"DELAY"];
                    [lblOrderCh4Second setTextColor:[UIColor orangeColor]];
                    [lblOrderCh4Third setText:@"EQ"];
                    [lblOrderCh4Third setTextColor:[UIColor blackColor]];
                } else if (order == ORDER_CH_DELAY_DYN_EQ) {
                    [lblOrderCh4First setText:@"DELAY"];
                    [lblOrderCh4First setTextColor:[UIColor orangeColor]];
                    [lblOrderCh4Second setText:@"DYN"];
                    [lblOrderCh4Second setTextColor:[UIColor blueColor]];
                    [lblOrderCh4Third setText:@"EQ"];
                    [lblOrderCh4Third setTextColor:[UIColor blackColor]];
                } else {
                    [lblOrderCh4First setText:@"DELAY"];
                    [lblOrderCh4First setTextColor:[UIColor orangeColor]];
                    [lblOrderCh4Second setText:@"EQ"];
                    [lblOrderCh4Second setTextColor:[UIColor blackColor]];
                    [lblOrderCh4Third setText:@"DYN"];
                    [lblOrderCh4Third setTextColor:[UIColor blueColor]];
                }
                [Global setOrder:&_ch4Ctrl value:order];
                [self sendData:CMD_CH4_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch4Ctrl];
            }
            break;
        case CH_5:
            {
                if (order == ORDER_CH_EQ_DYN_DELAY) {
                    [lblOrderCh5First setText:@"EQ"];
                    [lblOrderCh5First setTextColor:[UIColor blackColor]];
                    [lblOrderCh5Second setText:@"DYN"];
                    [lblOrderCh5Second setTextColor:[UIColor blueColor]];
                    [lblOrderCh5Third setText:@"DELAY"];
                    [lblOrderCh5Third setTextColor:[UIColor orangeColor]];
                } else if (order == ORDER_CH_EQ_DELAY_DYN) {
                    [lblOrderCh5First setText:@"EQ"];
                    [lblOrderCh5First setTextColor:[UIColor blackColor]];
                    [lblOrderCh5Second setText:@"DELAY"];
                    [lblOrderCh5Second setTextColor:[UIColor orangeColor]];
                    [lblOrderCh5Third setText:@"DYN"];
                    [lblOrderCh5Third setTextColor:[UIColor blueColor]];
                } else if (order == ORDER_CH_DYN_EQ_DELAY) {
                    [lblOrderCh5First setText:@"DYN"];
                    [lblOrderCh5First setTextColor:[UIColor blueColor]];
                    [lblOrderCh5Second setText:@"EQ"];
                    [lblOrderCh5Second setTextColor:[UIColor blackColor]];
                    [lblOrderCh5Third setText:@"DELAY"];
                    [lblOrderCh5Third setTextColor:[UIColor orangeColor]];
                } else if (order == ORDER_CH_DYN_DELAY_EQ) {
                    [lblOrderCh5First setText:@"DYN"];
                    [lblOrderCh5First setTextColor:[UIColor blueColor]];
                    [lblOrderCh5Second setText:@"DELAY"];
                    [lblOrderCh5Second setTextColor:[UIColor orangeColor]];
                    [lblOrderCh5Third setText:@"EQ"];
                    [lblOrderCh5Third setTextColor:[UIColor blackColor]];
                } else if (order == ORDER_CH_DELAY_DYN_EQ) {
                    [lblOrderCh5First setText:@"DELAY"];
                    [lblOrderCh5First setTextColor:[UIColor orangeColor]];
                    [lblOrderCh5Second setText:@"DYN"];
                    [lblOrderCh5Second setTextColor:[UIColor blueColor]];
                    [lblOrderCh5Third setText:@"EQ"];
                    [lblOrderCh5Third setTextColor:[UIColor blackColor]];
                } else {
                    [lblOrderCh5First setText:@"DELAY"];
                    [lblOrderCh5First setTextColor:[UIColor orangeColor]];
                    [lblOrderCh5Second setText:@"EQ"];
                    [lblOrderCh5Second setTextColor:[UIColor blackColor]];
                    [lblOrderCh5Third setText:@"DYN"];
                    [lblOrderCh5Third setTextColor:[UIColor blueColor]];
                }
                [Global setOrder:&_ch5Ctrl value:order];
                [self sendData:CMD_CH5_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch5Ctrl];
            }
            break;
        case CH_6:
            {
                if (order == ORDER_CH_EQ_DYN_DELAY) {
                    [lblOrderCh6First setText:@"EQ"];
                    [lblOrderCh6First setTextColor:[UIColor blackColor]];
                    [lblOrderCh6Second setText:@"DYN"];
                    [lblOrderCh6Second setTextColor:[UIColor blueColor]];
                    [lblOrderCh6Third setText:@"DELAY"];
                    [lblOrderCh6Third setTextColor:[UIColor orangeColor]];
                } else if (order == ORDER_CH_EQ_DELAY_DYN) {
                    [lblOrderCh6First setText:@"EQ"];
                    [lblOrderCh6First setTextColor:[UIColor blackColor]];
                    [lblOrderCh6Second setText:@"DELAY"];
                    [lblOrderCh6Second setTextColor:[UIColor orangeColor]];
                    [lblOrderCh6Third setText:@"DYN"];
                    [lblOrderCh6Third setTextColor:[UIColor blueColor]];
                } else if (order == ORDER_CH_DYN_EQ_DELAY) {
                    [lblOrderCh6First setText:@"DYN"];
                    [lblOrderCh6First setTextColor:[UIColor blueColor]];
                    [lblOrderCh6Second setText:@"EQ"];
                    [lblOrderCh6Second setTextColor:[UIColor blackColor]];
                    [lblOrderCh6Third setText:@"DELAY"];
                    [lblOrderCh6Third setTextColor:[UIColor orangeColor]];
                } else if (order == ORDER_CH_DYN_DELAY_EQ) {
                    [lblOrderCh6First setText:@"DYN"];
                    [lblOrderCh6First setTextColor:[UIColor blueColor]];
                    [lblOrderCh6Second setText:@"DELAY"];
                    [lblOrderCh6Second setTextColor:[UIColor orangeColor]];
                    [lblOrderCh6Third setText:@"EQ"];
                    [lblOrderCh6Third setTextColor:[UIColor blackColor]];
                } else if (order == ORDER_CH_DELAY_DYN_EQ) {
                    [lblOrderCh6First setText:@"DELAY"];
                    [lblOrderCh6First setTextColor:[UIColor orangeColor]];
                    [lblOrderCh6Second setText:@"DYN"];
                    [lblOrderCh6Second setTextColor:[UIColor blueColor]];
                    [lblOrderCh6Third setText:@"EQ"];
                    [lblOrderCh6Third setTextColor:[UIColor blackColor]];
                } else {
                    [lblOrderCh6First setText:@"DELAY"];
                    [lblOrderCh6First setTextColor:[UIColor orangeColor]];
                    [lblOrderCh6Second setText:@"EQ"];
                    [lblOrderCh6Second setTextColor:[UIColor blackColor]];
                    [lblOrderCh6Third setText:@"DYN"];
                    [lblOrderCh6Third setTextColor:[UIColor blueColor]];
                }
                [Global setOrder:&_ch6Ctrl value:order];
                [self sendData:CMD_CH6_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch6Ctrl];
            }
            break;
        case CH_7:
            {
                if (order == ORDER_CH_EQ_DYN_DELAY) {
                    [lblOrderCh7First setText:@"EQ"];
                    [lblOrderCh7First setTextColor:[UIColor blackColor]];
                    [lblOrderCh7Second setText:@"DYN"];
                    [lblOrderCh7Second setTextColor:[UIColor blueColor]];
                    [lblOrderCh7Third setText:@"DELAY"];
                    [lblOrderCh7Third setTextColor:[UIColor orangeColor]];
                } else if (order == ORDER_CH_EQ_DELAY_DYN) {
                    [lblOrderCh7First setText:@"EQ"];
                    [lblOrderCh7First setTextColor:[UIColor blackColor]];
                    [lblOrderCh7Second setText:@"DELAY"];
                    [lblOrderCh7Second setTextColor:[UIColor orangeColor]];
                    [lblOrderCh7Third setText:@"DYN"];
                    [lblOrderCh7Third setTextColor:[UIColor blueColor]];
                } else if (order == ORDER_CH_DYN_EQ_DELAY) {
                    [lblOrderCh7First setText:@"DYN"];
                    [lblOrderCh7First setTextColor:[UIColor blueColor]];
                    [lblOrderCh7Second setText:@"EQ"];
                    [lblOrderCh7Second setTextColor:[UIColor blackColor]];
                    [lblOrderCh7Third setText:@"DELAY"];
                    [lblOrderCh7Third setTextColor:[UIColor orangeColor]];
                } else if (order == ORDER_CH_DYN_DELAY_EQ) {
                    [lblOrderCh7First setText:@"DYN"];
                    [lblOrderCh7First setTextColor:[UIColor blueColor]];
                    [lblOrderCh7Second setText:@"DELAY"];
                    [lblOrderCh7Second setTextColor:[UIColor orangeColor]];
                    [lblOrderCh7Third setText:@"EQ"];
                    [lblOrderCh7Third setTextColor:[UIColor blackColor]];
                } else if (order == ORDER_CH_DELAY_DYN_EQ) {
                    [lblOrderCh7First setText:@"DELAY"];
                    [lblOrderCh7First setTextColor:[UIColor orangeColor]];
                    [lblOrderCh7Second setText:@"DYN"];
                    [lblOrderCh7Second setTextColor:[UIColor blueColor]];
                    [lblOrderCh7Third setText:@"EQ"];
                    [lblOrderCh7Third setTextColor:[UIColor blackColor]];
                } else {
                    [lblOrderCh7First setText:@"DELAY"];
                    [lblOrderCh7First setTextColor:[UIColor orangeColor]];
                    [lblOrderCh7Second setText:@"EQ"];
                    [lblOrderCh7Second setTextColor:[UIColor blackColor]];
                    [lblOrderCh7Third setText:@"DYN"];
                    [lblOrderCh7Third setTextColor:[UIColor blueColor]];
                }
                [Global setOrder:&_ch7Ctrl value:order];
                [self sendData:CMD_CH7_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch7Ctrl];
            }
            break;
        case CH_8:
            {
                if (order == ORDER_CH_EQ_DYN_DELAY) {
                    [lblOrderCh8First setText:@"EQ"];
                    [lblOrderCh8First setTextColor:[UIColor blackColor]];
                    [lblOrderCh8Second setText:@"DYN"];
                    [lblOrderCh8Second setTextColor:[UIColor blueColor]];
                    [lblOrderCh8Third setText:@"DELAY"];
                    [lblOrderCh8Third setTextColor:[UIColor orangeColor]];
                } else if (order == ORDER_CH_EQ_DELAY_DYN) {
                    [lblOrderCh8First setText:@"EQ"];
                    [lblOrderCh8First setTextColor:[UIColor blackColor]];
                    [lblOrderCh8Second setText:@"DELAY"];
                    [lblOrderCh8Second setTextColor:[UIColor orangeColor]];
                    [lblOrderCh8Third setText:@"DYN"];
                    [lblOrderCh8Third setTextColor:[UIColor blueColor]];
                } else if (order == ORDER_CH_DYN_EQ_DELAY) {
                    [lblOrderCh8First setText:@"DYN"];
                    [lblOrderCh8First setTextColor:[UIColor blueColor]];
                    [lblOrderCh8Second setText:@"EQ"];
                    [lblOrderCh8Second setTextColor:[UIColor blackColor]];
                    [lblOrderCh8Third setText:@"DELAY"];
                    [lblOrderCh8Third setTextColor:[UIColor orangeColor]];
                } else if (order == ORDER_CH_DYN_DELAY_EQ) {
                    [lblOrderCh8First setText:@"DYN"];
                    [lblOrderCh8First setTextColor:[UIColor blueColor]];
                    [lblOrderCh8Second setText:@"DELAY"];
                    [lblOrderCh8Second setTextColor:[UIColor orangeColor]];
                    [lblOrderCh8Third setText:@"EQ"];
                    [lblOrderCh8Third setTextColor:[UIColor blackColor]];
                } else if (order == ORDER_CH_DELAY_DYN_EQ) {
                    [lblOrderCh8First setText:@"DELAY"];
                    [lblOrderCh8First setTextColor:[UIColor orangeColor]];
                    [lblOrderCh8Second setText:@"DYN"];
                    [lblOrderCh8Second setTextColor:[UIColor blueColor]];
                    [lblOrderCh8Third setText:@"EQ"];
                    [lblOrderCh8Third setTextColor:[UIColor blackColor]];
                } else {
                    [lblOrderCh8First setText:@"DELAY"];
                    [lblOrderCh8First setTextColor:[UIColor orangeColor]];
                    [lblOrderCh8Second setText:@"EQ"];
                    [lblOrderCh8Second setTextColor:[UIColor blackColor]];
                    [lblOrderCh8Third setText:@"DYN"];
                    [lblOrderCh8Third setTextColor:[UIColor blueColor]];
                }
                [Global setOrder:&_ch8Ctrl value:order];
                [self sendData:CMD_CH8_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch8Ctrl];
            }
            break;
        case CH_9:
            {
                if (order == ORDER_CH_EQ_DYN_DELAY) {
                    [lblOrderCh9First setText:@"EQ"];
                    [lblOrderCh9First setTextColor:[UIColor blackColor]];
                    [lblOrderCh9Second setText:@"DYN"];
                    [lblOrderCh9Second setTextColor:[UIColor blueColor]];
                    [lblOrderCh9Third setText:@"DELAY"];
                    [lblOrderCh9Third setTextColor:[UIColor orangeColor]];
                } else if (order == ORDER_CH_EQ_DELAY_DYN) {
                    [lblOrderCh9First setText:@"EQ"];
                    [lblOrderCh9First setTextColor:[UIColor blackColor]];
                    [lblOrderCh9Second setText:@"DELAY"];
                    [lblOrderCh9Second setTextColor:[UIColor orangeColor]];
                    [lblOrderCh9Third setText:@"DYN"];
                    [lblOrderCh9Third setTextColor:[UIColor blueColor]];
                } else if (order == ORDER_CH_DYN_EQ_DELAY) {
                    [lblOrderCh9First setText:@"DYN"];
                    [lblOrderCh9First setTextColor:[UIColor blueColor]];
                    [lblOrderCh9Second setText:@"EQ"];
                    [lblOrderCh9Second setTextColor:[UIColor blackColor]];
                    [lblOrderCh9Third setText:@"DELAY"];
                    [lblOrderCh9Third setTextColor:[UIColor orangeColor]];
                } else if (order == ORDER_CH_DYN_DELAY_EQ) {
                    [lblOrderCh9First setText:@"DYN"];
                    [lblOrderCh9First setTextColor:[UIColor blueColor]];
                    [lblOrderCh9Second setText:@"DELAY"];
                    [lblOrderCh9Second setTextColor:[UIColor orangeColor]];
                    [lblOrderCh9Third setText:@"EQ"];
                    [lblOrderCh9Third setTextColor:[UIColor blackColor]];
                } else if (order == ORDER_CH_DELAY_DYN_EQ) {
                    [lblOrderCh9First setText:@"DELAY"];
                    [lblOrderCh9First setTextColor:[UIColor orangeColor]];
                    [lblOrderCh9Second setText:@"DYN"];
                    [lblOrderCh9Second setTextColor:[UIColor blueColor]];
                    [lblOrderCh9Third setText:@"EQ"];
                    [lblOrderCh9Third setTextColor:[UIColor blackColor]];
                } else {
                    [lblOrderCh9First setText:@"DELAY"];
                    [lblOrderCh9First setTextColor:[UIColor orangeColor]];
                    [lblOrderCh9Second setText:@"EQ"];
                    [lblOrderCh9Second setTextColor:[UIColor blackColor]];
                    [lblOrderCh9Third setText:@"DYN"];
                    [lblOrderCh9Third setTextColor:[UIColor blueColor]];
                }
                [Global setOrder:&_ch9Ctrl value:order];
                [self sendData:CMD_CH9_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch9Ctrl];
            }
            break;
        case CH_10:
            {
                if (order == ORDER_CH_EQ_DYN_DELAY) {
                    [lblOrderCh10First setText:@"EQ"];
                    [lblOrderCh10First setTextColor:[UIColor blackColor]];
                    [lblOrderCh10Second setText:@"DYN"];
                    [lblOrderCh10Second setTextColor:[UIColor blueColor]];
                    [lblOrderCh10Third setText:@"DELAY"];
                    [lblOrderCh10Third setTextColor:[UIColor orangeColor]];
                } else if (order == ORDER_CH_EQ_DELAY_DYN) {
                    [lblOrderCh10First setText:@"EQ"];
                    [lblOrderCh10First setTextColor:[UIColor blackColor]];
                    [lblOrderCh10Second setText:@"DELAY"];
                    [lblOrderCh10Second setTextColor:[UIColor orangeColor]];
                    [lblOrderCh10Third setText:@"DYN"];
                    [lblOrderCh10Third setTextColor:[UIColor blueColor]];
                } else if (order == ORDER_CH_DYN_EQ_DELAY) {
                    [lblOrderCh10First setText:@"DYN"];
                    [lblOrderCh10First setTextColor:[UIColor blueColor]];
                    [lblOrderCh10Second setText:@"EQ"];
                    [lblOrderCh10Second setTextColor:[UIColor blackColor]];
                    [lblOrderCh10Third setText:@"DELAY"];
                    [lblOrderCh10Third setTextColor:[UIColor orangeColor]];
                } else if (order == ORDER_CH_DYN_DELAY_EQ) {
                    [lblOrderCh10First setText:@"DYN"];
                    [lblOrderCh10First setTextColor:[UIColor blueColor]];
                    [lblOrderCh10Second setText:@"DELAY"];
                    [lblOrderCh10Second setTextColor:[UIColor orangeColor]];
                    [lblOrderCh10Third setText:@"EQ"];
                    [lblOrderCh10Third setTextColor:[UIColor blackColor]];
                } else if (order == ORDER_CH_DELAY_DYN_EQ) {
                    [lblOrderCh10First setText:@"DELAY"];
                    [lblOrderCh10First setTextColor:[UIColor orangeColor]];
                    [lblOrderCh10Second setText:@"DYN"];
                    [lblOrderCh10Second setTextColor:[UIColor blueColor]];
                    [lblOrderCh10Third setText:@"EQ"];
                    [lblOrderCh10Third setTextColor:[UIColor blackColor]];
                } else {
                    [lblOrderCh10First setText:@"DELAY"];
                    [lblOrderCh10First setTextColor:[UIColor orangeColor]];
                    [lblOrderCh10Second setText:@"EQ"];
                    [lblOrderCh10Second setTextColor:[UIColor blackColor]];
                    [lblOrderCh10Third setText:@"DYN"];
                    [lblOrderCh10Third setTextColor:[UIColor blueColor]];
                }
                [Global setOrder:&_ch10Ctrl value:order];
                [self sendData:CMD_CH10_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch10Ctrl];
            }
            break;
        case CH_11:
            {
                if (order == ORDER_CH_EQ_DYN_DELAY) {
                    [lblOrderCh11First setText:@"EQ"];
                    [lblOrderCh11First setTextColor:[UIColor blackColor]];
                    [lblOrderCh11Second setText:@"DYN"];
                    [lblOrderCh11Second setTextColor:[UIColor blueColor]];
                    [lblOrderCh11Third setText:@"DELAY"];
                    [lblOrderCh11Third setTextColor:[UIColor orangeColor]];
                } else if (order == ORDER_CH_EQ_DELAY_DYN) {
                    [lblOrderCh11First setText:@"EQ"];
                    [lblOrderCh11First setTextColor:[UIColor blackColor]];
                    [lblOrderCh11Second setText:@"DELAY"];
                    [lblOrderCh11Second setTextColor:[UIColor orangeColor]];
                    [lblOrderCh11Third setText:@"DYN"];
                    [lblOrderCh11Third setTextColor:[UIColor blueColor]];
                } else if (order == ORDER_CH_DYN_EQ_DELAY) {
                    [lblOrderCh11First setText:@"DYN"];
                    [lblOrderCh11First setTextColor:[UIColor blueColor]];
                    [lblOrderCh11Second setText:@"EQ"];
                    [lblOrderCh11Second setTextColor:[UIColor blackColor]];
                    [lblOrderCh11Third setText:@"DELAY"];
                    [lblOrderCh11Third setTextColor:[UIColor orangeColor]];
                } else if (order == ORDER_CH_DYN_DELAY_EQ) {
                    [lblOrderCh11First setText:@"DYN"];
                    [lblOrderCh11First setTextColor:[UIColor blueColor]];
                    [lblOrderCh11Second setText:@"DELAY"];
                    [lblOrderCh11Second setTextColor:[UIColor orangeColor]];
                    [lblOrderCh11Third setText:@"EQ"];
                    [lblOrderCh11Third setTextColor:[UIColor blackColor]];
                } else if (order == ORDER_CH_DELAY_DYN_EQ) {
                    [lblOrderCh11First setText:@"DELAY"];
                    [lblOrderCh11First setTextColor:[UIColor orangeColor]];
                    [lblOrderCh11Second setText:@"DYN"];
                    [lblOrderCh11Second setTextColor:[UIColor blueColor]];
                    [lblOrderCh11Third setText:@"EQ"];
                    [lblOrderCh11Third setTextColor:[UIColor blackColor]];
                } else {
                    [lblOrderCh11First setText:@"DELAY"];
                    [lblOrderCh11First setTextColor:[UIColor orangeColor]];
                    [lblOrderCh11Second setText:@"EQ"];
                    [lblOrderCh11Second setTextColor:[UIColor blackColor]];
                    [lblOrderCh11Third setText:@"DYN"];
                    [lblOrderCh11Third setTextColor:[UIColor blueColor]];
                }
                [Global setOrder:&_ch11Ctrl value:order];
                [self sendData:CMD_CH11_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch11Ctrl];
            }
            break;
        case CH_12:
            {
                if (order == ORDER_CH_EQ_DYN_DELAY) {
                    [lblOrderCh12First setText:@"EQ"];
                    [lblOrderCh12First setTextColor:[UIColor blackColor]];
                    [lblOrderCh12Second setText:@"DYN"];
                    [lblOrderCh12Second setTextColor:[UIColor blueColor]];
                    [lblOrderCh12Third setText:@"DELAY"];
                    [lblOrderCh12Third setTextColor:[UIColor orangeColor]];
                } else if (order == ORDER_CH_EQ_DELAY_DYN) {
                    [lblOrderCh12First setText:@"EQ"];
                    [lblOrderCh12First setTextColor:[UIColor blackColor]];
                    [lblOrderCh12Second setText:@"DELAY"];
                    [lblOrderCh12Second setTextColor:[UIColor orangeColor]];
                    [lblOrderCh12Third setText:@"DYN"];
                    [lblOrderCh12Third setTextColor:[UIColor blueColor]];
                } else if (order == ORDER_CH_DYN_EQ_DELAY) {
                    [lblOrderCh12First setText:@"DYN"];
                    [lblOrderCh12First setTextColor:[UIColor blueColor]];
                    [lblOrderCh12Second setText:@"EQ"];
                    [lblOrderCh12Second setTextColor:[UIColor blackColor]];
                    [lblOrderCh12Third setText:@"DELAY"];
                    [lblOrderCh12Third setTextColor:[UIColor orangeColor]];
                } else if (order == ORDER_CH_DYN_DELAY_EQ) {
                    [lblOrderCh12First setText:@"DYN"];
                    [lblOrderCh12First setTextColor:[UIColor blueColor]];
                    [lblOrderCh12Second setText:@"DELAY"];
                    [lblOrderCh12Second setTextColor:[UIColor orangeColor]];
                    [lblOrderCh12Third setText:@"EQ"];
                    [lblOrderCh12Third setTextColor:[UIColor blackColor]];
                } else if (order == ORDER_CH_DELAY_DYN_EQ) {
                    [lblOrderCh12First setText:@"DELAY"];
                    [lblOrderCh12First setTextColor:[UIColor orangeColor]];
                    [lblOrderCh12Second setText:@"DYN"];
                    [lblOrderCh12Second setTextColor:[UIColor blueColor]];
                    [lblOrderCh12Third setText:@"EQ"];
                    [lblOrderCh12Third setTextColor:[UIColor blackColor]];
                } else {
                    [lblOrderCh12First setText:@"DELAY"];
                    [lblOrderCh12First setTextColor:[UIColor orangeColor]];
                    [lblOrderCh12Second setText:@"EQ"];
                    [lblOrderCh12Second setTextColor:[UIColor blackColor]];
                    [lblOrderCh12Third setText:@"DYN"];
                    [lblOrderCh12Third setTextColor:[UIColor blueColor]];
                }
                [Global setOrder:&_ch12Ctrl value:order];
                [self sendData:CMD_CH12_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch12Ctrl];
            }
            break;
        case CH_13:
            {
                if (order == ORDER_CH_EQ_DYN_DELAY) {
                    [lblOrderCh13First setText:@"EQ"];
                    [lblOrderCh13First setTextColor:[UIColor blackColor]];
                    [lblOrderCh13Second setText:@"DYN"];
                    [lblOrderCh13Second setTextColor:[UIColor blueColor]];
                    [lblOrderCh13Third setText:@"DELAY"];
                    [lblOrderCh13Third setTextColor:[UIColor orangeColor]];
                } else if (order == ORDER_CH_EQ_DELAY_DYN) {
                    [lblOrderCh13First setText:@"EQ"];
                    [lblOrderCh13First setTextColor:[UIColor blackColor]];
                    [lblOrderCh13Second setText:@"DELAY"];
                    [lblOrderCh13Second setTextColor:[UIColor orangeColor]];
                    [lblOrderCh13Third setText:@"DYN"];
                    [lblOrderCh13Third setTextColor:[UIColor blueColor]];
                } else if (order == ORDER_CH_DYN_EQ_DELAY) {
                    [lblOrderCh13First setText:@"DYN"];
                    [lblOrderCh13First setTextColor:[UIColor blueColor]];
                    [lblOrderCh13Second setText:@"EQ"];
                    [lblOrderCh13Second setTextColor:[UIColor blackColor]];
                    [lblOrderCh13Third setText:@"DELAY"];
                    [lblOrderCh13Third setTextColor:[UIColor orangeColor]];
                } else if (order == ORDER_CH_DYN_DELAY_EQ) {
                    [lblOrderCh13First setText:@"DYN"];
                    [lblOrderCh13First setTextColor:[UIColor blueColor]];
                    [lblOrderCh13Second setText:@"DELAY"];
                    [lblOrderCh13Second setTextColor:[UIColor orangeColor]];
                    [lblOrderCh13Third setText:@"EQ"];
                    [lblOrderCh13Third setTextColor:[UIColor blackColor]];
                } else if (order == ORDER_CH_DELAY_DYN_EQ) {
                    [lblOrderCh13First setText:@"DELAY"];
                    [lblOrderCh13First setTextColor:[UIColor orangeColor]];
                    [lblOrderCh13Second setText:@"DYN"];
                    [lblOrderCh13Second setTextColor:[UIColor blueColor]];
                    [lblOrderCh13Third setText:@"EQ"];
                    [lblOrderCh13Third setTextColor:[UIColor blackColor]];
                } else {
                    [lblOrderCh13First setText:@"DELAY"];
                    [lblOrderCh13First setTextColor:[UIColor orangeColor]];
                    [lblOrderCh13Second setText:@"EQ"];
                    [lblOrderCh13Second setTextColor:[UIColor blackColor]];
                    [lblOrderCh13Third setText:@"DYN"];
                    [lblOrderCh13Third setTextColor:[UIColor blueColor]];
                }
                [Global setOrder:&_ch13Ctrl value:order];
                [self sendData:CMD_CH13_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch13Ctrl];
            }
            break;
        case CH_14:
            {
                if (order == ORDER_CH_EQ_DYN_DELAY) {
                    [lblOrderCh14First setText:@"EQ"];
                    [lblOrderCh14First setTextColor:[UIColor blackColor]];
                    [lblOrderCh14Second setText:@"DYN"];
                    [lblOrderCh14Second setTextColor:[UIColor blueColor]];
                    [lblOrderCh14Third setText:@"DELAY"];
                    [lblOrderCh14Third setTextColor:[UIColor orangeColor]];
                } else if (order == ORDER_CH_EQ_DELAY_DYN) {
                    [lblOrderCh14First setText:@"EQ"];
                    [lblOrderCh14First setTextColor:[UIColor blackColor]];
                    [lblOrderCh14Second setText:@"DELAY"];
                    [lblOrderCh14Second setTextColor:[UIColor orangeColor]];
                    [lblOrderCh14Third setText:@"DYN"];
                    [lblOrderCh14Third setTextColor:[UIColor blueColor]];
                } else if (order == ORDER_CH_DYN_EQ_DELAY) {
                    [lblOrderCh14First setText:@"DYN"];
                    [lblOrderCh14First setTextColor:[UIColor blueColor]];
                    [lblOrderCh14Second setText:@"EQ"];
                    [lblOrderCh14Second setTextColor:[UIColor blackColor]];
                    [lblOrderCh14Third setText:@"DELAY"];
                    [lblOrderCh14Third setTextColor:[UIColor orangeColor]];
                } else if (order == ORDER_CH_DYN_DELAY_EQ) {
                    [lblOrderCh14First setText:@"DYN"];
                    [lblOrderCh14First setTextColor:[UIColor blueColor]];
                    [lblOrderCh14Second setText:@"DELAY"];
                    [lblOrderCh14Second setTextColor:[UIColor orangeColor]];
                    [lblOrderCh14Third setText:@"EQ"];
                    [lblOrderCh14Third setTextColor:[UIColor blackColor]];
                } else if (order == ORDER_CH_DELAY_DYN_EQ) {
                    [lblOrderCh14First setText:@"DELAY"];
                    [lblOrderCh14First setTextColor:[UIColor orangeColor]];
                    [lblOrderCh14Second setText:@"DYN"];
                    [lblOrderCh14Second setTextColor:[UIColor blueColor]];
                    [lblOrderCh14Third setText:@"EQ"];
                    [lblOrderCh14Third setTextColor:[UIColor blackColor]];
                } else {
                    [lblOrderCh14First setText:@"DELAY"];
                    [lblOrderCh14First setTextColor:[UIColor orangeColor]];
                    [lblOrderCh14Second setText:@"EQ"];
                    [lblOrderCh14Second setTextColor:[UIColor blackColor]];
                    [lblOrderCh14Third setText:@"DYN"];
                    [lblOrderCh14Third setTextColor:[UIColor blueColor]];
                }
                [Global setOrder:&_ch14Ctrl value:order];
                [self sendData:CMD_CH14_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch14Ctrl];
            }
            break;
        case CH_15:
            {
                if (order == ORDER_CH_EQ_DYN_DELAY) {
                    [lblOrderCh15First setText:@"EQ"];
                    [lblOrderCh15First setTextColor:[UIColor blackColor]];
                    [lblOrderCh15Second setText:@"DYN"];
                    [lblOrderCh15Second setTextColor:[UIColor blueColor]];
                    [lblOrderCh15Third setText:@"DELAY"];
                    [lblOrderCh15Third setTextColor:[UIColor orangeColor]];
                } else if (order == ORDER_CH_EQ_DELAY_DYN) {
                    [lblOrderCh15First setText:@"EQ"];
                    [lblOrderCh15First setTextColor:[UIColor blackColor]];
                    [lblOrderCh15Second setText:@"DELAY"];
                    [lblOrderCh15Second setTextColor:[UIColor orangeColor]];
                    [lblOrderCh15Third setText:@"DYN"];
                    [lblOrderCh15Third setTextColor:[UIColor blueColor]];
                } else if (order == ORDER_CH_DYN_EQ_DELAY) {
                    [lblOrderCh15First setText:@"DYN"];
                    [lblOrderCh15First setTextColor:[UIColor blueColor]];
                    [lblOrderCh15Second setText:@"EQ"];
                    [lblOrderCh15Second setTextColor:[UIColor blackColor]];
                    [lblOrderCh15Third setText:@"DELAY"];
                    [lblOrderCh15Third setTextColor:[UIColor orangeColor]];
                } else if (order == ORDER_CH_DYN_DELAY_EQ) {
                    [lblOrderCh15First setText:@"DYN"];
                    [lblOrderCh15First setTextColor:[UIColor blueColor]];
                    [lblOrderCh15Second setText:@"DELAY"];
                    [lblOrderCh15Second setTextColor:[UIColor orangeColor]];
                    [lblOrderCh15Third setText:@"EQ"];
                    [lblOrderCh15Third setTextColor:[UIColor blackColor]];
                } else if (order == ORDER_CH_DELAY_DYN_EQ) {
                    [lblOrderCh15First setText:@"DELAY"];
                    [lblOrderCh15First setTextColor:[UIColor orangeColor]];
                    [lblOrderCh15Second setText:@"DYN"];
                    [lblOrderCh15Second setTextColor:[UIColor blueColor]];
                    [lblOrderCh15Third setText:@"EQ"];
                    [lblOrderCh15Third setTextColor:[UIColor blackColor]];
                } else {
                    [lblOrderCh15First setText:@"DELAY"];
                    [lblOrderCh15First setTextColor:[UIColor orangeColor]];
                    [lblOrderCh15Second setText:@"EQ"];
                    [lblOrderCh15Second setTextColor:[UIColor blackColor]];
                    [lblOrderCh15Third setText:@"DYN"];
                    [lblOrderCh15Third setTextColor:[UIColor blueColor]];
                }
                [Global setOrder:&_ch15Ctrl value:order];
                [self sendData:CMD_CH15_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch15Ctrl];
            }
            break;
        case CH_16:
            {
                if (order == ORDER_CH_EQ_DYN_DELAY) {
                    [lblOrderCh16First setText:@"EQ"];
                    [lblOrderCh16First setTextColor:[UIColor blackColor]];
                    [lblOrderCh16Second setText:@"DYN"];
                    [lblOrderCh16Second setTextColor:[UIColor blueColor]];
                    [lblOrderCh16Third setText:@"DELAY"];
                    [lblOrderCh16Third setTextColor:[UIColor orangeColor]];
                } else if (order == ORDER_CH_EQ_DELAY_DYN) {
                    [lblOrderCh16First setText:@"EQ"];
                    [lblOrderCh16First setTextColor:[UIColor blackColor]];
                    [lblOrderCh16Second setText:@"DELAY"];
                    [lblOrderCh16Second setTextColor:[UIColor orangeColor]];
                    [lblOrderCh16Third setText:@"DYN"];
                    [lblOrderCh16Third setTextColor:[UIColor blueColor]];
                } else if (order == ORDER_CH_DYN_EQ_DELAY) {
                    [lblOrderCh16First setText:@"DYN"];
                    [lblOrderCh16First setTextColor:[UIColor blueColor]];
                    [lblOrderCh16Second setText:@"EQ"];
                    [lblOrderCh16Second setTextColor:[UIColor blackColor]];
                    [lblOrderCh16Third setText:@"DELAY"];
                    [lblOrderCh16Third setTextColor:[UIColor orangeColor]];
                } else if (order == ORDER_CH_DYN_DELAY_EQ) {
                    [lblOrderCh16First setText:@"DYN"];
                    [lblOrderCh16First setTextColor:[UIColor blueColor]];
                    [lblOrderCh16Second setText:@"DELAY"];
                    [lblOrderCh16Second setTextColor:[UIColor orangeColor]];
                    [lblOrderCh16Third setText:@"EQ"];
                    [lblOrderCh16Third setTextColor:[UIColor blackColor]];
                } else if (order == ORDER_CH_DELAY_DYN_EQ) {
                    [lblOrderCh16First setText:@"DELAY"];
                    [lblOrderCh16First setTextColor:[UIColor orangeColor]];
                    [lblOrderCh16Second setText:@"DYN"];
                    [lblOrderCh16Second setTextColor:[UIColor blueColor]];
                    [lblOrderCh16Third setText:@"EQ"];
                    [lblOrderCh16Third setTextColor:[UIColor blackColor]];
                } else {
                    [lblOrderCh16First setText:@"DELAY"];
                    [lblOrderCh16First setTextColor:[UIColor orangeColor]];
                    [lblOrderCh16Second setText:@"EQ"];
                    [lblOrderCh16Second setTextColor:[UIColor blackColor]];
                    [lblOrderCh16Third setText:@"DYN"];
                    [lblOrderCh16Third setTextColor:[UIColor blueColor]];
                }
                [Global setOrder:&_ch16Ctrl value:order];
                [self sendData:CMD_CH16_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch16Ctrl];
            }
            break;
        case CH_17:
            {
                if (order == ORDER_CH_EQ_DYN_DELAY) {
                    [lblOrderCh17First setText:@"EQ"];
                    [lblOrderCh17First setTextColor:[UIColor blackColor]];
                    [lblOrderCh17Second setText:@"DYN"];
                    [lblOrderCh17Second setTextColor:[UIColor blueColor]];
                    [lblOrderCh17Third setText:@"DELAY"];
                    [lblOrderCh17Third setTextColor:[UIColor orangeColor]];
                } else if (order == ORDER_CH_EQ_DELAY_DYN) {
                    [lblOrderCh17First setText:@"EQ"];
                    [lblOrderCh17First setTextColor:[UIColor blackColor]];
                    [lblOrderCh17Second setText:@"DELAY"];
                    [lblOrderCh17Second setTextColor:[UIColor orangeColor]];
                    [lblOrderCh17Third setText:@"DYN"];
                    [lblOrderCh17Third setTextColor:[UIColor blueColor]];
                } else if (order == ORDER_CH_DYN_EQ_DELAY) {
                    [lblOrderCh17First setText:@"DYN"];
                    [lblOrderCh17First setTextColor:[UIColor blueColor]];
                    [lblOrderCh17Second setText:@"EQ"];
                    [lblOrderCh17Second setTextColor:[UIColor blackColor]];
                    [lblOrderCh17Third setText:@"DELAY"];
                    [lblOrderCh17Third setTextColor:[UIColor orangeColor]];
                } else if (order == ORDER_CH_DYN_DELAY_EQ) {
                    [lblOrderCh17First setText:@"DYN"];
                    [lblOrderCh17First setTextColor:[UIColor blueColor]];
                    [lblOrderCh17Second setText:@"DELAY"];
                    [lblOrderCh17Second setTextColor:[UIColor orangeColor]];
                    [lblOrderCh17Third setText:@"EQ"];
                    [lblOrderCh17Third setTextColor:[UIColor blackColor]];
                } else if (order == ORDER_CH_DELAY_DYN_EQ) {
                    [lblOrderCh17First setText:@"DELAY"];
                    [lblOrderCh17First setTextColor:[UIColor orangeColor]];
                    [lblOrderCh17Second setText:@"DYN"];
                    [lblOrderCh17Second setTextColor:[UIColor blueColor]];
                    [lblOrderCh17Third setText:@"EQ"];
                    [lblOrderCh17Third setTextColor:[UIColor blackColor]];
                } else {
                    [lblOrderCh17First setText:@"DELAY"];
                    [lblOrderCh17First setTextColor:[UIColor orangeColor]];
                    [lblOrderCh17Second setText:@"EQ"];
                    [lblOrderCh17Second setTextColor:[UIColor blackColor]];
                    [lblOrderCh17Third setText:@"DYN"];
                    [lblOrderCh17Third setTextColor:[UIColor blueColor]];
                }
                [Global setOrder:&_ch17Ctrl value:order];
                [self sendData:CMD_CH17_CTRL commandType:DSP_WRITE dspId:DSP_6 value:_ch17Ctrl];
            }
            break;
        case CH_18:
            {
                if (order == ORDER_CH_EQ_DYN_DELAY) {
                    [lblOrderCh18First setText:@"EQ"];
                    [lblOrderCh18First setTextColor:[UIColor blackColor]];
                    [lblOrderCh18Second setText:@"DYN"];
                    [lblOrderCh18Second setTextColor:[UIColor blueColor]];
                    [lblOrderCh18Third setText:@"DELAY"];
                    [lblOrderCh18Third setTextColor:[UIColor orangeColor]];
                } else if (order == ORDER_CH_EQ_DELAY_DYN) {
                    [lblOrderCh18First setText:@"EQ"];
                    [lblOrderCh18First setTextColor:[UIColor blackColor]];
                    [lblOrderCh18Second setText:@"DELAY"];
                    [lblOrderCh18Second setTextColor:[UIColor orangeColor]];
                    [lblOrderCh18Third setText:@"DYN"];
                    [lblOrderCh18Third setTextColor:[UIColor blueColor]];
                } else if (order == ORDER_CH_DYN_EQ_DELAY) {
                    [lblOrderCh18First setText:@"DYN"];
                    [lblOrderCh18First setTextColor:[UIColor blueColor]];
                    [lblOrderCh18Second setText:@"EQ"];
                    [lblOrderCh18Second setTextColor:[UIColor blackColor]];
                    [lblOrderCh18Third setText:@"DELAY"];
                    [lblOrderCh18Third setTextColor:[UIColor orangeColor]];
                } else if (order == ORDER_CH_DYN_DELAY_EQ) {
                    [lblOrderCh18First setText:@"DYN"];
                    [lblOrderCh18First setTextColor:[UIColor blueColor]];
                    [lblOrderCh18Second setText:@"DELAY"];
                    [lblOrderCh18Second setTextColor:[UIColor orangeColor]];
                    [lblOrderCh18Third setText:@"EQ"];
                    [lblOrderCh18Third setTextColor:[UIColor blackColor]];
                } else if (order == ORDER_CH_DELAY_DYN_EQ) {
                    [lblOrderCh18First setText:@"DELAY"];
                    [lblOrderCh18First setTextColor:[UIColor orangeColor]];
                    [lblOrderCh18Second setText:@"DYN"];
                    [lblOrderCh18Second setTextColor:[UIColor blueColor]];
                    [lblOrderCh18Third setText:@"EQ"];
                    [lblOrderCh18Third setTextColor:[UIColor blackColor]];
                } else {
                    [lblOrderCh18First setText:@"DELAY"];
                    [lblOrderCh18First setTextColor:[UIColor orangeColor]];
                    [lblOrderCh18Second setText:@"EQ"];
                    [lblOrderCh18Second setTextColor:[UIColor blackColor]];
                    [lblOrderCh18Third setText:@"DYN"];
                    [lblOrderCh18Third setTextColor:[UIColor blueColor]];
                }
                [Global setOrder:&_ch18Ctrl value:order];
                [self sendData:CMD_CH18_CTRL commandType:DSP_WRITE dspId:DSP_6 value:_ch18Ctrl];
            }
            break;
        case MT_1:
            {
                if (orderMulti[0] == ORDER_MT_EQ_DYN_DELAY) {
                    [lblOrderMt1First setText:@"EQ"];
                    [lblOrderMt1First setTextColor:[UIColor blackColor]];
                    [lblOrderMt1Second setText:@"DYN"];
                    [lblOrderMt1Second setTextColor:[UIColor blueColor]];
                    [lblOrderMt1Third setText:@"DELAY"];
                    [lblOrderMt1Third setTextColor:[UIColor orangeColor]];
                } else if (orderMulti[0] == ORDER_MT_DYN_EQ_DELAY) {
                    [lblOrderMt1First setText:@"DYN"];
                    [lblOrderMt1First setTextColor:[UIColor blueColor]];
                    [lblOrderMt1Second setText:@"EQ"];
                    [lblOrderMt1Second setTextColor:[UIColor blackColor]];
                    [lblOrderMt1Third setText:@"DELAY"];
                    [lblOrderMt1Third setTextColor:[UIColor orangeColor]];
                }
                [Global setOrder:&_multi1Ctrl value:orderMulti[0]];
                [self sendData:CMD_MULTI_1_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi1Ctrl];
            }
            break;
        case MT_2:
            {
                if (orderMulti[1] == ORDER_MT_EQ_DYN_DELAY) {
                    [lblOrderMt2First setText:@"EQ"];
                    [lblOrderMt2First setTextColor:[UIColor blackColor]];
                    [lblOrderMt2Second setText:@"DYN"];
                    [lblOrderMt2Second setTextColor:[UIColor blueColor]];
                    [lblOrderMt2Third setText:@"DELAY"];
                    [lblOrderMt2Third setTextColor:[UIColor orangeColor]];
                } else if (orderMulti[1] == ORDER_MT_DYN_EQ_DELAY) {
                    [lblOrderMt2First setText:@"DYN"];
                    [lblOrderMt2First setTextColor:[UIColor blueColor]];
                    [lblOrderMt2Second setText:@"EQ"];
                    [lblOrderMt2Second setTextColor:[UIColor blackColor]];
                    [lblOrderMt2Third setText:@"DELAY"];
                    [lblOrderMt2Third setTextColor:[UIColor orangeColor]];
                }
                [Global setOrder:&_multi2Ctrl value:orderMulti[1]];
                [self sendData:CMD_MULTI_2_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi2Ctrl];
            }
            break;
        case MT_3:
            {
                if (orderMulti[2] == ORDER_MT_EQ_DYN_DELAY) {
                    [lblOrderMt3First setText:@"EQ"];
                    [lblOrderMt3First setTextColor:[UIColor blackColor]];
                    [lblOrderMt3Second setText:@"DYN"];
                    [lblOrderMt3Second setTextColor:[UIColor blueColor]];
                    [lblOrderMt3Third setText:@"DELAY"];
                    [lblOrderMt3Third setTextColor:[UIColor orangeColor]];
                } else if (orderMulti[2] == ORDER_MT_DYN_EQ_DELAY) {
                    [lblOrderMt3First setText:@"DYN"];
                    [lblOrderMt3First setTextColor:[UIColor blueColor]];
                    [lblOrderMt3Second setText:@"EQ"];
                    [lblOrderMt3Second setTextColor:[UIColor blackColor]];
                    [lblOrderMt3Third setText:@"DELAY"];
                    [lblOrderMt3Third setTextColor:[UIColor orangeColor]];
                }
                [Global setOrder:&_multi3Ctrl value:orderMulti[2]];
                [self sendData:CMD_MULTI_3_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi3Ctrl];
            }
            break;
        case MT_4:
            {
                if (orderMulti[3] == ORDER_MT_EQ_DYN_DELAY) {
                    [lblOrderMt4First setText:@"EQ"];
                    [lblOrderMt4First setTextColor:[UIColor blackColor]];
                    [lblOrderMt4Second setText:@"DYN"];
                    [lblOrderMt4Second setTextColor:[UIColor blueColor]];
                    [lblOrderMt4Third setText:@"DELAY"];
                    [lblOrderMt4Third setTextColor:[UIColor orangeColor]];
                } else if (orderMulti[3] == ORDER_MT_DYN_EQ_DELAY) {
                    [lblOrderMt4First setText:@"DYN"];
                    [lblOrderMt4First setTextColor:[UIColor blueColor]];
                    [lblOrderMt4Second setText:@"EQ"];
                    [lblOrderMt4Second setTextColor:[UIColor blackColor]];
                    [lblOrderMt4Third setText:@"DELAY"];
                    [lblOrderMt4Third setTextColor:[UIColor orangeColor]];
                }
                [Global setOrder:&_multi4Ctrl value:orderMulti[3]];
                [self sendData:CMD_MULTI_4_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi4Ctrl];
            }
            break;
        case MAIN:
            {
                if (orderMain == ORDER_MAIN_EQ_DYN_DELAY) {
                    [lblOrderMainFirst setText:@"EQ"];
                    [lblOrderMainFirst setTextColor:[UIColor blackColor]];
                    [lblOrderMainSecond setText:@"DYN"];
                    [lblOrderMainSecond setTextColor:[UIColor blueColor]];
                    [lblOrderMainThird setText:@"DELAY"];
                    [lblOrderMainThird setTextColor:[UIColor orangeColor]];
                } else if (orderMain == ORDER_MAIN_DYN_EQ_DELAY) {
                    [lblOrderMainFirst setText:@"DYN"];
                    [lblOrderMainFirst setTextColor:[UIColor blueColor]];
                    [lblOrderMainSecond setText:@"EQ"];
                    [lblOrderMainSecond setTextColor:[UIColor blackColor]];
                    [lblOrderMainThird setText:@"DELAY"];
                    [lblOrderMainThird setTextColor:[UIColor orangeColor]];
                } else if (orderMain == ORDER_MAIN_DYN_GEQ_DELAY) {
                    [lblOrderMainFirst setText:@"DYN"];
                    [lblOrderMainFirst setTextColor:[UIColor blueColor]];
                    [lblOrderMainSecond setText:@"GEQ"];
                    [lblOrderMainSecond setTextColor:[UIColor blackColor]];
                    [lblOrderMainThird setText:@"DELAY"];
                    [lblOrderMainThird setTextColor:[UIColor orangeColor]];
                } else if (orderMain == ORDER_MAIN_GEQ_DYN_DELAY) {
                    [lblOrderMainFirst setText:@"GEQ"];
                    [lblOrderMainFirst setTextColor:[UIColor blackColor]];
                    [lblOrderMainSecond setText:@"DYN"];
                    [lblOrderMainSecond setTextColor:[UIColor blueColor]];
                    [lblOrderMainThird setText:@"DELAY"];
                    [lblOrderMainThird setTextColor:[UIColor orangeColor]];
                }
                [Global setOrder:&_mainCtrl value:orderMain];
                [self sendData:CMD_MAIN_LR_CTRL commandType:DSP_WRITE dspId:DSP_6 value:_mainCtrl];
            }
            break;
        default:
            break;
    }
}

- (IBAction)onBtnOrderEqDynDelay:(id)sender {
    [viewOrderPickerCtrl setHidden:YES];
    if (currentOrderChannel >= CH_1 && currentOrderChannel <= CH_18) {
        orderChannel[currentOrderChannel] = ORDER_CH_EQ_DYN_DELAY;
        [self updateOrderLabels:currentOrderChannel];
    }
}

- (IBAction)onBtnOrderEqDelayDyn:(id)sender {
    [viewOrderPickerCtrl setHidden:YES];
    if (currentOrderChannel >= CH_1 && currentOrderChannel <= CH_18) {
        orderChannel[currentOrderChannel] = ORDER_CH_EQ_DELAY_DYN;
        [self updateOrderLabels:currentOrderChannel];
    }
}

- (IBAction)onBtnOrderDynEqDelay:(id)sender {
    [viewOrderPickerCtrl setHidden:YES];
    if (currentOrderChannel >= CH_1 && currentOrderChannel <= CH_18) {
        orderChannel[currentOrderChannel] = ORDER_CH_DYN_EQ_DELAY;
        [self updateOrderLabels:currentOrderChannel];
    }
}

- (IBAction)onBtnOrderDynDelayEq:(id)sender {
    [viewOrderPickerCtrl setHidden:YES];
    if (currentOrderChannel >= CH_1 && currentOrderChannel <= CH_18) {
        orderChannel[currentOrderChannel] = ORDER_CH_DYN_DELAY_EQ;
        [self updateOrderLabels:currentOrderChannel];
    }
}

- (IBAction)onBtnOrderDelayEqDyn:(id)sender {
    [viewOrderPickerCtrl setHidden:YES];
    if (currentOrderChannel >= CH_1 && currentOrderChannel <= CH_18) {
        orderChannel[currentOrderChannel] = ORDER_CH_DELAY_EQ_DYN;
        [self updateOrderLabels:currentOrderChannel];
    }
}

- (IBAction)onBtnOrderDelayDynEq:(id)sender {
    [viewOrderPickerCtrl setHidden:YES];
    if (currentOrderChannel >= CH_1 && currentOrderChannel <= CH_18) {
        orderChannel[currentOrderChannel] = ORDER_CH_DELAY_DYN_EQ;
        [self updateOrderLabels:currentOrderChannel];
    }
}

- (IBAction)onBtnEqDynDelayMain:(id)sender {
    [viewOrderPickerMain setHidden:YES];
    if (currentOrderChannel == MAIN) {
        orderMain = ORDER_MAIN_EQ_DYN_DELAY;
        [self updateOrderLabels:currentOrderChannel];
    }
}

- (IBAction)onBtnDynEqDelayMain:(id)sender {
    [viewOrderPickerMain setHidden:YES];
    if (currentOrderChannel == MAIN) {
        orderMain = ORDER_MAIN_DYN_EQ_DELAY;
        [self updateOrderLabels:currentOrderChannel];
    }
}

- (IBAction)onBtnGeqDynDelayMain:(id)sender {
    [viewOrderPickerMain setHidden:YES];
    if (currentOrderChannel == MAIN) {
        orderMain = ORDER_MAIN_GEQ_DYN_DELAY;
        [self updateOrderLabels:currentOrderChannel];
    }
}

- (IBAction)onBtnDynGeqDelayMain:(id)sender {
    [viewOrderPickerMain setHidden:YES];
    if (currentOrderChannel == MAIN) {
        orderMain = ORDER_MAIN_DYN_GEQ_DELAY;
        [self updateOrderLabels:currentOrderChannel];
    }
}

- (IBAction)onBtnEqDynDelayMulti:(id)sender {
    [viewOrderPickerMulti setHidden:YES];
    if (currentOrderChannel >= MT_1 && currentOrderChannel <= MT_4) {
        orderMulti[currentOrderChannel-MT_1] = ORDER_MT_EQ_DYN_DELAY;
        [self updateOrderLabels:currentOrderChannel];
    }
}

- (IBAction)onBtnDynEqDelayMulti:(id)sender {
    [viewOrderPickerMulti setHidden:YES];
    if (currentOrderChannel >= MT_1 && currentOrderChannel <= MT_4) {
        orderMulti[currentOrderChannel-MT_1] = ORDER_MT_DYN_EQ_DELAY;
        [self updateOrderLabels:currentOrderChannel];
    }
}

#pragma mark -
#pragma mark USB audio out event handlers

- (void)clearUsbAudioOutButtons {
    btnUsbAudioOutCh1.selected = NO; btnUsbAudioOutCh2.selected = NO;
    btnUsbAudioOutCh3.selected = NO; btnUsbAudioOutCh4.selected = NO;
    btnUsbAudioOutCh5.selected = NO; btnUsbAudioOutCh6.selected = NO;
    btnUsbAudioOutCh7.selected = NO; btnUsbAudioOutCh8.selected = NO;
    btnUsbAudioOutCh9.selected = NO; btnUsbAudioOutCh10.selected = NO;
    btnUsbAudioOutCh11.selected = NO; btnUsbAudioOutCh12.selected = NO;
    btnUsbAudioOutCh13.selected = NO; btnUsbAudioOutCh14.selected = NO;
    btnUsbAudioOutCh15.selected = NO; btnUsbAudioOutCh16.selected = NO;
    btnUsbAudioOutCh17.selected = NO; btnUsbAudioOutCh18.selected = NO;
    btnUsbAudioOutGp1.selected = NO; btnUsbAudioOutGp2.selected = NO;
    btnUsbAudioOutGp3.selected = NO; btnUsbAudioOutGp4.selected = NO;
    btnUsbAudioOutAux1.selected = NO; btnUsbAudioOutAux2.selected = NO;
    btnUsbAudioOutAux3.selected = NO; btnUsbAudioOutAux4.selected = NO;
    btnUsbAudioOutNull.selected = NO; btnUsbAudioOutMulti1.selected = NO;
    btnUsbAudioOutMulti2.selected = NO; btnUsbAudioOutMulti3.selected = NO;
    btnUsbAudioOutMulti4.selected = NO; btnUsbAudioOutMainL.selected = NO;
    btnUsbAudioOutMainR.selected = NO;
}

- (IBAction)onBtnUsbAudioOutputL:(id)sender {
    _usbSelect = USB_OUTPUT_L;
    [self clearUsbAudioOutButtons];
    switch (_usbAudioOutL) {
        case USB_ASSIGN_CH_1:
            btnUsbAudioOutCh1.selected = YES;
            break;
        case USB_ASSIGN_CH_2:
            btnUsbAudioOutCh2.selected = YES;
            break;
        case USB_ASSIGN_CH_3:
            btnUsbAudioOutCh3.selected = YES;
            break;
        case USB_ASSIGN_CH_4:
            btnUsbAudioOutCh4.selected = YES;
            break;
        case USB_ASSIGN_CH_5:
            btnUsbAudioOutCh5.selected = YES;
            break;
        case USB_ASSIGN_CH_6:
            btnUsbAudioOutCh6.selected = YES;
            break;
        case USB_ASSIGN_CH_7:
            btnUsbAudioOutCh7.selected = YES;
            break;
        case USB_ASSIGN_CH_8:
            btnUsbAudioOutCh8.selected = YES;
            break;
        case USB_ASSIGN_CH_9:
            btnUsbAudioOutCh9.selected = YES;
            break;
        case USB_ASSIGN_CH_10:
            btnUsbAudioOutCh10.selected = YES;
            break;
        case USB_ASSIGN_CH_11:
            btnUsbAudioOutCh11.selected = YES;
            break;
        case USB_ASSIGN_CH_12:
            btnUsbAudioOutCh12.selected = YES;
            break;
        case USB_ASSIGN_CH_13:
            btnUsbAudioOutCh13.selected = YES;
            break;
        case USB_ASSIGN_CH_14:
            btnUsbAudioOutCh14.selected = YES;
            break;
        case USB_ASSIGN_CH_15:
            btnUsbAudioOutCh15.selected = YES;
            break;
        case USB_ASSIGN_CH_16:
            btnUsbAudioOutCh16.selected = YES;
            break;
        case USB_ASSIGN_CH_17:
            btnUsbAudioOutCh17.selected = YES;
            break;
        case USB_ASSIGN_CH_18:
            btnUsbAudioOutCh18.selected = YES;
            break;
        case USB_ASSIGN_NULL:
            btnUsbAudioOutNull.selected = YES;
            break;
        case USB_ASSIGN_AUX_1:
            btnUsbAudioOutAux1.selected = YES;
            break;
        case USB_ASSIGN_AUX_2:
            btnUsbAudioOutAux2.selected = YES;
            break;
        case USB_ASSIGN_AUX_3:
            btnUsbAudioOutAux3.selected = YES;
            break;
        case USB_ASSIGN_AUX_4:
            btnUsbAudioOutAux4.selected = YES;
            break;
        case USB_ASSIGN_GP_1:
            btnUsbAudioOutGp1.selected = YES;
            break;
        case USB_ASSIGN_GP_2:
            btnUsbAudioOutGp2.selected = YES;
            break;
        case USB_ASSIGN_GP_3:
            btnUsbAudioOutGp3.selected = YES;
            break;
        case USB_ASSIGN_GP_4:
            btnUsbAudioOutGp4.selected = YES;
            break;
        case USB_ASSIGN_MULTI_1:
            btnUsbAudioOutMulti1.selected = YES;
            break;
        case USB_ASSIGN_MULTI_2:
            btnUsbAudioOutMulti2.selected = YES;
            break;
        case USB_ASSIGN_MULTI_3:
            btnUsbAudioOutMulti3.selected = YES;
            break;
        case USB_ASSIGN_MULTI_4:
            btnUsbAudioOutMulti4.selected = YES;
            break;
        case USB_ASSIGN_MAIN_L:
            btnUsbAudioOutMainL.selected = YES;
            break;
        case USB_ASSIGN_MAIN_R:
            btnUsbAudioOutMainR.selected = YES;
            break;
        default:
            break;
    }
    [viewUsbAudioOutAssign setFrame:CGRectMake(105, 5, 537, 348)];
    [viewUsbAudioOutAssign setHidden:NO];
}

- (IBAction)onBtnUsbAudioOutoutR:(id)sender {
    _usbSelect = USB_OUTPUT_R;
    [self clearUsbAudioOutButtons];
    switch (_usbAudioOutR) {
        case USB_ASSIGN_CH_1:
            btnUsbAudioOutCh1.selected = YES;
            break;
        case USB_ASSIGN_CH_2:
            btnUsbAudioOutCh2.selected = YES;
            break;
        case USB_ASSIGN_CH_3:
            btnUsbAudioOutCh3.selected = YES;
            break;
        case USB_ASSIGN_CH_4:
            btnUsbAudioOutCh4.selected = YES;
            break;
        case USB_ASSIGN_CH_5:
            btnUsbAudioOutCh5.selected = YES;
            break;
        case USB_ASSIGN_CH_6:
            btnUsbAudioOutCh6.selected = YES;
            break;
        case USB_ASSIGN_CH_7:
            btnUsbAudioOutCh7.selected = YES;
            break;
        case USB_ASSIGN_CH_8:
            btnUsbAudioOutCh8.selected = YES;
            break;
        case USB_ASSIGN_CH_9:
            btnUsbAudioOutCh9.selected = YES;
            break;
        case USB_ASSIGN_CH_10:
            btnUsbAudioOutCh10.selected = YES;
            break;
        case USB_ASSIGN_CH_11:
            btnUsbAudioOutCh11.selected = YES;
            break;
        case USB_ASSIGN_CH_12:
            btnUsbAudioOutCh12.selected = YES;
            break;
        case USB_ASSIGN_CH_13:
            btnUsbAudioOutCh13.selected = YES;
            break;
        case USB_ASSIGN_CH_14:
            btnUsbAudioOutCh14.selected = YES;
            break;
        case USB_ASSIGN_CH_15:
            btnUsbAudioOutCh15.selected = YES;
            break;
        case USB_ASSIGN_CH_16:
            btnUsbAudioOutCh16.selected = YES;
            break;
        case USB_ASSIGN_CH_17:
            btnUsbAudioOutCh17.selected = YES;
            break;
        case USB_ASSIGN_CH_18:
            btnUsbAudioOutCh18.selected = YES;
            break;
        case USB_ASSIGN_NULL:
            btnUsbAudioOutNull.selected = YES;
            break;
        case USB_ASSIGN_AUX_1:
            btnUsbAudioOutAux1.selected = YES;
            break;
        case USB_ASSIGN_AUX_2:
            btnUsbAudioOutAux2.selected = YES;
            break;
        case USB_ASSIGN_AUX_3:
            btnUsbAudioOutAux3.selected = YES;
            break;
        case USB_ASSIGN_AUX_4:
            btnUsbAudioOutAux4.selected = YES;
            break;
        case USB_ASSIGN_GP_1:
            btnUsbAudioOutGp1.selected = YES;
            break;
        case USB_ASSIGN_GP_2:
            btnUsbAudioOutGp2.selected = YES;
            break;
        case USB_ASSIGN_GP_3:
            btnUsbAudioOutGp3.selected = YES;
            break;
        case USB_ASSIGN_GP_4:
            btnUsbAudioOutGp4.selected = YES;
            break;
        case USB_ASSIGN_MULTI_1:
            btnUsbAudioOutMulti1.selected = YES;
            break;
        case USB_ASSIGN_MULTI_2:
            btnUsbAudioOutMulti2.selected = YES;
            break;
        case USB_ASSIGN_MULTI_3:
            btnUsbAudioOutMulti3.selected = YES;
            break;
        case USB_ASSIGN_MULTI_4:
            btnUsbAudioOutMulti4.selected = YES;
            break;
        case USB_ASSIGN_MAIN_L:
            btnUsbAudioOutMainL.selected = YES;
            break;
        case USB_ASSIGN_MAIN_R:
            btnUsbAudioOutMainR.selected = YES;
            break;
        default:
            break;
    }
    [viewUsbAudioOutAssign setFrame:CGRectMake(105, 90, 537, 348)];
    [viewUsbAudioOutAssign setHidden:NO];
}

- (IBAction)onBtnUsbAudioOutCh1:(id)sender {
    [self clearUsbAudioOutButtons];
    btnUsbAudioOutCh1.selected = YES;
    if (_usbSelect == USB_OUTPUT_L) {
        [btnUsbAudioOutputL setTitle:@"CH 1" forState:UIControlStateNormal];
        _usbAudioOutL = USB_ASSIGN_CH_1;
    } else if (_usbSelect == USB_OUTPUT_R) {
        [btnUsbAudioOutputR setTitle:@"CH 1" forState:UIControlStateNormal];
        _usbAudioOutR = USB_ASSIGN_CH_1;
    }
    if (sender) {
        [viewUsbAudioOutAssign setHidden:YES];
        if (_usbSelect == USB_OUTPUT_L) {
            [self sendData:CMD_USB_AUDIO_LOUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutL];
        } else if (_usbSelect == USB_OUTPUT_R) {
            [self sendData:CMD_USB_AUDIO_ROUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutR];
        }
    }
}

- (IBAction)onBtnUsbAudioOutCh2:(id)sender {
    [self clearUsbAudioOutButtons];
    btnUsbAudioOutCh2.selected = YES;
    if (_usbSelect == USB_OUTPUT_L) {
        [btnUsbAudioOutputL setTitle:@"CH 2" forState:UIControlStateNormal];
        _usbAudioOutL = USB_ASSIGN_CH_2;
    } else if (_usbSelect == USB_OUTPUT_R) {
        [btnUsbAudioOutputR setTitle:@"CH 2" forState:UIControlStateNormal];
        _usbAudioOutR = USB_ASSIGN_CH_2;
    }
    if (sender) {
        [viewUsbAudioOutAssign setHidden:YES];
        if (_usbSelect == USB_OUTPUT_L) {
            [self sendData:CMD_USB_AUDIO_LOUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutL];
        } else if (_usbSelect == USB_OUTPUT_R) {
            [self sendData:CMD_USB_AUDIO_ROUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutR];
        }
    }
}

- (IBAction)onBtnUsbAudioOutCh3:(id)sender {
    [self clearUsbAudioOutButtons];
    btnUsbAudioOutCh3.selected = YES;
    if (_usbSelect == USB_OUTPUT_L) {
        [btnUsbAudioOutputL setTitle:@"CH 3" forState:UIControlStateNormal];
        _usbAudioOutL = USB_ASSIGN_CH_3;
    } else if (_usbSelect == USB_OUTPUT_R) {
        [btnUsbAudioOutputR setTitle:@"CH 3" forState:UIControlStateNormal];
        _usbAudioOutR = USB_ASSIGN_CH_3;
    }
    if (sender) {
        [viewUsbAudioOutAssign setHidden:YES];
        if (_usbSelect == USB_OUTPUT_L) {
            [self sendData:CMD_USB_AUDIO_LOUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutL];
        } else if (_usbSelect == USB_OUTPUT_R) {
            [self sendData:CMD_USB_AUDIO_ROUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutR];
        }
    }
}

- (IBAction)onBtnUsbAudioOutCh4:(id)sender {
    [self clearUsbAudioOutButtons];
    btnUsbAudioOutCh4.selected = YES;
    if (_usbSelect == USB_OUTPUT_L) {
        [btnUsbAudioOutputL setTitle:@"CH 4" forState:UIControlStateNormal];
        _usbAudioOutL = USB_ASSIGN_CH_4;
    } else if (_usbSelect == USB_OUTPUT_R) {
        [btnUsbAudioOutputR setTitle:@"CH 4" forState:UIControlStateNormal];
        _usbAudioOutR = USB_ASSIGN_CH_4;
    }
    if (sender) {
        [viewUsbAudioOutAssign setHidden:YES];
        if (_usbSelect == USB_OUTPUT_L) {
            [self sendData:CMD_USB_AUDIO_LOUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutL];
        } else if (_usbSelect == USB_OUTPUT_R) {
            [self sendData:CMD_USB_AUDIO_ROUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutR];
        }
    }
}

- (IBAction)onBtnUsbAudioOutCh5:(id)sender {
    [self clearUsbAudioOutButtons];
    btnUsbAudioOutCh5.selected = YES;
    if (_usbSelect == USB_OUTPUT_L) {
        [btnUsbAudioOutputL setTitle:@"CH 5" forState:UIControlStateNormal];
        _usbAudioOutL = USB_ASSIGN_CH_5;
    } else if (_usbSelect == USB_OUTPUT_R) {
        [btnUsbAudioOutputR setTitle:@"CH 5" forState:UIControlStateNormal];
        _usbAudioOutR = USB_ASSIGN_CH_5;
    }
    if (sender) {
        [viewUsbAudioOutAssign setHidden:YES];
        if (_usbSelect == USB_OUTPUT_L) {
            [self sendData:CMD_USB_AUDIO_LOUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutL];
        } else if (_usbSelect == USB_OUTPUT_R) {
            [self sendData:CMD_USB_AUDIO_ROUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutR];
        }
    }
}

- (IBAction)onBtnUsbAudioOutCh6:(id)sender {
    [self clearUsbAudioOutButtons];
    btnUsbAudioOutCh6.selected = YES;
    if (_usbSelect == USB_OUTPUT_L) {
        [btnUsbAudioOutputL setTitle:@"CH 6" forState:UIControlStateNormal];
        _usbAudioOutL = USB_ASSIGN_CH_6;
    } else if (_usbSelect == USB_OUTPUT_R) {
        [btnUsbAudioOutputR setTitle:@"CH 6" forState:UIControlStateNormal];
        _usbAudioOutR = USB_ASSIGN_CH_6;
    }
    if (sender) {
        [viewUsbAudioOutAssign setHidden:YES];
        if (_usbSelect == USB_OUTPUT_L) {
            [self sendData:CMD_USB_AUDIO_LOUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutL];
        } else if (_usbSelect == USB_OUTPUT_R) {
            [self sendData:CMD_USB_AUDIO_ROUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutR];
        }
    }
}

- (IBAction)onBtnUsbAudioOutCh7:(id)sender {
    [self clearUsbAudioOutButtons];
    btnUsbAudioOutCh7.selected = YES;
    if (_usbSelect == USB_OUTPUT_L) {
        [btnUsbAudioOutputL setTitle:@"CH 7" forState:UIControlStateNormal];
        _usbAudioOutL = USB_ASSIGN_CH_7;
    } else if (_usbSelect == USB_OUTPUT_R) {
        [btnUsbAudioOutputR setTitle:@"CH 7" forState:UIControlStateNormal];
        _usbAudioOutR = USB_ASSIGN_CH_7;
    }
    if (sender) {
        [viewUsbAudioOutAssign setHidden:YES];
        if (_usbSelect == USB_OUTPUT_L) {
            [self sendData:CMD_USB_AUDIO_LOUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutL];
        } else if (_usbSelect == USB_OUTPUT_R) {
            [self sendData:CMD_USB_AUDIO_ROUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutR];
        }
    }
}

- (IBAction)onBtnUsbAudioOutCh8:(id)sender {
    [self clearUsbAudioOutButtons];
    btnUsbAudioOutCh8.selected = YES;
    if (_usbSelect == USB_OUTPUT_L) {
        [btnUsbAudioOutputL setTitle:@"CH 8" forState:UIControlStateNormal];
        _usbAudioOutL = USB_ASSIGN_CH_8;
    } else if (_usbSelect == USB_OUTPUT_R) {
        [btnUsbAudioOutputR setTitle:@"CH 8" forState:UIControlStateNormal];
        _usbAudioOutR = USB_ASSIGN_CH_8;
    }
    if (sender) {
        [viewUsbAudioOutAssign setHidden:YES];
        if (_usbSelect == USB_OUTPUT_L) {
            [self sendData:CMD_USB_AUDIO_LOUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutL];
        } else if (_usbSelect == USB_OUTPUT_R) {
            [self sendData:CMD_USB_AUDIO_ROUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutR];
        }
    }
}

- (IBAction)onBtnUsbAudioOutCh9:(id)sender {
    [self clearUsbAudioOutButtons];
    btnUsbAudioOutCh9.selected = YES;
    if (_usbSelect == USB_OUTPUT_L) {
        [btnUsbAudioOutputL setTitle:@"CH 9" forState:UIControlStateNormal];
        _usbAudioOutL = USB_ASSIGN_CH_9;
    } else if (_usbSelect == USB_OUTPUT_R) {
        [btnUsbAudioOutputR setTitle:@"CH 9" forState:UIControlStateNormal];
        _usbAudioOutR = USB_ASSIGN_CH_9;
    }
    if (sender) {
        [viewUsbAudioOutAssign setHidden:YES];
        if (_usbSelect == USB_OUTPUT_L) {
            [self sendData:CMD_USB_AUDIO_LOUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutL];
        } else if (_usbSelect == USB_OUTPUT_R) {
            [self sendData:CMD_USB_AUDIO_ROUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutR];
        }
    }
}

- (IBAction)onBtnUsbAudioOutCh10:(id)sender {
    [self clearUsbAudioOutButtons];
    btnUsbAudioOutCh10.selected = YES;
    if (_usbSelect == USB_OUTPUT_L) {
        [btnUsbAudioOutputL setTitle:@"CH 10" forState:UIControlStateNormal];
        _usbAudioOutL = USB_ASSIGN_CH_10;
    } else if (_usbSelect == USB_OUTPUT_R) {
        [btnUsbAudioOutputR setTitle:@"CH 10" forState:UIControlStateNormal];
        _usbAudioOutR = USB_ASSIGN_CH_10;
    }
    if (sender) {
        [viewUsbAudioOutAssign setHidden:YES];
        if (_usbSelect == USB_OUTPUT_L) {
            [self sendData:CMD_USB_AUDIO_LOUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutL];
        } else if (_usbSelect == USB_OUTPUT_R) {
            [self sendData:CMD_USB_AUDIO_ROUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutR];
        }
    }
}

- (IBAction)onBtnUsbAudioOutCh11:(id)sender {
    [self clearUsbAudioOutButtons];
    btnUsbAudioOutCh11.selected = YES;
    if (_usbSelect == USB_OUTPUT_L) {
        [btnUsbAudioOutputL setTitle:@"CH 11" forState:UIControlStateNormal];
        _usbAudioOutL = USB_ASSIGN_CH_11;
    } else if (_usbSelect == USB_OUTPUT_R) {
        [btnUsbAudioOutputR setTitle:@"CH 11" forState:UIControlStateNormal];
        _usbAudioOutR = USB_ASSIGN_CH_11;
    }
    if (sender) {
        [viewUsbAudioOutAssign setHidden:YES];
        if (_usbSelect == USB_OUTPUT_L) {
            [self sendData:CMD_USB_AUDIO_LOUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutL];
        } else if (_usbSelect == USB_OUTPUT_R) {
            [self sendData:CMD_USB_AUDIO_ROUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutR];
        }
    }
}

- (IBAction)onBtnUsbAudioOutCh12:(id)sender {
    [self clearUsbAudioOutButtons];
    btnUsbAudioOutCh12.selected = YES;
    if (_usbSelect == USB_OUTPUT_L) {
        [btnUsbAudioOutputL setTitle:@"CH 12" forState:UIControlStateNormal];
        _usbAudioOutL = USB_ASSIGN_CH_12;
    } else if (_usbSelect == USB_OUTPUT_R) {
        [btnUsbAudioOutputR setTitle:@"CH 12" forState:UIControlStateNormal];
        _usbAudioOutR = USB_ASSIGN_CH_12;
    }
    if (sender) {
        [viewUsbAudioOutAssign setHidden:YES];
        if (_usbSelect == USB_OUTPUT_L) {
            [self sendData:CMD_USB_AUDIO_LOUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutL];
        } else if (_usbSelect == USB_OUTPUT_R) {
            [self sendData:CMD_USB_AUDIO_ROUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutR];
        }
    }
}

- (IBAction)onBtnUsbAudioOutCh13:(id)sender {
    [self clearUsbAudioOutButtons];
    btnUsbAudioOutCh13.selected = YES;
    if (_usbSelect == USB_OUTPUT_L) {
        [btnUsbAudioOutputL setTitle:@"CH 13" forState:UIControlStateNormal];
        _usbAudioOutL = USB_ASSIGN_CH_13;
    } else if (_usbSelect == USB_OUTPUT_R) {
        [btnUsbAudioOutputR setTitle:@"CH 13" forState:UIControlStateNormal];
        _usbAudioOutR = USB_ASSIGN_CH_13;
    }
    if (sender) {
        [viewUsbAudioOutAssign setHidden:YES];
        if (_usbSelect == USB_OUTPUT_L) {
            [self sendData:CMD_USB_AUDIO_LOUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutL];
        } else if (_usbSelect == USB_OUTPUT_R) {
            [self sendData:CMD_USB_AUDIO_ROUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutR];
        }
    }
}

- (IBAction)onBtnUsbAudioOutCh14:(id)sender {
    [self clearUsbAudioOutButtons];
    btnUsbAudioOutCh14.selected = YES;
    if (_usbSelect == USB_OUTPUT_L) {
        [btnUsbAudioOutputL setTitle:@"CH 14" forState:UIControlStateNormal];
        _usbAudioOutL = USB_ASSIGN_CH_14;
    } else if (_usbSelect == USB_OUTPUT_R) {
        [btnUsbAudioOutputR setTitle:@"CH 14" forState:UIControlStateNormal];
        _usbAudioOutR = USB_ASSIGN_CH_14;
    }
    if (sender) {
        [viewUsbAudioOutAssign setHidden:YES];
        if (_usbSelect == USB_OUTPUT_L) {
            [self sendData:CMD_USB_AUDIO_LOUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutL];
        } else if (_usbSelect == USB_OUTPUT_R) {
            [self sendData:CMD_USB_AUDIO_ROUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutR];
        }
    }
}

- (IBAction)onBtnUsbAudioOutCh15:(id)sender {
    [self clearUsbAudioOutButtons];
    btnUsbAudioOutCh15.selected = YES;
    if (_usbSelect == USB_OUTPUT_L) {
        [btnUsbAudioOutputL setTitle:@"CH 15" forState:UIControlStateNormal];
        _usbAudioOutL = USB_ASSIGN_CH_15;
    } else if (_usbSelect == USB_OUTPUT_R) {
        [btnUsbAudioOutputR setTitle:@"CH 15" forState:UIControlStateNormal];
        _usbAudioOutR = USB_ASSIGN_CH_15;
    }
    if (sender) {
        [viewUsbAudioOutAssign setHidden:YES];
        if (_usbSelect == USB_OUTPUT_L) {
            [self sendData:CMD_USB_AUDIO_LOUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutL];
        } else if (_usbSelect == USB_OUTPUT_R) {
            [self sendData:CMD_USB_AUDIO_ROUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutR];
        }
    }
}

- (IBAction)onBtnUsbAudioOutCh16:(id)sender {
    [self clearUsbAudioOutButtons];
    btnUsbAudioOutCh16.selected = YES;
    if (_usbSelect == USB_OUTPUT_L) {
        [btnUsbAudioOutputL setTitle:@"CH 16" forState:UIControlStateNormal];
        _usbAudioOutL = USB_ASSIGN_CH_16;
    } else if (_usbSelect == USB_OUTPUT_R) {
        [btnUsbAudioOutputR setTitle:@"CH 16" forState:UIControlStateNormal];
        _usbAudioOutR = USB_ASSIGN_CH_16;
    }
    if (sender) {
        [viewUsbAudioOutAssign setHidden:YES];
        if (_usbSelect == USB_OUTPUT_L) {
            [self sendData:CMD_USB_AUDIO_LOUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutL];
        } else if (_usbSelect == USB_OUTPUT_R) {
            [self sendData:CMD_USB_AUDIO_ROUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutR];
        }
    }
}

- (IBAction)onBtnUsbAudioOutCh17:(id)sender {
    [self clearUsbAudioOutButtons];
    btnUsbAudioOutCh17.selected = YES;
    if (_usbSelect == USB_OUTPUT_L) {
        [btnUsbAudioOutputL setTitle:@"CH 17" forState:UIControlStateNormal];
        _usbAudioOutL = USB_ASSIGN_CH_17;
    } else if (_usbSelect == USB_OUTPUT_R) {
        [btnUsbAudioOutputR setTitle:@"CH 17" forState:UIControlStateNormal];
        _usbAudioOutR = USB_ASSIGN_CH_17;
    }
    if (sender) {
        [viewUsbAudioOutAssign setHidden:YES];
        if (_usbSelect == USB_OUTPUT_L) {
            [self sendData:CMD_USB_AUDIO_LOUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutL];
        } else if (_usbSelect == USB_OUTPUT_R) {
            [self sendData:CMD_USB_AUDIO_ROUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutR];
        }
    }
}

- (IBAction)onBtnUsbAudioOutCh18:(id)sender {
    [self clearUsbAudioOutButtons];
    btnUsbAudioOutCh18.selected = YES;
    if (_usbSelect == USB_OUTPUT_L) {
        [btnUsbAudioOutputL setTitle:@"CH 18" forState:UIControlStateNormal];
        _usbAudioOutL = USB_ASSIGN_CH_18;
    } else if (_usbSelect == USB_OUTPUT_R) {
        [btnUsbAudioOutputR setTitle:@"CH 18" forState:UIControlStateNormal];
        _usbAudioOutR = USB_ASSIGN_CH_18;
    }
    if (sender) {
        [viewUsbAudioOutAssign setHidden:YES];
        if (_usbSelect == USB_OUTPUT_L) {
            [self sendData:CMD_USB_AUDIO_LOUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutL];
        } else if (_usbSelect == USB_OUTPUT_R) {
            [self sendData:CMD_USB_AUDIO_ROUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutR];
        }
    }
}

- (IBAction)onBtnUsbAudioOutAux1:(id)sender {
    [self clearUsbAudioOutButtons];
    btnUsbAudioOutAux1.selected = YES;
    if (_usbSelect == USB_OUTPUT_L) {
        [btnUsbAudioOutputL setTitle:@"AUX 1" forState:UIControlStateNormal];
        _usbAudioOutL = USB_ASSIGN_AUX_1;
    } else if (_usbSelect == USB_OUTPUT_R) {
        [btnUsbAudioOutputR setTitle:@"AUX 1" forState:UIControlStateNormal];
        _usbAudioOutR = USB_ASSIGN_AUX_1;
    }
    if (sender) {
        [viewUsbAudioOutAssign setHidden:YES];
        if (_usbSelect == USB_OUTPUT_L) {
            [self sendData:CMD_USB_AUDIO_LOUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutL];
        } else if (_usbSelect == USB_OUTPUT_R) {
            [self sendData:CMD_USB_AUDIO_ROUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutR];
        }
    }
}

- (IBAction)onBtnUsbAudioOutAux2:(id)sender {
    [self clearUsbAudioOutButtons];
    btnUsbAudioOutAux2.selected = YES;
    if (_usbSelect == USB_OUTPUT_L) {
        [btnUsbAudioOutputL setTitle:@"AUX 2" forState:UIControlStateNormal];
        _usbAudioOutL = USB_ASSIGN_AUX_2;
    } else if (_usbSelect == USB_OUTPUT_R) {
        [btnUsbAudioOutputR setTitle:@"AUX 2" forState:UIControlStateNormal];
        _usbAudioOutR = USB_ASSIGN_AUX_2;
    }
    if (sender) {
        [viewUsbAudioOutAssign setHidden:YES];
        if (_usbSelect == USB_OUTPUT_L) {
            [self sendData:CMD_USB_AUDIO_LOUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutL];
        } else if (_usbSelect == USB_OUTPUT_R) {
            [self sendData:CMD_USB_AUDIO_ROUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutR];
        }
    }
}

- (IBAction)onBtnUsbAudioOutAux3:(id)sender {
    [self clearUsbAudioOutButtons];
    btnUsbAudioOutAux3.selected = YES;
    if (_usbSelect == USB_OUTPUT_L) {
        [btnUsbAudioOutputL setTitle:@"AUX 3" forState:UIControlStateNormal];
        _usbAudioOutL = USB_ASSIGN_AUX_3;
    } else if (_usbSelect == USB_OUTPUT_R) {
        [btnUsbAudioOutputR setTitle:@"AUX 3" forState:UIControlStateNormal];
        _usbAudioOutR = USB_ASSIGN_AUX_3;
    }
    if (sender) {
        [viewUsbAudioOutAssign setHidden:YES];
        if (_usbSelect == USB_OUTPUT_L) {
            [self sendData:CMD_USB_AUDIO_LOUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutL];
        } else if (_usbSelect == USB_OUTPUT_R) {
            [self sendData:CMD_USB_AUDIO_ROUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutR];
        }
    }
}

- (IBAction)onBtnUsbAudioOutAux4:(id)sender {
    [self clearUsbAudioOutButtons];
    btnUsbAudioOutAux4.selected = YES;
    if (_usbSelect == USB_OUTPUT_L) {
        [btnUsbAudioOutputL setTitle:@"AUX 4" forState:UIControlStateNormal];
        _usbAudioOutL = USB_ASSIGN_AUX_4;
    } else if (_usbSelect == USB_OUTPUT_R) {
        [btnUsbAudioOutputR setTitle:@"AUX 4" forState:UIControlStateNormal];
        _usbAudioOutR = USB_ASSIGN_AUX_4;
    }
    if (sender) {
        [viewUsbAudioOutAssign setHidden:YES];
        if (_usbSelect == USB_OUTPUT_L) {
            [self sendData:CMD_USB_AUDIO_LOUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutL];
        } else if (_usbSelect == USB_OUTPUT_R) {
            [self sendData:CMD_USB_AUDIO_ROUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutR];
        }
    }
}

- (IBAction)onBtnUsbAudioOutGp1:(id)sender {
    [self clearUsbAudioOutButtons];
    btnUsbAudioOutGp1.selected = YES;
    if (_usbSelect == USB_OUTPUT_L) {
        [btnUsbAudioOutputL setTitle:@"GP 1" forState:UIControlStateNormal];
        _usbAudioOutL = USB_ASSIGN_GP_1;
    } else if (_usbSelect == USB_OUTPUT_R) {
        [btnUsbAudioOutputR setTitle:@"GP 1" forState:UIControlStateNormal];
        _usbAudioOutR = USB_ASSIGN_GP_1;
    }
    if (sender) {
        [viewUsbAudioOutAssign setHidden:YES];
        if (_usbSelect == USB_OUTPUT_L) {
            [self sendData:CMD_USB_AUDIO_LOUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutL];
        } else if (_usbSelect == USB_OUTPUT_R) {
            [self sendData:CMD_USB_AUDIO_ROUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutR];
        }
    }
}

- (IBAction)onBtnUsbAudioOutGp2:(id)sender {
    [self clearUsbAudioOutButtons];
    btnUsbAudioOutGp2.selected = YES;
    if (_usbSelect == USB_OUTPUT_L) {
        [btnUsbAudioOutputL setTitle:@"GP 2" forState:UIControlStateNormal];
        _usbAudioOutL = USB_ASSIGN_GP_2;
    } else if (_usbSelect == USB_OUTPUT_R) {
        [btnUsbAudioOutputR setTitle:@"GP 2" forState:UIControlStateNormal];
        _usbAudioOutR = USB_ASSIGN_GP_2;
    }
    if (sender) {
        [viewUsbAudioOutAssign setHidden:YES];
        if (_usbSelect == USB_OUTPUT_L) {
            [self sendData:CMD_USB_AUDIO_LOUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutL];
        } else if (_usbSelect == USB_OUTPUT_R) {
            [self sendData:CMD_USB_AUDIO_ROUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutR];
        }
    }
}

- (IBAction)onBtnUsbAudioOutGp3:(id)sender {
    [self clearUsbAudioOutButtons];
    btnUsbAudioOutGp3.selected = YES;
    if (_usbSelect == USB_OUTPUT_L) {
        [btnUsbAudioOutputL setTitle:@"GP 3" forState:UIControlStateNormal];
        _usbAudioOutL = USB_ASSIGN_GP_3;
    } else if (_usbSelect == USB_OUTPUT_R) {
        [btnUsbAudioOutputR setTitle:@"GP 3" forState:UIControlStateNormal];
        _usbAudioOutR = USB_ASSIGN_GP_3;
    }
    if (sender) {
        [viewUsbAudioOutAssign setHidden:YES];
        if (_usbSelect == USB_OUTPUT_L) {
            [self sendData:CMD_USB_AUDIO_LOUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutL];
        } else if (_usbSelect == USB_OUTPUT_R) {
            [self sendData:CMD_USB_AUDIO_ROUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutR];
        }
    }
}

- (IBAction)onBtnUsbAudioOutGp4:(id)sender {
    [self clearUsbAudioOutButtons];
    btnUsbAudioOutGp4.selected = YES;
    if (_usbSelect == USB_OUTPUT_L) {
        [btnUsbAudioOutputL setTitle:@"GP 4" forState:UIControlStateNormal];
        _usbAudioOutL = USB_ASSIGN_GP_4;
    } else if (_usbSelect == USB_OUTPUT_R) {
        [btnUsbAudioOutputR setTitle:@"GP 4" forState:UIControlStateNormal];
        _usbAudioOutR = USB_ASSIGN_GP_4;
    }
    if (sender) {
        [viewUsbAudioOutAssign setHidden:YES];
        if (_usbSelect == USB_OUTPUT_L) {
            [self sendData:CMD_USB_AUDIO_LOUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutL];
        } else if (_usbSelect == USB_OUTPUT_R) {
            [self sendData:CMD_USB_AUDIO_ROUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutR];
        }
    }
}

- (IBAction)onBtnUsbAudioOutNull:(id)sender {
    [self clearUsbAudioOutButtons];
    btnUsbAudioOutNull.selected = YES;
    if (_usbSelect == USB_OUTPUT_L) {
        [btnUsbAudioOutputL setTitle:@"NULL" forState:UIControlStateNormal];
        _usbAudioOutL = USB_ASSIGN_NULL;
    } else if (_usbSelect == USB_OUTPUT_R) {
        [btnUsbAudioOutputR setTitle:@"NULL" forState:UIControlStateNormal];
        _usbAudioOutR = USB_ASSIGN_NULL;
    }
    if (sender) {
        [viewUsbAudioOutAssign setHidden:YES];
        if (_usbSelect == USB_OUTPUT_L) {
            [self sendData:CMD_USB_AUDIO_LOUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutL];
        } else if (_usbSelect == USB_OUTPUT_R) {
            [self sendData:CMD_USB_AUDIO_ROUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutR];
        }
    }
}

- (IBAction)onBtnUsbAudioOutMulti1:(id)sender {
    [self clearUsbAudioOutButtons];
    btnUsbAudioOutMulti1.selected = YES;
    if (_usbSelect == USB_OUTPUT_L) {
        [btnUsbAudioOutputL setTitle:@"MULTI 1" forState:UIControlStateNormal];
        _usbAudioOutL = USB_ASSIGN_MULTI_1;
    } else if (_usbSelect == USB_OUTPUT_R) {
        [btnUsbAudioOutputR setTitle:@"MULTI 1" forState:UIControlStateNormal];
        _usbAudioOutR = USB_ASSIGN_MULTI_1;
    }
    if (sender) {
        [viewUsbAudioOutAssign setHidden:YES];
        if (_usbSelect == USB_OUTPUT_L) {
            [self sendData:CMD_USB_AUDIO_LOUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutL];
        } else if (_usbSelect == USB_OUTPUT_R) {
            [self sendData:CMD_USB_AUDIO_ROUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutR];
        }
    }
}

- (IBAction)onBtnUsbAudioOutMulti2:(id)sender {
    [self clearUsbAudioOutButtons];
    btnUsbAudioOutMulti2.selected = YES;
    if (_usbSelect == USB_OUTPUT_L) {
        [btnUsbAudioOutputL setTitle:@"MULTI 2" forState:UIControlStateNormal];
        _usbAudioOutL = USB_ASSIGN_MULTI_2;
    } else if (_usbSelect == USB_OUTPUT_R) {
        [btnUsbAudioOutputR setTitle:@"MULTI 2" forState:UIControlStateNormal];
        _usbAudioOutR = USB_ASSIGN_MULTI_2;
    }
    if (sender) {
        [viewUsbAudioOutAssign setHidden:YES];
        if (_usbSelect == USB_OUTPUT_L) {
            [self sendData:CMD_USB_AUDIO_LOUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutL];
        } else if (_usbSelect == USB_OUTPUT_R) {
            [self sendData:CMD_USB_AUDIO_ROUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutR];
        }
    }
}

- (IBAction)onBtnUsbAudioOutMulti3:(id)sender {
    [self clearUsbAudioOutButtons];
    btnUsbAudioOutMulti3.selected = YES;
    if (_usbSelect == USB_OUTPUT_L) {
        [btnUsbAudioOutputL setTitle:@"MULTI 3" forState:UIControlStateNormal];
        _usbAudioOutL = USB_ASSIGN_MULTI_3;
    } else if (_usbSelect == USB_OUTPUT_R) {
        [btnUsbAudioOutputR setTitle:@"MULTI 3" forState:UIControlStateNormal];
        _usbAudioOutR = USB_ASSIGN_MULTI_3;
    }
    if (sender) {
        [viewUsbAudioOutAssign setHidden:YES];
        if (_usbSelect == USB_OUTPUT_L) {
            [self sendData:CMD_USB_AUDIO_LOUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutL];
        } else if (_usbSelect == USB_OUTPUT_R) {
            [self sendData:CMD_USB_AUDIO_ROUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutR];
        }
    }
}

- (IBAction)onBtnUsbAudioOutMulti4:(id)sender {
    [self clearUsbAudioOutButtons];
    btnUsbAudioOutMulti4.selected = YES;
    if (_usbSelect == USB_OUTPUT_L) {
        [btnUsbAudioOutputL setTitle:@"MULTI 4" forState:UIControlStateNormal];
        _usbAudioOutL = USB_ASSIGN_MULTI_4;
    } else if (_usbSelect == USB_OUTPUT_R) {
        [btnUsbAudioOutputR setTitle:@"MULTI 4" forState:UIControlStateNormal];
        _usbAudioOutR = USB_ASSIGN_MULTI_4;
    }
    if (sender) {
        [viewUsbAudioOutAssign setHidden:YES];
        if (_usbSelect == USB_OUTPUT_L) {
            [self sendData:CMD_USB_AUDIO_LOUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutL];
        } else if (_usbSelect == USB_OUTPUT_R) {
            [self sendData:CMD_USB_AUDIO_ROUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutR];
        }
    }
}

- (IBAction)onBtnUsbAudioOutMainL:(id)sender {
    [self clearUsbAudioOutButtons];
    btnUsbAudioOutMainL.selected = YES;
    if (_usbSelect == USB_OUTPUT_L) {
        [btnUsbAudioOutputL setTitle:@"MAIN L" forState:UIControlStateNormal];
        _usbAudioOutL = USB_ASSIGN_MAIN_L;
    } else if (_usbSelect == USB_OUTPUT_R) {
        [btnUsbAudioOutputR setTitle:@"MAIN L" forState:UIControlStateNormal];
        _usbAudioOutR = USB_ASSIGN_MAIN_L;
    }
    if (sender) {
        [viewUsbAudioOutAssign setHidden:YES];
        if (_usbSelect == USB_OUTPUT_L) {
            [self sendData:CMD_USB_AUDIO_LOUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutL];
        } else if (_usbSelect == USB_OUTPUT_R) {
            [self sendData:CMD_USB_AUDIO_ROUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutR];
        }
    }
}

- (IBAction)onBtnUsbAudioOutMainR:(id)sender {
    [self clearUsbAudioOutButtons];
    btnUsbAudioOutMainR.selected = YES;
    if (_usbSelect == USB_OUTPUT_L) {
        [btnUsbAudioOutputL setTitle:@"MAIN R" forState:UIControlStateNormal];
        _usbAudioOutL = USB_ASSIGN_MAIN_R;
    } else if (_usbSelect == USB_OUTPUT_R) {
        [btnUsbAudioOutputR setTitle:@"MAIN R" forState:UIControlStateNormal];
        _usbAudioOutR = USB_ASSIGN_MAIN_R;
    }
    if (sender) {
        [viewUsbAudioOutAssign setHidden:YES];
        if (_usbSelect == USB_OUTPUT_L) {
            [self sendData:CMD_USB_AUDIO_LOUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutL];
        } else if (_usbSelect == USB_OUTPUT_R) {
            [self sendData:CMD_USB_AUDIO_ROUT_ASSIGN commandType:DSP_WRITE dspId:DSP_6 value:_usbAudioOutR];
        }
    }
}

- (void)setFileSceneMode:(SceneMode)sceneMode sceneChannel:(SceneChannel)sceneChannel {
    _currentSceneMode = sceneMode;
    _currentSceneChannel = sceneChannel;
    switch (_currentSceneMode) {
        case SCENE_MODE_SCENE:
            [imgViewScenesBg setImage:[UIImage imageNamed:@"basemap-31.png"]];
            [btnViewScenesInit setHidden:NO];
            break;
        case SCENE_MODE_DYN_GATE:
        case SCENE_MODE_DYN_EXP:
        case SCENE_MODE_DYN_COMP:
        case SCENE_MODE_DYN_LIM:
            [imgViewScenesBg setImage:[UIImage imageNamed:@"basemap-36.png"]];
            [btnViewScenesInit setHidden:YES];
            break;
        case SCENE_MODE_PEQ:
        case SCENE_MODE_GEQ:
            [imgViewScenesBg setImage:[UIImage imageNamed:@"basemap-35.png"]];
            [btnViewScenesInit setHidden:YES];
            break;
        case SCENE_MODE_EFFECT_1:
        case SCENE_MODE_EFFECT_2:
            [imgViewScenesBg setImage:[UIImage imageNamed:@"basemap-15.png"]];
            [btnViewScenesInit setHidden:YES];
            break;
        default:
            break;
    }
}

@end
