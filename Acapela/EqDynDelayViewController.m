//
//  EqDynDelayViewController.m
//  Acapela
//
//  Created by Kevin Phua on 13/8/31.
//  Copyright (c) 2013年 Kevin Phua. All rights reserved.
//

#import "EqDynDelayViewController.h"
#import "AppDelegate.h"
#import "ViewController.h"
#import "Global.h"
#import "JEProgressView.h"

@interface EqDynDelayViewController ()
{
    BOOL _sliderCurrentLock;
    BOOL _orderLock;
}
@end

@implementation EqDynDelayViewController
{
    ViewController* viewController;
    enum ViewPage currentViewPage;
    
    int     _ch1Ctrl, _ch2Ctrl, _ch3Ctrl, _ch4Ctrl, _ch5Ctrl, _ch6Ctrl, _ch7Ctrl, _ch8Ctrl;
    int     _ch9Ctrl, _ch10Ctrl, _ch11Ctrl, _ch12Ctrl, _ch13Ctrl, _ch14Ctrl, _ch15Ctrl, _ch16Ctrl;
    int     _ch17Ctrl, _ch18Ctrl, _ch17AudioSetting, _ch18AudioSetting;
    int     _aux1SendPrePost, _aux2SendPrePost, _aux3SendPrePost, _aux4SendPrePost;
    int     _ch1_16AssignGp1, _ch1_16AssignGp2, _ch1_16AssignGp3, _ch1_16AssignGp4;
    int     _ch1_16ToMainCtrl, _ch1_16OnOff, _ch1_16MeterPrePost, _ch1_16SoloCtrlOnOff;
    int     _multi1Ctrl, _multi2Ctrl, _multi3Ctrl, _multi4Ctrl;
    int     _multi_1_4MeterPrePost;
    int     _mainLRCtrl, _chToMainCtrl, _gpToMainCtrl, _mainMeterPrePost;
    int     _ctrlRmChannel, _ctrlRmAuxGp, _mainToCtrlRm, _soloCtrlChannel, _soloSafeCtrlChannel;
    int     _soloCtrlAuxGp, _soloSafeCtrlAuxGp, _digitalInOutCtrl, _gpToMain;
    int     _order;
}

@synthesize currentChannel,currentChannelLabel;
@synthesize imgBackground,viewTop,viewEqPreview,imgEqPreview,viewDynPreview,imgDynPreview;
@synthesize btnEq,btnDyn,btnDynGate,btnDynExpander,btnDynComp,btnDynLimiter;
@synthesize btnDelay,knobDelay,cpvDelay,lblDelay,imgOrder1,imgOrder2,imgOrder3;
@synthesize btnInv,lblPan,sliderPan,meterPan,meterEqOut,meterEqOutR,meterDynOut,meterDynOutR;
@synthesize viewBottomChannel,knobAux1,cpvAux1,lblAux1,knobAux2;
@synthesize cpvAux2,lblAux2,knobAux3,cpvAux3,lblAux3,knobAux4,cpvAux4,lblAux4;
@synthesize btnChannelAux1PostPre,btnChannelAux2PostPre,btnChannelAux3PostPre,btnChannelAux4PostPre;
@synthesize btnChannelGp1OnOff,btnChannelGp2OnOff,btnChannelGp3OnOff,btnChannelGp4OnOff,btnChannelToMainOnOff;
@synthesize viewBottomMulti,btnMultiAux1OnOff,btnMultiAux2OnOff,btnMultiAux3OnOff,btnMultiAux4OnOff;
@synthesize btnMultiGp1OnOff,btnMultiGp2OnOff,btnMultiGp3OnOff,btnMultiGp4OnOff;
@synthesize viewBottomMain,btnMainCh1OnOff,btnMainCh2OnOff,btnMainCh3OnOff,btnMainCh4OnOff,btnMainCh5OnOff;
@synthesize btnMainCh6OnOff,btnMainCh7OnOff,btnMainCh8OnOff,btnMainCh9OnOff,btnMainCh10OnOff;
@synthesize btnMainCh11OnOff,btnMainCh12OnOff,btnMainCh13OnOff,btnMainCh14OnOff,btnMainCh15OnOff,btnMainCh16OnOff;
@synthesize btnMainCh17OnOff,btnMainCh18OnOff,btnMainGp1OnOff,btnMainGp2OnOff,btnMainGp3OnOff,btnMainGp4OnOff;
@synthesize viewCtrlRm,btnGlobalPfl,btnGlobalAfl,btnGlobalClearAll,btnSoloSafe,btnSolo,btnSoloPflAfl,btnMainPflAfl,imgMainPflAflArrow;
@synthesize btnChannel1,btnChannel2,btnChannel3,btnChannel4,btnChannel5,btnChannel6,btnChannel7,btnChannel8;
@synthesize btnChannel9,btnChannel10,btnChannel11,btnChannel12,btnChannel13,btnChannel14,btnChannel15,btnChannel16,btnChannel17,btnChannel18;
@synthesize btnEfx1,btnEfx2,btnAux1,btnAux2,btnAux3,btnAux4,btnGp1,btnGp2,btnGp3,btnGp4;
@synthesize lblChannel1Safe,lblChannel2Safe,lblChannel3Safe,lblChannel4Safe,lblChannel5Safe,lblChannel6Safe,lblChannel7Safe,lblChannel8Safe;
@synthesize lblChannel9Safe,lblChannel10Safe,lblChannel11Safe,lblChannel12Safe,lblChannel13Safe,lblChannel14Safe,lblChannel15Safe,lblChannel16Safe;
@synthesize lblChannel17Safe,lblChannel18Safe,lblEfx1Safe,lblEfx2Safe,lblAux1Safe,lblAux2Safe,lblAux3Safe,lblAux4Safe;
@synthesize lblGp1Safe,lblGp2Safe,lblGp3Safe,lblGp4Safe;
@synthesize imgChannel1Solo,imgChannel2Solo,imgChannel3Solo,imgChannel4Solo,imgChannel5Solo,imgChannel6Solo,imgChannel7Solo,imgChannel8Solo;
@synthesize imgChannel9Solo,imgChannel10Solo,imgChannel11Solo,imgChannel12Solo,imgChannel13Solo,imgChannel14Solo,imgChannel15Solo,imgChannel16Solo;
@synthesize imgChannel17Solo,imgChannel18Solo,imgEfx1Solo,imgEfx2Solo,imgAux1Solo,imgAux2Solo,imgAux3Solo,imgAux4Solo;
@synthesize imgGp1Solo,imgGp2Solo,imgGp3Solo,imgGp4Solo;
@synthesize btnCurrentSolo,btnCurrentOn,btnChannelSelectDown,btnChannelSelectUp;
@synthesize btnCurrentMeter,txtCurrentChannel;
@synthesize lblCurrentChannel,sliderCurrent,meterCurrentL,meterCurrentR,lblSliderValue;
@synthesize viewOrder,btnOrderEqDynDelay,btnOrderEqDelayDyn,btnOrderDynEqDelay;
@synthesize btnOrderDynDelayEq,btnOrderDelayEqDyn,btnOrderDelayDynEq;
@synthesize viewOrderMain,btnOrderMainEqDynDelay,btnOrderMainDynEqDelay,btnOrderMainGeqDynDelay,btnOrderMainDynGeqDelay;
@synthesize viewOrderMulti,btnOrderMultiEqDynDelay,btnOrderMultiDynEqDelay;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    viewController = (ViewController *)appDelegate.window.rootViewController;
    
    _sliderCurrentLock = NO;
    _orderLock = NO;

    // Do any additional setup after loading the view from its nib.
    [btnGlobalClearAll.titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
    [btnGlobalClearAll.titleLabel setNumberOfLines:0];
    [btnGlobalClearAll.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [btnGlobalClearAll setTitle:@"SOLO\nCLEAR\nALL" forState:UIControlStateNormal];
    [btnSoloSafe.titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
    [btnSoloSafe.titleLabel setNumberOfLines:0];
    [btnSoloSafe.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [btnSoloSafe setTitle:@"SOLO\nSAFE" forState:UIControlStateNormal];
    [btnMainPflAfl.titleLabel setLineBreakMode:NSLineBreakByWordWrapping];
    [btnMainPflAfl.titleLabel setNumberOfLines:0];
    [btnMainPflAfl.titleLabel setTextAlignment:NSTextAlignmentCenter];
    [btnMainPflAfl setTitle:@"MAIN\nPFL" forState:UIControlStateNormal];
    [btnMainPflAfl setTitle:@"MAIN\nAFL" forState:UIControlStateSelected];

    // Add slider
    UIImage *faderImage = [UIImage imageNamed:@"fader-1.png"];
    
    sliderCurrent = [[UISlider alloc] initWithFrame:CGRectMake(844, 295, 301, 180)];
    sliderCurrent.transform = CGAffineTransformRotate(sliderCurrent.transform, 270.0/180*M_PI);
    [sliderCurrent addTarget:self action:@selector(onSliderCurrentBegin:) forControlEvents:UIControlEventTouchDown];
    [sliderCurrent addTarget:self action:@selector(onSliderCurrentEnd:) forControlEvents:UIControlEventTouchUpInside];
    [sliderCurrent addTarget:self action:@selector(onSliderCurrentEnd:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderCurrent addTarget:self action:@selector(onSliderCurrentAction:) forControlEvents:UIControlEventValueChanged];
    [sliderCurrent setBackgroundColor:[UIColor clearColor]];
    [sliderCurrent setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderCurrent setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderCurrent setThumbImage:faderImage forState:UIControlStateNormal];
    sliderCurrent.minimumValue = 0.0;
    sliderCurrent.maximumValue = FADER_MAX_VALUE;
    sliderCurrent.continuous = YES;
    sliderCurrent.value = FADER_DEFAULT_VALUE;
    [self.view addSubview:sliderCurrent];
    
    // Add slider meters
    UIImage *progressImage = [[UIImage imageNamed:@"meter-1.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0,0,0,0)];

    meterCurrentL = [[JEProgressView alloc] initWithFrame:CGRectMake(800, 375, 275, 4)];
    meterCurrentL.transform = CGAffineTransformRotate(meterCurrentL.transform, 270.0/180*M_PI);
    [meterCurrentL setProgressImage:progressImage];
    [meterCurrentL setTrackImage:[UIImage alloc]];
    [meterCurrentL setProgress:0.0];
    [self.view addSubview:meterCurrentL];
    
    meterCurrentR = [[JEProgressView alloc] initWithFrame:CGRectMake(810, 375, 275, 4)];
    meterCurrentR.transform = CGAffineTransformRotate(meterCurrentR.transform, 270.0/180*M_PI);
    [meterCurrentR setProgressImage:progressImage];
    [meterCurrentR setTrackImage:[UIImage alloc]];
    [meterCurrentR setProgress:0.0];
    [self.view addSubview:meterCurrentR];
    
    // Add EQ & DYN out meters
    UIImage *outImage = [[UIImage imageNamed:@"meter-24.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0,0,0,0)];
    
    meterEqOut = [[JEProgressView alloc] initWithFrame:CGRectMake(255, 138, 127, 10)];
    meterEqOut.transform = CGAffineTransformRotate(meterEqOut.transform, 270.0/180*M_PI);
    [meterEqOut setProgressImage:outImage];
    [meterEqOut setTrackImage:[UIImage alloc]];
    [meterEqOut setProgress:0.0];
    [self.view addSubview:meterEqOut];

    meterEqOutR = [[JEProgressView alloc] initWithFrame:CGRectMake(275, 138, 127, 10)];
    meterEqOutR.transform = CGAffineTransformRotate(meterEqOutR.transform, 270.0/180*M_PI);
    [meterEqOutR setProgressImage:outImage];
    [meterEqOutR setTrackImage:[UIImage alloc]];
    [meterEqOutR setProgress:0.0];
    [self.view addSubview:meterEqOutR];
    
    meterDynOut = [[JEProgressView alloc] initWithFrame:CGRectMake(510, 138, 127, 10)];
    meterDynOut.transform = CGAffineTransformRotate(meterDynOut.transform, 270.0/180*M_PI);
    [meterDynOut setProgressImage:outImage];
    [meterDynOut setTrackImage:[UIImage alloc]];
    [meterDynOut setProgress:0.0];
    [self.view addSubview:meterDynOut];
    
    meterDynOutR = [[JEProgressView alloc] initWithFrame:CGRectMake(530, 138, 127, 10)];
    meterDynOutR.transform = CGAffineTransformRotate(meterDynOutR.transform, 270.0/180*M_PI);
    [meterDynOutR setProgressImage:outImage];
    [meterDynOutR setTrackImage:[UIImage alloc]];
    [meterDynOutR setProgress:0.0];
    [self.view addSubview:meterDynOutR];
    
    // Setup delay knob
    knobDelay.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobDelay.scalingFactor = 2.0f;
	knobDelay.maximumValue = DELAY_MAX_VALUE;
	knobDelay.minimumValue = DELAY_MIN_VALUE;
	knobDelay.value = DELAY_MIN_VALUE;
	knobDelay.defaultValue = knobDelay.value;
	knobDelay.resetsToDefault = YES;
	knobDelay.backgroundColor = [UIColor clearColor];
	[knobDelay setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobDelay.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobDelay addTarget:self action:@selector(knobDelayDidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvDelay = [[DACircularProgressView alloc] initWithFrame:CGRectMake(736.0f, 81.0f, 65.0f, 65.0f)];
    [viewTop addSubview:cpvDelay];
    [viewTop sendSubviewToBack:cpvDelay];
    
    // Setup aux knobs
    knobAux1.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobAux1.scalingFactor = 2.0f;
	knobAux1.maximumValue = GAIN_MAX_VALUE;
	knobAux1.minimumValue = GAIN_MIN_VALUE;
	knobAux1.value = GAIN_MIN_VALUE;
	knobAux1.defaultValue = knobAux1.value;
	knobAux1.resetsToDefault = YES;
	knobAux1.backgroundColor = [UIColor clearColor];
	[knobAux1 setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobAux1.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobAux1 addTarget:self action:@selector(knobAux1DidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvAux1 = [[DACircularProgressView alloc] initWithFrame:CGRectMake(59.0f, 161.0f, 65.0f, 65.0f)];
    [viewBottomChannel addSubview:cpvAux1];
    [viewBottomChannel sendSubviewToBack:cpvAux1];

    knobAux2.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobAux2.scalingFactor = 2.0f;
	knobAux2.maximumValue = GAIN_MAX_VALUE;
	knobAux2.minimumValue = GAIN_MIN_VALUE;
	knobAux2.value = GAIN_MIN_VALUE;
	knobAux2.defaultValue = knobAux2.value;
	knobAux2.resetsToDefault = YES;
	knobAux2.backgroundColor = [UIColor clearColor];
	[knobAux2 setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobAux2.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobAux2 addTarget:self action:@selector(knobAux2DidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvAux2 = [[DACircularProgressView alloc] initWithFrame:CGRectMake(159.0f, 161.0f, 65.0f, 65.0f)];
    [viewBottomChannel addSubview:cpvAux2];
    [viewBottomChannel sendSubviewToBack:cpvAux2];
    
    knobAux3.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobAux3.scalingFactor = 2.0f;
	knobAux3.maximumValue = GAIN_MAX_VALUE;
	knobAux3.minimumValue = GAIN_MIN_VALUE;
	knobAux3.value = GAIN_MIN_VALUE;
	knobAux3.defaultValue = knobAux3.value;
	knobAux3.resetsToDefault = YES;
	knobAux3.backgroundColor = [UIColor clearColor];
	[knobAux3 setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobAux3.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobAux3 addTarget:self action:@selector(knobAux3DidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvAux3 = [[DACircularProgressView alloc] initWithFrame:CGRectMake(259.0f, 161.0f, 65.0f, 65.0f)];
    [viewBottomChannel addSubview:cpvAux3];
    [viewBottomChannel sendSubviewToBack:cpvAux3];
    
    knobAux4.interactionStyle = MHRotaryKnobInteractionStyleSliderVertical;
	knobAux4.scalingFactor = 2.0f;
	knobAux4.maximumValue = GAIN_MAX_VALUE;
	knobAux4.minimumValue = GAIN_MIN_VALUE;
	knobAux4.value = GAIN_MIN_VALUE;
	knobAux4.defaultValue = knobAux4.value;
	knobAux4.resetsToDefault = YES;
	knobAux4.backgroundColor = [UIColor clearColor];
	[knobAux4 setKnobImage:[UIImage imageNamed:@"knob-1.png"] forState:UIControlStateNormal];
	knobAux4.knobImageCenter = CGPointMake(32.0f, 32.0f);
	[knobAux4 addTarget:self action:@selector(knobAux4DidChange:) forControlEvents:UIControlEventValueChanged];
    
    cpvAux4 = [[DACircularProgressView alloc] initWithFrame:CGRectMake(360.0f, 161.0f, 65.0f, 65.0f)];
    [viewBottomChannel addSubview:cpvAux4];
    [viewBottomChannel sendSubviewToBack:cpvAux4];
    
    // Add slider meters
    UIImage *panProgressImage = [[UIImage imageNamed:@"pan-6.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0,0,0,0)];
    
    meterPan = [[JEProgressView alloc] initWithFrame:CGRectMake(764, 263, 82, 82)];
    [meterPan setTransform:CGAffineTransformMake(-1, 0, 0, 1, 0, 0)];
    [meterPan setProgressImage:panProgressImage];
    [meterPan setTrackImage:[UIImage alloc]];
    [meterPan setProgress:0.0];
    [viewTop addSubview:meterPan];

    // Add PAN sliders
    UIImage *panImage = [UIImage imageNamed:@"Slider-1.png"];
    
    sliderPan = [[UISlider alloc] initWithFrame:CGRectMake(764, 224, 86, 86)];
    [sliderPan addTarget:self action:@selector(onSliderPanAction:) forControlEvents:UIControlEventValueChanged];
    [sliderPan addTarget:self action:@selector(onSliderPanStart:) forControlEvents:UIControlEventTouchDown];
    [sliderPan addTarget:self action:@selector(onSliderPanEnd:) forControlEvents:UIControlEventTouchUpInside];
    [sliderPan addTarget:self action:@selector(onSliderPanEnd:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderPan setBackgroundColor:[UIColor clearColor]];
    [sliderPan setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderPan setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderPan setThumbImage:panImage forState:UIControlStateNormal];
    sliderPan.minimumValue = 0.0;
    sliderPan.maximumValue = PAN_MAX_VALUE;
    sliderPan.continuous = YES;
    sliderPan.value = PAN_CENTER_VALUE;
    [viewTop addSubview:sliderPan];
    
    // Init values
    [self refreshOrderView:ORDER_CH_EQ_DYN_DELAY sendValue:NO];
    btnSoloPflAfl.selected = YES;
    
    [self refreshEqPreview];
    [self refreshDynPreview];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)touchesBegan:(NSSet*)touches withEvent:(UIEvent*)event
{
    // Detect touches for EQ/DYN views
    UITouch *touch = [touches anyObject];
    CGPoint location = [touch locationInView:self.view];
    UIView *view = [self.view hitTest:location withEvent:nil];
    
    if (viewEqPreview == view) {
        /*
        switch (currentChannel) {
            case CH_1:
            case CH_2:
            case CH_3:
            case CH_4:
            case CH_5:
            case CH_6:
            case CH_7:
            case CH_8:
            case CH_9:
            case CH_10:
            case CH_11:
            case CH_12:
            case CH_13:
            case CH_14:
            case CH_15:
            case CH_16:
            case CH_17:
            case CH_18:
                [viewController setSendPage:EQ_PAGE reserve2:0];
                break;
            case MT_1:
            case MT_2:
            case MT_3:
            case MT_4:
                [viewController setSendPage:EQ_PAGE reserve2:1];
                break;
            case MAIN:
                [viewController setSendPage:EQ_PAGE reserve2:2];
                break;
            default:
                break;
        }
         */
        [viewController showGeqPeqDialog:currentChannel];
    } else if (viewDynPreview == view) {
        [viewController setSendPage:DYN_PAGE reserve2:0];
        [viewController showDynDialog:currentChannel];
    }
}

- (void)updateDynButtons
{
    // Set DYN button states
    if (!btnDyn.selected) {
        [btnDynGate setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
        [btnDynExpander setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
        [btnDynComp setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
        [btnDynLimiter setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
    }
}

- (void)refreshCtrlViews:(SInt16)value
{
    btnDyn.selected = (value & 0x02) >> 1;
    btnDelay.selected = (value & 0x04) >> 2;
    //bit3=EQ1 on/off bit4=EQ2 on/off, bit5=EQ3 on/off, bit6= EQ4 on/off
    btnEq.selected = (value & 0x80) >> 7;
    btnDynGate.selected = (value & 0x0100) >> 8;
    btnDynExpander.selected = (value & 0x0200) >> 9;
    btnDynComp.selected = (value & 0x0400) >> 10;
    btnDynLimiter.selected = (value & 0x0800) >> 11;
    btnInv.selected = (value & 0x1000) >> 12;
    
    [self updateDynButtons];
    
    if (!_orderLock) {
        int order = (value & 0xE000) >> 13;
        _order = order;
        switch (currentViewPage) {
            case VIEW_PAGE_CH_1_8:
            case VIEW_PAGE_CH_9_16:
            case VIEW_PAGE_USB_AUDIO:
                [self refreshOrderView:_order sendValue:NO];
                break;
            case VIEW_PAGE_MAIN:
                [self refreshOrderMainView:_order sendValue:NO];
                break;
            case VIEW_PAGE_MT_1_4:
                [self refreshOrderMultiView:_order sendValue:NO];
                break;
            default:
                break;
        }
    }
}

- (void)refreshOrderView:(int)order sendValue:(BOOL)sendValue {
    switch (order) {
        case ORDER_CH_EQ_DYN_DELAY:
            [imgOrder1 setImage:[UIImage imageNamed:@"eq.png"]];
            [imgOrder2 setImage:[UIImage imageNamed:@"dyn.png"]];
            [imgOrder3 setImage:[UIImage imageNamed:@"delay.png"]];
            [imgOrder1 setFrame:CGRectMake(242, 253, 66, 39)];
            [imgOrder2 setFrame:CGRectMake(299, 253, 78, 39)];
            [imgOrder3 setFrame:CGRectMake(365, 253, 112, 39)];
            [btnOrderEqDynDelay setBackgroundColor:[UIColor blueColor]];
            [btnOrderEqDelayDyn setBackgroundColor:[UIColor clearColor]];
            [btnOrderDynEqDelay setBackgroundColor:[UIColor clearColor]];
            [btnOrderDynDelayEq setBackgroundColor:[UIColor clearColor]];
            [btnOrderDelayEqDyn setBackgroundColor:[UIColor clearColor]];
            [btnOrderDelayDynEq setBackgroundColor:[UIColor clearColor]];
            break;
        case ORDER_CH_EQ_DELAY_DYN:
            [imgOrder1 setImage:[UIImage imageNamed:@"eq.png"]];
            [imgOrder2 setImage:[UIImage imageNamed:@"delay.png"]];
            [imgOrder3 setImage:[UIImage imageNamed:@"dyn.png"]];
            [imgOrder1 setFrame:CGRectMake(242, 253, 66, 39)];
            [imgOrder2 setFrame:CGRectMake(299, 253, 112, 39)];
            [imgOrder3 setFrame:CGRectMake(402, 253, 78, 39)];
            [btnOrderEqDynDelay setBackgroundColor:[UIColor clearColor]];
            [btnOrderEqDelayDyn setBackgroundColor:[UIColor blueColor]];
            [btnOrderDynEqDelay setBackgroundColor:[UIColor clearColor]];
            [btnOrderDynDelayEq setBackgroundColor:[UIColor clearColor]];
            [btnOrderDelayEqDyn setBackgroundColor:[UIColor clearColor]];
            [btnOrderDelayDynEq setBackgroundColor:[UIColor clearColor]];
            break;
        case ORDER_CH_DYN_EQ_DELAY:
            [imgOrder1 setImage:[UIImage imageNamed:@"dyn.png"]];
            [imgOrder2 setImage:[UIImage imageNamed:@"eq.png"]];
            [imgOrder3 setImage:[UIImage imageNamed:@"delay.png"]];
            [imgOrder1 setFrame:CGRectMake(242, 253, 78, 39)];
            [imgOrder2 setFrame:CGRectMake(311, 253, 66, 39)];
            [imgOrder3 setFrame:CGRectMake(368, 253, 112, 39)];
            [btnOrderEqDynDelay setBackgroundColor:[UIColor clearColor]];
            [btnOrderEqDelayDyn setBackgroundColor:[UIColor clearColor]];
            [btnOrderDynEqDelay setBackgroundColor:[UIColor blueColor]];
            [btnOrderDynDelayEq setBackgroundColor:[UIColor clearColor]];
            [btnOrderDelayEqDyn setBackgroundColor:[UIColor clearColor]];
            [btnOrderDelayDynEq setBackgroundColor:[UIColor clearColor]];
            break;
        case ORDER_CH_DYN_DELAY_EQ:
            [imgOrder1 setImage:[UIImage imageNamed:@"dyn.png"]];
            [imgOrder2 setImage:[UIImage imageNamed:@"delay.png"]];
            [imgOrder3 setImage:[UIImage imageNamed:@"eq.png"]];
            [imgOrder1 setFrame:CGRectMake(242, 253, 78, 39)];
            [imgOrder2 setFrame:CGRectMake(311, 253, 112, 39)];
            [imgOrder3 setFrame:CGRectMake(414, 253, 66, 39)];
            [btnOrderEqDynDelay setBackgroundColor:[UIColor clearColor]];
            [btnOrderEqDelayDyn setBackgroundColor:[UIColor clearColor]];
            [btnOrderDynEqDelay setBackgroundColor:[UIColor clearColor]];
            [btnOrderDynDelayEq setBackgroundColor:[UIColor blueColor]];
            [btnOrderDelayEqDyn setBackgroundColor:[UIColor clearColor]];
            [btnOrderDelayDynEq setBackgroundColor:[UIColor clearColor]];
            break;
        case ORDER_CH_DELAY_EQ_DYN:
            [imgOrder1 setImage:[UIImage imageNamed:@"delay.png"]];
            [imgOrder2 setImage:[UIImage imageNamed:@"eq.png"]];
            [imgOrder3 setImage:[UIImage imageNamed:@"dyn.png"]];
            [imgOrder1 setFrame:CGRectMake(242, 253, 112, 39)];
            [imgOrder2 setFrame:CGRectMake(345, 253, 66, 39)];
            [imgOrder3 setFrame:CGRectMake(402, 253, 78, 39)];
            [btnOrderEqDynDelay setBackgroundColor:[UIColor clearColor]];
            [btnOrderEqDelayDyn setBackgroundColor:[UIColor clearColor]];
            [btnOrderDynEqDelay setBackgroundColor:[UIColor clearColor]];
            [btnOrderDynDelayEq setBackgroundColor:[UIColor clearColor]];
            [btnOrderDelayEqDyn setBackgroundColor:[UIColor blueColor]];
            [btnOrderDelayDynEq setBackgroundColor:[UIColor clearColor]];
            break;
        case ORDER_CH_DELAY_DYN_EQ:
            [imgOrder1 setImage:[UIImage imageNamed:@"delay.png"]];
            [imgOrder2 setImage:[UIImage imageNamed:@"dyn.png"]];
            [imgOrder3 setImage:[UIImage imageNamed:@"eq.png"]];
            [imgOrder1 setFrame:CGRectMake(242, 253, 112, 39)];
            [imgOrder2 setFrame:CGRectMake(345, 253, 78, 39)];
            [imgOrder3 setFrame:CGRectMake(414, 253, 66, 39)];
            [btnOrderEqDynDelay setBackgroundColor:[UIColor clearColor]];
            [btnOrderEqDelayDyn setBackgroundColor:[UIColor clearColor]];
            [btnOrderDynEqDelay setBackgroundColor:[UIColor clearColor]];
            [btnOrderDynDelayEq setBackgroundColor:[UIColor clearColor]];
            [btnOrderDelayEqDyn setBackgroundColor:[UIColor clearColor]];
            [btnOrderDelayDynEq setBackgroundColor:[UIColor blueColor]];
            break;
        default:
            break;
    }
    
    if (sendValue) {
        switch (currentChannel) {
            case CH_1:
                [Global setOrder:&_ch1Ctrl value:order];
                [viewController sendData:CMD_CH1_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch1Ctrl];
                break;
            case CH_2:
                [Global setOrder:&_ch2Ctrl value:order];
                [viewController sendData:CMD_CH2_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch2Ctrl];
                break;
            case CH_3:
                [Global setOrder:&_ch3Ctrl value:order];
                [viewController sendData:CMD_CH3_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch3Ctrl];
                break;
            case CH_4:
                [Global setOrder:&_ch4Ctrl value:order];
                [viewController sendData:CMD_CH4_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch4Ctrl];
                break;
            case CH_5:
                [Global setOrder:&_ch5Ctrl value:order];
                [viewController sendData:CMD_CH5_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch5Ctrl];
                break;
            case CH_6:
                [Global setOrder:&_ch6Ctrl value:order];
                [viewController sendData:CMD_CH6_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch6Ctrl];
                break;
            case CH_7:
                [Global setOrder:&_ch7Ctrl value:order];
                [viewController sendData:CMD_CH7_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch7Ctrl];
                break;
            case CH_8:
                [Global setOrder:&_ch8Ctrl value:order];
                [viewController sendData:CMD_CH8_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch8Ctrl];
                break;
            case CH_9:
                [Global setOrder:&_ch9Ctrl value:order];
                [viewController sendData:CMD_CH9_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch9Ctrl];
                break;
            case CH_10:
                [Global setOrder:&_ch10Ctrl value:order];
                [viewController sendData:CMD_CH10_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch10Ctrl];
                break;
            case CH_11:
                [Global setOrder:&_ch11Ctrl value:order];
                [viewController sendData:CMD_CH11_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch11Ctrl];
                break;
            case CH_12:
                [Global setOrder:&_ch12Ctrl value:order];
                [viewController sendData:CMD_CH12_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch12Ctrl];
                break;
            case CH_13:
                [Global setOrder:&_ch13Ctrl value:order];
                [viewController sendData:CMD_CH13_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch13Ctrl];
                break;
            case CH_14:
                [Global setOrder:&_ch14Ctrl value:order];
                [viewController sendData:CMD_CH14_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch14Ctrl];
                break;
            case CH_15:
                [Global setOrder:&_ch15Ctrl value:order];
                [viewController sendData:CMD_CH15_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch15Ctrl];
                break;
            case CH_16:
                [Global setOrder:&_ch16Ctrl value:order];
                [viewController sendData:CMD_CH16_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch16Ctrl];
                break;
            case CH_17:
                [Global setOrder:&_ch17Ctrl value:order];
                [viewController sendData:CMD_CH17_CTRL commandType:DSP_WRITE dspId:DSP_6 value:_ch17Ctrl];
                break;
            case CH_18:
                [Global setOrder:&_ch18Ctrl value:order];
                [viewController sendData:CMD_CH18_CTRL commandType:DSP_WRITE dspId:DSP_6 value:_ch18Ctrl];
                break;
            default:
                break;
        }
    }
}

- (void)refreshOrderMainView:(int)order sendValue:(BOOL)sendValue {
    switch (order) {
        case ORDER_MAIN_EQ_DYN_DELAY:
            [imgOrder1 setImage:[UIImage imageNamed:@"eq.png"]];
            [imgOrder2 setImage:[UIImage imageNamed:@"dyn.png"]];
            [imgOrder3 setImage:[UIImage imageNamed:@"delay.png"]];
            [imgOrder1 setFrame:CGRectMake(242, 253, 66, 39)];
            [imgOrder2 setFrame:CGRectMake(299, 253, 78, 39)];
            [imgOrder3 setFrame:CGRectMake(365, 253, 112, 39)];
            [btnOrderEqDynDelay setBackgroundColor:[UIColor blueColor]];
            [btnOrderEqDelayDyn setBackgroundColor:[UIColor clearColor]];
            [btnOrderDynEqDelay setBackgroundColor:[UIColor clearColor]];
            [btnOrderDynDelayEq setBackgroundColor:[UIColor clearColor]];
            [btnOrderDelayEqDyn setBackgroundColor:[UIColor clearColor]];
            [btnOrderDelayDynEq setBackgroundColor:[UIColor clearColor]];
            [imgBackground setImage:[UIImage imageNamed:@"basemap-47.png"]];
            [self didFinishEditing];
            break;
        case ORDER_MAIN_DYN_EQ_DELAY:
            [imgOrder1 setImage:[UIImage imageNamed:@"dyn.png"]];
            [imgOrder2 setImage:[UIImage imageNamed:@"eq.png"]];
            [imgOrder3 setImage:[UIImage imageNamed:@"delay.png"]];
            [imgOrder1 setFrame:CGRectMake(242, 253, 78, 39)];
            [imgOrder2 setFrame:CGRectMake(311, 253, 66, 39)];
            [imgOrder3 setFrame:CGRectMake(368, 253, 112, 39)];
            [btnOrderEqDynDelay setBackgroundColor:[UIColor clearColor]];
            [btnOrderEqDelayDyn setBackgroundColor:[UIColor clearColor]];
            [btnOrderDynEqDelay setBackgroundColor:[UIColor blueColor]];
            [btnOrderDynDelayEq setBackgroundColor:[UIColor clearColor]];
            [btnOrderDelayEqDyn setBackgroundColor:[UIColor clearColor]];
            [btnOrderDelayDynEq setBackgroundColor:[UIColor clearColor]];
            [imgBackground setImage:[UIImage imageNamed:@"basemap-47.png"]];
            [self didFinishEditing];
            break;
        case ORDER_MAIN_GEQ_DYN_DELAY:
            [imgOrder1 setImage:[UIImage imageNamed:@"geq.png"]];
            [imgOrder2 setImage:[UIImage imageNamed:@"dyn.png"]];
            [imgOrder3 setImage:[UIImage imageNamed:@"delay.png"]];
            [imgOrder1 setFrame:CGRectMake(242, 253, 78, 39)];
            [imgOrder2 setFrame:CGRectMake(311, 253, 78, 39)];
            [imgOrder3 setFrame:CGRectMake(377, 253, 112, 39)];
            [btnOrderEqDynDelay setBackgroundColor:[UIColor blueColor]];
            [btnOrderEqDelayDyn setBackgroundColor:[UIColor clearColor]];
            [btnOrderDynEqDelay setBackgroundColor:[UIColor clearColor]];
            [btnOrderDynDelayEq setBackgroundColor:[UIColor clearColor]];
            [btnOrderDelayEqDyn setBackgroundColor:[UIColor clearColor]];
            [btnOrderDelayDynEq setBackgroundColor:[UIColor clearColor]];
            [imgBackground setImage:[UIImage imageNamed:@"basemap-49.png"]];
            [self didFinishEditing];
            break;
        case ORDER_MAIN_DYN_GEQ_DELAY:
            [imgOrder1 setImage:[UIImage imageNamed:@"dyn.png"]];
            [imgOrder2 setImage:[UIImage imageNamed:@"geq.png"]];
            [imgOrder3 setImage:[UIImage imageNamed:@"delay.png"]];
            [imgOrder1 setFrame:CGRectMake(242, 253, 78, 39)];
            [imgOrder2 setFrame:CGRectMake(311, 253, 78, 39)];
            [imgOrder3 setFrame:CGRectMake(380, 253, 112, 39)];
            [btnOrderEqDynDelay setBackgroundColor:[UIColor clearColor]];
            [btnOrderEqDelayDyn setBackgroundColor:[UIColor clearColor]];
            [btnOrderDynEqDelay setBackgroundColor:[UIColor blueColor]];
            [btnOrderDynDelayEq setBackgroundColor:[UIColor clearColor]];
            [btnOrderDelayEqDyn setBackgroundColor:[UIColor clearColor]];
            [btnOrderDelayDynEq setBackgroundColor:[UIColor clearColor]];
            [imgBackground setImage:[UIImage imageNamed:@"basemap-49.png"]];
            [self didFinishEditing];
            break;
        default:
            break;
    }
    
    if (sendValue) {
        switch (currentChannel) {
            case MAIN:
                [Global setOrder:&_mainLRCtrl value:order];
                [viewController sendData:CMD_MAIN_LR_CTRL commandType:DSP_WRITE dspId:DSP_6 value:_mainLRCtrl];
                break;
            default:
                break;
        }
    }
}

- (void)refreshOrderMultiView:(int)order sendValue:(BOOL)sendValue {
    switch (order) {
        case ORDER_MT_EQ_DYN_DELAY:
            [imgOrder1 setImage:[UIImage imageNamed:@"eq.png"]];
            [imgOrder2 setImage:[UIImage imageNamed:@"dyn.png"]];
            [imgOrder3 setImage:[UIImage imageNamed:@"delay.png"]];
            [imgOrder1 setFrame:CGRectMake(242, 253, 66, 39)];
            [imgOrder2 setFrame:CGRectMake(299, 253, 78, 39)];
            [imgOrder3 setFrame:CGRectMake(365, 253, 112, 39)];
            [btnOrderEqDynDelay setBackgroundColor:[UIColor blueColor]];
            [btnOrderEqDelayDyn setBackgroundColor:[UIColor clearColor]];
            [btnOrderDynEqDelay setBackgroundColor:[UIColor clearColor]];
            [btnOrderDynDelayEq setBackgroundColor:[UIColor clearColor]];
            [btnOrderDelayEqDyn setBackgroundColor:[UIColor clearColor]];
            [btnOrderDelayDynEq setBackgroundColor:[UIColor clearColor]];
            break;
        case ORDER_MT_DYN_EQ_DELAY:
            [imgOrder1 setImage:[UIImage imageNamed:@"dyn.png"]];
            [imgOrder2 setImage:[UIImage imageNamed:@"eq.png"]];
            [imgOrder3 setImage:[UIImage imageNamed:@"delay.png"]];
            [imgOrder1 setFrame:CGRectMake(242, 253, 78, 39)];
            [imgOrder2 setFrame:CGRectMake(311, 253, 66, 39)];
            [imgOrder3 setFrame:CGRectMake(368, 253, 112, 39)];
            [btnOrderEqDynDelay setBackgroundColor:[UIColor clearColor]];
            [btnOrderEqDelayDyn setBackgroundColor:[UIColor clearColor]];
            [btnOrderDynEqDelay setBackgroundColor:[UIColor blueColor]];
            [btnOrderDynDelayEq setBackgroundColor:[UIColor clearColor]];
            [btnOrderDelayEqDyn setBackgroundColor:[UIColor clearColor]];
            [btnOrderDelayDynEq setBackgroundColor:[UIColor clearColor]];
            break;
        default:
            break;
    }
    
    if (sendValue) {
        switch (currentChannel) {
            case MT_1:
                [Global setOrder:&_multi1Ctrl value:order];
                [viewController sendData:CMD_MULTI_1_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi1Ctrl];
                break;
            case MT_2:
                [Global setOrder:&_multi2Ctrl value:order];
                [viewController sendData:CMD_MULTI_2_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi2Ctrl];
                break;
            case MT_3:
                [Global setOrder:&_multi3Ctrl value:order];
                [viewController sendData:CMD_MULTI_3_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi3Ctrl];
                break;
            case MT_4:
                [Global setOrder:&_multi4Ctrl value:order];
                [viewController sendData:CMD_MULTI_4_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi4Ctrl];
                break;
            default:
                break;
        }
    }
}

- (void)processChannelViewPage:(ViewPageChannelPacket *)pkt
{
    _ch1Ctrl = pkt->channel_1_ctrl;
    _ch2Ctrl = pkt->channel_2_ctrl;
    _ch3Ctrl = pkt->channel_3_ctrl;
    _ch4Ctrl = pkt->channel_4_ctrl;
    _ch5Ctrl = pkt->channel_5_ctrl;
    _ch6Ctrl = pkt->channel_6_ctrl;
    _ch7Ctrl = pkt->channel_7_ctrl;
    _ch8Ctrl = pkt->channel_8_ctrl;
    _ch9Ctrl = pkt->channel_9_ctrl;
    _ch10Ctrl = pkt->channel_10_ctrl;
    _ch11Ctrl = pkt->channel_11_ctrl;
    _ch12Ctrl = pkt->channel_12_ctrl;
    _ch13Ctrl = pkt->channel_13_ctrl;
    _ch14Ctrl = pkt->channel_14_ctrl;
    _ch15Ctrl = pkt->channel_15_ctrl;
    _ch16Ctrl = pkt->channel_16_ctrl;
    _aux1SendPrePost = pkt->aux_1_send_pre_post;
    _aux2SendPrePost = pkt->aux_2_send_pre_post;
    _aux3SendPrePost = pkt->aux_3_send_pre_post;
    _aux4SendPrePost = pkt->aux_4_send_pre_post;
    _ch1_16AssignGp1 = pkt->ch_1_16_assign_gp_1;
    _ch1_16AssignGp2 = pkt->ch_1_16_assign_gp_2;
    _ch1_16AssignGp3 = pkt->ch_1_16_assign_gp_3;
    _ch1_16AssignGp4 = pkt->ch_1_16_assign_gp_4;
    _ch1_16SoloCtrlOnOff = pkt->ch_1_16_solo_ctrl_on_off;
    _ch1_16ToMainCtrl = pkt->ch_1_16_to_main_ctrl;
    _ch1_16OnOff = pkt->ch_1_16_on_off;
    _ch1_16MeterPrePost = pkt->ch_1_16_meter_pre_post;
    
    switch (currentChannel) {
        case CH_1:
            meterEqOut.progress = ((float)pkt->channel_1_eq_meter_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterDynOut.progress = ((float)pkt->channel_1_dyn_meter_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            if (![viewController sendCommandExists:CMD_CH1_CTRL dspId:DSP_1]) {
                [self refreshCtrlViews:pkt->channel_1_ctrl];
            }
            if (![viewController sendCommandExists:CMD_CH1_DELAY_TIME dspId:DSP_1]) {
                knobDelay.value = [Global delayToKnobValue:pkt->channel_1_delay_time];
                [self knobDelayDidChange:nil];
            }
            sliderPan.value = pkt->channel_1_pan_level + PAN_CENTER_VALUE;
            [self onSliderPanAction:nil];
            BOOL aux1PostPre = pkt->aux_1_send_pre_post & 0x01;
            if (aux1PostPre != btnChannelAux1PostPre.selected && ![viewController sendCommandExists:CMD_AUX1_SEND dspId:DSP_5]) {
                btnChannelAux1PostPre.selected = aux1PostPre;
            }
            BOOL aux2PostPre = pkt->aux_2_send_pre_post & 0x01;
            if (aux2PostPre != btnChannelAux2PostPre.selected && ![viewController sendCommandExists:CMD_AUX2_SEND dspId:DSP_5]) {
                btnChannelAux2PostPre.selected = aux2PostPre;
            }
            BOOL aux3PostPre = pkt->aux_3_send_pre_post & 0x01;
            if (aux3PostPre != btnChannelAux3PostPre.selected && ![viewController sendCommandExists:CMD_AUX3_SEND dspId:DSP_5]) {
                btnChannelAux3PostPre.selected = aux3PostPre;
            }
            BOOL aux4PostPre = pkt->aux_4_send_pre_post & 0x01;
            if (aux4PostPre != btnChannelAux4PostPre.selected && ![viewController sendCommandExists:CMD_AUX4_SEND dspId:DSP_5]) {
                btnChannelAux4PostPre.selected = aux4PostPre;
            }
            knobAux1.value = pkt->aux_1_ch_1_fader_level;
            [self knobAux1DidChange:nil];
            knobAux2.value = pkt->aux_2_ch_1_fader_level;
            [self knobAux2DidChange:nil];
            knobAux3.value = pkt->aux_3_ch_1_fader_level;
            [self knobAux3DidChange:nil];
            knobAux4.value = pkt->aux_4_ch_1_fader_level;
            [self knobAux4DidChange:nil];
            BOOL gp1OnOff = pkt->ch_1_16_assign_gp_1 & 0x01;
            if (gp1OnOff != btnChannelGp1OnOff.selected && ![viewController sendCommandExists:CMD_GP1_ASSIGN dspId:DSP_5]) {
                btnChannelGp1OnOff.selected = gp1OnOff;
            }
            BOOL gp2OnOff = pkt->ch_1_16_assign_gp_2 & 0x01;
            if (gp2OnOff != btnChannelGp2OnOff.selected && ![viewController sendCommandExists:CMD_GP2_ASSIGN dspId:DSP_5]) {
                btnChannelGp2OnOff.selected = gp2OnOff;
            }
            BOOL gp3OnOff = pkt->ch_1_16_assign_gp_3 & 0x01;
            if (gp3OnOff != btnChannelGp3OnOff.selected && ![viewController sendCommandExists:CMD_GP3_ASSIGN dspId:DSP_5]) {
                btnChannelGp3OnOff.selected = gp3OnOff;
            }
            BOOL gp4OnOff = pkt->ch_1_16_assign_gp_4 & 0x01;
            if (gp4OnOff != btnChannelGp4OnOff.selected && ![viewController sendCommandExists:CMD_GP4_ASSIGN dspId:DSP_5]) {
                btnChannelGp4OnOff.selected = gp4OnOff;
            }
            BOOL toMainOnOff = pkt->ch_1_16_to_main_ctrl & 0x01;
            if (toMainOnOff != btnChannelToMainOnOff.selected && ![viewController sendCommandExists:CMD_CH_TO_MAIN_CTRL dspId:DSP_5]) {
                btnChannelToMainOnOff.selected = toMainOnOff;
            }
            if (!_sliderCurrentLock) {
                sliderCurrent.value = pkt->ch_1_fader_level;
                [lblSliderValue setText:[Global getSliderGain:sliderCurrent.value appendDb:YES]];
            }
            BOOL soloSafe = pkt->ch_1_16_solo_safe_ctrl_on_off & 0x01;
            if (soloSafe) {
                [btnCurrentSolo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
                [btnCurrentSolo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
            } else {
                [btnCurrentSolo setTitle:@"SOLO" forState:UIControlStateNormal];
                [btnCurrentSolo setTitle:@"SOLO" forState:UIControlStateSelected];
            }
            BOOL currentSolo = pkt->ch_1_16_solo_ctrl_on_off & 0x01;
            if (currentSolo != btnCurrentSolo.selected && ![viewController sendCommandExists:CMD_SOLO_CTRL dspId:DSP_5]) {
                btnCurrentSolo.selected = currentSolo;
            }
            BOOL currentOn = pkt->ch_1_16_on_off & 0x01;
            if (currentOn != btnCurrentOn.selected && ![viewController sendCommandExists:CMD_CH_ON_OFF dspId:DSP_5]) {
                btnCurrentOn.selected = currentOn;
            }
            BOOL currentMeter = pkt->ch_1_16_meter_pre_post & 0x01;
            if (currentMeter != btnCurrentMeter.selected && ![viewController sendCommandExists:CMD_CH_METER dspId:DSP_5]) {
                btnCurrentMeter.selected = currentMeter;
            }
            // Update DYN status
            if (btnDyn.selected) {
                // Check DYN status, green if ACTIVE, yellow if NOP
                if (pkt->channel_1_4_dyn_active & 0x01) {
                    [btnDynGate setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynGate setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->channel_1_4_dyn_active & 0x02) >> 1) {
                    [btnDynExpander setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynExpander setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->channel_1_4_dyn_active & 0x04) >> 2) {
                    [btnDynComp setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynComp setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->channel_1_4_dyn_active & 0x08) >> 3) {
                    [btnDynLimiter setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynLimiter setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
            }
            break;
        case CH_2:
            meterEqOut.progress = ((float)pkt->channel_2_eq_meter_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterDynOut.progress = ((float)pkt->channel_2_dyn_meter_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            if (![viewController sendCommandExists:CMD_CH2_CTRL dspId:DSP_1]) {
                [self refreshCtrlViews:pkt->channel_2_ctrl];
            }
            if (![viewController sendCommandExists:CMD_CH2_DELAY_TIME dspId:DSP_1]) {
                knobDelay.value = [Global delayToKnobValue:pkt->channel_2_delay_time];
                [self knobDelayDidChange:nil];
            }
            sliderPan.value = pkt->channel_2_pan_level + PAN_CENTER_VALUE;
            [self onSliderPanAction:nil];
            aux1PostPre = (pkt->aux_1_send_pre_post & 0x02) >> 1;
            if (aux1PostPre != btnChannelAux1PostPre.selected && ![viewController sendCommandExists:CMD_AUX1_SEND dspId:DSP_5]) {
                btnChannelAux1PostPre.selected = aux1PostPre;
            }
            aux2PostPre = (pkt->aux_2_send_pre_post & 0x02) >> 1;
            if (aux2PostPre != btnChannelAux2PostPre.selected && ![viewController sendCommandExists:CMD_AUX2_SEND dspId:DSP_5]) {
                btnChannelAux2PostPre.selected = aux2PostPre;
            }
            aux3PostPre = (pkt->aux_3_send_pre_post & 0x02) >> 1;
            if (aux3PostPre != btnChannelAux3PostPre.selected && ![viewController sendCommandExists:CMD_AUX3_SEND dspId:DSP_5]) {
                btnChannelAux3PostPre.selected = aux3PostPre;
            }
            aux4PostPre = (pkt->aux_4_send_pre_post & 0x02) >> 1;
            if (aux4PostPre != btnChannelAux4PostPre.selected && ![viewController sendCommandExists:CMD_AUX4_SEND dspId:DSP_5]) {
                btnChannelAux4PostPre.selected = aux4PostPre;
            }
            knobAux1.value = pkt->aux_1_ch_2_fader_level;
            [self knobAux1DidChange:nil];
            knobAux2.value = pkt->aux_2_ch_2_fader_level;
            [self knobAux2DidChange:nil];
            knobAux3.value = pkt->aux_3_ch_2_fader_level;
            [self knobAux3DidChange:nil];
            knobAux4.value = pkt->aux_4_ch_2_fader_level;
            [self knobAux4DidChange:nil];
            gp1OnOff = (pkt->ch_1_16_assign_gp_1 & 0x02) >> 1;
            if (gp1OnOff != btnChannelGp1OnOff.selected && ![viewController sendCommandExists:CMD_GP1_ASSIGN dspId:DSP_5]) {
                btnChannelGp1OnOff.selected = gp1OnOff;
            }
            gp2OnOff = (pkt->ch_1_16_assign_gp_2 & 0x02) >> 1;
            if (gp2OnOff != btnChannelGp2OnOff.selected && ![viewController sendCommandExists:CMD_GP2_ASSIGN dspId:DSP_5]) {
                btnChannelGp2OnOff.selected = gp2OnOff;
            }
            gp3OnOff = (pkt->ch_1_16_assign_gp_3 & 0x02) >> 1;
            if (gp3OnOff != btnChannelGp3OnOff.selected && ![viewController sendCommandExists:CMD_GP3_ASSIGN dspId:DSP_5]) {
                btnChannelGp3OnOff.selected = gp3OnOff;
            }
            gp4OnOff = (pkt->ch_1_16_assign_gp_4 & 0x02) >> 1;
            if (gp4OnOff != btnChannelGp4OnOff.selected && ![viewController sendCommandExists:CMD_GP4_ASSIGN dspId:DSP_5]) {
                btnChannelGp4OnOff.selected = gp4OnOff;
            }
            toMainOnOff = (pkt->ch_1_16_to_main_ctrl & 0x02) >> 1;
            if (toMainOnOff != btnChannelToMainOnOff.selected && ![viewController sendCommandExists:CMD_CH_TO_MAIN_CTRL dspId:DSP_5]) {
                btnChannelToMainOnOff.selected = toMainOnOff;
            }
            if (!_sliderCurrentLock) {
                sliderCurrent.value = pkt->ch_2_fader_level;
                [lblSliderValue setText:[Global getSliderGain:sliderCurrent.value appendDb:YES]];
            }
            soloSafe = (pkt->ch_1_16_solo_safe_ctrl_on_off & 0x02) >> 1;
            if (soloSafe) {
                [btnCurrentSolo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
                [btnCurrentSolo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
            } else {
                [btnCurrentSolo setTitle:@"SOLO" forState:UIControlStateNormal];
                [btnCurrentSolo setTitle:@"SOLO" forState:UIControlStateSelected];
            }
            currentSolo = (pkt->ch_1_16_solo_ctrl_on_off & 0x02) >> 1;
            if (currentSolo != btnCurrentSolo.selected && ![viewController sendCommandExists:CMD_SOLO_CTRL dspId:DSP_5]) {
                btnCurrentSolo.selected = currentSolo;
            }
            currentOn = (pkt->ch_1_16_on_off & 0x02) >> 1;
            if (currentOn != btnCurrentOn.selected && ![viewController sendCommandExists:CMD_CH_ON_OFF dspId:DSP_5]) {
                btnCurrentOn.selected = currentOn;
            }
            currentMeter = (pkt->ch_1_16_meter_pre_post & 0x02) >> 1;
            if (currentMeter != btnCurrentMeter.selected && ![viewController sendCommandExists:CMD_CH_METER dspId:DSP_5]) {
                btnCurrentMeter.selected = currentMeter;
            }
            // Update DYN status
            if (btnDyn.selected) {
                // Check DYN status, green if ACTIVE, yellow if NOP
                if ((pkt->channel_1_4_dyn_active & 0x10) >> 4) {
                    [btnDynGate setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynGate setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->channel_1_4_dyn_active & 0x20) >> 5) {
                    [btnDynExpander setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynExpander setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->channel_1_4_dyn_active & 0x40) >> 6) {
                    [btnDynComp setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynComp setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->channel_1_4_dyn_active & 0x80) >> 7) {
                    [btnDynLimiter setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynLimiter setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
            }
            break;
        case CH_3:
            meterEqOut.progress = ((float)pkt->channel_3_eq_meter_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterDynOut.progress = ((float)pkt->channel_3_dyn_meter_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            if (![viewController sendCommandExists:CMD_CH3_CTRL dspId:DSP_1]) {
                [self refreshCtrlViews:pkt->channel_3_ctrl];
            }
            if (![viewController sendCommandExists:CMD_CH3_DELAY_TIME dspId:DSP_1]) {
                knobDelay.value = [Global delayToKnobValue:pkt->channel_3_delay_time];
                [self knobDelayDidChange:nil];
            }
            sliderPan.value = pkt->channel_3_pan_level + PAN_CENTER_VALUE;
            [self onSliderPanAction:nil];
            aux1PostPre = (pkt->aux_1_send_pre_post & 0x04) >> 2;
            if (aux1PostPre != btnChannelAux1PostPre.selected && ![viewController sendCommandExists:CMD_AUX1_SEND dspId:DSP_5]) {
                btnChannelAux1PostPre.selected = aux1PostPre;
            }
            aux2PostPre = (pkt->aux_2_send_pre_post & 0x04) >> 2;
            if (aux2PostPre != btnChannelAux2PostPre.selected && ![viewController sendCommandExists:CMD_AUX2_SEND dspId:DSP_5]) {
                btnChannelAux2PostPre.selected = aux2PostPre;
            }
            aux3PostPre = (pkt->aux_3_send_pre_post & 0x04) >> 2;
            if (aux3PostPre != btnChannelAux3PostPre.selected && ![viewController sendCommandExists:CMD_AUX3_SEND dspId:DSP_5]) {
                btnChannelAux3PostPre.selected = aux3PostPre;
            }
            aux4PostPre = (pkt->aux_4_send_pre_post & 0x04) >> 2;
            if (aux4PostPre != btnChannelAux4PostPre.selected && ![viewController sendCommandExists:CMD_AUX4_SEND dspId:DSP_5]) {
                btnChannelAux4PostPre.selected = aux4PostPre;
            }
            knobAux1.value = pkt->aux_1_ch_3_fader_level;
            [self knobAux1DidChange:nil];
            knobAux2.value = pkt->aux_2_ch_3_fader_level;
            [self knobAux2DidChange:nil];
            knobAux3.value = pkt->aux_3_ch_3_fader_level;
            [self knobAux3DidChange:nil];
            knobAux4.value = pkt->aux_4_ch_3_fader_level;
            [self knobAux4DidChange:nil];
            gp1OnOff = (pkt->ch_1_16_assign_gp_1 & 0x04) >> 2;
            if (gp1OnOff != btnChannelGp1OnOff.selected && ![viewController sendCommandExists:CMD_GP1_ASSIGN dspId:DSP_5]) {
                btnChannelGp1OnOff.selected = gp1OnOff;
            }
            gp2OnOff = (pkt->ch_1_16_assign_gp_2 & 0x04) >> 2;
            if (gp2OnOff != btnChannelGp2OnOff.selected && ![viewController sendCommandExists:CMD_GP2_ASSIGN dspId:DSP_5]) {
                btnChannelGp2OnOff.selected = gp2OnOff;
            }
            gp3OnOff = (pkt->ch_1_16_assign_gp_3 & 0x04) >> 2;
            if (gp3OnOff != btnChannelGp3OnOff.selected && ![viewController sendCommandExists:CMD_GP3_ASSIGN dspId:DSP_5]) {
                btnChannelGp3OnOff.selected = gp3OnOff;
            }
            gp4OnOff = (pkt->ch_1_16_assign_gp_4 & 0x04) >> 2;
            if (gp4OnOff != btnChannelGp4OnOff.selected && ![viewController sendCommandExists:CMD_GP4_ASSIGN dspId:DSP_5]) {
                btnChannelGp4OnOff.selected = gp4OnOff;
            }
            toMainOnOff = (pkt->ch_1_16_to_main_ctrl & 0x04) >> 2;
            if (toMainOnOff != btnChannelToMainOnOff.selected && ![viewController sendCommandExists:CMD_CH_TO_MAIN_CTRL dspId:DSP_5]) {
                btnChannelToMainOnOff.selected = toMainOnOff;
            }
            if (!_sliderCurrentLock) {
                sliderCurrent.value = pkt->ch_3_fader_level;
                [lblSliderValue setText:[Global getSliderGain:sliderCurrent.value appendDb:YES]];
            }
            soloSafe = (pkt->ch_1_16_solo_safe_ctrl_on_off & 0x04) >> 2;
            if (soloSafe) {
                [btnCurrentSolo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
                [btnCurrentSolo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
            } else {
                [btnCurrentSolo setTitle:@"SOLO" forState:UIControlStateNormal];
                [btnCurrentSolo setTitle:@"SOLO" forState:UIControlStateSelected];
            }
            currentSolo = (pkt->ch_1_16_solo_ctrl_on_off & 0x04) >> 2;
            if (currentSolo != btnCurrentSolo.selected && ![viewController sendCommandExists:CMD_SOLO_CTRL dspId:DSP_5]) {
                btnCurrentSolo.selected = currentSolo;
            }
            currentOn = (pkt->ch_1_16_on_off & 0x04) >> 2;
            if (currentOn != btnCurrentOn.selected && ![viewController sendCommandExists:CMD_CH_ON_OFF dspId:DSP_5]) {
                btnCurrentOn.selected = currentOn;
            }
            currentMeter = (pkt->ch_1_16_meter_pre_post & 0x04) >> 2;
            if (currentMeter != btnCurrentMeter.selected && ![viewController sendCommandExists:CMD_CH_METER dspId:DSP_5]) {
                btnCurrentMeter.selected = currentMeter;
            }
            // Update DYN status
            if (btnDyn.selected) {
                // Check DYN status, green if ACTIVE, yellow if NOP
                if ((pkt->channel_1_4_dyn_active & 0x0100) >> 8) {
                    [btnDynGate setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynGate setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->channel_1_4_dyn_active & 0x0200) >> 9) {
                    [btnDynExpander setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynExpander setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->channel_1_4_dyn_active & 0x0400) >> 10) {
                    [btnDynComp setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynComp setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->channel_1_4_dyn_active & 0x0800) >> 11) {
                    [btnDynLimiter setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynLimiter setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
            }
            break;
        case CH_4:
            meterEqOut.progress = ((float)pkt->channel_4_eq_meter_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterDynOut.progress = ((float)pkt->channel_4_dyn_meter_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            if (![viewController sendCommandExists:CMD_CH4_CTRL dspId:DSP_1]) {
                [self refreshCtrlViews:pkt->channel_4_ctrl];
            }
            if (![viewController sendCommandExists:CMD_CH4_DELAY_TIME dspId:DSP_1]) {
                knobDelay.value = [Global delayToKnobValue:pkt->channel_4_delay_time];
                [self knobDelayDidChange:nil];
            }
            sliderPan.value = pkt->channel_4_pan_level + PAN_CENTER_VALUE;
            [self onSliderPanAction:nil];
            aux1PostPre = (pkt->aux_1_send_pre_post & 0x08) >> 3;
            if (aux1PostPre != btnChannelAux1PostPre.selected && ![viewController sendCommandExists:CMD_AUX1_SEND dspId:DSP_5]) {
                btnChannelAux1PostPre.selected = aux1PostPre;
            }
            aux2PostPre = (pkt->aux_2_send_pre_post & 0x08) >> 3;
            if (aux2PostPre != btnChannelAux2PostPre.selected && ![viewController sendCommandExists:CMD_AUX2_SEND dspId:DSP_5]) {
                btnChannelAux2PostPre.selected = aux2PostPre;
            }
            aux3PostPre = (pkt->aux_3_send_pre_post & 0x08) >> 3;
            if (aux3PostPre != btnChannelAux3PostPre.selected && ![viewController sendCommandExists:CMD_AUX3_SEND dspId:DSP_5]) {
                btnChannelAux3PostPre.selected = aux3PostPre;
            }
            aux4PostPre = (pkt->aux_4_send_pre_post & 0x08) >> 3;
            if (aux4PostPre != btnChannelAux4PostPre.selected && ![viewController sendCommandExists:CMD_AUX4_SEND dspId:DSP_5]) {
                btnChannelAux4PostPre.selected = aux4PostPre;
            }
            knobAux1.value = pkt->aux_1_ch_4_fader_level;
            [self knobAux1DidChange:nil];
            knobAux2.value = pkt->aux_2_ch_4_fader_level;
            [self knobAux2DidChange:nil];
            knobAux3.value = pkt->aux_3_ch_4_fader_level;
            [self knobAux3DidChange:nil];
            knobAux4.value = pkt->aux_4_ch_4_fader_level;
            [self knobAux4DidChange:nil];
            gp1OnOff = (pkt->ch_1_16_assign_gp_1 & 0x08) >> 3;
            if (gp1OnOff != btnChannelGp1OnOff.selected && ![viewController sendCommandExists:CMD_GP1_ASSIGN dspId:DSP_5]) {
                btnChannelGp1OnOff.selected = gp1OnOff;
            }
            gp2OnOff = (pkt->ch_1_16_assign_gp_2 & 0x08) >> 3;
            if (gp2OnOff != btnChannelGp2OnOff.selected && ![viewController sendCommandExists:CMD_GP2_ASSIGN dspId:DSP_5]) {
                btnChannelGp2OnOff.selected = gp2OnOff;
            }
            gp3OnOff = (pkt->ch_1_16_assign_gp_3 & 0x08) >> 3;
            if (gp3OnOff != btnChannelGp3OnOff.selected && ![viewController sendCommandExists:CMD_GP3_ASSIGN dspId:DSP_5]) {
                btnChannelGp3OnOff.selected = gp3OnOff;
            }
            gp4OnOff = (pkt->ch_1_16_assign_gp_4 & 0x08) >> 3;
            if (gp4OnOff != btnChannelGp4OnOff.selected && ![viewController sendCommandExists:CMD_GP4_ASSIGN dspId:DSP_5]) {
                btnChannelGp4OnOff.selected = gp4OnOff;
            }
            toMainOnOff = (pkt->ch_1_16_to_main_ctrl & 0x08) >> 3;
            if (toMainOnOff != btnChannelToMainOnOff.selected && ![viewController sendCommandExists:CMD_CH_TO_MAIN_CTRL dspId:DSP_5]) {
                btnChannelToMainOnOff.selected = toMainOnOff;
            }
            if (!_sliderCurrentLock) {
                sliderCurrent.value = pkt->ch_4_fader_level;
                [lblSliderValue setText:[Global getSliderGain:sliderCurrent.value appendDb:YES]];
            }
            soloSafe = (pkt->ch_1_16_solo_safe_ctrl_on_off & 0x08) >> 3;
            if (soloSafe) {
                [btnCurrentSolo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
                [btnCurrentSolo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
            } else {
                [btnCurrentSolo setTitle:@"SOLO" forState:UIControlStateNormal];
                [btnCurrentSolo setTitle:@"SOLO" forState:UIControlStateSelected];
            }
            currentSolo = (pkt->ch_1_16_solo_ctrl_on_off & 0x08) >> 3;
            if (currentSolo != btnCurrentSolo.selected && ![viewController sendCommandExists:CMD_SOLO_CTRL dspId:DSP_5]) {
                btnCurrentSolo.selected = currentSolo;
            }
            currentOn = (pkt->ch_1_16_on_off & 0x08) >> 3;
            if (currentOn != btnCurrentOn.selected && ![viewController sendCommandExists:CMD_CH_ON_OFF dspId:DSP_5]) {
                btnCurrentOn.selected = currentOn;
            }
            currentMeter = (pkt->ch_1_16_meter_pre_post & 0x08) >> 3;
            if (currentMeter != btnCurrentMeter.selected && ![viewController sendCommandExists:CMD_CH_METER dspId:DSP_5]) {
                btnCurrentMeter.selected = currentMeter;
            }
            // Update DYN status
            if (btnDyn.selected) {
                // Check DYN status, green if ACTIVE, yellow if NOP
                if ((pkt->channel_1_4_dyn_active & 0x1000) >> 12) {
                    [btnDynGate setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynGate setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->channel_1_4_dyn_active & 0x2000) >> 13) {
                    [btnDynExpander setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynExpander setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                }
                if ((pkt->channel_1_4_dyn_active & 0x4000) >> 14) {
                    [btnDynComp setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynComp setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->channel_1_4_dyn_active & 0x8000) >> 15) {
                    [btnDynLimiter setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynLimiter setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
            }
            break;
        case CH_5:
            meterEqOut.progress = ((float)pkt->channel_5_eq_meter_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterDynOut.progress = ((float)pkt->channel_5_dyn_meter_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            if (![viewController sendCommandExists:CMD_CH5_CTRL dspId:DSP_2]) {
                [self refreshCtrlViews:pkt->channel_5_ctrl];
            }
            if (![viewController sendCommandExists:CMD_CH5_DELAY_TIME dspId:DSP_2]) {
                knobDelay.value = [Global delayToKnobValue:pkt->channel_5_delay_time];
                [self knobDelayDidChange:nil];
            }
            sliderPan.value = pkt->channel_5_pan_level + PAN_CENTER_VALUE;
            [self onSliderPanAction:nil];
            aux1PostPre = (pkt->aux_1_send_pre_post & 0x10) >> 4;
            if (aux1PostPre != btnChannelAux1PostPre.selected && ![viewController sendCommandExists:CMD_AUX1_SEND dspId:DSP_5]) {
                btnChannelAux1PostPre.selected = aux1PostPre;
            }
            aux2PostPre = (pkt->aux_2_send_pre_post & 0x10) >> 4;
            if (aux2PostPre != btnChannelAux2PostPre.selected && ![viewController sendCommandExists:CMD_AUX2_SEND dspId:DSP_5]) {
                btnChannelAux2PostPre.selected = aux2PostPre;
            }
            aux3PostPre = (pkt->aux_3_send_pre_post & 0x10) >> 4;
            if (aux3PostPre != btnChannelAux3PostPre.selected && ![viewController sendCommandExists:CMD_AUX3_SEND dspId:DSP_5]) {
                btnChannelAux3PostPre.selected = aux3PostPre;
            }
            aux4PostPre = (pkt->aux_4_send_pre_post & 0x10) >> 4;
            if (aux4PostPre != btnChannelAux4PostPre.selected && ![viewController sendCommandExists:CMD_AUX4_SEND dspId:DSP_5]) {
                btnChannelAux4PostPre.selected = aux4PostPre;
            }
            knobAux1.value = pkt->aux_1_ch_5_fader_level;
            [self knobAux1DidChange:nil];
            knobAux2.value = pkt->aux_2_ch_5_fader_level;
            [self knobAux2DidChange:nil];
            knobAux3.value = pkt->aux_3_ch_5_fader_level;
            [self knobAux3DidChange:nil];
            knobAux4.value = pkt->aux_4_ch_5_fader_level;
            [self knobAux4DidChange:nil];
            gp1OnOff = (pkt->ch_1_16_assign_gp_1 & 0x10) >> 4;
            if (gp1OnOff != btnChannelGp1OnOff.selected && ![viewController sendCommandExists:CMD_GP1_ASSIGN dspId:DSP_5]) {
                btnChannelGp1OnOff.selected = gp1OnOff;
            }
            gp2OnOff = (pkt->ch_1_16_assign_gp_2 & 0x10) >> 4;
            if (gp2OnOff != btnChannelGp2OnOff.selected && ![viewController sendCommandExists:CMD_GP2_ASSIGN dspId:DSP_5]) {
                btnChannelGp2OnOff.selected = gp2OnOff;
            }
            gp3OnOff = (pkt->ch_1_16_assign_gp_3 & 0x10) >> 4;
            if (gp3OnOff != btnChannelGp3OnOff.selected && ![viewController sendCommandExists:CMD_GP3_ASSIGN dspId:DSP_5]) {
                btnChannelGp3OnOff.selected = gp3OnOff;
            }
            gp4OnOff = (pkt->ch_1_16_assign_gp_4 & 0x10) >> 4;
            if (gp4OnOff != btnChannelGp4OnOff.selected && ![viewController sendCommandExists:CMD_GP4_ASSIGN dspId:DSP_5]) {
                btnChannelGp4OnOff.selected = gp4OnOff;
            }
            toMainOnOff = (pkt->ch_1_16_to_main_ctrl & 0x10) >> 4;
            if (toMainOnOff != btnChannelToMainOnOff.selected && ![viewController sendCommandExists:CMD_CH_TO_MAIN_CTRL dspId:DSP_5]) {
                btnChannelToMainOnOff.selected = toMainOnOff;
            }
            if (!_sliderCurrentLock) {
                sliderCurrent.value = pkt->ch_5_fader_level;
                [lblSliderValue setText:[Global getSliderGain:sliderCurrent.value appendDb:YES]];
            }
            soloSafe = (pkt->ch_1_16_solo_safe_ctrl_on_off & 0x10) >> 4;
            if (soloSafe) {
                [btnCurrentSolo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
                [btnCurrentSolo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
            } else {
                [btnCurrentSolo setTitle:@"SOLO" forState:UIControlStateNormal];
                [btnCurrentSolo setTitle:@"SOLO" forState:UIControlStateSelected];
            }
            currentSolo = (pkt->ch_1_16_solo_ctrl_on_off & 0x10) >> 4;
            if (currentSolo != btnCurrentSolo.selected && ![viewController sendCommandExists:CMD_SOLO_CTRL dspId:DSP_5]) {
                btnCurrentSolo.selected = currentSolo;
            }
            currentOn = (pkt->ch_1_16_on_off & 0x10) >> 4;
            if (currentOn != btnCurrentOn.selected && ![viewController sendCommandExists:CMD_CH_ON_OFF dspId:DSP_5]) {
                btnCurrentOn.selected = currentOn;
            }
            currentMeter = (pkt->ch_1_16_meter_pre_post & 0x10) >> 4;
            if (currentMeter != btnCurrentMeter.selected && ![viewController sendCommandExists:CMD_CH_METER dspId:DSP_5]) {
                btnCurrentMeter.selected = currentMeter;
            }
            // Update DYN status
            if (btnDyn.selected) {
                // Check DYN status, green if ACTIVE, yellow if NOP
                if (pkt->channel_5_8_dyn_active & 0x01) {
                    [btnDynGate setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynGate setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->channel_5_8_dyn_active & 0x02) >> 1) {
                    [btnDynExpander setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynExpander setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->channel_5_8_dyn_active & 0x04) >> 2) {
                    [btnDynComp setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynComp setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->channel_5_8_dyn_active & 0x08) >> 3) {
                    [btnDynLimiter setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynLimiter setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
            }
            break;
        case CH_6:
            meterEqOut.progress = ((float)pkt->channel_6_eq_meter_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterDynOut.progress = ((float)pkt->channel_6_dyn_meter_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            if (![viewController sendCommandExists:CMD_CH6_CTRL dspId:DSP_2]) {
                [self refreshCtrlViews:pkt->channel_6_ctrl];
            }
            if (![viewController sendCommandExists:CMD_CH6_DELAY_TIME dspId:DSP_2]) {
                knobDelay.value = [Global delayToKnobValue:pkt->channel_6_delay_time];
                [self knobDelayDidChange:nil];
            }
            sliderPan.value = pkt->channel_6_pan_level + PAN_CENTER_VALUE;
            [self onSliderPanAction:nil];
            aux1PostPre = (pkt->aux_1_send_pre_post & 0x20) >> 5;
            if (aux1PostPre != btnChannelAux1PostPre.selected && ![viewController sendCommandExists:CMD_AUX1_SEND dspId:DSP_5]) {
                btnChannelAux1PostPre.selected = aux1PostPre;
            }
            aux2PostPre = (pkt->aux_2_send_pre_post & 0x20) >> 5;
            if (aux2PostPre != btnChannelAux2PostPre.selected && ![viewController sendCommandExists:CMD_AUX2_SEND dspId:DSP_5]) {
                btnChannelAux2PostPre.selected = aux2PostPre;
            }
            aux3PostPre = (pkt->aux_3_send_pre_post & 0x20) >> 5;
            if (aux3PostPre != btnChannelAux3PostPre.selected && ![viewController sendCommandExists:CMD_AUX3_SEND dspId:DSP_5]) {
                btnChannelAux3PostPre.selected = aux3PostPre;
            }
            aux4PostPre = (pkt->aux_4_send_pre_post & 0x20) >> 5;
            if (aux4PostPre != btnChannelAux4PostPre.selected && ![viewController sendCommandExists:CMD_AUX4_SEND dspId:DSP_5]) {
                btnChannelAux4PostPre.selected = aux4PostPre;
            }
            knobAux1.value = pkt->aux_1_ch_6_fader_level;
            [self knobAux1DidChange:nil];
            knobAux2.value = pkt->aux_2_ch_6_fader_level;
            [self knobAux2DidChange:nil];
            knobAux3.value = pkt->aux_3_ch_6_fader_level;
            [self knobAux3DidChange:nil];
            knobAux4.value = pkt->aux_4_ch_6_fader_level;
            [self knobAux4DidChange:nil];
            gp1OnOff = (pkt->ch_1_16_assign_gp_1 & 0x20) >> 5;
            if (gp1OnOff != btnChannelGp1OnOff.selected && ![viewController sendCommandExists:CMD_GP1_ASSIGN dspId:DSP_5]) {
                btnChannelGp1OnOff.selected = gp1OnOff;
            }
            gp2OnOff = (pkt->ch_1_16_assign_gp_2 & 0x20) >> 5;
            if (gp2OnOff != btnChannelGp2OnOff.selected && ![viewController sendCommandExists:CMD_GP2_ASSIGN dspId:DSP_5]) {
                btnChannelGp2OnOff.selected = gp2OnOff;
            }
            gp3OnOff = (pkt->ch_1_16_assign_gp_3 & 0x20) >> 5;
            if (gp3OnOff != btnChannelGp3OnOff.selected && ![viewController sendCommandExists:CMD_GP3_ASSIGN dspId:DSP_5]) {
                btnChannelGp3OnOff.selected = gp3OnOff;
            }
            gp4OnOff = (pkt->ch_1_16_assign_gp_4 & 0x20) >> 5;
            if (gp4OnOff != btnChannelGp4OnOff.selected && ![viewController sendCommandExists:CMD_GP4_ASSIGN dspId:DSP_5]) {
                btnChannelGp4OnOff.selected = gp4OnOff;
            }
            toMainOnOff = (pkt->ch_1_16_to_main_ctrl & 0x20) >> 5;
            if (toMainOnOff != btnChannelToMainOnOff.selected && ![viewController sendCommandExists:CMD_CH_TO_MAIN_CTRL dspId:DSP_5]) {
                btnChannelToMainOnOff.selected = toMainOnOff;
            }
            if (!_sliderCurrentLock) {
                sliderCurrent.value = pkt->ch_6_fader_level;
                [lblSliderValue setText:[Global getSliderGain:sliderCurrent.value appendDb:YES]];
            }
            soloSafe = (pkt->ch_1_16_solo_safe_ctrl_on_off & 0x20) >> 5;
            if (soloSafe) {
                [btnCurrentSolo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
                [btnCurrentSolo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
            } else {
                [btnCurrentSolo setTitle:@"SOLO" forState:UIControlStateNormal];
                [btnCurrentSolo setTitle:@"SOLO" forState:UIControlStateSelected];
            }
            currentSolo = (pkt->ch_1_16_solo_ctrl_on_off & 0x20) >> 5;
            if (currentSolo != btnCurrentSolo.selected && ![viewController sendCommandExists:CMD_SOLO_CTRL dspId:DSP_5]) {
                btnCurrentSolo.selected = currentSolo;
            }
            currentOn = (pkt->ch_1_16_on_off & 0x20) >> 5;
            if (currentOn != btnCurrentOn.selected && ![viewController sendCommandExists:CMD_CH_ON_OFF dspId:DSP_5]) {
                btnCurrentOn.selected = currentOn;
            }
            currentMeter = (pkt->ch_1_16_meter_pre_post & 0x20) >> 5;
            if (currentMeter != btnCurrentMeter.selected && ![viewController sendCommandExists:CMD_CH_METER dspId:DSP_5]) {
                btnCurrentMeter.selected = currentMeter;
            }
            // Update DYN status
            if (btnDyn.selected) {
                // Check DYN status, green if ACTIVE, yellow if NOP
                if ((pkt->channel_5_8_dyn_active & 0x10) >> 4) {
                    [btnDynGate setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynGate setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->channel_5_8_dyn_active & 0x20) >> 5) {
                    [btnDynExpander setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynExpander setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->channel_5_8_dyn_active & 0x40) >> 6) {
                    [btnDynComp setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynComp setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->channel_5_8_dyn_active & 0x80) >> 7) {
                    [btnDynLimiter setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynLimiter setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
            }
            break;
        case CH_7:
            meterEqOut.progress = ((float)pkt->channel_7_eq_meter_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterDynOut.progress = ((float)pkt->channel_7_dyn_meter_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            if (![viewController sendCommandExists:CMD_CH7_CTRL dspId:DSP_2]) {
                [self refreshCtrlViews:pkt->channel_7_ctrl];
            }
            if (![viewController sendCommandExists:CMD_CH7_DELAY_TIME dspId:DSP_2]) {
                knobDelay.value = [Global delayToKnobValue:pkt->channel_7_delay_time];
                [self knobDelayDidChange:nil];
            }
            sliderPan.value = pkt->channel_7_pan_level + PAN_CENTER_VALUE;
            [self onSliderPanAction:nil];
            aux1PostPre = (pkt->aux_1_send_pre_post & 0x40) >> 6;
            if (aux1PostPre != btnChannelAux1PostPre.selected && ![viewController sendCommandExists:CMD_AUX1_SEND dspId:DSP_5]) {
                btnChannelAux1PostPre.selected = aux1PostPre;
            }
            aux2PostPre = (pkt->aux_2_send_pre_post & 0x40) >> 6;
            if (aux2PostPre != btnChannelAux2PostPre.selected && ![viewController sendCommandExists:CMD_AUX2_SEND dspId:DSP_5]) {
                btnChannelAux2PostPre.selected = aux2PostPre;
            }
            aux3PostPre = (pkt->aux_3_send_pre_post & 0x40) >> 6;
            if (aux3PostPre != btnChannelAux3PostPre.selected && ![viewController sendCommandExists:CMD_AUX3_SEND dspId:DSP_5]) {
                btnChannelAux3PostPre.selected = aux3PostPre;
            }
            aux4PostPre = (pkt->aux_4_send_pre_post & 0x40) >> 6;
            if (aux4PostPre != btnChannelAux4PostPre.selected && ![viewController sendCommandExists:CMD_AUX4_SEND dspId:DSP_5]) {
                btnChannelAux4PostPre.selected = aux4PostPre;
            }
            knobAux1.value = pkt->aux_1_ch_7_fader_level;
            [self knobAux1DidChange:nil];
            knobAux2.value = pkt->aux_2_ch_7_fader_level;
            [self knobAux2DidChange:nil];
            knobAux3.value = pkt->aux_3_ch_7_fader_level;
            [self knobAux3DidChange:nil];
            knobAux4.value = pkt->aux_4_ch_7_fader_level;
            [self knobAux4DidChange:nil];
            gp1OnOff = (pkt->ch_1_16_assign_gp_1 & 0x40) >> 6;
            if (gp1OnOff != btnChannelGp1OnOff.selected && ![viewController sendCommandExists:CMD_GP1_ASSIGN dspId:DSP_5]) {
                btnChannelGp1OnOff.selected = gp1OnOff;
            }
            gp2OnOff = (pkt->ch_1_16_assign_gp_2 & 0x40) >> 6;
            if (gp2OnOff != btnChannelGp2OnOff.selected && ![viewController sendCommandExists:CMD_GP2_ASSIGN dspId:DSP_5]) {
                btnChannelGp2OnOff.selected = gp2OnOff;
            }
            gp3OnOff = (pkt->ch_1_16_assign_gp_3 & 0x40) >> 6;
            if (gp3OnOff != btnChannelGp3OnOff.selected && ![viewController sendCommandExists:CMD_GP3_ASSIGN dspId:DSP_5]) {
                btnChannelGp3OnOff.selected = gp3OnOff;
            }
            gp4OnOff = (pkt->ch_1_16_assign_gp_4 & 0x40) >> 6;
            if (gp4OnOff != btnChannelGp4OnOff.selected && ![viewController sendCommandExists:CMD_GP4_ASSIGN dspId:DSP_5]) {
                btnChannelGp4OnOff.selected = gp4OnOff;
            }
            toMainOnOff = (pkt->ch_1_16_to_main_ctrl & 0x40) >> 6;
            if (toMainOnOff != btnChannelToMainOnOff.selected && ![viewController sendCommandExists:CMD_CH_TO_MAIN_CTRL dspId:DSP_5]) {
                btnChannelToMainOnOff.selected = toMainOnOff;
            }
            if (!_sliderCurrentLock) {
                sliderCurrent.value = pkt->ch_7_fader_level;
                [lblSliderValue setText:[Global getSliderGain:sliderCurrent.value appendDb:YES]];
            }
            soloSafe = (pkt->ch_1_16_solo_safe_ctrl_on_off & 0x40) >> 6;
            if (soloSafe) {
                [btnCurrentSolo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
                [btnCurrentSolo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
            } else {
                [btnCurrentSolo setTitle:@"SOLO" forState:UIControlStateNormal];
                [btnCurrentSolo setTitle:@"SOLO" forState:UIControlStateSelected];
            }
            currentSolo = (pkt->ch_1_16_solo_ctrl_on_off & 0x40) >> 6;
            if (currentSolo != btnCurrentSolo.selected && ![viewController sendCommandExists:CMD_SOLO_CTRL dspId:DSP_5]) {
                btnCurrentSolo.selected = currentSolo;
            }
            currentOn = (pkt->ch_1_16_on_off & 0x40) >> 6;
            if (currentOn != btnCurrentOn.selected && ![viewController sendCommandExists:CMD_CH_ON_OFF dspId:DSP_5]) {
                btnCurrentOn.selected = currentOn;
            }
            currentMeter = (pkt->ch_1_16_meter_pre_post & 0x40) >> 6;
            if (currentMeter != btnCurrentMeter.selected && ![viewController sendCommandExists:CMD_CH_METER dspId:DSP_5]) {
                btnCurrentMeter.selected = currentMeter;
            }
            // Update DYN status
            if (btnDyn.selected) {
                // Check DYN status, green if ACTIVE, yellow if NOP
                if ((pkt->channel_5_8_dyn_active & 0x0100) >> 8) {
                    [btnDynGate setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynGate setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->channel_5_8_dyn_active & 0x0200) >> 9) {
                    [btnDynExpander setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynExpander setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->channel_5_8_dyn_active & 0x0400) >> 10) {
                    [btnDynComp setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynComp setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->channel_5_8_dyn_active & 0x0800) >> 11) {
                    [btnDynLimiter setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynLimiter setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
            }
            break;
        case CH_8:
            meterEqOut.progress = ((float)pkt->channel_8_eq_meter_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterDynOut.progress = ((float)pkt->channel_8_dyn_meter_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            if (![viewController sendCommandExists:CMD_CH8_CTRL dspId:DSP_2]) {
                [self refreshCtrlViews:pkt->channel_8_ctrl];
            }
            if (![viewController sendCommandExists:CMD_CH8_DELAY_TIME dspId:DSP_2]) {
                knobDelay.value = [Global delayToKnobValue:pkt->channel_8_delay_time];
                [self knobDelayDidChange:nil];
            }
            sliderPan.value = pkt->channel_8_pan_level + PAN_CENTER_VALUE;
            [self onSliderPanAction:nil];
            aux1PostPre = (pkt->aux_1_send_pre_post & 0x80) >> 7;
            if (aux1PostPre != btnChannelAux1PostPre.selected && ![viewController sendCommandExists:CMD_AUX1_SEND dspId:DSP_5]) {
                btnChannelAux1PostPre.selected = aux1PostPre;
            }
            aux2PostPre = (pkt->aux_2_send_pre_post & 0x80) >> 7;
            if (aux2PostPre != btnChannelAux2PostPre.selected && ![viewController sendCommandExists:CMD_AUX2_SEND dspId:DSP_5]) {
                btnChannelAux2PostPre.selected = aux2PostPre;
            }
            aux3PostPre = (pkt->aux_3_send_pre_post & 0x80) >> 7;
            if (aux3PostPre != btnChannelAux3PostPre.selected && ![viewController sendCommandExists:CMD_AUX3_SEND dspId:DSP_5]) {
                btnChannelAux3PostPre.selected = aux3PostPre;
            }
            aux4PostPre = (pkt->aux_4_send_pre_post & 0x80) >> 7;
            if (aux4PostPre != btnChannelAux4PostPre.selected && ![viewController sendCommandExists:CMD_AUX4_SEND dspId:DSP_5]) {
                btnChannelAux4PostPre.selected = aux4PostPre;
            }
            knobAux1.value = pkt->aux_1_ch_8_fader_level;
            [self knobAux1DidChange:nil];
            knobAux2.value = pkt->aux_2_ch_8_fader_level;
            [self knobAux2DidChange:nil];
            knobAux3.value = pkt->aux_3_ch_8_fader_level;
            [self knobAux3DidChange:nil];
            knobAux4.value = pkt->aux_4_ch_8_fader_level;
            [self knobAux4DidChange:nil];
            gp1OnOff = (pkt->ch_1_16_assign_gp_1 & 0x80) >> 7;
            if (gp1OnOff != btnChannelGp1OnOff.selected && ![viewController sendCommandExists:CMD_GP1_ASSIGN dspId:DSP_5]) {
                btnChannelGp1OnOff.selected = gp1OnOff;
            }
            gp2OnOff = (pkt->ch_1_16_assign_gp_2 & 0x80) >> 7;
            if (gp2OnOff != btnChannelGp2OnOff.selected && ![viewController sendCommandExists:CMD_GP2_ASSIGN dspId:DSP_5]) {
                btnChannelGp2OnOff.selected = gp2OnOff;
            }
            gp3OnOff = (pkt->ch_1_16_assign_gp_3 & 0x80) >> 7;
            if (gp3OnOff != btnChannelGp3OnOff.selected && ![viewController sendCommandExists:CMD_GP3_ASSIGN dspId:DSP_5]) {
                btnChannelGp3OnOff.selected = gp3OnOff;
            }
            gp4OnOff = (pkt->ch_1_16_assign_gp_4 & 0x80) >> 7;
            if (gp4OnOff != btnChannelGp4OnOff.selected && ![viewController sendCommandExists:CMD_GP4_ASSIGN dspId:DSP_5]) {
                btnChannelGp4OnOff.selected = gp4OnOff;
            }
            toMainOnOff = (pkt->ch_1_16_to_main_ctrl & 0x80) >> 7;
            if (toMainOnOff != btnChannelToMainOnOff.selected && ![viewController sendCommandExists:CMD_CH_TO_MAIN_CTRL dspId:DSP_5]) {
                btnChannelToMainOnOff.selected = toMainOnOff;
            }
            if (!_sliderCurrentLock) {
                sliderCurrent.value = pkt->ch_8_fader_level;
                [lblSliderValue setText:[Global getSliderGain:sliderCurrent.value appendDb:YES]];
            }
            soloSafe = (pkt->ch_1_16_solo_safe_ctrl_on_off & 0x80) >> 7;
            if (soloSafe) {
                [btnCurrentSolo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
                [btnCurrentSolo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
            } else {
                [btnCurrentSolo setTitle:@"SOLO" forState:UIControlStateNormal];
                [btnCurrentSolo setTitle:@"SOLO" forState:UIControlStateSelected];
            }
            currentSolo = (pkt->ch_1_16_solo_ctrl_on_off & 0x80) >> 7;
            if (currentSolo != btnCurrentSolo.selected && ![viewController sendCommandExists:CMD_SOLO_CTRL dspId:DSP_5]) {
                btnCurrentSolo.selected = currentSolo;
            }
            currentOn = (pkt->ch_1_16_on_off & 0x80) >> 7;
            if (currentOn != btnCurrentOn.selected && ![viewController sendCommandExists:CMD_CH_ON_OFF dspId:DSP_5]) {
                btnCurrentOn.selected = currentOn;
            }
            currentMeter = (pkt->ch_1_16_meter_pre_post & 0x80) >> 7;
            if (currentMeter != btnCurrentMeter.selected && ![viewController sendCommandExists:CMD_CH_METER dspId:DSP_5]) {
                btnCurrentMeter.selected = currentMeter;
            }
            // Update DYN status
            if (btnDyn.selected) {
                // Check DYN status, green if ACTIVE, yellow if NOP
                if ((pkt->channel_5_8_dyn_active & 0x1000) >> 12) {
                    [btnDynGate setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynGate setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->channel_5_8_dyn_active & 0x2000) >> 13) {
                    [btnDynExpander setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynExpander setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->channel_5_8_dyn_active & 0x4000) >> 14) {
                    [btnDynComp setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynComp setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->channel_5_8_dyn_active & 0x8000) >> 15) {
                    [btnDynLimiter setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynLimiter setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
            }
            break;
        case CH_9:
            meterEqOut.progress = ((float)pkt->channel_9_eq_meter_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterDynOut.progress = ((float)pkt->channel_9_dyn_meter_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            if (![viewController sendCommandExists:CMD_CH9_CTRL dspId:DSP_3]) {
                [self refreshCtrlViews:pkt->channel_9_ctrl];
            }
            if (![viewController sendCommandExists:CMD_CH9_DELAY_TIME dspId:DSP_3]) {
                knobDelay.value = [Global delayToKnobValue:pkt->channel_9_delay_time];
                [self knobDelayDidChange:nil];
            }
            sliderPan.value = pkt->channel_9_pan_level + PAN_CENTER_VALUE;
            [self onSliderPanAction:nil];
            aux1PostPre = (pkt->aux_1_send_pre_post & 0x0100) >> 8;
            if (aux1PostPre != btnChannelAux1PostPre.selected && ![viewController sendCommandExists:CMD_AUX1_SEND dspId:DSP_5]) {
                btnChannelAux1PostPre.selected = aux1PostPre;
            }
            aux2PostPre = (pkt->aux_2_send_pre_post & 0x0100) >> 8;
            if (aux2PostPre != btnChannelAux2PostPre.selected && ![viewController sendCommandExists:CMD_AUX2_SEND dspId:DSP_5]) {
                btnChannelAux2PostPre.selected = aux2PostPre;
            }
            aux3PostPre = (pkt->aux_3_send_pre_post & 0x0100) >> 8;
            if (aux3PostPre != btnChannelAux3PostPre.selected && ![viewController sendCommandExists:CMD_AUX3_SEND dspId:DSP_5]) {
                btnChannelAux3PostPre.selected = aux3PostPre;
            }
            aux4PostPre = (pkt->aux_4_send_pre_post & 0x0100) >> 8;
            if (aux4PostPre != btnChannelAux4PostPre.selected && ![viewController sendCommandExists:CMD_AUX4_SEND dspId:DSP_5]) {
                btnChannelAux4PostPre.selected = aux4PostPre;
            }
            knobAux1.value = pkt->aux_1_ch_9_fader_level;
            [self knobAux1DidChange:nil];
            knobAux2.value = pkt->aux_2_ch_9_fader_level;
            [self knobAux2DidChange:nil];
            knobAux3.value = pkt->aux_3_ch_9_fader_level;
            [self knobAux3DidChange:nil];
            knobAux4.value = pkt->aux_4_ch_9_fader_level;
            [self knobAux4DidChange:nil];
            gp1OnOff = (pkt->ch_1_16_assign_gp_1 & 0x0100) >> 8;
            if (gp1OnOff != btnChannelGp1OnOff.selected && ![viewController sendCommandExists:CMD_GP1_ASSIGN dspId:DSP_5]) {
                btnChannelGp1OnOff.selected = gp1OnOff;
            }
            gp2OnOff = (pkt->ch_1_16_assign_gp_2 & 0x0100) >> 8;
            if (gp2OnOff != btnChannelGp2OnOff.selected && ![viewController sendCommandExists:CMD_GP2_ASSIGN dspId:DSP_5]) {
                btnChannelGp2OnOff.selected = gp2OnOff;
            }
            gp3OnOff = (pkt->ch_1_16_assign_gp_3 & 0x0100) >> 8;
            if (gp3OnOff != btnChannelGp3OnOff.selected && ![viewController sendCommandExists:CMD_GP3_ASSIGN dspId:DSP_5]) {
                btnChannelGp3OnOff.selected = gp3OnOff;
            }
            gp4OnOff = (pkt->ch_1_16_assign_gp_4 & 0x0100) >> 8;
            if (gp4OnOff != btnChannelGp4OnOff.selected && ![viewController sendCommandExists:CMD_GP4_ASSIGN dspId:DSP_5]) {
                btnChannelGp4OnOff.selected = gp4OnOff;
            }
            toMainOnOff = (pkt->ch_1_16_to_main_ctrl & 0x0100) >> 8;
            if (toMainOnOff != btnChannelToMainOnOff.selected && ![viewController sendCommandExists:CMD_CH_TO_MAIN_CTRL dspId:DSP_5]) {
                btnChannelToMainOnOff.selected = toMainOnOff;
            }
            if (!_sliderCurrentLock) {
                sliderCurrent.value = pkt->ch_9_fader_level;
                [lblSliderValue setText:[Global getSliderGain:sliderCurrent.value appendDb:YES]];
            }
            soloSafe = (pkt->ch_1_16_solo_safe_ctrl_on_off & 0x0100) >> 8;
            if (soloSafe) {
                [btnCurrentSolo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
                [btnCurrentSolo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
            } else {
                [btnCurrentSolo setTitle:@"SOLO" forState:UIControlStateNormal];
                [btnCurrentSolo setTitle:@"SOLO" forState:UIControlStateSelected];
            }
            currentSolo = (pkt->ch_1_16_solo_ctrl_on_off & 0x0100) >> 8;
            if (currentSolo != btnCurrentSolo.selected && ![viewController sendCommandExists:CMD_SOLO_CTRL dspId:DSP_5]) {
                btnCurrentSolo.selected = currentSolo;
            }
            currentOn = (pkt->ch_1_16_on_off & 0x0100) >> 8;
            if (currentOn != btnCurrentOn.selected && ![viewController sendCommandExists:CMD_CH_ON_OFF dspId:DSP_5]) {
                btnCurrentOn.selected = currentOn;
            }
            currentMeter = (pkt->ch_1_16_meter_pre_post & 0x0100) >> 8;
            if (currentMeter != btnCurrentMeter.selected && ![viewController sendCommandExists:CMD_CH_METER dspId:DSP_5]) {
                btnCurrentMeter.selected = currentMeter;
            }
            // Update DYN status
            if (btnDyn.selected) {
                // Check DYN status, green if ACTIVE, yellow if NOP
                if (pkt->channel_9_12_dyn_active & 0x01) {
                    [btnDynGate setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynGate setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->channel_9_12_dyn_active & 0x02) >> 1) {
                    [btnDynExpander setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynExpander setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->channel_9_12_dyn_active & 0x04) >> 2) {
                    [btnDynComp setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynComp setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->channel_9_12_dyn_active & 0x08) >> 3) {
                    [btnDynLimiter setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynLimiter setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
            }
            break;
        case CH_10:
            meterEqOut.progress = ((float)pkt->channel_10_eq_meter_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterDynOut.progress = ((float)pkt->channel_10_dyn_meter_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            if (![viewController sendCommandExists:CMD_CH10_CTRL dspId:DSP_3]) {
                [self refreshCtrlViews:pkt->channel_10_ctrl];
            }
            if (![viewController sendCommandExists:CMD_CH10_DELAY_TIME dspId:DSP_3]) {
                knobDelay.value = [Global delayToKnobValue:pkt->channel_10_delay_time];
                [self knobDelayDidChange:nil];
            }
            sliderPan.value = pkt->channel_10_pan_level + PAN_CENTER_VALUE;
            [self onSliderPanAction:nil];
            aux1PostPre = (pkt->aux_1_send_pre_post & 0x0200) >> 9;
            if (aux1PostPre != btnChannelAux1PostPre.selected && ![viewController sendCommandExists:CMD_AUX1_SEND dspId:DSP_5]) {
                btnChannelAux1PostPre.selected = aux1PostPre;
            }
            aux2PostPre = (pkt->aux_2_send_pre_post & 0x0200) >> 9;
            if (aux2PostPre != btnChannelAux2PostPre.selected && ![viewController sendCommandExists:CMD_AUX2_SEND dspId:DSP_5]) {
                btnChannelAux2PostPre.selected = aux2PostPre;
            }
            aux3PostPre = (pkt->aux_3_send_pre_post & 0x0200) >> 9;
            if (aux3PostPre != btnChannelAux3PostPre.selected && ![viewController sendCommandExists:CMD_AUX3_SEND dspId:DSP_5]) {
                btnChannelAux3PostPre.selected = aux3PostPre;
            }
            aux4PostPre = (pkt->aux_4_send_pre_post & 0x0200) >> 9;
            if (aux4PostPre != btnChannelAux4PostPre.selected && ![viewController sendCommandExists:CMD_AUX4_SEND dspId:DSP_5]) {
                btnChannelAux4PostPre.selected = aux4PostPre;
            }
            knobAux1.value = pkt->aux_1_ch_10_fader_level;
            [self knobAux1DidChange:nil];
            knobAux2.value = pkt->aux_2_ch_10_fader_level;
            [self knobAux2DidChange:nil];
            knobAux3.value = pkt->aux_3_ch_10_fader_level;
            [self knobAux3DidChange:nil];
            knobAux4.value = pkt->aux_4_ch_10_fader_level;
            [self knobAux4DidChange:nil];
            gp1OnOff = (pkt->ch_1_16_assign_gp_1 & 0x0200) >> 9;
            if (gp1OnOff != btnChannelGp1OnOff.selected && ![viewController sendCommandExists:CMD_GP1_ASSIGN dspId:DSP_5]) {
                btnChannelGp1OnOff.selected = gp1OnOff;
            }
            gp2OnOff = (pkt->ch_1_16_assign_gp_2 & 0x0200) >> 9;
            if (gp2OnOff != btnChannelGp2OnOff.selected && ![viewController sendCommandExists:CMD_GP2_ASSIGN dspId:DSP_5]) {
                btnChannelGp2OnOff.selected = gp2OnOff;
            }
            gp3OnOff = (pkt->ch_1_16_assign_gp_3 & 0x0200) >> 9;
            if (gp3OnOff != btnChannelGp3OnOff.selected && ![viewController sendCommandExists:CMD_GP3_ASSIGN dspId:DSP_5]) {
                btnChannelGp3OnOff.selected = gp3OnOff;
            }
            gp4OnOff = (pkt->ch_1_16_assign_gp_4 & 0x0200) >> 9;
            if (gp4OnOff != btnChannelGp4OnOff.selected && ![viewController sendCommandExists:CMD_GP4_ASSIGN dspId:DSP_5]) {
                btnChannelGp4OnOff.selected = gp4OnOff;
            }
            toMainOnOff = (pkt->ch_1_16_to_main_ctrl & 0x0200) >> 9;
            if (toMainOnOff != btnChannelToMainOnOff.selected && ![viewController sendCommandExists:CMD_CH_TO_MAIN_CTRL dspId:DSP_5]) {
                btnChannelToMainOnOff.selected = toMainOnOff;
            }
            if (!_sliderCurrentLock) {
                sliderCurrent.value = pkt->ch_10_fader_level;
                [lblSliderValue setText:[Global getSliderGain:sliderCurrent.value appendDb:YES]];
            }
            soloSafe = (pkt->ch_1_16_solo_safe_ctrl_on_off & 0x0200) >> 9;
            if (soloSafe) {
                [btnCurrentSolo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
                [btnCurrentSolo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
            } else {
                [btnCurrentSolo setTitle:@"SOLO" forState:UIControlStateNormal];
                [btnCurrentSolo setTitle:@"SOLO" forState:UIControlStateSelected];
            }
            currentSolo = (pkt->ch_1_16_solo_ctrl_on_off & 0x0200) >> 9;
            if (currentSolo != btnCurrentSolo.selected && ![viewController sendCommandExists:CMD_SOLO_CTRL dspId:DSP_5]) {
                btnCurrentSolo.selected = currentSolo;
            }
            currentOn = (pkt->ch_1_16_on_off & 0x0200) >> 9;
            if (currentOn != btnCurrentOn.selected && ![viewController sendCommandExists:CMD_CH_ON_OFF dspId:DSP_5]) {
                btnCurrentOn.selected = currentOn;
            }
            currentMeter = (pkt->ch_1_16_meter_pre_post & 0x0200) >> 9;
            if (currentMeter != btnCurrentMeter.selected && ![viewController sendCommandExists:CMD_CH_METER dspId:DSP_5]) {
                btnCurrentMeter.selected = currentMeter;
            }
            // Update DYN status
            if (btnDyn.selected) {
                // Check DYN status, green if ACTIVE, yellow if NOP
                if ((pkt->channel_9_12_dyn_active & 0x10) >> 4) {
                    [btnDynGate setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynGate setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->channel_9_12_dyn_active & 0x20) >> 5) {
                    [btnDynExpander setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynExpander setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->channel_9_12_dyn_active & 0x40) >> 6) {
                    [btnDynComp setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynComp setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->channel_9_12_dyn_active & 0x80) >> 7) {
                    [btnDynLimiter setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynLimiter setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
            }
            break;
        case CH_11:
            meterEqOut.progress = ((float)pkt->channel_11_eq_meter_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterDynOut.progress = ((float)pkt->channel_11_dyn_meter_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            if (![viewController sendCommandExists:CMD_CH11_CTRL dspId:DSP_3]) {
                [self refreshCtrlViews:pkt->channel_11_ctrl];
            }
            if (![viewController sendCommandExists:CMD_CH11_DELAY_TIME dspId:DSP_3]) {
                knobDelay.value = [Global delayToKnobValue:pkt->channel_11_delay_time];
                [self knobDelayDidChange:nil];
            }
            sliderPan.value = pkt->channel_11_pan_level + PAN_CENTER_VALUE;
            [self onSliderPanAction:nil];
            aux1PostPre = (pkt->aux_1_send_pre_post & 0x0400) >> 10;
            if (aux1PostPre != btnChannelAux1PostPre.selected && ![viewController sendCommandExists:CMD_AUX1_SEND dspId:DSP_5]) {
                btnChannelAux1PostPre.selected = aux1PostPre;
            }
            aux2PostPre = (pkt->aux_2_send_pre_post & 0x0400) >> 10;
            if (aux2PostPre != btnChannelAux2PostPre.selected && ![viewController sendCommandExists:CMD_AUX2_SEND dspId:DSP_5]) {
                btnChannelAux2PostPre.selected = aux2PostPre;
            }
            aux3PostPre = (pkt->aux_3_send_pre_post & 0x0400) >> 10;
            if (aux3PostPre != btnChannelAux3PostPre.selected && ![viewController sendCommandExists:CMD_AUX3_SEND dspId:DSP_5]) {
                btnChannelAux3PostPre.selected = aux3PostPre;
            }
            aux4PostPre = (pkt->aux_4_send_pre_post & 0x0400) >> 10;
            if (aux4PostPre != btnChannelAux4PostPre.selected && ![viewController sendCommandExists:CMD_AUX4_SEND dspId:DSP_5]) {
                btnChannelAux4PostPre.selected = aux4PostPre;
            }
            knobAux1.value = pkt->aux_1_ch_11_fader_level;
            [self knobAux1DidChange:nil];
            knobAux2.value = pkt->aux_2_ch_11_fader_level;
            [self knobAux2DidChange:nil];
            knobAux3.value = pkt->aux_3_ch_11_fader_level;
            [self knobAux3DidChange:nil];
            knobAux4.value = pkt->aux_4_ch_11_fader_level;
            [self knobAux4DidChange:nil];
            gp1OnOff = (pkt->ch_1_16_assign_gp_1 & 0x0400) >> 10;
            if (gp1OnOff != btnChannelGp1OnOff.selected && ![viewController sendCommandExists:CMD_GP1_ASSIGN dspId:DSP_5]) {
                btnChannelGp1OnOff.selected = gp1OnOff;
            }
            gp2OnOff = (pkt->ch_1_16_assign_gp_2 & 0x0400) >> 10;
            if (gp2OnOff != btnChannelGp2OnOff.selected && ![viewController sendCommandExists:CMD_GP2_ASSIGN dspId:DSP_5]) {
                btnChannelGp2OnOff.selected = gp2OnOff;
            }
            gp3OnOff = (pkt->ch_1_16_assign_gp_3 & 0x0400) >> 10;
            if (gp3OnOff != btnChannelGp3OnOff.selected && ![viewController sendCommandExists:CMD_GP3_ASSIGN dspId:DSP_5]) {
                btnChannelGp3OnOff.selected = gp3OnOff;
            }
            gp4OnOff = (pkt->ch_1_16_assign_gp_4 & 0x0400) >> 10;
            if (gp4OnOff != btnChannelGp4OnOff.selected && ![viewController sendCommandExists:CMD_GP4_ASSIGN dspId:DSP_5]) {
                btnChannelGp4OnOff.selected = gp4OnOff;
            }
            toMainOnOff = (pkt->ch_1_16_to_main_ctrl & 0x0400) >> 10;
            if (toMainOnOff != btnChannelToMainOnOff.selected && ![viewController sendCommandExists:CMD_CH_TO_MAIN_CTRL dspId:DSP_5]) {
                btnChannelToMainOnOff.selected = toMainOnOff;
            }
            if (!_sliderCurrentLock) {
                sliderCurrent.value = pkt->ch_11_fader_level;
                [lblSliderValue setText:[Global getSliderGain:sliderCurrent.value appendDb:YES]];
            }
            soloSafe = (pkt->ch_1_16_solo_safe_ctrl_on_off & 0x0400) >> 10;
            if (soloSafe) {
                [btnCurrentSolo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
                [btnCurrentSolo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
            } else {
                [btnCurrentSolo setTitle:@"SOLO" forState:UIControlStateNormal];
                [btnCurrentSolo setTitle:@"SOLO" forState:UIControlStateSelected];
            }
            currentSolo = (pkt->ch_1_16_solo_ctrl_on_off & 0x0400) >> 10;
            if (currentSolo != btnCurrentSolo.selected && ![viewController sendCommandExists:CMD_SOLO_CTRL dspId:DSP_5]) {
                btnCurrentSolo.selected = currentSolo;
            }
            currentOn = (pkt->ch_1_16_on_off & 0x0400) >> 10;
            if (currentOn != btnCurrentOn.selected && ![viewController sendCommandExists:CMD_CH_ON_OFF dspId:DSP_5]) {
                btnCurrentOn.selected = currentOn;
            }
            currentMeter = (pkt->ch_1_16_meter_pre_post & 0x0400) >> 10;
            if (currentMeter != btnCurrentMeter.selected && ![viewController sendCommandExists:CMD_CH_METER dspId:DSP_5]) {
                btnCurrentMeter.selected = currentMeter;
            }
            // Update DYN status
            if (btnDyn.selected) {
                // Check DYN status, green if ACTIVE, yellow if NOP
                if ((pkt->channel_9_12_dyn_active & 0x0100) >> 8) {
                    [btnDynGate setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynGate setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->channel_9_12_dyn_active & 0x0200) >> 9) {
                    [btnDynExpander setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynExpander setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->channel_9_12_dyn_active & 0x0400) >> 10) {
                    [btnDynComp setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynComp setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->channel_9_12_dyn_active & 0x0800) >> 11) {
                    [btnDynLimiter setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynLimiter setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
            }
            break;
        case CH_12:
            meterEqOut.progress = ((float)pkt->channel_12_eq_meter_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterDynOut.progress = ((float)pkt->channel_12_dyn_meter_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            if (![viewController sendCommandExists:CMD_CH12_CTRL dspId:DSP_3]) {
                [self refreshCtrlViews:pkt->channel_12_ctrl];
            }
            if (![viewController sendCommandExists:CMD_CH12_DELAY_TIME dspId:DSP_3]) {
                knobDelay.value = [Global delayToKnobValue:pkt->channel_12_delay_time];
                [self knobDelayDidChange:nil];
            }
            sliderPan.value = pkt->channel_12_pan_level + PAN_CENTER_VALUE;
            [self onSliderPanAction:nil];
            aux1PostPre = (pkt->aux_1_send_pre_post & 0x0800) >> 11;
            if (aux1PostPre != btnChannelAux1PostPre.selected && ![viewController sendCommandExists:CMD_AUX1_SEND dspId:DSP_5]) {
                btnChannelAux1PostPre.selected = aux1PostPre;
            }
            aux2PostPre = (pkt->aux_2_send_pre_post & 0x0800) >> 11;
            if (aux2PostPre != btnChannelAux2PostPre.selected && ![viewController sendCommandExists:CMD_AUX2_SEND dspId:DSP_5]) {
                btnChannelAux2PostPre.selected = aux2PostPre;
            }
            aux3PostPre = (pkt->aux_3_send_pre_post & 0x0800) >> 11;
            if (aux3PostPre != btnChannelAux3PostPre.selected && ![viewController sendCommandExists:CMD_AUX3_SEND dspId:DSP_5]) {
                btnChannelAux3PostPre.selected = aux3PostPre;
            }
            aux4PostPre = (pkt->aux_4_send_pre_post & 0x0800) >> 11;
            if (aux4PostPre != btnChannelAux4PostPre.selected && ![viewController sendCommandExists:CMD_AUX4_SEND dspId:DSP_5]) {
                btnChannelAux4PostPre.selected = aux4PostPre;
            }
            knobAux1.value = pkt->aux_1_ch_12_fader_level;
            [self knobAux1DidChange:nil];
            knobAux2.value = pkt->aux_2_ch_12_fader_level;
            [self knobAux2DidChange:nil];
            knobAux3.value = pkt->aux_3_ch_12_fader_level;
            [self knobAux3DidChange:nil];
            knobAux4.value = pkt->aux_4_ch_12_fader_level;
            [self knobAux4DidChange:nil];
            gp1OnOff = (pkt->ch_1_16_assign_gp_1 & 0x0800) >> 11;
            if (gp1OnOff != btnChannelGp1OnOff.selected && ![viewController sendCommandExists:CMD_GP1_ASSIGN dspId:DSP_5]) {
                btnChannelGp1OnOff.selected = gp1OnOff;
            }
            gp2OnOff = (pkt->ch_1_16_assign_gp_2 & 0x0800) >> 11;
            if (gp2OnOff != btnChannelGp2OnOff.selected && ![viewController sendCommandExists:CMD_GP2_ASSIGN dspId:DSP_5]) {
                btnChannelGp2OnOff.selected = gp2OnOff;
            }
            gp3OnOff = (pkt->ch_1_16_assign_gp_3 & 0x0800) >> 11;
            if (gp3OnOff != btnChannelGp3OnOff.selected && ![viewController sendCommandExists:CMD_GP3_ASSIGN dspId:DSP_5]) {
                btnChannelGp3OnOff.selected = gp3OnOff;
            }
            gp4OnOff = (pkt->ch_1_16_assign_gp_4 & 0x0800) >> 11;
            if (gp4OnOff != btnChannelGp4OnOff.selected && ![viewController sendCommandExists:CMD_GP4_ASSIGN dspId:DSP_5]) {
                btnChannelGp4OnOff.selected = gp4OnOff;
            }
            toMainOnOff = (pkt->ch_1_16_to_main_ctrl & 0x0800) >> 11;
            if (toMainOnOff != btnChannelToMainOnOff.selected && ![viewController sendCommandExists:CMD_CH_TO_MAIN_CTRL dspId:DSP_5]) {
                btnChannelToMainOnOff.selected = toMainOnOff;
            }
            if (!_sliderCurrentLock) {
                sliderCurrent.value = pkt->ch_12_fader_level;
                [lblSliderValue setText:[Global getSliderGain:sliderCurrent.value appendDb:YES]];
            }
            soloSafe = (pkt->ch_1_16_solo_safe_ctrl_on_off & 0x0800) >> 11;
            if (soloSafe) {
                [btnCurrentSolo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
                [btnCurrentSolo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
            } else {
                [btnCurrentSolo setTitle:@"SOLO" forState:UIControlStateNormal];
                [btnCurrentSolo setTitle:@"SOLO" forState:UIControlStateSelected];
            }
            currentSolo = (pkt->ch_1_16_solo_ctrl_on_off & 0x0800) >> 11;
            if (currentSolo != btnCurrentSolo.selected && ![viewController sendCommandExists:CMD_SOLO_CTRL dspId:DSP_5]) {
                btnCurrentSolo.selected = currentSolo;
            }
            currentOn = (pkt->ch_1_16_on_off & 0x0800) >> 11;
            if (currentOn != btnCurrentOn.selected && ![viewController sendCommandExists:CMD_CH_ON_OFF dspId:DSP_5]) {
                btnCurrentOn.selected = currentOn;
            }
            currentMeter = (pkt->ch_1_16_meter_pre_post & 0x0800) >> 11;
            if (currentMeter != btnCurrentMeter.selected && ![viewController sendCommandExists:CMD_CH_METER dspId:DSP_5]) {
                btnCurrentMeter.selected = currentMeter;
            }
            // Update DYN status
            if (btnDyn.selected) {
                // Check DYN status, green if ACTIVE, yellow if NOP
                if ((pkt->channel_9_12_dyn_active & 0x1000) >> 12) {
                    [btnDynGate setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynGate setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->channel_9_12_dyn_active & 0x2000) >> 13) {
                    [btnDynExpander setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynExpander setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->channel_9_12_dyn_active & 0x4000) >> 14) {
                    [btnDynComp setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynComp setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->channel_9_12_dyn_active & 0x8000) >> 15) {
                    [btnDynLimiter setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynLimiter setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
            }
            break;
        case CH_13:
            meterEqOut.progress = ((float)pkt->channel_13_eq_meter_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterDynOut.progress = ((float)pkt->channel_13_dyn_meter_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            if (![viewController sendCommandExists:CMD_CH13_CTRL dspId:DSP_4]) {
                [self refreshCtrlViews:pkt->channel_13_ctrl];
            }
            if (![viewController sendCommandExists:CMD_CH13_DELAY_TIME dspId:DSP_4]) {
                knobDelay.value = [Global delayToKnobValue:pkt->channel_13_delay_time];
                [self knobDelayDidChange:nil];
            }
            sliderPan.value = pkt->channel_13_pan_level + PAN_CENTER_VALUE;
            [self onSliderPanAction:nil];
            aux1PostPre = (pkt->aux_1_send_pre_post & 0x1000) >> 12;
            if (aux1PostPre != btnChannelAux1PostPre.selected && ![viewController sendCommandExists:CMD_AUX1_SEND dspId:DSP_5]) {
                btnChannelAux1PostPre.selected = aux1PostPre;
            }
            aux2PostPre = (pkt->aux_2_send_pre_post & 0x1000) >> 12;
            if (aux2PostPre != btnChannelAux2PostPre.selected && ![viewController sendCommandExists:CMD_AUX2_SEND dspId:DSP_5]) {
                btnChannelAux2PostPre.selected = aux2PostPre;
            }
            aux3PostPre = (pkt->aux_3_send_pre_post & 0x1000) >> 12;
            if (aux3PostPre != btnChannelAux3PostPre.selected && ![viewController sendCommandExists:CMD_AUX3_SEND dspId:DSP_5]) {
                btnChannelAux3PostPre.selected = aux3PostPre;
            }
            aux4PostPre = (pkt->aux_4_send_pre_post & 0x1000) >> 12;
            if (aux4PostPre != btnChannelAux4PostPre.selected && ![viewController sendCommandExists:CMD_AUX4_SEND dspId:DSP_5]) {
                btnChannelAux4PostPre.selected = aux4PostPre;
            }
            knobAux1.value = pkt->aux_1_ch_13_fader_level;
            [self knobAux1DidChange:nil];
            knobAux2.value = pkt->aux_2_ch_13_fader_level;
            [self knobAux2DidChange:nil];
            knobAux3.value = pkt->aux_3_ch_13_fader_level;
            [self knobAux3DidChange:nil];
            knobAux4.value = pkt->aux_4_ch_13_fader_level;
            [self knobAux4DidChange:nil];
            gp1OnOff = (pkt->ch_1_16_assign_gp_1 & 0x1000) >> 12;
            if (gp1OnOff != btnChannelGp1OnOff.selected && ![viewController sendCommandExists:CMD_GP1_ASSIGN dspId:DSP_5]) {
                btnChannelGp1OnOff.selected = gp1OnOff;
            }
            gp2OnOff = (pkt->ch_1_16_assign_gp_2 & 0x1000) >> 12;
            if (gp2OnOff != btnChannelGp2OnOff.selected && ![viewController sendCommandExists:CMD_GP2_ASSIGN dspId:DSP_5]) {
                btnChannelGp2OnOff.selected = gp2OnOff;
            }
            gp3OnOff = (pkt->ch_1_16_assign_gp_3 & 0x1000) >> 12;
            if (gp3OnOff != btnChannelGp3OnOff.selected && ![viewController sendCommandExists:CMD_GP3_ASSIGN dspId:DSP_5]) {
                btnChannelGp3OnOff.selected = gp3OnOff;
            }
            gp4OnOff = (pkt->ch_1_16_assign_gp_4 & 0x1000) >> 12;
            if (gp4OnOff != btnChannelGp4OnOff.selected && ![viewController sendCommandExists:CMD_GP4_ASSIGN dspId:DSP_5]) {
                btnChannelGp4OnOff.selected = gp4OnOff;
            }
            toMainOnOff = (pkt->ch_1_16_to_main_ctrl & 0x1000) >> 12;
            if (toMainOnOff != btnChannelToMainOnOff.selected && ![viewController sendCommandExists:CMD_CH_TO_MAIN_CTRL dspId:DSP_5]) {
                btnChannelToMainOnOff.selected = toMainOnOff;
            }
            if (!_sliderCurrentLock) {
                sliderCurrent.value = pkt->ch_13_fader_level;
                [lblSliderValue setText:[Global getSliderGain:sliderCurrent.value appendDb:YES]];
            }
            soloSafe = (pkt->ch_1_16_solo_safe_ctrl_on_off & 0x1000) >> 12;
            if (soloSafe) {
                [btnCurrentSolo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
                [btnCurrentSolo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
            } else {
                [btnCurrentSolo setTitle:@"SOLO" forState:UIControlStateNormal];
                [btnCurrentSolo setTitle:@"SOLO" forState:UIControlStateSelected];
            }
            currentSolo = (pkt->ch_1_16_solo_ctrl_on_off & 0x1000) >> 12;
            if (currentSolo != btnCurrentSolo.selected && ![viewController sendCommandExists:CMD_SOLO_CTRL dspId:DSP_5]) {
                btnCurrentSolo.selected = currentSolo;
            }
            currentOn = (pkt->ch_1_16_on_off & 0x1000) >> 12;
            if (currentOn != btnCurrentOn.selected && ![viewController sendCommandExists:CMD_CH_ON_OFF dspId:DSP_5]) {
                btnCurrentOn.selected = currentOn;
            }
            currentMeter = (pkt->ch_1_16_meter_pre_post & 0x1000) >> 12;
            if (currentMeter != btnCurrentMeter.selected && ![viewController sendCommandExists:CMD_CH_METER dspId:DSP_5]) {
                btnCurrentMeter.selected = currentMeter;
            }
            // Update DYN status
            if (btnDyn.selected) {
                // Check DYN status, green if ACTIVE, yellow if NOP
                if (pkt->channel_13_16_dyn_active & 0x01) {
                    [btnDynGate setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynGate setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->channel_13_16_dyn_active & 0x02) >> 1) {
                    [btnDynExpander setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynExpander setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->channel_13_16_dyn_active & 0x04) >> 2) {
                    [btnDynComp setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynComp setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->channel_13_16_dyn_active & 0x08) >> 3) {
                    [btnDynLimiter setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynLimiter setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
            }
            break;
        case CH_14:
            meterEqOut.progress = ((float)pkt->channel_14_eq_meter_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterDynOut.progress = ((float)pkt->channel_14_dyn_meter_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            if (![viewController sendCommandExists:CMD_CH14_CTRL dspId:DSP_4]) {
                [self refreshCtrlViews:pkt->channel_14_ctrl];
            }
            if (![viewController sendCommandExists:CMD_CH14_DELAY_TIME dspId:DSP_4]) {
                knobDelay.value = [Global delayToKnobValue:pkt->channel_14_delay_time];
                [self knobDelayDidChange:nil];
            }
            sliderPan.value = pkt->channel_14_pan_level + PAN_CENTER_VALUE;
            [self onSliderPanAction:nil];
            aux1PostPre = (pkt->aux_1_send_pre_post & 0x2000) >> 13;
            if (aux1PostPre != btnChannelAux1PostPre.selected && ![viewController sendCommandExists:CMD_AUX1_SEND dspId:DSP_5]) {
                btnChannelAux1PostPre.selected = aux1PostPre;
            }
            aux2PostPre = (pkt->aux_2_send_pre_post & 0x2000) >> 13;
            if (aux2PostPre != btnChannelAux2PostPre.selected && ![viewController sendCommandExists:CMD_AUX2_SEND dspId:DSP_5]) {
                btnChannelAux2PostPre.selected = aux2PostPre;
            }
            aux3PostPre = (pkt->aux_3_send_pre_post & 0x2000) >> 13;
            if (aux3PostPre != btnChannelAux3PostPre.selected && ![viewController sendCommandExists:CMD_AUX3_SEND dspId:DSP_5]) {
                btnChannelAux3PostPre.selected = aux3PostPre;
            }
            aux4PostPre = (pkt->aux_4_send_pre_post & 0x2000) >> 13;
            if (aux4PostPre != btnChannelAux4PostPre.selected && ![viewController sendCommandExists:CMD_AUX4_SEND dspId:DSP_5]) {
                btnChannelAux4PostPre.selected = aux4PostPre;
            }
            knobAux1.value = pkt->aux_1_ch_14_fader_level;
            [self knobAux1DidChange:nil];
            knobAux2.value = pkt->aux_2_ch_14_fader_level;
            [self knobAux2DidChange:nil];
            knobAux3.value = pkt->aux_3_ch_14_fader_level;
            [self knobAux3DidChange:nil];
            knobAux4.value = pkt->aux_4_ch_14_fader_level;
            [self knobAux4DidChange:nil];
            gp1OnOff = (pkt->ch_1_16_assign_gp_1 & 0x2000) >> 13;
            if (gp1OnOff != btnChannelGp1OnOff.selected && ![viewController sendCommandExists:CMD_GP1_ASSIGN dspId:DSP_5]) {
                btnChannelGp1OnOff.selected = gp1OnOff;
            }
            gp2OnOff = (pkt->ch_1_16_assign_gp_2 & 0x2000) >> 13;
            if (gp2OnOff != btnChannelGp2OnOff.selected && ![viewController sendCommandExists:CMD_GP2_ASSIGN dspId:DSP_5]) {
                btnChannelGp2OnOff.selected = gp2OnOff;
            }
            gp3OnOff = (pkt->ch_1_16_assign_gp_3 & 0x2000) >> 13;
            if (gp3OnOff != btnChannelGp3OnOff.selected && ![viewController sendCommandExists:CMD_GP3_ASSIGN dspId:DSP_5]) {
                btnChannelGp3OnOff.selected = gp3OnOff;
            }
            gp4OnOff = (pkt->ch_1_16_assign_gp_4 & 0x2000) >> 13;
            if (gp4OnOff != btnChannelGp4OnOff.selected && ![viewController sendCommandExists:CMD_GP4_ASSIGN dspId:DSP_5]) {
                btnChannelGp4OnOff.selected = gp4OnOff;
            }
            toMainOnOff = (pkt->ch_1_16_to_main_ctrl & 0x2000) >> 13;
            if (toMainOnOff != btnChannelToMainOnOff.selected && ![viewController sendCommandExists:CMD_CH_TO_MAIN_CTRL dspId:DSP_5]) {
                btnChannelToMainOnOff.selected = toMainOnOff;
            }
            if (!_sliderCurrentLock) {
                sliderCurrent.value = pkt->ch_14_fader_level;
                [lblSliderValue setText:[Global getSliderGain:sliderCurrent.value appendDb:YES]];
            }
            soloSafe = (pkt->ch_1_16_solo_safe_ctrl_on_off & 0x2000) >> 13;
            if (soloSafe) {
                [btnCurrentSolo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
                [btnCurrentSolo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
            } else {
                [btnCurrentSolo setTitle:@"SOLO" forState:UIControlStateNormal];
                [btnCurrentSolo setTitle:@"SOLO" forState:UIControlStateSelected];
            }
            currentSolo = (pkt->ch_1_16_solo_ctrl_on_off & 0x2000) >> 13;
            if (currentSolo != btnCurrentSolo.selected && ![viewController sendCommandExists:CMD_SOLO_CTRL dspId:DSP_5]) {
                btnCurrentSolo.selected = currentSolo;
            }
            currentOn = (pkt->ch_1_16_on_off & 0x2000) >> 13;
            if (currentOn != btnCurrentOn.selected && ![viewController sendCommandExists:CMD_CH_ON_OFF dspId:DSP_5]) {
                btnCurrentOn.selected = currentOn;
            }
            currentMeter = (pkt->ch_1_16_meter_pre_post & 0x2000) >> 13;
            if (currentMeter != btnCurrentMeter.selected && ![viewController sendCommandExists:CMD_CH_METER dspId:DSP_5]) {
                btnCurrentMeter.selected = currentMeter;
            }
            // Update DYN status
            if (btnDyn.selected) {
                // Check DYN status, green if ACTIVE, yellow if NOP
                if ((pkt->channel_13_16_dyn_active & 0x10) >> 4) {
                    [btnDynGate setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynGate setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->channel_13_16_dyn_active & 0x20) >> 5) {
                    [btnDynExpander setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynExpander setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->channel_13_16_dyn_active & 0x40) >> 6) {
                    [btnDynComp setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynComp setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->channel_13_16_dyn_active & 0x80) >> 7) {
                    [btnDynLimiter setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynLimiter setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
            }
            break;
        case CH_15:
            meterEqOut.progress = ((float)pkt->channel_15_eq_meter_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterDynOut.progress = ((float)pkt->channel_15_dyn_meter_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            if (![viewController sendCommandExists:CMD_CH15_CTRL dspId:DSP_4]) {
                [self refreshCtrlViews:pkt->channel_15_ctrl];
            }
            if (![viewController sendCommandExists:CMD_CH15_DELAY_TIME dspId:DSP_4]) {
                knobDelay.value = [Global delayToKnobValue:pkt->channel_15_delay_time];
                [self knobDelayDidChange:nil];
            }
            sliderPan.value = pkt->channel_15_pan_level + PAN_CENTER_VALUE;
            [self onSliderPanAction:nil];
            aux1PostPre = (pkt->aux_1_send_pre_post & 0x4000) >> 14;
            if (aux1PostPre != btnChannelAux1PostPre.selected && ![viewController sendCommandExists:CMD_AUX1_SEND dspId:DSP_5]) {
                btnChannelAux1PostPre.selected = aux1PostPre;
            }
            aux2PostPre = (pkt->aux_2_send_pre_post & 0x4000) >> 14;
            if (aux2PostPre != btnChannelAux2PostPre.selected && ![viewController sendCommandExists:CMD_AUX2_SEND dspId:DSP_5]) {
                btnChannelAux2PostPre.selected = aux2PostPre;
            }
            aux3PostPre = (pkt->aux_3_send_pre_post & 0x4000) >> 14;
            if (aux3PostPre != btnChannelAux3PostPre.selected && ![viewController sendCommandExists:CMD_AUX3_SEND dspId:DSP_5]) {
                btnChannelAux3PostPre.selected = aux3PostPre;
            }
            aux4PostPre = (pkt->aux_4_send_pre_post & 0x4000) >> 14;
            if (aux4PostPre != btnChannelAux4PostPre.selected && ![viewController sendCommandExists:CMD_AUX4_SEND dspId:DSP_5]) {
                btnChannelAux4PostPre.selected = aux4PostPre;
            }
            knobAux1.value = pkt->aux_1_ch_15_fader_level;
            [self knobAux1DidChange:nil];
            knobAux2.value = pkt->aux_2_ch_15_fader_level;
            [self knobAux2DidChange:nil];
            knobAux3.value = pkt->aux_3_ch_15_fader_level;
            [self knobAux3DidChange:nil];
            knobAux4.value = pkt->aux_4_ch_15_fader_level;
            [self knobAux4DidChange:nil];
            gp1OnOff = (pkt->ch_1_16_assign_gp_1 & 0x4000) >> 14;
            if (gp1OnOff != btnChannelGp1OnOff.selected && ![viewController sendCommandExists:CMD_GP1_ASSIGN dspId:DSP_5]) {
                btnChannelGp1OnOff.selected = gp1OnOff;
            }
            gp2OnOff = (pkt->ch_1_16_assign_gp_2 & 0x4000) >> 14;
            if (gp2OnOff != btnChannelGp2OnOff.selected && ![viewController sendCommandExists:CMD_GP2_ASSIGN dspId:DSP_5]) {
                btnChannelGp2OnOff.selected = gp2OnOff;
            }
            gp3OnOff = (pkt->ch_1_16_assign_gp_3 & 0x4000) >> 14;
            if (gp3OnOff != btnChannelGp3OnOff.selected && ![viewController sendCommandExists:CMD_GP3_ASSIGN dspId:DSP_5]) {
                btnChannelGp3OnOff.selected = gp3OnOff;
            }
            gp4OnOff = (pkt->ch_1_16_assign_gp_4 & 0x4000) >> 14;
            if (gp4OnOff != btnChannelGp4OnOff.selected && ![viewController sendCommandExists:CMD_GP4_ASSIGN dspId:DSP_5]) {
                btnChannelGp4OnOff.selected = gp4OnOff;
            }
            toMainOnOff = (pkt->ch_1_16_to_main_ctrl & 0x4000) >> 14;
            if (toMainOnOff != btnChannelToMainOnOff.selected && ![viewController sendCommandExists:CMD_CH_TO_MAIN_CTRL dspId:DSP_5]) {
                btnChannelToMainOnOff.selected = toMainOnOff;
            }
            if (!_sliderCurrentLock) {
                sliderCurrent.value = pkt->ch_15_fader_level;
                [lblSliderValue setText:[Global getSliderGain:sliderCurrent.value appendDb:YES]];
            }
            soloSafe = (pkt->ch_1_16_solo_safe_ctrl_on_off & 0x4000) >> 14;
            if (soloSafe) {
                [btnCurrentSolo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
                [btnCurrentSolo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
            } else {
                [btnCurrentSolo setTitle:@"SOLO" forState:UIControlStateNormal];
                [btnCurrentSolo setTitle:@"SOLO" forState:UIControlStateSelected];
            }
            currentSolo = (pkt->ch_1_16_solo_ctrl_on_off & 0x4000) >> 14;
            if (currentSolo != btnCurrentSolo.selected && ![viewController sendCommandExists:CMD_SOLO_CTRL dspId:DSP_5]) {
                btnCurrentSolo.selected = currentSolo;
            }
            currentOn = (pkt->ch_1_16_on_off & 0x4000) >> 14;
            if (currentOn != btnCurrentOn.selected && ![viewController sendCommandExists:CMD_CH_ON_OFF dspId:DSP_5]) {
                btnCurrentOn.selected = currentOn;
            }
            currentMeter = (pkt->ch_1_16_meter_pre_post & 0x4000) >> 14;
            if (currentMeter != btnCurrentMeter.selected && ![viewController sendCommandExists:CMD_CH_METER dspId:DSP_5]) {
                btnCurrentMeter.selected = currentMeter;
            }
            // Update DYN status
            if (btnDyn.selected) {
                // Check DYN status, green if ACTIVE, yellow if NOP
                if ((pkt->channel_13_16_dyn_active & 0x0100) >> 8) {
                    [btnDynGate setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynGate setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->channel_13_16_dyn_active & 0x0200) >> 9) {
                    [btnDynExpander setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynExpander setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->channel_13_16_dyn_active & 0x0400) >> 10) {
                    [btnDynComp setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynComp setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->channel_13_16_dyn_active & 0x0800) >> 11) {
                    [btnDynLimiter setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynLimiter setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
            }
            break;
        case CH_16:
            meterEqOut.progress = ((float)pkt->channel_16_eq_meter_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterDynOut.progress = ((float)pkt->channel_16_dyn_meter_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            if (![viewController sendCommandExists:CMD_CH16_CTRL dspId:DSP_4]) {
                [self refreshCtrlViews:pkt->channel_16_ctrl];
            }
            if (![viewController sendCommandExists:CMD_CH16_DELAY_TIME dspId:DSP_4]) {
                knobDelay.value = [Global delayToKnobValue:pkt->channel_16_delay_time];
                [self knobDelayDidChange:nil];
            }
            sliderPan.value = pkt->channel_16_pan_level + PAN_CENTER_VALUE;
            [self onSliderPanAction:nil];
            aux1PostPre = (pkt->aux_1_send_pre_post & 0x8000) >> 15;
            if (aux1PostPre != btnChannelAux1PostPre.selected && ![viewController sendCommandExists:CMD_AUX1_SEND dspId:DSP_5]) {
                btnChannelAux1PostPre.selected = aux1PostPre;
            }
            aux2PostPre = (pkt->aux_2_send_pre_post & 0x8000) >> 15;
            if (aux2PostPre != btnChannelAux2PostPre.selected && ![viewController sendCommandExists:CMD_AUX2_SEND dspId:DSP_5]) {
                btnChannelAux2PostPre.selected = aux2PostPre;
            }
            aux3PostPre = (pkt->aux_3_send_pre_post & 0x8000) >> 15;
            if (aux3PostPre != btnChannelAux3PostPre.selected && ![viewController sendCommandExists:CMD_AUX3_SEND dspId:DSP_5]) {
                btnChannelAux3PostPre.selected = aux3PostPre;
            }
            aux4PostPre = (pkt->aux_4_send_pre_post & 0x8000) >> 15;
            if (aux4PostPre != btnChannelAux4PostPre.selected && ![viewController sendCommandExists:CMD_AUX4_SEND dspId:DSP_5]) {
                btnChannelAux4PostPre.selected = aux4PostPre;
            }
            knobAux1.value = pkt->aux_1_ch_16_fader_level;
            [self knobAux1DidChange:nil];
            knobAux2.value = pkt->aux_2_ch_16_fader_level;
            [self knobAux2DidChange:nil];
            knobAux3.value = pkt->aux_3_ch_16_fader_level;
            [self knobAux3DidChange:nil];
            knobAux4.value = pkt->aux_4_ch_16_fader_level;
            [self knobAux4DidChange:nil];
            gp1OnOff = (pkt->ch_1_16_assign_gp_1 & 0x8000) >> 15;
            if (gp1OnOff != btnChannelGp1OnOff.selected && ![viewController sendCommandExists:CMD_GP1_ASSIGN dspId:DSP_5]) {
                btnChannelGp1OnOff.selected = gp1OnOff;
            }
            gp2OnOff = (pkt->ch_1_16_assign_gp_2 & 0x8000) >> 15;
            if (gp2OnOff != btnChannelGp2OnOff.selected && ![viewController sendCommandExists:CMD_GP2_ASSIGN dspId:DSP_5]) {
                btnChannelGp2OnOff.selected = gp2OnOff;
            }
            gp3OnOff = (pkt->ch_1_16_assign_gp_3 & 0x8000) >> 15;
            if (gp3OnOff != btnChannelGp3OnOff.selected && ![viewController sendCommandExists:CMD_GP3_ASSIGN dspId:DSP_5]) {
                btnChannelGp3OnOff.selected = gp3OnOff;
            }
            gp4OnOff = (pkt->ch_1_16_assign_gp_4 & 0x8000) >> 15;
            if (gp4OnOff != btnChannelGp4OnOff.selected && ![viewController sendCommandExists:CMD_GP4_ASSIGN dspId:DSP_5]) {
                btnChannelGp4OnOff.selected = gp4OnOff;
            }
            toMainOnOff = (pkt->ch_1_16_to_main_ctrl & 0x8000) >> 15;
            if (toMainOnOff != btnChannelToMainOnOff.selected && ![viewController sendCommandExists:CMD_CH_TO_MAIN_CTRL dspId:DSP_5]) {
                btnChannelToMainOnOff.selected = toMainOnOff;
            }
            if (!_sliderCurrentLock) {
                sliderCurrent.value = pkt->ch_16_fader_level;
                [lblSliderValue setText:[Global getSliderGain:sliderCurrent.value appendDb:YES]];
            }
            soloSafe = (pkt->ch_1_16_solo_safe_ctrl_on_off & 0x8000) >> 15;
            if (soloSafe) {
                [btnCurrentSolo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
                [btnCurrentSolo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
            } else {
                [btnCurrentSolo setTitle:@"SOLO" forState:UIControlStateNormal];
                [btnCurrentSolo setTitle:@"SOLO" forState:UIControlStateSelected];
            }
            currentSolo = (pkt->ch_1_16_solo_ctrl_on_off & 0x8000) >> 15;
            if (currentSolo != btnCurrentSolo.selected && ![viewController sendCommandExists:CMD_SOLO_CTRL dspId:DSP_5]) {
                btnCurrentSolo.selected = currentSolo;
            }
            currentOn = (pkt->ch_1_16_on_off & 0x8000) >> 15;
            if (currentOn != btnCurrentOn.selected && ![viewController sendCommandExists:CMD_CH_ON_OFF dspId:DSP_5]) {
                btnCurrentOn.selected = currentOn;
            }
            currentMeter = (pkt->ch_1_16_meter_pre_post & 0x8000) >> 15;
            if (currentMeter != btnCurrentMeter.selected && ![viewController sendCommandExists:CMD_CH_METER dspId:DSP_5]) {
                btnCurrentMeter.selected = currentMeter;
            }
            if (btnDyn.selected) {
                // Check DYN status, green if ACTIVE, yellow if NOP
                if ((pkt->channel_13_16_dyn_active & 0x1000) >> 12) {
                    [btnDynGate setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynGate setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->channel_13_16_dyn_active & 0x2000) >> 13) {
                    [btnDynExpander setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynExpander setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->channel_13_16_dyn_active & 0x4000) >> 14) {
                    [btnDynComp setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynComp setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->channel_13_16_dyn_active & 0x8000) >> 15) {
                    [btnDynLimiter setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynLimiter setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
            }
            break;
        default:
            break;
    }
}

- (void)processUsbAudioViewPage:(ViewPageUsbAudioPacket *)pkt
{
    _ch17Ctrl = pkt->ch_17_ctrl;
    _ch18Ctrl = pkt->ch_18_ctrl;
    _ch17AudioSetting = pkt->ch_17_audio_setting;
    _ch18AudioSetting = pkt->ch_18_audio_setting;
    
    switch (currentChannel) {
        case CH_17:
            meterEqOut.progress = ((float)pkt->ch_17_meter_eq_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterDynOut.progress = ((float)pkt->ch_17_meter_dyn_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            if (![viewController sendCommandExists:CMD_CH17_CTRL dspId:DSP_6]) {
                [self refreshCtrlViews:pkt->ch_17_ctrl];
            }
            if (![viewController sendCommandExists:CMD_CH17_DELAY_TIME dspId:DSP_6]) {
                knobDelay.value = [Global delayToKnobValue:pkt->ch_17_delay_time];
                [self knobDelayDidChange:nil];
            }
            sliderPan.value = pkt->ch_17_pan + PAN_CENTER_VALUE;
            [self onSliderPanAction:nil];
            BOOL aux1PostPre = (pkt->ch_17_audio_setting & 0x10) >> 4;
            if (aux1PostPre != btnChannelAux1PostPre.selected && ![viewController sendCommandExists:CMD_CH17_USB_AUDIO_SETTING dspId:DSP_5]) {
                btnChannelAux1PostPre.selected = aux1PostPre;
            }
            BOOL aux2PostPre = (pkt->ch_17_audio_setting & 0x20) >> 5;
            if (aux2PostPre != btnChannelAux2PostPre.selected && ![viewController sendCommandExists:CMD_CH17_USB_AUDIO_SETTING dspId:DSP_5]) {
                btnChannelAux2PostPre.selected = aux2PostPre;
            }
            BOOL aux3PostPre = (pkt->ch_17_audio_setting & 0x40) >> 6;
            if (aux3PostPre != btnChannelAux3PostPre.selected && ![viewController sendCommandExists:CMD_CH17_USB_AUDIO_SETTING dspId:DSP_5]) {
                btnChannelAux3PostPre.selected = aux3PostPre;
            }
            BOOL aux4PostPre = (pkt->ch_17_audio_setting & 0x80) >> 7;
            if (aux4PostPre != btnChannelAux4PostPre.selected && ![viewController sendCommandExists:CMD_CH17_USB_AUDIO_SETTING dspId:DSP_5]) {
                btnChannelAux4PostPre.selected = aux4PostPre;
            }
            knobAux1.value = pkt->aux_1_ch_17_level;
            [self knobAux1DidChange:nil];
            knobAux2.value = pkt->aux_2_ch_17_level;
            [self knobAux2DidChange:nil];
            knobAux3.value = pkt->aux_3_ch_17_level;
            [self knobAux3DidChange:nil];
            knobAux4.value = pkt->aux_4_ch_17_level;
            [self knobAux4DidChange:nil];
            BOOL gp1OnOff = pkt->ch_17_audio_setting & 0x01;
            if (gp1OnOff != btnChannelGp1OnOff.selected && ![viewController sendCommandExists:CMD_CH17_USB_AUDIO_SETTING dspId:DSP_5]) {
                btnChannelGp1OnOff.selected = gp1OnOff;
            }
            BOOL gp2OnOff = (pkt->ch_17_audio_setting & 0x02) >> 1;
            if (gp2OnOff != btnChannelGp2OnOff.selected && ![viewController sendCommandExists:CMD_CH17_USB_AUDIO_SETTING dspId:DSP_5]) {
                btnChannelGp2OnOff.selected = gp2OnOff;
            }
            BOOL gp3OnOff = (pkt->ch_17_audio_setting & 0x04) >> 2;
            if (gp3OnOff != btnChannelGp3OnOff.selected && ![viewController sendCommandExists:CMD_CH17_USB_AUDIO_SETTING dspId:DSP_5]) {
                btnChannelGp3OnOff.selected = gp3OnOff;
            }
            BOOL gp4OnOff = (pkt->ch_17_audio_setting & 0x08) >> 3;
            if (gp4OnOff != btnChannelGp4OnOff.selected && ![viewController sendCommandExists:CMD_CH17_USB_AUDIO_SETTING dspId:DSP_5]) {
                btnChannelGp4OnOff.selected = gp4OnOff;
            }
            BOOL toMainOnOff = (pkt->ch_17_audio_setting & 0x0200) >> 9;
            if (toMainOnOff != btnChannelToMainOnOff.selected && ![viewController sendCommandExists:CMD_CH17_USB_AUDIO_SETTING dspId:DSP_5]) {
                btnChannelToMainOnOff.selected = toMainOnOff;
            }
            if (!_sliderCurrentLock) {
                sliderCurrent.value = pkt->ch_17_fader;
                [lblSliderValue setText:[Global getSliderGain:sliderCurrent.value appendDb:YES]];
            }
            BOOL soloSafe = (pkt->ch_17_audio_setting & 0x0800) >> 11;
            if (soloSafe) {
                [btnCurrentSolo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
                [btnCurrentSolo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
            } else {
                [btnCurrentSolo setTitle:@"SOLO" forState:UIControlStateNormal];
                [btnCurrentSolo setTitle:@"SOLO" forState:UIControlStateSelected];
            }
            BOOL currentSolo = (pkt->ch_17_audio_setting & 0x0400) >> 10;
            if (currentSolo != btnCurrentSolo.selected && ![viewController sendCommandExists:CMD_CH17_USB_AUDIO_SETTING dspId:DSP_5]) {
                btnCurrentSolo.selected = currentSolo;
            }
            BOOL currentOn = (pkt->ch_17_audio_setting & 0x0100) >> 8;
            if (currentOn != btnCurrentOn.selected && ![viewController sendCommandExists:CMD_CH17_USB_AUDIO_SETTING dspId:DSP_5]) {
                btnCurrentOn.selected = currentOn;
            }
            BOOL currentMeter = (pkt->ch_17_audio_setting & 0x8000) >> 15;
            if (currentMeter != btnCurrentMeter.selected && ![viewController sendCommandExists:CMD_CH17_USB_AUDIO_SETTING dspId:DSP_5]) {
                btnCurrentMeter.selected = currentMeter;
            }
            // Update DYN status
            if (btnDyn.selected) {
                // Check DYN status, green if ACTIVE, yellow if NOP
                if ((pkt->main_l_r_ch_17_18_dyn_status & 0x0100) >> 8) {
                    [btnDynGate setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynGate setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->main_l_r_ch_17_18_dyn_status & 0x0200) >> 9) {
                    [btnDynExpander setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynExpander setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->main_l_r_ch_17_18_dyn_status & 0x0400) >> 10) {
                    [btnDynComp setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynComp setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->main_l_r_ch_17_18_dyn_status & 0x0800) >> 11) {
                    [btnDynLimiter setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynLimiter setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
            }
            break;
        case CH_18:
            meterEqOut.progress = ((float)pkt->ch_18_meter_eq_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterDynOut.progress = ((float)pkt->ch_18_meter_dyn_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            if (![viewController sendCommandExists:CMD_CH18_CTRL dspId:DSP_6]) {
                [self refreshCtrlViews:pkt->ch_18_ctrl];
            }
            if (![viewController sendCommandExists:CMD_CH18_DELAY_TIME dspId:DSP_6]) {
                knobDelay.value = [Global delayToKnobValue:pkt->ch_18_delay_time];
                [self knobDelayDidChange:nil];
            }
            sliderPan.value = pkt->ch_18_pan + PAN_CENTER_VALUE;
            [self onSliderPanAction:nil];
            aux1PostPre = (pkt->ch_18_audio_setting & 0x10) >> 4;
            if (aux1PostPre != btnChannelAux1PostPre.selected && ![viewController sendCommandExists:CMD_CH18_USB_AUDIO_SETTING dspId:DSP_5]) {
                btnChannelAux1PostPre.selected = aux1PostPre;
            }
            aux2PostPre = (pkt->ch_18_audio_setting & 0x20) >> 5;
            if (aux2PostPre != btnChannelAux2PostPre.selected && ![viewController sendCommandExists:CMD_CH18_USB_AUDIO_SETTING dspId:DSP_5]) {
                btnChannelAux2PostPre.selected = aux2PostPre;
            }
            aux3PostPre = (pkt->ch_18_audio_setting & 0x40) >> 6;
            if (aux3PostPre != btnChannelAux3PostPre.selected && ![viewController sendCommandExists:CMD_CH18_USB_AUDIO_SETTING dspId:DSP_5]) {
                btnChannelAux3PostPre.selected = aux3PostPre;
            }
            aux4PostPre = (pkt->ch_18_audio_setting & 0x80) >> 7;
            if (aux4PostPre != btnChannelAux4PostPre.selected && ![viewController sendCommandExists:CMD_CH18_USB_AUDIO_SETTING dspId:DSP_5]) {
                btnChannelAux4PostPre.selected = aux4PostPre;
            }
            knobAux1.value = pkt->aux_1_ch_18_level;
            [self knobAux1DidChange:nil];
            knobAux2.value = pkt->aux_2_ch_18_level;
            [self knobAux2DidChange:nil];
            knobAux3.value = pkt->aux_3_ch_18_level;
            [self knobAux3DidChange:nil];
            knobAux4.value = pkt->aux_4_ch_18_level;
            [self knobAux4DidChange:nil];
            gp1OnOff = pkt->ch_18_audio_setting & 0x01;
            if (gp1OnOff != btnChannelGp1OnOff.selected && ![viewController sendCommandExists:CMD_CH18_USB_AUDIO_SETTING dspId:DSP_5]) {
                btnChannelGp1OnOff.selected = gp1OnOff;
            }
            gp2OnOff = (pkt->ch_18_audio_setting & 0x02) >> 1;
            if (gp2OnOff != btnChannelGp2OnOff.selected && ![viewController sendCommandExists:CMD_CH18_USB_AUDIO_SETTING dspId:DSP_5]) {
                btnChannelGp2OnOff.selected = gp2OnOff;
            }
            gp3OnOff = (pkt->ch_18_audio_setting & 0x04) >> 2;
            if (gp3OnOff != btnChannelGp3OnOff.selected && ![viewController sendCommandExists:CMD_CH18_USB_AUDIO_SETTING dspId:DSP_5]) {
                btnChannelGp3OnOff.selected = gp3OnOff;
            }
            gp4OnOff = (pkt->ch_18_audio_setting & 0x08) >> 3;
            if (gp4OnOff != btnChannelGp4OnOff.selected && ![viewController sendCommandExists:CMD_CH18_USB_AUDIO_SETTING dspId:DSP_5]) {
                btnChannelGp4OnOff.selected = gp4OnOff;
            }
            toMainOnOff = (pkt->ch_18_audio_setting & 0x0200) >> 9;
            if (toMainOnOff != btnChannelToMainOnOff.selected && ![viewController sendCommandExists:CMD_CH18_USB_AUDIO_SETTING dspId:DSP_5]) {
                btnChannelToMainOnOff.selected = toMainOnOff;
            }
            if (!_sliderCurrentLock) {
                sliderCurrent.value = pkt->ch_18_fader;
                [lblSliderValue setText:[Global getSliderGain:sliderCurrent.value appendDb:YES]];
            }
            soloSafe = (pkt->ch_18_audio_setting & 0x0800) >> 11;
            if (soloSafe) {
                [btnCurrentSolo setTitle:@"SOLO SAFE" forState:UIControlStateNormal];
                [btnCurrentSolo setTitle:@"SOLO SAFE" forState:UIControlStateSelected];
            } else {
                [btnCurrentSolo setTitle:@"SOLO" forState:UIControlStateNormal];
                [btnCurrentSolo setTitle:@"SOLO" forState:UIControlStateSelected];
            }
            currentSolo = (pkt->ch_18_audio_setting & 0x0400) >> 10;
            if (currentSolo != btnCurrentSolo.selected && ![viewController sendCommandExists:CMD_CH18_USB_AUDIO_SETTING dspId:DSP_5]) {
                btnCurrentSolo.selected = currentSolo;
            }
            currentOn = (pkt->ch_18_audio_setting & 0x0100) >> 8;
            if (currentOn != btnCurrentOn.selected && ![viewController sendCommandExists:CMD_CH18_USB_AUDIO_SETTING dspId:DSP_5]) {
                btnCurrentOn.selected = currentOn;
            }
            currentMeter = (pkt->ch_18_audio_setting & 0x8000) >> 15;
            if (currentMeter != btnCurrentMeter.selected && ![viewController sendCommandExists:CMD_CH18_USB_AUDIO_SETTING dspId:DSP_5]) {
                btnCurrentMeter.selected = currentMeter;
            }
            // Update DYN status
            if (btnDyn.selected) {
                // Check DYN status, green if ACTIVE, yellow if NOP
                if ((pkt->main_l_r_ch_17_18_dyn_status & 0x1000) >> 12) {
                    [btnDynGate setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynGate setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->main_l_r_ch_17_18_dyn_status & 0x2000) >> 13) {
                    [btnDynExpander setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynExpander setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->main_l_r_ch_17_18_dyn_status & 0x4000) >> 14) {
                    [btnDynComp setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynComp setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->main_l_r_ch_17_18_dyn_status & 0x8000) >> 15) {
                    [btnDynLimiter setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynLimiter setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
            }
            break;
        default:
            break;
    }
}

- (void)processMultiViewPage:(ViewPageMultiPacket *)pkt
{
    _multi1Ctrl = pkt->multi_1_ctrl;
    _multi2Ctrl = pkt->multi_2_ctrl;
    _multi3Ctrl = pkt->multi_3_ctrl;
    _multi4Ctrl = pkt->multi_4_ctrl;
    _multi_1_4MeterPrePost = pkt->multi_1_4_meter_pre_post;
 
    switch (currentChannel) {
        case MT_1:
            meterEqOut.progress = ((float)pkt->multi_1_meter_eq_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterDynOut.progress = ((float)pkt->multi_1_meter_dyn_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            if (![viewController sendCommandExists:CMD_MULTI_1_CTRL dspId:DSP_7]) {
                [self refreshCtrlViews:pkt->multi_1_ctrl];
            }
            if (![viewController sendCommandExists:CMD_MULTI1_DELAY_TIME dspId:DSP_7]) {
                knobDelay.value = [Global delayToKnobValue:pkt->multi_1_delay_time];
                [self knobDelayDidChange:nil];
            }
            BOOL aux1OnOff = (pkt->multi_1_source & 0x0100) >> 8;
            if (aux1OnOff != btnMultiAux1OnOff.selected && ![viewController sendCommandExists:CMD_MULTI1_SOURCE dspId:DSP_7]) {
                btnMultiAux1OnOff.selected = aux1OnOff;
            }
            BOOL aux2OnOff = (pkt->multi_1_source & 0x0200) >> 9;
            if (aux2OnOff != btnMultiAux2OnOff.selected && ![viewController sendCommandExists:CMD_MULTI1_SOURCE dspId:DSP_7]) {
                btnMultiAux2OnOff.selected = aux2OnOff;
            }
            BOOL aux3OnOff = (pkt->multi_1_source & 0x0400) >> 10;
            if (aux3OnOff != btnMultiAux3OnOff.selected && ![viewController sendCommandExists:CMD_MULTI1_SOURCE dspId:DSP_7]) {
                btnMultiAux3OnOff.selected = aux3OnOff;
            }
            BOOL aux4OnOff = (pkt->multi_1_source & 0x0800) >> 11;
            if (aux4OnOff != btnMultiAux4OnOff.selected && ![viewController sendCommandExists:CMD_MULTI1_SOURCE dspId:DSP_7]) {
                btnMultiAux4OnOff.selected = aux4OnOff;
            }
            BOOL gp1OnOff = pkt->multi_1_source & 0x01;
            if (gp1OnOff != btnMultiGp1OnOff.selected && ![viewController sendCommandExists:CMD_MULTI1_SOURCE dspId:DSP_7]) {
                btnMultiGp1OnOff.selected = gp1OnOff;
            }
            BOOL gp2OnOff = (pkt->multi_1_source & 0x02) >> 1;
            if (gp2OnOff != btnMultiGp2OnOff.selected && ![viewController sendCommandExists:CMD_MULTI1_SOURCE dspId:DSP_7]) {
                btnMultiGp2OnOff.selected = gp2OnOff;
            }
            BOOL gp3OnOff = (pkt->multi_1_source & 0x04) >> 2;
            if (gp3OnOff != btnMultiGp3OnOff.selected && ![viewController sendCommandExists:CMD_MULTI1_SOURCE dspId:DSP_7]) {
                btnMultiGp3OnOff.selected = gp3OnOff;
            }
            BOOL gp4OnOff = (pkt->multi_1_source & 0x08) >> 3;
            if (gp4OnOff != btnMultiGp4OnOff.selected && ![viewController sendCommandExists:CMD_MULTI1_SOURCE dspId:DSP_7]) {
                btnMultiGp4OnOff.selected = gp4OnOff;
            }
            BOOL currentOn = pkt->multi_1_ctrl & 0x01;
            if (currentOn != btnCurrentOn.selected && ![viewController sendCommandExists:CMD_MULTI_1_CTRL dspId:DSP_7]) {
                btnCurrentOn.selected = currentOn;
            }
            BOOL currentMeter = pkt->multi_1_4_meter_pre_post & 0x01;
            if (currentMeter != btnCurrentMeter.selected && ![viewController sendCommandExists:CMD_MULTI_METER dspId:DSP_7]) {
                btnCurrentMeter.selected = currentMeter;
            }
            if (!_sliderCurrentLock) {
                sliderCurrent.value = pkt->multi_1_fader;
                [lblSliderValue setText:[Global getSliderGain:sliderCurrent.value appendDb:YES]];
            }
            // Update DYN status
            if (btnDyn.selected) {
                // Check DYN status, green if ACTIVE, yellow if NOP
                if (pkt->multi_1_4_dyn_status & 0x01) {
                    [btnDynGate setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynGate setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->multi_1_4_dyn_status & 0x02) >> 1) {
                    [btnDynExpander setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynExpander setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->multi_1_4_dyn_status & 0x04) >> 2) {
                    [btnDynComp setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynComp setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->multi_1_4_dyn_status & 0x08) >> 3) {
                    [btnDynLimiter setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynLimiter setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
            }
            break;
        case MT_2:
            meterEqOut.progress = ((float)pkt->multi_2_meter_eq_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterDynOut.progress = ((float)pkt->multi_2_meter_dyn_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            if (![viewController sendCommandExists:CMD_MULTI_2_CTRL dspId:DSP_7]) {
                [self refreshCtrlViews:pkt->multi_2_ctrl];
            }
            if (![viewController sendCommandExists:CMD_MULTI2_DELAY_TIME dspId:DSP_7]) {
                knobDelay.value = [Global delayToKnobValue:pkt->multi_2_delay_time];
                [self knobDelayDidChange:nil];
            }
            aux1OnOff = (pkt->multi_2_source & 0x0100) >> 8;
            if (aux1OnOff != btnMultiAux1OnOff.selected && ![viewController sendCommandExists:CMD_MULTI2_SOURCE dspId:DSP_7]) {
                btnMultiAux1OnOff.selected = aux1OnOff;
            }
            aux2OnOff = (pkt->multi_2_source & 0x0200) >> 9;
            if (aux2OnOff != btnMultiAux2OnOff.selected && ![viewController sendCommandExists:CMD_MULTI2_SOURCE dspId:DSP_7]) {
                btnMultiAux2OnOff.selected = aux2OnOff;
            }
            aux3OnOff = (pkt->multi_2_source & 0x0400) >> 10;
            if (aux3OnOff != btnMultiAux3OnOff.selected && ![viewController sendCommandExists:CMD_MULTI2_SOURCE dspId:DSP_7]) {
                btnMultiAux3OnOff.selected = aux3OnOff;
            }
            aux4OnOff = (pkt->multi_2_source & 0x0800) >> 11;
            if (aux4OnOff != btnMultiAux4OnOff.selected && ![viewController sendCommandExists:CMD_MULTI2_SOURCE dspId:DSP_7]) {
                btnMultiAux4OnOff.selected = aux4OnOff;
            }
            gp1OnOff = pkt->multi_2_source & 0x01;
            if (gp1OnOff != btnMultiGp1OnOff.selected && ![viewController sendCommandExists:CMD_MULTI2_SOURCE dspId:DSP_7]) {
                btnMultiGp1OnOff.selected = gp1OnOff;
            }
            gp2OnOff = (pkt->multi_2_source & 0x02) >> 1;
            if (gp2OnOff != btnMultiGp2OnOff.selected && ![viewController sendCommandExists:CMD_MULTI2_SOURCE dspId:DSP_7]) {
                btnMultiGp2OnOff.selected = gp2OnOff;
            }
            gp3OnOff = (pkt->multi_2_source & 0x04) >> 2;
            if (gp3OnOff != btnMultiGp3OnOff.selected && ![viewController sendCommandExists:CMD_MULTI2_SOURCE dspId:DSP_7]) {
                btnMultiGp3OnOff.selected = gp3OnOff;
            }
            gp4OnOff = (pkt->multi_2_source & 0x08) >> 3;
            if (gp4OnOff != btnMultiGp4OnOff.selected && ![viewController sendCommandExists:CMD_MULTI2_SOURCE dspId:DSP_7]) {
                btnMultiGp4OnOff.selected = gp4OnOff;
            }
            currentOn = pkt->multi_2_ctrl & 0x01;
            if (currentOn != btnCurrentOn.selected && ![viewController sendCommandExists:CMD_MULTI_2_CTRL dspId:DSP_7]) {
                btnCurrentOn.selected = currentOn;
            }
            currentMeter = (pkt->multi_1_4_meter_pre_post & 0x02) >> 1;
            if (currentMeter != btnCurrentMeter.selected && ![viewController sendCommandExists:CMD_MULTI_METER dspId:DSP_7]) {
                btnCurrentMeter.selected = currentMeter;
            }
            if (!_sliderCurrentLock) {
                sliderCurrent.value = pkt->multi_2_fader;
                [lblSliderValue setText:[Global getSliderGain:sliderCurrent.value appendDb:YES]];
            }
            // Update DYN status
            if (btnDyn.selected) {
                // Check DYN status, green if ACTIVE, yellow if NOP
                if ((pkt->multi_1_4_dyn_status & 0x10) >> 4) {
                    [btnDynGate setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynGate setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->multi_1_4_dyn_status & 0x20) >> 5) {
                    [btnDynExpander setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynExpander setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->multi_1_4_dyn_status & 0x40) >> 6) {
                    [btnDynComp setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynComp setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->multi_1_4_dyn_status & 0x80) >> 7) {
                    [btnDynLimiter setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynLimiter setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
            }
            break;
        case MT_3:
            meterEqOut.progress = ((float)pkt->multi_3_meter_eq_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterDynOut.progress = ((float)pkt->multi_3_meter_dyn_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            if (![viewController sendCommandExists:CMD_MULTI_3_CTRL dspId:DSP_7]) {
                [self refreshCtrlViews:pkt->multi_3_ctrl];
            }
            if (![viewController sendCommandExists:CMD_MULTI3_DELAY_TIME dspId:DSP_7]) {
                knobDelay.value = [Global delayToKnobValue:pkt->multi_3_delay_time];
                [self knobDelayDidChange:nil];
            }
            aux1OnOff = (pkt->multi_3_source & 0x0100) >> 8;
            if (aux1OnOff != btnMultiAux1OnOff.selected && ![viewController sendCommandExists:CMD_MULTI3_SOURCE dspId:DSP_7]) {
                btnMultiAux1OnOff.selected = aux1OnOff;
            }
            aux2OnOff = (pkt->multi_3_source & 0x0200) >> 9;
            if (aux2OnOff != btnMultiAux2OnOff.selected && ![viewController sendCommandExists:CMD_MULTI3_SOURCE dspId:DSP_7]) {
                btnMultiAux2OnOff.selected = aux2OnOff;
            }
            aux3OnOff = (pkt->multi_3_source & 0x0400) >> 10;
            if (aux3OnOff != btnMultiAux3OnOff.selected && ![viewController sendCommandExists:CMD_MULTI3_SOURCE dspId:DSP_7]) {
                btnMultiAux3OnOff.selected = aux3OnOff;
            }
            aux4OnOff = (pkt->multi_3_source & 0x0800) >> 11;
            if (aux4OnOff != btnMultiAux4OnOff.selected && ![viewController sendCommandExists:CMD_MULTI3_SOURCE dspId:DSP_7]) {
                btnMultiAux4OnOff.selected = aux4OnOff;
            }
            gp1OnOff = pkt->multi_3_source & 0x01;
            if (gp1OnOff != btnMultiGp1OnOff.selected && ![viewController sendCommandExists:CMD_MULTI3_SOURCE dspId:DSP_7]) {
                btnMultiGp1OnOff.selected = gp1OnOff;
            }
            gp2OnOff = (pkt->multi_3_source & 0x02) >> 1;
            if (gp2OnOff != btnMultiGp2OnOff.selected && ![viewController sendCommandExists:CMD_MULTI3_SOURCE dspId:DSP_7]) {
                btnMultiGp2OnOff.selected = gp2OnOff;
            }
            gp3OnOff = (pkt->multi_3_source & 0x04) >> 2;
            if (gp3OnOff != btnMultiGp3OnOff.selected && ![viewController sendCommandExists:CMD_MULTI3_SOURCE dspId:DSP_7]) {
                btnMultiGp3OnOff.selected = gp3OnOff;
            }
            gp4OnOff = (pkt->multi_3_source & 0x08) >> 3;
            if (gp4OnOff != btnMultiGp4OnOff.selected && ![viewController sendCommandExists:CMD_MULTI3_SOURCE dspId:DSP_7]) {
                btnMultiGp4OnOff.selected = gp4OnOff;
            }
            currentOn = pkt->multi_3_ctrl & 0x01;
            if (currentOn != btnCurrentOn.selected && ![viewController sendCommandExists:CMD_MULTI_3_CTRL dspId:DSP_7]) {
                btnCurrentOn.selected = currentOn;
            }
            currentMeter = (pkt->multi_1_4_meter_pre_post & 0x04) >> 2;
            if (currentMeter != btnCurrentMeter.selected && ![viewController sendCommandExists:CMD_MULTI_METER dspId:DSP_7]) {
                btnCurrentMeter.selected = currentMeter;
            }
            if (!_sliderCurrentLock) {
                sliderCurrent.value = pkt->multi_3_fader;
                [lblSliderValue setText:[Global getSliderGain:sliderCurrent.value appendDb:YES]];
            }
            // Update DYN status
            if (btnDyn.selected) {
                // Check DYN status, green if ACTIVE, yellow if NOP
                if ((pkt->multi_1_4_dyn_status & 0x0100) >> 8) {
                    [btnDynGate setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynGate setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->multi_1_4_dyn_status & 0x0200) >> 9) {
                    [btnDynExpander setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynExpander setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->multi_1_4_dyn_status & 0x0400) >> 10) {
                    [btnDynComp setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynComp setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->multi_1_4_dyn_status & 0x0800) >> 11) {
                    [btnDynLimiter setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynLimiter setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
            }
            break;
        case MT_4:
            meterEqOut.progress = ((float)pkt->multi_4_meter_eq_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterDynOut.progress = ((float)pkt->multi_4_meter_dyn_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            if (![viewController sendCommandExists:CMD_MULTI_4_CTRL dspId:DSP_7]) {
                [self refreshCtrlViews:pkt->multi_4_ctrl];
            }
            if (![viewController sendCommandExists:CMD_MULTI4_DELAY_TIME dspId:DSP_7]) {
                knobDelay.value = [Global delayToKnobValue:pkt->multi_4_delay_time];
                [self knobDelayDidChange:nil];
            }
            aux1OnOff = (pkt->multi_4_source & 0x0100) >> 8;
            if (aux1OnOff != btnMultiAux1OnOff.selected && ![viewController sendCommandExists:CMD_MULTI4_SOURCE dspId:DSP_7]) {
                btnMultiAux1OnOff.selected = aux1OnOff;
            }
            aux2OnOff = (pkt->multi_4_source & 0x0200) >> 9;
            if (aux2OnOff != btnMultiAux2OnOff.selected && ![viewController sendCommandExists:CMD_MULTI4_SOURCE dspId:DSP_7]) {
                btnMultiAux2OnOff.selected = aux2OnOff;
            }
            aux3OnOff = (pkt->multi_4_source & 0x0400) >> 10;
            if (aux3OnOff != btnMultiAux3OnOff.selected && ![viewController sendCommandExists:CMD_MULTI4_SOURCE dspId:DSP_7]) {
                btnMultiAux3OnOff.selected = aux3OnOff;
            }
            aux4OnOff = (pkt->multi_4_source & 0x0800) >> 11;
            if (aux4OnOff != btnMultiAux4OnOff.selected && ![viewController sendCommandExists:CMD_MULTI4_SOURCE dspId:DSP_7]) {
                btnMultiAux4OnOff.selected = aux4OnOff;
            }
            gp1OnOff = pkt->multi_4_source & 0x01;
            if (gp1OnOff != btnMultiGp1OnOff.selected && ![viewController sendCommandExists:CMD_MULTI4_SOURCE dspId:DSP_7]) {
                btnMultiGp1OnOff.selected = gp1OnOff;
            }
            gp2OnOff = (pkt->multi_4_source & 0x02) >> 1;
            if (gp2OnOff != btnMultiGp2OnOff.selected && ![viewController sendCommandExists:CMD_MULTI4_SOURCE dspId:DSP_7]) {
                btnMultiGp2OnOff.selected = gp2OnOff;
            }
            gp3OnOff = (pkt->multi_4_source & 0x04) >> 2;
            if (gp3OnOff != btnMultiGp3OnOff.selected && ![viewController sendCommandExists:CMD_MULTI4_SOURCE dspId:DSP_7]) {
                btnMultiGp3OnOff.selected = gp3OnOff;
            }
            gp4OnOff = (pkt->multi_4_source & 0x08) >> 3;
            if (gp4OnOff != btnMultiGp4OnOff.selected && ![viewController sendCommandExists:CMD_MULTI4_SOURCE dspId:DSP_7]) {
                btnMultiGp4OnOff.selected = gp4OnOff;
            }
            currentOn = pkt->multi_4_ctrl & 0x01;
            if (currentOn != btnCurrentOn.selected && ![viewController sendCommandExists:CMD_MULTI_4_CTRL dspId:DSP_7]) {
                btnCurrentOn.selected = currentOn;
            }
            currentMeter = (pkt->multi_1_4_meter_pre_post & 0x08) >> 3;
            if (currentMeter != btnCurrentMeter.selected && ![viewController sendCommandExists:CMD_MULTI_METER dspId:DSP_7]) {
                btnCurrentMeter.selected = currentMeter;
            }
            if (!_sliderCurrentLock) {
                sliderCurrent.value = pkt->multi_4_fader;
                [lblSliderValue setText:[Global getSliderGain:sliderCurrent.value appendDb:YES]];
            }
            // Update DYN status
            if (btnDyn.selected) {
                // Check DYN status, green if ACTIVE, yellow if NOP
                if ((pkt->multi_1_4_dyn_status & 0x1000) >> 12) {
                    [btnDynGate setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynGate setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->multi_1_4_dyn_status & 0x2000) >> 13) {
                    [btnDynExpander setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynExpander setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->multi_1_4_dyn_status & 0x4000) >> 14) {
                    [btnDynComp setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynComp setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
                if ((pkt->multi_1_4_dyn_status & 0x0800) >> 15) {
                    [btnDynLimiter setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
                } else {
                    [btnDynLimiter setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
                }
            }
            break;
        default:
            break;
    }
}

- (void)processMainViewPage:(ViewPageMainPacket *)pkt
{
    _mainLRCtrl = pkt->main_l_r_ctrl;
    _chToMainCtrl = pkt->ch_to_main_ctrl;
    _gpToMainCtrl = pkt->gp_to_main_ctrl;
    _mainMeterPrePost = pkt->main_meter_pre_post;
    
    meterEqOut.progress = ((float)pkt->main_l_meter_eq_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
    meterDynOut.progress = ((float)pkt->main_l_meter_dyn_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
    meterEqOutR.progress = ((float)pkt->main_r_meter_eq_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
    meterDynOutR.progress = ((float)pkt->main_r_meter_dyn_out/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
    
    if (![viewController sendCommandExists:CMD_MAIN_LR_CTRL dspId:DSP_6]) {
        [self refreshCtrlViews:pkt->main_l_r_ctrl];
    }
    if (![viewController sendCommandExists:CMD_MAIN_LR_DELAY_TIME dspId:DSP_6]) {
        knobDelay.value = [Global delayToKnobValue:pkt->main_l_r_delay_time];
        [self knobDelayDidChange:nil];
    }

    if (![viewController sendCommandExists:CMD_CH_TO_MAIN_CTRL dspId:DSP_5]) {
        btnMainCh1OnOff.selected = pkt->ch_to_main_ctrl & 0x01;
        btnMainCh2OnOff.selected = (pkt->ch_to_main_ctrl & 0x02) >> 1;
        btnMainCh3OnOff.selected = (pkt->ch_to_main_ctrl & 0x04) >> 2;
        btnMainCh4OnOff.selected = (pkt->ch_to_main_ctrl & 0x08) >> 3;
        btnMainCh5OnOff.selected = (pkt->ch_to_main_ctrl & 0x10) >> 4;
        btnMainCh6OnOff.selected = (pkt->ch_to_main_ctrl & 0x20) >> 5;
        btnMainCh7OnOff.selected = (pkt->ch_to_main_ctrl & 0x40) >> 6;
        btnMainCh8OnOff.selected = (pkt->ch_to_main_ctrl & 0x80) >> 7;
        btnMainCh9OnOff.selected = (pkt->ch_to_main_ctrl & 0x0100) >> 8;
        btnMainCh10OnOff.selected = (pkt->ch_to_main_ctrl & 0x0200) >> 9;
        btnMainCh11OnOff.selected = (pkt->ch_to_main_ctrl & 0x0400) >> 10;
        btnMainCh12OnOff.selected = (pkt->ch_to_main_ctrl & 0x0800) >> 11;
        btnMainCh13OnOff.selected = (pkt->ch_to_main_ctrl & 0x1000) >> 12;
        btnMainCh14OnOff.selected = (pkt->ch_to_main_ctrl & 0x2000) >> 13;
        btnMainCh15OnOff.selected = (pkt->ch_to_main_ctrl & 0x4000) >> 14;
        btnMainCh16OnOff.selected = (pkt->ch_to_main_ctrl & 0x8000) >> 15;
    }
    if (![viewController sendCommandExists:CMD_CH17_USB_AUDIO_SETTING dspId:DSP_5]) {
        btnMainCh17OnOff.selected = (pkt->ch_17_audio_setting & 0x0200) >> 9;
    }
    if (![viewController sendCommandExists:CMD_CH18_USB_AUDIO_SETTING dspId:DSP_5]) {
        btnMainCh18OnOff.selected = (pkt->ch_18_audio_setting & 0x0200) >> 9;
    }
    if (![viewController sendCommandExists:CMD_GP_TO_MAIN_CTRL dspId:DSP_5]) {
        btnMainGp1OnOff.selected = pkt->gp_to_main_ctrl & 0x01;
        btnMainGp2OnOff.selected = (pkt->gp_to_main_ctrl & 0x02) >> 1;
        btnMainGp3OnOff.selected = (pkt->gp_to_main_ctrl & 0x04) >> 2;
        btnMainGp4OnOff.selected = (pkt->gp_to_main_ctrl & 0x08) >> 3;
    }
    
    if (!_sliderCurrentLock) {
        sliderCurrent.value = pkt->main_fader;
        [lblSliderValue setText:[Global getSliderGain:sliderCurrent.value appendDb:YES]];
    }    
    BOOL currentSolo = (pkt->main_meter_pre_post & 0x80) >> 7;
    if (currentSolo != btnCurrentSolo.selected && ![viewController sendCommandExists:CMD_MAIN_METER dspId:DSP_6]) {
        btnCurrentSolo.selected = currentSolo;
    }
    BOOL currentOn = pkt->main_l_r_ctrl & 0x01;
    if (currentOn != btnCurrentOn.selected && ![viewController sendCommandExists:CMD_MAIN_LR_CTRL dspId:DSP_6]) {
        btnCurrentOn.selected = currentOn;
    }
    BOOL currentMeter = pkt->main_meter_pre_post & 0x01;
    if (currentMeter != btnCurrentMeter.selected && ![viewController sendCommandExists:CMD_MAIN_METER dspId:DSP_6]) {
        btnCurrentMeter.selected = currentMeter;
    }
    // Update DYN status
    if (btnDyn.selected) {
        // Check DYN status, green if ACTIVE, yellow if NOP
        if ((pkt->main_l_r_ch_17_18_dyn_status & 0x01) ||
            ((pkt->main_l_r_ch_17_18_dyn_status & 0x10) >> 4)) {
            [btnDynGate setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
        } else {
            [btnDynGate setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
        }
        if (((pkt->main_l_r_ch_17_18_dyn_status & 0x02) >> 1) ||
            ((pkt->main_l_r_ch_17_18_dyn_status & 0x20) >> 5)) {
            [btnDynExpander setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
        } else {
            [btnDynExpander setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
        }
        if (((pkt->main_l_r_ch_17_18_dyn_status & 0x04) >> 2) ||
            ((pkt->main_l_r_ch_17_18_dyn_status & 0x40) >> 6)) {
            [btnDynComp setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
        } else {
            [btnDynComp setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
        }
        if (((pkt->main_l_r_ch_17_18_dyn_status & 0x08) >> 3) ||
            ((pkt->main_l_r_ch_17_18_dyn_status & 0x80) >> 7)) {
            [btnDynLimiter setBackgroundImage:[UIImage imageNamed:@"button-51-1.png"] forState:UIControlStateSelected];
        } else {
            [btnDynLimiter setBackgroundImage:[UIImage imageNamed:@"button-51.png"] forState:UIControlStateSelected];
        }
    }
}

- (void)processCtrlRmPage:(SetupCtrlRmUsbAssignPagePacket *)pkt
{
    if (currentChannel != CTRL_RM) {
        return;
    }

    _ctrlRmChannel = pkt->control_room_channel;
    _ctrlRmAuxGp = pkt->control_room_aux_gp;
    _mainToCtrlRm = pkt->main_to_control_room;
    _soloCtrlChannel = pkt->solo_ctrl_channel;
    _soloSafeCtrlChannel = pkt->solo_safe_ctrl_channel;
    _soloCtrlAuxGp = pkt->solo_ctrl_aux_gp;
    _soloSafeCtrlAuxGp = pkt->solo_safe_ctrl_aux_gp;
    _digitalInOutCtrl = pkt->digital_in_out_ctrl;
    _gpToMain = pkt->gp_to_main;
    _ch17AudioSetting = pkt->ch_17_audio_setting;
    _ch18AudioSetting = pkt->ch_18_audio_setting;
    
    sliderCurrent.value = pkt->control_room_trim_level;
    [lblSliderValue setText:[Global getSliderGain:sliderCurrent.value appendDb:YES]];
    meterCurrentL.progress = ((float)pkt->control_room_l_meter_level/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
    meterCurrentR.progress = ((float)pkt->control_room_r_meter_level/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
    
    if (![viewController sendCommandExists:CMD_DIGITAL_INOUT_CTRL dspId:DSP_6]) {
        btnCurrentSolo.selected = (pkt->digital_in_out_ctrl & 0x04) >> 2;
    }
    
    // Update PFL/AFL
    if (![viewController sendCommandExists:CMD_CTRL_RM_ON_OFF_CH dspId:DSP_5]) {
        btnChannel1.selected = _ctrlRmChannel & 0x01;
        btnChannel2.selected = (_ctrlRmChannel & 0x02) >> 1;
        btnChannel3.selected = (_ctrlRmChannel & 0x04) >> 2;
        btnChannel4.selected = (_ctrlRmChannel & 0x08) >> 3;
        btnChannel5.selected = (_ctrlRmChannel & 0x10) >> 4;
        btnChannel6.selected = (_ctrlRmChannel & 0x20) >> 5;
        btnChannel7.selected = (_ctrlRmChannel & 0x40) >> 6;
        btnChannel8.selected = (_ctrlRmChannel & 0x80) >> 7;
        btnChannel9.selected = (_ctrlRmChannel & 0x0100) >> 8;
        btnChannel10.selected = (_ctrlRmChannel & 0x0200) >> 9;
        btnChannel11.selected = (_ctrlRmChannel & 0x0400) >> 10;
        btnChannel12.selected = (_ctrlRmChannel & 0x0800) >> 11;
        btnChannel13.selected = (_ctrlRmChannel & 0x1000) >> 12;
        btnChannel14.selected = (_ctrlRmChannel & 0x2000) >> 13;
        btnChannel15.selected = (_ctrlRmChannel & 0x4000) >> 14;
        btnChannel16.selected = (_ctrlRmChannel & 0x8000) >> 15;
    }
    if (![viewController sendCommandExists:CMD_CH17_USB_AUDIO_SETTING dspId:DSP_5]) {
        btnChannel17.selected = (_ch17AudioSetting & 0x1000) >> 12;
    }
    if (![viewController sendCommandExists:CMD_CH18_USB_AUDIO_SETTING dspId:DSP_5]) {
        btnChannel18.selected = (_ch18AudioSetting & 0x1000) >> 12;
    }
    if (![viewController sendCommandExists:CMD_CTRL_RM_ON_OFF_AUXGP dspId:DSP_5]) {
        btnGp1.selected = _ctrlRmAuxGp & 0x01;
        btnGp2.selected = (_ctrlRmAuxGp & 0x02) >> 1;
        btnGp3.selected = (_ctrlRmAuxGp & 0x04) >> 2;
        btnGp4.selected = (_ctrlRmAuxGp & 0x08) >> 3;
        btnAux1.selected = (_ctrlRmAuxGp & 0x0100) >> 8;
        btnAux2.selected = (_ctrlRmAuxGp & 0x0200) >> 9;
        btnAux3.selected = (_ctrlRmAuxGp & 0x0400) >> 10;
        btnAux4.selected = (_ctrlRmAuxGp & 0x0800) >> 11;
    }
    if (![viewController sendCommandExists:CMD_MAIN_TO_CTRL_RM dspId:DSP_6]) {
        btnMainPflAfl.selected = _mainToCtrlRm & 0x01;
    }
    
    // Update solo
    if (![viewController sendCommandExists:CMD_SOLO_CTRL dspId:DSP_5]) {
        imgChannel1Solo.hidden = !(_soloCtrlChannel & 0x01);
        imgChannel2Solo.hidden = !((_soloCtrlChannel & 0x02) >> 1);
        imgChannel3Solo.hidden = !((_soloCtrlChannel & 0x04) >> 2);
        imgChannel4Solo.hidden = !((_soloCtrlChannel & 0x08) >> 3);
        imgChannel5Solo.hidden = !((_soloCtrlChannel & 0x10) >> 4);
        imgChannel6Solo.hidden = !((_soloCtrlChannel & 0x20) >> 5);
        imgChannel7Solo.hidden = !((_soloCtrlChannel & 0x40) >> 6);
        imgChannel8Solo.hidden = !((_soloCtrlChannel & 0x80) >> 7);
        imgChannel9Solo.hidden = !((_soloCtrlChannel & 0x0100) >> 8);
        imgChannel10Solo.hidden = !((_soloCtrlChannel & 0x0200) >> 9);
        imgChannel11Solo.hidden = !((_soloCtrlChannel & 0x0400) >> 10);
        imgChannel12Solo.hidden = !((_soloCtrlChannel & 0x0800) >> 11);
        imgChannel13Solo.hidden = !((_soloCtrlChannel & 0x1000) >> 12);
        imgChannel14Solo.hidden = !((_soloCtrlChannel & 0x2000) >> 13);
        imgChannel15Solo.hidden = !((_soloCtrlChannel & 0x4000) >> 14);
        imgChannel16Solo.hidden = !((_soloCtrlChannel & 0x8000) >> 15);
    }
    if (![viewController sendCommandExists:CMD_CH17_USB_AUDIO_SETTING dspId:DSP_5]) {
        imgChannel17Solo.hidden = !((_ch17AudioSetting & 0x0400) >> 10);
    }
    if (![viewController sendCommandExists:CMD_CH18_USB_AUDIO_SETTING dspId:DSP_5]) {
        imgChannel18Solo.hidden = !((_ch18AudioSetting & 0x0400) >> 10);
    }
    if (![viewController sendCommandExists:CMD_AUXGP_SOLO_CTRL dspId:DSP_5]) {
        imgGp1Solo.hidden = !(_soloCtrlAuxGp & 0x01);
        imgGp2Solo.hidden = !((_soloCtrlAuxGp & 0x02) >> 1);
        imgGp3Solo.hidden = !((_soloCtrlAuxGp & 0x04) >> 2);
        imgGp4Solo.hidden = !((_soloCtrlAuxGp & 0x08) >> 3);
        imgAux1Solo.hidden = !((_soloCtrlAuxGp & 0x0100) >> 8);
        imgAux2Solo.hidden = !((_soloCtrlAuxGp & 0x0200) >> 9);
        imgAux3Solo.hidden = !((_soloCtrlAuxGp & 0x0400) >> 10);
        imgAux4Solo.hidden = !((_soloCtrlAuxGp & 0x0800) >> 11);
    }
    if (![viewController sendCommandExists:CMD_GP_TO_MAIN_CTRL dspId:DSP_5]) {
        imgEfx1Solo.hidden = !((_gpToMain & 0x0400) >> 10);
        imgEfx2Solo.hidden = !((_gpToMain & 0x0800) >> 11);
    }
    [self updateMainAflPflArrow];
    
    // Update solo safe
    if (![viewController sendCommandExists:CMD_SOLO_SAFE_CTRL dspId:DSP_5]) {
        lblChannel1Safe.hidden = !(_soloSafeCtrlChannel & 0x01);
        lblChannel2Safe.hidden = !((_soloSafeCtrlChannel & 0x02) >> 1);
        lblChannel3Safe.hidden = !((_soloSafeCtrlChannel & 0x04) >> 2);
        lblChannel4Safe.hidden = !((_soloSafeCtrlChannel & 0x08) >> 3);
        lblChannel5Safe.hidden = !((_soloSafeCtrlChannel & 0x10) >> 4);
        lblChannel6Safe.hidden = !((_soloSafeCtrlChannel & 0x20) >> 5);
        lblChannel7Safe.hidden = !((_soloSafeCtrlChannel & 0x40) >> 6);
        lblChannel8Safe.hidden = !((_soloSafeCtrlChannel & 0x80) >> 7);
        lblChannel9Safe.hidden = !((_soloSafeCtrlChannel & 0x0100) >> 8);
        lblChannel10Safe.hidden = !((_soloSafeCtrlChannel & 0x0200) >> 9);
        lblChannel11Safe.hidden = !((_soloSafeCtrlChannel & 0x0400) >> 10);
        lblChannel12Safe.hidden = !((_soloSafeCtrlChannel & 0x0800) >> 11);
        lblChannel13Safe.hidden = !((_soloSafeCtrlChannel & 0x1000) >> 12);
        lblChannel14Safe.hidden = !((_soloSafeCtrlChannel & 0x2000) >> 13);
        lblChannel15Safe.hidden = !((_soloSafeCtrlChannel & 0x4000) >> 14);
        lblChannel16Safe.hidden = !((_soloSafeCtrlChannel & 0x8000) >> 15);
    }
    if (![viewController sendCommandExists:CMD_CH17_USB_AUDIO_SETTING dspId:DSP_5]) {
        lblChannel17Safe.hidden = !((_ch17AudioSetting & 0x0800) >> 11);
    }
    if (![viewController sendCommandExists:CMD_CH18_USB_AUDIO_SETTING dspId:DSP_5]) {
        lblChannel18Safe.hidden = !((_ch18AudioSetting & 0x0800) >> 11);
    }
    if (![viewController sendCommandExists:CMD_AUXGP_SOLO_SAFE_CTRL dspId:DSP_5]) {
        lblGp1Safe.hidden = !(_soloSafeCtrlAuxGp & 0x01);
        lblGp2Safe.hidden = !((_soloSafeCtrlAuxGp & 0x02) >> 1);
        lblGp3Safe.hidden = !((_soloSafeCtrlAuxGp & 0x04) >> 2);
        lblGp4Safe.hidden = !((_soloSafeCtrlAuxGp & 0x08) >> 3);
        lblAux1Safe.hidden = !((_soloSafeCtrlAuxGp & 0x0100) >> 8);
        lblAux2Safe.hidden = !((_soloSafeCtrlAuxGp & 0x0200) >> 9);
        lblAux3Safe.hidden = !((_soloSafeCtrlAuxGp & 0x0400) >> 10);
        lblAux4Safe.hidden = !((_soloSafeCtrlAuxGp & 0x0800) >> 11);
    }
    if (![viewController sendCommandExists:CMD_GP_TO_MAIN_CTRL dspId:DSP_5]) {
        lblEfx1Safe.hidden = !((_gpToMain & 0x1000) >> 12);
        lblEfx2Safe.hidden = !((_gpToMain & 0x2000) >> 13);
    }
}

- (void)processChannelLabel:(AllMeterValueRxPacket *)pkt
{
    switch (currentChannel) {
        case CH_1:
            txtCurrentChannel.text = [NSString stringWithUTF8String:(const char *)pkt->channel_1_audio_name];
            meterCurrentR.progress = ((float)pkt->channel_1_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case CH_2:
            txtCurrentChannel.text = [NSString stringWithUTF8String:(const char *)pkt->channel_2_audio_name];
            meterCurrentR.progress = ((float)pkt->channel_2_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case CH_3:
            txtCurrentChannel.text = [NSString stringWithUTF8String:(const char *)pkt->channel_3_audio_name];
            meterCurrentR.progress = ((float)pkt->channel_3_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case CH_4:
            txtCurrentChannel.text = [NSString stringWithUTF8String:(const char *)pkt->channel_4_audio_name];
            meterCurrentR.progress = ((float)pkt->channel_4_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case CH_5:
            txtCurrentChannel.text = [NSString stringWithUTF8String:(const char *)pkt->channel_5_audio_name];
            meterCurrentR.progress = ((float)pkt->channel_5_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case CH_6:
            txtCurrentChannel.text = [NSString stringWithUTF8String:(const char *)pkt->channel_6_audio_name];
            meterCurrentR.progress = ((float)pkt->channel_6_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case CH_7:
            txtCurrentChannel.text = [NSString stringWithUTF8String:(const char *)pkt->channel_7_audio_name];
            meterCurrentR.progress = ((float)pkt->channel_7_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case CH_8:
            txtCurrentChannel.text = [NSString stringWithUTF8String:(const char *)pkt->channel_8_audio_name];
            meterCurrentR.progress = ((float)pkt->channel_8_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case CH_9:
            txtCurrentChannel.text = [NSString stringWithUTF8String:(const char *)pkt->channel_9_audio_name];
            meterCurrentR.progress = ((float)pkt->channel_9_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case CH_10:
            txtCurrentChannel.text = [NSString stringWithUTF8String:(const char *)pkt->channel_10_audio_name];
            meterCurrentR.progress = ((float)pkt->channel_10_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case CH_11:
            txtCurrentChannel.text = [NSString stringWithUTF8String:(const char *)pkt->channel_11_audio_name];
            meterCurrentR.progress = ((float)pkt->channel_11_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case CH_12:
            txtCurrentChannel.text = [NSString stringWithUTF8String:(const char *)pkt->channel_12_audio_name];
            meterCurrentR.progress = ((float)pkt->channel_12_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case CH_13:
            txtCurrentChannel.text = [NSString stringWithUTF8String:(const char *)pkt->channel_13_audio_name];
            meterCurrentR.progress = ((float)pkt->channel_13_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case CH_14:
            txtCurrentChannel.text = [NSString stringWithUTF8String:(const char *)pkt->channel_14_audio_name];
            meterCurrentR.progress = ((float)pkt->channel_14_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case CH_15:
            txtCurrentChannel.text = [NSString stringWithUTF8String:(const char *)pkt->channel_15_audio_name];
            meterCurrentR.progress = ((float)pkt->channel_15_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case CH_16:
            txtCurrentChannel.text = [NSString stringWithUTF8String:(const char *)pkt->channel_16_audio_name];
            meterCurrentR.progress = ((float)pkt->channel_16_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case CH_17:
            txtCurrentChannel.text = [NSString stringWithUTF8String:(const char *)pkt->channel_17_audio_name];
            meterCurrentR.progress = ((float)pkt->channel_17_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case CH_18:
            txtCurrentChannel.text = [NSString stringWithUTF8String:(const char *)pkt->channel_18_audio_name];
            meterCurrentR.progress = ((float)pkt->channel_18_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case MT_1:
            txtCurrentChannel.text = [NSString stringWithUTF8String:(const char *)pkt->multi_1_audio_name];
            meterCurrentR.progress = ((float)pkt->multi_1_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case MT_2:
            txtCurrentChannel.text = [NSString stringWithUTF8String:(const char *)pkt->multi_2_audio_name];
            meterCurrentR.progress = ((float)pkt->multi_2_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case MT_3:
            txtCurrentChannel.text = [NSString stringWithUTF8String:(const char *)pkt->multi_3_audio_name];
            meterCurrentR.progress = ((float)pkt->multi_3_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case MT_4:
            txtCurrentChannel.text = [NSString stringWithUTF8String:(const char *)pkt->multi_4_audio_name];
            meterCurrentR.progress = ((float)pkt->multi_4_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case CTRL_RM:
            txtCurrentChannel.text = [NSString stringWithUTF8String:(const char *)pkt->ctrl_room_audio_name];
            meterCurrentL.progress = ((float)pkt->ctrl_l_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterCurrentR.progress = ((float)pkt->ctrl_r_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        case MAIN:
            txtCurrentChannel.text = [NSString stringWithUTF8String:(const char *)pkt->main_audio_name];
            meterCurrentL.progress = ((float)pkt->main_l_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            meterCurrentR.progress = ((float)pkt->main_r_meter_value/128.0 + METER_MAX_VALUE)/METER_MAX_VALUE;
            break;
        default:
            break;
    }
}

- (void)updateCurrentChannelLabel
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    switch (currentChannel) {
        case CH_1:
            if ([userDefaults objectForKey:@"Ch1"] != nil) {
                [txtCurrentChannel setText:(NSString *)[userDefaults objectForKey:@"Ch1"]];
            } else {
                [txtCurrentChannel setText:NSLocalizedStringFromTable(@"Ch1", @"Acapela", nil)];
            }
            [lblCurrentChannel setText:NSLocalizedStringFromTable(@"Ch1", @"Acapela", nil)];
            [sliderCurrent setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            break;
        case CH_2:
            if ([userDefaults objectForKey:@"Ch2"] != nil) {
                [txtCurrentChannel setText:(NSString *)[userDefaults objectForKey:@"Ch2"]];
            } else {
                [txtCurrentChannel setText:NSLocalizedStringFromTable(@"Ch2", @"Acapela", nil)];
            }
            [lblCurrentChannel setText:NSLocalizedStringFromTable(@"Ch2", @"Acapela", nil)];
            [sliderCurrent setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            break;
        case CH_3:
            if ([userDefaults objectForKey:@"Ch3"] != nil) {
                [txtCurrentChannel setText:(NSString *)[userDefaults objectForKey:@"Ch3"]];
            } else {
                [txtCurrentChannel setText:NSLocalizedStringFromTable(@"Ch3", @"Acapela", nil)];
            }
            [lblCurrentChannel setText:NSLocalizedStringFromTable(@"Ch3", @"Acapela", nil)];
            [sliderCurrent setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            break;
        case CH_4:
            if ([userDefaults objectForKey:@"Ch4"] != nil) {
                [txtCurrentChannel setText:(NSString *)[userDefaults objectForKey:@"Ch4"]];
            } else {
                [txtCurrentChannel setText:NSLocalizedStringFromTable(@"Ch4", @"Acapela", nil)];
            }
            [lblCurrentChannel setText:NSLocalizedStringFromTable(@"Ch4", @"Acapela", nil)];
            [sliderCurrent setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            break;
        case CH_5:
            if ([userDefaults objectForKey:@"Ch5"] != nil) {
                [txtCurrentChannel setText:(NSString *)[userDefaults objectForKey:@"Ch5"]];
            } else {
                [txtCurrentChannel setText:NSLocalizedStringFromTable(@"Ch5", @"Acapela", nil)];
            }
            [lblCurrentChannel setText:NSLocalizedStringFromTable(@"Ch5", @"Acapela", nil)];
            [sliderCurrent setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            break;
        case CH_6:
            if ([userDefaults objectForKey:@"Ch6"] != nil) {
                [txtCurrentChannel setText:(NSString *)[userDefaults objectForKey:@"Ch6"]];
            } else {
                [txtCurrentChannel setText:NSLocalizedStringFromTable(@"Ch6", @"Acapela", nil)];
            }
            [lblCurrentChannel setText:NSLocalizedStringFromTable(@"Ch6", @"Acapela", nil)];
            [sliderCurrent setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            break;
        case CH_7:
            if ([userDefaults objectForKey:@"Ch7"] != nil) {
                [txtCurrentChannel setText:(NSString *)[userDefaults objectForKey:@"Ch7"]];
            } else {
                [txtCurrentChannel setText:NSLocalizedStringFromTable(@"Ch7", @"Acapela", nil)];
            }
            [lblCurrentChannel setText:NSLocalizedStringFromTable(@"Ch7", @"Acapela", nil)];
            [sliderCurrent setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            break;
        case CH_8:
            if ([userDefaults objectForKey:@"Ch8"] != nil) {
                [txtCurrentChannel setText:(NSString *)[userDefaults objectForKey:@"Ch8"]];
            } else {
                [txtCurrentChannel setText:NSLocalizedStringFromTable(@"Ch8", @"Acapela", nil)];
            }
            [lblCurrentChannel setText:NSLocalizedStringFromTable(@"Ch8", @"Acapela", nil)];
            [sliderCurrent setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            break;
        case CH_9:
            if ([userDefaults objectForKey:@"Ch9"] != nil) {
                [txtCurrentChannel setText:(NSString *)[userDefaults objectForKey:@"Ch9"]];
            } else {
                [txtCurrentChannel setText:NSLocalizedStringFromTable(@"Ch9", @"Acapela", nil)];
            }
            [lblCurrentChannel setText:NSLocalizedStringFromTable(@"Ch9", @"Acapela", nil)];
            [sliderCurrent setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            break;
        case CH_10:
            if ([userDefaults objectForKey:@"Ch10"] != nil) {
                [txtCurrentChannel setText:(NSString *)[userDefaults objectForKey:@"Ch10"]];
            } else {
                [txtCurrentChannel setText:NSLocalizedStringFromTable(@"Ch10", @"Acapela", nil)];
            }
            [lblCurrentChannel setText:NSLocalizedStringFromTable(@"Ch10", @"Acapela", nil)];
            [sliderCurrent setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            break;
        case CH_11:
            if ([userDefaults objectForKey:@"Ch11"] != nil) {
                [txtCurrentChannel setText:(NSString *)[userDefaults objectForKey:@"Ch11"]];
            } else {
                [txtCurrentChannel setText:NSLocalizedStringFromTable(@"Ch11", @"Acapela", nil)];
            }
            [lblCurrentChannel setText:NSLocalizedStringFromTable(@"Ch11", @"Acapela", nil)];
            [sliderCurrent setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            break;
        case CH_12:
            if ([userDefaults objectForKey:@"Ch12"] != nil) {
                [txtCurrentChannel setText:(NSString *)[userDefaults objectForKey:@"Ch12"]];
            } else {
                [txtCurrentChannel setText:NSLocalizedStringFromTable(@"Ch12", @"Acapela", nil)];
            }
            [lblCurrentChannel setText:NSLocalizedStringFromTable(@"Ch12", @"Acapela", nil)];
            [sliderCurrent setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            break;
        case CH_13:
            if ([userDefaults objectForKey:@"Ch13"] != nil) {
                [txtCurrentChannel setText:(NSString *)[userDefaults objectForKey:@"Ch13"]];
            } else {
                [txtCurrentChannel setText:NSLocalizedStringFromTable(@"Ch13", @"Acapela", nil)];
            }
            [lblCurrentChannel setText:NSLocalizedStringFromTable(@"Ch13", @"Acapela", nil)];
            [sliderCurrent setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            break;
        case CH_14:
            if ([userDefaults objectForKey:@"Ch14"] != nil) {
                [txtCurrentChannel setText:(NSString *)[userDefaults objectForKey:@"Ch14"]];
            } else {
                [txtCurrentChannel setText:NSLocalizedStringFromTable(@"Ch14", @"Acapela", nil)];
            }
            [lblCurrentChannel setText:NSLocalizedStringFromTable(@"Ch14", @"Acapela", nil)];
            [sliderCurrent setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            break;
        case CH_15:
            if ([userDefaults objectForKey:@"Ch15"] != nil) {
                [txtCurrentChannel setText:(NSString *)[userDefaults objectForKey:@"Ch15"]];
            } else {
                [txtCurrentChannel setText:NSLocalizedStringFromTable(@"Ch15", @"Acapela", nil)];
            }
            [lblCurrentChannel setText:NSLocalizedStringFromTable(@"Ch15", @"Acapela", nil)];
            [sliderCurrent setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            break;
        case CH_16:
            if ([userDefaults objectForKey:@"Ch16"] != nil) {
                [txtCurrentChannel setText:(NSString *)[userDefaults objectForKey:@"Ch16"]];
            } else {
                [txtCurrentChannel setText:NSLocalizedStringFromTable(@"Ch16", @"Acapela", nil)];
            }
            [lblCurrentChannel setText:NSLocalizedStringFromTable(@"Ch16", @"Acapela", nil)];
            [sliderCurrent setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            break;
        case CH_17:
            if ([userDefaults objectForKey:@"Ch17"] != nil) {
                [txtCurrentChannel setText:(NSString *)[userDefaults objectForKey:@"Ch17"]];
            } else {
                [txtCurrentChannel setText:NSLocalizedStringFromTable(@"Ch17", @"Acapela", nil)];
            }
            [lblCurrentChannel setText:NSLocalizedStringFromTable(@"Ch17", @"Acapela", nil)];
            [sliderCurrent setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            break;
        case CH_18:
            if ([userDefaults objectForKey:@"Ch18"] != nil) {
                [txtCurrentChannel setText:(NSString *)[userDefaults objectForKey:@"Ch18"]];
            } else {
                [txtCurrentChannel setText:NSLocalizedStringFromTable(@"Ch18", @"Acapela", nil)];
            }
            [lblCurrentChannel setText:NSLocalizedStringFromTable(@"Ch18", @"Acapela", nil)];
            [sliderCurrent setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            break;
        case MT_1:
            if ([userDefaults objectForKey:@"Multi1"] != nil) {
                [txtCurrentChannel setText:(NSString *)[userDefaults objectForKey:@"Multi1"]];
            } else {
                [txtCurrentChannel setText:NSLocalizedStringFromTable(@"Multi1", @"Acapela", nil)];
            }
            [lblCurrentChannel setText:NSLocalizedStringFromTable(@"Multi1", @"Acapela", nil)];
            [sliderCurrent setThumbImage:[UIImage imageNamed:@"fader-4.png"] forState:UIControlStateNormal];
            break;
        case MT_2:
            if ([userDefaults objectForKey:@"Multi2"] != nil) {
                [txtCurrentChannel setText:(NSString *)[userDefaults objectForKey:@"Multi2"]];
            } else {
                [txtCurrentChannel setText:NSLocalizedStringFromTable(@"Multi2", @"Acapela", nil)];
            }
            [lblCurrentChannel setText:NSLocalizedStringFromTable(@"Multi2", @"Acapela", nil)];
            [sliderCurrent setThumbImage:[UIImage imageNamed:@"fader-4.png"] forState:UIControlStateNormal];
            break;
        case MT_3:
            if ([userDefaults objectForKey:@"Multi3"] != nil) {
                [txtCurrentChannel setText:(NSString *)[userDefaults objectForKey:@"Multi3"]];
            } else {
                [txtCurrentChannel setText:NSLocalizedStringFromTable(@"Multi3", @"Acapela", nil)];
            }
            [lblCurrentChannel setText:NSLocalizedStringFromTable(@"Multi3", @"Acapela", nil)];
            [sliderCurrent setThumbImage:[UIImage imageNamed:@"fader-4.png"] forState:UIControlStateNormal];
            break;
        case MT_4:
            if ([userDefaults objectForKey:@"Multi4"] != nil) {
                [txtCurrentChannel setText:(NSString *)[userDefaults objectForKey:@"Multi4"]];
            } else {
                [txtCurrentChannel setText:NSLocalizedStringFromTable(@"Multi4", @"Acapela", nil)];
            }
            [lblCurrentChannel setText:NSLocalizedStringFromTable(@"Multi4", @"Acapela", nil)];
            [sliderCurrent setThumbImage:[UIImage imageNamed:@"fader-4.png"] forState:UIControlStateNormal];
            break;
        case CTRL_RM:
            if ([userDefaults objectForKey:@"CtrlRm"] != nil) {
                [txtCurrentChannel setText:(NSString *)[userDefaults objectForKey:@"CtrlRm"]];
            } else {
                [txtCurrentChannel setText:NSLocalizedStringFromTable(@"CtrlRm", @"Acapela", nil)];
            }
            [lblCurrentChannel setText:NSLocalizedStringFromTable(@"CtrlRm", @"Acapela", nil)];
            [sliderCurrent setThumbImage:[UIImage imageNamed:@"fader-1.png"] forState:UIControlStateNormal];
            [self updateMainAflPflArrow];
            break;
        case MAIN:
            if ([userDefaults objectForKey:@"Main"] != nil) {
                [txtCurrentChannel setText:(NSString *)[userDefaults objectForKey:@"Main"]];
            } else {
                [txtCurrentChannel setText:NSLocalizedStringFromTable(@"Main", @"Acapela", nil)];
            }
            [lblCurrentChannel setText:NSLocalizedStringFromTable(@"Main", @"Acapela", nil)];
            [sliderCurrent setThumbImage:[UIImage imageNamed:@"fader-3.png"] forState:UIControlStateNormal];
            break;
        default:
            break;
    }
}

- (void)setViewPage:(int)page
{
    [btnCurrentMeter setHidden:NO];
    [btnChannelSelectUp setHidden:NO];
    [btnChannelSelectDown setHidden:NO];
    
    [meterEqOut setHidden:NO];
    [meterDynOut setHidden:NO];
    [meterEqOutR setHidden:YES];
    [meterDynOutR setHidden:YES];
    [meterCurrentL setHidden:YES];
    
    switch (page) {
        case VIEW_PAGE_CH_1_8:
            [viewTop setHidden:NO];
            [viewCtrlRm setHidden:YES];
            [viewBottomChannel setHidden:NO];
            [viewBottomMulti setHidden:YES];
            [viewBottomMain setHidden:YES];
            [imgBackground setImage:[UIImage imageNamed:@"basemap-43.png"]];
            [lblPan setHidden:NO];
            [sliderPan setHidden:NO];
            [meterPan setHidden:NO];
            [btnInv setHidden:NO];
            currentChannel = CH_1;
            [btnCurrentSolo setHidden:NO];
            [btnCurrentOn setHidden:NO];
            [btnCurrentSolo setTitle:@"SOLO" forState:UIControlStateNormal];
            [btnCurrentSolo setTitle:@"SOLO" forState:UIControlStateSelected];
            [btnCurrentSolo setBackgroundImage:[UIImage imageNamed:@"button-2.png"] forState:UIControlStateNormal];
            [btnCurrentSolo setBackgroundImage:[UIImage imageNamed:@"button-4.png"] forState:UIControlStateSelected];
            [btnCurrentMeter setTitle:@"METER INPUT" forState:UIControlStateNormal];
            [btnCurrentMeter setBackgroundImage:[UIImage imageNamed:@"button-6.png"] forState:UIControlStateNormal];
            break;
        case VIEW_PAGE_CH_9_16:
            [viewTop setHidden:NO];
            [viewCtrlRm setHidden:YES];
            [viewBottomChannel setHidden:NO];
            [viewBottomMulti setHidden:YES];
            [viewBottomMain setHidden:YES];
            [imgBackground setImage:[UIImage imageNamed:@"basemap-43.png"]];
            [lblPan setHidden:NO];
            [sliderPan setHidden:NO];
            [meterPan setHidden:NO];
            [btnInv setHidden:NO];
            currentChannel = CH_9;
            [btnCurrentSolo setHidden:NO];
            [btnCurrentOn setHidden:NO];
            [btnCurrentSolo setTitle:@"SOLO" forState:UIControlStateNormal];
            [btnCurrentSolo setTitle:@"SOLO" forState:UIControlStateSelected];
            [btnCurrentSolo setBackgroundImage:[UIImage imageNamed:@"button-2.png"] forState:UIControlStateNormal];
            [btnCurrentSolo setBackgroundImage:[UIImage imageNamed:@"button-4.png"] forState:UIControlStateSelected];
            [btnCurrentMeter setTitle:@"METER INPUT" forState:UIControlStateNormal];
            [btnCurrentMeter setBackgroundImage:[UIImage imageNamed:@"button-6.png"] forState:UIControlStateNormal];
            break;
        case VIEW_PAGE_USB_AUDIO:
            [viewTop setHidden:NO];
            [viewCtrlRm setHidden:YES];
            [viewBottomChannel setHidden:NO];
            [viewBottomMulti setHidden:YES];
            [viewBottomMain setHidden:YES];
            [imgBackground setImage:[UIImage imageNamed:@"basemap-43.png"]];
            [lblPan setHidden:NO];
            [sliderPan setHidden:NO];
            [meterPan setHidden:NO];
            [btnInv setHidden:NO];
            currentChannel = CH_17;
            [btnCurrentSolo setHidden:NO];
            [btnCurrentOn setHidden:NO];
            [btnCurrentSolo setTitle:@"SOLO" forState:UIControlStateNormal];
            [btnCurrentSolo setTitle:@"SOLO" forState:UIControlStateSelected];
            [btnCurrentSolo setBackgroundImage:[UIImage imageNamed:@"button-2.png"] forState:UIControlStateNormal];
            [btnCurrentSolo setBackgroundImage:[UIImage imageNamed:@"button-4.png"] forState:UIControlStateSelected];
            [btnCurrentMeter setTitle:@"METER INPUT" forState:UIControlStateNormal];
            [btnCurrentMeter setBackgroundImage:[UIImage imageNamed:@"button-6.png"] forState:UIControlStateNormal];
            break;
        case VIEW_PAGE_MT_1_4:
            [viewTop setHidden:NO];
            [viewCtrlRm setHidden:YES];
            [viewBottomChannel setHidden:YES];
            [viewBottomMulti setHidden:NO];
            [viewBottomMain setHidden:YES];
            [imgBackground setImage:[UIImage imageNamed:@"basemap-45.png"]];
            [lblPan setHidden:YES];
            [sliderPan setHidden:YES];
            [meterPan setHidden:YES];
            [btnInv setHidden:YES];
            currentChannel = MT_1;
            [btnCurrentSolo setHidden:YES];
            [btnCurrentOn setHidden:NO];
            [btnCurrentSolo setTitle:@"SOLO" forState:UIControlStateNormal];
            [btnCurrentSolo setTitle:@"SOLO" forState:UIControlStateSelected];
            [btnCurrentSolo setBackgroundImage:[UIImage imageNamed:@"button-2.png"] forState:UIControlStateNormal];
            [btnCurrentSolo setBackgroundImage:[UIImage imageNamed:@"button-4.png"] forState:UIControlStateSelected];
            [btnCurrentMeter setTitle:@"METER PRE" forState:UIControlStateNormal];
            [btnCurrentMeter setBackgroundImage:[UIImage imageNamed:@"button-3.png"] forState:UIControlStateNormal];
            break;
        case VIEW_PAGE_CTRL_RM:
            [viewTop setHidden:YES];
            [viewCtrlRm setHidden:NO];
            [viewBottomChannel setHidden:YES];
            [viewBottomMulti setHidden:YES];
            [viewBottomMain setHidden:YES];
            [imgBackground setImage:[UIImage imageNamed:@"basemap-46.png"]];
            currentChannel = CTRL_RM;
            [btnCurrentSolo setHidden:NO];
            [btnCurrentOn setHidden:YES];
            [btnCurrentMeter setHidden:YES];
            [btnChannelSelectUp setHidden:YES];
            [btnChannelSelectDown setHidden:YES];
            [btnCurrentSolo setTitle:@"STEREO" forState:UIControlStateNormal];
            [btnCurrentSolo setTitle:@"MONO" forState:UIControlStateSelected];
            [btnCurrentSolo setBackgroundImage:[UIImage imageNamed:@"button-4.png"] forState:UIControlStateNormal];
            [btnCurrentSolo setBackgroundImage:[UIImage imageNamed:@"button-5.png"] forState:UIControlStateSelected];
            [btnCurrentMeter setTitle:@"METER PRE" forState:UIControlStateNormal];
            [btnCurrentMeter setBackgroundImage:[UIImage imageNamed:@"button-3.png"] forState:UIControlStateNormal];
            [meterEqOut setHidden:YES];
            [meterDynOut setHidden:YES];
            [meterCurrentL setHidden:NO];
            break;
        case VIEW_PAGE_MAIN:
            [viewTop setHidden:NO];
            [viewCtrlRm setHidden:YES];
            [viewBottomChannel setHidden:YES];
            [viewBottomMulti setHidden:YES];
            [viewBottomMain setHidden:NO];
            [imgBackground setImage:[UIImage imageNamed:@"basemap-47.png"]];
            [lblPan setHidden:YES];
            [sliderPan setHidden:YES];
            [meterPan setHidden:YES];
            [btnInv setHidden:YES];
            currentChannel = MAIN;
            [btnCurrentSolo setHidden:NO];
            [btnCurrentOn setHidden:NO];
            [btnCurrentSolo setTitle:@"STEREO" forState:UIControlStateNormal];
            [btnCurrentSolo setTitle:@"MONO" forState:UIControlStateSelected];
            [btnCurrentSolo setBackgroundImage:[UIImage imageNamed:@"button-4.png"] forState:UIControlStateNormal];
            [btnCurrentSolo setBackgroundImage:[UIImage imageNamed:@"button-5.png"] forState:UIControlStateSelected];
            [btnCurrentMeter setTitle:@"METER PRE" forState:UIControlStateNormal];
            [btnCurrentMeter setBackgroundImage:[UIImage imageNamed:@"button-3.png"] forState:UIControlStateNormal];
            [meterEqOutR setHidden:NO];
            [meterDynOutR setHidden:NO];
            [meterCurrentL setHidden:NO];
            break;
    }
    
    currentViewPage = page;
    currentChannelLabel = currentChannel;
    [self updateCurrentChannelLabel];
    if (page != VIEW_PAGE_CTRL_RM) {
        [self refreshEqPreview];
        [self refreshDynPreview];
    }
}

#pragma mark -
#pragma mark Event handler methods

- (IBAction)onBtnChannelSelectDown:(id)sender {
    switch (currentViewPage) {
        case VIEW_PAGE_CH_1_8:
            if (currentChannel > CH_1)
                currentChannel--;
            break;
        case VIEW_PAGE_CH_9_16:
            if (currentChannel > CH_9)
                currentChannel--;
            break;
        case VIEW_PAGE_USB_AUDIO:
            if (currentChannel > CH_17)
                currentChannel--;
            break;
        case VIEW_PAGE_MT_1_4:
            if (currentChannel > MT_1)
                currentChannel--;
            break;
        default:
            break;
    }
    currentChannelLabel = currentChannel;
    [self updateCurrentChannelLabel];
    [self refreshEqPreview];
    [self refreshDynPreview];
}

- (IBAction)onBtnChannelSelectUp:(id)sender {
    switch (currentViewPage) {
        case VIEW_PAGE_CH_1_8:
            if (currentChannel < CH_8)
                currentChannel++;
            break;
        case VIEW_PAGE_CH_9_16:
            if (currentChannel < CH_16)
                currentChannel++;
            break;
        case VIEW_PAGE_USB_AUDIO:
            if (currentChannel < CH_18)
                currentChannel++;
            break;
        case VIEW_PAGE_MT_1_4:
            if (currentChannel < MT_4)
                currentChannel++;
            break;
        default:
            break;
    }
    currentChannelLabel = currentChannel;
    [self updateCurrentChannelLabel];
    [self refreshEqPreview];
    [self refreshDynPreview];
}

- (IBAction)onBtnSolo:(id)sender {
    btnCurrentSolo.selected = !btnCurrentSolo.selected;
    switch (currentChannel) {
        case CH_1:
        case CH_2:
        case CH_3:
        case CH_4:
        case CH_5:
        case CH_6:
        case CH_7:
        case CH_8:
        case CH_9:
        case CH_10:
        case CH_11:
        case CH_12:
        case CH_13:
        case CH_14:
        case CH_15:
        case CH_16:
            [Global setClearBit:&_ch1_16SoloCtrlOnOff bitValue:btnCurrentSolo.selected bitOrder:currentChannel];
            [viewController sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16SoloCtrlOnOff];
            break;
        case CH_17:
            [Global setClearBit:&_ch17AudioSetting bitValue:btnCurrentSolo.selected bitOrder:10];
            [viewController sendData:CMD_CH17_USB_AUDIO_SETTING commandType:DSP_WRITE dspId:DSP_5 value:_ch17AudioSetting];
            break;
        case CH_18:
            [Global setClearBit:&_ch18AudioSetting bitValue:btnCurrentSolo.selected bitOrder:10];
            [viewController sendData:CMD_CH18_USB_AUDIO_SETTING commandType:DSP_WRITE dspId:DSP_5 value:_ch18AudioSetting];
            break;
        case CTRL_RM:
            [Global setClearBit:&_digitalInOutCtrl bitValue:btnCurrentSolo.selected bitOrder:2];
            [viewController sendData:CMD_DIGITAL_INOUT_CTRL commandType:DSP_WRITE dspId:DSP_6 value:_digitalInOutCtrl];
            break;
        case MAIN:
            [Global setClearBit:&_mainMeterPrePost bitValue:btnCurrentSolo.selected bitOrder:7];
            [viewController sendData:CMD_MAIN_METER commandType:DSP_WRITE dspId:DSP_6 value:_mainMeterPrePost];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnOnOff:(id)sender {
    btnCurrentOn.selected = !btnCurrentOn.selected;
    switch (currentChannel) {
        case CH_1:
        case CH_2:
        case CH_3:
        case CH_4:
        case CH_5:
        case CH_6:
        case CH_7:
        case CH_8:
        case CH_9:
        case CH_10:
        case CH_11:
        case CH_12:
        case CH_13:
        case CH_14:
        case CH_15:
        case CH_16:
            [Global setClearBit:&_ch1_16OnOff bitValue:btnCurrentOn.selected bitOrder:currentChannel];
            [viewController sendData:CMD_CH_ON_OFF commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16OnOff];
            break;
        case CH_17:
            [Global setClearBit:&_ch17AudioSetting bitValue:btnCurrentOn.selected bitOrder:8];
            [viewController sendData:CMD_CH17_USB_AUDIO_SETTING commandType:DSP_WRITE dspId:DSP_5 value:_ch17AudioSetting];
            break;
        case CH_18:
            [Global setClearBit:&_ch18AudioSetting bitValue:btnCurrentOn.selected bitOrder:8];
            [viewController sendData:CMD_CH18_USB_AUDIO_SETTING commandType:DSP_WRITE dspId:DSP_5 value:_ch18AudioSetting];
            break;
        case MT_1:
            [Global setClearBit:&_multi1Ctrl bitValue:btnCurrentOn.selected bitOrder:0];
            [viewController sendData:CMD_MULTI_1_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi1Ctrl];
            break;
        case MT_2:
            [Global setClearBit:&_multi2Ctrl bitValue:btnCurrentOn.selected bitOrder:0];
            [viewController sendData:CMD_MULTI_2_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi2Ctrl];
            break;
        case MT_3:
            [Global setClearBit:&_multi3Ctrl bitValue:btnCurrentOn.selected bitOrder:0];
            [viewController sendData:CMD_MULTI_3_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi3Ctrl];
            break;
        case MT_4:
            [Global setClearBit:&_multi4Ctrl bitValue:btnCurrentOn.selected bitOrder:0];
            [viewController sendData:CMD_MULTI_4_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi4Ctrl];
            break;
        case CTRL_RM:
            break;
        case MAIN:
            [Global setClearBit:&_mainLRCtrl bitValue:btnCurrentOn.selected bitOrder:0];
            [viewController sendData:CMD_MAIN_LR_CTRL commandType:DSP_WRITE dspId:DSP_6 value:_mainLRCtrl];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnMeterPrePost:(id)sender {
    btnCurrentMeter.selected = !btnCurrentMeter.selected;
    switch (currentChannel) {
        case CH_1:
        case CH_2:
        case CH_3:
        case CH_4:
        case CH_5:
        case CH_6:
        case CH_7:
        case CH_8:
        case CH_9:
        case CH_10:
        case CH_11:
        case CH_12:
        case CH_13:
        case CH_14:
        case CH_15:
        case CH_16:
            [Global setClearBit:&_ch1_16MeterPrePost bitValue:btnCurrentMeter.selected bitOrder:currentChannel];
            [viewController sendData:CMD_CH_METER commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16MeterPrePost];
            break;
        case CH_17:
            [Global setClearBit:&_ch17AudioSetting bitValue:btnCurrentMeter.selected bitOrder:15];
            [viewController sendData:CMD_CH17_USB_AUDIO_SETTING commandType:DSP_WRITE dspId:DSP_5 value:_ch17AudioSetting];
            break;
        case CH_18:
            [Global setClearBit:&_ch18AudioSetting bitValue:btnCurrentMeter.selected bitOrder:15];
            [viewController sendData:CMD_CH18_USB_AUDIO_SETTING commandType:DSP_WRITE dspId:DSP_5 value:_ch18AudioSetting];
            break;
        case MT_1:
            [Global setClearBit:&_multi_1_4MeterPrePost bitValue:btnCurrentMeter.selected bitOrder:0];
            [viewController sendData:CMD_MULTI_METER commandType:DSP_WRITE dspId:DSP_7 value:_multi_1_4MeterPrePost];
            break;
        case MT_2:
            [Global setClearBit:&_multi_1_4MeterPrePost bitValue:btnCurrentMeter.selected bitOrder:1];
            [viewController sendData:CMD_MULTI_METER commandType:DSP_WRITE dspId:DSP_7 value:_multi_1_4MeterPrePost];
            break;
        case MT_3:
            [Global setClearBit:&_multi_1_4MeterPrePost bitValue:btnCurrentMeter.selected bitOrder:2];
            [viewController sendData:CMD_MULTI_METER commandType:DSP_WRITE dspId:DSP_7 value:_multi_1_4MeterPrePost];
            break;
        case MT_4:
            [Global setClearBit:&_multi_1_4MeterPrePost bitValue:btnCurrentMeter.selected bitOrder:3];
            [viewController sendData:CMD_MULTI_METER commandType:DSP_WRITE dspId:DSP_7 value:_multi_1_4MeterPrePost];
            break;
        case CTRL_RM:
            break;
        case MAIN:
            [Global setClearBit:&_mainMeterPrePost bitValue:btnCurrentMeter.selected bitOrder:0];
            [viewController sendData:CMD_MAIN_METER commandType:DSP_WRITE dspId:DSP_6 value:_mainMeterPrePost];
            break;
        default:
            break;
    }
}

-(IBAction)onSliderCurrentBegin:(id)sender {
    _sliderCurrentLock = YES;
}

-(IBAction)onSliderCurrentEnd:(id)sender {
    _sliderCurrentLock = NO;
}

-(IBAction)onSliderCurrentAction:(id)sender {
    [lblSliderValue setText:[Global getSliderGain:sliderCurrent.value appendDb:YES]];
    switch (currentChannel) {
        case CH_1:
            [viewController sendData:CMD_CH1_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderCurrent.value];
            break;
        case CH_2:
            [viewController sendData:CMD_CH2_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderCurrent.value];
            break;
        case CH_3:
            [viewController sendData:CMD_CH3_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderCurrent.value];
            break;
        case CH_4:
            [viewController sendData:CMD_CH4_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderCurrent.value];
            break;
        case CH_5:
            [viewController sendData:CMD_CH5_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderCurrent.value];
            break;
        case CH_6:
            [viewController sendData:CMD_CH6_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderCurrent.value];
            break;
        case CH_7:
            [viewController sendData:CMD_CH7_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderCurrent.value];
            break;
        case CH_8:
            [viewController sendData:CMD_CH8_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderCurrent.value];
            break;
        case CH_9:
            [viewController sendData:CMD_CH9_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderCurrent.value];
            break;
        case CH_10:
            [viewController sendData:CMD_CH10_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderCurrent.value];
            break;
        case CH_11:
            [viewController sendData:CMD_CH11_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderCurrent.value];
            break;
        case CH_12:
            [viewController sendData:CMD_CH12_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderCurrent.value];
            break;
        case CH_13:
            [viewController sendData:CMD_CH13_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderCurrent.value];
            break;
        case CH_14:
            [viewController sendData:CMD_CH14_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderCurrent.value];
            break;
        case CH_15:
            [viewController sendData:CMD_CH15_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderCurrent.value];
            break;
        case CH_16:
            [viewController sendData:CMD_CH16_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderCurrent.value];
            break;
        case CH_17:
            [viewController sendData:CMD_CH17_USB_AUDIO_FADER commandType:DSP_WRITE dspId:DSP_5 value:sliderCurrent.value];
            break;
        case CH_18:
            [viewController sendData:CMD_CH18_USB_AUDIO_FADER commandType:DSP_WRITE dspId:DSP_5 value:sliderCurrent.value];
            break;
        case MT_1:
            [viewController sendData:CMD_MULTI1_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_7 value:sliderCurrent.value];
            break;
        case MT_2:
            [viewController sendData:CMD_MULTI2_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_7 value:sliderCurrent.value];
            break;
        case MT_3:
            [viewController sendData:CMD_MULTI3_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_7 value:sliderCurrent.value];
            break;
        case MT_4:
            [viewController sendData:CMD_MULTI4_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_7 value:sliderCurrent.value];
            break;
        case CTRL_RM:
            [viewController sendData:CMD_CTRL_RM_TRIM_LEVEL commandType:DSP_WRITE dspId:DSP_6 value:sliderCurrent.value];
            break;
        case MAIN:
            [viewController sendData:CMD_MAIN_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_6 value:sliderCurrent.value];
            break;
        default:
            break;
    }
}

- (IBAction)knobDelayDidChange:(id)sender
{
	lblDelay.text = [Global getDelayString:knobDelay.value type:DELAY_TIME temp:0];
    cpvDelay.progress = knobDelay.value/DELAY_MAX_VALUE;
    if (sender == nil) {
        return;
    }
    
    int delay = [Global knobValueToDelay:knobDelay.value];
    switch (currentChannel) {
        case CH_1:
            [viewController sendData:CMD_CH1_DELAY_TIME commandType:DSP_WRITE dspId:DSP_1 value:delay];
            break;
        case CH_2:
            [viewController sendData:CMD_CH2_DELAY_TIME commandType:DSP_WRITE dspId:DSP_1 value:delay];
            break;
        case CH_3:
            [viewController sendData:CMD_CH3_DELAY_TIME commandType:DSP_WRITE dspId:DSP_1 value:delay];
            break;
        case CH_4:
            [viewController sendData:CMD_CH4_DELAY_TIME commandType:DSP_WRITE dspId:DSP_1 value:delay];
            break;
        case CH_5:
            [viewController sendData:CMD_CH5_DELAY_TIME commandType:DSP_WRITE dspId:DSP_2 value:delay];
            break;
        case CH_6:
            [viewController sendData:CMD_CH6_DELAY_TIME commandType:DSP_WRITE dspId:DSP_2 value:delay];
            break;
        case CH_7:
            [viewController sendData:CMD_CH7_DELAY_TIME commandType:DSP_WRITE dspId:DSP_2 value:delay];
            break;
        case CH_8:
            [viewController sendData:CMD_CH8_DELAY_TIME commandType:DSP_WRITE dspId:DSP_2 value:delay];
            break;
        case CH_9:
            [viewController sendData:CMD_CH9_DELAY_TIME commandType:DSP_WRITE dspId:DSP_3 value:delay];
            break;
        case CH_10:
            [viewController sendData:CMD_CH10_DELAY_TIME commandType:DSP_WRITE dspId:DSP_3 value:delay];
            break;
        case CH_11:
            [viewController sendData:CMD_CH11_DELAY_TIME commandType:DSP_WRITE dspId:DSP_3 value:delay];
            break;
        case CH_12:
            [viewController sendData:CMD_CH12_DELAY_TIME commandType:DSP_WRITE dspId:DSP_3 value:delay];
            break;
        case CH_13:
            [viewController sendData:CMD_CH13_DELAY_TIME commandType:DSP_WRITE dspId:DSP_4 value:delay];
            break;
        case CH_14:
            [viewController sendData:CMD_CH14_DELAY_TIME commandType:DSP_WRITE dspId:DSP_4 value:delay];
            break;
        case CH_15:
            [viewController sendData:CMD_CH15_DELAY_TIME commandType:DSP_WRITE dspId:DSP_4 value:delay];
            break;
        case CH_16:
            [viewController sendData:CMD_CH16_DELAY_TIME commandType:DSP_WRITE dspId:DSP_4 value:delay];
            break;
        case CH_17:
            [viewController sendData:CMD_CH17_DELAY_TIME commandType:DSP_WRITE dspId:DSP_6 value:delay];
            break;
        case CH_18:
            [viewController sendData:CMD_CH18_DELAY_TIME commandType:DSP_WRITE dspId:DSP_6 value:delay];
            break;
        case MT_1:
            [viewController sendData:CMD_MULTI1_DELAY_TIME commandType:DSP_WRITE dspId:DSP_7 value:delay];
            break;
        case MT_2:
            [viewController sendData:CMD_MULTI2_DELAY_TIME commandType:DSP_WRITE dspId:DSP_7 value:delay];
            break;
        case MT_3:
            [viewController sendData:CMD_MULTI3_DELAY_TIME commandType:DSP_WRITE dspId:DSP_7 value:delay];
            break;
        case MT_4:
            [viewController sendData:CMD_MULTI4_DELAY_TIME commandType:DSP_WRITE dspId:DSP_7 value:delay];
            break;
        case MAIN:
            [viewController sendData:CMD_MAIN_LR_DELAY_TIME commandType:DSP_WRITE dspId:DSP_6 value:delay];
            break;
        default:
            break;
    }
}

- (IBAction)knobAux1DidChange:(id)sender
{
	lblAux1.text = [Global getSliderGain:knobAux1.value appendDb:YES];
    cpvAux1.progress = knobAux1.value/GAIN_MAX_VALUE;
    if (sender == nil) {
        return;
    }
    
    switch (currentChannel) {
        case CH_1:
            [viewController sendData:CMD_AUX1_CH1_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux1.value];
            break;
        case CH_2:
            [viewController sendData:CMD_AUX1_CH2_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux1.value];
            break;
        case CH_3:
            [viewController sendData:CMD_AUX1_CH3_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux1.value];
            break;
        case CH_4:
            [viewController sendData:CMD_AUX1_CH4_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux1.value];
            break;
        case CH_5:
            [viewController sendData:CMD_AUX1_CH5_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux1.value];
            break;
        case CH_6:
            [viewController sendData:CMD_AUX1_CH6_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux1.value];
            break;
        case CH_7:
            [viewController sendData:CMD_AUX1_CH7_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux1.value];
            break;
        case CH_8:
            [viewController sendData:CMD_AUX1_CH8_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux1.value];
            break;
        case CH_9:
            [viewController sendData:CMD_AUX1_CH9_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux1.value];
            break;
        case CH_10:
            [viewController sendData:CMD_AUX1_CH10_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux1.value];
            break;
        case CH_11:
            [viewController sendData:CMD_AUX1_CH11_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux1.value];
            break;
        case CH_12:
            [viewController sendData:CMD_AUX1_CH12_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux1.value];
            break;
        case CH_13:
            [viewController sendData:CMD_AUX1_CH13_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux1.value];
            break;
        case CH_14:
            [viewController sendData:CMD_AUX1_CH14_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux1.value];
            break;
        case CH_15:
            [viewController sendData:CMD_AUX1_CH15_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux1.value];
            break;
        case CH_16:
            [viewController sendData:CMD_AUX1_CH16_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux1.value];
            break;
        case CH_17:
            [viewController sendData:CMD_AUX1_CH17_USB_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux1.value];
            break;
        case CH_18:
            [viewController sendData:CMD_AUX1_CH18_USB_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux1.value];
            break;
        default:
            break;
    }
}

- (IBAction)knobAux2DidChange:(id)sender
{
	lblAux2.text = [Global getSliderGain:knobAux2.value appendDb:YES];
    cpvAux2.progress = knobAux2.value/GAIN_MAX_VALUE;
    if (sender == nil) {
        return;
    }

    switch (currentChannel) {
        case CH_1:
            [viewController sendData:CMD_AUX2_CH1_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux2.value];
            break;
        case CH_2:
            [viewController sendData:CMD_AUX2_CH2_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux2.value];
            break;
        case CH_3:
            [viewController sendData:CMD_AUX2_CH3_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux2.value];
            break;
        case CH_4:
            [viewController sendData:CMD_AUX2_CH4_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux2.value];
            break;
        case CH_5:
            [viewController sendData:CMD_AUX2_CH5_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux2.value];
            break;
        case CH_6:
            [viewController sendData:CMD_AUX2_CH6_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux2.value];
            break;
        case CH_7:
            [viewController sendData:CMD_AUX2_CH7_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux2.value];
            break;
        case CH_8:
            [viewController sendData:CMD_AUX2_CH8_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux2.value];
            break;
        case CH_9:
            [viewController sendData:CMD_AUX2_CH9_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux2.value];
            break;
        case CH_10:
            [viewController sendData:CMD_AUX2_CH10_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux2.value];
            break;
        case CH_11:
            [viewController sendData:CMD_AUX2_CH11_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux2.value];
            break;
        case CH_12:
            [viewController sendData:CMD_AUX2_CH12_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux2.value];
            break;
        case CH_13:
            [viewController sendData:CMD_AUX2_CH13_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux2.value];
            break;
        case CH_14:
            [viewController sendData:CMD_AUX2_CH14_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux2.value];
            break;
        case CH_15:
            [viewController sendData:CMD_AUX2_CH15_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux2.value];
            break;
        case CH_16:
            [viewController sendData:CMD_AUX2_CH16_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux2.value];
            break;
        case CH_17:
            [viewController sendData:CMD_AUX2_CH17_USB_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux2.value];
            break;
        case CH_18:
            [viewController sendData:CMD_AUX2_CH18_USB_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux2.value];
            break;
        default:
            break;
    }
}

- (IBAction)knobAux3DidChange:(id)sender
{
	lblAux3.text = [Global getSliderGain:knobAux3.value appendDb:YES];
    cpvAux3.progress = knobAux3.value/GAIN_MAX_VALUE;
    if (sender == nil) {
        return;
    }

    switch (currentChannel) {
        case CH_1:
            [viewController sendData:CMD_AUX3_CH1_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux3.value];
            break;
        case CH_2:
            [viewController sendData:CMD_AUX3_CH2_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux3.value];
            break;
        case CH_3:
            [viewController sendData:CMD_AUX3_CH3_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux3.value];
            break;
        case CH_4:
            [viewController sendData:CMD_AUX3_CH4_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux3.value];
            break;
        case CH_5:
            [viewController sendData:CMD_AUX3_CH5_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux3.value];
            break;
        case CH_6:
            [viewController sendData:CMD_AUX3_CH6_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux3.value];
            break;
        case CH_7:
            [viewController sendData:CMD_AUX3_CH7_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux3.value];
            break;
        case CH_8:
            [viewController sendData:CMD_AUX3_CH8_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux3.value];
            break;
        case CH_9:
            [viewController sendData:CMD_AUX3_CH9_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux3.value];
            break;
        case CH_10:
            [viewController sendData:CMD_AUX3_CH10_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux3.value];
            break;
        case CH_11:
            [viewController sendData:CMD_AUX3_CH11_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux3.value];
            break;
        case CH_12:
            [viewController sendData:CMD_AUX3_CH12_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux3.value];
            break;
        case CH_13:
            [viewController sendData:CMD_AUX3_CH13_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux3.value];
            break;
        case CH_14:
            [viewController sendData:CMD_AUX3_CH14_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux3.value];
            break;
        case CH_15:
            [viewController sendData:CMD_AUX3_CH15_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux3.value];
            break;
        case CH_16:
            [viewController sendData:CMD_AUX3_CH16_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux3.value];
            break;
        case CH_17:
            [viewController sendData:CMD_AUX3_CH17_USB_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux3.value];
            break;
        case CH_18:
            [viewController sendData:CMD_AUX3_CH18_USB_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux3.value];
            break;
        default:
            break;
    }
}

- (IBAction)knobAux4DidChange:(id)sender
{
	lblAux4.text = [Global getSliderGain:knobAux4.value appendDb:YES];
    cpvAux4.progress = knobAux4.value/GAIN_MAX_VALUE;
    if (sender == nil) {
        return;
    }

    switch (currentChannel) {
        case CH_1:
            [viewController sendData:CMD_AUX4_CH1_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux4.value];
            break;
        case CH_2:
            [viewController sendData:CMD_AUX4_CH2_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux4.value];
            break;
        case CH_3:
            [viewController sendData:CMD_AUX4_CH3_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux4.value];
            break;
        case CH_4:
            [viewController sendData:CMD_AUX4_CH4_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux4.value];
            break;
        case CH_5:
            [viewController sendData:CMD_AUX4_CH5_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux4.value];
            break;
        case CH_6:
            [viewController sendData:CMD_AUX4_CH6_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux4.value];
            break;
        case CH_7:
            [viewController sendData:CMD_AUX4_CH7_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux4.value];
            break;
        case CH_8:
            [viewController sendData:CMD_AUX4_CH8_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux4.value];
            break;
        case CH_9:
            [viewController sendData:CMD_AUX4_CH9_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux4.value];
            break;
        case CH_10:
            [viewController sendData:CMD_AUX4_CH10_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux4.value];
            break;
        case CH_11:
            [viewController sendData:CMD_AUX4_CH11_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux4.value];
            break;
        case CH_12:
            [viewController sendData:CMD_AUX4_CH12_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux4.value];
            break;
        case CH_13:
            [viewController sendData:CMD_AUX4_CH13_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux4.value];
            break;
        case CH_14:
            [viewController sendData:CMD_AUX4_CH14_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux4.value];
            break;
        case CH_15:
            [viewController sendData:CMD_AUX4_CH15_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux4.value];
            break;
        case CH_16:
            [viewController sendData:CMD_AUX4_CH16_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux4.value];
            break;
        case CH_17:
            [viewController sendData:CMD_AUX4_CH17_USB_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux4.value];
            break;
        case CH_18:
            [viewController sendData:CMD_AUX4_CH18_USB_FADER_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:knobAux4.value];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnEq:(id)sender {
    btnEq.selected = !btnEq.selected;
    switch (currentChannel) {
        case CH_1:
            [Global setClearBit:&_ch1Ctrl bitValue:btnEq.selected bitOrder:7];
            [viewController sendData:CMD_CH1_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch1Ctrl];
            break;
        case CH_2:
            [Global setClearBit:&_ch2Ctrl bitValue:btnEq.selected bitOrder:7];
            [viewController sendData:CMD_CH2_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch2Ctrl];
            break;
        case CH_3:
            [Global setClearBit:&_ch3Ctrl bitValue:btnEq.selected bitOrder:7];
            [viewController sendData:CMD_CH3_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch3Ctrl];
            break;
        case CH_4:
            [Global setClearBit:&_ch4Ctrl bitValue:btnEq.selected bitOrder:7];
            [viewController sendData:CMD_CH4_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch4Ctrl];
            break;
        case CH_5:
            [Global setClearBit:&_ch5Ctrl bitValue:btnEq.selected bitOrder:7];
            [viewController sendData:CMD_CH5_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch5Ctrl];
            break;
        case CH_6:
            [Global setClearBit:&_ch6Ctrl bitValue:btnEq.selected bitOrder:7];
            [viewController sendData:CMD_CH6_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch6Ctrl];
            break;
        case CH_7:
            [Global setClearBit:&_ch7Ctrl bitValue:btnEq.selected bitOrder:7];
            [viewController sendData:CMD_CH7_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch7Ctrl];
            break;
        case CH_8:
            [Global setClearBit:&_ch8Ctrl bitValue:btnEq.selected bitOrder:7];
            [viewController sendData:CMD_CH8_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch8Ctrl];
            break;
        case CH_9:
            [Global setClearBit:&_ch9Ctrl bitValue:btnEq.selected bitOrder:7];
            [viewController sendData:CMD_CH9_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch9Ctrl];
            break;
        case CH_10:
            [Global setClearBit:&_ch10Ctrl bitValue:btnEq.selected bitOrder:7];
            [viewController sendData:CMD_CH10_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch10Ctrl];
            break;
        case CH_11:
            [Global setClearBit:&_ch11Ctrl bitValue:btnEq.selected bitOrder:7];
            [viewController sendData:CMD_CH11_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch11Ctrl];
            break;
        case CH_12:
            [Global setClearBit:&_ch12Ctrl bitValue:btnEq.selected bitOrder:7];
            [viewController sendData:CMD_CH12_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch12Ctrl];
            break;
        case CH_13:
            [Global setClearBit:&_ch13Ctrl bitValue:btnEq.selected bitOrder:7];
            [viewController sendData:CMD_CH13_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch13Ctrl];
            break;
        case CH_14:
            [Global setClearBit:&_ch14Ctrl bitValue:btnEq.selected bitOrder:7];
            [viewController sendData:CMD_CH14_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch14Ctrl];
            break;
        case CH_15:
            [Global setClearBit:&_ch15Ctrl bitValue:btnEq.selected bitOrder:7];
            [viewController sendData:CMD_CH15_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch15Ctrl];
            break;
        case CH_16:
            [Global setClearBit:&_ch16Ctrl bitValue:btnEq.selected bitOrder:7];
            [viewController sendData:CMD_CH16_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch16Ctrl];
            break;
        case CH_17:
            [Global setClearBit:&_ch17Ctrl bitValue:btnEq.selected bitOrder:7];
            [viewController sendData:CMD_CH17_CTRL commandType:DSP_WRITE dspId:DSP_6 value:_ch17Ctrl];
            break;
        case CH_18:
            [Global setClearBit:&_ch18Ctrl bitValue:btnEq.selected bitOrder:7];
            [viewController sendData:CMD_CH18_CTRL commandType:DSP_WRITE dspId:DSP_6 value:_ch18Ctrl];
            break;
        case MT_1:
            [Global setClearBit:&_multi1Ctrl bitValue:btnEq.selected bitOrder:7];
            [viewController sendData:CMD_MULTI_1_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi1Ctrl];
            break;
        case MT_2:
            [Global setClearBit:&_multi2Ctrl bitValue:btnEq.selected bitOrder:7];
            [viewController sendData:CMD_MULTI_2_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi2Ctrl];
            break;
        case MT_3:
            [Global setClearBit:&_multi3Ctrl bitValue:btnEq.selected bitOrder:7];
            [viewController sendData:CMD_MULTI_3_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi3Ctrl];
            break;
        case MT_4:
            [Global setClearBit:&_multi4Ctrl bitValue:btnEq.selected bitOrder:7];
            [viewController sendData:CMD_MULTI_4_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi4Ctrl];
            break;
        case MAIN:
            [Global setClearBit:&_mainLRCtrl bitValue:btnEq.selected bitOrder:7];
            [viewController sendData:CMD_MAIN_LR_CTRL commandType:DSP_WRITE dspId:DSP_6 value:_mainLRCtrl];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnDyn:(id)sender {
    btnDyn.selected = !btnDyn.selected;
    [self updateDynButtons];
    
    switch (currentChannel) {
        case CH_1:
            [Global setClearBit:&_ch1Ctrl bitValue:btnDyn.selected bitOrder:1];
            [viewController sendData:CMD_CH1_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch1Ctrl];
            break;
        case CH_2:
            [Global setClearBit:&_ch2Ctrl bitValue:btnDyn.selected bitOrder:1];
            [viewController sendData:CMD_CH2_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch2Ctrl];
            break;
        case CH_3:
            [Global setClearBit:&_ch3Ctrl bitValue:btnDyn.selected bitOrder:1];
            [viewController sendData:CMD_CH3_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch3Ctrl];
            break;
        case CH_4:
            [Global setClearBit:&_ch4Ctrl bitValue:btnDyn.selected bitOrder:1];
            [viewController sendData:CMD_CH4_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch4Ctrl];
            break;
        case CH_5:
            [Global setClearBit:&_ch5Ctrl bitValue:btnDyn.selected bitOrder:1];
            [viewController sendData:CMD_CH5_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch5Ctrl];
            break;
        case CH_6:
            [Global setClearBit:&_ch6Ctrl bitValue:btnDyn.selected bitOrder:1];
            [viewController sendData:CMD_CH6_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch6Ctrl];
            break;
        case CH_7:
            [Global setClearBit:&_ch7Ctrl bitValue:btnDyn.selected bitOrder:1];
            [viewController sendData:CMD_CH7_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch7Ctrl];
            break;
        case CH_8:
            [Global setClearBit:&_ch8Ctrl bitValue:btnDyn.selected bitOrder:1];
            [viewController sendData:CMD_CH8_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch8Ctrl];
            break;
        case CH_9:
            [Global setClearBit:&_ch9Ctrl bitValue:btnDyn.selected bitOrder:1];
            [viewController sendData:CMD_CH9_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch9Ctrl];
            break;
        case CH_10:
            [Global setClearBit:&_ch10Ctrl bitValue:btnDyn.selected bitOrder:1];
            [viewController sendData:CMD_CH10_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch10Ctrl];
            break;
        case CH_11:
            [Global setClearBit:&_ch11Ctrl bitValue:btnDyn.selected bitOrder:1];
            [viewController sendData:CMD_CH11_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch11Ctrl];
            break;
        case CH_12:
            [Global setClearBit:&_ch12Ctrl bitValue:btnDyn.selected bitOrder:1];
            [viewController sendData:CMD_CH12_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch12Ctrl];
            break;
        case CH_13:
            [Global setClearBit:&_ch13Ctrl bitValue:btnDyn.selected bitOrder:1];
            [viewController sendData:CMD_CH13_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch13Ctrl];
            break;
        case CH_14:
            [Global setClearBit:&_ch14Ctrl bitValue:btnDyn.selected bitOrder:1];
            [viewController sendData:CMD_CH14_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch14Ctrl];
            break;
        case CH_15:
            [Global setClearBit:&_ch15Ctrl bitValue:btnDyn.selected bitOrder:1];
            [viewController sendData:CMD_CH15_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch15Ctrl];
            break;
        case CH_16:
            [Global setClearBit:&_ch16Ctrl bitValue:btnDyn.selected bitOrder:1];
            [viewController sendData:CMD_CH16_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch16Ctrl];
            break;
        case CH_17:
            [Global setClearBit:&_ch17Ctrl bitValue:btnDyn.selected bitOrder:1];
            [viewController sendData:CMD_CH17_CTRL commandType:DSP_WRITE dspId:DSP_6 value:_ch17Ctrl];
            break;
        case CH_18:
            [Global setClearBit:&_ch18Ctrl bitValue:btnDyn.selected bitOrder:1];
            [viewController sendData:CMD_CH18_CTRL commandType:DSP_WRITE dspId:DSP_6 value:_ch18Ctrl];
            break;
        case MT_1:
            [Global setClearBit:&_multi1Ctrl bitValue:btnDyn.selected bitOrder:1];
            [viewController sendData:CMD_MULTI_1_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi1Ctrl];
            break;
        case MT_2:
            [Global setClearBit:&_multi2Ctrl bitValue:btnDyn.selected bitOrder:1];
            [viewController sendData:CMD_MULTI_2_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi2Ctrl];
            break;
        case MT_3:
            [Global setClearBit:&_multi3Ctrl bitValue:btnDyn.selected bitOrder:1];
            [viewController sendData:CMD_MULTI_3_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi3Ctrl];
            break;
        case MT_4:
            [Global setClearBit:&_multi4Ctrl bitValue:btnDyn.selected bitOrder:1];
            [viewController sendData:CMD_MULTI_4_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi4Ctrl];
            break;
        case MAIN:
            [Global setClearBit:&_mainLRCtrl bitValue:btnDyn.selected bitOrder:1];
            [viewController sendData:CMD_MAIN_LR_CTRL commandType:DSP_WRITE dspId:DSP_6 value:_mainLRCtrl];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnDelay:(id)sender {
    btnDelay.selected = !btnDelay.selected;
    switch (currentChannel) {
        case CH_1:
            [Global setClearBit:&_ch1Ctrl bitValue:btnDelay.selected bitOrder:2];
            [viewController sendData:CMD_CH1_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch1Ctrl];
            break;
        case CH_2:
            [Global setClearBit:&_ch2Ctrl bitValue:btnDelay.selected bitOrder:2];
            [viewController sendData:CMD_CH2_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch2Ctrl];
            break;
        case CH_3:
            [Global setClearBit:&_ch3Ctrl bitValue:btnDelay.selected bitOrder:2];
            [viewController sendData:CMD_CH3_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch3Ctrl];
            break;
        case CH_4:
            [Global setClearBit:&_ch4Ctrl bitValue:btnDelay.selected bitOrder:2];
            [viewController sendData:CMD_CH4_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch4Ctrl];
            break;
        case CH_5:
            [Global setClearBit:&_ch5Ctrl bitValue:btnDelay.selected bitOrder:2];
            [viewController sendData:CMD_CH5_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch5Ctrl];
            break;
        case CH_6:
            [Global setClearBit:&_ch6Ctrl bitValue:btnDelay.selected bitOrder:2];
            [viewController sendData:CMD_CH6_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch6Ctrl];
            break;
        case CH_7:
            [Global setClearBit:&_ch7Ctrl bitValue:btnDelay.selected bitOrder:2];
            [viewController sendData:CMD_CH7_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch7Ctrl];
            break;
        case CH_8:
            [Global setClearBit:&_ch8Ctrl bitValue:btnDelay.selected bitOrder:2];
            [viewController sendData:CMD_CH8_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch8Ctrl];
            break;
        case CH_9:
            [Global setClearBit:&_ch9Ctrl bitValue:btnDelay.selected bitOrder:2];
            [viewController sendData:CMD_CH9_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch9Ctrl];
            break;
        case CH_10:
            [Global setClearBit:&_ch10Ctrl bitValue:btnDelay.selected bitOrder:2];
            [viewController sendData:CMD_CH10_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch10Ctrl];
            break;
        case CH_11:
            [Global setClearBit:&_ch11Ctrl bitValue:btnDelay.selected bitOrder:2];
            [viewController sendData:CMD_CH11_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch11Ctrl];
            break;
        case CH_12:
            [Global setClearBit:&_ch12Ctrl bitValue:btnDelay.selected bitOrder:2];
            [viewController sendData:CMD_CH12_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch12Ctrl];
            break;
        case CH_13:
            [Global setClearBit:&_ch13Ctrl bitValue:btnDelay.selected bitOrder:2];
            [viewController sendData:CMD_CH13_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch13Ctrl];
            break;
        case CH_14:
            [Global setClearBit:&_ch14Ctrl bitValue:btnDelay.selected bitOrder:2];
            [viewController sendData:CMD_CH14_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch14Ctrl];
            break;
        case CH_15:
            [Global setClearBit:&_ch15Ctrl bitValue:btnDelay.selected bitOrder:2];
            [viewController sendData:CMD_CH15_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch15Ctrl];
            break;
        case CH_16:
            [Global setClearBit:&_ch16Ctrl bitValue:btnDelay.selected bitOrder:2];
            [viewController sendData:CMD_CH16_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch16Ctrl];
            break;
        case CH_17:
            [Global setClearBit:&_ch17Ctrl bitValue:btnDelay.selected bitOrder:2];
            [viewController sendData:CMD_CH17_CTRL commandType:DSP_WRITE dspId:DSP_6 value:_ch17Ctrl];
            break;
        case CH_18:
            [Global setClearBit:&_ch18Ctrl bitValue:btnDelay.selected bitOrder:2];
            [viewController sendData:CMD_CH18_CTRL commandType:DSP_WRITE dspId:DSP_6 value:_ch18Ctrl];
            break;
        case MT_1:
            [Global setClearBit:&_multi1Ctrl bitValue:btnDelay.selected bitOrder:2];
            [viewController sendData:CMD_MULTI_1_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi1Ctrl];
            break;
        case MT_2:
            [Global setClearBit:&_multi2Ctrl bitValue:btnDelay.selected bitOrder:2];
            [viewController sendData:CMD_MULTI_2_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi2Ctrl];
            break;
        case MT_3:
            [Global setClearBit:&_multi3Ctrl bitValue:btnDelay.selected bitOrder:2];
            [viewController sendData:CMD_MULTI_3_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi3Ctrl];
            break;
        case MT_4:
            [Global setClearBit:&_multi4Ctrl bitValue:btnDelay.selected bitOrder:2];
            [viewController sendData:CMD_MULTI_4_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi4Ctrl];
            break;
        case MAIN:
            [Global setClearBit:&_mainLRCtrl bitValue:btnDelay.selected bitOrder:2];
            [viewController sendData:CMD_MAIN_LR_CTRL commandType:DSP_WRITE dspId:DSP_6 value:_mainLRCtrl];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnOrder:(id)sender {
    switch (currentViewPage) {
        case VIEW_PAGE_CH_1_8:
        case VIEW_PAGE_CH_9_16:
        case VIEW_PAGE_USB_AUDIO:
            viewOrder.hidden = !viewOrder.hidden;
            break;
        case VIEW_PAGE_MAIN:
            viewOrderMain.hidden = !viewOrderMain.hidden;
            break;
        case VIEW_PAGE_MT_1_4:
            viewOrderMulti.hidden = !viewOrderMulti.hidden;
            break;
        default:
            break;
    }
}

- (IBAction)onBtnGate:(id)sender {
    btnDynGate.selected = !btnDynGate.selected;
    switch (currentChannel) {
        case CH_1:
            [Global setClearBit:&_ch1Ctrl bitValue:btnDynGate.selected bitOrder:8];
            [viewController sendData:CMD_CH1_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch1Ctrl];
            break;
        case CH_2:
            [Global setClearBit:&_ch2Ctrl bitValue:btnDynGate.selected bitOrder:8];
            [viewController sendData:CMD_CH2_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch2Ctrl];
            break;
        case CH_3:
            [Global setClearBit:&_ch3Ctrl bitValue:btnDynGate.selected bitOrder:8];
            [viewController sendData:CMD_CH3_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch3Ctrl];
            break;
        case CH_4:
            [Global setClearBit:&_ch4Ctrl bitValue:btnDynGate.selected bitOrder:8];
            [viewController sendData:CMD_CH4_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch4Ctrl];
            break;
        case CH_5:
            [Global setClearBit:&_ch5Ctrl bitValue:btnDynGate.selected bitOrder:8];
            [viewController sendData:CMD_CH5_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch5Ctrl];
            break;
        case CH_6:
            [Global setClearBit:&_ch6Ctrl bitValue:btnDynGate.selected bitOrder:8];
            [viewController sendData:CMD_CH6_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch6Ctrl];
            break;
        case CH_7:
            [Global setClearBit:&_ch7Ctrl bitValue:btnDynGate.selected bitOrder:8];
            [viewController sendData:CMD_CH7_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch7Ctrl];
            break;
        case CH_8:
            [Global setClearBit:&_ch8Ctrl bitValue:btnDynGate.selected bitOrder:8];
            [viewController sendData:CMD_CH8_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch8Ctrl];
            break;
        case CH_9:
            [Global setClearBit:&_ch9Ctrl bitValue:btnDynGate.selected bitOrder:8];
            [viewController sendData:CMD_CH9_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch9Ctrl];
            break;
        case CH_10:
            [Global setClearBit:&_ch10Ctrl bitValue:btnDynGate.selected bitOrder:8];
            [viewController sendData:CMD_CH10_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch10Ctrl];
            break;
        case CH_11:
            [Global setClearBit:&_ch11Ctrl bitValue:btnDynGate.selected bitOrder:8];
            [viewController sendData:CMD_CH11_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch11Ctrl];
            break;
        case CH_12:
            [Global setClearBit:&_ch12Ctrl bitValue:btnDynGate.selected bitOrder:8];
            [viewController sendData:CMD_CH12_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch12Ctrl];
            break;
        case CH_13:
            [Global setClearBit:&_ch13Ctrl bitValue:btnDynGate.selected bitOrder:8];
            [viewController sendData:CMD_CH13_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch13Ctrl];
            break;
        case CH_14:
            [Global setClearBit:&_ch14Ctrl bitValue:btnDynGate.selected bitOrder:8];
            [viewController sendData:CMD_CH14_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch14Ctrl];
            break;
        case CH_15:
            [Global setClearBit:&_ch15Ctrl bitValue:btnDynGate.selected bitOrder:8];
            [viewController sendData:CMD_CH15_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch15Ctrl];
            break;
        case CH_16:
            [Global setClearBit:&_ch16Ctrl bitValue:btnDynGate.selected bitOrder:8];
            [viewController sendData:CMD_CH16_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch16Ctrl];
            break;
        case CH_17:
            [Global setClearBit:&_ch17Ctrl bitValue:btnDynGate.selected bitOrder:8];
            [viewController sendData:CMD_CH17_CTRL commandType:DSP_WRITE dspId:DSP_6 value:_ch17Ctrl];
            break;
        case CH_18:
            [Global setClearBit:&_ch18Ctrl bitValue:btnDynGate.selected bitOrder:8];
            [viewController sendData:CMD_CH18_CTRL commandType:DSP_WRITE dspId:DSP_6 value:_ch18Ctrl];
            break;
        case MT_1:
            [Global setClearBit:&_multi1Ctrl bitValue:btnDynGate.selected bitOrder:8];
            [viewController sendData:CMD_MULTI_1_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi1Ctrl];
            break;
        case MT_2:
            [Global setClearBit:&_multi2Ctrl bitValue:btnDynGate.selected bitOrder:8];
            [viewController sendData:CMD_MULTI_2_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi2Ctrl];
            break;
        case MT_3:
            [Global setClearBit:&_multi3Ctrl bitValue:btnDynGate.selected bitOrder:8];
            [viewController sendData:CMD_MULTI_3_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi3Ctrl];
            break;
        case MT_4:
            [Global setClearBit:&_multi4Ctrl bitValue:btnDynGate.selected bitOrder:8];
            [viewController sendData:CMD_MULTI_4_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi4Ctrl];
            break;
        case MAIN:
            [Global setClearBit:&_mainLRCtrl bitValue:btnDynGate.selected bitOrder:8];
            [viewController sendData:CMD_MAIN_LR_CTRL commandType:DSP_WRITE dspId:DSP_6 value:_mainLRCtrl];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnExpander:(id)sender {
    btnDynExpander.selected = !btnDynExpander.selected;
    switch (currentChannel) {
        case CH_1:
            [Global setClearBit:&_ch1Ctrl bitValue:btnDynExpander.selected bitOrder:9];
            [viewController sendData:CMD_CH1_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch1Ctrl];
            break;
        case CH_2:
            [Global setClearBit:&_ch2Ctrl bitValue:btnDynExpander.selected bitOrder:9];
            [viewController sendData:CMD_CH2_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch2Ctrl];
            break;
        case CH_3:
            [Global setClearBit:&_ch3Ctrl bitValue:btnDynExpander.selected bitOrder:9];
            [viewController sendData:CMD_CH3_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch3Ctrl];
            break;
        case CH_4:
            [Global setClearBit:&_ch4Ctrl bitValue:btnDynExpander.selected bitOrder:9];
            [viewController sendData:CMD_CH4_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch4Ctrl];
            break;
        case CH_5:
            [Global setClearBit:&_ch5Ctrl bitValue:btnDynExpander.selected bitOrder:9];
            [viewController sendData:CMD_CH5_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch5Ctrl];
            break;
        case CH_6:
            [Global setClearBit:&_ch6Ctrl bitValue:btnDynExpander.selected bitOrder:9];
            [viewController sendData:CMD_CH6_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch6Ctrl];
            break;
        case CH_7:
            [Global setClearBit:&_ch7Ctrl bitValue:btnDynExpander.selected bitOrder:9];
            [viewController sendData:CMD_CH7_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch7Ctrl];
            break;
        case CH_8:
            [Global setClearBit:&_ch8Ctrl bitValue:btnDynExpander.selected bitOrder:9];
            [viewController sendData:CMD_CH8_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch8Ctrl];
            break;
        case CH_9:
            [Global setClearBit:&_ch9Ctrl bitValue:btnDynExpander.selected bitOrder:9];
            [viewController sendData:CMD_CH9_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch9Ctrl];
            break;
        case CH_10:
            [Global setClearBit:&_ch10Ctrl bitValue:btnDynExpander.selected bitOrder:9];
            [viewController sendData:CMD_CH10_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch10Ctrl];
            break;
        case CH_11:
            [Global setClearBit:&_ch11Ctrl bitValue:btnDynExpander.selected bitOrder:9];
            [viewController sendData:CMD_CH11_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch11Ctrl];
            break;
        case CH_12:
            [Global setClearBit:&_ch12Ctrl bitValue:btnDynExpander.selected bitOrder:9];
            [viewController sendData:CMD_CH12_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch12Ctrl];
            break;
        case CH_13:
            [Global setClearBit:&_ch13Ctrl bitValue:btnDynExpander.selected bitOrder:9];
            [viewController sendData:CMD_CH13_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch13Ctrl];
            break;
        case CH_14:
            [Global setClearBit:&_ch14Ctrl bitValue:btnDynExpander.selected bitOrder:9];
            [viewController sendData:CMD_CH14_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch14Ctrl];
            break;
        case CH_15:
            [Global setClearBit:&_ch15Ctrl bitValue:btnDynExpander.selected bitOrder:9];
            [viewController sendData:CMD_CH15_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch15Ctrl];
            break;
        case CH_16:
            [Global setClearBit:&_ch16Ctrl bitValue:btnDynExpander.selected bitOrder:9];
            [viewController sendData:CMD_CH16_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch16Ctrl];
            break;
        case CH_17:
            [Global setClearBit:&_ch17Ctrl bitValue:btnDynExpander.selected bitOrder:9];
            [viewController sendData:CMD_CH17_CTRL commandType:DSP_WRITE dspId:DSP_6 value:_ch17Ctrl];
            break;
        case CH_18:
            [Global setClearBit:&_ch18Ctrl bitValue:btnDynExpander.selected bitOrder:9];
            [viewController sendData:CMD_CH18_CTRL commandType:DSP_WRITE dspId:DSP_6 value:_ch18Ctrl];
            break;
        case MT_1:
            [Global setClearBit:&_multi1Ctrl bitValue:btnDynExpander.selected bitOrder:9];
            [viewController sendData:CMD_MULTI_1_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi1Ctrl];
            break;
        case MT_2:
            [Global setClearBit:&_multi2Ctrl bitValue:btnDynExpander.selected bitOrder:9];
            [viewController sendData:CMD_MULTI_2_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi2Ctrl];
            break;
        case MT_3:
            [Global setClearBit:&_multi3Ctrl bitValue:btnDynExpander.selected bitOrder:9];
            [viewController sendData:CMD_MULTI_3_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi3Ctrl];
            break;
        case MT_4:
            [Global setClearBit:&_multi4Ctrl bitValue:btnDynExpander.selected bitOrder:9];
            [viewController sendData:CMD_MULTI_4_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi4Ctrl];
            break;
        case MAIN:
            [Global setClearBit:&_mainLRCtrl bitValue:btnDynExpander.selected bitOrder:9];
            [viewController sendData:CMD_MAIN_LR_CTRL commandType:DSP_WRITE dspId:DSP_6 value:_mainLRCtrl];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnComp:(id)sender {
    btnDynComp.selected = !btnDynComp.selected;
    switch (currentChannel) {
        case CH_1:
            [Global setClearBit:&_ch1Ctrl bitValue:btnDynComp.selected bitOrder:10];
            [viewController sendData:CMD_CH1_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch1Ctrl];
            break;
        case CH_2:
            [Global setClearBit:&_ch2Ctrl bitValue:btnDynComp.selected bitOrder:10];
            [viewController sendData:CMD_CH2_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch2Ctrl];
            break;
        case CH_3:
            [Global setClearBit:&_ch3Ctrl bitValue:btnDynComp.selected bitOrder:10];
            [viewController sendData:CMD_CH3_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch3Ctrl];
            break;
        case CH_4:
            [Global setClearBit:&_ch4Ctrl bitValue:btnDynComp.selected bitOrder:10];
            [viewController sendData:CMD_CH4_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch4Ctrl];
            break;
        case CH_5:
            [Global setClearBit:&_ch5Ctrl bitValue:btnDynComp.selected bitOrder:10];
            [viewController sendData:CMD_CH5_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch5Ctrl];
            break;
        case CH_6:
            [Global setClearBit:&_ch6Ctrl bitValue:btnDynComp.selected bitOrder:10];
            [viewController sendData:CMD_CH6_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch6Ctrl];
            break;
        case CH_7:
            [Global setClearBit:&_ch7Ctrl bitValue:btnDynComp.selected bitOrder:10];
            [viewController sendData:CMD_CH7_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch7Ctrl];
            break;
        case CH_8:
            [Global setClearBit:&_ch8Ctrl bitValue:btnDynComp.selected bitOrder:10];
            [viewController sendData:CMD_CH8_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch8Ctrl];
            break;
        case CH_9:
            [Global setClearBit:&_ch9Ctrl bitValue:btnDynComp.selected bitOrder:10];
            [viewController sendData:CMD_CH9_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch9Ctrl];
            break;
        case CH_10:
            [Global setClearBit:&_ch10Ctrl bitValue:btnDynComp.selected bitOrder:10];
            [viewController sendData:CMD_CH10_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch10Ctrl];
            break;
        case CH_11:
            [Global setClearBit:&_ch11Ctrl bitValue:btnDynComp.selected bitOrder:10];
            [viewController sendData:CMD_CH11_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch11Ctrl];
            break;
        case CH_12:
            [Global setClearBit:&_ch12Ctrl bitValue:btnDynComp.selected bitOrder:10];
            [viewController sendData:CMD_CH12_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch12Ctrl];
            break;
        case CH_13:
            [Global setClearBit:&_ch13Ctrl bitValue:btnDynComp.selected bitOrder:10];
            [viewController sendData:CMD_CH13_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch13Ctrl];
            break;
        case CH_14:
            [Global setClearBit:&_ch14Ctrl bitValue:btnDynComp.selected bitOrder:10];
            [viewController sendData:CMD_CH14_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch14Ctrl];
            break;
        case CH_15:
            [Global setClearBit:&_ch15Ctrl bitValue:btnDynComp.selected bitOrder:10];
            [viewController sendData:CMD_CH15_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch15Ctrl];
            break;
        case CH_16:
            [Global setClearBit:&_ch16Ctrl bitValue:btnDynComp.selected bitOrder:10];
            [viewController sendData:CMD_CH16_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch16Ctrl];
            break;
        case CH_17:
            [Global setClearBit:&_ch17Ctrl bitValue:btnDynComp.selected bitOrder:10];
            [viewController sendData:CMD_CH17_CTRL commandType:DSP_WRITE dspId:DSP_6 value:_ch17Ctrl];
            break;
        case CH_18:
            [Global setClearBit:&_ch18Ctrl bitValue:btnDynComp.selected bitOrder:10];
            [viewController sendData:CMD_CH18_CTRL commandType:DSP_WRITE dspId:DSP_6 value:_ch18Ctrl];
            break;
        case MT_1:
            [Global setClearBit:&_multi1Ctrl bitValue:btnDynComp.selected bitOrder:10];
            [viewController sendData:CMD_MULTI_1_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi1Ctrl];
            break;
        case MT_2:
            [Global setClearBit:&_multi2Ctrl bitValue:btnDynComp.selected bitOrder:10];
            [viewController sendData:CMD_MULTI_2_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi2Ctrl];
            break;
        case MT_3:
            [Global setClearBit:&_multi3Ctrl bitValue:btnDynComp.selected bitOrder:10];
            [viewController sendData:CMD_MULTI_3_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi3Ctrl];
            break;
        case MT_4:
            [Global setClearBit:&_multi4Ctrl bitValue:btnDynComp.selected bitOrder:10];
            [viewController sendData:CMD_MULTI_4_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi4Ctrl];
            break;
        case MAIN:
            [Global setClearBit:&_mainLRCtrl bitValue:btnDynComp.selected bitOrder:10];
            [viewController sendData:CMD_MAIN_LR_CTRL commandType:DSP_WRITE dspId:DSP_6 value:_mainLRCtrl];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnLimiter:(id)sender {
    btnDynLimiter.selected = !btnDynLimiter.selected;
    switch (currentChannel) {
        case CH_1:
            [Global setClearBit:&_ch1Ctrl bitValue:btnDynLimiter.selected bitOrder:11];
            [viewController sendData:CMD_CH1_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch1Ctrl];
            break;
        case CH_2:
            [Global setClearBit:&_ch2Ctrl bitValue:btnDynLimiter.selected bitOrder:11];
            [viewController sendData:CMD_CH2_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch2Ctrl];
            break;
        case CH_3:
            [Global setClearBit:&_ch3Ctrl bitValue:btnDynLimiter.selected bitOrder:11];
            [viewController sendData:CMD_CH3_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch3Ctrl];
            break;
        case CH_4:
            [Global setClearBit:&_ch4Ctrl bitValue:btnDynLimiter.selected bitOrder:11];
            [viewController sendData:CMD_CH4_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch4Ctrl];
            break;
        case CH_5:
            [Global setClearBit:&_ch5Ctrl bitValue:btnDynLimiter.selected bitOrder:11];
            [viewController sendData:CMD_CH5_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch5Ctrl];
            break;
        case CH_6:
            [Global setClearBit:&_ch6Ctrl bitValue:btnDynLimiter.selected bitOrder:11];
            [viewController sendData:CMD_CH6_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch6Ctrl];
            break;
        case CH_7:
            [Global setClearBit:&_ch7Ctrl bitValue:btnDynLimiter.selected bitOrder:11];
            [viewController sendData:CMD_CH7_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch7Ctrl];
            break;
        case CH_8:
            [Global setClearBit:&_ch8Ctrl bitValue:btnDynLimiter.selected bitOrder:11];
            [viewController sendData:CMD_CH8_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch8Ctrl];
            break;
        case CH_9:
            [Global setClearBit:&_ch9Ctrl bitValue:btnDynLimiter.selected bitOrder:11];
            [viewController sendData:CMD_CH9_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch9Ctrl];
            break;
        case CH_10:
            [Global setClearBit:&_ch10Ctrl bitValue:btnDynLimiter.selected bitOrder:11];
            [viewController sendData:CMD_CH10_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch10Ctrl];
            break;
        case CH_11:
            [Global setClearBit:&_ch11Ctrl bitValue:btnDynLimiter.selected bitOrder:11];
            [viewController sendData:CMD_CH11_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch11Ctrl];
            break;
        case CH_12:
            [Global setClearBit:&_ch12Ctrl bitValue:btnDynLimiter.selected bitOrder:11];
            [viewController sendData:CMD_CH12_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch12Ctrl];
            break;
        case CH_13:
            [Global setClearBit:&_ch13Ctrl bitValue:btnDynLimiter.selected bitOrder:11];
            [viewController sendData:CMD_CH13_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch13Ctrl];
            break;
        case CH_14:
            [Global setClearBit:&_ch14Ctrl bitValue:btnDynLimiter.selected bitOrder:11];
            [viewController sendData:CMD_CH14_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch14Ctrl];
            break;
        case CH_15:
            [Global setClearBit:&_ch15Ctrl bitValue:btnDynLimiter.selected bitOrder:11];
            [viewController sendData:CMD_CH15_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch15Ctrl];
            break;
        case CH_16:
            [Global setClearBit:&_ch16Ctrl bitValue:btnDynLimiter.selected bitOrder:11];
            [viewController sendData:CMD_CH16_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch16Ctrl];
            break;
        case CH_17:
            [Global setClearBit:&_ch17Ctrl bitValue:btnDynLimiter.selected bitOrder:11];
            [viewController sendData:CMD_CH17_CTRL commandType:DSP_WRITE dspId:DSP_6 value:_ch17Ctrl];
            break;
        case CH_18:
            [Global setClearBit:&_ch18Ctrl bitValue:btnDynLimiter.selected bitOrder:11];
            [viewController sendData:CMD_CH18_CTRL commandType:DSP_WRITE dspId:DSP_6 value:_ch18Ctrl];
            break;
        case MT_1:
            [Global setClearBit:&_multi1Ctrl bitValue:btnDynLimiter.selected bitOrder:11];
            [viewController sendData:CMD_MULTI_1_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi1Ctrl];
            break;
        case MT_2:
            [Global setClearBit:&_multi2Ctrl bitValue:btnDynLimiter.selected bitOrder:11];
            [viewController sendData:CMD_MULTI_2_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi2Ctrl];
            break;
        case MT_3:
            [Global setClearBit:&_multi3Ctrl bitValue:btnDynLimiter.selected bitOrder:11];
            [viewController sendData:CMD_MULTI_3_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi3Ctrl];
            break;
        case MT_4:
            [Global setClearBit:&_multi4Ctrl bitValue:btnDynLimiter.selected bitOrder:11];
            [viewController sendData:CMD_MULTI_4_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi4Ctrl];
            break;
        case MAIN:
            [Global setClearBit:&_mainLRCtrl bitValue:btnDynLimiter.selected bitOrder:11];
            [viewController sendData:CMD_MAIN_LR_CTRL commandType:DSP_WRITE dspId:DSP_6 value:_mainLRCtrl];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnInv:(id)sender {
    btnInv.selected = !btnInv.selected;
    switch (currentChannel) {
        case CH_1:
            [Global setClearBit:&_ch1Ctrl bitValue:btnInv.selected bitOrder:12];
            [viewController sendData:CMD_CH1_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch1Ctrl];
            break;
        case CH_2:
            [Global setClearBit:&_ch2Ctrl bitValue:btnInv.selected bitOrder:12];
            [viewController sendData:CMD_CH2_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch2Ctrl];
            break;
        case CH_3:
            [Global setClearBit:&_ch3Ctrl bitValue:btnInv.selected bitOrder:12];
            [viewController sendData:CMD_CH3_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch3Ctrl];
            break;
        case CH_4:
            [Global setClearBit:&_ch4Ctrl bitValue:btnInv.selected bitOrder:12];
            [viewController sendData:CMD_CH4_CTRL commandType:DSP_WRITE dspId:DSP_1 value:_ch4Ctrl];
            break;
        case CH_5:
            [Global setClearBit:&_ch5Ctrl bitValue:btnInv.selected bitOrder:12];
            [viewController sendData:CMD_CH5_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch5Ctrl];
            break;
        case CH_6:
            [Global setClearBit:&_ch6Ctrl bitValue:btnInv.selected bitOrder:12];
            [viewController sendData:CMD_CH6_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch6Ctrl];
            break;
        case CH_7:
            [Global setClearBit:&_ch7Ctrl bitValue:btnInv.selected bitOrder:12];
            [viewController sendData:CMD_CH7_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch7Ctrl];
            break;
        case CH_8:
            [Global setClearBit:&_ch8Ctrl bitValue:btnInv.selected bitOrder:12];
            [viewController sendData:CMD_CH8_CTRL commandType:DSP_WRITE dspId:DSP_2 value:_ch8Ctrl];
            break;
        case CH_9:
            [Global setClearBit:&_ch9Ctrl bitValue:btnInv.selected bitOrder:12];
            [viewController sendData:CMD_CH9_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch9Ctrl];
            break;
        case CH_10:
            [Global setClearBit:&_ch10Ctrl bitValue:btnInv.selected bitOrder:12];
            [viewController sendData:CMD_CH10_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch10Ctrl];
            break;
        case CH_11:
            [Global setClearBit:&_ch11Ctrl bitValue:btnInv.selected bitOrder:12];
            [viewController sendData:CMD_CH11_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch11Ctrl];
            break;
        case CH_12:
            [Global setClearBit:&_ch12Ctrl bitValue:btnInv.selected bitOrder:12];
            [viewController sendData:CMD_CH12_CTRL commandType:DSP_WRITE dspId:DSP_3 value:_ch12Ctrl];
            break;
        case CH_13:
            [Global setClearBit:&_ch13Ctrl bitValue:btnInv.selected bitOrder:12];
            [viewController sendData:CMD_CH13_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch13Ctrl];
            break;
        case CH_14:
            [Global setClearBit:&_ch14Ctrl bitValue:btnInv.selected bitOrder:12];
            [viewController sendData:CMD_CH14_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch14Ctrl];
            break;
        case CH_15:
            [Global setClearBit:&_ch15Ctrl bitValue:btnInv.selected bitOrder:12];
            [viewController sendData:CMD_CH15_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch15Ctrl];
            break;
        case CH_16:
            [Global setClearBit:&_ch16Ctrl bitValue:btnInv.selected bitOrder:12];
            [viewController sendData:CMD_CH16_CTRL commandType:DSP_WRITE dspId:DSP_4 value:_ch16Ctrl];
            break;
        case CH_17:
            [Global setClearBit:&_ch17Ctrl bitValue:btnInv.selected bitOrder:12];
            [viewController sendData:CMD_CH17_CTRL commandType:DSP_WRITE dspId:DSP_6 value:_ch17Ctrl];
            break;
        case CH_18:
            [Global setClearBit:&_ch18Ctrl bitValue:btnInv.selected bitOrder:12];
            [viewController sendData:CMD_CH18_CTRL commandType:DSP_WRITE dspId:DSP_6 value:_ch18Ctrl];
            break;
        case MT_1:
            [Global setClearBit:&_multi1Ctrl bitValue:btnInv.selected bitOrder:12];
            [viewController sendData:CMD_MULTI_1_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi1Ctrl];
            break;
        case MT_2:
            [Global setClearBit:&_multi2Ctrl bitValue:btnInv.selected bitOrder:12];
            [viewController sendData:CMD_MULTI_2_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi2Ctrl];
            break;
        case MT_3:
            [Global setClearBit:&_multi3Ctrl bitValue:btnInv.selected bitOrder:12];
            [viewController sendData:CMD_MULTI_3_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi3Ctrl];
            break;
        case MT_4:
            [Global setClearBit:&_multi4Ctrl bitValue:btnInv.selected bitOrder:12];
            [viewController sendData:CMD_MULTI_4_CTRL commandType:DSP_WRITE dspId:DSP_7 value:_multi4Ctrl];
            break;
        case MAIN:
            [Global setClearBit:&_mainLRCtrl bitValue:btnInv.selected bitOrder:12];
            [viewController sendData:CMD_MAIN_LR_CTRL commandType:DSP_WRITE dspId:DSP_6 value:_mainLRCtrl];
            break;
        default:
            break;
    }
}

#pragma mark -
#pragma mark CtrlRm panel event handler methods

- (void)updateMainAflPflArrow {
    if (imgChannel1Solo.hidden &&
        imgChannel2Solo.hidden &&
        imgChannel3Solo.hidden &&
        imgChannel4Solo.hidden &&
        imgChannel5Solo.hidden &&
        imgChannel6Solo.hidden &&
        imgChannel7Solo.hidden &&
        imgChannel8Solo.hidden &&
        imgChannel9Solo.hidden &&
        imgChannel10Solo.hidden &&
        imgChannel11Solo.hidden &&
        imgChannel12Solo.hidden &&
        imgChannel13Solo.hidden &&
        imgChannel14Solo.hidden &&
        imgChannel15Solo.hidden &&
        imgChannel16Solo.hidden &&
        imgChannel17Solo.hidden &&
        imgChannel18Solo.hidden &&
        imgAux1Solo.hidden &&
        imgAux2Solo.hidden &&
        imgAux3Solo.hidden &&
        imgAux4Solo.hidden &&
        imgGp1Solo.hidden &&
        imgGp2Solo.hidden &&
        imgGp3Solo.hidden &&
        imgGp4Solo.hidden &&
        imgEfx1Solo.hidden &&
        imgEfx2Solo.hidden) {
        [imgMainPflAflArrow setHidden:NO];
    } else {
        [imgMainPflAflArrow setHidden:YES];
    }
}

- (IBAction)onBtnGlobalPfl:(id)sender {
    btnChannel1.selected = NO;
    btnChannel2.selected = NO;
    btnChannel3.selected = NO;
    btnChannel4.selected = NO;
    btnChannel5.selected = NO;
    btnChannel6.selected = NO;
    btnChannel7.selected = NO;
    btnChannel8.selected = NO;
    btnChannel9.selected = NO;
    btnChannel10.selected = NO;
    btnChannel11.selected = NO;
    btnChannel12.selected = NO;
    btnChannel13.selected = NO;
    btnChannel14.selected = NO;
    btnChannel15.selected = NO;
    btnChannel16.selected = NO;
    btnChannel17.selected = NO;
    btnChannel18.selected = NO;
    btnAux1.selected = NO;
    btnAux2.selected = NO;
    btnAux3.selected = NO;
    btnAux4.selected = NO;
    btnGp1.selected = NO;
    btnGp2.selected = NO;
    btnGp3.selected = NO;
    btnGp4.selected = NO;
    btnMainPflAfl.selected = NO;
    
    _ctrlRmChannel = 0;
    _ctrlRmAuxGp = 0;
    [viewController sendData:CMD_CTRL_RM_ON_OFF_CH commandType:DSP_WRITE dspId:DSP_5 value:_ctrlRmChannel];
    [viewController sendData:CMD_CTRL_RM_ON_OFF_AUXGP commandType:DSP_WRITE dspId:DSP_5 value:_ctrlRmAuxGp];
    [Global setClearBit:&_ch17AudioSetting bitValue:btnChannel17.selected bitOrder:12];
    [viewController sendData:CMD_CH17_USB_AUDIO_SETTING commandType:DSP_WRITE dspId:DSP_5 value:_ch17AudioSetting];
    [Global setClearBit:&_ch18AudioSetting bitValue:btnChannel18.selected bitOrder:12];
    [viewController sendData:CMD_CH18_USB_AUDIO_SETTING commandType:DSP_WRITE dspId:DSP_5 value:_ch18AudioSetting];
    [Global setClearBit:&_mainToCtrlRm bitValue:btnMainPflAfl.selected bitOrder:0];
    [viewController sendData:CMD_MAIN_TO_CTRL_RM commandType:DSP_WRITE dspId:DSP_6 value:_mainToCtrlRm];
}

- (IBAction)onBtnGlobalAfl:(id)sender {
    btnChannel1.selected = YES;
    btnChannel2.selected = YES;
    btnChannel3.selected = YES;
    btnChannel4.selected = YES;
    btnChannel5.selected = YES;
    btnChannel6.selected = YES;
    btnChannel7.selected = YES;
    btnChannel8.selected = YES;
    btnChannel9.selected = YES;
    btnChannel10.selected = YES;
    btnChannel11.selected = YES;
    btnChannel12.selected = YES;
    btnChannel13.selected = YES;
    btnChannel14.selected = YES;
    btnChannel15.selected = YES;
    btnChannel16.selected = YES;
    btnChannel17.selected = YES;
    btnChannel18.selected = YES;
    btnAux1.selected = YES;
    btnAux2.selected = YES;
    btnAux3.selected = YES;
    btnAux4.selected = YES;
    btnGp1.selected = YES;
    btnGp2.selected = YES;
    btnGp3.selected = YES;
    btnGp4.selected = YES;
    btnMainPflAfl.selected = YES;
    
    _ctrlRmChannel = 0xFFFF;
    _ctrlRmAuxGp = 0x0F0F;
    [viewController sendData:CMD_CTRL_RM_ON_OFF_CH commandType:DSP_WRITE dspId:DSP_5 value:_ctrlRmChannel];
    [viewController sendData:CMD_CTRL_RM_ON_OFF_AUXGP commandType:DSP_WRITE dspId:DSP_5 value:_ctrlRmAuxGp];
    [Global setClearBit:&_ch17AudioSetting bitValue:btnChannel17.selected bitOrder:12];
    [viewController sendData:CMD_CH17_USB_AUDIO_SETTING commandType:DSP_WRITE dspId:DSP_5 value:_ch17AudioSetting];
    [Global setClearBit:&_ch18AudioSetting bitValue:btnChannel18.selected bitOrder:12];
    [viewController sendData:CMD_CH18_USB_AUDIO_SETTING commandType:DSP_WRITE dspId:DSP_5 value:_ch18AudioSetting];
    [Global setClearBit:&_mainToCtrlRm bitValue:btnMainPflAfl.selected bitOrder:0];
    [viewController sendData:CMD_MAIN_TO_CTRL_RM commandType:DSP_WRITE dspId:DSP_6 value:_mainToCtrlRm];
}

- (IBAction)onBtnGlobalAll:(id)sender {
    imgChannel1Solo.hidden = YES;
    imgChannel2Solo.hidden = YES;
    imgChannel3Solo.hidden = YES;
    imgChannel4Solo.hidden = YES;
    imgChannel5Solo.hidden = YES;
    imgChannel6Solo.hidden = YES;
    imgChannel7Solo.hidden = YES;
    imgChannel8Solo.hidden = YES;
    imgChannel9Solo.hidden = YES;
    imgChannel10Solo.hidden = YES;
    imgChannel11Solo.hidden = YES;
    imgChannel12Solo.hidden = YES;
    imgChannel13Solo.hidden = YES;
    imgChannel14Solo.hidden = YES;
    imgChannel15Solo.hidden = YES;
    imgChannel16Solo.hidden = YES;
    imgChannel17Solo.hidden = YES;
    imgChannel18Solo.hidden = YES;
    imgAux1Solo.hidden = YES;
    imgAux2Solo.hidden = YES;
    imgAux3Solo.hidden = YES;
    imgAux4Solo.hidden = YES;
    imgGp1Solo.hidden = YES;
    imgGp2Solo.hidden = YES;
    imgGp3Solo.hidden = YES;
    imgGp4Solo.hidden = YES;
    imgEfx1Solo.hidden = YES;
    imgEfx2Solo.hidden = YES;
    
    _soloCtrlChannel = 0;
    _soloCtrlAuxGp = 0;
    [viewController sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloCtrlChannel];
    [viewController sendData:CMD_AUXGP_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloCtrlAuxGp];
    [Global setClearBit:&_ch17AudioSetting bitValue:!imgChannel17Solo.hidden bitOrder:10];
    [viewController sendData:CMD_CH17_USB_AUDIO_SETTING commandType:DSP_WRITE dspId:DSP_5 value:_ch17AudioSetting];
    [Global setClearBit:&_ch18AudioSetting bitValue:!imgChannel18Solo.hidden bitOrder:10];
    [viewController sendData:CMD_CH18_USB_AUDIO_SETTING commandType:DSP_WRITE dspId:DSP_5 value:_ch18AudioSetting];
    [Global setClearBit:&_gpToMain bitValue:!imgEfx1Solo.hidden bitOrder:10];
    [viewController sendData:CMD_GP_TO_MAIN_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_gpToMain];
    [Global setClearBit:&_gpToMain bitValue:!imgEfx2Solo.hidden bitOrder:11];
    [viewController sendData:CMD_GP_TO_MAIN_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_gpToMain];
    
    [self updateMainAflPflArrow];
}

- (IBAction)onBtnCtrlRmSolo:(id)sender {
    btnSolo.selected = YES;
    btnSoloSafe.selected = NO;
    btnSoloPflAfl.selected = NO;
}

- (IBAction)onBtnSoloSafe:(id)sender {
    btnSolo.selected = NO;
    btnSoloSafe.selected = YES;
    btnSoloPflAfl.selected = NO;
}

- (IBAction)onBtnSoloPflAfl:(id)sender {
    btnSolo.selected = NO;
    btnSoloSafe.selected = NO;
    btnSoloPflAfl.selected = YES;
}

- (IBAction)onBtnMainPflAfl:(id)sender {
    btnMainPflAfl.selected = !btnMainPflAfl.selected;
    [Global setClearBit:&_mainToCtrlRm bitValue:btnMainPflAfl.selected bitOrder:0];
    [viewController sendData:CMD_MAIN_TO_CTRL_RM commandType:DSP_WRITE dspId:DSP_6 value:_mainToCtrlRm];
}

- (IBAction)onBtnEfx1:(id)sender {
    if (btnSoloSafe.selected) {
        [lblEfx1Safe setHidden:!lblEfx1Safe.hidden];
        [Global setClearBit:&_gpToMain bitValue:!lblEfx1Safe.hidden bitOrder:12];
        [viewController sendData:CMD_GP_TO_MAIN_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_gpToMain];
    } else if (btnSolo.selected) {
        [imgEfx1Solo setHidden:!imgEfx1Solo.hidden];
        [Global setClearBit:&_gpToMain bitValue:!imgEfx1Solo.hidden bitOrder:10];
        [viewController sendData:CMD_GP_TO_MAIN_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_gpToMain];
        [self updateMainAflPflArrow];
    }
}

- (IBAction)onBtnEfx2:(id)sender {
    if (btnSoloSafe.selected) {
        [lblEfx2Safe setHidden:!lblEfx2Safe.hidden];
        [Global setClearBit:&_gpToMain bitValue:!lblEfx2Safe.hidden bitOrder:13];
        [viewController sendData:CMD_GP_TO_MAIN_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_gpToMain];
    } else if (btnSolo.selected) {
        [imgEfx2Solo setHidden:!imgEfx2Solo.hidden];
        [Global setClearBit:&_gpToMain bitValue:!imgEfx2Solo.hidden bitOrder:11];
        [viewController sendData:CMD_GP_TO_MAIN_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_gpToMain];
        [self updateMainAflPflArrow];
    }
}

- (IBAction)onBtnChannel1:(id)sender {
    if (btnSoloSafe.selected) {
        [lblChannel1Safe setHidden:!lblChannel1Safe.hidden];
        [Global setClearBit:&_soloSafeCtrlChannel bitValue:!lblChannel1Safe.hidden bitOrder:0];
        [viewController sendData:CMD_SOLO_SAFE_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloSafeCtrlChannel];
    } else if (btnSoloPflAfl.selected) {
        btnChannel1.selected = !btnChannel1.selected;
        [Global setClearBit:&_ctrlRmChannel bitValue:btnChannel1.selected bitOrder:0];
        [viewController sendData:CMD_CTRL_RM_ON_OFF_CH commandType:DSP_WRITE dspId:DSP_5 value:_ctrlRmChannel];
    } else if (btnSolo.selected) {
        [imgChannel1Solo setHidden:!imgChannel1Solo.hidden];
        [Global setClearBit:&_soloCtrlChannel bitValue:!imgChannel1Solo.hidden bitOrder:0];
        [viewController sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloCtrlChannel];
        [self updateMainAflPflArrow];
    }
}

- (IBAction)onBtnChannel2:(id)sender {
    if (btnSoloSafe.selected) {
        [lblChannel2Safe setHidden:!lblChannel2Safe.hidden];
        [Global setClearBit:&_soloSafeCtrlChannel bitValue:!lblChannel2Safe.hidden bitOrder:1];
        [viewController sendData:CMD_SOLO_SAFE_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloSafeCtrlChannel];
    } else if (btnSoloPflAfl.selected) {
        btnChannel2.selected = !btnChannel2.selected;
        [Global setClearBit:&_ctrlRmChannel bitValue:btnChannel2.selected bitOrder:1];
        [viewController sendData:CMD_CTRL_RM_ON_OFF_CH commandType:DSP_WRITE dspId:DSP_5 value:_ctrlRmChannel];
    } else if (btnSolo.selected) {
        [imgChannel2Solo setHidden:!imgChannel2Solo.hidden];
        [Global setClearBit:&_soloCtrlChannel bitValue:!imgChannel2Solo.hidden bitOrder:1];
        [viewController sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloCtrlChannel];
        [self updateMainAflPflArrow];
    }
}

- (IBAction)onBtnChannel3:(id)sender {
    if (btnSoloSafe.selected) {
        [lblChannel3Safe setHidden:!lblChannel3Safe.hidden];
        [Global setClearBit:&_soloSafeCtrlChannel bitValue:!lblChannel3Safe.hidden bitOrder:2];
        [viewController sendData:CMD_SOLO_SAFE_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloSafeCtrlChannel];
    } else if (btnSoloPflAfl.selected) {
        btnChannel3.selected = !btnChannel3.selected;
        [Global setClearBit:&_ctrlRmChannel bitValue:btnChannel3.selected bitOrder:2];
        [viewController sendData:CMD_CTRL_RM_ON_OFF_CH commandType:DSP_WRITE dspId:DSP_5 value:_ctrlRmChannel];
    } else if (btnSolo.selected) {
        [imgChannel3Solo setHidden:!imgChannel3Solo.hidden];
        [Global setClearBit:&_soloCtrlChannel bitValue:!imgChannel3Solo.hidden bitOrder:2];
        [viewController sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloCtrlChannel];
        [self updateMainAflPflArrow];
    }
}

- (IBAction)onBtnChannel4:(id)sender {
    if (btnSoloSafe.selected) {
        [lblChannel4Safe setHidden:!lblChannel4Safe.hidden];
        [Global setClearBit:&_soloSafeCtrlChannel bitValue:!lblChannel4Safe.hidden bitOrder:3];
        [viewController sendData:CMD_SOLO_SAFE_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloSafeCtrlChannel];
    } else if (btnSoloPflAfl.selected) {
        btnChannel4.selected = !btnChannel4.selected;
        [Global setClearBit:&_ctrlRmChannel bitValue:btnChannel4.selected bitOrder:3];
        [viewController sendData:CMD_CTRL_RM_ON_OFF_CH commandType:DSP_WRITE dspId:DSP_5 value:_ctrlRmChannel];
    } else if (btnSolo.selected) {
        [imgChannel4Solo setHidden:!imgChannel4Solo.hidden];
        [Global setClearBit:&_soloCtrlChannel bitValue:!imgChannel4Solo.hidden bitOrder:3];
        [viewController sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloCtrlChannel];
        [self updateMainAflPflArrow];
    }
}

- (IBAction)onBtnChannel5:(id)sender {
    if (btnSoloSafe.selected) {
        [lblChannel5Safe setHidden:!lblChannel5Safe.hidden];
        [Global setClearBit:&_soloSafeCtrlChannel bitValue:!lblChannel5Safe.hidden bitOrder:4];
        [viewController sendData:CMD_SOLO_SAFE_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloSafeCtrlChannel];
    } else if (btnSoloPflAfl.selected) {
        btnChannel5.selected = !btnChannel5.selected;
        [Global setClearBit:&_ctrlRmChannel bitValue:btnChannel5.selected bitOrder:4];
        [viewController sendData:CMD_CTRL_RM_ON_OFF_CH commandType:DSP_WRITE dspId:DSP_5 value:_ctrlRmChannel];
    } else if (btnSolo.selected) {
        [imgChannel5Solo setHidden:!imgChannel5Solo.hidden];
        [Global setClearBit:&_soloCtrlChannel bitValue:!imgChannel5Solo.hidden bitOrder:4];
        [viewController sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloCtrlChannel];
        [self updateMainAflPflArrow];
    }
}

- (IBAction)onBtnChannel6:(id)sender {
    if (btnSoloSafe.selected) {
        [lblChannel6Safe setHidden:!lblChannel6Safe.hidden];
        [Global setClearBit:&_soloSafeCtrlChannel bitValue:!lblChannel6Safe.hidden bitOrder:5];
        [viewController sendData:CMD_SOLO_SAFE_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloSafeCtrlChannel];
    } else if (btnSoloPflAfl.selected) {
        btnChannel6.selected = !btnChannel6.selected;
        [Global setClearBit:&_ctrlRmChannel bitValue:btnChannel6.selected bitOrder:5];
        [viewController sendData:CMD_CTRL_RM_ON_OFF_CH commandType:DSP_WRITE dspId:DSP_5 value:_ctrlRmChannel];
    } else if (btnSolo.selected) {
        [imgChannel6Solo setHidden:!imgChannel6Solo.hidden];
        [Global setClearBit:&_soloCtrlChannel bitValue:!imgChannel6Solo.hidden bitOrder:5];
        [viewController sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloCtrlChannel];
        [self updateMainAflPflArrow];
    }
}

- (IBAction)onBtnChannel7:(id)sender {
    if (btnSoloSafe.selected) {
        [lblChannel7Safe setHidden:!lblChannel7Safe.hidden];
        [Global setClearBit:&_soloSafeCtrlChannel bitValue:!lblChannel7Safe.hidden bitOrder:6];
        [viewController sendData:CMD_SOLO_SAFE_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloSafeCtrlChannel];
    } else if (btnSoloPflAfl.selected) {
        btnChannel7.selected = !btnChannel7.selected;
        [Global setClearBit:&_ctrlRmChannel bitValue:btnChannel7.selected bitOrder:6];
        [viewController sendData:CMD_CTRL_RM_ON_OFF_CH commandType:DSP_WRITE dspId:DSP_5 value:_ctrlRmChannel];
    } else if (btnSolo.selected) {
        [imgChannel7Solo setHidden:!imgChannel7Solo.hidden];
        [Global setClearBit:&_soloCtrlChannel bitValue:!imgChannel7Solo.hidden bitOrder:6];
        [viewController sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloCtrlChannel];
        [self updateMainAflPflArrow];
    }
}

- (IBAction)onBtnChannel8:(id)sender {
    if (btnSoloSafe.selected) {
        [lblChannel8Safe setHidden:!lblChannel8Safe.hidden];
        [Global setClearBit:&_soloSafeCtrlChannel bitValue:!lblChannel8Safe.hidden bitOrder:7];
        [viewController sendData:CMD_SOLO_SAFE_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloSafeCtrlChannel];
    } else if (btnSoloPflAfl.selected) {
        btnChannel8.selected = !btnChannel8.selected;
        [Global setClearBit:&_ctrlRmChannel bitValue:btnChannel8.selected bitOrder:7];
        [viewController sendData:CMD_CTRL_RM_ON_OFF_CH commandType:DSP_WRITE dspId:DSP_5 value:_ctrlRmChannel];
    } else if (btnSolo.selected) {
        [imgChannel8Solo setHidden:!imgChannel8Solo.hidden];
        [Global setClearBit:&_soloCtrlChannel bitValue:!imgChannel8Solo.hidden bitOrder:7];
        [viewController sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloCtrlChannel];
        [self updateMainAflPflArrow];
    }
}

- (IBAction)onBtnChannel9:(id)sender {
    if (btnSoloSafe.selected) {
        [lblChannel9Safe setHidden:!lblChannel9Safe.hidden];
        [Global setClearBit:&_soloSafeCtrlChannel bitValue:!lblChannel9Safe.hidden bitOrder:8];
        [viewController sendData:CMD_SOLO_SAFE_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloSafeCtrlChannel];
    } else if (btnSoloPflAfl.selected) {
        btnChannel9.selected = !btnChannel9.selected;
        [Global setClearBit:&_ctrlRmChannel bitValue:btnChannel9.selected bitOrder:8];
        [viewController sendData:CMD_CTRL_RM_ON_OFF_CH commandType:DSP_WRITE dspId:DSP_5 value:_ctrlRmChannel];
    } else if (btnSolo.selected) {
        [imgChannel9Solo setHidden:!imgChannel9Solo.hidden];
        [Global setClearBit:&_soloCtrlChannel bitValue:!imgChannel9Solo.hidden bitOrder:8];
        [viewController sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloCtrlChannel];
        [self updateMainAflPflArrow];
    }
}

- (IBAction)onBtnChannel10:(id)sender {
    if (btnSoloSafe.selected) {
        [lblChannel10Safe setHidden:!lblChannel10Safe.hidden];
        [Global setClearBit:&_soloSafeCtrlChannel bitValue:!lblChannel10Safe.hidden bitOrder:9];
        [viewController sendData:CMD_SOLO_SAFE_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloSafeCtrlChannel];
    } else if (btnSoloPflAfl.selected) {
        btnChannel10.selected = !btnChannel10.selected;
        [Global setClearBit:&_ctrlRmChannel bitValue:btnChannel10.selected bitOrder:9];
        [viewController sendData:CMD_CTRL_RM_ON_OFF_CH commandType:DSP_WRITE dspId:DSP_5 value:_ctrlRmChannel];
    } else if (btnSolo.selected) {
        [imgChannel10Solo setHidden:!imgChannel10Solo.hidden];
        [Global setClearBit:&_soloCtrlChannel bitValue:!imgChannel10Solo.hidden bitOrder:9];
        [viewController sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloCtrlChannel];
        [self updateMainAflPflArrow];
    }
}

- (IBAction)onBtnChannel11:(id)sender {
    if (btnSoloSafe.selected) {
        [lblChannel11Safe setHidden:!lblChannel11Safe.hidden];
        [Global setClearBit:&_soloSafeCtrlChannel bitValue:!lblChannel11Safe.hidden bitOrder:10];
        [viewController sendData:CMD_SOLO_SAFE_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloSafeCtrlChannel];
    } else if (btnSoloPflAfl.selected) {
        btnChannel11.selected = !btnChannel11.selected;
        [Global setClearBit:&_ctrlRmChannel bitValue:btnChannel11.selected bitOrder:10];
        [viewController sendData:CMD_CTRL_RM_ON_OFF_CH commandType:DSP_WRITE dspId:DSP_5 value:_ctrlRmChannel];
    } else if (btnSolo.selected) {
        [imgChannel11Solo setHidden:!imgChannel11Solo.hidden];
        [Global setClearBit:&_soloCtrlChannel bitValue:!imgChannel11Solo.hidden bitOrder:10];
        [viewController sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloCtrlChannel];
        [self updateMainAflPflArrow];
    }
}

- (IBAction)onBtnChannel12:(id)sender {
    if (btnSoloSafe.selected) {
        [lblChannel12Safe setHidden:!lblChannel12Safe.hidden];
        [Global setClearBit:&_soloSafeCtrlChannel bitValue:!lblChannel12Safe.hidden bitOrder:11];
        [viewController sendData:CMD_SOLO_SAFE_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloSafeCtrlChannel];
    } else if (btnSoloPflAfl.selected) {
        btnChannel12.selected = !btnChannel12.selected;
        [Global setClearBit:&_ctrlRmChannel bitValue:btnChannel12.selected bitOrder:11];
        [viewController sendData:CMD_CTRL_RM_ON_OFF_CH commandType:DSP_WRITE dspId:DSP_5 value:_ctrlRmChannel];
    } else if (btnSolo.selected) {
        [imgChannel12Solo setHidden:!imgChannel12Solo.hidden];
        [Global setClearBit:&_soloCtrlChannel bitValue:!imgChannel12Solo.hidden bitOrder:11];
        [viewController sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloCtrlChannel];
        [self updateMainAflPflArrow];
    }
}

- (IBAction)onBtnChannel13:(id)sender {
    if (btnSoloSafe.selected) {
        [lblChannel13Safe setHidden:!lblChannel13Safe.hidden];
        [Global setClearBit:&_soloSafeCtrlChannel bitValue:!lblChannel13Safe.hidden bitOrder:12];
        [viewController sendData:CMD_SOLO_SAFE_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloSafeCtrlChannel];
    } else if (btnSoloPflAfl.selected) {
        btnChannel13.selected = !btnChannel13.selected;
        [Global setClearBit:&_ctrlRmChannel bitValue:btnChannel13.selected bitOrder:12];
        [viewController sendData:CMD_CTRL_RM_ON_OFF_CH commandType:DSP_WRITE dspId:DSP_5 value:_ctrlRmChannel];
    } else if (btnSolo.selected) {
        [imgChannel13Solo setHidden:!imgChannel13Solo.hidden];
        [Global setClearBit:&_soloCtrlChannel bitValue:!imgChannel13Solo.hidden bitOrder:12];
        [viewController sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloCtrlChannel];
        [self updateMainAflPflArrow];
    }
}

- (IBAction)onBtnChannel14:(id)sender {
    if (btnSoloSafe.selected) {
        [lblChannel14Safe setHidden:!lblChannel14Safe.hidden];
        [Global setClearBit:&_soloSafeCtrlChannel bitValue:!lblChannel14Safe.hidden bitOrder:13];
        [viewController sendData:CMD_SOLO_SAFE_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloSafeCtrlChannel];
    } else if (btnSoloPflAfl.selected) {
        btnChannel14.selected = !btnChannel14.selected;
        [Global setClearBit:&_ctrlRmChannel bitValue:btnChannel14.selected bitOrder:13];
        [viewController sendData:CMD_CTRL_RM_ON_OFF_CH commandType:DSP_WRITE dspId:DSP_5 value:_ctrlRmChannel];
    } else if (btnSolo.selected) {
        [imgChannel14Solo setHidden:!imgChannel14Solo.hidden];
        [Global setClearBit:&_soloCtrlChannel bitValue:!imgChannel14Solo.hidden bitOrder:13];
        [viewController sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloCtrlChannel];
        [self updateMainAflPflArrow];
    }
}

- (IBAction)onBtnChannel15:(id)sender {
    if (btnSoloSafe.selected) {
        [lblChannel15Safe setHidden:!lblChannel15Safe.hidden];
        [Global setClearBit:&_soloSafeCtrlChannel bitValue:!lblChannel15Safe.hidden bitOrder:14];
        [viewController sendData:CMD_SOLO_SAFE_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloSafeCtrlChannel];
    } else if (btnSoloPflAfl.selected) {
        btnChannel15.selected = !btnChannel15.selected;
        [Global setClearBit:&_ctrlRmChannel bitValue:btnChannel15.selected bitOrder:14];
        [viewController sendData:CMD_CTRL_RM_ON_OFF_CH commandType:DSP_WRITE dspId:DSP_5 value:_ctrlRmChannel];
    } else if (btnSolo.selected) {
        [imgChannel15Solo setHidden:!imgChannel15Solo.hidden];
        [Global setClearBit:&_soloCtrlChannel bitValue:!imgChannel15Solo.hidden bitOrder:14];
        [viewController sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloCtrlChannel];
        [self updateMainAflPflArrow];
    }
}

- (IBAction)onBtnChannel16:(id)sender {
    if (btnSoloSafe.selected) {
        [lblChannel16Safe setHidden:!lblChannel16Safe.hidden];
        [Global setClearBit:&_soloSafeCtrlChannel bitValue:!lblChannel16Safe.hidden bitOrder:15];
        [viewController sendData:CMD_SOLO_SAFE_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloSafeCtrlChannel];
    } else if (btnSoloPflAfl.selected) {
        btnChannel16.selected = !btnChannel16.selected;
        [Global setClearBit:&_ctrlRmChannel bitValue:btnChannel16.selected bitOrder:15];
        [viewController sendData:CMD_CTRL_RM_ON_OFF_CH commandType:DSP_WRITE dspId:DSP_5 value:_ctrlRmChannel];
    } else if (btnSolo.selected) {
        [imgChannel16Solo setHidden:!imgChannel16Solo.hidden];
        [Global setClearBit:&_soloCtrlChannel bitValue:!imgChannel16Solo.hidden bitOrder:15];
        [viewController sendData:CMD_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloCtrlChannel];
        [self updateMainAflPflArrow];
    }
}

- (IBAction)onBtnChannel17:(id)sender {
    if (btnSoloSafe.selected) {
        [lblChannel17Safe setHidden:!lblChannel17Safe.hidden];
        [Global setClearBit:&_ch17AudioSetting bitValue:!lblChannel17Safe.hidden bitOrder:11];
        [viewController sendData:CMD_CH17_USB_AUDIO_SETTING commandType:DSP_WRITE dspId:DSP_5 value:_ch17AudioSetting];
    } else if (btnSoloPflAfl.selected) {
        btnChannel17.selected = !btnChannel17.selected;
        [Global setClearBit:&_ch17AudioSetting bitValue:btnChannel17.selected bitOrder:12];
        [viewController sendData:CMD_CH17_USB_AUDIO_SETTING commandType:DSP_WRITE dspId:DSP_5 value:_ch17AudioSetting];
    } else if (btnSolo.selected) {
        [imgChannel17Solo setHidden:!imgChannel17Solo.hidden];
        [Global setClearBit:&_ch17AudioSetting bitValue:!imgChannel17Solo.hidden bitOrder:10];
        [viewController sendData:CMD_CH17_USB_AUDIO_SETTING commandType:DSP_WRITE dspId:DSP_5 value:_ch17AudioSetting];
        [self updateMainAflPflArrow];
    }
}

- (IBAction)onBtnChannel18:(id)sender {
    if (btnSoloSafe.selected) {
        [lblChannel18Safe setHidden:!lblChannel18Safe.hidden];
        [Global setClearBit:&_ch18AudioSetting bitValue:!lblChannel18Safe.hidden bitOrder:11];
        [viewController sendData:CMD_CH18_USB_AUDIO_SETTING commandType:DSP_WRITE dspId:DSP_5 value:_ch18AudioSetting];
    } else if (btnSoloPflAfl.selected) {
        btnChannel18.selected = !btnChannel18.selected;
        [Global setClearBit:&_ch18AudioSetting bitValue:btnChannel18.selected bitOrder:12];
        [viewController sendData:CMD_CH18_USB_AUDIO_SETTING commandType:DSP_WRITE dspId:DSP_5 value:_ch18AudioSetting];
    } else if (btnSolo.selected) {
        [imgChannel18Solo setHidden:!imgChannel18Solo.hidden];
        [Global setClearBit:&_ch18AudioSetting bitValue:!imgChannel18Solo.hidden bitOrder:10];
        [viewController sendData:CMD_CH18_USB_AUDIO_SETTING commandType:DSP_WRITE dspId:DSP_5 value:_ch18AudioSetting];
        [self updateMainAflPflArrow];
    }
}

- (IBAction)onBtnAux1:(id)sender {
    if (btnSoloSafe.selected) {
        [lblAux1Safe setHidden:!lblAux1Safe.hidden];
        [Global setClearBit:&_soloSafeCtrlAuxGp bitValue:!lblAux1Safe.hidden bitOrder:8];
        [viewController sendData:CMD_AUXGP_SOLO_SAFE_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloSafeCtrlAuxGp];
    } else if (btnSoloPflAfl.selected) {
        btnAux1.selected = !btnAux1.selected;
        [Global setClearBit:&_ctrlRmAuxGp bitValue:btnAux1.selected bitOrder:8];
        [viewController sendData:CMD_CTRL_RM_ON_OFF_AUXGP commandType:DSP_WRITE dspId:DSP_5 value:_ctrlRmAuxGp];
    } else if (btnSolo.selected) {
        [imgAux1Solo setHidden:!imgAux1Solo.hidden];
        [Global setClearBit:&_soloCtrlAuxGp bitValue:!imgAux1Solo.hidden bitOrder:8];
        [viewController sendData:CMD_AUXGP_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloCtrlAuxGp];
        [self updateMainAflPflArrow];
    }
}

- (IBAction)onBtnAux2:(id)sender {
    if (btnSoloSafe.selected) {
        [lblAux2Safe setHidden:!lblAux2Safe.hidden];
        [Global setClearBit:&_soloSafeCtrlAuxGp bitValue:!lblAux2Safe.hidden bitOrder:9];
        [viewController sendData:CMD_AUXGP_SOLO_SAFE_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloSafeCtrlAuxGp];
    } else if (btnSoloPflAfl.selected) {
        btnAux2.selected = !btnAux2.selected;
        [Global setClearBit:&_ctrlRmAuxGp bitValue:btnAux2.selected bitOrder:9];
        [viewController sendData:CMD_CTRL_RM_ON_OFF_AUXGP commandType:DSP_WRITE dspId:DSP_5 value:_ctrlRmAuxGp];
    } else if (btnSolo.selected) {
        [imgAux2Solo setHidden:!imgAux2Solo.hidden];
        [Global setClearBit:&_soloCtrlAuxGp bitValue:!imgAux2Solo.hidden bitOrder:9];
        [viewController sendData:CMD_AUXGP_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloCtrlAuxGp];
        [self updateMainAflPflArrow];
    }
}

- (IBAction)onBtnAux3:(id)sender {
    if (btnSoloSafe.selected) {
        [lblAux3Safe setHidden:!lblAux3Safe.hidden];
        [Global setClearBit:&_soloSafeCtrlAuxGp bitValue:!lblAux3Safe.hidden bitOrder:10];
        [viewController sendData:CMD_AUXGP_SOLO_SAFE_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloSafeCtrlAuxGp];
    } else if (btnSoloPflAfl.selected) {
        btnAux3.selected = !btnAux3.selected;
        [Global setClearBit:&_ctrlRmAuxGp bitValue:btnAux3.selected bitOrder:10];
        [viewController sendData:CMD_CTRL_RM_ON_OFF_AUXGP commandType:DSP_WRITE dspId:DSP_5 value:_ctrlRmAuxGp];
    } else if (btnSolo.selected) {
        [imgAux3Solo setHidden:!imgAux3Solo.hidden];
        [Global setClearBit:&_soloCtrlAuxGp bitValue:!imgAux3Solo.hidden bitOrder:10];
        [viewController sendData:CMD_AUXGP_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloCtrlAuxGp];
        [self updateMainAflPflArrow];
    }
}

- (IBAction)onBtnAux4:(id)sender {
    if (btnSoloSafe.selected) {
        [lblAux4Safe setHidden:!lblAux4Safe.hidden];
        [Global setClearBit:&_soloSafeCtrlAuxGp bitValue:!lblAux4Safe.hidden bitOrder:11];
        [viewController sendData:CMD_AUXGP_SOLO_SAFE_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloSafeCtrlAuxGp];
    } else if (btnSoloPflAfl.selected) {
        btnAux4.selected = !btnAux4.selected;
        [Global setClearBit:&_ctrlRmAuxGp bitValue:btnAux4.selected bitOrder:11];
        [viewController sendData:CMD_CTRL_RM_ON_OFF_AUXGP commandType:DSP_WRITE dspId:DSP_5 value:_ctrlRmAuxGp];
    } else if (btnSolo.selected) {
        [imgAux4Solo setHidden:!imgAux4Solo.hidden];
        [Global setClearBit:&_soloCtrlAuxGp bitValue:!imgAux4Solo.hidden bitOrder:11];
        [viewController sendData:CMD_AUXGP_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloCtrlAuxGp];
        [self updateMainAflPflArrow];
    }
}

- (IBAction)onBtnGp1:(id)sender {
    if (btnSoloSafe.selected) {
        [lblGp1Safe setHidden:!lblGp1Safe.hidden];
        [Global setClearBit:&_soloSafeCtrlAuxGp bitValue:!lblGp1Safe.hidden bitOrder:0];
        [viewController sendData:CMD_AUXGP_SOLO_SAFE_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloSafeCtrlAuxGp];
    } else if (btnSoloPflAfl.selected) {
        btnGp1.selected = !btnGp1.selected;
        [Global setClearBit:&_ctrlRmAuxGp bitValue:btnGp1.selected bitOrder:0];
        [viewController sendData:CMD_CTRL_RM_ON_OFF_AUXGP commandType:DSP_WRITE dspId:DSP_5 value:_ctrlRmAuxGp];
    } else if (btnSolo.selected) {
        [imgGp1Solo setHidden:!imgGp1Solo.hidden];
        [Global setClearBit:&_soloCtrlAuxGp bitValue:!imgGp1Solo.hidden bitOrder:0];
        [viewController sendData:CMD_AUXGP_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloCtrlAuxGp];
        [self updateMainAflPflArrow];
    }
}

- (IBAction)onBtnGp2:(id)sender {
    if (btnSoloSafe.selected) {
        [lblGp2Safe setHidden:!lblGp2Safe.hidden];
        [Global setClearBit:&_soloSafeCtrlAuxGp bitValue:!lblGp2Safe.hidden bitOrder:1];
        [viewController sendData:CMD_AUXGP_SOLO_SAFE_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloSafeCtrlAuxGp];
    } else if (btnSoloPflAfl.selected) {
        btnGp2.selected = !btnGp2.selected;
        [Global setClearBit:&_ctrlRmAuxGp bitValue:btnGp2.selected bitOrder:1];
        [viewController sendData:CMD_CTRL_RM_ON_OFF_AUXGP commandType:DSP_WRITE dspId:DSP_5 value:_ctrlRmAuxGp];
    } else if (btnSolo.selected) {
        [imgGp2Solo setHidden:!imgGp2Solo.hidden];
        [Global setClearBit:&_soloCtrlAuxGp bitValue:!imgGp2Solo.hidden bitOrder:1];
        [viewController sendData:CMD_AUXGP_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloCtrlAuxGp];
        [self updateMainAflPflArrow];
    }
}

- (IBAction)onBtnGp3:(id)sender {
    if (btnSoloSafe.selected) {
        [lblGp3Safe setHidden:!lblGp3Safe.hidden];
        [Global setClearBit:&_soloSafeCtrlAuxGp bitValue:!lblGp3Safe.hidden bitOrder:2];
        [viewController sendData:CMD_AUXGP_SOLO_SAFE_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloSafeCtrlAuxGp];
    } else if (btnSoloPflAfl.selected) {
        btnGp3.selected = !btnGp3.selected;
        [Global setClearBit:&_ctrlRmAuxGp bitValue:btnGp3.selected bitOrder:2];
        [viewController sendData:CMD_CTRL_RM_ON_OFF_AUXGP commandType:DSP_WRITE dspId:DSP_5 value:_ctrlRmAuxGp];
    } else if (btnSolo.selected) {
        [imgGp3Solo setHidden:!imgGp3Solo.hidden];
        [Global setClearBit:&_soloCtrlAuxGp bitValue:!imgGp3Solo.hidden bitOrder:2];
        [viewController sendData:CMD_AUXGP_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloCtrlAuxGp];
        [self updateMainAflPflArrow];
    }
}

- (IBAction)onBtnGp4:(id)sender {
    if (btnSoloSafe.selected) {
        [lblGp4Safe setHidden:!lblGp4Safe.hidden];
        [Global setClearBit:&_soloSafeCtrlAuxGp bitValue:!lblGp4Safe.hidden bitOrder:3];
        [viewController sendData:CMD_AUXGP_SOLO_SAFE_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloSafeCtrlAuxGp];
    } else if (btnSoloPflAfl.selected) {
        btnGp4.selected = !btnGp4.selected;
        [Global setClearBit:&_ctrlRmAuxGp bitValue:btnGp4.selected bitOrder:3];
        [viewController sendData:CMD_CTRL_RM_ON_OFF_AUXGP commandType:DSP_WRITE dspId:DSP_5 value:_ctrlRmAuxGp];
    } else if (btnSolo.selected) {
        [imgGp4Solo setHidden:!imgGp4Solo.hidden];
        [Global setClearBit:&_soloCtrlAuxGp bitValue:!imgGp4Solo.hidden bitOrder:3];
        [viewController sendData:CMD_AUXGP_SOLO_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_soloCtrlAuxGp];
        [self updateMainAflPflArrow];
    }
}

#pragma mark -
#pragma mark Channel bottom panel event handler methods

- (IBAction)onBtnChannelAux1PrePost:(id)sender {
    btnChannelAux1PostPre.selected = !btnChannelAux1PostPre.selected;
    switch (currentChannel) {
        case CH_1:
            [Global setClearBit:&_aux1SendPrePost bitValue:btnChannelAux1PostPre.selected bitOrder:0];
            [viewController sendData:CMD_AUX1_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux1SendPrePost];
            break;
        case CH_2:
            [Global setClearBit:&_aux1SendPrePost bitValue:btnChannelAux1PostPre.selected bitOrder:1];
            [viewController sendData:CMD_AUX1_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux1SendPrePost];
            break;
        case CH_3:
            [Global setClearBit:&_aux1SendPrePost bitValue:btnChannelAux1PostPre.selected bitOrder:2];
            [viewController sendData:CMD_AUX1_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux1SendPrePost];
            break;
        case CH_4:
            [Global setClearBit:&_aux1SendPrePost bitValue:btnChannelAux1PostPre.selected bitOrder:3];
            [viewController sendData:CMD_AUX1_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux1SendPrePost];
            break;
        case CH_5:
            [Global setClearBit:&_aux1SendPrePost bitValue:btnChannelAux1PostPre.selected bitOrder:4];
            [viewController sendData:CMD_AUX1_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux1SendPrePost];
            break;
        case CH_6:
            [Global setClearBit:&_aux1SendPrePost bitValue:btnChannelAux1PostPre.selected bitOrder:5];
            [viewController sendData:CMD_AUX1_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux1SendPrePost];
            break;
        case CH_7:
            [Global setClearBit:&_aux1SendPrePost bitValue:btnChannelAux1PostPre.selected bitOrder:6];
            [viewController sendData:CMD_AUX1_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux1SendPrePost];
            break;
        case CH_8:
            [Global setClearBit:&_aux1SendPrePost bitValue:btnChannelAux1PostPre.selected bitOrder:7];
            [viewController sendData:CMD_AUX1_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux1SendPrePost];
            break;
        case CH_9:
            [Global setClearBit:&_aux1SendPrePost bitValue:btnChannelAux1PostPre.selected bitOrder:8];
            [viewController sendData:CMD_AUX1_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux1SendPrePost];
            break;
        case CH_10:
            [Global setClearBit:&_aux1SendPrePost bitValue:btnChannelAux1PostPre.selected bitOrder:9];
            [viewController sendData:CMD_AUX1_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux1SendPrePost];
            break;
        case CH_11:
            [Global setClearBit:&_aux1SendPrePost bitValue:btnChannelAux1PostPre.selected bitOrder:10];
            [viewController sendData:CMD_AUX1_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux1SendPrePost];
            break;
        case CH_12:
            [Global setClearBit:&_aux1SendPrePost bitValue:btnChannelAux1PostPre.selected bitOrder:11];
            [viewController sendData:CMD_AUX1_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux1SendPrePost];
            break;
        case CH_13:
            [Global setClearBit:&_aux1SendPrePost bitValue:btnChannelAux1PostPre.selected bitOrder:12];
            [viewController sendData:CMD_AUX1_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux1SendPrePost];
            break;
        case CH_14:
            [Global setClearBit:&_aux1SendPrePost bitValue:btnChannelAux1PostPre.selected bitOrder:13];
            [viewController sendData:CMD_AUX1_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux1SendPrePost];
            break;
        case CH_15:
            [Global setClearBit:&_aux1SendPrePost bitValue:btnChannelAux1PostPre.selected bitOrder:14];
            [viewController sendData:CMD_AUX1_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux1SendPrePost];
            break;
        case CH_16:
            [Global setClearBit:&_aux1SendPrePost bitValue:btnChannelAux1PostPre.selected bitOrder:15];
            [viewController sendData:CMD_AUX1_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux1SendPrePost];
            break;
        case CH_17:
            [Global setClearBit:&_ch17AudioSetting bitValue:btnChannelAux1PostPre.selected bitOrder:4];
            [viewController sendData:CMD_CH17_USB_AUDIO_SETTING commandType:DSP_WRITE dspId:DSP_5 value:_ch17AudioSetting];
            break;
        case CH_18:
            [Global setClearBit:&_ch18AudioSetting bitValue:btnChannelAux1PostPre.selected bitOrder:4];
            [viewController sendData:CMD_CH18_USB_AUDIO_SETTING commandType:DSP_WRITE dspId:DSP_5 value:_ch18AudioSetting];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnChannelAux2PrePost:(id)sender {
    btnChannelAux2PostPre.selected = !btnChannelAux2PostPre.selected;
    switch (currentChannel) {
        case CH_1:
            [Global setClearBit:&_aux2SendPrePost bitValue:btnChannelAux2PostPre.selected bitOrder:0];
            [viewController sendData:CMD_AUX2_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux2SendPrePost];
            break;
        case CH_2:
            [Global setClearBit:&_aux2SendPrePost bitValue:btnChannelAux2PostPre.selected bitOrder:1];
            [viewController sendData:CMD_AUX2_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux2SendPrePost];
            break;
        case CH_3:
            [Global setClearBit:&_aux2SendPrePost bitValue:btnChannelAux2PostPre.selected bitOrder:2];
            [viewController sendData:CMD_AUX2_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux2SendPrePost];
            break;
        case CH_4:
            [Global setClearBit:&_aux2SendPrePost bitValue:btnChannelAux2PostPre.selected bitOrder:3];
            [viewController sendData:CMD_AUX2_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux2SendPrePost];
            break;
        case CH_5:
            [Global setClearBit:&_aux2SendPrePost bitValue:btnChannelAux2PostPre.selected bitOrder:4];
            [viewController sendData:CMD_AUX2_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux2SendPrePost];
            break;
        case CH_6:
            [Global setClearBit:&_aux2SendPrePost bitValue:btnChannelAux2PostPre.selected bitOrder:5];
            [viewController sendData:CMD_AUX2_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux2SendPrePost];
            break;
        case CH_7:
            [Global setClearBit:&_aux2SendPrePost bitValue:btnChannelAux2PostPre.selected bitOrder:6];
            [viewController sendData:CMD_AUX2_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux2SendPrePost];
            break;
        case CH_8:
            [Global setClearBit:&_aux2SendPrePost bitValue:btnChannelAux2PostPre.selected bitOrder:7];
            [viewController sendData:CMD_AUX2_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux2SendPrePost];
            break;
        case CH_9:
            [Global setClearBit:&_aux2SendPrePost bitValue:btnChannelAux2PostPre.selected bitOrder:8];
            [viewController sendData:CMD_AUX2_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux2SendPrePost];
            break;
        case CH_10:
            [Global setClearBit:&_aux2SendPrePost bitValue:btnChannelAux2PostPre.selected bitOrder:9];
            [viewController sendData:CMD_AUX2_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux2SendPrePost];
            break;
        case CH_11:
            [Global setClearBit:&_aux2SendPrePost bitValue:btnChannelAux2PostPre.selected bitOrder:10];
            [viewController sendData:CMD_AUX2_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux2SendPrePost];
            break;
        case CH_12:
            [Global setClearBit:&_aux2SendPrePost bitValue:btnChannelAux2PostPre.selected bitOrder:11];
            [viewController sendData:CMD_AUX2_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux2SendPrePost];
            break;
        case CH_13:
            [Global setClearBit:&_aux2SendPrePost bitValue:btnChannelAux2PostPre.selected bitOrder:12];
            [viewController sendData:CMD_AUX2_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux2SendPrePost];
            break;
        case CH_14:
            [Global setClearBit:&_aux2SendPrePost bitValue:btnChannelAux2PostPre.selected bitOrder:13];
            [viewController sendData:CMD_AUX2_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux2SendPrePost];
            break;
        case CH_15:
            [Global setClearBit:&_aux2SendPrePost bitValue:btnChannelAux2PostPre.selected bitOrder:14];
            [viewController sendData:CMD_AUX2_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux2SendPrePost];
            break;
        case CH_16:
            [Global setClearBit:&_aux2SendPrePost bitValue:btnChannelAux2PostPre.selected bitOrder:15];
            [viewController sendData:CMD_AUX2_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux2SendPrePost];
            break;
        case CH_17:
            [Global setClearBit:&_ch17AudioSetting bitValue:btnChannelAux2PostPre.selected bitOrder:5];
            [viewController sendData:CMD_CH17_USB_AUDIO_SETTING commandType:DSP_WRITE dspId:DSP_5 value:_ch17AudioSetting];
            break;
        case CH_18:
            [Global setClearBit:&_ch18AudioSetting bitValue:btnChannelAux2PostPre.selected bitOrder:5];
            [viewController sendData:CMD_CH18_USB_AUDIO_SETTING commandType:DSP_WRITE dspId:DSP_5 value:_ch18AudioSetting];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnChannelAux3PrePost:(id)sender {
    btnChannelAux3PostPre.selected = !btnChannelAux3PostPre.selected;
    switch (currentChannel) {
        case CH_1:
            [Global setClearBit:&_aux3SendPrePost bitValue:btnChannelAux3PostPre.selected bitOrder:0];
            [viewController sendData:CMD_AUX3_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux3SendPrePost];
            break;
        case CH_2:
            [Global setClearBit:&_aux3SendPrePost bitValue:btnChannelAux3PostPre.selected bitOrder:1];
            [viewController sendData:CMD_AUX3_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux3SendPrePost];
            break;
        case CH_3:
            [Global setClearBit:&_aux3SendPrePost bitValue:btnChannelAux3PostPre.selected bitOrder:2];
            [viewController sendData:CMD_AUX3_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux3SendPrePost];
            break;
        case CH_4:
            [Global setClearBit:&_aux3SendPrePost bitValue:btnChannelAux3PostPre.selected bitOrder:3];
            [viewController sendData:CMD_AUX3_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux3SendPrePost];
            break;
        case CH_5:
            [Global setClearBit:&_aux3SendPrePost bitValue:btnChannelAux3PostPre.selected bitOrder:4];
            [viewController sendData:CMD_AUX3_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux3SendPrePost];
            break;
        case CH_6:
            [Global setClearBit:&_aux3SendPrePost bitValue:btnChannelAux3PostPre.selected bitOrder:5];
            [viewController sendData:CMD_AUX3_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux3SendPrePost];
            break;
        case CH_7:
            [Global setClearBit:&_aux3SendPrePost bitValue:btnChannelAux3PostPre.selected bitOrder:6];
            [viewController sendData:CMD_AUX3_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux3SendPrePost];
            break;
        case CH_8:
            [Global setClearBit:&_aux3SendPrePost bitValue:btnChannelAux3PostPre.selected bitOrder:7];
            [viewController sendData:CMD_AUX3_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux3SendPrePost];
            break;
        case CH_9:
            [Global setClearBit:&_aux3SendPrePost bitValue:btnChannelAux3PostPre.selected bitOrder:8];
            [viewController sendData:CMD_AUX3_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux3SendPrePost];
            break;
        case CH_10:
            [Global setClearBit:&_aux3SendPrePost bitValue:btnChannelAux3PostPre.selected bitOrder:9];
            [viewController sendData:CMD_AUX3_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux3SendPrePost];
            break;
        case CH_11:
            [Global setClearBit:&_aux3SendPrePost bitValue:btnChannelAux3PostPre.selected bitOrder:10];
            [viewController sendData:CMD_AUX3_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux3SendPrePost];
            break;
        case CH_12:
            [Global setClearBit:&_aux3SendPrePost bitValue:btnChannelAux3PostPre.selected bitOrder:11];
            [viewController sendData:CMD_AUX3_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux3SendPrePost];
            break;
        case CH_13:
            [Global setClearBit:&_aux3SendPrePost bitValue:btnChannelAux3PostPre.selected bitOrder:12];
            [viewController sendData:CMD_AUX3_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux3SendPrePost];
            break;
        case CH_14:
            [Global setClearBit:&_aux3SendPrePost bitValue:btnChannelAux3PostPre.selected bitOrder:13];
            [viewController sendData:CMD_AUX3_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux3SendPrePost];
            break;
        case CH_15:
            [Global setClearBit:&_aux3SendPrePost bitValue:btnChannelAux3PostPre.selected bitOrder:14];
            [viewController sendData:CMD_AUX3_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux3SendPrePost];
            break;
        case CH_16:
            [Global setClearBit:&_aux3SendPrePost bitValue:btnChannelAux3PostPre.selected bitOrder:15];
            [viewController sendData:CMD_AUX3_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux3SendPrePost];
            break;
        case CH_17:
            [Global setClearBit:&_ch17AudioSetting bitValue:btnChannelAux3PostPre.selected bitOrder:6];
            [viewController sendData:CMD_CH17_USB_AUDIO_SETTING commandType:DSP_WRITE dspId:DSP_5 value:_ch17AudioSetting];
            break;
        case CH_18:
            [Global setClearBit:&_ch18AudioSetting bitValue:btnChannelAux3PostPre.selected bitOrder:6];
            [viewController sendData:CMD_CH18_USB_AUDIO_SETTING commandType:DSP_WRITE dspId:DSP_5 value:_ch18AudioSetting];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnChannelAux4PrePost:(id)sender {
    btnChannelAux4PostPre.selected = !btnChannelAux4PostPre.selected;
    switch (currentChannel) {
        case CH_1:
            [Global setClearBit:&_aux4SendPrePost bitValue:btnChannelAux4PostPre.selected bitOrder:0];
            [viewController sendData:CMD_AUX4_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux4SendPrePost];
            break;
        case CH_2:
            [Global setClearBit:&_aux4SendPrePost bitValue:btnChannelAux4PostPre.selected bitOrder:1];
            [viewController sendData:CMD_AUX4_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux4SendPrePost];
            break;
        case CH_3:
            [Global setClearBit:&_aux4SendPrePost bitValue:btnChannelAux4PostPre.selected bitOrder:2];
            [viewController sendData:CMD_AUX4_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux4SendPrePost];
            break;
        case CH_4:
            [Global setClearBit:&_aux4SendPrePost bitValue:btnChannelAux4PostPre.selected bitOrder:3];
            [viewController sendData:CMD_AUX4_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux4SendPrePost];
            break;
        case CH_5:
            [Global setClearBit:&_aux4SendPrePost bitValue:btnChannelAux4PostPre.selected bitOrder:4];
            [viewController sendData:CMD_AUX4_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux4SendPrePost];
            break;
        case CH_6:
            [Global setClearBit:&_aux4SendPrePost bitValue:btnChannelAux4PostPre.selected bitOrder:5];
            [viewController sendData:CMD_AUX4_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux4SendPrePost];
            break;
        case CH_7:
            [Global setClearBit:&_aux4SendPrePost bitValue:btnChannelAux4PostPre.selected bitOrder:6];
            [viewController sendData:CMD_AUX4_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux4SendPrePost];
            break;
        case CH_8:
            [Global setClearBit:&_aux4SendPrePost bitValue:btnChannelAux4PostPre.selected bitOrder:7];
            [viewController sendData:CMD_AUX4_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux4SendPrePost];
            break;
        case CH_9:
            [Global setClearBit:&_aux4SendPrePost bitValue:btnChannelAux4PostPre.selected bitOrder:8];
            [viewController sendData:CMD_AUX4_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux4SendPrePost];
            break;
        case CH_10:
            [Global setClearBit:&_aux4SendPrePost bitValue:btnChannelAux4PostPre.selected bitOrder:9];
            [viewController sendData:CMD_AUX4_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux4SendPrePost];
            break;
        case CH_11:
            [Global setClearBit:&_aux4SendPrePost bitValue:btnChannelAux4PostPre.selected bitOrder:10];
            [viewController sendData:CMD_AUX4_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux4SendPrePost];
            break;
        case CH_12:
            [Global setClearBit:&_aux4SendPrePost bitValue:btnChannelAux4PostPre.selected bitOrder:11];
            [viewController sendData:CMD_AUX4_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux4SendPrePost];
            break;
        case CH_13:
            [Global setClearBit:&_aux4SendPrePost bitValue:btnChannelAux4PostPre.selected bitOrder:12];
            [viewController sendData:CMD_AUX4_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux4SendPrePost];
            break;
        case CH_14:
            [Global setClearBit:&_aux4SendPrePost bitValue:btnChannelAux4PostPre.selected bitOrder:13];
            [viewController sendData:CMD_AUX4_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux4SendPrePost];
            break;
        case CH_15:
            [Global setClearBit:&_aux4SendPrePost bitValue:btnChannelAux4PostPre.selected bitOrder:14];
            [viewController sendData:CMD_AUX4_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux4SendPrePost];
            break;
        case CH_16:
            [Global setClearBit:&_aux4SendPrePost bitValue:btnChannelAux4PostPre.selected bitOrder:15];
            [viewController sendData:CMD_AUX4_SEND commandType:DSP_WRITE dspId:DSP_5 value:_aux4SendPrePost];
            break;
        case CH_17:
            [Global setClearBit:&_ch17AudioSetting bitValue:btnChannelAux4PostPre.selected bitOrder:7];
            [viewController sendData:CMD_CH17_USB_AUDIO_SETTING commandType:DSP_WRITE dspId:DSP_5 value:_ch17AudioSetting];
            break;
        case CH_18:
            [Global setClearBit:&_ch18AudioSetting bitValue:btnChannelAux4PostPre.selected bitOrder:7];
            [viewController sendData:CMD_CH18_USB_AUDIO_SETTING commandType:DSP_WRITE dspId:DSP_5 value:_ch18AudioSetting];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnChannelGp1OnOff:(id)sender {
    btnChannelGp1OnOff.selected = !btnChannelGp1OnOff.selected;
    switch (currentChannel) {
        case CH_1:
            [Global setClearBit:&_ch1_16AssignGp1 bitValue:btnChannelGp1OnOff.selected bitOrder:0];
            [viewController sendData:CMD_GP1_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp1];
            break;
        case CH_2:
            [Global setClearBit:&_ch1_16AssignGp1 bitValue:btnChannelGp1OnOff.selected bitOrder:1];
            [viewController sendData:CMD_GP1_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp1];
            break;
        case CH_3:
            [Global setClearBit:&_ch1_16AssignGp1 bitValue:btnChannelGp1OnOff.selected bitOrder:2];
            [viewController sendData:CMD_GP1_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp1];
            break;
        case CH_4:
            [Global setClearBit:&_ch1_16AssignGp1 bitValue:btnChannelGp1OnOff.selected bitOrder:3];
            [viewController sendData:CMD_GP1_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp1];
            break;
        case CH_5:
            [Global setClearBit:&_ch1_16AssignGp1 bitValue:btnChannelGp1OnOff.selected bitOrder:4];
            [viewController sendData:CMD_GP1_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp1];
            break;
        case CH_6:
            [Global setClearBit:&_ch1_16AssignGp1 bitValue:btnChannelGp1OnOff.selected bitOrder:5];
            [viewController sendData:CMD_GP1_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp1];
            break;
        case CH_7:
            [Global setClearBit:&_ch1_16AssignGp1 bitValue:btnChannelGp1OnOff.selected bitOrder:6];
            [viewController sendData:CMD_GP1_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp1];
            break;
        case CH_8:
            [Global setClearBit:&_ch1_16AssignGp1 bitValue:btnChannelGp1OnOff.selected bitOrder:7];
            [viewController sendData:CMD_GP1_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp1];
            break;
        case CH_9:
            [Global setClearBit:&_ch1_16AssignGp1 bitValue:btnChannelGp1OnOff.selected bitOrder:8];
            [viewController sendData:CMD_GP1_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp1];
            break;
        case CH_10:
            [Global setClearBit:&_ch1_16AssignGp1 bitValue:btnChannelGp1OnOff.selected bitOrder:9];
            [viewController sendData:CMD_GP1_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp1];
            break;
        case CH_11:
            [Global setClearBit:&_ch1_16AssignGp1 bitValue:btnChannelGp1OnOff.selected bitOrder:10];
            [viewController sendData:CMD_GP1_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp1];
            break;
        case CH_12:
            [Global setClearBit:&_ch1_16AssignGp1 bitValue:btnChannelGp1OnOff.selected bitOrder:11];
            [viewController sendData:CMD_GP1_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp1];
            break;
        case CH_13:
            [Global setClearBit:&_ch1_16AssignGp1 bitValue:btnChannelGp1OnOff.selected bitOrder:12];
            [viewController sendData:CMD_GP1_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp1];
            break;
        case CH_14:
            [Global setClearBit:&_ch1_16AssignGp1 bitValue:btnChannelGp1OnOff.selected bitOrder:13];
            [viewController sendData:CMD_GP1_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp1];
            break;
        case CH_15:
            [Global setClearBit:&_ch1_16AssignGp1 bitValue:btnChannelGp1OnOff.selected bitOrder:14];
            [viewController sendData:CMD_GP1_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp1];
            break;
        case CH_16:
            [Global setClearBit:&_ch1_16AssignGp1 bitValue:btnChannelGp1OnOff.selected bitOrder:15];
            [viewController sendData:CMD_GP1_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp1];
            break;
        case CH_17:
            [Global setClearBit:&_ch17AudioSetting bitValue:btnChannelGp1OnOff.selected bitOrder:0];
            [viewController sendData:CMD_CH17_USB_AUDIO_SETTING commandType:DSP_WRITE dspId:DSP_5 value:_ch17AudioSetting];
            break;
        case CH_18:
            [Global setClearBit:&_ch18AudioSetting bitValue:btnChannelGp1OnOff.selected bitOrder:0];
            [viewController sendData:CMD_CH18_USB_AUDIO_SETTING commandType:DSP_WRITE dspId:DSP_5 value:_ch18AudioSetting];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnChannelGp2OnOff:(id)sender {
    btnChannelGp2OnOff.selected = !btnChannelGp2OnOff.selected;
    switch (currentChannel) {
        case CH_1:
            [Global setClearBit:&_ch1_16AssignGp2 bitValue:btnChannelGp2OnOff.selected bitOrder:0];
            [viewController sendData:CMD_GP2_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp2];
            break;
        case CH_2:
            [Global setClearBit:&_ch1_16AssignGp2 bitValue:btnChannelGp2OnOff.selected bitOrder:1];
            [viewController sendData:CMD_GP2_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp2];
            break;
        case CH_3:
            [Global setClearBit:&_ch1_16AssignGp2 bitValue:btnChannelGp2OnOff.selected bitOrder:2];
            [viewController sendData:CMD_GP2_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp2];
            break;
        case CH_4:
            [Global setClearBit:&_ch1_16AssignGp2 bitValue:btnChannelGp2OnOff.selected bitOrder:3];
            [viewController sendData:CMD_GP2_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp2];
            break;
        case CH_5:
            [Global setClearBit:&_ch1_16AssignGp2 bitValue:btnChannelGp2OnOff.selected bitOrder:4];
            [viewController sendData:CMD_GP2_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp2];
            break;
        case CH_6:
            [Global setClearBit:&_ch1_16AssignGp2 bitValue:btnChannelGp2OnOff.selected bitOrder:5];
            [viewController sendData:CMD_GP2_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp2];
            break;
        case CH_7:
            [Global setClearBit:&_ch1_16AssignGp2 bitValue:btnChannelGp2OnOff.selected bitOrder:6];
            [viewController sendData:CMD_GP2_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp2];
            break;
        case CH_8:
            [Global setClearBit:&_ch1_16AssignGp2 bitValue:btnChannelGp2OnOff.selected bitOrder:7];
            [viewController sendData:CMD_GP2_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp2];
            break;
        case CH_9:
            [Global setClearBit:&_ch1_16AssignGp2 bitValue:btnChannelGp2OnOff.selected bitOrder:8];
            [viewController sendData:CMD_GP2_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp2];
            break;
        case CH_10:
            [Global setClearBit:&_ch1_16AssignGp2 bitValue:btnChannelGp2OnOff.selected bitOrder:9];
            [viewController sendData:CMD_GP2_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp2];
            break;
        case CH_11:
            [Global setClearBit:&_ch1_16AssignGp2 bitValue:btnChannelGp2OnOff.selected bitOrder:10];
            [viewController sendData:CMD_GP2_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp2];
            break;
        case CH_12:
            [Global setClearBit:&_ch1_16AssignGp2 bitValue:btnChannelGp2OnOff.selected bitOrder:11];
            [viewController sendData:CMD_GP2_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp2];
            break;
        case CH_13:
            [Global setClearBit:&_ch1_16AssignGp2 bitValue:btnChannelGp2OnOff.selected bitOrder:12];
            [viewController sendData:CMD_GP2_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp2];
            break;
        case CH_14:
            [Global setClearBit:&_ch1_16AssignGp2 bitValue:btnChannelGp2OnOff.selected bitOrder:13];
            [viewController sendData:CMD_GP2_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp2];
            break;
        case CH_15:
            [Global setClearBit:&_ch1_16AssignGp2 bitValue:btnChannelGp2OnOff.selected bitOrder:14];
            [viewController sendData:CMD_GP2_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp2];
            break;
        case CH_16:
            [Global setClearBit:&_ch1_16AssignGp2 bitValue:btnChannelGp2OnOff.selected bitOrder:15];
            [viewController sendData:CMD_GP2_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp2];
            break;
        case CH_17:
            [Global setClearBit:&_ch17AudioSetting bitValue:btnChannelGp2OnOff.selected bitOrder:1];
            [viewController sendData:CMD_CH17_USB_AUDIO_SETTING commandType:DSP_WRITE dspId:DSP_5 value:_ch17AudioSetting];
            break;
        case CH_18:
            [Global setClearBit:&_ch18AudioSetting bitValue:btnChannelGp2OnOff.selected bitOrder:1];
            [viewController sendData:CMD_CH18_USB_AUDIO_SETTING commandType:DSP_WRITE dspId:DSP_5 value:_ch18AudioSetting];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnChannelGp3OnOff:(id)sender {
    btnChannelGp3OnOff.selected = !btnChannelGp3OnOff.selected;
    switch (currentChannel) {
        case CH_1:
            [Global setClearBit:&_ch1_16AssignGp3 bitValue:btnChannelGp3OnOff.selected bitOrder:0];
            [viewController sendData:CMD_GP3_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp3];
            break;
        case CH_2:
            [Global setClearBit:&_ch1_16AssignGp3 bitValue:btnChannelGp3OnOff.selected bitOrder:1];
            [viewController sendData:CMD_GP3_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp3];
            break;
        case CH_3:
            [Global setClearBit:&_ch1_16AssignGp3 bitValue:btnChannelGp3OnOff.selected bitOrder:2];
            [viewController sendData:CMD_GP3_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp3];
            break;
        case CH_4:
            [Global setClearBit:&_ch1_16AssignGp3 bitValue:btnChannelGp3OnOff.selected bitOrder:3];
            [viewController sendData:CMD_GP3_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp3];
            break;
        case CH_5:
            [Global setClearBit:&_ch1_16AssignGp3 bitValue:btnChannelGp3OnOff.selected bitOrder:4];
            [viewController sendData:CMD_GP3_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp3];
            break;
        case CH_6:
            [Global setClearBit:&_ch1_16AssignGp3 bitValue:btnChannelGp3OnOff.selected bitOrder:5];
            [viewController sendData:CMD_GP3_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp3];
            break;
        case CH_7:
            [Global setClearBit:&_ch1_16AssignGp3 bitValue:btnChannelGp3OnOff.selected bitOrder:6];
            [viewController sendData:CMD_GP3_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp3];
            break;
        case CH_8:
            [Global setClearBit:&_ch1_16AssignGp3 bitValue:btnChannelGp3OnOff.selected bitOrder:7];
            [viewController sendData:CMD_GP3_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp3];
            break;
        case CH_9:
            [Global setClearBit:&_ch1_16AssignGp3 bitValue:btnChannelGp3OnOff.selected bitOrder:8];
            [viewController sendData:CMD_GP3_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp3];
            break;
        case CH_10:
            [Global setClearBit:&_ch1_16AssignGp3 bitValue:btnChannelGp3OnOff.selected bitOrder:9];
            [viewController sendData:CMD_GP3_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp3];
            break;
        case CH_11:
            [Global setClearBit:&_ch1_16AssignGp3 bitValue:btnChannelGp3OnOff.selected bitOrder:10];
            [viewController sendData:CMD_GP3_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp3];
            break;
        case CH_12:
            [Global setClearBit:&_ch1_16AssignGp3 bitValue:btnChannelGp3OnOff.selected bitOrder:11];
            [viewController sendData:CMD_GP3_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp3];
            break;
        case CH_13:
            [Global setClearBit:&_ch1_16AssignGp3 bitValue:btnChannelGp3OnOff.selected bitOrder:12];
            [viewController sendData:CMD_GP3_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp3];
            break;
        case CH_14:
            [Global setClearBit:&_ch1_16AssignGp3 bitValue:btnChannelGp3OnOff.selected bitOrder:13];
            [viewController sendData:CMD_GP3_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp3];
            break;
        case CH_15:
            [Global setClearBit:&_ch1_16AssignGp3 bitValue:btnChannelGp3OnOff.selected bitOrder:14];
            [viewController sendData:CMD_GP3_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp3];
            break;
        case CH_16:
            [Global setClearBit:&_ch1_16AssignGp3 bitValue:btnChannelGp3OnOff.selected bitOrder:15];
            [viewController sendData:CMD_GP3_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp3];
            break;
        case CH_17:
            [Global setClearBit:&_ch17AudioSetting bitValue:btnChannelGp3OnOff.selected bitOrder:2];
            [viewController sendData:CMD_CH17_USB_AUDIO_SETTING commandType:DSP_WRITE dspId:DSP_5 value:_ch17AudioSetting];
            break;
        case CH_18:
            [Global setClearBit:&_ch18AudioSetting bitValue:btnChannelGp3OnOff.selected bitOrder:2];
            [viewController sendData:CMD_CH18_USB_AUDIO_SETTING commandType:DSP_WRITE dspId:DSP_5 value:_ch18AudioSetting];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnChannelGp4OnOff:(id)sender {
    btnChannelGp4OnOff.selected = !btnChannelGp4OnOff.selected;
    switch (currentChannel) {
        case CH_1:
            [Global setClearBit:&_ch1_16AssignGp4 bitValue:btnChannelGp4OnOff.selected bitOrder:0];
            [viewController sendData:CMD_GP4_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp4];
            break;
        case CH_2:
            [Global setClearBit:&_ch1_16AssignGp4 bitValue:btnChannelGp4OnOff.selected bitOrder:1];
            [viewController sendData:CMD_GP4_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp4];
            break;
        case CH_3:
            [Global setClearBit:&_ch1_16AssignGp4 bitValue:btnChannelGp4OnOff.selected bitOrder:2];
            [viewController sendData:CMD_GP4_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp4];
            break;
        case CH_4:
            [Global setClearBit:&_ch1_16AssignGp4 bitValue:btnChannelGp4OnOff.selected bitOrder:3];
            [viewController sendData:CMD_GP4_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp4];
            break;
        case CH_5:
            [Global setClearBit:&_ch1_16AssignGp4 bitValue:btnChannelGp4OnOff.selected bitOrder:4];
            [viewController sendData:CMD_GP4_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp4];
            break;
        case CH_6:
            [Global setClearBit:&_ch1_16AssignGp4 bitValue:btnChannelGp4OnOff.selected bitOrder:5];
            [viewController sendData:CMD_GP4_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp4];
            break;
        case CH_7:
            [Global setClearBit:&_ch1_16AssignGp4 bitValue:btnChannelGp4OnOff.selected bitOrder:6];
            [viewController sendData:CMD_GP4_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp4];
            break;
        case CH_8:
            [Global setClearBit:&_ch1_16AssignGp4 bitValue:btnChannelGp4OnOff.selected bitOrder:7];
            [viewController sendData:CMD_GP4_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp4];
            break;
        case CH_9:
            [Global setClearBit:&_ch1_16AssignGp4 bitValue:btnChannelGp4OnOff.selected bitOrder:8];
            [viewController sendData:CMD_GP4_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp4];
            break;
        case CH_10:
            [Global setClearBit:&_ch1_16AssignGp4 bitValue:btnChannelGp4OnOff.selected bitOrder:9];
            [viewController sendData:CMD_GP4_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp4];
            break;
        case CH_11:
            [Global setClearBit:&_ch1_16AssignGp4 bitValue:btnChannelGp4OnOff.selected bitOrder:10];
            [viewController sendData:CMD_GP4_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp4];
            break;
        case CH_12:
            [Global setClearBit:&_ch1_16AssignGp4 bitValue:btnChannelGp4OnOff.selected bitOrder:11];
            [viewController sendData:CMD_GP4_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp4];
            break;
        case CH_13:
            [Global setClearBit:&_ch1_16AssignGp4 bitValue:btnChannelGp4OnOff.selected bitOrder:12];
            [viewController sendData:CMD_GP4_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp4];
            break;
        case CH_14:
            [Global setClearBit:&_ch1_16AssignGp4 bitValue:btnChannelGp4OnOff.selected bitOrder:13];
            [viewController sendData:CMD_GP4_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp4];
            break;
        case CH_15:
            [Global setClearBit:&_ch1_16AssignGp4 bitValue:btnChannelGp4OnOff.selected bitOrder:14];
            [viewController sendData:CMD_GP4_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp4];
            break;
        case CH_16:
            [Global setClearBit:&_ch1_16AssignGp4 bitValue:btnChannelGp4OnOff.selected bitOrder:15];
            [viewController sendData:CMD_GP4_ASSIGN commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16AssignGp4];
            break;
        case CH_17:
            [Global setClearBit:&_ch17AudioSetting bitValue:btnChannelGp4OnOff.selected bitOrder:3];
            [viewController sendData:CMD_CH17_USB_AUDIO_SETTING commandType:DSP_WRITE dspId:DSP_5 value:_ch17AudioSetting];
            break;
        case CH_18:
            [Global setClearBit:&_ch18AudioSetting bitValue:btnChannelGp4OnOff.selected bitOrder:3];
            [viewController sendData:CMD_CH18_USB_AUDIO_SETTING commandType:DSP_WRITE dspId:DSP_5 value:_ch18AudioSetting];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnChannelToMainOnOff:(id)sender {
    btnChannelToMainOnOff.selected = !btnChannelToMainOnOff.selected;
    switch (currentChannel) {
        case CH_1:
            [Global setClearBit:&_ch1_16ToMainCtrl bitValue:btnChannelToMainOnOff.selected bitOrder:0];
            [viewController sendData:CMD_CH_TO_MAIN_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16ToMainCtrl];
            break;
        case CH_2:
            [Global setClearBit:&_ch1_16ToMainCtrl bitValue:btnChannelToMainOnOff.selected bitOrder:1];
            [viewController sendData:CMD_CH_TO_MAIN_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16ToMainCtrl];
            break;
        case CH_3:
            [Global setClearBit:&_ch1_16ToMainCtrl bitValue:btnChannelToMainOnOff.selected bitOrder:2];
            [viewController sendData:CMD_CH_TO_MAIN_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16ToMainCtrl];
            break;
        case CH_4:
            [Global setClearBit:&_ch1_16ToMainCtrl bitValue:btnChannelToMainOnOff.selected bitOrder:3];
            [viewController sendData:CMD_CH_TO_MAIN_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16ToMainCtrl];
            break;
        case CH_5:
            [Global setClearBit:&_ch1_16ToMainCtrl bitValue:btnChannelToMainOnOff.selected bitOrder:4];
            [viewController sendData:CMD_CH_TO_MAIN_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16ToMainCtrl];
            break;
        case CH_6:
            [Global setClearBit:&_ch1_16ToMainCtrl bitValue:btnChannelToMainOnOff.selected bitOrder:5];
            [viewController sendData:CMD_CH_TO_MAIN_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16ToMainCtrl];
            break;
        case CH_7:
            [Global setClearBit:&_ch1_16ToMainCtrl bitValue:btnChannelToMainOnOff.selected bitOrder:6];
            [viewController sendData:CMD_CH_TO_MAIN_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16ToMainCtrl];
            break;
        case CH_8:
            [Global setClearBit:&_ch1_16ToMainCtrl bitValue:btnChannelToMainOnOff.selected bitOrder:7];
            [viewController sendData:CMD_CH_TO_MAIN_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16ToMainCtrl];
            break;
        case CH_9:
            [Global setClearBit:&_ch1_16ToMainCtrl bitValue:btnChannelToMainOnOff.selected bitOrder:8];
            [viewController sendData:CMD_CH_TO_MAIN_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16ToMainCtrl];
            break;
        case CH_10:
            [Global setClearBit:&_ch1_16ToMainCtrl bitValue:btnChannelToMainOnOff.selected bitOrder:9];
            [viewController sendData:CMD_CH_TO_MAIN_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16ToMainCtrl];
            break;
        case CH_11:
            [Global setClearBit:&_ch1_16ToMainCtrl bitValue:btnChannelToMainOnOff.selected bitOrder:10];
            [viewController sendData:CMD_CH_TO_MAIN_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16ToMainCtrl];
            break;
        case CH_12:
            [Global setClearBit:&_ch1_16ToMainCtrl bitValue:btnChannelToMainOnOff.selected bitOrder:11];
            [viewController sendData:CMD_CH_TO_MAIN_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16ToMainCtrl];
            break;
        case CH_13:
            [Global setClearBit:&_ch1_16ToMainCtrl bitValue:btnChannelToMainOnOff.selected bitOrder:12];
            [viewController sendData:CMD_CH_TO_MAIN_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16ToMainCtrl];
            break;
        case CH_14:
            [Global setClearBit:&_ch1_16ToMainCtrl bitValue:btnChannelToMainOnOff.selected bitOrder:13];
            [viewController sendData:CMD_CH_TO_MAIN_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16ToMainCtrl];
            break;
        case CH_15:
            [Global setClearBit:&_ch1_16ToMainCtrl bitValue:btnChannelToMainOnOff.selected bitOrder:14];
            [viewController sendData:CMD_CH_TO_MAIN_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16ToMainCtrl];
            break;
        case CH_16:
            [Global setClearBit:&_ch1_16ToMainCtrl bitValue:btnChannelToMainOnOff.selected bitOrder:15];
            [viewController sendData:CMD_CH_TO_MAIN_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_ch1_16ToMainCtrl];
            break;
        case CH_17:
            [Global setClearBit:&_ch17AudioSetting bitValue:btnChannelToMainOnOff.selected bitOrder:9];
            [viewController sendData:CMD_CH17_USB_AUDIO_SETTING commandType:DSP_WRITE dspId:DSP_5 value:_ch17AudioSetting];
            break;
        case CH_18:
            [Global setClearBit:&_ch18AudioSetting bitValue:btnChannelToMainOnOff.selected bitOrder:9];
            [viewController sendData:CMD_CH18_USB_AUDIO_SETTING commandType:DSP_WRITE dspId:DSP_5 value:_ch18AudioSetting];
            break;
        default:
            break;
    }
}

#pragma mark -
#pragma mark Multi bottom panel event handler methods

- (void)clearAllMultiSource {
    btnMultiAux1OnOff.selected = NO;
    btnMultiAux2OnOff.selected = NO;
    btnMultiAux3OnOff.selected = NO;
    btnMultiAux4OnOff.selected = NO;
    btnMultiGp1OnOff.selected = NO;
    btnMultiGp2OnOff.selected = NO;
    btnMultiGp3OnOff.selected = NO;
    btnMultiGp4OnOff.selected = NO;
}

- (IBAction)onBtnMultiAux1OnOff:(id)sender {
    int value = 0;
    if (btnMultiAux1OnOff.selected) {
        btnMultiAux1OnOff.selected = NO;
    } else {
        [self clearAllMultiSource];
        btnMultiAux1OnOff.selected = YES;
        value = 0x0100;
    }
    if (sender == nil) {
        return;
    }
    
    switch (currentChannel) {
        case MT_1:
            [viewController sendData:CMD_MULTI1_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:value];
            break;
        case MT_2:
            [viewController sendData:CMD_MULTI2_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:value];
            break;
        case MT_3:
            [viewController sendData:CMD_MULTI3_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:value];
            break;
        case MT_4:
            [viewController sendData:CMD_MULTI4_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:value];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnMultiAux2OnOff:(id)sender {
    int value = 0;
    if (btnMultiAux2OnOff.selected) {
        btnMultiAux2OnOff.selected = NO;
    } else {
        [self clearAllMultiSource];
        btnMultiAux2OnOff.selected = YES;
        value = 0x0200;
    }
    if (sender == nil) {
        return;
    }
    
    switch (currentChannel) {
        case MT_1:
            [viewController sendData:CMD_MULTI1_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:value];
            break;
        case MT_2:
            [viewController sendData:CMD_MULTI2_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:value];
            break;
        case MT_3:
            [viewController sendData:CMD_MULTI3_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:value];
            break;
        case MT_4:
            [viewController sendData:CMD_MULTI4_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:value];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnMultiAux3OnOff:(id)sender {
    int value = 0;
    if (btnMultiAux3OnOff.selected) {
        btnMultiAux3OnOff.selected = NO;
    } else {
        [self clearAllMultiSource];
        btnMultiAux3OnOff.selected = YES;
        value = 0x0400;
    }
    if (sender == nil) {
        return;
    }
    
    switch (currentChannel) {
        case MT_1:
            [viewController sendData:CMD_MULTI1_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:value];
            break;
        case MT_2:
            [viewController sendData:CMD_MULTI2_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:value];
            break;
        case MT_3:
            [viewController sendData:CMD_MULTI3_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:value];
            break;
        case MT_4:
            [viewController sendData:CMD_MULTI4_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:value];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnMultiAux4OnOff:(id)sender {
    int value = 0;
    if (btnMultiAux4OnOff.selected) {
        btnMultiAux4OnOff.selected = NO;
    } else {
        [self clearAllMultiSource];
        btnMultiAux4OnOff.selected = YES;
        value = 0x0800;
    }
    if (sender == nil) {
        return;
    }
    
    switch (currentChannel) {
        case MT_1:
            [viewController sendData:CMD_MULTI1_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:value];
            break;
        case MT_2:
            [viewController sendData:CMD_MULTI2_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:value];
            break;
        case MT_3:
            [viewController sendData:CMD_MULTI3_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:value];
            break;
        case MT_4:
            [viewController sendData:CMD_MULTI4_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:value];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnMultiGp1OnOff:(id)sender {
    int value = 0;
    if (btnMultiGp1OnOff.selected) {
        btnMultiGp1OnOff.selected = NO;
    } else {
        [self clearAllMultiSource];
        btnMultiGp1OnOff.selected = YES;
        value = 0x01;
    }
    if (sender == nil) {
        return;
    }
    
    switch (currentChannel) {
        case MT_1:
            [viewController sendData:CMD_MULTI1_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:value];
            break;
        case MT_2:
            [viewController sendData:CMD_MULTI2_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:value];
            break;
        case MT_3:
            [viewController sendData:CMD_MULTI3_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:value];
            break;
        case MT_4:
            [viewController sendData:CMD_MULTI4_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:value];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnMultiGp2OnOff:(id)sender {
    int value = 0;
    if (btnMultiGp2OnOff.selected) {
        btnMultiGp2OnOff.selected = NO;
    } else {
        [self clearAllMultiSource];
        btnMultiGp2OnOff.selected = YES;
        value = 0x02;
    }
    if (sender == nil) {
        return;
    }
    
    switch (currentChannel) {
        case MT_1:
            [viewController sendData:CMD_MULTI1_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:value];
            break;
        case MT_2:
            [viewController sendData:CMD_MULTI2_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:value];
            break;
        case MT_3:
            [viewController sendData:CMD_MULTI3_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:value];
            break;
        case MT_4:
            [viewController sendData:CMD_MULTI4_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:value];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnMultiGp3OnOff:(id)sender {
    int value = 0;
    if (btnMultiGp3OnOff.selected) {
        btnMultiGp3OnOff.selected = NO;
    } else {
        [self clearAllMultiSource];
        btnMultiGp3OnOff.selected = YES;
        value = 0x04;
    }
    if (sender == nil) {
        return;
    }
    
    switch (currentChannel) {
        case MT_1:
            [viewController sendData:CMD_MULTI1_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:value];
            break;
        case MT_2:
            [viewController sendData:CMD_MULTI2_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:value];
            break;
        case MT_3:
            [viewController sendData:CMD_MULTI3_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:value];
            break;
        case MT_4:
            [viewController sendData:CMD_MULTI4_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:value];
            break;
        default:
            break;
    }
}

- (IBAction)onBtnMultiGp4OnOff:(id)sender {
    int value = 0;
    if (btnMultiGp4OnOff.selected) {
        btnMultiGp4OnOff.selected = NO;
    } else {
        [self clearAllMultiSource];
        btnMultiGp4OnOff.selected = YES;
        value = 0x08;
    }
    if (sender == nil) {
        return;
    }
    
    switch (currentChannel) {
        case MT_1:
            [viewController sendData:CMD_MULTI1_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:value];
            break;
        case MT_2:
            [viewController sendData:CMD_MULTI2_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:value];
            break;
        case MT_3:
            [viewController sendData:CMD_MULTI3_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:value];
            break;
        case MT_4:
            [viewController sendData:CMD_MULTI4_SOURCE commandType:DSP_WRITE dspId:DSP_5 value:value];
            break;
        default:
            break;
    }
}

#pragma mark -
#pragma mark Main bottom panel event handler methods

- (IBAction)onBtnMainCh1OnOff:(id)sender {
    btnMainCh1OnOff.selected = !btnMainCh1OnOff.selected;
    if (sender != nil) {
        [Global setClearBit:&_chToMainCtrl bitValue:btnMainCh1OnOff.selected bitOrder:0];
        [viewController sendData:CMD_CH_TO_MAIN_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_chToMainCtrl];
    }
}

- (IBAction)onBtnMainCh2OnOff:(id)sender {
    btnMainCh2OnOff.selected = !btnMainCh2OnOff.selected;
    if (sender != nil) {
        [Global setClearBit:&_chToMainCtrl bitValue:btnMainCh2OnOff.selected bitOrder:1];
        [viewController sendData:CMD_CH_TO_MAIN_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_chToMainCtrl];
    }
}

- (IBAction)onBtnMainCh3OnOff:(id)sender {
    btnMainCh3OnOff.selected = !btnMainCh3OnOff.selected;
    if (sender != nil) {
        [Global setClearBit:&_chToMainCtrl bitValue:btnMainCh3OnOff.selected bitOrder:2];
        [viewController sendData:CMD_CH_TO_MAIN_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_chToMainCtrl];
    }
}

- (IBAction)onBtnMainCh4OnOff:(id)sender {
    btnMainCh4OnOff.selected = !btnMainCh4OnOff.selected;
    if (sender != nil) {
        [Global setClearBit:&_chToMainCtrl bitValue:btnMainCh4OnOff.selected bitOrder:3];
        [viewController sendData:CMD_CH_TO_MAIN_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_chToMainCtrl];
    }
}

- (IBAction)onBtnMainCh5OnOff:(id)sender {
    btnMainCh5OnOff.selected = !btnMainCh5OnOff.selected;
    if (sender != nil) {
        [Global setClearBit:&_chToMainCtrl bitValue:btnMainCh5OnOff.selected bitOrder:4];
        [viewController sendData:CMD_CH_TO_MAIN_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_chToMainCtrl];
    }
}

- (IBAction)onBtnMainCh6OnOff:(id)sender {
    btnMainCh6OnOff.selected = !btnMainCh6OnOff.selected;
    if (sender != nil) {
        [Global setClearBit:&_chToMainCtrl bitValue:btnMainCh6OnOff.selected bitOrder:5];
        [viewController sendData:CMD_CH_TO_MAIN_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_chToMainCtrl];
    }
}

- (IBAction)onBtnMainCh7OnOff:(id)sender {
    btnMainCh7OnOff.selected = !btnMainCh7OnOff.selected;
    if (sender != nil) {
        [Global setClearBit:&_chToMainCtrl bitValue:btnMainCh7OnOff.selected bitOrder:6];
        [viewController sendData:CMD_CH_TO_MAIN_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_chToMainCtrl];
    }
}

- (IBAction)onBtnMainCh8OnOff:(id)sender {
    btnMainCh8OnOff.selected = !btnMainCh8OnOff.selected;
    if (sender != nil) {
        [Global setClearBit:&_chToMainCtrl bitValue:btnMainCh8OnOff.selected bitOrder:7];
        [viewController sendData:CMD_CH_TO_MAIN_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_chToMainCtrl];
    }
}

- (IBAction)onBtnMainCh9OnOff:(id)sender {
    btnMainCh9OnOff.selected = !btnMainCh9OnOff.selected;
    if (sender != nil) {
        [Global setClearBit:&_chToMainCtrl bitValue:btnMainCh9OnOff.selected bitOrder:8];
        [viewController sendData:CMD_CH_TO_MAIN_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_chToMainCtrl];
    }
}

- (IBAction)onBtnMainCh10OnOff:(id)sender {
    btnMainCh10OnOff.selected = !btnMainCh10OnOff.selected;
    if (sender != nil) {
        [Global setClearBit:&_chToMainCtrl bitValue:btnMainCh10OnOff.selected bitOrder:9];
        [viewController sendData:CMD_CH_TO_MAIN_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_chToMainCtrl];
    }
}

- (IBAction)onBtnMainCh11OnOff:(id)sender {
    btnMainCh11OnOff.selected = !btnMainCh11OnOff.selected;
    if (sender != nil) {
        [Global setClearBit:&_chToMainCtrl bitValue:btnMainCh11OnOff.selected bitOrder:10];
        [viewController sendData:CMD_CH_TO_MAIN_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_chToMainCtrl];
    }
}

- (IBAction)onBtnMainCh12OnOff:(id)sender {
    btnMainCh12OnOff.selected = !btnMainCh12OnOff.selected;
    if (sender != nil) {
        [Global setClearBit:&_chToMainCtrl bitValue:btnMainCh12OnOff.selected bitOrder:11];
        [viewController sendData:CMD_CH_TO_MAIN_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_chToMainCtrl];
    }
}

- (IBAction)onBtnMainCh13OnOff:(id)sender {
    btnMainCh13OnOff.selected = !btnMainCh13OnOff.selected;
    if (sender != nil) {
        [Global setClearBit:&_chToMainCtrl bitValue:btnMainCh13OnOff.selected bitOrder:12];
        [viewController sendData:CMD_CH_TO_MAIN_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_chToMainCtrl];
    }
}

- (IBAction)onBtnMainCh14OnOff:(id)sender {
    btnMainCh14OnOff.selected = !btnMainCh14OnOff.selected;
    if (sender != nil) {
        [Global setClearBit:&_chToMainCtrl bitValue:btnMainCh14OnOff.selected bitOrder:13];
        [viewController sendData:CMD_CH_TO_MAIN_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_chToMainCtrl];
    }
}

- (IBAction)onBtnMainCh15OnOff:(id)sender {
    btnMainCh15OnOff.selected = !btnMainCh15OnOff.selected;
    if (sender != nil) {
        [Global setClearBit:&_chToMainCtrl bitValue:btnMainCh15OnOff.selected bitOrder:14];
        [viewController sendData:CMD_CH_TO_MAIN_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_chToMainCtrl];
    }
}

- (IBAction)onBtnMainCh16OnOff:(id)sender {
    btnMainCh16OnOff.selected = !btnMainCh16OnOff.selected;
    if (sender != nil) {
        [Global setClearBit:&_chToMainCtrl bitValue:btnMainCh16OnOff.selected bitOrder:15];
        [viewController sendData:CMD_CH_TO_MAIN_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_chToMainCtrl];
    }
}

- (IBAction)onBtnMainCh17OnOff:(id)sender {
    btnMainCh17OnOff.selected = !btnMainCh17OnOff.selected;
    if (sender != nil) {
        [Global setClearBit:&_ch17AudioSetting bitValue:btnMainCh17OnOff.selected bitOrder:9];
        [viewController sendData:CMD_CH17_USB_AUDIO_SETTING commandType:DSP_WRITE dspId:DSP_5 value:_ch17AudioSetting];
    }
}

- (IBAction)onBtnMainCh18OnOff:(id)sender {
    btnMainCh18OnOff.selected = !btnMainCh18OnOff.selected;
    if (sender != nil) {
        [Global setClearBit:&_ch18AudioSetting bitValue:btnMainCh18OnOff.selected bitOrder:9];
        [viewController sendData:CMD_CH18_USB_AUDIO_SETTING commandType:DSP_WRITE dspId:DSP_5 value:_ch18AudioSetting];
    }
}

- (IBAction)onBtnMainGp1OnOff:(id)sender {
    btnMainGp1OnOff.selected = !btnMainGp1OnOff.selected;
    if (sender != nil) {
        [Global setClearBit:&_gpToMainCtrl bitValue:btnMainGp1OnOff.selected bitOrder:0];
        [viewController sendData:CMD_GP_TO_MAIN_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_gpToMainCtrl];
    }
}

- (IBAction)onBtnMainGp2OnOff:(id)sender {
    btnMainGp2OnOff.selected = !btnMainGp2OnOff.selected;
    if (sender != nil) {
        [Global setClearBit:&_gpToMainCtrl bitValue:btnMainGp2OnOff.selected bitOrder:1];
        [viewController sendData:CMD_GP_TO_MAIN_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_gpToMainCtrl];
    }
}

- (IBAction)onBtnMainGp3OnOff:(id)sender {
    btnMainGp3OnOff.selected = !btnMainGp3OnOff.selected;
    if (sender != nil) {
        [Global setClearBit:&_gpToMainCtrl bitValue:btnMainGp3OnOff.selected bitOrder:2];
        [viewController sendData:CMD_GP_TO_MAIN_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_gpToMainCtrl];
    }
}

- (IBAction)onBtnMainGp4OnOff:(id)sender {
    btnMainGp4OnOff.selected = !btnMainGp4OnOff.selected;
    if (sender != nil) {
        [Global setClearBit:&_gpToMainCtrl bitValue:btnMainGp4OnOff.selected bitOrder:3];
        [viewController sendData:CMD_GP_TO_MAIN_CTRL commandType:DSP_WRITE dspId:DSP_5 value:_gpToMainCtrl];
    }
}

#pragma mark -
#pragma mark PAN slider event handler methods

- (IBAction)onSliderPanAction:(id) sender {
    sliderPan.value = floorf(sliderPan.value);
    if (sliderPan.value >= 0.0 && sliderPan.value < PAN_CENTER_VALUE) {
        [meterPan setHidden:NO];
        [meterPan setTransform:CGAffineTransformMake(-1, 0, 0, 1, 0, 0)];
        [meterPan setProgress:1.0 - sliderPan.value/PAN_MAX_VALUE];
    } else if (sliderPan.value > PAN_CENTER_VALUE && sliderPan.value <= PAN_MAX_VALUE) {
        [meterPan setHidden:NO];
        [meterPan setTransform:CGAffineTransformMake(1, 0, 0, 1, 0, 0)];
        [meterPan setProgress:sliderPan.value/PAN_MAX_VALUE];
    } else {
        [meterPan setHidden:YES];
        //[btnSliderPanEq1 setBackgroundImage:[UIImage imageNamed:@"pan-2.png"] forState:UIControlStateNormal];
    }
    [lblPan setText:[Global getPanString:sliderPan.value]];
    
    if (sender != nil) {
        switch (currentChannel) {
            case CH_1:
                [viewController sendData:CMD_CH1_PAN_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderPan.value-PAN_CENTER_VALUE];
                break;
            case CH_2:
                [viewController sendData:CMD_CH2_PAN_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderPan.value-PAN_CENTER_VALUE];
                break;
            case CH_3:
                [viewController sendData:CMD_CH3_PAN_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderPan.value-PAN_CENTER_VALUE];
                break;
            case CH_4:
                [viewController sendData:CMD_CH4_PAN_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderPan.value-PAN_CENTER_VALUE];
                break;
            case CH_5:
                [viewController sendData:CMD_CH5_PAN_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderPan.value-PAN_CENTER_VALUE];
                break;
            case CH_6:
                [viewController sendData:CMD_CH6_PAN_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderPan.value-PAN_CENTER_VALUE];
                break;
            case CH_7:
                [viewController sendData:CMD_CH7_PAN_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderPan.value-PAN_CENTER_VALUE];
                break;
            case CH_8:
                [viewController sendData:CMD_CH8_PAN_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderPan.value-PAN_CENTER_VALUE];
                break;
            case CH_9:
                [viewController sendData:CMD_CH9_PAN_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderPan.value-PAN_CENTER_VALUE];
                break;
            case CH_10:
                [viewController sendData:CMD_CH10_PAN_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderPan.value-PAN_CENTER_VALUE];
                break;
            case CH_11:
                [viewController sendData:CMD_CH11_PAN_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderPan.value-PAN_CENTER_VALUE];
                break;
            case CH_12:
                [viewController sendData:CMD_CH12_PAN_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderPan.value-PAN_CENTER_VALUE];
                break;
            case CH_13:
                [viewController sendData:CMD_CH13_PAN_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderPan.value-PAN_CENTER_VALUE];
                break;
            case CH_14:
                [viewController sendData:CMD_CH14_PAN_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderPan.value-PAN_CENTER_VALUE];
                break;
            case CH_15:
                [viewController sendData:CMD_CH15_PAN_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderPan.value-PAN_CENTER_VALUE];
                break;
            case CH_16:
                [viewController sendData:CMD_CH16_PAN_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderPan.value-PAN_CENTER_VALUE];
                break;
            case CH_17:
                [viewController sendData:CMD_CH17_PAN_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderPan.value-PAN_CENTER_VALUE];
                break;
            case CH_18:
                [viewController sendData:CMD_CH18_PAN_LEVEL commandType:DSP_WRITE dspId:DSP_5 value:sliderPan.value-PAN_CENTER_VALUE];
                break;
            default:
                break;
        }
    }
}

- (IBAction)onSliderPanStart:(id) sender {
    [sliderPan setThumbImage:[UIImage imageNamed:@"Slider-2.png"] forState:UIControlStateNormal];
}

- (IBAction)onSliderPanEnd:(id) sender {
    [sliderPan setThumbImage:[UIImage imageNamed:@"Slider-1.png"] forState:UIControlStateNormal];
}

#pragma mark -
#pragma mark Order event handler methods

- (IBAction)onBtnOrderEqDynDelay:(id)sender {
    _orderLock = YES;
    [viewOrder setHidden:YES];
    [self refreshOrderView:ORDER_CH_EQ_DYN_DELAY sendValue:YES];
    _orderLock = NO;
}

- (IBAction)onBtnOrderEqDelayDyn:(id)sender {
    _orderLock = YES;
    [viewOrder setHidden:YES];
    [self refreshOrderView:ORDER_CH_EQ_DELAY_DYN sendValue:YES];
    _orderLock = NO;
}

- (IBAction)onBtnOrderDynEqDelay:(id)sender {
    _orderLock = YES;
    [viewOrder setHidden:YES];
    [self refreshOrderView:ORDER_CH_DYN_EQ_DELAY sendValue:YES];
    _orderLock = NO;
}

- (IBAction)onBtnOrderDynDelayEq:(id)sender {
    _orderLock = YES;
    [viewOrder setHidden:YES];
    [self refreshOrderView:ORDER_CH_DYN_DELAY_EQ sendValue:YES];
    _orderLock = NO;
}

- (IBAction)onBtnOrderDelayEqDyn:(id)sender {
    _orderLock = YES;
    [viewOrder setHidden:YES];
    [self refreshOrderView:ORDER_CH_DELAY_EQ_DYN sendValue:YES];
    _orderLock = NO;
}

- (IBAction)onBtnOrderDelayDynEq:(id)sender {
    _orderLock = YES;
    [viewOrder setHidden:YES];
    [self refreshOrderView:ORDER_CH_DELAY_DYN_EQ sendValue:YES];
    _orderLock = NO;
}

- (IBAction)onBtnOrderMainEqDynDelay:(id)sender {
    _orderLock = YES;
    [viewOrderMain setHidden:YES];
    [self refreshOrderMainView:ORDER_MAIN_EQ_DYN_DELAY sendValue:YES];
    [imgBackground setImage:[UIImage imageNamed:@"basemap-47.png"]];
    _orderLock = NO;
}

- (IBAction)onBtnOrderMainDynEqDelay:(id)sender {
    _orderLock = YES;
    [viewOrderMain setHidden:YES];
    [self refreshOrderMainView:ORDER_MAIN_DYN_EQ_DELAY sendValue:YES];
    [imgBackground setImage:[UIImage imageNamed:@"basemap-47.png"]];
    _orderLock = NO;
}

- (IBAction)onBtnOrderMainGeqDynDelay:(id)sender {
    _orderLock = YES;
    [viewOrderMain setHidden:YES];
    [self refreshOrderMainView:ORDER_MAIN_GEQ_DYN_DELAY sendValue:YES];
    [imgBackground setImage:[UIImage imageNamed:@"basemap-49.png"]];
    _orderLock = NO;
}

- (IBAction)onBtnOrderMainDynGeqDelay:(id)sender {
    _orderLock = YES;
    [viewOrderMain setHidden:YES];
    [self refreshOrderMainView:ORDER_MAIN_DYN_GEQ_DELAY sendValue:YES];
    [imgBackground setImage:[UIImage imageNamed:@"basemap-49.png"]];
    _orderLock = NO;
}

- (IBAction)onBtnOrderMultiEqDynDelay:(id)sender {
    _orderLock = YES;
    [viewOrderMulti setHidden:YES];
    [self refreshOrderMultiView:ORDER_MT_EQ_DYN_DELAY sendValue:YES];
    _orderLock = NO;
}

- (IBAction)onBtnOrderMultiDynEqDelay:(id)sender {
    _orderLock = YES;
    [viewOrderMulti setHidden:YES];
    [self refreshOrderMultiView:ORDER_MT_DYN_EQ_DELAY sendValue:YES];
    _orderLock = NO;
}

#pragma mark -
#pragma mark Callback from GeqPeqViewController

- (void)refreshEqPreview
{
    GeqPeqViewController *peqController = viewController.geqPeqController;
    if (peqController == nil) {
        return;
    }
    
    UIImage *image = [peqController.eqPreviewImages objectAtIndex:currentChannel];
    if (image != nil) {
        [imgEqPreview setImage:image];
    }
}

#pragma mark -
#pragma mark Callback from DynViewController

- (void)refreshDynPreview
{
    DynViewController *dynController = viewController.dynController;
    if (dynController == nil) {
        return;
    }
    
    UIImage *image = [dynController.dynPreviewImages objectAtIndex:currentChannel];
    if (image != nil) {
        [imgDynPreview setImage:image];
    }
}

#pragma mark -
#pragma mark Text label helper methods

- (void)updatecurrentChannelLabel
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    
    switch (currentChannelLabel) {
        case CHANNEL_CH_1:
            if ([userDefaults objectForKey:@"Ch1"] != nil) {
                [txtCurrentChannel setText:(NSString *)[userDefaults objectForKey:@"Ch1"]];
            } else {
                [txtCurrentChannel setText:NSLocalizedStringFromTable(@"Ch1", @"Acapela", nil)];
            }
            [lblCurrentChannel setText:NSLocalizedStringFromTable(@"Ch1", @"Acapela", nil)];
            break;
        case CHANNEL_CH_2:
            if ([userDefaults objectForKey:@"Ch2"] != nil) {
                [txtCurrentChannel setText:(NSString *)[userDefaults objectForKey:@"Ch2"]];
            } else {
                [txtCurrentChannel setText:NSLocalizedStringFromTable(@"Ch2", @"Acapela", nil)];
            }
            [lblCurrentChannel setText:NSLocalizedStringFromTable(@"Ch2", @"Acapela", nil)];
            break;
        case CHANNEL_CH_3:
            if ([userDefaults objectForKey:@"Ch3"] != nil) {
                [txtCurrentChannel setText:(NSString *)[userDefaults objectForKey:@"Ch3"]];
            } else {
                [txtCurrentChannel setText:NSLocalizedStringFromTable(@"Ch3", @"Acapela", nil)];
            }
            [lblCurrentChannel setText:NSLocalizedStringFromTable(@"Ch3", @"Acapela", nil)];
            break;
        case CHANNEL_CH_4:
            if ([userDefaults objectForKey:@"Ch4"] != nil) {
                [txtCurrentChannel setText:(NSString *)[userDefaults objectForKey:@"Ch4"]];
            } else {
                [txtCurrentChannel setText:NSLocalizedStringFromTable(@"Ch4", @"Acapela", nil)];
            }
            [lblCurrentChannel setText:NSLocalizedStringFromTable(@"Ch4", @"Acapela", nil)];
            break;
        case CHANNEL_CH_5:
            if ([userDefaults objectForKey:@"Ch5"] != nil) {
                [txtCurrentChannel setText:(NSString *)[userDefaults objectForKey:@"Ch5"]];
            } else {
                [txtCurrentChannel setText:NSLocalizedStringFromTable(@"Ch5", @"Acapela", nil)];
            }
            [lblCurrentChannel setText:NSLocalizedStringFromTable(@"Ch5", @"Acapela", nil)];
            break;
        case CHANNEL_CH_6:
            if ([userDefaults objectForKey:@"Ch6"] != nil) {
                [txtCurrentChannel setText:(NSString *)[userDefaults objectForKey:@"Ch6"]];
            } else {
                [txtCurrentChannel setText:NSLocalizedStringFromTable(@"Ch6", @"Acapela", nil)];
            }
            [lblCurrentChannel setText:NSLocalizedStringFromTable(@"Ch6", @"Acapela", nil)];
            break;
        case CHANNEL_CH_7:
            if ([userDefaults objectForKey:@"Ch7"] != nil) {
                [txtCurrentChannel setText:(NSString *)[userDefaults objectForKey:@"Ch7"]];
            } else {
                [txtCurrentChannel setText:NSLocalizedStringFromTable(@"Ch7", @"Acapela", nil)];
            }
            [lblCurrentChannel setText:NSLocalizedStringFromTable(@"Ch7", @"Acapela", nil)];
            break;
        case CHANNEL_CH_8:
            if ([userDefaults objectForKey:@"Ch8"] != nil) {
                [txtCurrentChannel setText:(NSString *)[userDefaults objectForKey:@"Ch8"]];
            } else {
                [txtCurrentChannel setText:NSLocalizedStringFromTable(@"Ch8", @"Acapela", nil)];
            }
            [lblCurrentChannel setText:NSLocalizedStringFromTable(@"Ch8", @"Acapela", nil)];
            break;
        case CHANNEL_CH_9:
            if ([userDefaults objectForKey:@"Ch9"] != nil) {
                [txtCurrentChannel setText:(NSString *)[userDefaults objectForKey:@"Ch9"]];
            } else {
                [txtCurrentChannel setText:NSLocalizedStringFromTable(@"Ch9", @"Acapela", nil)];
            }
            [lblCurrentChannel setText:NSLocalizedStringFromTable(@"Ch9", @"Acapela", nil)];
            break;
        case CHANNEL_CH_10:
            if ([userDefaults objectForKey:@"Ch10"] != nil) {
                [txtCurrentChannel setText:(NSString *)[userDefaults objectForKey:@"Ch10"]];
            } else {
                [txtCurrentChannel setText:NSLocalizedStringFromTable(@"Ch10", @"Acapela", nil)];
            }
            [lblCurrentChannel setText:NSLocalizedStringFromTable(@"Ch10", @"Acapela", nil)];
            break;
        case CHANNEL_CH_11:
            if ([userDefaults objectForKey:@"Ch11"] != nil) {
                [txtCurrentChannel setText:(NSString *)[userDefaults objectForKey:@"Ch11"]];
            } else {
                [txtCurrentChannel setText:NSLocalizedStringFromTable(@"Ch11", @"Acapela", nil)];
            }
            [lblCurrentChannel setText:NSLocalizedStringFromTable(@"Ch11", @"Acapela", nil)];
            break;
        case CHANNEL_CH_12:
            if ([userDefaults objectForKey:@"Ch12"] != nil) {
                [txtCurrentChannel setText:(NSString *)[userDefaults objectForKey:@"Ch12"]];
            } else {
                [txtCurrentChannel setText:NSLocalizedStringFromTable(@"Ch12", @"Acapela", nil)];
            }
            [lblCurrentChannel setText:NSLocalizedStringFromTable(@"Ch12", @"Acapela", nil)];
            break;
        case CHANNEL_CH_13:
            if ([userDefaults objectForKey:@"Ch13"] != nil) {
                [txtCurrentChannel setText:(NSString *)[userDefaults objectForKey:@"Ch13"]];
            } else {
                [txtCurrentChannel setText:NSLocalizedStringFromTable(@"Ch13", @"Acapela", nil)];
            }
            [lblCurrentChannel setText:NSLocalizedStringFromTable(@"Ch13", @"Acapela", nil)];
            break;
        case CHANNEL_CH_14:
            if ([userDefaults objectForKey:@"Ch14"] != nil) {
                [txtCurrentChannel setText:(NSString *)[userDefaults objectForKey:@"Ch14"]];
            } else {
                [txtCurrentChannel setText:NSLocalizedStringFromTable(@"Ch14", @"Acapela", nil)];
            }
            [lblCurrentChannel setText:NSLocalizedStringFromTable(@"Ch14", @"Acapela", nil)];
            break;
        case CHANNEL_CH_15:
            if ([userDefaults objectForKey:@"Ch15"] != nil) {
                [txtCurrentChannel setText:(NSString *)[userDefaults objectForKey:@"Ch15"]];
            } else {
                [txtCurrentChannel setText:NSLocalizedStringFromTable(@"Ch15", @"Acapela", nil)];
            }
            [lblCurrentChannel setText:NSLocalizedStringFromTable(@"Ch15", @"Acapela", nil)];
            break;
        case CHANNEL_CH_16:
            if ([userDefaults objectForKey:@"Ch16"] != nil) {
                [txtCurrentChannel setText:(NSString *)[userDefaults objectForKey:@"Ch16"]];
            } else {
                [txtCurrentChannel setText:NSLocalizedStringFromTable(@"Ch16", @"Acapela", nil)];
            }
            [lblCurrentChannel setText:NSLocalizedStringFromTable(@"Ch16", @"Acapela", nil)];
            break;
        case CHANNEL_CH_17:
            if ([userDefaults objectForKey:@"Ch17"] != nil) {
                [txtCurrentChannel setText:(NSString *)[userDefaults objectForKey:@"Ch17"]];
            } else {
                [txtCurrentChannel setText:NSLocalizedStringFromTable(@"Ch17", @"Acapela", nil)];
            }
            [lblCurrentChannel setText:NSLocalizedStringFromTable(@"Ch17", @"Acapela", nil)];
            break;
        case CHANNEL_CH_18:
            if ([userDefaults objectForKey:@"Ch18"] != nil) {
                [txtCurrentChannel setText:(NSString *)[userDefaults objectForKey:@"Ch18"]];
            } else {
                [txtCurrentChannel setText:NSLocalizedStringFromTable(@"Ch18", @"Acapela", nil)];
            }
            [lblCurrentChannel setText:NSLocalizedStringFromTable(@"Ch18", @"Acapela", nil)];
            break;
        case CHANNEL_MULTI_1:
            if ([userDefaults objectForKey:@"Multi1"] != nil) {
                [txtCurrentChannel setText:(NSString *)[userDefaults objectForKey:@"Multi1"]];
            } else {
                [txtCurrentChannel setText:NSLocalizedStringFromTable(@"Multi1", @"Acapela", nil)];
            }
            [lblCurrentChannel setText:NSLocalizedStringFromTable(@"Multi1", @"Acapela", nil)];
            break;
        case CHANNEL_MULTI_2:
            if ([userDefaults objectForKey:@"Multi2"] != nil) {
                [txtCurrentChannel setText:(NSString *)[userDefaults objectForKey:@"Multi2"]];
            } else {
                [txtCurrentChannel setText:NSLocalizedStringFromTable(@"Multi2", @"Acapela", nil)];
            }
            [lblCurrentChannel setText:NSLocalizedStringFromTable(@"Multi2", @"Acapela", nil)];
            break;
        case CHANNEL_MULTI_3:
            if ([userDefaults objectForKey:@"Multi3"] != nil) {
                [txtCurrentChannel setText:(NSString *)[userDefaults objectForKey:@"Multi3"]];
            } else {
                [txtCurrentChannel setText:NSLocalizedStringFromTable(@"Multi3", @"Acapela", nil)];
            }
            [lblCurrentChannel setText:NSLocalizedStringFromTable(@"Multi3", @"Acapela", nil)];
            break;
        case CHANNEL_MULTI_4:
            if ([userDefaults objectForKey:@"Multi4"] != nil) {
                [txtCurrentChannel setText:(NSString *)[userDefaults objectForKey:@"Multi4"]];
            } else {
                [txtCurrentChannel setText:NSLocalizedStringFromTable(@"Multi4", @"Acapela", nil)];
            }
            [lblCurrentChannel setText:NSLocalizedStringFromTable(@"Multi4", @"Acapela", nil)];
            break;
        case CHANNEL_MAIN:
            if ([userDefaults objectForKey:@"Main"] != nil) {
                [txtCurrentChannel setText:(NSString *)[userDefaults objectForKey:@"Main"]];
            } else {
                [txtCurrentChannel setText:NSLocalizedStringFromTable(@"Main", @"Acapela", nil)];
            }
            [lblCurrentChannel setText:NSLocalizedStringFromTable(@"Main", @"Acapela", nil)];
            break;
        default:
            break;
    }
}

#pragma mark -
#pragma mark Callback from GeqPeqViewController

- (void)didFinishEditing {
    GeqPeqViewController *peqController = viewController.geqPeqController;
    if (peqController == nil) {
        return;
    }
    
    UIImage *image = [peqController.eqPreviewImages objectAtIndex:currentChannel];
    if (image != nil) {
        [imgEqPreview setImage:image];
    }
}

#pragma mark -
#pragma mark Callback from DynViewController

- (void)didFinishEditingDyn {
    DynViewController *dynController = viewController.dynController;
    if (dynController == nil) {
        return;
    }
    
    UIImage *image = [dynController.dynPreviewImages objectAtIndex:currentChannel];
    if (image != nil) {
        [imgDynPreview setImage:image];
    }
}

@end
