//
//  DynViewController.h
//  Acapela
//
//  Created by Kevin Phua on 13/10/7.
//  Copyright (c) 2013年 Kevin Phua. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MHRotaryKnob.h"
#import "DACircularProgressView.h"
#import "CorePlot-CocoaTouch.h"
#import "acapela.h"
#import "JEProgressView.h"

@class DynViewController;
@protocol DynPreviewDelegate <NSObject>
@required
- (void)didFinishEditingDyn;
@end

@interface DynViewController : UIViewController<CPTPlotDataSource, UITextFieldDelegate>
{
    // Label edit
    ChannelType currentChannel;
    ChannelType currentChannelLabel;
}

@property (nonatomic) ChannelType currentChannel;
@property (nonatomic) ChannelType currentChannelLabel;

@property (weak, nonatomic) IBOutlet UIImageView *imgBackground;
@property (weak, nonatomic) IBOutlet UIImageView *imgPreview;

@property (strong, nonatomic) NSMutableArray *dynImages;
@property (strong, nonatomic) NSMutableArray *dynPreviewImages;

@property (weak, nonatomic) IBOutlet UIButton *btnDyn;
@property (weak, nonatomic) IBOutlet UIButton *btnFile;
@property (weak, nonatomic) IBOutlet UIButton *btnReset;
@property (weak, nonatomic) IBOutlet UIButton *btnDynGate;
@property (weak, nonatomic) IBOutlet UIButton *btnDynExpander;
@property (weak, nonatomic) IBOutlet UIButton *btnDynComp;
@property (weak, nonatomic) IBOutlet UIButton *btnDynLimiter;
@property (weak, nonatomic) IBOutlet UIButton *btnDynModeOnOff;

@property (nonatomic,retain) JEProgressView *meterDynOutL;
@property (nonatomic,retain) JEProgressView *meterDynOutR;
@property (nonatomic,retain) JEProgressView *meterDynGrL;
@property (nonatomic,retain) JEProgressView *meterDynGrR;

@property (strong, nonatomic) IBOutlet UIButton *btnSelectDown;
@property (strong, nonatomic) IBOutlet UIButton *btnSelectUp;
@property (strong, nonatomic) IBOutlet UIButton *btnSolo;
@property (strong, nonatomic) IBOutlet UIButton *btnOn;
@property (strong, nonatomic) IBOutlet UILabel *lblPage;
@property (weak, nonatomic) IBOutlet UILabel *lblSliderValue;
@property (nonatomic,retain) UISlider *slider;
@property (nonatomic,retain) JEProgressView *meterL;
@property (nonatomic,retain) JEProgressView *meterR;
@property (strong, nonatomic) IBOutlet UIButton *btnMeterPrePost;
@property (strong, nonatomic) IBOutlet UITextField *txtPage;

@property (weak, nonatomic) IBOutlet UIView *viewOutput;
@property (strong, nonatomic) IBOutlet UIButton *btnBallThreshold;
@property (strong, nonatomic) IBOutlet UIButton *btnBallRatio;

@property (weak, nonatomic) IBOutlet UIView *viewGate;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobGateThreshold;
@property (nonatomic, retain) DACircularProgressView *cpvGateThreshold;
@property (weak, nonatomic) IBOutlet UILabel *lblGateThreshold;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobGateRange;
@property (nonatomic, retain) DACircularProgressView *cpvGateRange;
@property (weak, nonatomic) IBOutlet UILabel *lblGateRange;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobGateAttack;
@property (nonatomic, retain) DACircularProgressView *cpvGateAttack;
@property (weak, nonatomic) IBOutlet UILabel *lblGateAttack;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobGateHold;
@property (nonatomic, retain) DACircularProgressView *cpvGateHold;
@property (weak, nonatomic) IBOutlet UILabel *lblGateHold;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobGateRelease;
@property (nonatomic, retain) DACircularProgressView *cpvGateRelease;
@property (weak, nonatomic) IBOutlet UILabel *lblGateRelease;

@property (weak, nonatomic) IBOutlet UIView *viewExpander;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobExpanderThreshold;
@property (nonatomic, retain) DACircularProgressView *cpvExpanderThreshold;
@property (weak, nonatomic) IBOutlet UILabel *lblExpanderThreshold;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobExpanderRatio;
@property (nonatomic, retain) DACircularProgressView *cpvExpanderRatio;
@property (weak, nonatomic) IBOutlet UILabel *lblExpanderRatio;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobExpanderAttack;
@property (nonatomic, retain) DACircularProgressView *cpvExpanderAttack;
@property (weak, nonatomic) IBOutlet UILabel *lblExpanderAttack;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobExpanderRelease;
@property (nonatomic, retain) DACircularProgressView *cpvExpanderRelease;
@property (weak, nonatomic) IBOutlet UILabel *lblExpanderRelease;

@property (weak, nonatomic) IBOutlet UIView *viewComp;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobCompThreshold;
@property (nonatomic, retain) DACircularProgressView *cpvCompThreshold;
@property (weak, nonatomic) IBOutlet UILabel *lblCompThreshold;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobCompRatio;
@property (nonatomic, retain) DACircularProgressView *cpvCompRatio;
@property (weak, nonatomic) IBOutlet UILabel *lblCompRatio;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobCompOutputGain;
@property (nonatomic, retain) DACircularProgressView *cpvCompOutputGain;
@property (weak, nonatomic) IBOutlet UILabel *lblCompOutputGain;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobCompAttack;
@property (nonatomic, retain) DACircularProgressView *cpvCompAttack;
@property (weak, nonatomic) IBOutlet UILabel *lblCompAttack;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobCompRelease;
@property (nonatomic, retain) DACircularProgressView *cpvCompRelease;
@property (weak, nonatomic) IBOutlet UILabel *lblCompRelease;

@property (weak, nonatomic) IBOutlet UIView *viewLimiter;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobLimiterThreshold;
@property (nonatomic, retain) DACircularProgressView *cpvLimiterThreshold;
@property (weak, nonatomic) IBOutlet UILabel *lblLimiterThreshold;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobLimiterAttack;
@property (nonatomic, retain) DACircularProgressView *cpvLimiterAttack;
@property (weak, nonatomic) IBOutlet UILabel *lblLimiterAttack;
@property (weak, nonatomic) IBOutlet MHRotaryKnob *knobLimiterRelease;
@property (nonatomic, retain) DACircularProgressView *cpvLimiterRelease;
@property (weak, nonatomic) IBOutlet UILabel *lblLimiterRelease;

// CorePlot scatter plot view
@property (nonatomic, strong) CPTGraphHostingView *hostView;

// Delegate callback to parent controller
@property (nonatomic, assign) id <DynPreviewDelegate> delegate;

- (IBAction)onBtnSelectDown:(id)sender;
- (IBAction)onBtnSelectUp:(id)sender;
- (IBAction)onBtnSolo:(id)sender;
- (IBAction)onBtnOn:(id)sender;
- (IBAction)onBtnMeterPrePost:(id)sender;

// Public methods
- (void)setDynChannel:(ChannelType)channel;
- (void)processReply:(DynPagePacket *)pkt;
- (void)processChannel1_8PageReply:(ChannelCh1_8PagePacket *)eqChannelPkt;
- (void)processChannel9_16PageReply:(ChannelCh9_16PagePacket *)eqChannelPkt;
- (void)processChannel17_18PageReply:(ChannelCh17_18PagePacket *)eqChannelPkt;
- (void)processMulti1_4PageReply:(ChannelMulti1_4PagePacket *)eqMultiPkt;
- (void)processChannelViewPage:(ViewPageChannelPacket *)pkt;
- (void)processUsbAudioViewPage:(ViewPageUsbAudioPacket *)pkt;
- (void)processMultiViewPage:(ViewPageMultiPacket *)pkt;
- (void)processMainViewPage:(ViewPageMainPacket *)pkt;
- (void)processChannelLabel:(AllMeterValueRxPacket *)pkt;
- (void)setSceneFilename:(NSString*)fileName;

@end
