//
//  LoginViewController.m
//  eLive
//
//  Created by Kevin on 12/12/13.
//  Copyright (c) 2012年 Kevin Phua. All rights reserved.
//

#import "LoginViewController.h"

@interface LoginViewController ()

@end

@implementation LoginViewController

@synthesize lblIpAddress, lblUserName, lblPassword;
@synthesize txtIpAddress, txtUserName, txtPassword;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    NSString *ipAddress = [[NSUserDefaults standardUserDefaults] objectForKey:@"DeviceIP"];
    if (ipAddress != nil) {
        [txtIpAddress setText:ipAddress];
    }
    NSString *username = [[NSUserDefaults standardUserDefaults] objectForKey:@"Username"];
    if (username != nil) {
        [txtUserName setText:username];
    }
    /*
    NSString *password = [[NSUserDefaults standardUserDefaults] objectForKey:@"Password"];
    if (password != nil) {
        [txtPassword setText:password];
    }
    */
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)onBtnConnect:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    
    NSDictionary* dict = [NSDictionary dictionaryWithObjects:
                          [NSArray arrayWithObjects:txtIpAddress.text, txtUserName.text, txtPassword.text, nil]
                            forKeys:[NSArray arrayWithObjects:@"ipAddress", @"username", @"password", nil]];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ConnectToDevice"
                                                        object:self
                                                      userInfo:dict];
}

- (IBAction)onBtnCancel:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ConnectToDevice"
                                                        object:self
                                                      userInfo:nil];
}

@end
