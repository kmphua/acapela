//
//  EffectGeq15ViewController
//  Acapela
//
//  Created by Kevin on 12/12/27.
//  Copyright (c) 2012年 Kevin Phua. All rights reserved.
//

#import "EffectGeq15ViewController.h"
#import "acapela.h"
#import "AppDelegate.h"
#import "ViewController.h"
#import "Global.h"
#import "JEProgressView.h"

#define GEQ_SLIDER_MIN_VALUE    -12.0
#define GEQ_SLIDER_MAX_VALUE    12.0
#define PEQ_FRAME_WIDTH         535.0
#define PEQ_FRAME_HEIGHT        227.0

@implementation EffectGeq15ViewController
{
    ViewController *viewController;
    BOOL geqLock, mainEqLock;
    NSTimer *channelScrollTimer;
    BOOL geqOnOff;
    BOOL geqLinkMode;
}

@synthesize btnGeqLink, btnGeqDraw, btnFile, viewGeqFrameL, viewGeqDrawL, viewGeqFrameR, viewGeqDrawR, imgEqL, imgEqR;
@synthesize sliderLGeq1, sliderLGeq2, sliderLGeq3, sliderLGeq4, sliderLGeq5;
@synthesize sliderLGeq6, sliderLGeq7, sliderLGeq8, sliderLGeq9, sliderLGeq10;
@synthesize sliderLGeq11, sliderLGeq12, sliderLGeq13, sliderLGeq14, sliderLGeq15;
@synthesize sliderRGeq1, sliderRGeq2, sliderRGeq3, sliderRGeq4, sliderRGeq5;
@synthesize sliderRGeq6, sliderRGeq7, sliderRGeq8, sliderRGeq9, sliderRGeq10;
@synthesize sliderRGeq11, sliderRGeq12, sliderRGeq13, sliderRGeq14, sliderRGeq15;
@synthesize meterL1Top, meterL1Btm, meterL2Top, meterL2Btm, meterL3Top, meterL3Btm;
@synthesize meterL4Top, meterL4Btm, meterL5Top, meterL5Btm, meterL6Top, meterL6Btm;
@synthesize meterL7Top, meterL7Btm, meterL8Top, meterL8Btm, meterL9Top, meterL9Btm;
@synthesize meterL10Top, meterL10Btm, meterL11Top, meterL11Btm, meterL12Top, meterL12Btm;
@synthesize meterL13Top, meterL13Btm, meterL14Top, meterL14Btm, meterL15Top, meterL15Btm;
@synthesize meterR1Top, meterR1Btm, meterR2Top, meterR2Btm, meterR3Top, meterR3Btm;
@synthesize meterR4Top, meterR4Btm, meterR5Top, meterR5Btm, meterR6Top, meterR6Btm;
@synthesize meterR7Top, meterR7Btm, meterR8Top, meterR8Btm, meterR9Top, meterR9Btm;
@synthesize meterR10Top, meterR10Btm, meterR11Top, meterR11Btm, meterR12Top, meterR12Btm;
@synthesize meterR13Top, meterR13Btm, meterR14Top, meterR14Btm, meterR15Top, meterR15Btm;

static BOOL initDone = NO;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    
    mainEqLock = NO;
    initDone = YES;
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    AppDelegate *appDelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    viewController = (ViewController *)appDelegate.window.rootViewController;
    
    // Do any additional setup after loading the view from its nib.

    // Add GEQ vertical sliders
    UIImage *sliderImage = [UIImage imageNamed:@"fader-9.png"];
    UIImage *progressImage = [[UIImage imageNamed:@"bar-4.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0,0,0,0)];
    
    sliderLGeq1 = [[UISlider alloc] initWithFrame:CGRectMake(-69, 100, 230, 12)];
    sliderLGeq1.transform = CGAffineTransformRotate(sliderLGeq1.transform, 270.0/180*M_PI);
    [sliderLGeq1 addTarget:self action:@selector(onSlider1Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderLGeq1 addTarget:self action:@selector(onSlider1End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderLGeq1 addTarget:self action:@selector(onSlider1End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderLGeq1 addTarget:self action:@selector(onSlider1Action:) forControlEvents:UIControlEventValueChanged];
    [sliderLGeq1 setBackgroundColor:[UIColor clearColor]];
    [sliderLGeq1 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq1 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq1 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderLGeq1.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderLGeq1.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderLGeq1.continuous = YES;
    sliderLGeq1.value = 0.0;
    [viewGeqFrameL addSubview:sliderLGeq1];
    
    sliderLGeq2 = [[UISlider alloc] initWithFrame:CGRectMake(-19, 100, 230, 12)];
    sliderLGeq2.transform = CGAffineTransformRotate(sliderLGeq2.transform, 270.0/180*M_PI);
    [sliderLGeq2 addTarget:self action:@selector(onSlider2Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderLGeq2 addTarget:self action:@selector(onSlider2End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderLGeq2 addTarget:self action:@selector(onSlider2End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderLGeq2 addTarget:self action:@selector(onSlider2Action:) forControlEvents:UIControlEventValueChanged];
    [sliderLGeq2 setBackgroundColor:[UIColor clearColor]];
    [sliderLGeq2 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq2 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq2 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderLGeq2.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderLGeq2.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderLGeq2.continuous = YES;
    sliderLGeq2.value = 0.0;
    [viewGeqFrameL addSubview:sliderLGeq2];
    
    sliderLGeq3 = [[UISlider alloc] initWithFrame:CGRectMake(31, 100, 230, 12)];
    sliderLGeq3.transform = CGAffineTransformRotate(sliderLGeq3.transform, 270.0/180*M_PI);
    [sliderLGeq3 addTarget:self action:@selector(onSlider3Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderLGeq3 addTarget:self action:@selector(onSlider3End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderLGeq3 addTarget:self action:@selector(onSlider3End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderLGeq3 addTarget:self action:@selector(onSlider3Action:) forControlEvents:UIControlEventValueChanged];
    [sliderLGeq3 setBackgroundColor:[UIColor clearColor]];
    [sliderLGeq3 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq3 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq3 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderLGeq3.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderLGeq3.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderLGeq3.continuous = YES;
    sliderLGeq3.value = 0.0;
    [viewGeqFrameL addSubview:sliderLGeq3];
    
    sliderLGeq4 = [[UISlider alloc] initWithFrame:CGRectMake(81, 100, 230, 12)];
    sliderLGeq4.transform = CGAffineTransformRotate(sliderLGeq4.transform, 270.0/180*M_PI);
    [sliderLGeq4 addTarget:self action:@selector(onSlider4Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderLGeq4 addTarget:self action:@selector(onSlider4End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderLGeq4 addTarget:self action:@selector(onSlider4End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderLGeq4 addTarget:self action:@selector(onSlider4Action:) forControlEvents:UIControlEventValueChanged];
    [sliderLGeq4 setBackgroundColor:[UIColor clearColor]];
    [sliderLGeq4 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq4 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq4 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderLGeq4.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderLGeq4.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderLGeq4.continuous = YES;
    sliderLGeq4.value = 0.0;
    [viewGeqFrameL addSubview:sliderLGeq4];
    
    sliderLGeq5 = [[UISlider alloc] initWithFrame:CGRectMake(131, 100, 230, 12)];
    sliderLGeq5.transform = CGAffineTransformRotate(sliderLGeq5.transform, 270.0/180*M_PI);
    [sliderLGeq5 addTarget:self action:@selector(onSlider5Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderLGeq5 addTarget:self action:@selector(onSlider5End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderLGeq5 addTarget:self action:@selector(onSlider5End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderLGeq5 addTarget:self action:@selector(onSlider5Action:) forControlEvents:UIControlEventValueChanged];
    [sliderLGeq5 setBackgroundColor:[UIColor clearColor]];
    [sliderLGeq5 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq5 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq5 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderLGeq5.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderLGeq5.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderLGeq5.continuous = YES;
    sliderLGeq5.value = 0.0;
    [viewGeqFrameL addSubview:sliderLGeq5];
    
    sliderLGeq6 = [[UISlider alloc] initWithFrame:CGRectMake(181, 100, 230, 12)];
    sliderLGeq6.transform = CGAffineTransformRotate(sliderLGeq6.transform, 270.0/180*M_PI);
    [sliderLGeq6 addTarget:self action:@selector(onSlider6Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderLGeq6 addTarget:self action:@selector(onSlider6End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderLGeq6 addTarget:self action:@selector(onSlider6End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderLGeq6 addTarget:self action:@selector(onSlider6Action:) forControlEvents:UIControlEventValueChanged];
    [sliderLGeq6 setBackgroundColor:[UIColor clearColor]];
    [sliderLGeq6 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq6 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq6 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderLGeq6.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderLGeq6.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderLGeq6.continuous = YES;
    sliderLGeq6.value = 0.0;
    [viewGeqFrameL addSubview:sliderLGeq6];
    
    sliderLGeq7 = [[UISlider alloc] initWithFrame:CGRectMake(231, 100, 230, 12)];
    sliderLGeq7.transform = CGAffineTransformRotate(sliderLGeq7.transform, 270.0/180*M_PI);
    [sliderLGeq7 addTarget:self action:@selector(onSlider7Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderLGeq7 addTarget:self action:@selector(onSlider7End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderLGeq7 addTarget:self action:@selector(onSlider7End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderLGeq7 addTarget:self action:@selector(onSlider7Action:) forControlEvents:UIControlEventValueChanged];
    [sliderLGeq7 setBackgroundColor:[UIColor clearColor]];
    [sliderLGeq7 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq7 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq7 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderLGeq7.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderLGeq7.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderLGeq7.continuous = YES;
    sliderLGeq7.value = 0.0;
    [viewGeqFrameL addSubview:sliderLGeq7];
    
    sliderLGeq8 = [[UISlider alloc] initWithFrame:CGRectMake(281, 100, 230, 12)];
    sliderLGeq8.transform = CGAffineTransformRotate(sliderLGeq8.transform, 270.0/180*M_PI);
    [sliderLGeq8 addTarget:self action:@selector(onSlider8Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderLGeq8 addTarget:self action:@selector(onSlider8End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderLGeq8 addTarget:self action:@selector(onSlider8End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderLGeq8 addTarget:self action:@selector(onSlider8Action:) forControlEvents:UIControlEventValueChanged];
    [sliderLGeq8 setBackgroundColor:[UIColor clearColor]];
    [sliderLGeq8 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq8 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq8 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderLGeq8.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderLGeq8.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderLGeq8.continuous = YES;
    sliderLGeq8.value = 0.0;
    [viewGeqFrameL addSubview:sliderLGeq8];
    
    sliderLGeq9 = [[UISlider alloc] initWithFrame:CGRectMake(331, 100, 230, 12)];
    sliderLGeq9.transform = CGAffineTransformRotate(sliderLGeq9.transform, 270.0/180*M_PI);
    [sliderLGeq9 addTarget:self action:@selector(onSlider9Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderLGeq9 addTarget:self action:@selector(onSlider9End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderLGeq9 addTarget:self action:@selector(onSlider9End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderLGeq9 addTarget:self action:@selector(onSlider9Action:) forControlEvents:UIControlEventValueChanged];
    [sliderLGeq9 setBackgroundColor:[UIColor clearColor]];
    [sliderLGeq9 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq9 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq9 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderLGeq9.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderLGeq9.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderLGeq9.continuous = YES;
    sliderLGeq9.value = 0.0;
    [viewGeqFrameL addSubview:sliderLGeq9];
    
    sliderLGeq10 = [[UISlider alloc] initWithFrame:CGRectMake(381, 100, 230, 12)];
    sliderLGeq10.transform = CGAffineTransformRotate(sliderLGeq10.transform, 270.0/180*M_PI);
    [sliderLGeq10 addTarget:self action:@selector(onSlider10Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderLGeq10 addTarget:self action:@selector(onSlider10End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderLGeq10 addTarget:self action:@selector(onSlider10End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderLGeq10 addTarget:self action:@selector(onSlider10Action:) forControlEvents:UIControlEventValueChanged];
    [sliderLGeq10 setBackgroundColor:[UIColor clearColor]];
    [sliderLGeq10 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq10 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq10 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderLGeq10.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderLGeq10.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderLGeq10.continuous = YES;
    sliderLGeq10.value = 0.0;
    [viewGeqFrameL addSubview:sliderLGeq10];
    
    sliderLGeq11 = [[UISlider alloc] initWithFrame:CGRectMake(431, 100, 230, 12)];
    sliderLGeq11.transform = CGAffineTransformRotate(sliderLGeq11.transform, 270.0/180*M_PI);
    [sliderLGeq11 addTarget:self action:@selector(onSlider11Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderLGeq11 addTarget:self action:@selector(onSlider11End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderLGeq11 addTarget:self action:@selector(onSlider11End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderLGeq11 addTarget:self action:@selector(onSlider11Action:) forControlEvents:UIControlEventValueChanged];
    [sliderLGeq11 setBackgroundColor:[UIColor clearColor]];
    [sliderLGeq11 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq11 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq11 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderLGeq11.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderLGeq11.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderLGeq11.continuous = YES;
    sliderLGeq11.value = 0.0;
    [viewGeqFrameL addSubview:sliderLGeq11];
    
    sliderLGeq12 = [[UISlider alloc] initWithFrame:CGRectMake(481, 100, 230, 12)];
    sliderLGeq12.transform = CGAffineTransformRotate(sliderLGeq12.transform, 270.0/180*M_PI);
    [sliderLGeq12 addTarget:self action:@selector(onSlider12Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderLGeq12 addTarget:self action:@selector(onSlider12End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderLGeq12 addTarget:self action:@selector(onSlider12End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderLGeq12 addTarget:self action:@selector(onSlider12Action:) forControlEvents:UIControlEventValueChanged];
    [sliderLGeq12 setBackgroundColor:[UIColor clearColor]];
    [sliderLGeq12 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq12 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq12 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderLGeq12.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderLGeq12.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderLGeq12.continuous = YES;
    sliderLGeq12.value = 0.0;
    [viewGeqFrameL addSubview:sliderLGeq12];
    
    sliderLGeq13 = [[UISlider alloc] initWithFrame:CGRectMake(531, 100, 230, 12)];
    sliderLGeq13.transform = CGAffineTransformRotate(sliderLGeq13.transform, 270.0/180*M_PI);
    [sliderLGeq13 addTarget:self action:@selector(onSlider13Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderLGeq13 addTarget:self action:@selector(onSlider13End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderLGeq13 addTarget:self action:@selector(onSlider13End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderLGeq13 addTarget:self action:@selector(onSlider13Action:) forControlEvents:UIControlEventValueChanged];
    [sliderLGeq13 setBackgroundColor:[UIColor clearColor]];
    [sliderLGeq13 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq13 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq13 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderLGeq13.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderLGeq13.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderLGeq13.continuous = YES;
    sliderLGeq13.value = 0.0;
    [viewGeqFrameL addSubview:sliderLGeq13];
    
    sliderLGeq14 = [[UISlider alloc] initWithFrame:CGRectMake(581, 100, 230, 12)];
    sliderLGeq14.transform = CGAffineTransformRotate(sliderLGeq14.transform, 270.0/180*M_PI);
    [sliderLGeq14 addTarget:self action:@selector(onSlider14Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderLGeq14 addTarget:self action:@selector(onSlider14End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderLGeq14 addTarget:self action:@selector(onSlider14End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderLGeq14 addTarget:self action:@selector(onSlider14Action:) forControlEvents:UIControlEventValueChanged];
    [sliderLGeq14 setBackgroundColor:[UIColor clearColor]];
    [sliderLGeq14 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq14 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq14 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderLGeq14.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderLGeq14.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderLGeq14.continuous = YES;
    sliderLGeq14.value = 0.0;
    [viewGeqFrameL addSubview:sliderLGeq14];
    
    sliderLGeq15 = [[UISlider alloc] initWithFrame:CGRectMake(631, 100, 230, 12)];
    sliderLGeq15.transform = CGAffineTransformRotate(sliderLGeq15.transform, 270.0/180*M_PI);
    [sliderLGeq15 addTarget:self action:@selector(onSlider15Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderLGeq15 addTarget:self action:@selector(onSlider15End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderLGeq15 addTarget:self action:@selector(onSlider15End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderLGeq15 addTarget:self action:@selector(onSlider15Action:) forControlEvents:UIControlEventValueChanged];
    [sliderLGeq15 setBackgroundColor:[UIColor clearColor]];
    [sliderLGeq15 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq15 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderLGeq15 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderLGeq15.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderLGeq15.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderLGeq15.continuous = YES;
    sliderLGeq15.value = 0.0;
    [viewGeqFrameL addSubview:sliderLGeq15];
    
    // Add R sliders
    sliderRGeq1 = [[UISlider alloc] initWithFrame:CGRectMake(-69, 100, 230, 12)];
    sliderRGeq1.transform = CGAffineTransformRotate(sliderRGeq1.transform, 270.0/180*M_PI);
    [sliderRGeq1 addTarget:self action:@selector(onSlider1Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderRGeq1 addTarget:self action:@selector(onSlider1End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderRGeq1 addTarget:self action:@selector(onSlider1End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderRGeq1 addTarget:self action:@selector(onSlider1Action:) forControlEvents:UIControlEventValueChanged];
    [sliderRGeq1 setBackgroundColor:[UIColor clearColor]];
    [sliderRGeq1 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq1 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq1 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderRGeq1.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderRGeq1.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderRGeq1.continuous = YES;
    sliderRGeq1.value = 0.0;
    [viewGeqFrameR addSubview:sliderRGeq1];
    
    sliderRGeq2 = [[UISlider alloc] initWithFrame:CGRectMake(-19, 100, 230, 12)];
    sliderRGeq2.transform = CGAffineTransformRotate(sliderRGeq2.transform, 270.0/180*M_PI);
    [sliderRGeq2 addTarget:self action:@selector(onSlider2Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderRGeq2 addTarget:self action:@selector(onSlider2End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderRGeq2 addTarget:self action:@selector(onSlider2End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderRGeq2 addTarget:self action:@selector(onSlider2Action:) forControlEvents:UIControlEventValueChanged];
    [sliderRGeq2 setBackgroundColor:[UIColor clearColor]];
    [sliderRGeq2 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq2 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq2 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderRGeq2.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderRGeq2.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderRGeq2.continuous = YES;
    sliderRGeq2.value = 0.0;
    [viewGeqFrameR addSubview:sliderRGeq2];
    
    sliderRGeq3 = [[UISlider alloc] initWithFrame:CGRectMake(31, 100, 230, 12)];
    sliderRGeq3.transform = CGAffineTransformRotate(sliderRGeq3.transform, 270.0/180*M_PI);
    [sliderRGeq3 addTarget:self action:@selector(onSlider3Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderRGeq3 addTarget:self action:@selector(onSlider3End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderRGeq3 addTarget:self action:@selector(onSlider3End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderRGeq3 addTarget:self action:@selector(onSlider3Action:) forControlEvents:UIControlEventValueChanged];
    [sliderRGeq3 setBackgroundColor:[UIColor clearColor]];
    [sliderRGeq3 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq3 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq3 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderRGeq3.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderRGeq3.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderRGeq3.continuous = YES;
    sliderRGeq3.value = 0.0;
    [viewGeqFrameR addSubview:sliderRGeq3];
    
    sliderRGeq4 = [[UISlider alloc] initWithFrame:CGRectMake(81, 100, 230, 12)];
    sliderRGeq4.transform = CGAffineTransformRotate(sliderRGeq4.transform, 270.0/180*M_PI);
    [sliderRGeq4 addTarget:self action:@selector(onSlider4Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderRGeq4 addTarget:self action:@selector(onSlider4End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderRGeq4 addTarget:self action:@selector(onSlider4End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderRGeq4 addTarget:self action:@selector(onSlider4Action:) forControlEvents:UIControlEventValueChanged];
    [sliderRGeq4 setBackgroundColor:[UIColor clearColor]];
    [sliderRGeq4 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq4 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq4 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderRGeq4.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderRGeq4.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderRGeq4.continuous = YES;
    sliderRGeq4.value = 0.0;
    [viewGeqFrameR addSubview:sliderRGeq4];
    
    sliderRGeq5 = [[UISlider alloc] initWithFrame:CGRectMake(131, 100, 230, 12)];
    sliderRGeq5.transform = CGAffineTransformRotate(sliderRGeq5.transform, 270.0/180*M_PI);
    [sliderRGeq5 addTarget:self action:@selector(onSlider5Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderRGeq5 addTarget:self action:@selector(onSlider5End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderRGeq5 addTarget:self action:@selector(onSlider5End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderRGeq5 addTarget:self action:@selector(onSlider5Action:) forControlEvents:UIControlEventValueChanged];
    [sliderRGeq5 setBackgroundColor:[UIColor clearColor]];
    [sliderRGeq5 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq5 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq5 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderRGeq5.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderRGeq5.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderRGeq5.continuous = YES;
    sliderRGeq5.value = 0.0;
    [viewGeqFrameR addSubview:sliderRGeq5];
    
    sliderRGeq6 = [[UISlider alloc] initWithFrame:CGRectMake(181, 100, 230, 12)];
    sliderRGeq6.transform = CGAffineTransformRotate(sliderRGeq6.transform, 270.0/180*M_PI);
    [sliderRGeq6 addTarget:self action:@selector(onSlider6Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderRGeq6 addTarget:self action:@selector(onSlider6End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderRGeq6 addTarget:self action:@selector(onSlider6End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderRGeq6 addTarget:self action:@selector(onSlider6Action:) forControlEvents:UIControlEventValueChanged];
    [sliderRGeq6 setBackgroundColor:[UIColor clearColor]];
    [sliderRGeq6 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq6 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq6 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderRGeq6.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderRGeq6.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderRGeq6.continuous = YES;
    sliderRGeq6.value = 0.0;
    [viewGeqFrameR addSubview:sliderRGeq6];
    
    sliderRGeq7 = [[UISlider alloc] initWithFrame:CGRectMake(231, 100, 230, 12)];
    sliderRGeq7.transform = CGAffineTransformRotate(sliderRGeq7.transform, 270.0/180*M_PI);
    [sliderRGeq7 addTarget:self action:@selector(onSlider7Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderRGeq7 addTarget:self action:@selector(onSlider7End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderRGeq7 addTarget:self action:@selector(onSlider7End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderRGeq7 addTarget:self action:@selector(onSlider7Action:) forControlEvents:UIControlEventValueChanged];
    [sliderRGeq7 setBackgroundColor:[UIColor clearColor]];
    [sliderRGeq7 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq7 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq7 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderRGeq7.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderRGeq7.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderRGeq7.continuous = YES;
    sliderRGeq7.value = 0.0;
    [viewGeqFrameR addSubview:sliderRGeq7];
    
    sliderRGeq8 = [[UISlider alloc] initWithFrame:CGRectMake(281, 100, 230, 12)];
    sliderRGeq8.transform = CGAffineTransformRotate(sliderRGeq8.transform, 270.0/180*M_PI);
    [sliderRGeq8 addTarget:self action:@selector(onSlider8Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderRGeq8 addTarget:self action:@selector(onSlider8End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderRGeq8 addTarget:self action:@selector(onSlider8End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderRGeq8 addTarget:self action:@selector(onSlider8Action:) forControlEvents:UIControlEventValueChanged];
    [sliderRGeq8 setBackgroundColor:[UIColor clearColor]];
    [sliderRGeq8 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq8 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq8 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderRGeq8.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderRGeq8.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderRGeq8.continuous = YES;
    sliderRGeq8.value = 0.0;
    [viewGeqFrameR addSubview:sliderRGeq8];
    
    sliderRGeq9 = [[UISlider alloc] initWithFrame:CGRectMake(331, 100, 230, 12)];
    sliderRGeq9.transform = CGAffineTransformRotate(sliderRGeq9.transform, 270.0/180*M_PI);
    [sliderRGeq9 addTarget:self action:@selector(onSlider9Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderRGeq9 addTarget:self action:@selector(onSlider9End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderRGeq9 addTarget:self action:@selector(onSlider9End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderRGeq9 addTarget:self action:@selector(onSlider9Action:) forControlEvents:UIControlEventValueChanged];
    [sliderRGeq9 setBackgroundColor:[UIColor clearColor]];
    [sliderRGeq9 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq9 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq9 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderRGeq9.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderRGeq9.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderRGeq9.continuous = YES;
    sliderRGeq9.value = 0.0;
    [viewGeqFrameR addSubview:sliderRGeq9];
    
    sliderRGeq10 = [[UISlider alloc] initWithFrame:CGRectMake(381, 100, 230, 12)];
    sliderRGeq10.transform = CGAffineTransformRotate(sliderRGeq10.transform, 270.0/180*M_PI);
    [sliderRGeq10 addTarget:self action:@selector(onSlider10Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderRGeq10 addTarget:self action:@selector(onSlider10End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderRGeq10 addTarget:self action:@selector(onSlider10End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderRGeq10 addTarget:self action:@selector(onSlider10Action:) forControlEvents:UIControlEventValueChanged];
    [sliderRGeq10 setBackgroundColor:[UIColor clearColor]];
    [sliderRGeq10 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq10 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq10 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderRGeq10.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderRGeq10.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderRGeq10.continuous = YES;
    sliderRGeq10.value = 0.0;
    [viewGeqFrameR addSubview:sliderRGeq10];
    
    sliderRGeq11 = [[UISlider alloc] initWithFrame:CGRectMake(431, 100, 230, 12)];
    sliderRGeq11.transform = CGAffineTransformRotate(sliderRGeq11.transform, 270.0/180*M_PI);
    [sliderRGeq11 addTarget:self action:@selector(onSlider11Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderRGeq11 addTarget:self action:@selector(onSlider11End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderRGeq11 addTarget:self action:@selector(onSlider11End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderRGeq11 addTarget:self action:@selector(onSlider11Action:) forControlEvents:UIControlEventValueChanged];
    [sliderRGeq11 setBackgroundColor:[UIColor clearColor]];
    [sliderRGeq11 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq11 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq11 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderRGeq11.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderRGeq11.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderRGeq11.continuous = YES;
    sliderRGeq11.value = 0.0;
    [viewGeqFrameR addSubview:sliderRGeq11];
    
    sliderRGeq12 = [[UISlider alloc] initWithFrame:CGRectMake(481, 100, 230, 12)];
    sliderRGeq12.transform = CGAffineTransformRotate(sliderRGeq12.transform, 270.0/180*M_PI);
    [sliderRGeq12 addTarget:self action:@selector(onSlider12Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderRGeq12 addTarget:self action:@selector(onSlider12End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderRGeq12 addTarget:self action:@selector(onSlider12End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderRGeq12 addTarget:self action:@selector(onSlider12Action:) forControlEvents:UIControlEventValueChanged];
    [sliderRGeq12 setBackgroundColor:[UIColor clearColor]];
    [sliderRGeq12 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq12 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq12 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderRGeq12.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderRGeq12.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderRGeq12.continuous = YES;
    sliderRGeq12.value = 0.0;
    [viewGeqFrameR addSubview:sliderRGeq12];
    
    sliderRGeq13 = [[UISlider alloc] initWithFrame:CGRectMake(531, 100, 230, 12)];
    sliderRGeq13.transform = CGAffineTransformRotate(sliderRGeq13.transform, 270.0/180*M_PI);
    [sliderRGeq13 addTarget:self action:@selector(onSlider13Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderRGeq13 addTarget:self action:@selector(onSlider13End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderRGeq13 addTarget:self action:@selector(onSlider13End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderRGeq13 addTarget:self action:@selector(onSlider13Action:) forControlEvents:UIControlEventValueChanged];
    [sliderRGeq13 setBackgroundColor:[UIColor clearColor]];
    [sliderRGeq13 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq13 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq13 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderRGeq13.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderRGeq13.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderRGeq13.continuous = YES;
    sliderRGeq13.value = 0.0;
    [viewGeqFrameR addSubview:sliderRGeq13];
    
    sliderRGeq14 = [[UISlider alloc] initWithFrame:CGRectMake(581, 100, 230, 12)];
    sliderRGeq14.transform = CGAffineTransformRotate(sliderRGeq14.transform, 270.0/180*M_PI);
    [sliderRGeq14 addTarget:self action:@selector(onSlider14Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderRGeq14 addTarget:self action:@selector(onSlider14End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderRGeq14 addTarget:self action:@selector(onSlider14End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderRGeq14 addTarget:self action:@selector(onSlider14Action:) forControlEvents:UIControlEventValueChanged];
    [sliderRGeq14 setBackgroundColor:[UIColor clearColor]];
    [sliderRGeq14 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq14 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq14 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderRGeq14.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderRGeq14.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderRGeq14.continuous = YES;
    sliderRGeq14.value = 0.0;
    [viewGeqFrameR addSubview:sliderRGeq14];
    
    sliderRGeq15 = [[UISlider alloc] initWithFrame:CGRectMake(631, 100, 230, 12)];
    sliderRGeq15.transform = CGAffineTransformRotate(sliderRGeq15.transform, 270.0/180*M_PI);
    [sliderRGeq15 addTarget:self action:@selector(onSlider15Begin:) forControlEvents:UIControlEventTouchDown];
    [sliderRGeq15 addTarget:self action:@selector(onSlider15End:) forControlEvents:UIControlEventTouchUpInside];
    [sliderRGeq15 addTarget:self action:@selector(onSlider15End:) forControlEvents:UIControlEventTouchUpOutside];
    [sliderRGeq15 addTarget:self action:@selector(onSlider15Action:) forControlEvents:UIControlEventValueChanged];
    [sliderRGeq15 setBackgroundColor:[UIColor clearColor]];
    [sliderRGeq15 setMinimumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq15 setMaximumTrackImage:[UIImage alloc] forState:UIControlStateNormal];
    [sliderRGeq15 setThumbImage:sliderImage forState:UIControlStateNormal];
    sliderRGeq15.minimumValue = GEQ_SLIDER_MIN_VALUE;
    sliderRGeq15.maximumValue = GEQ_SLIDER_MAX_VALUE;
    sliderRGeq15.continuous = YES;
    sliderRGeq15.value = 0.0;
    [viewGeqFrameR addSubview:sliderRGeq15];
    
    // Add L slider meters
    meterL1Top = [[JEProgressView alloc] initWithFrame:CGRectMake(-8, 53, 108, 12)];
    meterL1Top.transform = CGAffineTransformRotate(meterL1Top.transform, 270.0/180*M_PI);
    [meterL1Top setProgressImage:progressImage];
    [meterL1Top setTrackImage:[UIImage alloc]];
    [meterL1Top setProgress:0.0];
    [viewGeqFrameL addSubview:meterL1Top];
    [viewGeqFrameL sendSubviewToBack:meterL1Top];
    
    meterL1Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(-8, 158, 108, 12)];
    meterL1Btm.transform = CGAffineTransformRotate(meterL1Btm.transform, 90.0/180*M_PI);
    [meterL1Btm setProgressImage:progressImage];
    [meterL1Btm setTrackImage:[UIImage alloc]];
    [meterL1Btm setProgress:0.0];
    [viewGeqFrameL addSubview:meterL1Btm];
    [viewGeqFrameL sendSubviewToBack:meterL1Btm];
    
    meterL2Top = [[JEProgressView alloc] initWithFrame:CGRectMake(42, 53, 108, 12)];
    meterL2Top.transform = CGAffineTransformRotate(meterL2Top.transform, 270.0/180*M_PI);
    [meterL2Top setProgressImage:progressImage];
    [meterL2Top setTrackImage:[UIImage alloc]];
    [meterL2Top setProgress:0.0];
    [viewGeqFrameL addSubview:meterL2Top];
    [viewGeqFrameL sendSubviewToBack:meterL2Top];
    
    meterL2Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(42, 158, 108, 12)];
    meterL2Btm.transform = CGAffineTransformRotate(meterL2Btm.transform, 90.0/180*M_PI);
    [meterL2Btm setProgressImage:progressImage];
    [meterL2Btm setTrackImage:[UIImage alloc]];
    [meterL2Btm setProgress:0.0];
    [viewGeqFrameL addSubview:meterL2Btm];
    [viewGeqFrameL sendSubviewToBack:meterL2Btm];
    
    meterL3Top = [[JEProgressView alloc] initWithFrame:CGRectMake(92, 53, 108, 12)];
    meterL3Top.transform = CGAffineTransformRotate(meterL3Top.transform, 270.0/180*M_PI);
    [meterL3Top setProgressImage:progressImage];
    [meterL3Top setTrackImage:[UIImage alloc]];
    [meterL3Top setProgress:0.0];
    [viewGeqFrameL addSubview:meterL3Top];
    [viewGeqFrameL sendSubviewToBack:meterL3Top];
    
    meterL3Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(92, 158, 108, 12)];
    meterL3Btm.transform = CGAffineTransformRotate(meterL3Btm.transform, 90.0/180*M_PI);
    [meterL3Btm setProgressImage:progressImage];
    [meterL3Btm setTrackImage:[UIImage alloc]];
    [meterL3Btm setProgress:0.0];
    [viewGeqFrameL addSubview:meterL3Btm];
    [viewGeqFrameL sendSubviewToBack:meterL3Btm];
    
    meterL4Top = [[JEProgressView alloc] initWithFrame:CGRectMake(142, 53, 108, 12)];
    meterL4Top.transform = CGAffineTransformRotate(meterL4Top.transform, 270.0/180*M_PI);
    [meterL4Top setProgressImage:progressImage];
    [meterL4Top setTrackImage:[UIImage alloc]];
    [meterL4Top setProgress:0.0];
    [viewGeqFrameL addSubview:meterL4Top];
    [viewGeqFrameL sendSubviewToBack:meterL4Top];
    
    meterL4Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(142, 158, 108, 12)];
    meterL4Btm.transform = CGAffineTransformRotate(meterL4Btm.transform, 90.0/180*M_PI);
    [meterL4Btm setProgressImage:progressImage];
    [meterL4Btm setTrackImage:[UIImage alloc]];
    [meterL4Btm setProgress:0.0];
    [viewGeqFrameL addSubview:meterL4Btm];
    [viewGeqFrameL sendSubviewToBack:meterL4Btm];
    
    meterL5Top = [[JEProgressView alloc] initWithFrame:CGRectMake(192, 53, 108, 12)];
    meterL5Top.transform = CGAffineTransformRotate(meterL5Top.transform, 270.0/180*M_PI);
    [meterL5Top setProgressImage:progressImage];
    [meterL5Top setTrackImage:[UIImage alloc]];
    [meterL5Top setProgress:0.0];
    [viewGeqFrameL addSubview:meterL5Top];
    [viewGeqFrameL sendSubviewToBack:meterL5Top];
    
    meterL5Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(192, 158, 108, 12)];
    meterL5Btm.transform = CGAffineTransformRotate(meterL5Btm.transform, 90.0/180*M_PI);
    [meterL5Btm setProgressImage:progressImage];
    [meterL5Btm setTrackImage:[UIImage alloc]];
    [meterL5Btm setProgress:0.0];
    [viewGeqFrameL addSubview:meterL5Btm];
    [viewGeqFrameL sendSubviewToBack:meterL5Btm];
    
    meterL6Top = [[JEProgressView alloc] initWithFrame:CGRectMake(242, 53, 108, 12)];
    meterL6Top.transform = CGAffineTransformRotate(meterL6Top.transform, 270.0/180*M_PI);
    [meterL6Top setProgressImage:progressImage];
    [meterL6Top setTrackImage:[UIImage alloc]];
    [meterL6Top setProgress:0.0];
    [viewGeqFrameL addSubview:meterL6Top];
    [viewGeqFrameL sendSubviewToBack:meterL6Top];
    
    meterL6Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(242, 158, 108, 12)];
    meterL6Btm.transform = CGAffineTransformRotate(meterL6Btm.transform, 90.0/180*M_PI);
    [meterL6Btm setProgressImage:progressImage];
    [meterL6Btm setTrackImage:[UIImage alloc]];
    [meterL6Btm setProgress:0.0];
    [viewGeqFrameL addSubview:meterL6Btm];
    [viewGeqFrameL sendSubviewToBack:meterL6Btm];
    
    meterL7Top = [[JEProgressView alloc] initWithFrame:CGRectMake(292, 53, 108, 12)];
    meterL7Top.transform = CGAffineTransformRotate(meterL7Top.transform, 270.0/180*M_PI);
    [meterL7Top setProgressImage:progressImage];
    [meterL7Top setTrackImage:[UIImage alloc]];
    [meterL7Top setProgress:0.0];
    [viewGeqFrameL addSubview:meterL7Top];
    [viewGeqFrameL sendSubviewToBack:meterL7Top];
    
    meterL7Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(292, 158, 108, 12)];
    meterL7Btm.transform = CGAffineTransformRotate(meterL7Btm.transform, 90.0/180*M_PI);
    [meterL7Btm setProgressImage:progressImage];
    [meterL7Btm setTrackImage:[UIImage alloc]];
    [meterL7Btm setProgress:0.0];
    [viewGeqFrameL addSubview:meterL7Btm];
    [viewGeqFrameL sendSubviewToBack:meterL7Btm];
    
    meterL8Top = [[JEProgressView alloc] initWithFrame:CGRectMake(342, 53, 108, 12)];
    meterL8Top.transform = CGAffineTransformRotate(meterL8Top.transform, 270.0/180*M_PI);
    [meterL8Top setProgressImage:progressImage];
    [meterL8Top setTrackImage:[UIImage alloc]];
    [meterL8Top setProgress:0.0];
    [viewGeqFrameL addSubview:meterL8Top];
    [viewGeqFrameL sendSubviewToBack:meterL8Top];
    
    meterL8Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(342, 158, 108, 12)];
    meterL8Btm.transform = CGAffineTransformRotate(meterL8Btm.transform, 90.0/180*M_PI);
    [meterL8Btm setProgressImage:progressImage];
    [meterL8Btm setTrackImage:[UIImage alloc]];
    [meterL8Btm setProgress:0.0];
    [viewGeqFrameL addSubview:meterL8Btm];
    [viewGeqFrameL sendSubviewToBack:meterL8Btm];
    
    meterL9Top = [[JEProgressView alloc] initWithFrame:CGRectMake(392, 53, 108, 12)];
    meterL9Top.transform = CGAffineTransformRotate(meterL9Top.transform, 270.0/180*M_PI);
    [meterL9Top setProgressImage:progressImage];
    [meterL9Top setTrackImage:[UIImage alloc]];
    [meterL9Top setProgress:0.0];
    [viewGeqFrameL addSubview:meterL9Top];
    [viewGeqFrameL sendSubviewToBack:meterL9Top];
    
    meterL9Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(392, 158, 108, 12)];
    meterL9Btm.transform = CGAffineTransformRotate(meterL9Btm.transform, 90.0/180*M_PI);
    [meterL9Btm setProgressImage:progressImage];
    [meterL9Btm setTrackImage:[UIImage alloc]];
    [meterL9Btm setProgress:0.0];
    [viewGeqFrameL addSubview:meterL9Btm];
    [viewGeqFrameL sendSubviewToBack:meterL9Btm];
    
    meterL10Top = [[JEProgressView alloc] initWithFrame:CGRectMake(442, 53, 108, 12)];
    meterL10Top.transform = CGAffineTransformRotate(meterL10Top.transform, 270.0/180*M_PI);
    [meterL10Top setProgressImage:progressImage];
    [meterL10Top setTrackImage:[UIImage alloc]];
    [meterL10Top setProgress:0.0];
    [viewGeqFrameL addSubview:meterL10Top];
    [viewGeqFrameL sendSubviewToBack:meterL10Top];
    
    meterL10Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(442, 158, 108, 12)];
    meterL10Btm.transform = CGAffineTransformRotate(meterL10Btm.transform, 90.0/180*M_PI);
    [meterL10Btm setProgressImage:progressImage];
    [meterL10Btm setTrackImage:[UIImage alloc]];
    [meterL10Btm setProgress:0.0];
    [viewGeqFrameL addSubview:meterL10Btm];
    [viewGeqFrameL sendSubviewToBack:meterL10Btm];
    
    meterL11Top = [[JEProgressView alloc] initWithFrame:CGRectMake(492, 53, 108, 12)];
    meterL11Top.transform = CGAffineTransformRotate(meterL11Top.transform, 270.0/180*M_PI);
    [meterL11Top setProgressImage:progressImage];
    [meterL11Top setTrackImage:[UIImage alloc]];
    [meterL11Top setProgress:0.0];
    [viewGeqFrameL addSubview:meterL11Top];
    [viewGeqFrameL sendSubviewToBack:meterL11Top];
    
    meterL11Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(492, 158, 108, 12)];
    meterL11Btm.transform = CGAffineTransformRotate(meterL11Btm.transform, 90.0/180*M_PI);
    [meterL11Btm setProgressImage:progressImage];
    [meterL11Btm setTrackImage:[UIImage alloc]];
    [meterL11Btm setProgress:0.0];
    [viewGeqFrameL addSubview:meterL11Btm];
    [viewGeqFrameL sendSubviewToBack:meterL11Btm];
    
    meterL12Top = [[JEProgressView alloc] initWithFrame:CGRectMake(542, 53, 108, 12)];
    meterL12Top.transform = CGAffineTransformRotate(meterL12Top.transform, 270.0/180*M_PI);
    [meterL12Top setProgressImage:progressImage];
    [meterL12Top setTrackImage:[UIImage alloc]];
    [meterL12Top setProgress:0.0];
    [viewGeqFrameL addSubview:meterL12Top];
    [viewGeqFrameL sendSubviewToBack:meterL12Top];
    
    meterL12Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(542, 158, 108, 12)];
    meterL12Btm.transform = CGAffineTransformRotate(meterL12Btm.transform, 90.0/180*M_PI);
    [meterL12Btm setProgressImage:progressImage];
    [meterL12Btm setTrackImage:[UIImage alloc]];
    [meterL12Btm setProgress:0.0];
    [viewGeqFrameL addSubview:meterL12Btm];
    [viewGeqFrameL sendSubviewToBack:meterL12Btm];
    
    meterL13Top = [[JEProgressView alloc] initWithFrame:CGRectMake(592, 53, 108, 12)];
    meterL13Top.transform = CGAffineTransformRotate(meterL13Top.transform, 270.0/180*M_PI);
    [meterL13Top setProgressImage:progressImage];
    [meterL13Top setTrackImage:[UIImage alloc]];
    [meterL13Top setProgress:0.0];
    [viewGeqFrameL addSubview:meterL13Top];
    [viewGeqFrameL sendSubviewToBack:meterL13Top];
    
    meterL13Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(592, 158, 108, 12)];
    meterL13Btm.transform = CGAffineTransformRotate(meterL13Btm.transform, 90.0/180*M_PI);
    [meterL13Btm setProgressImage:progressImage];
    [meterL13Btm setTrackImage:[UIImage alloc]];
    [meterL13Btm setProgress:0.0];
    [viewGeqFrameL addSubview:meterL13Btm];
    [viewGeqFrameL sendSubviewToBack:meterL13Btm];
    
    meterL14Top = [[JEProgressView alloc] initWithFrame:CGRectMake(642, 53, 108, 12)];
    meterL14Top.transform = CGAffineTransformRotate(meterL14Top.transform, 270.0/180*M_PI);
    [meterL14Top setProgressImage:progressImage];
    [meterL14Top setTrackImage:[UIImage alloc]];
    [meterL14Top setProgress:0.0];
    [viewGeqFrameL addSubview:meterL14Top];
    [viewGeqFrameL sendSubviewToBack:meterL14Top];
    
    meterL14Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(642, 158, 108, 12)];
    meterL14Btm.transform = CGAffineTransformRotate(meterL14Btm.transform, 90.0/180*M_PI);
    [meterL14Btm setProgressImage:progressImage];
    [meterL14Btm setTrackImage:[UIImage alloc]];
    [meterL14Btm setProgress:0.0];
    [viewGeqFrameL addSubview:meterL14Btm];
    [viewGeqFrameL sendSubviewToBack:meterL14Btm];
    
    meterL15Top = [[JEProgressView alloc] initWithFrame:CGRectMake(692, 53, 108, 12)];
    meterL15Top.transform = CGAffineTransformRotate(meterL15Top.transform, 270.0/180*M_PI);
    [meterL15Top setProgressImage:progressImage];
    [meterL15Top setTrackImage:[UIImage alloc]];
    [meterL15Top setProgress:0.0];
    [viewGeqFrameL addSubview:meterL15Top];
    [viewGeqFrameL sendSubviewToBack:meterL15Top];
    
    meterL15Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(692, 158, 108, 12)];
    meterL15Btm.transform = CGAffineTransformRotate(meterL15Btm.transform, 90.0/180*M_PI);
    [meterL15Btm setProgressImage:progressImage];
    [meterL15Btm setTrackImage:[UIImage alloc]];
    [meterL15Btm setProgress:0.0];
    [viewGeqFrameL addSubview:meterL15Btm];
    [viewGeqFrameL sendSubviewToBack:meterL15Btm];
    
    // Add R slider meters
    meterR1Top = [[JEProgressView alloc] initWithFrame:CGRectMake(-8, 53, 108, 12)];
    meterR1Top.transform = CGAffineTransformRotate(meterR1Top.transform, 270.0/180*M_PI);
    [meterR1Top setProgressImage:progressImage];
    [meterR1Top setTrackImage:[UIImage alloc]];
    [meterR1Top setProgress:0.0];
    [viewGeqFrameR addSubview:meterR1Top];
    [viewGeqFrameR sendSubviewToBack:meterR1Top];
    
    meterR1Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(-8, 158, 108, 12)];
    meterR1Btm.transform = CGAffineTransformRotate(meterR1Btm.transform, 90.0/180*M_PI);
    [meterR1Btm setProgressImage:progressImage];
    [meterR1Btm setTrackImage:[UIImage alloc]];
    [meterR1Btm setProgress:0.0];
    [viewGeqFrameR addSubview:meterR1Btm];
    [viewGeqFrameR sendSubviewToBack:meterR1Btm];
    
    meterR2Top = [[JEProgressView alloc] initWithFrame:CGRectMake(42, 53, 108, 12)];
    meterR2Top.transform = CGAffineTransformRotate(meterR2Top.transform, 270.0/180*M_PI);
    [meterR2Top setProgressImage:progressImage];
    [meterR2Top setTrackImage:[UIImage alloc]];
    [meterR2Top setProgress:0.0];
    [viewGeqFrameR addSubview:meterR2Top];
    [viewGeqFrameR sendSubviewToBack:meterR2Top];
    
    meterR2Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(42, 158, 108, 12)];
    meterR2Btm.transform = CGAffineTransformRotate(meterR2Btm.transform, 90.0/180*M_PI);
    [meterR2Btm setProgressImage:progressImage];
    [meterR2Btm setTrackImage:[UIImage alloc]];
    [meterR2Btm setProgress:0.0];
    [viewGeqFrameR addSubview:meterR2Btm];
    [viewGeqFrameR sendSubviewToBack:meterR2Btm];
    
    meterR3Top = [[JEProgressView alloc] initWithFrame:CGRectMake(92, 53, 108, 12)];
    meterR3Top.transform = CGAffineTransformRotate(meterR3Top.transform, 270.0/180*M_PI);
    [meterR3Top setProgressImage:progressImage];
    [meterR3Top setTrackImage:[UIImage alloc]];
    [meterR3Top setProgress:0.0];
    [viewGeqFrameR addSubview:meterR3Top];
    [viewGeqFrameR sendSubviewToBack:meterR3Top];
    
    meterR3Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(92, 158, 108, 12)];
    meterR3Btm.transform = CGAffineTransformRotate(meterR3Btm.transform, 90.0/180*M_PI);
    [meterR3Btm setProgressImage:progressImage];
    [meterR3Btm setTrackImage:[UIImage alloc]];
    [meterR3Btm setProgress:0.0];
    [viewGeqFrameR addSubview:meterR3Btm];
    [viewGeqFrameR sendSubviewToBack:meterR3Btm];
    
    meterR4Top = [[JEProgressView alloc] initWithFrame:CGRectMake(142, 53, 108, 12)];
    meterR4Top.transform = CGAffineTransformRotate(meterR4Top.transform, 270.0/180*M_PI);
    [meterR4Top setProgressImage:progressImage];
    [meterR4Top setTrackImage:[UIImage alloc]];
    [meterR4Top setProgress:0.0];
    [viewGeqFrameR addSubview:meterR4Top];
    [viewGeqFrameR sendSubviewToBack:meterR4Top];
    
    meterR4Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(142, 158, 108, 12)];
    meterR4Btm.transform = CGAffineTransformRotate(meterR4Btm.transform, 90.0/180*M_PI);
    [meterR4Btm setProgressImage:progressImage];
    [meterR4Btm setTrackImage:[UIImage alloc]];
    [meterR4Btm setProgress:0.0];
    [viewGeqFrameR addSubview:meterR4Btm];
    [viewGeqFrameR sendSubviewToBack:meterR4Btm];
    
    meterR5Top = [[JEProgressView alloc] initWithFrame:CGRectMake(192, 53, 108, 12)];
    meterR5Top.transform = CGAffineTransformRotate(meterR5Top.transform, 270.0/180*M_PI);
    [meterR5Top setProgressImage:progressImage];
    [meterR5Top setTrackImage:[UIImage alloc]];
    [meterR5Top setProgress:0.0];
    [viewGeqFrameR addSubview:meterR5Top];
    [viewGeqFrameR sendSubviewToBack:meterR5Top];
    
    meterR5Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(192, 158, 108, 12)];
    meterR5Btm.transform = CGAffineTransformRotate(meterR5Btm.transform, 90.0/180*M_PI);
    [meterR5Btm setProgressImage:progressImage];
    [meterR5Btm setTrackImage:[UIImage alloc]];
    [meterR5Btm setProgress:0.0];
    [viewGeqFrameR addSubview:meterR5Btm];
    [viewGeqFrameR sendSubviewToBack:meterR5Btm];
    
    meterR6Top = [[JEProgressView alloc] initWithFrame:CGRectMake(242, 53, 108, 12)];
    meterR6Top.transform = CGAffineTransformRotate(meterR6Top.transform, 270.0/180*M_PI);
    [meterR6Top setProgressImage:progressImage];
    [meterR6Top setTrackImage:[UIImage alloc]];
    [meterR6Top setProgress:0.0];
    [viewGeqFrameR addSubview:meterR6Top];
    [viewGeqFrameR sendSubviewToBack:meterR6Top];
    
    meterR6Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(242, 158, 108, 12)];
    meterR6Btm.transform = CGAffineTransformRotate(meterR6Btm.transform, 90.0/180*M_PI);
    [meterR6Btm setProgressImage:progressImage];
    [meterR6Btm setTrackImage:[UIImage alloc]];
    [meterR6Btm setProgress:0.0];
    [viewGeqFrameR addSubview:meterR6Btm];
    [viewGeqFrameR sendSubviewToBack:meterR6Btm];
    
    meterR7Top = [[JEProgressView alloc] initWithFrame:CGRectMake(292, 53, 108, 12)];
    meterR7Top.transform = CGAffineTransformRotate(meterR7Top.transform, 270.0/180*M_PI);
    [meterR7Top setProgressImage:progressImage];
    [meterR7Top setTrackImage:[UIImage alloc]];
    [meterR7Top setProgress:0.0];
    [viewGeqFrameR addSubview:meterR7Top];
    [viewGeqFrameR sendSubviewToBack:meterR7Top];
    
    meterR7Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(292, 158, 108, 12)];
    meterR7Btm.transform = CGAffineTransformRotate(meterR7Btm.transform, 90.0/180*M_PI);
    [meterR7Btm setProgressImage:progressImage];
    [meterR7Btm setTrackImage:[UIImage alloc]];
    [meterR7Btm setProgress:0.0];
    [viewGeqFrameR addSubview:meterR7Btm];
    [viewGeqFrameR sendSubviewToBack:meterR7Btm];
    
    meterR8Top = [[JEProgressView alloc] initWithFrame:CGRectMake(342, 53, 108, 12)];
    meterR8Top.transform = CGAffineTransformRotate(meterR8Top.transform, 270.0/180*M_PI);
    [meterR8Top setProgressImage:progressImage];
    [meterR8Top setTrackImage:[UIImage alloc]];
    [meterR8Top setProgress:0.0];
    [viewGeqFrameR addSubview:meterR8Top];
    [viewGeqFrameR sendSubviewToBack:meterR8Top];
    
    meterR8Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(342, 158, 108, 12)];
    meterR8Btm.transform = CGAffineTransformRotate(meterR8Btm.transform, 90.0/180*M_PI);
    [meterR8Btm setProgressImage:progressImage];
    [meterR8Btm setTrackImage:[UIImage alloc]];
    [meterR8Btm setProgress:0.0];
    [viewGeqFrameR addSubview:meterR8Btm];
    [viewGeqFrameR sendSubviewToBack:meterR8Btm];
    
    meterR9Top = [[JEProgressView alloc] initWithFrame:CGRectMake(392, 53, 108, 12)];
    meterR9Top.transform = CGAffineTransformRotate(meterR9Top.transform, 270.0/180*M_PI);
    [meterR9Top setProgressImage:progressImage];
    [meterR9Top setTrackImage:[UIImage alloc]];
    [meterR9Top setProgress:0.0];
    [viewGeqFrameR addSubview:meterR9Top];
    [viewGeqFrameR sendSubviewToBack:meterR9Top];
    
    meterR9Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(392, 158, 108, 12)];
    meterR9Btm.transform = CGAffineTransformRotate(meterR9Btm.transform, 90.0/180*M_PI);
    [meterR9Btm setProgressImage:progressImage];
    [meterR9Btm setTrackImage:[UIImage alloc]];
    [meterR9Btm setProgress:0.0];
    [viewGeqFrameR addSubview:meterR9Btm];
    [viewGeqFrameR sendSubviewToBack:meterR9Btm];
    
    meterR10Top = [[JEProgressView alloc] initWithFrame:CGRectMake(442, 53, 108, 12)];
    meterR10Top.transform = CGAffineTransformRotate(meterR10Top.transform, 270.0/180*M_PI);
    [meterR10Top setProgressImage:progressImage];
    [meterR10Top setTrackImage:[UIImage alloc]];
    [meterR10Top setProgress:0.0];
    [viewGeqFrameR addSubview:meterR10Top];
    [viewGeqFrameR sendSubviewToBack:meterR10Top];
    
    meterR10Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(442, 158, 108, 12)];
    meterR10Btm.transform = CGAffineTransformRotate(meterR10Btm.transform, 90.0/180*M_PI);
    [meterR10Btm setProgressImage:progressImage];
    [meterR10Btm setTrackImage:[UIImage alloc]];
    [meterR10Btm setProgress:0.0];
    [viewGeqFrameR addSubview:meterR10Btm];
    [viewGeqFrameR sendSubviewToBack:meterR10Btm];
    
    meterR11Top = [[JEProgressView alloc] initWithFrame:CGRectMake(492, 53, 108, 12)];
    meterR11Top.transform = CGAffineTransformRotate(meterR11Top.transform, 270.0/180*M_PI);
    [meterR11Top setProgressImage:progressImage];
    [meterR11Top setTrackImage:[UIImage alloc]];
    [meterR11Top setProgress:0.0];
    [viewGeqFrameR addSubview:meterR11Top];
    [viewGeqFrameR sendSubviewToBack:meterR11Top];
    
    meterR11Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(492, 158, 108, 12)];
    meterR11Btm.transform = CGAffineTransformRotate(meterR11Btm.transform, 90.0/180*M_PI);
    [meterR11Btm setProgressImage:progressImage];
    [meterR11Btm setTrackImage:[UIImage alloc]];
    [meterR11Btm setProgress:0.0];
    [viewGeqFrameR addSubview:meterR11Btm];
    [viewGeqFrameR sendSubviewToBack:meterR11Btm];
    
    meterR12Top = [[JEProgressView alloc] initWithFrame:CGRectMake(542, 53, 108, 12)];
    meterR12Top.transform = CGAffineTransformRotate(meterR12Top.transform, 270.0/180*M_PI);
    [meterR12Top setProgressImage:progressImage];
    [meterR12Top setTrackImage:[UIImage alloc]];
    [meterR12Top setProgress:0.0];
    [viewGeqFrameR addSubview:meterR12Top];
    [viewGeqFrameR sendSubviewToBack:meterR12Top];
    
    meterR12Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(542, 158, 108, 12)];
    meterR12Btm.transform = CGAffineTransformRotate(meterR12Btm.transform, 90.0/180*M_PI);
    [meterR12Btm setProgressImage:progressImage];
    [meterR12Btm setTrackImage:[UIImage alloc]];
    [meterR12Btm setProgress:0.0];
    [viewGeqFrameR addSubview:meterR12Btm];
    [viewGeqFrameR sendSubviewToBack:meterR12Btm];
    
    meterR13Top = [[JEProgressView alloc] initWithFrame:CGRectMake(592, 53, 108, 12)];
    meterR13Top.transform = CGAffineTransformRotate(meterR13Top.transform, 270.0/180*M_PI);
    [meterR13Top setProgressImage:progressImage];
    [meterR13Top setTrackImage:[UIImage alloc]];
    [meterR13Top setProgress:0.0];
    [viewGeqFrameR addSubview:meterR13Top];
    [viewGeqFrameR sendSubviewToBack:meterR13Top];
    
    meterR13Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(592, 158, 108, 12)];
    meterR13Btm.transform = CGAffineTransformRotate(meterR13Btm.transform, 90.0/180*M_PI);
    [meterR13Btm setProgressImage:progressImage];
    [meterR13Btm setTrackImage:[UIImage alloc]];
    [meterR13Btm setProgress:0.0];
    [viewGeqFrameR addSubview:meterR13Btm];
    [viewGeqFrameR sendSubviewToBack:meterR13Btm];
    
    meterR14Top = [[JEProgressView alloc] initWithFrame:CGRectMake(642, 53, 108, 12)];
    meterR14Top.transform = CGAffineTransformRotate(meterR14Top.transform, 270.0/180*M_PI);
    [meterR14Top setProgressImage:progressImage];
    [meterR14Top setTrackImage:[UIImage alloc]];
    [meterR14Top setProgress:0.0];
    [viewGeqFrameR addSubview:meterR14Top];
    [viewGeqFrameR sendSubviewToBack:meterR14Top];
    
    meterR14Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(642, 158, 108, 12)];
    meterR14Btm.transform = CGAffineTransformRotate(meterR14Btm.transform, 90.0/180*M_PI);
    [meterR14Btm setProgressImage:progressImage];
    [meterR14Btm setTrackImage:[UIImage alloc]];
    [meterR14Btm setProgress:0.0];
    [viewGeqFrameR addSubview:meterR14Btm];
    [viewGeqFrameR sendSubviewToBack:meterR14Btm];
    
    meterR15Top = [[JEProgressView alloc] initWithFrame:CGRectMake(692, 53, 108, 12)];
    meterR15Top.transform = CGAffineTransformRotate(meterR15Top.transform, 270.0/180*M_PI);
    [meterR15Top setProgressImage:progressImage];
    [meterR15Top setTrackImage:[UIImage alloc]];
    [meterR15Top setProgress:0.0];
    [viewGeqFrameR addSubview:meterR15Top];
    [viewGeqFrameR sendSubviewToBack:meterR15Top];
    
    meterR15Btm = [[JEProgressView alloc] initWithFrame:CGRectMake(692, 158, 108, 12)];
    meterR15Btm.transform = CGAffineTransformRotate(meterR15Btm.transform, 90.0/180*M_PI);
    [meterR15Btm setProgressImage:progressImage];
    [meterR15Btm setTrackImage:[UIImage alloc]];
    [meterR15Btm setProgress:0.0];
    [viewGeqFrameR addSubview:meterR15Btm];
    [viewGeqFrameR sendSubviewToBack:meterR15Btm];
    
    [self.view sendSubviewToBack:self.viewGeqDrawL];
    [self.view sendSubviewToBack:self.viewGeqDrawR];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
    NSLog(@"GeqPeqViewController didReceiveMemoryWarning");
}

- (void)processReply:(EffectPagePacket *)pkt
{
    BOOL needsUpdate = NO;
    
    // Don't update if user is moving sliders
    if (geqLock) {
        return;
    }
    
    // Update GEQ sliders
    int geqL1Value = pkt->l_geq15_25_hz_db;
    int geqL2Value = pkt->l_geq15_40_hz_db;
    int geqL3Value = pkt->l_geq15_63_hz_db;
    int geqL4Value = pkt->l_geq15_100_hz_db;
    int geqL5Value = pkt->l_geq15_160_hz_db;
    int geqL6Value = pkt->l_geq15_250_hz_db;
    int geqL7Value = pkt->l_geq15_400_hz_db;
    int geqL8Value = pkt->l_geq15_630_hz_db;
    int geqL9Value = pkt->l_geq15_1_khz_db;
    int geqL10Value = pkt->l_geq15_1_6_khz_db;
    int geqL11Value = pkt->l_geq15_2_5_khz_db;
    int geqL12Value = pkt->l_geq15_4_khz_db;
    int geqL13Value = pkt->l_geq15_6_3_khz_db;
    int geqL14Value = pkt->l_geq15_10_khz_db;
    int geqL15Value = pkt->l_geq15_16_khz_db;
    
    if (sliderLGeq1.value != geqL1Value && ![viewController sendCommandExists:CMD_L_GEQ15_25HZ_DB dspId:DSP_5]) {
        sliderLGeq1.value = geqL1Value;
        if (sliderLGeq1.value > 0.0) {
            meterL1Top.progress = sliderLGeq1.value/GEQ_SLIDER_MAX_VALUE;
            meterL1Btm.progress = 0.0;
        } else if (sliderLGeq1.value < 0.0) {
            meterL1Btm.progress = sliderLGeq1.value/GEQ_SLIDER_MIN_VALUE;
            meterL1Top.progress = 0.0;
        } else {
            meterL1Top.progress = 0.0;
            meterL1Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderLGeq2.value != geqL2Value && ![viewController sendCommandExists:CMD_L_GEQ15_40HZ_DB dspId:DSP_5]) {
        sliderLGeq2.value = geqL2Value;
        if (sliderLGeq2.value > 0.0) {
            meterL2Top.progress = sliderLGeq2.value/GEQ_SLIDER_MAX_VALUE;
            meterL2Btm.progress = 0.0;
        } else if (sliderLGeq2.value < 0.0) {
            meterL2Btm.progress = sliderLGeq2.value/GEQ_SLIDER_MIN_VALUE;
            meterL2Top.progress = 0.0;
        } else {
            meterL2Top.progress = 0.0;
            meterL2Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderLGeq3.value != geqL3Value && ![viewController sendCommandExists:CMD_L_GEQ15_63HZ_DB dspId:DSP_5]) {
        sliderLGeq3.value = geqL3Value;
        if (sliderLGeq3.value > 0.0) {
            meterL3Top.progress = sliderLGeq3.value/GEQ_SLIDER_MAX_VALUE;
            meterL3Btm.progress = 0.0;
        } else if (sliderLGeq3.value < 0.0) {
            meterL3Btm.progress = sliderLGeq3.value/GEQ_SLIDER_MIN_VALUE;
            meterL3Top.progress = 0.0;
        } else {
            meterL3Top.progress = 0.0;
            meterL3Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderLGeq4.value != geqL4Value && ![viewController sendCommandExists:CMD_L_GEQ15_100HZ_DB dspId:DSP_5]) {
        sliderLGeq4.value = geqL4Value;
        if (sliderLGeq4.value > 0.0) {
            meterL4Top.progress = sliderLGeq4.value/GEQ_SLIDER_MAX_VALUE;
            meterL4Btm.progress = 0.0;
        } else if (sliderLGeq4.value < 0.0) {
            meterL4Btm.progress = sliderLGeq4.value/GEQ_SLIDER_MIN_VALUE;
            meterL4Top.progress = 0.0;
        } else {
            meterL4Top.progress = 0.0;
            meterL4Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderLGeq5.value != geqL5Value && ![viewController sendCommandExists:CMD_L_GEQ15_160HZ_DB dspId:DSP_5]) {
        sliderLGeq5.value = geqL5Value;
        if (sliderLGeq5.value > 0.0) {
            meterL5Top.progress = sliderLGeq5.value/GEQ_SLIDER_MAX_VALUE;
            meterL5Btm.progress = 0.0;
        } else if (sliderLGeq5.value < 0.0) {
            meterL5Btm.progress = sliderLGeq5.value/GEQ_SLIDER_MIN_VALUE;
            meterL5Top.progress = 0.0;
        } else {
            meterL5Top.progress = 0.0;
            meterL5Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderLGeq6.value != geqL6Value && ![viewController sendCommandExists:CMD_L_GEQ15_250HZ_DB dspId:DSP_5]) {
        sliderLGeq6.value = geqL6Value;
        if (sliderLGeq6.value > 0.0) {
            meterL6Top.progress = sliderLGeq6.value/GEQ_SLIDER_MAX_VALUE;
            meterL6Btm.progress = 0.0;
        } else if (sliderLGeq6.value < 0.0) {
            meterL6Btm.progress = sliderLGeq6.value/GEQ_SLIDER_MIN_VALUE;
            meterL6Top.progress = 0.0;
        } else {
            meterL6Top.progress = 0.0;
            meterL6Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderLGeq7.value != geqL7Value && ![viewController sendCommandExists:CMD_L_GEQ15_400HZ_DB dspId:DSP_5]) {
        sliderLGeq7.value = geqL7Value;
        if (sliderLGeq7.value > 0.0) {
            meterL7Top.progress = sliderLGeq7.value/GEQ_SLIDER_MAX_VALUE;
            meterL7Btm.progress = 0.0;
        } else if (sliderLGeq7.value < 0.0) {
            meterL7Btm.progress = sliderLGeq7.value/GEQ_SLIDER_MIN_VALUE;
            meterL7Top.progress = 0.0;
        } else {
            meterL7Top.progress = 0.0;
            meterL7Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderLGeq8.value != geqL8Value && ![viewController sendCommandExists:CMD_L_GEQ15_630HZ_DB dspId:DSP_5]) {
        sliderLGeq8.value = geqL8Value;
        if (sliderLGeq8.value > 0.0) {
            meterL8Top.progress = sliderLGeq8.value/GEQ_SLIDER_MAX_VALUE;
            meterL8Btm.progress = 0.0;
        } else if (sliderLGeq8.value < 0.0) {
            meterL8Btm.progress = sliderLGeq8.value/GEQ_SLIDER_MIN_VALUE;
            meterL8Top.progress = 0.0;
        } else {
            meterL8Top.progress = 0.0;
            meterL8Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderLGeq9.value != geqL9Value && ![viewController sendCommandExists:CMD_L_GEQ15_1KHZ_DB dspId:DSP_5]) {
        sliderLGeq9.value = geqL9Value;
        if (sliderLGeq9.value > 0.0) {
            meterL9Top.progress = sliderLGeq9.value/GEQ_SLIDER_MAX_VALUE;
            meterL9Btm.progress = 0.0;
        } else if (sliderLGeq9.value < 0.0) {
            meterL9Btm.progress = sliderLGeq9.value/GEQ_SLIDER_MIN_VALUE;
            meterL9Top.progress = 0.0;
        } else {
            meterL9Top.progress = 0.0;
            meterL9Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderLGeq10.value != geqL10Value && ![viewController sendCommandExists:CMD_L_GEQ15_1_6KHZ_DB dspId:DSP_5]) {
        sliderLGeq10.value = geqL10Value;
        if (sliderLGeq10.value > 0.0) {
            meterL10Top.progress = sliderLGeq10.value/GEQ_SLIDER_MAX_VALUE;
            meterL10Btm.progress = 0.0;
        } else if (sliderLGeq10.value < 0.0) {
            meterL10Btm.progress = sliderLGeq10.value/GEQ_SLIDER_MIN_VALUE;
            meterL10Top.progress = 0.0;
        } else {
            meterL10Top.progress = 0.0;
            meterL10Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderLGeq11.value != geqL11Value && ![viewController sendCommandExists:CMD_L_GEQ15_2_5KHZ_DB dspId:DSP_5]) {
        sliderLGeq11.value = geqL11Value;
        if (sliderLGeq11.value > 0.0) {
            meterL11Top.progress = sliderLGeq11.value/GEQ_SLIDER_MAX_VALUE;
            meterL11Btm.progress = 0.0;
        } else if (sliderLGeq11.value < 0.0) {
            meterL11Btm.progress = sliderLGeq11.value/GEQ_SLIDER_MIN_VALUE;
            meterL1Top.progress = 0.0;
        } else {
            meterL11Top.progress = 0.0;
            meterL11Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderLGeq12.value != geqL12Value && ![viewController sendCommandExists:CMD_L_GEQ15_4KHZ_DB dspId:DSP_5]) {
        sliderLGeq12.value = geqL12Value;
        if (sliderLGeq12.value > 0.0) {
            meterL12Top.progress = sliderLGeq12.value/GEQ_SLIDER_MAX_VALUE;
            meterL12Btm.progress = 0.0;
        } else if (sliderLGeq12.value < 0.0) {
            meterL12Btm.progress = sliderLGeq12.value/GEQ_SLIDER_MIN_VALUE;
            meterL12Top.progress = 0.0;
        } else {
            meterL12Top.progress = 0.0;
            meterL12Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderLGeq13.value != geqL13Value && ![viewController sendCommandExists:CMD_L_GEQ15_6_3KHZ_DB dspId:DSP_5]) {
        sliderLGeq13.value = geqL13Value;
        if (sliderLGeq13.value > 0.0) {
            meterL13Top.progress = sliderLGeq13.value/GEQ_SLIDER_MAX_VALUE;
            meterL13Btm.progress = 0.0;
        } else if (sliderLGeq13.value < 0.0) {
            meterL13Btm.progress = sliderLGeq13.value/GEQ_SLIDER_MIN_VALUE;
            meterL13Top.progress = 0.0;
        } else {
            meterL13Top.progress = 0.0;
            meterL13Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderLGeq14.value != geqL14Value && ![viewController sendCommandExists:CMD_L_GEQ15_10KHZ_DB dspId:DSP_5]) {
        sliderLGeq14.value = geqL14Value;
        if (sliderLGeq14.value > 0.0) {
            meterL14Top.progress = sliderLGeq14.value/GEQ_SLIDER_MAX_VALUE;
            meterL14Btm.progress = 0.0;
        } else if (sliderLGeq14.value < 0.0) {
            meterL14Btm.progress = sliderLGeq14.value/GEQ_SLIDER_MIN_VALUE;
            meterL14Top.progress = 0.0;
        } else {
            meterL14Top.progress = 0.0;
            meterL14Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderLGeq15.value != geqL15Value && ![viewController sendCommandExists:CMD_L_GEQ15_16KHZ_DB dspId:DSP_5]) {
        sliderLGeq15.value = geqL15Value;
        if (sliderLGeq15.value > 0.0) {
            meterL15Top.progress = sliderLGeq15.value/GEQ_SLIDER_MAX_VALUE;
            meterL15Btm.progress = 0.0;
        } else if (sliderLGeq15.value < 0.0) {
            meterL15Btm.progress = sliderLGeq15.value/GEQ_SLIDER_MIN_VALUE;
            meterL15Top.progress = 0.0;
        } else {
            meterL15Top.progress = 0.0;
            meterL15Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    
    int geqR1Value = pkt->r_geq15_25_hz_db;
    int geqR2Value = pkt->r_geq15_40_hz_db;
    int geqR3Value = pkt->r_geq15_63_hz_db;
    int geqR4Value = pkt->r_geq15_100_hz_db;
    int geqR5Value = pkt->r_geq15_160_hz_db;
    int geqR6Value = pkt->r_geq15_250_hz_db;
    int geqR7Value = pkt->r_geq15_400_hz_db;
    int geqR8Value = pkt->r_geq15_630_hz_db;
    int geqR9Value = pkt->r_geq15_1_khz_db;
    int geqR10Value = pkt->r_geq15_1_6_khz_db;
    int geqR11Value = pkt->r_geq15_2_5_khz_db;
    int geqR12Value = pkt->r_geq15_4_khz_db;
    int geqR13Value = pkt->r_geq15_6_3_khz_db;
    int geqR14Value = pkt->r_geq15_10_khz_db;
    int geqR15Value = pkt->r_geq15_16_khz_db;
    
    if (sliderRGeq1.value != geqR1Value && ![viewController sendCommandExists:CMD_R_GEQ15_25HZ_DB dspId:DSP_5]) {
        sliderRGeq1.value = geqR1Value;
        if (sliderRGeq1.value > 0.0) {
            meterR1Top.progress = sliderRGeq1.value/GEQ_SLIDER_MAX_VALUE;
            meterR1Btm.progress = 0.0;
        } else if (sliderRGeq1.value < 0.0) {
            meterR1Btm.progress = sliderRGeq1.value/GEQ_SLIDER_MIN_VALUE;
            meterR1Top.progress = 0.0;
        } else {
            meterR1Top.progress = 0.0;
            meterR1Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderRGeq2.value != geqR2Value && ![viewController sendCommandExists:CMD_R_GEQ15_40HZ_DB dspId:DSP_5]) {
        sliderRGeq2.value = geqR2Value;
        if (sliderRGeq2.value > 0.0) {
            meterR2Top.progress = sliderRGeq2.value/GEQ_SLIDER_MAX_VALUE;
            meterR2Btm.progress = 0.0;
        } else if (sliderRGeq2.value < 0.0) {
            meterR2Btm.progress = sliderRGeq2.value/GEQ_SLIDER_MIN_VALUE;
            meterR2Top.progress = 0.0;
        } else {
            meterR2Top.progress = 0.0;
            meterR2Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderRGeq3.value != geqR3Value && ![viewController sendCommandExists:CMD_R_GEQ15_63HZ_DB dspId:DSP_5]) {
        sliderRGeq3.value = geqR3Value;
        if (sliderRGeq3.value > 0.0) {
            meterR3Top.progress = sliderRGeq3.value/GEQ_SLIDER_MAX_VALUE;
            meterR3Btm.progress = 0.0;
        } else if (sliderRGeq3.value < 0.0) {
            meterR3Btm.progress = sliderRGeq3.value/GEQ_SLIDER_MIN_VALUE;
            meterR3Top.progress = 0.0;
        } else {
            meterR3Top.progress = 0.0;
            meterR3Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderRGeq4.value != geqR4Value && ![viewController sendCommandExists:CMD_R_GEQ15_100HZ_DB dspId:DSP_5]) {
        sliderRGeq4.value = geqR4Value;
        if (sliderRGeq4.value > 0.0) {
            meterR4Top.progress = sliderRGeq4.value/GEQ_SLIDER_MAX_VALUE;
            meterR4Btm.progress = 0.0;
        } else if (sliderRGeq4.value < 0.0) {
            meterR4Btm.progress = sliderRGeq4.value/GEQ_SLIDER_MIN_VALUE;
            meterR4Top.progress = 0.0;
        } else {
            meterR4Top.progress = 0.0;
            meterR4Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderRGeq5.value != geqR5Value && ![viewController sendCommandExists:CMD_R_GEQ15_160HZ_DB dspId:DSP_5]) {
        sliderRGeq5.value = geqR5Value;
        if (sliderRGeq5.value > 0.0) {
            meterR5Top.progress = sliderRGeq5.value/GEQ_SLIDER_MAX_VALUE;
            meterR5Btm.progress = 0.0;
        } else if (sliderRGeq5.value < 0.0) {
            meterR5Btm.progress = sliderRGeq5.value/GEQ_SLIDER_MIN_VALUE;
            meterR5Top.progress = 0.0;
        } else {
            meterR5Top.progress = 0.0;
            meterR5Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderRGeq6.value != geqR6Value && ![viewController sendCommandExists:CMD_R_GEQ15_250HZ_DB dspId:DSP_5]) {
        sliderRGeq6.value = geqR6Value;
        if (sliderRGeq6.value > 0.0) {
            meterR6Top.progress = sliderRGeq6.value/GEQ_SLIDER_MAX_VALUE;
            meterR6Btm.progress = 0.0;
        } else if (sliderRGeq6.value < 0.0) {
            meterR6Btm.progress = sliderRGeq6.value/GEQ_SLIDER_MIN_VALUE;
            meterR6Top.progress = 0.0;
        } else {
            meterR6Top.progress = 0.0;
            meterR6Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderRGeq7.value != geqR7Value && ![viewController sendCommandExists:CMD_R_GEQ15_400HZ_DB dspId:DSP_5]) {
        sliderRGeq7.value = geqR7Value;
        if (sliderRGeq7.value > 0.0) {
            meterR7Top.progress = sliderRGeq7.value/GEQ_SLIDER_MAX_VALUE;
            meterR7Btm.progress = 0.0;
        } else if (sliderRGeq7.value < 0.0) {
            meterR7Btm.progress = sliderRGeq7.value/GEQ_SLIDER_MIN_VALUE;
            meterR7Top.progress = 0.0;
        } else {
            meterR7Top.progress = 0.0;
            meterR7Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderRGeq8.value != geqR8Value && ![viewController sendCommandExists:CMD_R_GEQ15_630HZ_DB dspId:DSP_5]) {
        sliderRGeq8.value = geqR8Value;
        if (sliderRGeq8.value > 0.0) {
            meterR8Top.progress = sliderRGeq8.value/GEQ_SLIDER_MAX_VALUE;
            meterR8Btm.progress = 0.0;
        } else if (sliderRGeq8.value < 0.0) {
            meterR8Btm.progress = sliderRGeq8.value/GEQ_SLIDER_MIN_VALUE;
            meterR8Top.progress = 0.0;
        } else {
            meterR8Top.progress = 0.0;
            meterR8Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderRGeq9.value != geqR9Value && ![viewController sendCommandExists:CMD_R_GEQ15_1KHZ_DB dspId:DSP_5]) {
        sliderRGeq9.value = geqR9Value;
        if (sliderRGeq9.value > 0.0) {
            meterR9Top.progress = sliderRGeq9.value/GEQ_SLIDER_MAX_VALUE;
            meterR9Btm.progress = 0.0;
        } else if (sliderRGeq9.value < 0.0) {
            meterR9Btm.progress = sliderRGeq9.value/GEQ_SLIDER_MIN_VALUE;
            meterR9Top.progress = 0.0;
        } else {
            meterR9Top.progress = 0.0;
            meterR9Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderRGeq10.value != geqR10Value && ![viewController sendCommandExists:CMD_R_GEQ15_1_6KHZ_DB dspId:DSP_5]) {
        sliderRGeq10.value = geqR10Value;
        if (sliderRGeq10.value > 0.0) {
            meterR10Top.progress = sliderRGeq10.value/GEQ_SLIDER_MAX_VALUE;
            meterR10Btm.progress = 0.0;
        } else if (sliderRGeq10.value < 0.0) {
            meterR10Btm.progress = sliderRGeq10.value/GEQ_SLIDER_MIN_VALUE;
            meterR10Top.progress = 0.0;
        } else {
            meterR10Top.progress = 0.0;
            meterR10Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderRGeq11.value != geqR11Value && ![viewController sendCommandExists:CMD_R_GEQ15_2_5KHZ_DB dspId:DSP_5]) {
        sliderRGeq11.value = geqR11Value;
        if (sliderRGeq11.value > 0.0) {
            meterR11Top.progress = sliderRGeq11.value/GEQ_SLIDER_MAX_VALUE;
            meterR11Btm.progress = 0.0;
        } else if (sliderRGeq11.value < 0.0) {
            meterR11Btm.progress = sliderRGeq11.value/GEQ_SLIDER_MIN_VALUE;
            meterR1Top.progress = 0.0;
        } else {
            meterR11Top.progress = 0.0;
            meterR11Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderRGeq12.value != geqR12Value && ![viewController sendCommandExists:CMD_R_GEQ15_4KHZ_DB dspId:DSP_5]) {
        sliderRGeq12.value = geqR12Value;
        if (sliderRGeq12.value > 0.0) {
            meterR12Top.progress = sliderRGeq12.value/GEQ_SLIDER_MAX_VALUE;
            meterR12Btm.progress = 0.0;
        } else if (sliderRGeq12.value < 0.0) {
            meterR12Btm.progress = sliderRGeq12.value/GEQ_SLIDER_MIN_VALUE;
            meterR12Top.progress = 0.0;
        } else {
            meterR12Top.progress = 0.0;
            meterR12Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderRGeq13.value != geqR13Value && ![viewController sendCommandExists:CMD_R_GEQ15_6_3KHZ_DB dspId:DSP_5]) {
        sliderRGeq13.value = geqR13Value;
        if (sliderRGeq13.value > 0.0) {
            meterR13Top.progress = sliderRGeq13.value/GEQ_SLIDER_MAX_VALUE;
            meterR13Btm.progress = 0.0;
        } else if (sliderRGeq13.value < 0.0) {
            meterR13Btm.progress = sliderRGeq13.value/GEQ_SLIDER_MIN_VALUE;
            meterR13Top.progress = 0.0;
        } else {
            meterR13Top.progress = 0.0;
            meterR13Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderRGeq14.value != geqR14Value && ![viewController sendCommandExists:CMD_R_GEQ15_10KHZ_DB dspId:DSP_5]) {
        sliderRGeq14.value = geqR14Value;
        if (sliderRGeq14.value > 0.0) {
            meterR14Top.progress = sliderRGeq14.value/GEQ_SLIDER_MAX_VALUE;
            meterR14Btm.progress = 0.0;
        } else if (sliderRGeq14.value < 0.0) {
            meterR14Btm.progress = sliderRGeq14.value/GEQ_SLIDER_MIN_VALUE;
            meterR14Top.progress = 0.0;
        } else {
            meterR14Top.progress = 0.0;
            meterR14Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    if (sliderRGeq15.value != geqR15Value && ![viewController sendCommandExists:CMD_R_GEQ15_16KHZ_DB dspId:DSP_5]) {
        sliderRGeq15.value = geqR15Value;
        if (sliderRGeq15.value > 0.0) {
            meterR15Top.progress = sliderRGeq15.value/GEQ_SLIDER_MAX_VALUE;
            meterR15Btm.progress = 0.0;
        } else if (sliderRGeq15.value < 0.0) {
            meterR15Btm.progress = sliderRGeq15.value/GEQ_SLIDER_MIN_VALUE;
            meterR15Top.progress = 0.0;
        } else {
            meterR15Top.progress = 0.0;
            meterR15Btm.progress = 0.0;
        }
        needsUpdate = YES;
    }
    
    if (needsUpdate) {
        [self updatePreviewImage];
    }
}

- (void)setGeqSoloOnOff:(BOOL)value {
    if (value) {
        
    } else {
        
    }
}

- (void)setGeqOnOff:(BOOL)value {
    geqOnOff = value;
    [self updateGeqMeters];
}

/*
- (void)setLinkMode:(BOOL)value {
    geqLinkMode = value;
    btnGeqLink.selected = value;
}
*/

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
}

// Convert touch point to slider value
- (float)pointToSliderValue:(CGPoint)point
{
    return (12.0 - (12.0/108.0) * point.y);
}

- (void)touchesBegan:(NSSet*)touches withEvent:(UIEvent*)event
{
    /*
    UITouch *touch = [touches anyObject];
    if (!viewGeq.isHidden && btnGeqDraw.selected) {
        CGPoint startPoint = [touch locationInView:self.viewGeqDraw];
        NSLog(@"Start point = (%.f, %.f)", startPoint.x, startPoint.y);
    }
    */
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    geqLock = YES;
    
    UITouch *touch = [touches anyObject];
    
    // Handle GEQ draw
    if (btnGeqDraw.selected) {
        CGPoint touchPoint = [touch locationInView:self.viewGeqDrawL];
        //NSLog(@"Touch point L = (%.f, %.f)", touchPoint.x, touchPoint.y);
        
        if (CGRectContainsPoint(sliderLGeq1.frame, touchPoint)) {
            sliderLGeq1.value = [self pointToSliderValue:touchPoint];
            [self onSlider1Action:sliderLGeq1];
        } else if (CGRectContainsPoint(sliderLGeq2.frame, touchPoint)) {
            sliderLGeq2.value = [self pointToSliderValue:touchPoint];
            [self onSlider2Action:sliderLGeq2];
        } else if (CGRectContainsPoint(sliderLGeq3.frame, touchPoint)) {
            sliderLGeq3.value = [self pointToSliderValue:touchPoint];
            [self onSlider3Action:sliderLGeq3];
        } else if (CGRectContainsPoint(sliderLGeq4.frame, touchPoint)) {
            sliderLGeq4.value = [self pointToSliderValue:touchPoint];
            [self onSlider4Action:sliderLGeq4];
        } else if (CGRectContainsPoint(sliderLGeq5.frame, touchPoint)) {
            sliderLGeq5.value = [self pointToSliderValue:touchPoint];
            [self onSlider5Action:sliderLGeq5];
        } else if (CGRectContainsPoint(sliderLGeq6.frame, touchPoint)) {
            sliderLGeq6.value = [self pointToSliderValue:touchPoint];
            [self onSlider6Action:sliderLGeq6];
        } else if (CGRectContainsPoint(sliderLGeq7.frame, touchPoint)) {
            sliderLGeq7.value = [self pointToSliderValue:touchPoint];
            [self onSlider7Action:sliderLGeq7];
        } else if (CGRectContainsPoint(sliderLGeq8.frame, touchPoint)) {
            sliderLGeq8.value = [self pointToSliderValue:touchPoint];
            [self onSlider8Action:sliderLGeq8];
        } else if (CGRectContainsPoint(sliderLGeq9.frame, touchPoint)) {
            sliderLGeq9.value = [self pointToSliderValue:touchPoint];
            [self onSlider9Action:sliderLGeq9];
        } else if (CGRectContainsPoint(sliderLGeq10.frame, touchPoint)) {
            sliderLGeq10.value = [self pointToSliderValue:touchPoint];
            [self onSlider10Action:sliderLGeq10];
        } else if (CGRectContainsPoint(sliderLGeq11.frame, touchPoint)) {
            sliderLGeq11.value = [self pointToSliderValue:touchPoint];
            [self onSlider11Action:sliderLGeq11];
        } else if (CGRectContainsPoint(sliderLGeq12.frame, touchPoint)) {
            sliderLGeq12.value = [self pointToSliderValue:touchPoint];
            [self onSlider12Action:sliderLGeq12];
        } else if (CGRectContainsPoint(sliderLGeq13.frame, touchPoint)) {
            sliderLGeq13.value = [self pointToSliderValue:touchPoint];
            [self onSlider13Action:sliderLGeq13];
        } else if (CGRectContainsPoint(sliderLGeq14.frame, touchPoint)) {
            sliderLGeq14.value = [self pointToSliderValue:touchPoint];
            [self onSlider14Action:sliderLGeq14];
        } else if (CGRectContainsPoint(sliderLGeq15.frame, touchPoint)) {
            sliderLGeq15.value = [self pointToSliderValue:touchPoint];
            [self onSlider15Action:sliderLGeq15];
        }
        
        touchPoint = [touch locationInView:self.viewGeqDrawR];
        //NSLog(@"Touch point R = (%.f, %.f)", touchPoint.x, touchPoint.y);
        
        if (CGRectContainsPoint(sliderRGeq1.frame, touchPoint)) {
            sliderRGeq1.value = [self pointToSliderValue:touchPoint];
            [self onSlider1Action:sliderRGeq1];
        } else if (CGRectContainsPoint(sliderRGeq2.frame, touchPoint)) {
            sliderRGeq2.value = [self pointToSliderValue:touchPoint];
            [self onSlider2Action:sliderRGeq2];
        } else if (CGRectContainsPoint(sliderRGeq3.frame, touchPoint)) {
            sliderRGeq3.value = [self pointToSliderValue:touchPoint];
            [self onSlider3Action:sliderRGeq3];
        } else if (CGRectContainsPoint(sliderRGeq4.frame, touchPoint)) {
            sliderRGeq4.value = [self pointToSliderValue:touchPoint];
            [self onSlider4Action:sliderRGeq4];
        } else if (CGRectContainsPoint(sliderRGeq5.frame, touchPoint)) {
            sliderRGeq5.value = [self pointToSliderValue:touchPoint];
            [self onSlider5Action:sliderRGeq5];
        } else if (CGRectContainsPoint(sliderRGeq6.frame, touchPoint)) {
            sliderRGeq6.value = [self pointToSliderValue:touchPoint];
            [self onSlider6Action:sliderRGeq6];
        } else if (CGRectContainsPoint(sliderRGeq7.frame, touchPoint)) {
            sliderRGeq7.value = [self pointToSliderValue:touchPoint];
            [self onSlider7Action:sliderRGeq7];
        } else if (CGRectContainsPoint(sliderRGeq8.frame, touchPoint)) {
            sliderRGeq8.value = [self pointToSliderValue:touchPoint];
            [self onSlider8Action:sliderRGeq8];
        } else if (CGRectContainsPoint(sliderRGeq9.frame, touchPoint)) {
            sliderRGeq9.value = [self pointToSliderValue:touchPoint];
            [self onSlider9Action:sliderRGeq9];
        } else if (CGRectContainsPoint(sliderRGeq10.frame, touchPoint)) {
            sliderRGeq10.value = [self pointToSliderValue:touchPoint];
            [self onSlider10Action:sliderRGeq10];
        } else if (CGRectContainsPoint(sliderRGeq11.frame, touchPoint)) {
            sliderRGeq11.value = [self pointToSliderValue:touchPoint];
            [self onSlider11Action:sliderRGeq11];
        } else if (CGRectContainsPoint(sliderRGeq12.frame, touchPoint)) {
            sliderRGeq12.value = [self pointToSliderValue:touchPoint];
            [self onSlider12Action:sliderRGeq12];
        } else if (CGRectContainsPoint(sliderRGeq13.frame, touchPoint)) {
            sliderRGeq13.value = [self pointToSliderValue:touchPoint];
            [self onSlider13Action:sliderRGeq13];
        } else if (CGRectContainsPoint(sliderRGeq14.frame, touchPoint)) {
            sliderRGeq14.value = [self pointToSliderValue:touchPoint];
            [self onSlider14Action:sliderRGeq14];
        } else if (CGRectContainsPoint(sliderRGeq15.frame, touchPoint)) {
            sliderRGeq15.value = [self pointToSliderValue:touchPoint];
            [self onSlider15Action:sliderRGeq15];
        } 
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    /*
    UITouch *touch = [touches anyObject];
    if (!viewGeq.isHidden && btnGeqDraw.selected) {
        CGPoint endPoint = [touch locationInView:self.viewGeqDraw];
        NSLog(@"End point = (%.f, %.f)", endPoint.x, endPoint.y);
    }
    */
    geqLock = NO;
}

// Update image preview
- (void)updatePreviewImage
{
    if (!initDone) {
        NSLog(@"updatePreviewImage: Init not done!");
        return;
    }
    
    UIImage *finalImage;
    
    // GEQ L
    UIGraphicsBeginImageContext(viewGeqFrameL.frame.size);
    
    [viewGeqFrameL.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    finalImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    if (finalImage != nil) {
        imgEqL = finalImage;
    } else {
        NSLog(@"updatePreviewImage: NULL preview image!");
    }
    
    // GEQ R
    UIGraphicsBeginImageContext(viewGeqFrameR.frame.size);
    
    [viewGeqFrameR.layer renderInContext:UIGraphicsGetCurrentContext()];
    
    finalImage = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    if (finalImage != nil) {
        imgEqR = finalImage;
    } else {
        NSLog(@"updatePreviewImage: NULL preview image!");
    }
    
    // Update delegate image
    [self.delegate didFinishEditingGeq15];
}

#pragma mark -
#pragma mark Main event handler methods

- (IBAction)onBtnFile:(id)sender {
    [viewController setFileSceneMode:SCENE_MODE_EFFECT_2 sceneChannel:0];
    [viewController.view bringSubviewToFront:viewController.viewScenes];
    [viewController.viewScenes setHidden:NO];
}

- (IBAction)onBtnBack:(id)sender {
    [self.view setHidden:YES];
}

- (IBAction)onBtnReset:(id)sender {
    geqLock = YES;
    
    sliderLGeq1.value = 0; sliderLGeq2.value = 0; sliderLGeq3.value = 0;
    sliderLGeq4.value = 0; sliderLGeq5.value = 0; sliderLGeq6.value = 0;
    sliderLGeq7.value = 0; sliderLGeq8.value = 0; sliderLGeq9.value = 0;
    sliderLGeq10.value = 0; sliderLGeq11.value = 0; sliderLGeq12.value = 0;
    sliderLGeq13.value = 0; sliderLGeq14.value = 0; sliderLGeq15.value = 0;
    
    [self onSlider1Action:sliderLGeq1];     [self onSlider2Action:sliderLGeq2];
    [self onSlider3Action:sliderLGeq3];     [self onSlider4Action:sliderLGeq4];
    [self onSlider5Action:sliderLGeq5];     [self onSlider6Action:sliderLGeq6];
    [self onSlider7Action:sliderLGeq7];     [self onSlider8Action:sliderLGeq8];
    [self onSlider9Action:sliderLGeq9];     [self onSlider10Action:sliderLGeq10];
    [self onSlider11Action:sliderLGeq11];     [self onSlider12Action:sliderLGeq12];
    [self onSlider13Action:sliderLGeq13];     [self onSlider14Action:sliderLGeq14];
    [self onSlider15Action:sliderLGeq15];
    
    sliderRGeq1.value = 0; sliderRGeq2.value = 0; sliderRGeq3.value = 0;
    sliderRGeq4.value = 0; sliderRGeq5.value = 0; sliderRGeq6.value = 0;
    sliderRGeq7.value = 0; sliderRGeq8.value = 0; sliderRGeq9.value = 0;
    sliderRGeq10.value = 0; sliderRGeq11.value = 0; sliderRGeq12.value = 0;
    sliderRGeq13.value = 0; sliderRGeq14.value = 0; sliderRGeq15.value = 0;
    
    [self onSlider1Action:sliderRGeq1];     [self onSlider2Action:sliderRGeq2];
    [self onSlider3Action:sliderRGeq3];     [self onSlider4Action:sliderRGeq4];
    [self onSlider5Action:sliderRGeq5];     [self onSlider6Action:sliderRGeq6];
    [self onSlider7Action:sliderRGeq7];     [self onSlider8Action:sliderRGeq8];
    [self onSlider9Action:sliderRGeq9];     [self onSlider10Action:sliderRGeq10];
    [self onSlider11Action:sliderRGeq11];     [self onSlider12Action:sliderRGeq12];
    [self onSlider13Action:sliderRGeq13];     [self onSlider14Action:sliderRGeq14];
    [self onSlider15Action:sliderRGeq15];     
    [self updatePreviewImage];
    
    geqLock = NO;
}

- (IBAction)onBtnDraw:(id)sender {
    btnGeqDraw.selected = !btnGeqDraw.selected;
    if (btnGeqDraw.selected) {
        [self.view bringSubviewToFront:self.viewGeqDrawL];
        [self.view bringSubviewToFront:self.viewGeqDrawR];
    } else {
        [self.view sendSubviewToBack:self.viewGeqDrawL];
        [self.view sendSubviewToBack:self.viewGeqDrawR];
    }
}

#pragma mark -
#pragma mark GEQ event handler methods

- (IBAction)onBtnGeqLink:(id)sender {
    btnGeqLink.selected = !btnGeqLink.selected;
    
    // Sync right channel to left
    if (btnGeqLink.selected) {
        sliderRGeq1.value = sliderLGeq1.value;
        sliderRGeq2.value = sliderLGeq2.value;
        sliderRGeq3.value = sliderLGeq3.value;
        sliderRGeq4.value = sliderLGeq4.value;
        sliderRGeq5.value = sliderLGeq5.value;
        sliderRGeq6.value = sliderLGeq6.value;
        sliderRGeq7.value = sliderLGeq7.value;
        sliderRGeq8.value = sliderLGeq8.value;
        sliderRGeq9.value = sliderLGeq9.value;
        sliderRGeq10.value = sliderLGeq10.value;
        sliderRGeq11.value = sliderLGeq11.value;
        sliderRGeq12.value = sliderLGeq12.value;
        sliderRGeq13.value = sliderLGeq13.value;
        sliderRGeq14.value = sliderLGeq14.value;
        sliderRGeq15.value = sliderLGeq15.value;
        [self onSlider1Action:sliderRGeq1];
        [self onSlider2Action:sliderRGeq2];
        [self onSlider3Action:sliderRGeq3];
        [self onSlider4Action:sliderRGeq4];
        [self onSlider5Action:sliderRGeq5];
        [self onSlider6Action:sliderRGeq6];
        [self onSlider7Action:sliderRGeq7];
        [self onSlider8Action:sliderRGeq8];
        [self onSlider9Action:sliderRGeq9];
        [self onSlider10Action:sliderRGeq10];
        [self onSlider11Action:sliderRGeq11];
        [self onSlider12Action:sliderRGeq12];
        [self onSlider13Action:sliderRGeq13];
        [self onSlider14Action:sliderRGeq14];
        [self onSlider15Action:sliderRGeq15];
    }
    
    geqLinkMode = btnGeqLink.selected;
    [viewController sendData2:SET_GEQ_LINK_MODE value1:1 value2:(int)btnGeqLink.selected];
}

- (void)updateGeqMeters {
    // Update meter images
    UIImage *progressImage;
    if (geqOnOff) {
        progressImage = [[UIImage imageNamed:@"bar-2.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0,0,0,0)];
    } else {
        progressImage = [[UIImage imageNamed:@"bar-4.png"] resizableImageWithCapInsets:UIEdgeInsetsMake(0,0,0,0)];
    }

    [meterL1Top setProgressImage:progressImage];
    [meterL1Btm setProgressImage:progressImage];
    [meterL2Top setProgressImage:progressImage];
    [meterL2Btm setProgressImage:progressImage];
    [meterL3Top setProgressImage:progressImage];
    [meterL3Btm setProgressImage:progressImage];
    [meterL4Top setProgressImage:progressImage];
    [meterL4Btm setProgressImage:progressImage];
    [meterL5Top setProgressImage:progressImage];
    [meterL5Btm setProgressImage:progressImage];
    [meterL6Top setProgressImage:progressImage];
    [meterL6Btm setProgressImage:progressImage];
    [meterL7Top setProgressImage:progressImage];
    [meterL7Btm setProgressImage:progressImage];
    [meterL8Top setProgressImage:progressImage];
    [meterL8Btm setProgressImage:progressImage];
    [meterL9Top setProgressImage:progressImage];
    [meterL9Btm setProgressImage:progressImage];
    [meterL10Top setProgressImage:progressImage];
    [meterL10Btm setProgressImage:progressImage];
    [meterL11Top setProgressImage:progressImage];
    [meterL11Btm setProgressImage:progressImage];
    [meterL12Top setProgressImage:progressImage];
    [meterL12Btm setProgressImage:progressImage];
    [meterL13Top setProgressImage:progressImage];
    [meterL13Btm setProgressImage:progressImage];
    [meterL14Top setProgressImage:progressImage];
    [meterL14Btm setProgressImage:progressImage];
    [meterL15Top setProgressImage:progressImage];
    [meterL15Btm setProgressImage:progressImage];
    
    [meterR1Top setProgressImage:progressImage];
    [meterR1Btm setProgressImage:progressImage];
    [meterR2Top setProgressImage:progressImage];
    [meterR2Btm setProgressImage:progressImage];
    [meterR3Top setProgressImage:progressImage];
    [meterR3Btm setProgressImage:progressImage];
    [meterR4Top setProgressImage:progressImage];
    [meterR4Btm setProgressImage:progressImage];
    [meterR5Top setProgressImage:progressImage];
    [meterR5Btm setProgressImage:progressImage];
    [meterR6Top setProgressImage:progressImage];
    [meterR6Btm setProgressImage:progressImage];
    [meterR7Top setProgressImage:progressImage];
    [meterR7Btm setProgressImage:progressImage];
    [meterR8Top setProgressImage:progressImage];
    [meterR8Btm setProgressImage:progressImage];
    [meterR9Top setProgressImage:progressImage];
    [meterR9Btm setProgressImage:progressImage];
    [meterR10Top setProgressImage:progressImage];
    [meterR10Btm setProgressImage:progressImage];
    [meterR11Top setProgressImage:progressImage];
    [meterR11Btm setProgressImage:progressImage];
    [meterR12Top setProgressImage:progressImage];
    [meterR12Btm setProgressImage:progressImage];
    [meterR13Top setProgressImage:progressImage];
    [meterR13Btm setProgressImage:progressImage];
    [meterR14Top setProgressImage:progressImage];
    [meterR14Btm setProgressImage:progressImage];
    [meterR15Top setProgressImage:progressImage];
    [meterR15Btm setProgressImage:progressImage];
    
    [self updatePreviewImage];
}

#pragma mark -
#pragma mark Slider event handler methods

- (IBAction)onSlider1Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider1End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider1Action:(id) sender {
    if (sender == sliderLGeq1) {
        if (sliderLGeq1.value > 0.0) {
            meterL1Top.progress = sliderLGeq1.value/GEQ_SLIDER_MAX_VALUE;
            meterL1Btm.progress = 0.0;
        } else if (sliderLGeq1.value < 0.0) {
            meterL1Btm.progress = sliderLGeq1.value/GEQ_SLIDER_MIN_VALUE;
            meterL1Top.progress = 0.0;
        } else {
            meterL1Top.progress = 0.0;
            meterL1Btm.progress = 0.0;
        }
        [viewController sendData:CMD_L_GEQ15_25HZ_DB commandType:DSP_WRITE dspId:DSP_5 value:(int)(sliderLGeq1.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderRGeq1.value = sliderLGeq1.value;
            if (sliderRGeq1.value > 0.0) {
                meterR1Top.progress = sliderRGeq1.value/GEQ_SLIDER_MAX_VALUE;
                meterR1Btm.progress = 0.0;
            } else if (sliderRGeq1.value < 0.0) {
                meterR1Btm.progress = sliderRGeq1.value/GEQ_SLIDER_MIN_VALUE;
                meterR1Top.progress = 0.0;
            } else {
                meterR1Top.progress = 0.0;
                meterR1Btm.progress = 0.0;
            }
            [viewController sendData:CMD_R_GEQ15_25HZ_DB commandType:DSP_WRITE dspId:DSP_5 value:(int)(sliderRGeq1.value)];
        }
        [self updatePreviewImage];
    } else if (sender == sliderRGeq1) {
        if (sliderRGeq1.value > 0.0) {
            meterR1Top.progress = sliderRGeq1.value/GEQ_SLIDER_MAX_VALUE;
            meterR1Btm.progress = 0.0;
        } else if (sliderRGeq1.value < 0.0) {
            meterR1Btm.progress = sliderRGeq1.value/GEQ_SLIDER_MIN_VALUE;
            meterR1Top.progress = 0.0;
        } else {
            meterR1Top.progress = 0.0;
            meterR1Btm.progress = 0.0;
        }
        [viewController sendData:CMD_R_GEQ15_25HZ_DB commandType:DSP_WRITE dspId:DSP_5 value:(int)(sliderRGeq1.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderLGeq1.value = sliderRGeq1.value;
            if (sliderLGeq1.value > 0.0) {
                meterL1Top.progress = sliderLGeq1.value/GEQ_SLIDER_MAX_VALUE;
                meterL1Btm.progress = 0.0;
            } else if (sliderLGeq1.value < 0.0) {
                meterL1Btm.progress = sliderLGeq1.value/GEQ_SLIDER_MIN_VALUE;
                meterL1Top.progress = 0.0;
            } else {
                meterL1Top.progress = 0.0;
                meterL1Btm.progress = 0.0;
            }
            [viewController sendData:CMD_L_GEQ15_25HZ_DB commandType:DSP_WRITE dspId:DSP_5 value:(int)(sliderLGeq1.value)];
        }
        [self updatePreviewImage];
    }
}

- (IBAction)onSlider2Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider2End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider2Action:(id) sender {
    if (sender == sliderLGeq2) {
        if (sliderLGeq2.value > 0.0) {
            meterL2Top.progress = sliderLGeq2.value/GEQ_SLIDER_MAX_VALUE;
            meterL2Btm.progress = 0.0;
        } else if (sliderLGeq2.value < 0.0) {
            meterL2Btm.progress = sliderLGeq2.value/GEQ_SLIDER_MIN_VALUE;
            meterL2Top.progress = 0.0;
        } else {
            meterL2Top.progress = 0.0;
            meterL2Btm.progress = 0.0;
        }
        [viewController sendData:CMD_L_GEQ15_40HZ_DB commandType:DSP_WRITE dspId:DSP_5 value:(int)(sliderLGeq2.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderRGeq2.value = sliderLGeq2.value;
            if (sliderRGeq2.value > 0.0) {
                meterR2Top.progress = sliderRGeq2.value/GEQ_SLIDER_MAX_VALUE;
                meterR2Btm.progress = 0.0;
            } else if (sliderRGeq2.value < 0.0) {
                meterR2Btm.progress = sliderRGeq2.value/GEQ_SLIDER_MIN_VALUE;
                meterR2Top.progress = 0.0;
            } else {
                meterR2Top.progress = 0.0;
                meterR2Btm.progress = 0.0;
            }
            [viewController sendData:CMD_R_GEQ15_40HZ_DB commandType:DSP_WRITE dspId:DSP_5 value:(int)(sliderRGeq2.value)];
        }
        [self updatePreviewImage];
    } else if (sender == sliderRGeq2) {
        if (sliderRGeq2.value > 0.0) {
            meterR2Top.progress = sliderRGeq2.value/GEQ_SLIDER_MAX_VALUE;
            meterR2Btm.progress = 0.0;
        } else if (sliderRGeq2.value < 0.0) {
            meterR2Btm.progress = sliderRGeq2.value/GEQ_SLIDER_MIN_VALUE;
            meterR2Top.progress = 0.0;
        } else {
            meterR2Top.progress = 0.0;
            meterR2Btm.progress = 0.0;
        }
        [viewController sendData:CMD_R_GEQ15_40HZ_DB commandType:DSP_WRITE dspId:DSP_5 value:(int)(sliderRGeq2.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderLGeq2.value = sliderRGeq2.value;
            if (sliderLGeq2.value > 0.0) {
                meterL2Top.progress = sliderLGeq2.value/GEQ_SLIDER_MAX_VALUE;
                meterL2Btm.progress = 0.0;
            } else if (sliderLGeq2.value < 0.0) {
                meterL2Btm.progress = sliderLGeq2.value/GEQ_SLIDER_MIN_VALUE;
                meterL2Top.progress = 0.0;
            } else {
                meterL2Top.progress = 0.0;
                meterL2Btm.progress = 0.0;
            }
            [viewController sendData:CMD_L_GEQ15_40HZ_DB commandType:DSP_WRITE dspId:DSP_5 value:(int)(sliderLGeq2.value)];
        }
        [self updatePreviewImage];
    }
}

- (IBAction)onSlider3Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider3End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider3Action:(id) sender {
    if (sender == sliderLGeq3) {
        if (sliderLGeq3.value > 0.0) {
            meterL3Top.progress = sliderLGeq3.value/GEQ_SLIDER_MAX_VALUE;
            meterL3Btm.progress = 0.0;
        } else if (sliderLGeq3.value < 0.0) {
            meterL3Btm.progress = sliderLGeq3.value/GEQ_SLIDER_MIN_VALUE;
            meterL3Top.progress = 0.0;
        } else {
            meterL3Top.progress = 0.0;
            meterL3Btm.progress = 0.0;
        }
        [viewController sendData:CMD_L_GEQ15_63HZ_DB commandType:DSP_WRITE dspId:DSP_5 value:(int)(sliderLGeq3.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderRGeq3.value = sliderLGeq3.value;
            if (sliderRGeq3.value > 0.0) {
                meterR3Top.progress = sliderRGeq3.value/GEQ_SLIDER_MAX_VALUE;
                meterR3Btm.progress = 0.0;
            } else if (sliderRGeq3.value < 0.0) {
                meterR3Btm.progress = sliderRGeq3.value/GEQ_SLIDER_MIN_VALUE;
                meterR3Top.progress = 0.0;
            } else {
                meterR3Top.progress = 0.0;
                meterR3Btm.progress = 0.0;
            }
            [viewController sendData:CMD_R_GEQ15_63HZ_DB commandType:DSP_WRITE dspId:DSP_5 value:(int)(sliderRGeq3.value)];
        }
        [self updatePreviewImage];
    } else if (sender == sliderRGeq3) {
        if (sliderRGeq3.value > 0.0) {
            meterR3Top.progress = sliderRGeq3.value/GEQ_SLIDER_MAX_VALUE;
            meterR3Btm.progress = 0.0;
        } else if (sliderRGeq3.value < 0.0) {
            meterR3Btm.progress = sliderRGeq3.value/GEQ_SLIDER_MIN_VALUE;
            meterR3Top.progress = 0.0;
        } else {
            meterR3Top.progress = 0.0;
            meterR3Btm.progress = 0.0;
        }
        [viewController sendData:CMD_R_GEQ15_63HZ_DB commandType:DSP_WRITE dspId:DSP_5 value:(int)(sliderRGeq3.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderLGeq3.value = sliderRGeq3.value;
            if (sliderLGeq3.value > 0.0) {
                meterL3Top.progress = sliderLGeq3.value/GEQ_SLIDER_MAX_VALUE;
                meterL3Btm.progress = 0.0;
            } else if (sliderLGeq3.value < 0.0) {
                meterL3Btm.progress = sliderLGeq3.value/GEQ_SLIDER_MIN_VALUE;
                meterL3Top.progress = 0.0;
            } else {
                meterL3Top.progress = 0.0;
                meterL3Btm.progress = 0.0;
            }
            [viewController sendData:CMD_L_GEQ15_63HZ_DB commandType:DSP_WRITE dspId:DSP_5 value:(int)(sliderLGeq3.value)];
        }
        [self updatePreviewImage];
    }
}

- (IBAction)onSlider4Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider4End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider4Action:(id) sender {
    if (sender == sliderLGeq4) {
        if (sliderLGeq4.value > 0.0) {
            meterL4Top.progress = sliderLGeq4.value/GEQ_SLIDER_MAX_VALUE;
            meterL4Btm.progress = 0.0;
        } else if (sliderLGeq4.value < 0.0) {
            meterL4Btm.progress = sliderLGeq4.value/GEQ_SLIDER_MIN_VALUE;
            meterL4Top.progress = 0.0;
        } else {
            meterL4Top.progress = 0.0;
            meterL4Btm.progress = 0.0;
        }
        [viewController sendData:CMD_L_GEQ15_100HZ_DB commandType:DSP_WRITE dspId:DSP_5 value:(int)(sliderLGeq4.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderRGeq4.value = sliderLGeq4.value;
            if (sliderRGeq4.value > 0.0) {
                meterR4Top.progress = sliderRGeq4.value/GEQ_SLIDER_MAX_VALUE;
                meterR4Btm.progress = 0.0;
            } else if (sliderRGeq4.value < 0.0) {
                meterR4Btm.progress = sliderRGeq4.value/GEQ_SLIDER_MIN_VALUE;
                meterR4Top.progress = 0.0;
            } else {
                meterR4Top.progress = 0.0;
                meterR4Btm.progress = 0.0;
            }
            [viewController sendData:CMD_R_GEQ15_100HZ_DB commandType:DSP_WRITE dspId:DSP_5 value:(int)(sliderRGeq4.value)];
        }
        [self updatePreviewImage];
    } else if (sender == sliderRGeq4) {
        if (sliderRGeq4.value > 0.0) {
            meterR4Top.progress = sliderRGeq4.value/GEQ_SLIDER_MAX_VALUE;
            meterR4Btm.progress = 0.0;
        } else if (sliderRGeq4.value < 0.0) {
            meterR4Btm.progress = sliderRGeq4.value/GEQ_SLIDER_MIN_VALUE;
            meterR4Top.progress = 0.0;
        } else {
            meterR4Top.progress = 0.0;
            meterR4Btm.progress = 0.0;
        }
        [viewController sendData:CMD_R_GEQ15_100HZ_DB commandType:DSP_WRITE dspId:DSP_5 value:(int)(sliderRGeq4.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderLGeq4.value = sliderRGeq4.value;
            if (sliderLGeq4.value > 0.0) {
                meterL4Top.progress = sliderLGeq4.value/GEQ_SLIDER_MAX_VALUE;
                meterL4Btm.progress = 0.0;
            } else if (sliderLGeq4.value < 0.0) {
                meterL4Btm.progress = sliderLGeq4.value/GEQ_SLIDER_MIN_VALUE;
                meterL4Top.progress = 0.0;
            } else {
                meterL4Top.progress = 0.0;
                meterL4Btm.progress = 0.0;
            }
            [viewController sendData:CMD_L_GEQ15_100HZ_DB commandType:DSP_WRITE dspId:DSP_5 value:(int)(sliderLGeq4.value)];
        }
        [self updatePreviewImage];
    }
}

- (IBAction)onSlider5Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider5End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider5Action:(id) sender {
    if (sender == sliderLGeq5) {
        if (sliderLGeq5.value > 0.0) {
            meterL5Top.progress = sliderLGeq5.value/GEQ_SLIDER_MAX_VALUE;
            meterL5Btm.progress = 0.0;
        } else if (sliderLGeq5.value < 0.0) {
            meterL5Btm.progress = sliderLGeq5.value/GEQ_SLIDER_MIN_VALUE;
            meterL5Top.progress = 0.0;
        } else {
            meterL5Top.progress = 0.0;
            meterL5Btm.progress = 0.0;
        }
        [viewController sendData:CMD_L_GEQ15_160HZ_DB commandType:DSP_WRITE dspId:DSP_5 value:(int)(sliderLGeq5.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderRGeq5.value = sliderLGeq5.value;
            if (sliderRGeq5.value > 0.0) {
                meterR5Top.progress = sliderRGeq5.value/GEQ_SLIDER_MAX_VALUE;
                meterR5Btm.progress = 0.0;
            } else if (sliderRGeq5.value < 0.0) {
                meterR5Btm.progress = sliderRGeq5.value/GEQ_SLIDER_MIN_VALUE;
                meterR5Top.progress = 0.0;
            } else {
                meterR5Top.progress = 0.0;
                meterR5Btm.progress = 0.0;
            }
            [viewController sendData:CMD_R_GEQ15_160HZ_DB commandType:DSP_WRITE dspId:DSP_5 value:(int)(sliderRGeq5.value)];
        }
        [self updatePreviewImage];
    } else if (sender == sliderRGeq5) {
        if (sliderRGeq5.value > 0.0) {
            meterR5Top.progress = sliderRGeq5.value/GEQ_SLIDER_MAX_VALUE;
            meterR5Btm.progress = 0.0;
        } else if (sliderRGeq5.value < 0.0) {
            meterR5Btm.progress = sliderRGeq5.value/GEQ_SLIDER_MIN_VALUE;
            meterR5Top.progress = 0.0;
        } else {
            meterR5Top.progress = 0.0;
            meterR5Btm.progress = 0.0;
        }
        [viewController sendData:CMD_R_GEQ15_160HZ_DB commandType:DSP_WRITE dspId:DSP_5 value:(int)(sliderRGeq5.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderLGeq5.value = sliderRGeq5.value;
            if (sliderLGeq5.value > 0.0) {
                meterL5Top.progress = sliderLGeq5.value/GEQ_SLIDER_MAX_VALUE;
                meterL5Btm.progress = 0.0;
            } else if (sliderLGeq5.value < 0.0) {
                meterL5Btm.progress = sliderLGeq5.value/GEQ_SLIDER_MIN_VALUE;
                meterL5Top.progress = 0.0;
            } else {
                meterL5Top.progress = 0.0;
                meterL5Btm.progress = 0.0;
            }
            [viewController sendData:CMD_L_GEQ15_160HZ_DB commandType:DSP_WRITE dspId:DSP_5 value:(int)(sliderLGeq5.value)];
        }
        [self updatePreviewImage];
    }
}

- (IBAction)onSlider6Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider6End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider6Action:(id) sender {
    if (sender == sliderLGeq6) {
        if (sliderLGeq6.value > 0.0) {
            meterL6Top.progress = sliderLGeq6.value/GEQ_SLIDER_MAX_VALUE;
            meterL6Btm.progress = 0.0;
        } else if (sliderLGeq6.value < 0.0) {
            meterL6Btm.progress = sliderLGeq6.value/GEQ_SLIDER_MIN_VALUE;
            meterL6Top.progress = 0.0;
        } else {
            meterL6Top.progress = 0.0;
            meterL6Btm.progress = 0.0;
        }
        [viewController sendData:CMD_L_GEQ15_250HZ_DB commandType:DSP_WRITE dspId:DSP_5 value:(int)(sliderLGeq6.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderRGeq6.value = sliderLGeq6.value;
            if (sliderRGeq6.value > 0.0) {
                meterR6Top.progress = sliderRGeq6.value/GEQ_SLIDER_MAX_VALUE;
                meterR6Btm.progress = 0.0;
            } else if (sliderRGeq6.value < 0.0) {
                meterR6Btm.progress = sliderRGeq6.value/GEQ_SLIDER_MIN_VALUE;
                meterR6Top.progress = 0.0;
            } else {
                meterR6Top.progress = 0.0;
                meterR6Btm.progress = 0.0;
            }
            [viewController sendData:CMD_R_GEQ15_250HZ_DB commandType:DSP_WRITE dspId:DSP_5 value:(int)(sliderRGeq6.value)];
        }
        [self updatePreviewImage];
    } else if (sender == sliderRGeq6) {
        if (sliderRGeq6.value > 0.0) {
            meterR6Top.progress = sliderRGeq6.value/GEQ_SLIDER_MAX_VALUE;
            meterR6Btm.progress = 0.0;
        } else if (sliderRGeq6.value < 0.0) {
            meterR6Btm.progress = sliderRGeq6.value/GEQ_SLIDER_MIN_VALUE;
            meterR6Top.progress = 0.0;
        } else {
            meterR6Top.progress = 0.0;
            meterR6Btm.progress = 0.0;
        }
        [viewController sendData:CMD_R_GEQ15_250HZ_DB commandType:DSP_WRITE dspId:DSP_5 value:(int)(sliderRGeq6.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderLGeq6.value = sliderRGeq6.value;
            if (sliderLGeq6.value > 0.0) {
                meterL6Top.progress = sliderLGeq6.value/GEQ_SLIDER_MAX_VALUE;
                meterL6Btm.progress = 0.0;
            } else if (sliderLGeq6.value < 0.0) {
                meterL6Btm.progress = sliderLGeq6.value/GEQ_SLIDER_MIN_VALUE;
                meterL6Top.progress = 0.0;
            } else {
                meterL6Top.progress = 0.0;
                meterL6Btm.progress = 0.0;
            }
            [viewController sendData:CMD_L_GEQ15_250HZ_DB commandType:DSP_WRITE dspId:DSP_5 value:(int)(sliderLGeq6.value)];
        }
        [self updatePreviewImage];
    }
}

- (IBAction)onSlider7Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider7End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider7Action:(id) sender {
    if (sender == sliderLGeq7) {
        if (sliderLGeq7.value > 0.0) {
            meterL7Top.progress = sliderLGeq7.value/GEQ_SLIDER_MAX_VALUE;
            meterL7Btm.progress = 0.0;
        } else if (sliderLGeq7.value < 0.0) {
            meterL7Btm.progress = sliderLGeq7.value/GEQ_SLIDER_MIN_VALUE;
            meterL7Top.progress = 0.0;
        } else {
            meterL7Top.progress = 0.0;
            meterL7Btm.progress = 0.0;
        }
        [viewController sendData:CMD_L_GEQ15_400HZ_DB commandType:DSP_WRITE dspId:DSP_5 value:(int)(sliderLGeq7.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderRGeq7.value = sliderLGeq7.value;
            if (sliderRGeq7.value > 0.0) {
                meterR7Top.progress = sliderRGeq7.value/GEQ_SLIDER_MAX_VALUE;
                meterR7Btm.progress = 0.0;
            } else if (sliderRGeq7.value < 0.0) {
                meterR7Btm.progress = sliderRGeq7.value/GEQ_SLIDER_MIN_VALUE;
                meterR7Top.progress = 0.0;
            } else {
                meterR7Top.progress = 0.0;
                meterR7Btm.progress = 0.0;
            }
            [viewController sendData:CMD_R_GEQ15_400HZ_DB commandType:DSP_WRITE dspId:DSP_5 value:(int)(sliderRGeq7.value)];
        }
        [self updatePreviewImage];
    } else if (sender == sliderRGeq7) {
        if (sliderRGeq7.value > 0.0) {
            meterR7Top.progress = sliderRGeq7.value/GEQ_SLIDER_MAX_VALUE;
            meterR7Btm.progress = 0.0;
        } else if (sliderRGeq7.value < 0.0) {
            meterR7Btm.progress = sliderRGeq7.value/GEQ_SLIDER_MIN_VALUE;
            meterR7Top.progress = 0.0;
        } else {
            meterR7Top.progress = 0.0;
            meterR7Btm.progress = 0.0;
        }
        [viewController sendData:CMD_R_GEQ15_400HZ_DB commandType:DSP_WRITE dspId:DSP_5 value:(int)(sliderRGeq7.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderLGeq7.value = sliderRGeq7.value;
            if (sliderLGeq7.value > 0.0) {
                meterL7Top.progress = sliderLGeq7.value/GEQ_SLIDER_MAX_VALUE;
                meterL7Btm.progress = 0.0;
            } else if (sliderLGeq7.value < 0.0) {
                meterL7Btm.progress = sliderLGeq7.value/GEQ_SLIDER_MIN_VALUE;
                meterL7Top.progress = 0.0;
            } else {
                meterL7Top.progress = 0.0;
                meterL7Btm.progress = 0.0;
            }
            [viewController sendData:CMD_L_GEQ15_400HZ_DB commandType:DSP_WRITE dspId:DSP_5 value:(int)(sliderLGeq7.value)];
        }
        [self updatePreviewImage];
    }
}

- (IBAction)onSlider8Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider8End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider8Action:(id) sender {
    if (sender == sliderLGeq8) {
        if (sliderLGeq8.value > 0.0) {
            meterL8Top.progress = sliderLGeq8.value/GEQ_SLIDER_MAX_VALUE;
            meterL8Btm.progress = 0.0;
        } else if (sliderLGeq8.value < 0.0) {
            meterL8Btm.progress = sliderLGeq8.value/GEQ_SLIDER_MIN_VALUE;
            meterL8Top.progress = 0.0;
        } else {
            meterL8Top.progress = 0.0;
            meterL8Btm.progress = 0.0;
        }
        [viewController sendData:CMD_L_GEQ15_630HZ_DB commandType:DSP_WRITE dspId:DSP_5 value:(int)(sliderLGeq8.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderRGeq8.value = sliderLGeq8.value;
            if (sliderRGeq8.value > 0.0) {
                meterR8Top.progress = sliderRGeq8.value/GEQ_SLIDER_MAX_VALUE;
                meterR8Btm.progress = 0.0;
            } else if (sliderRGeq8.value < 0.0) {
                meterR8Btm.progress = sliderRGeq8.value/GEQ_SLIDER_MIN_VALUE;
                meterR8Top.progress = 0.0;
            } else {
                meterR8Top.progress = 0.0;
                meterR8Btm.progress = 0.0;
            }
            [viewController sendData:CMD_R_GEQ15_630HZ_DB commandType:DSP_WRITE dspId:DSP_5 value:(int)(sliderRGeq8.value)];
        }
        [self updatePreviewImage];
    } else if (sender == sliderRGeq8) {
        if (sliderRGeq8.value > 0.0) {
            meterR8Top.progress = sliderRGeq8.value/GEQ_SLIDER_MAX_VALUE;
            meterR8Btm.progress = 0.0;
        } else if (sliderRGeq8.value < 0.0) {
            meterR8Btm.progress = sliderRGeq8.value/GEQ_SLIDER_MIN_VALUE;
            meterR8Top.progress = 0.0;
        } else {
            meterR8Top.progress = 0.0;
            meterR8Btm.progress = 0.0;
        }
        [viewController sendData:CMD_R_GEQ15_630HZ_DB commandType:DSP_WRITE dspId:DSP_5 value:(int)(sliderRGeq8.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderLGeq8.value = sliderRGeq8.value;
            if (sliderLGeq8.value > 0.0) {
                meterL8Top.progress = sliderLGeq8.value/GEQ_SLIDER_MAX_VALUE;
                meterL8Btm.progress = 0.0;
            } else if (sliderLGeq8.value < 0.0) {
                meterL8Btm.progress = sliderLGeq8.value/GEQ_SLIDER_MIN_VALUE;
                meterL8Top.progress = 0.0;
            } else {
                meterL8Top.progress = 0.0;
                meterL8Btm.progress = 0.0;
            }
            [viewController sendData:CMD_L_GEQ15_630HZ_DB commandType:DSP_WRITE dspId:DSP_5 value:(int)(sliderLGeq8.value)];
        }
        [self updatePreviewImage];
    }
}

- (IBAction)onSlider9Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider9End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider9Action:(id) sender {
    if (sender == sliderLGeq9) {
        if (sliderLGeq9.value > 0.0) {
            meterL9Top.progress = sliderLGeq9.value/GEQ_SLIDER_MAX_VALUE;
            meterL9Btm.progress = 0.0;
        } else if (sliderLGeq9.value < 0.0) {
            meterL9Btm.progress = sliderLGeq9.value/GEQ_SLIDER_MIN_VALUE;
            meterL9Top.progress = 0.0;
        } else {
            meterL9Top.progress = 0.0;
            meterL9Btm.progress = 0.0;
        }
        [viewController sendData:CMD_L_GEQ15_1KHZ_DB commandType:DSP_WRITE dspId:DSP_5 value:(int)(sliderLGeq9.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderRGeq9.value = sliderLGeq9.value;
            if (sliderRGeq9.value > 0.0) {
                meterR9Top.progress = sliderRGeq9.value/GEQ_SLIDER_MAX_VALUE;
                meterR9Btm.progress = 0.0;
            } else if (sliderRGeq9.value < 0.0) {
                meterR9Btm.progress = sliderRGeq9.value/GEQ_SLIDER_MIN_VALUE;
                meterR9Top.progress = 0.0;
            } else {
                meterR9Top.progress = 0.0;
                meterR9Btm.progress = 0.0;
            }
            [viewController sendData:CMD_R_GEQ15_1KHZ_DB commandType:DSP_WRITE dspId:DSP_5 value:(int)(sliderRGeq9.value)];
        }
        [self updatePreviewImage];
    } else if (sender == sliderRGeq9) {
        if (sliderRGeq9.value > 0.0) {
            meterR9Top.progress = sliderRGeq9.value/GEQ_SLIDER_MAX_VALUE;
            meterR9Btm.progress = 0.0;
        } else if (sliderRGeq9.value < 0.0) {
            meterR9Btm.progress = sliderRGeq9.value/GEQ_SLIDER_MIN_VALUE;
            meterR9Top.progress = 0.0;
        } else {
            meterR9Top.progress = 0.0;
            meterR9Btm.progress = 0.0;
        }
        [viewController sendData:CMD_R_GEQ15_1KHZ_DB commandType:DSP_WRITE dspId:DSP_5 value:(int)(sliderRGeq9.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderLGeq9.value = sliderRGeq9.value;
            if (sliderLGeq9.value > 0.0) {
                meterL9Top.progress = sliderLGeq9.value/GEQ_SLIDER_MAX_VALUE;
                meterL9Btm.progress = 0.0;
            } else if (sliderLGeq9.value < 0.0) {
                meterL9Btm.progress = sliderLGeq9.value/GEQ_SLIDER_MIN_VALUE;
                meterL9Top.progress = 0.0;
            } else {
                meterL9Top.progress = 0.0;
                meterL9Btm.progress = 0.0;
            }
            [viewController sendData:CMD_L_GEQ15_1KHZ_DB commandType:DSP_WRITE dspId:DSP_5 value:(int)(sliderLGeq9.value)];
        }
        [self updatePreviewImage];
    }
}

- (IBAction)onSlider10Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider10End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider10Action:(id) sender {
    if (sender == sliderLGeq10) {
        if (sliderLGeq10.value > 0.0) {
            meterL10Top.progress = sliderLGeq10.value/GEQ_SLIDER_MAX_VALUE;
            meterL10Btm.progress = 0.0;
        } else if (sliderLGeq10.value < 0.0) {
            meterL10Btm.progress = sliderLGeq10.value/GEQ_SLIDER_MIN_VALUE;
            meterL10Top.progress = 0.0;
        } else {
            meterL10Top.progress = 0.0;
            meterL10Btm.progress = 0.0;
        }
        [viewController sendData:CMD_L_GEQ15_1_6KHZ_DB commandType:DSP_WRITE dspId:DSP_5 value:(int)(sliderLGeq10.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderRGeq10.value = sliderLGeq10.value;
            if (sliderRGeq10.value > 0.0) {
                meterR10Top.progress = sliderRGeq10.value/GEQ_SLIDER_MAX_VALUE;
                meterR10Btm.progress = 0.0;
            } else if (sliderRGeq10.value < 0.0) {
                meterR10Btm.progress = sliderRGeq10.value/GEQ_SLIDER_MIN_VALUE;
                meterR10Top.progress = 0.0;
            } else {
                meterR10Top.progress = 0.0;
                meterR10Btm.progress = 0.0;
            }
            [viewController sendData:CMD_R_GEQ15_1_6KHZ_DB commandType:DSP_WRITE dspId:DSP_5 value:(int)(sliderRGeq10.value)];
        }
        [self updatePreviewImage];
    } else if (sender == sliderRGeq10) {
        if (sliderRGeq10.value > 0.0) {
            meterR10Top.progress = sliderRGeq10.value/GEQ_SLIDER_MAX_VALUE;
            meterR10Btm.progress = 0.0;
        } else if (sliderRGeq10.value < 0.0) {
            meterR10Btm.progress = sliderRGeq10.value/GEQ_SLIDER_MIN_VALUE;
            meterR10Top.progress = 0.0;
        } else {
            meterR10Top.progress = 0.0;
            meterR10Btm.progress = 0.0;
        }
        [viewController sendData:CMD_R_GEQ15_1_6KHZ_DB commandType:DSP_WRITE dspId:DSP_5 value:(int)(sliderRGeq10.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderLGeq10.value = sliderRGeq10.value;
            if (sliderLGeq10.value > 0.0) {
                meterL10Top.progress = sliderLGeq10.value/GEQ_SLIDER_MAX_VALUE;
                meterL10Btm.progress = 0.0;
            } else if (sliderLGeq10.value < 0.0) {
                meterL10Btm.progress = sliderLGeq10.value/GEQ_SLIDER_MIN_VALUE;
                meterL10Top.progress = 0.0;
            } else {
                meterL10Top.progress = 0.0;
                meterL10Btm.progress = 0.0;
            }
            [viewController sendData:CMD_L_GEQ15_1_6KHZ_DB commandType:DSP_WRITE dspId:DSP_5 value:(int)(sliderLGeq10.value)];
        }
        [self updatePreviewImage];
    }
}

- (IBAction)onSlider11Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider11End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider11Action:(id) sender {
    if (sender == sliderLGeq11) {
        if (sliderLGeq11.value > 0.0) {
            meterL11Top.progress = sliderLGeq11.value/GEQ_SLIDER_MAX_VALUE;
            meterL11Btm.progress = 0.0;
        } else if (sliderLGeq11.value < 0.0) {
            meterL11Btm.progress = sliderLGeq11.value/GEQ_SLIDER_MIN_VALUE;
            meterL11Top.progress = 0.0;
        } else {
            meterL11Top.progress = 0.0;
            meterL11Btm.progress = 0.0;
        }
        [viewController sendData:CMD_L_GEQ15_2_5KHZ_DB commandType:DSP_WRITE dspId:DSP_5 value:(int)(sliderLGeq11.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderRGeq11.value = sliderLGeq11.value;
            if (sliderRGeq11.value > 0.0) {
                meterR11Top.progress = sliderRGeq11.value/GEQ_SLIDER_MAX_VALUE;
                meterR11Btm.progress = 0.0;
            } else if (sliderRGeq11.value < 0.0) {
                meterR11Btm.progress = sliderRGeq11.value/GEQ_SLIDER_MIN_VALUE;
                meterR11Top.progress = 0.0;
            } else {
                meterR11Top.progress = 0.0;
                meterR11Btm.progress = 0.0;
            }
            [viewController sendData:CMD_R_GEQ15_2_5KHZ_DB commandType:DSP_WRITE dspId:DSP_5 value:(int)(sliderRGeq11.value)];
        }
        [self updatePreviewImage];
    } else if (sender == sliderRGeq11) {
        if (sliderRGeq11.value > 0.0) {
            meterR11Top.progress = sliderRGeq11.value/GEQ_SLIDER_MAX_VALUE;
            meterR11Btm.progress = 0.0;
        } else if (sliderRGeq11.value < 0.0) {
            meterR11Btm.progress = sliderRGeq11.value/GEQ_SLIDER_MIN_VALUE;
            meterR11Top.progress = 0.0;
        } else {
            meterR11Top.progress = 0.0;
            meterR11Btm.progress = 0.0;
        }
        [viewController sendData:CMD_R_GEQ15_2_5KHZ_DB commandType:DSP_WRITE dspId:DSP_5 value:(int)(sliderRGeq11.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderLGeq11.value = sliderRGeq11.value;
            if (sliderLGeq11.value > 0.0) {
                meterL11Top.progress = sliderLGeq11.value/GEQ_SLIDER_MAX_VALUE;
                meterL11Btm.progress = 0.0;
            } else if (sliderLGeq11.value < 0.0) {
                meterL11Btm.progress = sliderLGeq11.value/GEQ_SLIDER_MIN_VALUE;
                meterL11Top.progress = 0.0;
            } else {
                meterL11Top.progress = 0.0;
                meterL11Btm.progress = 0.0;
            }
            [viewController sendData:CMD_L_GEQ15_2_5KHZ_DB commandType:DSP_WRITE dspId:DSP_5 value:(int)(sliderLGeq11.value)];
        }
        [self updatePreviewImage];
    }
}

- (IBAction)onSlider12Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider12End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider12Action:(id) sender {
    if (sender == sliderLGeq12) {
        if (sliderLGeq12.value > 0.0) {
            meterL12Top.progress = sliderLGeq12.value/GEQ_SLIDER_MAX_VALUE;
            meterL12Btm.progress = 0.0;
        } else if (sliderLGeq12.value < 0.0) {
            meterL12Btm.progress = sliderLGeq12.value/GEQ_SLIDER_MIN_VALUE;
            meterL12Top.progress = 0.0;
        } else {
            meterL12Top.progress = 0.0;
            meterL12Btm.progress = 0.0;
        }
        [viewController sendData:CMD_L_GEQ15_4KHZ_DB commandType:DSP_WRITE dspId:DSP_5 value:(int)(sliderLGeq12.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderRGeq12.value = sliderLGeq12.value;
            if (sliderRGeq12.value > 0.0) {
                meterR12Top.progress = sliderRGeq12.value/GEQ_SLIDER_MAX_VALUE;
                meterR12Btm.progress = 0.0;
            } else if (sliderRGeq12.value < 0.0) {
                meterR12Btm.progress = sliderRGeq12.value/GEQ_SLIDER_MIN_VALUE;
                meterR12Top.progress = 0.0;
            } else {
                meterR12Top.progress = 0.0;
                meterR12Btm.progress = 0.0;
            }
            [viewController sendData:CMD_R_GEQ15_4KHZ_DB commandType:DSP_WRITE dspId:DSP_5 value:(int)(sliderRGeq12.value)];
        }
        [self updatePreviewImage];
    } else if (sender == sliderRGeq12) {
        if (sliderRGeq12.value > 0.0) {
            meterR12Top.progress = sliderRGeq12.value/GEQ_SLIDER_MAX_VALUE;
            meterR12Btm.progress = 0.0;
        } else if (sliderRGeq12.value < 0.0) {
            meterR12Btm.progress = sliderRGeq12.value/GEQ_SLIDER_MIN_VALUE;
            meterR12Top.progress = 0.0;
        } else {
            meterR12Top.progress = 0.0;
            meterR12Btm.progress = 0.0;
        }
        [viewController sendData:CMD_R_GEQ15_4KHZ_DB commandType:DSP_WRITE dspId:DSP_5 value:(int)(sliderRGeq12.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderLGeq12.value = sliderRGeq12.value;
            if (sliderLGeq12.value > 0.0) {
                meterL12Top.progress = sliderLGeq12.value/GEQ_SLIDER_MAX_VALUE;
                meterL12Btm.progress = 0.0;
            } else if (sliderLGeq12.value < 0.0) {
                meterL12Btm.progress = sliderLGeq12.value/GEQ_SLIDER_MIN_VALUE;
                meterL12Top.progress = 0.0;
            } else {
                meterL12Top.progress = 0.0;
                meterL12Btm.progress = 0.0;
            }
            [viewController sendData:CMD_L_GEQ15_4KHZ_DB commandType:DSP_WRITE dspId:DSP_5 value:(int)(sliderLGeq12.value)];
        }
        [self updatePreviewImage];
    }
}

- (IBAction)onSlider13Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider13End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider13Action:(id) sender {
    if (sender == sliderLGeq13) {
        if (sliderLGeq13.value > 0.0) {
            meterL13Top.progress = sliderLGeq13.value/GEQ_SLIDER_MAX_VALUE;
            meterL13Btm.progress = 0.0;
        } else if (sliderLGeq13.value < 0.0) {
            meterL13Btm.progress = sliderLGeq13.value/GEQ_SLIDER_MIN_VALUE;
            meterL13Top.progress = 0.0;
        } else {
            meterL13Top.progress = 0.0;
            meterL13Btm.progress = 0.0;
        }
        [viewController sendData:CMD_L_GEQ15_6_3KHZ_DB commandType:DSP_WRITE dspId:DSP_5 value:(int)(sliderLGeq13.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderRGeq13.value = sliderLGeq13.value;
            if (sliderRGeq13.value > 0.0) {
                meterR13Top.progress = sliderRGeq13.value/GEQ_SLIDER_MAX_VALUE;
                meterR13Btm.progress = 0.0;
            } else if (sliderRGeq13.value < 0.0) {
                meterR13Btm.progress = sliderRGeq13.value/GEQ_SLIDER_MIN_VALUE;
                meterR13Top.progress = 0.0;
            } else {
                meterR13Top.progress = 0.0;
                meterR13Btm.progress = 0.0;
            }
            [viewController sendData:CMD_R_GEQ15_6_3KHZ_DB commandType:DSP_WRITE dspId:DSP_5 value:(int)(sliderRGeq13.value)];
        }
        [self updatePreviewImage];
    } else if (sender == sliderRGeq13) {
        if (sliderRGeq13.value > 0.0) {
            meterR13Top.progress = sliderRGeq13.value/GEQ_SLIDER_MAX_VALUE;
            meterR13Btm.progress = 0.0;
        } else if (sliderRGeq13.value < 0.0) {
            meterR13Btm.progress = sliderRGeq13.value/GEQ_SLIDER_MIN_VALUE;
            meterR13Top.progress = 0.0;
        } else {
            meterR13Top.progress = 0.0;
            meterR13Btm.progress = 0.0;
        }
        [viewController sendData:CMD_R_GEQ15_6_3KHZ_DB commandType:DSP_WRITE dspId:DSP_5 value:(int)(sliderRGeq13.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderLGeq13.value = sliderRGeq13.value;
            if (sliderLGeq13.value > 0.0) {
                meterL13Top.progress = sliderLGeq13.value/GEQ_SLIDER_MAX_VALUE;
                meterL13Btm.progress = 0.0;
            } else if (sliderLGeq13.value < 0.0) {
                meterL13Btm.progress = sliderLGeq13.value/GEQ_SLIDER_MIN_VALUE;
                meterL13Top.progress = 0.0;
            } else {
                meterL13Top.progress = 0.0;
                meterL13Btm.progress = 0.0;
            }
            [viewController sendData:CMD_L_GEQ15_6_3KHZ_DB commandType:DSP_WRITE dspId:DSP_5 value:(int)(sliderLGeq13.value)];
        }
        [self updatePreviewImage];
    }
}

- (IBAction)onSlider14Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider14End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider14Action:(id) sender {
    if (sender == sliderLGeq14) {
        if (sliderLGeq14.value > 0.0) {
            meterL14Top.progress = sliderLGeq14.value/GEQ_SLIDER_MAX_VALUE;
            meterL14Btm.progress = 0.0;
        } else if (sliderLGeq14.value < 0.0) {
            meterL14Btm.progress = sliderLGeq14.value/GEQ_SLIDER_MIN_VALUE;
            meterL14Top.progress = 0.0;
        } else {
            meterL14Top.progress = 0.0;
            meterL14Btm.progress = 0.0;
        }
        [viewController sendData:CMD_L_GEQ15_10KHZ_DB commandType:DSP_WRITE dspId:DSP_5 value:(int)(sliderLGeq14.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderRGeq14.value = sliderLGeq14.value;
            if (sliderRGeq14.value > 0.0) {
                meterR14Top.progress = sliderRGeq14.value/GEQ_SLIDER_MAX_VALUE;
                meterR14Btm.progress = 0.0;
            } else if (sliderRGeq14.value < 0.0) {
                meterR14Btm.progress = sliderRGeq14.value/GEQ_SLIDER_MIN_VALUE;
                meterR14Top.progress = 0.0;
            } else {
                meterR14Top.progress = 0.0;
                meterR14Btm.progress = 0.0;
            }
            [viewController sendData:CMD_R_GEQ15_10KHZ_DB commandType:DSP_WRITE dspId:DSP_5 value:(int)(sliderRGeq14.value)];
        }
        [self updatePreviewImage];
    } else if (sender == sliderRGeq14) {
        if (sliderRGeq14.value > 0.0) {
            meterR14Top.progress = sliderRGeq14.value/GEQ_SLIDER_MAX_VALUE;
            meterR14Btm.progress = 0.0;
        } else if (sliderRGeq14.value < 0.0) {
            meterR14Btm.progress = sliderRGeq14.value/GEQ_SLIDER_MIN_VALUE;
            meterR14Top.progress = 0.0;
        } else {
            meterR14Top.progress = 0.0;
            meterR14Btm.progress = 0.0;
        }
        [viewController sendData:CMD_R_GEQ15_10KHZ_DB commandType:DSP_WRITE dspId:DSP_5 value:(int)(sliderRGeq14.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderLGeq14.value = sliderRGeq14.value;
            if (sliderLGeq14.value > 0.0) {
                meterL14Top.progress = sliderLGeq14.value/GEQ_SLIDER_MAX_VALUE;
                meterL14Btm.progress = 0.0;
            } else if (sliderLGeq14.value < 0.0) {
                meterL14Btm.progress = sliderLGeq14.value/GEQ_SLIDER_MIN_VALUE;
                meterL14Top.progress = 0.0;
            } else {
                meterL14Top.progress = 0.0;
                meterL14Btm.progress = 0.0;
            }
            [viewController sendData:CMD_L_GEQ15_10KHZ_DB commandType:DSP_WRITE dspId:DSP_5 value:(int)(sliderLGeq14.value)];
        }
        [self updatePreviewImage];
    }
}

- (IBAction)onSlider15Begin:(id) sender {
    geqLock = YES;
}

- (IBAction)onSlider15End:(id) sender {
    geqLock = NO;
}

- (IBAction)onSlider15Action:(id) sender {
    if (sender == sliderLGeq15) {
        if (sliderLGeq15.value > 0.0) {
            meterL15Top.progress = sliderLGeq15.value/GEQ_SLIDER_MAX_VALUE;
            meterL15Btm.progress = 0.0;
        } else if (sliderLGeq15.value < 0.0) {
            meterL15Btm.progress = sliderLGeq15.value/GEQ_SLIDER_MIN_VALUE;
            meterL15Top.progress = 0.0;
        } else {
            meterL15Top.progress = 0.0;
            meterL15Btm.progress = 0.0;
        }
        [viewController sendData:CMD_L_GEQ15_16KHZ_DB commandType:DSP_WRITE dspId:DSP_5 value:(int)(sliderLGeq15.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderRGeq15.value = sliderLGeq15.value;
            if (sliderRGeq15.value > 0.0) {
                meterR15Top.progress = sliderRGeq15.value/GEQ_SLIDER_MAX_VALUE;
                meterR15Btm.progress = 0.0;
            } else if (sliderRGeq15.value < 0.0) {
                meterR15Btm.progress = sliderRGeq15.value/GEQ_SLIDER_MIN_VALUE;
                meterR15Top.progress = 0.0;
            } else {
                meterR15Top.progress = 0.0;
                meterR15Btm.progress = 0.0;
            }
            [viewController sendData:CMD_R_GEQ15_16KHZ_DB commandType:DSP_WRITE dspId:DSP_5 value:(int)(sliderRGeq15.value)];
        }
        [self updatePreviewImage];
    } else if (sender == sliderRGeq15) {
        if (sliderRGeq15.value > 0.0) {
            meterR15Top.progress = sliderRGeq15.value/GEQ_SLIDER_MAX_VALUE;
            meterR15Btm.progress = 0.0;
        } else if (sliderRGeq15.value < 0.0) {
            meterR15Btm.progress = sliderRGeq15.value/GEQ_SLIDER_MIN_VALUE;
            meterR15Top.progress = 0.0;
        } else {
            meterR15Top.progress = 0.0;
            meterR15Btm.progress = 0.0;
        }
        [viewController sendData:CMD_R_GEQ15_16KHZ_DB commandType:DSP_WRITE dspId:DSP_5 value:(int)(sliderRGeq15.value)];
        
        // Handle link mode
        if (geqLinkMode) {
            sliderLGeq15.value = sliderRGeq15.value;
            if (sliderLGeq15.value > 0.0) {
                meterL15Top.progress = sliderLGeq15.value/GEQ_SLIDER_MAX_VALUE;
                meterL15Btm.progress = 0.0;
            } else if (sliderLGeq15.value < 0.0) {
                meterL15Btm.progress = sliderLGeq15.value/GEQ_SLIDER_MIN_VALUE;
                meterL15Top.progress = 0.0;
            } else {
                meterL15Top.progress = 0.0;
                meterL15Btm.progress = 0.0;
            }
            [viewController sendData:CMD_L_GEQ15_16KHZ_DB commandType:DSP_WRITE dspId:DSP_5 value:(int)(sliderLGeq15.value)];
        }
        [self updatePreviewImage];
    }
}

- (void)reset {
    [self onBtnReset:nil];
}

@end
