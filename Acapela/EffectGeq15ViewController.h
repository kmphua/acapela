//
//  EffectGeq15Controller.h
//  Acapela
//
//  Created by Kevin on 13/8/25.
//  Copyright (c) 2013年 Kevin Phua. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "acapela.h"
#import "JEProgressView.h"

@class EffectGeq15Controller;
@protocol Geq15PreviewDelegate <NSObject>
@required
- (void)didFinishEditingGeq15;
@end

@interface EffectGeq15ViewController : UIViewController

// Delegate callback to parent controller
@property (nonatomic, assign) id <Geq15PreviewDelegate> delegate;
@property (strong, nonatomic) UIImage *imgEqL;
@property (strong, nonatomic) UIImage *imgEqR;

// GEQ panel
@property (weak, nonatomic) IBOutlet UIButton *btnGeqLink;
@property (weak, nonatomic) IBOutlet UIButton *btnGeqDraw;
@property (weak, nonatomic) IBOutlet UIButton *btnFile;
@property (weak, nonatomic) IBOutlet UIView *viewGeqFrameL;
@property (weak, nonatomic) IBOutlet UIView *viewGeqDrawL;
@property (weak, nonatomic) IBOutlet UIView *viewGeqFrameR;
@property (weak, nonatomic) IBOutlet UIView *viewGeqDrawR;

// Sliders
@property (nonatomic,retain) UISlider *sliderLGeq1;
@property (nonatomic,retain) UISlider *sliderLGeq2;
@property (nonatomic,retain) UISlider *sliderLGeq3;
@property (nonatomic,retain) UISlider *sliderLGeq4;
@property (nonatomic,retain) UISlider *sliderLGeq5;
@property (nonatomic,retain) UISlider *sliderLGeq6;
@property (nonatomic,retain) UISlider *sliderLGeq7;
@property (nonatomic,retain) UISlider *sliderLGeq8;
@property (nonatomic,retain) UISlider *sliderLGeq9;
@property (nonatomic,retain) UISlider *sliderLGeq10;
@property (nonatomic,retain) UISlider *sliderLGeq11;
@property (nonatomic,retain) UISlider *sliderLGeq12;
@property (nonatomic,retain) UISlider *sliderLGeq13;
@property (nonatomic,retain) UISlider *sliderLGeq14;
@property (nonatomic,retain) UISlider *sliderLGeq15;

@property (nonatomic,retain) UISlider *sliderRGeq1;
@property (nonatomic,retain) UISlider *sliderRGeq2;
@property (nonatomic,retain) UISlider *sliderRGeq3;
@property (nonatomic,retain) UISlider *sliderRGeq4;
@property (nonatomic,retain) UISlider *sliderRGeq5;
@property (nonatomic,retain) UISlider *sliderRGeq6;
@property (nonatomic,retain) UISlider *sliderRGeq7;
@property (nonatomic,retain) UISlider *sliderRGeq8;
@property (nonatomic,retain) UISlider *sliderRGeq9;
@property (nonatomic,retain) UISlider *sliderRGeq10;
@property (nonatomic,retain) UISlider *sliderRGeq11;
@property (nonatomic,retain) UISlider *sliderRGeq12;
@property (nonatomic,retain) UISlider *sliderRGeq13;
@property (nonatomic,retain) UISlider *sliderRGeq14;
@property (nonatomic,retain) UISlider *sliderRGeq15;

// Slider Meters
@property (nonatomic,retain) JEProgressView *meterL1Top;
@property (nonatomic,retain) JEProgressView *meterL1Btm;
@property (nonatomic,retain) JEProgressView *meterL2Top;
@property (nonatomic,retain) JEProgressView *meterL2Btm;
@property (nonatomic,retain) JEProgressView *meterL3Top;
@property (nonatomic,retain) JEProgressView *meterL3Btm;
@property (nonatomic,retain) JEProgressView *meterL4Top;
@property (nonatomic,retain) JEProgressView *meterL4Btm;
@property (nonatomic,retain) JEProgressView *meterL5Top;
@property (nonatomic,retain) JEProgressView *meterL5Btm;
@property (nonatomic,retain) JEProgressView *meterL6Top;
@property (nonatomic,retain) JEProgressView *meterL6Btm;
@property (nonatomic,retain) JEProgressView *meterL7Top;
@property (nonatomic,retain) JEProgressView *meterL7Btm;
@property (nonatomic,retain) JEProgressView *meterL8Top;
@property (nonatomic,retain) JEProgressView *meterL8Btm;
@property (nonatomic,retain) JEProgressView *meterL9Top;
@property (nonatomic,retain) JEProgressView *meterL9Btm;
@property (nonatomic,retain) JEProgressView *meterL10Top;
@property (nonatomic,retain) JEProgressView *meterL10Btm;
@property (nonatomic,retain) JEProgressView *meterL11Top;
@property (nonatomic,retain) JEProgressView *meterL11Btm;
@property (nonatomic,retain) JEProgressView *meterL12Top;
@property (nonatomic,retain) JEProgressView *meterL12Btm;
@property (nonatomic,retain) JEProgressView *meterL13Top;
@property (nonatomic,retain) JEProgressView *meterL13Btm;
@property (nonatomic,retain) JEProgressView *meterL14Top;
@property (nonatomic,retain) JEProgressView *meterL14Btm;
@property (nonatomic,retain) JEProgressView *meterL15Top;
@property (nonatomic,retain) JEProgressView *meterL15Btm;

@property (nonatomic,retain) JEProgressView *meterR1Top;
@property (nonatomic,retain) JEProgressView *meterR1Btm;
@property (nonatomic,retain) JEProgressView *meterR2Top;
@property (nonatomic,retain) JEProgressView *meterR2Btm;
@property (nonatomic,retain) JEProgressView *meterR3Top;
@property (nonatomic,retain) JEProgressView *meterR3Btm;
@property (nonatomic,retain) JEProgressView *meterR4Top;
@property (nonatomic,retain) JEProgressView *meterR4Btm;
@property (nonatomic,retain) JEProgressView *meterR5Top;
@property (nonatomic,retain) JEProgressView *meterR5Btm;
@property (nonatomic,retain) JEProgressView *meterR6Top;
@property (nonatomic,retain) JEProgressView *meterR6Btm;
@property (nonatomic,retain) JEProgressView *meterR7Top;
@property (nonatomic,retain) JEProgressView *meterR7Btm;
@property (nonatomic,retain) JEProgressView *meterR8Top;
@property (nonatomic,retain) JEProgressView *meterR8Btm;
@property (nonatomic,retain) JEProgressView *meterR9Top;
@property (nonatomic,retain) JEProgressView *meterR9Btm;
@property (nonatomic,retain) JEProgressView *meterR10Top;
@property (nonatomic,retain) JEProgressView *meterR10Btm;
@property (nonatomic,retain) JEProgressView *meterR11Top;
@property (nonatomic,retain) JEProgressView *meterR11Btm;
@property (nonatomic,retain) JEProgressView *meterR12Top;
@property (nonatomic,retain) JEProgressView *meterR12Btm;
@property (nonatomic,retain) JEProgressView *meterR13Top;
@property (nonatomic,retain) JEProgressView *meterR13Btm;
@property (nonatomic,retain) JEProgressView *meterR14Top;
@property (nonatomic,retain) JEProgressView *meterR14Btm;
@property (nonatomic,retain) JEProgressView *meterR15Top;
@property (nonatomic,retain) JEProgressView *meterR15Btm;

- (void)processReply:(EffectPagePacket *)pkt;
- (void)setGeqSoloOnOff:(BOOL)value;
- (void)setGeqOnOff:(BOOL)value;
//- (void)setLinkMode:(BOOL)value;
- (void)reset;

@end
