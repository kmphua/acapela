//
//  acapela.h
//  Acapela16 frame definition
//
//  Created by Kevin Phua on 2013/7/15.
//  Copyright (c) 2013年 Kevin Phua. All rights reserved.
//

#ifndef acapela_h
#define acapela_h

// Meter/fader/EQ values
#define METER_MAX_VALUE                 52.0
#define FADER_MAX_VALUE                 107.0
#define PAN_MAX_VALUE                   32.0
#define PAN_CENTER_VALUE                16.0
#define EQ_GAIN_OFFSET                  36.0
#define EQ_GAIN_MAX_VALUE               72.0
#define EQ_FREQ_MAX_VALUE               210.0
#define EQ_Q_MAX_VALUE                  41.0
#define FADER_DEFAULT_VALUE             79.0
#define DYN_METER_MAX_VALUE             50.0

// Knob values
#define SIG_GEN_MAX_VALUE               40
#define SIG_GEN_MIN_VALUE               0
#define SIG_GEN_OFFSET                  50
#define GAIN_MAX_VALUE                  107
#define GAIN_MIN_VALUE                  0
#define DELAY_MAX_VALUE                 490
#define DELAY_MIN_VALUE                 0
#define TEMP_MAX_VALUE                  50
#define TEMP_MIN_VALUE                  0

#define MAIN_EQ_PEQ                     0
#define MAIN_EQ_GEQ                     1

// Reply packet lengths
#define FRAME_HEADER_LEN                5
#define FRAME_PROTOCOL_LEN              6
#define DSP_READ_LEN                    26
#define DSP_WRITE_LEN                   26
#define AUTHORIZED_LEN                  22
#define FIRMWARE_VERSION_LEN            30
#define FILE_SCENE_LEN                  126

/*
 * Acapela command codes
 */

// Command type
typedef enum {
    DSP_READ = 1,
    DSP_WRITE,
    AUTHORIZED,
    GET_FILE_SCENE_LIST,
    CONNECT_MAX,
    FILE_SELECT,
    GET_AUDIO_NAME,
    SET_AUDIO_NAME,
    GET_DELAY,
    SET_DELAY,
    GET_FIRMWARE_VERSION,
    SET_INIT,
    GET_GEQ_LINK_MODE,
    SET_GEQ_LINK_MODE,
    ALL_METER_VALUE,        // Sent every 100ms
    VIEW_PAGE_PARAMETER,    // Sent every 100ms
    DELAY_48V_ORDER_PAGE_PARAMETER,
    DYN_PAGE_PARAMETER,
    ORDER_PAGE_PARAMETER,
    CHANNEL_PAGE_PARAMETER,
    EFFECT_PAGE_PARAMETER,
    AUX_GP_PAGE_PARAMETER,
    EQ_PAGE_PARAMETER,
    SETUP_CTRLRM_USB_ASSIGN_PAGE_PARAMETER,
    SET_SEND_PAGE,
    REVERB_READ,
    REVERB_WRITE,
    GET_USB_STATE,          // Sent every 100ms
    SET_FILE_ACCESS,
    GET_FILE_LIST
} COMMAND_TYPE;

// Page number
typedef enum {
    PAGE_NULL = 0,
    VIEW_PAGE,
    DELAY_48V_PAGE,
    DYN_PAGE,
    ORDER_PAGE,
    CHANNEL_PAGE,
    EFFECT_PAGE,
    AUX_GP_PAGE,
    EQ_PAGE,
    SETUP_CTRLRM_USB_ASSIGN_PAGE,
} PAGE_NUMBER;

// View page
typedef enum {
    VIEW_PAGE_TYPE_CH_1_16 = 0,
    VIEW_PAGE_TYPE_CH_17_18,
    VIEW_PAGE_TYPE_MULTI,
    VIEW_PAGE_TYPE_MAIN
} VIEW_PAGE_TYPE;

// Channel page
typedef enum {
    CHANNEL_PAGE_CH_1_8 = 0,
    CHANNEL_PAGE_CH_9_16,
    CHANNEL_PAGE_CH_17_18,
    CHANNEL_PAGE_AUX_1_4,
    CHANNEL_PAGE_GP_1_4,
    CHANNEL_PAGE_MULTI_1_4,
    CHANNEL_PAGE_CTRL
} CHANNEL_PAGE_TYPE;

// Aux/Gp page
typedef enum {
    AUX_PAGE = 0,
    GP_PAGE
} AUX_GP_PAGE_TYPE;

enum OrderChannel {
    ORDER_CH_EQ_DYN_DELAY = 0,
    ORDER_CH_EQ_DELAY_DYN,
    ORDER_CH_DYN_EQ_DELAY,
    ORDER_CH_DYN_DELAY_EQ,
    ORDER_CH_DELAY_EQ_DYN,
    ORDER_CH_DELAY_DYN_EQ
};

enum OrderMain {
    ORDER_MAIN_EQ_DYN_DELAY = 0,
    ORDER_MAIN_DYN_EQ_DELAY,
    ORDER_MAIN_GEQ_DYN_DELAY,
    ORDER_MAIN_DYN_GEQ_DELAY
};

enum OrderMulti {
    ORDER_MT_EQ_DYN_DELAY = 0,
    ORDER_MT_DYN_EQ_DELAY
};

typedef enum {
    CHANNEL_CH_1 = 0,
    CHANNEL_CH_2,
    CHANNEL_CH_3,
    CHANNEL_CH_4,
    CHANNEL_CH_5,
    CHANNEL_CH_6,
    CHANNEL_CH_7,
    CHANNEL_CH_8,
    CHANNEL_CH_9,
    CHANNEL_CH_10,
    CHANNEL_CH_11,
    CHANNEL_CH_12,
    CHANNEL_CH_13,
    CHANNEL_CH_14,
    CHANNEL_CH_15,
    CHANNEL_CH_16,
    CHANNEL_CH_17,
    CHANNEL_CH_18,
    CHANNEL_MULTI_1,
    CHANNEL_MULTI_2,
    CHANNEL_MULTI_3,
    CHANNEL_MULTI_4,
    CHANNEL_MAIN,
    CHANNEL_MAX
} ChannelType;

typedef enum {
    SCENE_MODE_SCENE = 0,
    SCENE_MODE_DYN_GATE,
    SCENE_MODE_DYN_EXP,
    SCENE_MODE_DYN_COMP,
    SCENE_MODE_DYN_LIM,
    SCENE_MODE_PEQ,
    SCENE_MODE_GEQ,
    SCENE_MODE_EFFECT_1,
    SCENE_MODE_EFFECT_2
} SceneMode;

typedef enum {
    SCENE_CHANNEL_CH_1 = 0,
    SCENE_CHANNEL_CH_2,
    SCENE_CHANNEL_CH_3,
    SCENE_CHANNEL_CH_4,
    SCENE_CHANNEL_CH_5,
    SCENE_CHANNEL_CH_6,
    SCENE_CHANNEL_CH_7,
    SCENE_CHANNEL_CH_8,
    SCENE_CHANNEL_CH_9,
    SCENE_CHANNEL_CH_10,
    SCENE_CHANNEL_CH_11,
    SCENE_CHANNEL_CH_12,
    SCENE_CHANNEL_CH_13,
    SCENE_CHANNEL_CH_14,
    SCENE_CHANNEL_CH_15,
    SCENE_CHANNEL_CH_16,
    SCENE_CHANNEL_MULTI_1,
    SCENE_CHANNEL_MULTI_2,
    SCENE_CHANNEL_MULTI_3,
    SCENE_CHANNEL_MULTI_4,
    SCENE_CHANNEL_MAIN,
    SCENE_CHANNEL_CH_17,
    SCENE_CHANNEL_CH_18
} SceneChannel;

#pragma pack(push)
#pragma pack(1)

typedef struct {
    unsigned char start_of_frame;
    unsigned int size;
    unsigned int seq_num;
    unsigned int command;
    unsigned int reserve_1;
    unsigned int reserve_2;
    unsigned int dsp;
    unsigned int address;
    unsigned char crc;
} DspReadPacket;

typedef struct {
    unsigned char start_of_frame;
    unsigned int size;
    unsigned int seq_num;
    unsigned int command;
    unsigned int reserve_1;
    unsigned int reserve_2;
    unsigned int dsp;
    unsigned int address;
    unsigned int data;
    unsigned char crc;
} DspWritePacket;

typedef struct {
    unsigned char start_of_frame;
    unsigned int size;
    unsigned int seq_num;
    unsigned int command;
    unsigned int reserve_1;
    unsigned int reserve_2;
    unsigned char user_name[10];
    unsigned char password[10];
    unsigned char crc;
} AuthorizedPacket;

typedef struct {
    unsigned char start_of_frame;
    unsigned int size;
    unsigned int seq_num;
    unsigned int command;
    unsigned int reserve_1;
    unsigned int reserve_2;
    unsigned int device;
    unsigned int scene_mode;        // 0: File Scene, 1: Dyn Gate, 2: Dyn Exp, 3: Dyn Comp
                                    // 4: Dyn Lim, 5: PEQ, 6: GEQ, 7:Effect 1, 8: Effect 2
    unsigned int list_index;
    unsigned char crc;
} GetFileListTxPacket;

typedef struct {
    unsigned int seq_num;
    unsigned int command;
    unsigned int result;            // 1: success, 0: fail
    unsigned int reserve_2;
    unsigned int file_count;        // 0~100, each subsequent filename is 12 bytes
    unsigned char crc;
} GetFileListRxPacket;

typedef struct {
    unsigned char start_of_frame;
    unsigned int size;
    unsigned int seq_num;
    unsigned int command;
    unsigned int scene_mode;        // 0: File Scene, 1: Dyn Gate, 2: Dyn Exp, 3: Dyn Comp
                                    // 4: Dyn Lim, 5: EQ Name, 6: Effect 1, 7: Effect 2
    unsigned int reserve_2;
    unsigned char file_name[10];
    unsigned char crc;
} FileSelectTxPacket;

typedef struct {
    unsigned char start_of_frame;
    unsigned int size;
    unsigned int seq_num;
    unsigned int command;
    unsigned int reserve_1;
    unsigned int reserve_2;
    unsigned int device;            // 0: flash, 1: USB storage
    unsigned int scene_mode;        // 0: SCENE, 1: DYN Gate, 2: DYN Expander, 3: DYN Compressor, 4: DYN Limit
                                    // 5: PEQ, 6: GEQ, 7: Effect 1, 8: Effect 2
    unsigned int channel_select;    // 0-15: Channel 1-16, 16-19: Multi 1-4, 20: Main, 21-22: Channel 17-18
    unsigned int access_mode;       // 0: Save, 1: Load, 2: Rename, 3: Delete
    unsigned char file_name[12];
    unsigned char new_file_name[12];
    unsigned char crc;
} SetFileAccessTxPacket;

typedef struct {
    unsigned int seq_num;
    unsigned int command;
    unsigned int result;
    unsigned int reserve_2;
    unsigned char crc;
} SetFileAccessRxPacket;

typedef struct {
    unsigned char start_of_frame;
    unsigned int size;
    unsigned int seq_num;
    unsigned int command;
    unsigned int audio_type;        // 0:main, 1:channel, 2:aux, 3:group, 4:multi
    unsigned int channel_num;       // channel = 0~17, aux = 0~3, group = 0~3, multi = 0~3
    unsigned char crc;
} GetAudioNameTxPacket;

typedef struct {
    unsigned int seq_num;
    unsigned int command;
    unsigned int result;
    unsigned int reserve_2;
    unsigned int audio_type;        // 0:main, 1:channel, 2:aux, 3:group, 4:multi
    unsigned int channel_num;       // channel = 0~17, aux = 0~3, group = 0~3, multi = 0~3
    unsigned char channel_name[10];
    unsigned char crc;
} GetAudioNameRxPacket;

typedef struct {
    unsigned char start_of_frame;
    unsigned int size;
    unsigned int seq_num;
    unsigned int command;
    unsigned int audio_type;        // 0:main, 1:channel, 2:aux, 3:group, 4:multi
    unsigned int channel_num;       // channel = 0~17, aux = 0~3, group = 0~3, multi = 0~3
    unsigned char channel_name[10];
    unsigned char crc;
} SetAudioNameTxPacket;

typedef struct {
    unsigned char start_of_frame;
    unsigned int size;
    unsigned int seq_num;
    unsigned int command;
    unsigned int page_num;
    unsigned int reserve_2;
    unsigned char crc;
} SetSendPageTxPacket;

typedef struct {
    unsigned char start_of_frame;
    unsigned int size;
    unsigned int seq_num;
    unsigned int command;
    unsigned int reserve_1;
    unsigned int reserve_2;
    unsigned char crc;
} GetFirmwareVersionTxPacket;

typedef struct {
    unsigned int seq_num;
    unsigned int command;
    unsigned int machine_type;
    unsigned int version;
    unsigned char name[10];
    unsigned char crc;
} GetFirmwareVersionRxPacket;

typedef struct {
    unsigned char start_of_frame;
    unsigned int size;
    unsigned int seq_num;
    unsigned int command;
    unsigned int reserve_1;
    unsigned int reserve_2;
    unsigned char crc;
} GetDelayTxPacket;

typedef struct {
    unsigned int seq_num;
    unsigned int command;
    unsigned int result;
    unsigned int reserve_2;
    unsigned int delay_scale;
    unsigned int delay_temp;
    unsigned char crc;
} GetDelayRxPacket;

typedef struct {
    unsigned char start_of_frame;
    unsigned int size;
    unsigned int seq_num;
    unsigned int command;
    unsigned int reserve_1;
    unsigned int reserve_2;
    unsigned int delay_scale;
    unsigned int delay_temp;
    unsigned char crc;
} SetDelayTxPacket;

typedef struct {
    unsigned int seq_num;
    unsigned int command;
    unsigned int result;
    unsigned int reserve_2;
    unsigned char crc;
} SetDelayRxPacket;

typedef struct {
    unsigned char start_of_frame;
    unsigned int size;
    unsigned int seq_num;
    unsigned int command;
    unsigned int reserve_1;
    unsigned int reserve_2;
    unsigned char crc;
} SetInitPacket;

typedef struct {
    unsigned char start_of_frame;
    unsigned int size;
    unsigned int seq_num;
    unsigned int command;
    unsigned int reserve_1;
    unsigned int reserve_2;
    unsigned int link_type;     // 0: Effect1 GEQ31, 1: Effect2 GEQ15
    unsigned int link_mode;     // 0: Off, 1: On
    unsigned char crc;
} SetGeqLinkModeTxPacket;

typedef struct {
    unsigned int seq_num;
    unsigned int command;
    unsigned int reserve_1;
    unsigned int reserve_2;
    SInt16 channel_1_meter_value;
    SInt16 channel_2_meter_value;
    SInt16 channel_3_meter_value;
    SInt16 channel_4_meter_value;
    SInt16 channel_5_meter_value;
    SInt16 channel_6_meter_value;
    SInt16 channel_7_meter_value;
    SInt16 channel_8_meter_value;
    SInt16 channel_9_meter_value;
    SInt16 channel_10_meter_value;
    SInt16 channel_11_meter_value;
    SInt16 channel_12_meter_value;
    SInt16 channel_13_meter_value;
    SInt16 channel_14_meter_value;
    SInt16 channel_15_meter_value;
    SInt16 channel_16_meter_value;
    SInt16 aux_1_meter_value;
    SInt16 aux_2_meter_value;
    SInt16 aux_3_meter_value;
    SInt16 aux_4_meter_value;
    SInt16 group_1_meter_value;
    SInt16 group_2_meter_value;
    SInt16 group_3_meter_value;
    SInt16 group_4_meter_value;
    SInt16 multi_1_meter_value;
    SInt16 multi_2_meter_value;
    SInt16 multi_3_meter_value;
    SInt16 multi_4_meter_value;
    SInt16 main_r_meter_value;
    SInt16 main_l_meter_value;
    SInt16 channel_17_meter_value;
    SInt16 channel_18_meter_value;
    SInt16 ctrl_r_meter_value;
    SInt16 ctrl_l_meter_value;
    SInt16 effect_1_in_r_meter_value;
    SInt16 effect_1_in_l_meter_value;
    SInt16 effect_1_out_r_meter_value;
    SInt16 effect_1_out_l_meter_value;
    SInt16 effect_2_in_r_meter_value;
    SInt16 effect_2_in_l_meter_value;
    SInt16 effect_2_out_r_meter_value;
    SInt16 effect_2_out_l_meter_value;
    SInt16 channel_17_out_meter_value;
    SInt16 channel_18_out_meter_value;
    SInt16 dsp_sample_rate;
    SInt16 usb_sample_rate;
    SInt16 dsp_status_3;
    SInt16 dsp_sets_clock_source;
    SInt16 delay_scale;
    SInt16 delay_temperature;
    SInt16 effect_1_geq_31;
    SInt16 effect_2_geq_15;
    SInt16 usb_state;
    unsigned char version[12];
    unsigned char main_audio_name[12];
    unsigned char ctrl_room_audio_name[12];
    unsigned char effect_1_audio_name[12];
    unsigned char effect_2_audio_name[12];
    unsigned char aux_1_audio_name[12];
    unsigned char aux_2_audio_name[12];
    unsigned char aux_3_audio_name[12];
    unsigned char aux_4_audio_name[12];
    unsigned char group_1_audio_name[12];
    unsigned char group_2_audio_name[12];
    unsigned char group_3_audio_name[12];
    unsigned char group_4_audio_name[12];
    unsigned char multi_1_audio_name[12];
    unsigned char multi_2_audio_name[12];
    unsigned char multi_3_audio_name[12];
    unsigned char multi_4_audio_name[12];
    unsigned char channel_1_audio_name[12];
    unsigned char channel_2_audio_name[12];
    unsigned char channel_3_audio_name[12];
    unsigned char channel_4_audio_name[12];
    unsigned char channel_5_audio_name[12];
    unsigned char channel_6_audio_name[12];
    unsigned char channel_7_audio_name[12];
    unsigned char channel_8_audio_name[12];
    unsigned char channel_9_audio_name[12];
    unsigned char channel_10_audio_name[12];
    unsigned char channel_11_audio_name[12];
    unsigned char channel_12_audio_name[12];
    unsigned char channel_13_audio_name[12];
    unsigned char channel_14_audio_name[12];
    unsigned char channel_15_audio_name[12];
    unsigned char channel_16_audio_name[12];
    unsigned char channel_17_audio_name[12];
    unsigned char channel_18_audio_name[12];
    SInt16 channel_1_4_dyn_active;
    SInt16 channel_5_8_dyn_active;
    SInt16 channel_9_12_dyn_active;
    SInt16 channel_13_16_dyn_active;
    SInt16 multi_1_4_dyn_active;
    SInt16 main_l_r_ch_17_18_dyn_active;
    unsigned char crc;
} AllMeterValueRxPacket;

typedef struct {
    unsigned int seq_num;
    unsigned int command;
    unsigned int page_type;
    unsigned int reserve_2;
    SInt16 channel_1_ctrl;
    SInt16 channel_2_ctrl;
    SInt16 channel_3_ctrl;
    SInt16 channel_4_ctrl;
    SInt16 channel_5_ctrl;
    SInt16 channel_6_ctrl;
    SInt16 channel_7_ctrl;
    SInt16 channel_8_ctrl;
    SInt16 channel_9_ctrl;
    SInt16 channel_10_ctrl;
    SInt16 channel_11_ctrl;
    SInt16 channel_12_ctrl;
    SInt16 channel_13_ctrl;
    SInt16 channel_14_ctrl;
    SInt16 channel_15_ctrl;
    SInt16 channel_16_ctrl;
    SInt16 channel_1_delay_time;
    SInt16 channel_2_delay_time;
    SInt16 channel_3_delay_time;
    SInt16 channel_4_delay_time;
    SInt16 channel_5_delay_time;
    SInt16 channel_6_delay_time;
    SInt16 channel_7_delay_time;
    SInt16 channel_8_delay_time;
    SInt16 channel_9_delay_time;
    SInt16 channel_10_delay_time;
    SInt16 channel_11_delay_time;
    SInt16 channel_12_delay_time;
    SInt16 channel_13_delay_time;
    SInt16 channel_14_delay_time;
    SInt16 channel_15_delay_time;
    SInt16 channel_16_delay_time;
    SInt16 channel_1_eq_meter_out;
    SInt16 channel_2_eq_meter_out;
    SInt16 channel_3_eq_meter_out;
    SInt16 channel_4_eq_meter_out;
    SInt16 channel_5_eq_meter_out;
    SInt16 channel_6_eq_meter_out;
    SInt16 channel_7_eq_meter_out;
    SInt16 channel_8_eq_meter_out;
    SInt16 channel_9_eq_meter_out;
    SInt16 channel_10_eq_meter_out;
    SInt16 channel_11_eq_meter_out;
    SInt16 channel_12_eq_meter_out;
    SInt16 channel_13_eq_meter_out;
    SInt16 channel_14_eq_meter_out;
    SInt16 channel_15_eq_meter_out;
    SInt16 channel_16_eq_meter_out;
    SInt16 channel_1_dyn_meter_out;
    SInt16 channel_2_dyn_meter_out;
    SInt16 channel_3_dyn_meter_out;
    SInt16 channel_4_dyn_meter_out;
    SInt16 channel_5_dyn_meter_out;
    SInt16 channel_6_dyn_meter_out;
    SInt16 channel_7_dyn_meter_out;
    SInt16 channel_8_dyn_meter_out;
    SInt16 channel_9_dyn_meter_out;
    SInt16 channel_10_dyn_meter_out;
    SInt16 channel_11_dyn_meter_out;
    SInt16 channel_12_dyn_meter_out;
    SInt16 channel_13_dyn_meter_out;
    SInt16 channel_14_dyn_meter_out;
    SInt16 channel_15_dyn_meter_out;
    SInt16 channel_16_dyn_meter_out;
    SInt16 channel_1_4_dyn_active;
    SInt16 channel_5_8_dyn_active;
    SInt16 channel_9_12_dyn_active;
    SInt16 channel_13_16_dyn_active;
    SInt16 channel_1_pan_level;
    SInt16 channel_2_pan_level;
    SInt16 channel_3_pan_level;
    SInt16 channel_4_pan_level;
    SInt16 channel_5_pan_level;
    SInt16 channel_6_pan_level;
    SInt16 channel_7_pan_level;
    SInt16 channel_8_pan_level;
    SInt16 channel_9_pan_level;
    SInt16 channel_10_pan_level;
    SInt16 channel_11_pan_level;
    SInt16 channel_12_pan_level;
    SInt16 channel_13_pan_level;
    SInt16 channel_14_pan_level;
    SInt16 channel_15_pan_level;
    SInt16 channel_16_pan_level;
    SInt16 aux_1_send_pre_post;
    SInt16 aux_2_send_pre_post;
    SInt16 aux_3_send_pre_post;
    SInt16 aux_4_send_pre_post;
    SInt16 aux_1_ch_1_fader_level;
    SInt16 aux_1_ch_2_fader_level;
    SInt16 aux_1_ch_3_fader_level;
    SInt16 aux_1_ch_4_fader_level;
    SInt16 aux_1_ch_5_fader_level;
    SInt16 aux_1_ch_6_fader_level;
    SInt16 aux_1_ch_7_fader_level;
    SInt16 aux_1_ch_8_fader_level;
    SInt16 aux_1_ch_9_fader_level;
    SInt16 aux_1_ch_10_fader_level;
    SInt16 aux_1_ch_11_fader_level;
    SInt16 aux_1_ch_12_fader_level;
    SInt16 aux_1_ch_13_fader_level;
    SInt16 aux_1_ch_14_fader_level;
    SInt16 aux_1_ch_15_fader_level;
    SInt16 aux_1_ch_16_fader_level;
    SInt16 aux_2_ch_1_fader_level;
    SInt16 aux_2_ch_2_fader_level;
    SInt16 aux_2_ch_3_fader_level;
    SInt16 aux_2_ch_4_fader_level;
    SInt16 aux_2_ch_5_fader_level;
    SInt16 aux_2_ch_6_fader_level;
    SInt16 aux_2_ch_7_fader_level;
    SInt16 aux_2_ch_8_fader_level;
    SInt16 aux_2_ch_9_fader_level;
    SInt16 aux_2_ch_10_fader_level;
    SInt16 aux_2_ch_11_fader_level;
    SInt16 aux_2_ch_12_fader_level;
    SInt16 aux_2_ch_13_fader_level;
    SInt16 aux_2_ch_14_fader_level;
    SInt16 aux_2_ch_15_fader_level;
    SInt16 aux_2_ch_16_fader_level;
    SInt16 aux_3_ch_1_fader_level;
    SInt16 aux_3_ch_2_fader_level;
    SInt16 aux_3_ch_3_fader_level;
    SInt16 aux_3_ch_4_fader_level;
    SInt16 aux_3_ch_5_fader_level;
    SInt16 aux_3_ch_6_fader_level;
    SInt16 aux_3_ch_7_fader_level;
    SInt16 aux_3_ch_8_fader_level;
    SInt16 aux_3_ch_9_fader_level;
    SInt16 aux_3_ch_10_fader_level;
    SInt16 aux_3_ch_11_fader_level;
    SInt16 aux_3_ch_12_fader_level;
    SInt16 aux_3_ch_13_fader_level;
    SInt16 aux_3_ch_14_fader_level;
    SInt16 aux_3_ch_15_fader_level;
    SInt16 aux_3_ch_16_fader_level;
    SInt16 aux_4_ch_1_fader_level;
    SInt16 aux_4_ch_2_fader_level;
    SInt16 aux_4_ch_3_fader_level;
    SInt16 aux_4_ch_4_fader_level;
    SInt16 aux_4_ch_5_fader_level;
    SInt16 aux_4_ch_6_fader_level;
    SInt16 aux_4_ch_7_fader_level;
    SInt16 aux_4_ch_8_fader_level;
    SInt16 aux_4_ch_9_fader_level;
    SInt16 aux_4_ch_10_fader_level;
    SInt16 aux_4_ch_11_fader_level;
    SInt16 aux_4_ch_12_fader_level;
    SInt16 aux_4_ch_13_fader_level;
    SInt16 aux_4_ch_14_fader_level;
    SInt16 aux_4_ch_15_fader_level;
    SInt16 aux_4_ch_16_fader_level;
    SInt16 ch_1_16_assign_gp_1;
    SInt16 ch_1_16_assign_gp_2;
    SInt16 ch_1_16_assign_gp_3;
    SInt16 ch_1_16_assign_gp_4;
    SInt16 ch_1_fader_level;
    SInt16 ch_2_fader_level;
    SInt16 ch_3_fader_level;
    SInt16 ch_4_fader_level;
    SInt16 ch_5_fader_level;
    SInt16 ch_6_fader_level;
    SInt16 ch_7_fader_level;
    SInt16 ch_8_fader_level;
    SInt16 ch_9_fader_level;
    SInt16 ch_10_fader_level;
    SInt16 ch_11_fader_level;
    SInt16 ch_12_fader_level;
    SInt16 ch_13_fader_level;
    SInt16 ch_14_fader_level;
    SInt16 ch_15_fader_level;
    SInt16 ch_16_fader_level;
    SInt16 ch_1_16_on_off;
    SInt16 ch_1_16_solo_ctrl_on_off;
    SInt16 ch_1_16_solo_safe_ctrl_on_off;
    SInt16 ch_1_16_meter_pre_post;
    SInt16 ch_1_16_to_main_ctrl;
    SInt16 ch_1_eq_1_db;
    SInt16 ch_1_eq_2_db;
    SInt16 ch_1_eq_3_db;
    SInt16 ch_1_eq_4_db;
    SInt16 ch_2_eq_1_db;
    SInt16 ch_2_eq_2_db;
    SInt16 ch_2_eq_3_db;
    SInt16 ch_2_eq_4_db;
    SInt16 ch_3_eq_1_db;
    SInt16 ch_3_eq_2_db;
    SInt16 ch_3_eq_3_db;
    SInt16 ch_3_eq_4_db;
    SInt16 ch_4_eq_1_db;
    SInt16 ch_4_eq_2_db;
    SInt16 ch_4_eq_3_db;
    SInt16 ch_4_eq_4_db;
    SInt16 ch_5_eq_1_db;
    SInt16 ch_5_eq_2_db;
    SInt16 ch_5_eq_3_db;
    SInt16 ch_5_eq_4_db;
    SInt16 ch_6_eq_1_db;
    SInt16 ch_6_eq_2_db;
    SInt16 ch_6_eq_3_db;
    SInt16 ch_6_eq_4_db;
    SInt16 ch_7_eq_1_db;
    SInt16 ch_7_eq_2_db;
    SInt16 ch_7_eq_3_db;
    SInt16 ch_7_eq_4_db;
    SInt16 ch_8_eq_1_db;
    SInt16 ch_8_eq_2_db;
    SInt16 ch_8_eq_3_db;
    SInt16 ch_8_eq_4_db;
    SInt16 ch_9_eq_1_db;
    SInt16 ch_9_eq_2_db;
    SInt16 ch_9_eq_3_db;
    SInt16 ch_9_eq_4_db;
    SInt16 ch_10_eq_1_db;
    SInt16 ch_10_eq_2_db;
    SInt16 ch_10_eq_3_db;
    SInt16 ch_10_eq_4_db;
    SInt16 ch_11_eq_1_db;
    SInt16 ch_11_eq_2_db;
    SInt16 ch_11_eq_3_db;
    SInt16 ch_11_eq_4_db;
    SInt16 ch_12_eq_1_db;
    SInt16 ch_12_eq_2_db;
    SInt16 ch_12_eq_3_db;
    SInt16 ch_12_eq_4_db;
    SInt16 ch_13_eq_1_db;
    SInt16 ch_13_eq_2_db;
    SInt16 ch_13_eq_3_db;
    SInt16 ch_13_eq_4_db;
    SInt16 ch_14_eq_1_db;
    SInt16 ch_14_eq_2_db;
    SInt16 ch_14_eq_3_db;
    SInt16 ch_14_eq_4_db;
    SInt16 ch_15_eq_1_db;
    SInt16 ch_15_eq_2_db;
    SInt16 ch_15_eq_3_db;
    SInt16 ch_15_eq_4_db;
    SInt16 ch_16_eq_1_db;
    SInt16 ch_16_eq_2_db;
    SInt16 ch_16_eq_3_db;
    SInt16 ch_16_eq_4_db;
    SInt16 ch_1_eq_1_q;
    SInt16 ch_1_eq_2_q;
    SInt16 ch_1_eq_3_q;
    SInt16 ch_1_eq_4_q;
    SInt16 ch_2_eq_1_q;
    SInt16 ch_2_eq_2_q;
    SInt16 ch_2_eq_3_q;
    SInt16 ch_2_eq_4_q;
    SInt16 ch_3_eq_1_q;
    SInt16 ch_3_eq_2_q;
    SInt16 ch_3_eq_3_q;
    SInt16 ch_3_eq_4_q;
    SInt16 ch_4_eq_1_q;
    SInt16 ch_4_eq_2_q;
    SInt16 ch_4_eq_3_q;
    SInt16 ch_4_eq_4_q;
    SInt16 ch_5_eq_1_q;
    SInt16 ch_5_eq_2_q;
    SInt16 ch_5_eq_3_q;
    SInt16 ch_5_eq_4_q;
    SInt16 ch_6_eq_1_q;
    SInt16 ch_6_eq_2_q;
    SInt16 ch_6_eq_3_q;
    SInt16 ch_6_eq_4_q;
    SInt16 ch_7_eq_1_q;
    SInt16 ch_7_eq_2_q;
    SInt16 ch_7_eq_3_q;
    SInt16 ch_7_eq_4_q;
    SInt16 ch_8_eq_1_q;
    SInt16 ch_8_eq_2_q;
    SInt16 ch_8_eq_3_q;
    SInt16 ch_8_eq_4_q;
    SInt16 ch_9_eq_1_q;
    SInt16 ch_9_eq_2_q;
    SInt16 ch_9_eq_3_q;
    SInt16 ch_9_eq_4_q;
    SInt16 ch_10_eq_1_q;
    SInt16 ch_10_eq_2_q;
    SInt16 ch_10_eq_3_q;
    SInt16 ch_10_eq_4_q;
    SInt16 ch_11_eq_1_q;
    SInt16 ch_11_eq_2_q;
    SInt16 ch_11_eq_3_q;
    SInt16 ch_11_eq_4_q;
    SInt16 ch_12_eq_1_q;
    SInt16 ch_12_eq_2_q;
    SInt16 ch_12_eq_3_q;
    SInt16 ch_12_eq_4_q;
    SInt16 ch_13_eq_1_q;
    SInt16 ch_13_eq_2_q;
    SInt16 ch_13_eq_3_q;
    SInt16 ch_13_eq_4_q;
    SInt16 ch_14_eq_1_q;
    SInt16 ch_14_eq_2_q;
    SInt16 ch_14_eq_3_q;
    SInt16 ch_14_eq_4_q;
    SInt16 ch_15_eq_1_q;
    SInt16 ch_15_eq_2_q;
    SInt16 ch_15_eq_3_q;
    SInt16 ch_15_eq_4_q;
    SInt16 ch_16_eq_1_q;
    SInt16 ch_16_eq_2_q;
    SInt16 ch_16_eq_3_q;
    SInt16 ch_16_eq_4_q;
    SInt16 ch_1_eq_1_freq;
    SInt16 ch_1_eq_2_freq;
    SInt16 ch_1_eq_3_freq;
    SInt16 ch_1_eq_4_freq;
    SInt16 ch_2_eq_1_freq;
    SInt16 ch_2_eq_2_freq;
    SInt16 ch_2_eq_3_freq;
    SInt16 ch_2_eq_4_freq;
    SInt16 ch_3_eq_1_freq;
    SInt16 ch_3_eq_2_freq;
    SInt16 ch_3_eq_3_freq;
    SInt16 ch_3_eq_4_freq;
    SInt16 ch_4_eq_1_freq;
    SInt16 ch_4_eq_2_freq;
    SInt16 ch_4_eq_3_freq;
    SInt16 ch_4_eq_4_freq;
    SInt16 ch_5_eq_1_freq;
    SInt16 ch_5_eq_2_freq;
    SInt16 ch_5_eq_3_freq;
    SInt16 ch_5_eq_4_freq;
    SInt16 ch_6_eq_1_freq;
    SInt16 ch_6_eq_2_freq;
    SInt16 ch_6_eq_3_freq;
    SInt16 ch_6_eq_4_freq;
    SInt16 ch_7_eq_1_freq;
    SInt16 ch_7_eq_2_freq;
    SInt16 ch_7_eq_3_freq;
    SInt16 ch_7_eq_4_freq;
    SInt16 ch_8_eq_1_freq;
    SInt16 ch_8_eq_2_freq;
    SInt16 ch_8_eq_3_freq;
    SInt16 ch_8_eq_4_freq;
    SInt16 ch_9_eq_1_freq;
    SInt16 ch_9_eq_2_freq;
    SInt16 ch_9_eq_3_freq;
    SInt16 ch_9_eq_4_freq;
    SInt16 ch_10_eq_1_freq;
    SInt16 ch_10_eq_2_freq;
    SInt16 ch_10_eq_3_freq;
    SInt16 ch_10_eq_4_freq;
    SInt16 ch_11_eq_1_freq;
    SInt16 ch_11_eq_2_freq;
    SInt16 ch_11_eq_3_freq;
    SInt16 ch_11_eq_4_freq;
    SInt16 ch_12_eq_1_freq;
    SInt16 ch_12_eq_2_freq;
    SInt16 ch_12_eq_3_freq;
    SInt16 ch_12_eq_4_freq;
    SInt16 ch_13_eq_1_freq;
    SInt16 ch_13_eq_2_freq;
    SInt16 ch_13_eq_3_freq;
    SInt16 ch_13_eq_4_freq;
    SInt16 ch_14_eq_1_freq;
    SInt16 ch_14_eq_2_freq;
    SInt16 ch_14_eq_3_freq;
    SInt16 ch_14_eq_4_freq;
    SInt16 ch_15_eq_1_freq;
    SInt16 ch_15_eq_2_freq;
    SInt16 ch_15_eq_3_freq;
    SInt16 ch_15_eq_4_freq;
    SInt16 ch_16_eq_1_freq;
    SInt16 ch_16_eq_2_freq;
    SInt16 ch_16_eq_3_freq;
    SInt16 ch_16_eq_4_freq;
    SInt16 ch_1_gate_range;
    SInt16 ch_2_gate_range;
    SInt16 ch_3_gate_range;
    SInt16 ch_4_gate_range;
    SInt16 ch_1_gate_threshold;
    SInt16 ch_2_gate_threshold;
    SInt16 ch_3_gate_threshold;
    SInt16 ch_4_gate_threshold;
    SInt16 ch_1_gate_attack;
    SInt16 ch_2_gate_attack;
    SInt16 ch_3_gate_attack;
    SInt16 ch_4_gate_attack;
    SInt16 ch_1_gate_release;
    SInt16 ch_2_gate_release;
    SInt16 ch_3_gate_release;
    SInt16 ch_4_gate_release;
    SInt16 ch_1_gate_hold;
    SInt16 ch_2_gate_hold;
    SInt16 ch_3_gate_hold;
    SInt16 ch_4_gate_hold;
    SInt16 ch_1_expand_ratio;
    SInt16 ch_2_expand_ratio;
    SInt16 ch_3_expand_ratio;
    SInt16 ch_4_expand_ratio;
    SInt16 ch_1_expand_threshold;
    SInt16 ch_2_expand_threshold;
    SInt16 ch_3_expand_threshold;
    SInt16 ch_4_expand_threshold;
    SInt16 ch_1_expand_attack;
    SInt16 ch_2_expand_attack;
    SInt16 ch_3_expand_attack;
    SInt16 ch_4_expand_attack;
    SInt16 ch_1_expand_release;
    SInt16 ch_2_expand_release;
    SInt16 ch_3_expand_release;
    SInt16 ch_4_expand_release;
    SInt16 ch_1_compressor_ratio;
    SInt16 ch_2_compressor_ratio;
    SInt16 ch_3_compressor_ratio;
    SInt16 ch_4_compressor_ratio;
    SInt16 ch_1_compressor_threshold;
    SInt16 ch_2_compressor_threshold;
    SInt16 ch_3_compressor_threshold;
    SInt16 ch_4_compressor_threshold;
    SInt16 ch_1_compressor_attack;
    SInt16 ch_2_compressor_attack;
    SInt16 ch_3_compressor_attack;
    SInt16 ch_4_compressor_attack;
    SInt16 ch_1_compressor_release;
    SInt16 ch_2_compressor_release;
    SInt16 ch_3_compressor_release;
    SInt16 ch_4_compressor_release;
    SInt16 ch_1_compressor_out_gain;
    SInt16 ch_2_compressor_out_gain;
    SInt16 ch_3_compressor_out_gain;
    SInt16 ch_4_compressor_out_gain;
    SInt16 ch_1_limiter_threshold;
    SInt16 ch_2_limiter_threshold;
    SInt16 ch_3_limiter_threshold;
    SInt16 ch_4_limiter_threshold;
    SInt16 ch_1_limiter_attack;
    SInt16 ch_2_limiter_attack;
    SInt16 ch_3_limiter_attack;
    SInt16 ch_4_limiter_attack;
    SInt16 ch_1_limiter_release;
    SInt16 ch_2_limiter_release;
    SInt16 ch_3_limiter_release;
    SInt16 ch_4_limiter_release;
    SInt16 ch_5_gate_range;
    SInt16 ch_6_gate_range;
    SInt16 ch_7_gate_range;
    SInt16 ch_8_gate_range;
    SInt16 ch_5_gate_threshold;
    SInt16 ch_6_gate_threshold;
    SInt16 ch_7_gate_threshold;
    SInt16 ch_8_gate_threshold;
    SInt16 ch_5_gate_attack;
    SInt16 ch_6_gate_attack;
    SInt16 ch_7_gate_attack;
    SInt16 ch_8_gate_attack;
    SInt16 ch_5_gate_release;
    SInt16 ch_6_gate_release;
    SInt16 ch_7_gate_release;
    SInt16 ch_8_gate_release;
    SInt16 ch_5_gate_hold;
    SInt16 ch_6_gate_hold;
    SInt16 ch_7_gate_hold;
    SInt16 ch_8_gate_hold;
    SInt16 ch_5_expand_ratio;
    SInt16 ch_6_expand_ratio;
    SInt16 ch_7_expand_ratio;
    SInt16 ch_8_expand_ratio;
    SInt16 ch_5_expand_threshold;
    SInt16 ch_6_expand_threshold;
    SInt16 ch_7_expand_threshold;
    SInt16 ch_8_expand_threshold;
    SInt16 ch_5_expand_attack;
    SInt16 ch_6_expand_attack;
    SInt16 ch_7_expand_attack;
    SInt16 ch_8_expand_attack;
    SInt16 ch_5_expand_release;
    SInt16 ch_6_expand_release;
    SInt16 ch_7_expand_release;
    SInt16 ch_8_expand_release;
    SInt16 ch_5_compressor_ratio;
    SInt16 ch_6_compressor_ratio;
    SInt16 ch_7_compressor_ratio;
    SInt16 ch_8_compressor_ratio;
    SInt16 ch_5_compressor_threshold;
    SInt16 ch_6_compressor_threshold;
    SInt16 ch_7_compressor_threshold;
    SInt16 ch_8_compressor_threshold;
    SInt16 ch_5_compressor_attack;
    SInt16 ch_6_compressor_attack;
    SInt16 ch_7_compressor_attack;
    SInt16 ch_8_compressor_attack;
    SInt16 ch_5_compressor_release;
    SInt16 ch_6_compressor_release;
    SInt16 ch_7_compressor_release;
    SInt16 ch_8_compressor_release;
    SInt16 ch_5_compressor_out_gain;
    SInt16 ch_6_compressor_out_gain;
    SInt16 ch_7_compressor_out_gain;
    SInt16 ch_8_compressor_out_gain;
    SInt16 ch_5_limiter_threshold;
    SInt16 ch_6_limiter_threshold;
    SInt16 ch_7_limiter_threshold;
    SInt16 ch_8_limiter_threshold;
    SInt16 ch_5_limiter_attack;
    SInt16 ch_6_limiter_attack;
    SInt16 ch_7_limiter_attack;
    SInt16 ch_8_limiter_attack;
    SInt16 ch_5_limiter_release;
    SInt16 ch_6_limiter_release;
    SInt16 ch_7_limiter_release;
    SInt16 ch_8_limiter_release;
    SInt16 ch_9_gate_range;
    SInt16 ch_10_gate_range;
    SInt16 ch_11_gate_range;
    SInt16 ch_12_gate_range;
    SInt16 ch_9_gate_threshold;
    SInt16 ch_10_gate_threshold;
    SInt16 ch_11_gate_threshold;
    SInt16 ch_12_gate_threshold;
    SInt16 ch_9_gate_attack;
    SInt16 ch_10_gate_attack;
    SInt16 ch_11_gate_attack;
    SInt16 ch_12_gate_attack;
    SInt16 ch_9_gate_release;
    SInt16 ch_10_gate_release;
    SInt16 ch_11_gate_release;
    SInt16 ch_12_gate_release;
    SInt16 ch_9_gate_hold;
    SInt16 ch_10_gate_hold;
    SInt16 ch_11_gate_hold;
    SInt16 ch_12_gate_hold;
    SInt16 ch_9_expand_ratio;
    SInt16 ch_10_expand_ratio;
    SInt16 ch_11_expand_ratio;
    SInt16 ch_12_expand_ratio;
    SInt16 ch_9_expand_threshold;
    SInt16 ch_10_expand_threshold;
    SInt16 ch_11_expand_threshold;
    SInt16 ch_12_expand_threshold;
    SInt16 ch_9_expand_attack;
    SInt16 ch_10_expand_attack;
    SInt16 ch_11_expand_attack;
    SInt16 ch_12_expand_attack;
    SInt16 ch_9_expand_release;
    SInt16 ch_10_expand_release;
    SInt16 ch_11_expand_release;
    SInt16 ch_12_expand_release;
    SInt16 ch_9_compressor_ratio;
    SInt16 ch_10_compressor_ratio;
    SInt16 ch_11_compressor_ratio;
    SInt16 ch_12_compressor_ratio;
    SInt16 ch_9_compressor_threshold;
    SInt16 ch_10_compressor_threshold;
    SInt16 ch_11_compressor_threshold;
    SInt16 ch_12_compressor_threshold;
    SInt16 ch_9_compressor_attack;
    SInt16 ch_10_compressor_attack;
    SInt16 ch_11_compressor_attack;
    SInt16 ch_12_compressor_attack;
    SInt16 ch_9_compressor_release;
    SInt16 ch_10_compressor_release;
    SInt16 ch_11_compressor_release;
    SInt16 ch_12_compressor_release;
    SInt16 ch_9_compressor_out_gain;
    SInt16 ch_10_compressor_out_gain;
    SInt16 ch_11_compressor_out_gain;
    SInt16 ch_12_compressor_out_gain;
    SInt16 ch_9_limiter_threshold;
    SInt16 ch_10_limiter_threshold;
    SInt16 ch_11_limiter_threshold;
    SInt16 ch_12_limiter_threshold;
    SInt16 ch_9_limiter_attack;
    SInt16 ch_10_limiter_attack;
    SInt16 ch_11_limiter_attack;
    SInt16 ch_12_limiter_attack;
    SInt16 ch_9_limiter_release;
    SInt16 ch_10_limiter_release;
    SInt16 ch_11_limiter_release;
    SInt16 ch_12_limiter_release;
    SInt16 ch_13_gate_range;
    SInt16 ch_14_gate_range;
    SInt16 ch_15_gate_range;
    SInt16 ch_16_gate_range;
    SInt16 ch_13_gate_threshold;
    SInt16 ch_14_gate_threshold;
    SInt16 ch_15_gate_threshold;
    SInt16 ch_16_gate_threshold;
    SInt16 ch_13_gate_attack;
    SInt16 ch_14_gate_attack;
    SInt16 ch_15_gate_attack;
    SInt16 ch_16_gate_attack;
    SInt16 ch_13_gate_release;
    SInt16 ch_14_gate_release;
    SInt16 ch_15_gate_release;
    SInt16 ch_16_gate_release;
    SInt16 ch_13_gate_hold;
    SInt16 ch_14_gate_hold;
    SInt16 ch_15_gate_hold;
    SInt16 ch_16_gate_hold;
    SInt16 ch_13_expand_ratio;
    SInt16 ch_14_expand_ratio;
    SInt16 ch_15_expand_ratio;
    SInt16 ch_16_expand_ratio;
    SInt16 ch_13_expand_threshold;
    SInt16 ch_14_expand_threshold;
    SInt16 ch_15_expand_threshold;
    SInt16 ch_16_expand_threshold;
    SInt16 ch_13_expand_attack;
    SInt16 ch_14_expand_attack;
    SInt16 ch_15_expand_attack;
    SInt16 ch_16_expand_attack;
    SInt16 ch_13_expand_release;
    SInt16 ch_14_expand_release;
    SInt16 ch_15_expand_release;
    SInt16 ch_16_expand_release;
    SInt16 ch_13_compressor_ratio;
    SInt16 ch_14_compressor_ratio;
    SInt16 ch_15_compressor_ratio;
    SInt16 ch_16_compressor_ratio;
    SInt16 ch_13_compressor_threshold;
    SInt16 ch_14_compressor_threshold;
    SInt16 ch_15_compressor_threshold;
    SInt16 ch_16_compressor_threshold;
    SInt16 ch_13_compressor_attack;
    SInt16 ch_14_compressor_attack;
    SInt16 ch_15_compressor_attack;
    SInt16 ch_16_compressor_attack;
    SInt16 ch_13_compressor_release;
    SInt16 ch_14_compressor_release;
    SInt16 ch_15_compressor_release;
    SInt16 ch_16_compressor_release;
    SInt16 ch_13_compressor_out_gain;
    SInt16 ch_14_compressor_out_gain;
    SInt16 ch_15_compressor_out_gain;
    SInt16 ch_16_compressor_out_gain;
    SInt16 ch_13_limiter_threshold;
    SInt16 ch_14_limiter_threshold;
    SInt16 ch_15_limiter_threshold;
    SInt16 ch_16_limiter_threshold;
    SInt16 ch_13_limiter_attack;
    SInt16 ch_14_limiter_attack;
    SInt16 ch_15_limiter_attack;
    SInt16 ch_16_limiter_attack;
    SInt16 ch_13_limiter_release;
    SInt16 ch_14_limiter_release;
    SInt16 ch_15_limiter_release;
    SInt16 ch_16_limiter_release;
    unsigned char crc;
} ViewPageChannelPacket;

typedef struct {
    unsigned int seq_num;
    unsigned int command;
    unsigned int page_type;
    unsigned int reserve_2;
    SInt16 ch_17_audio_setting;
    SInt16 ch_18_audio_setting;
    SInt16 ch_17_pan;
    SInt16 ch_18_pan;
    SInt16 ch_17_fader;
    SInt16 ch_18_fader;
    SInt16 aux_1_ch_17_level;
    SInt16 aux_1_ch_18_level;
    SInt16 aux_2_ch_17_level;
    SInt16 aux_2_ch_18_level;
    SInt16 aux_3_ch_17_level;
    SInt16 aux_3_ch_18_level;
    SInt16 aux_4_ch_17_level;
    SInt16 aux_4_ch_18_level;
    SInt16 ch_17_ctrl;
    SInt16 ch_18_ctrl;
    SInt16 ch_17_delay_time;
    SInt16 ch_18_delay_time;
    SInt16 main_l_r_ch_17_18_dyn_status;
    SInt16 ch_17_meter_dyn_out;
    SInt16 ch_18_meter_dyn_out;
    SInt16 ch_17_meter_eq_out;
    SInt16 ch_18_meter_eq_out;
    SInt16 ch_17_eq_1_db;
    SInt16 ch_17_eq_2_db;
    SInt16 ch_17_eq_3_db;
    SInt16 ch_17_eq_4_db;
    SInt16 ch_17_eq_1_q;
    SInt16 ch_17_eq_2_q;
    SInt16 ch_17_eq_3_q;
    SInt16 ch_17_eq_4_q;
    SInt16 ch_17_eq_1_freq;
    SInt16 ch_17_eq_2_freq;
    SInt16 ch_17_eq_3_freq;
    SInt16 ch_17_eq_4_freq;
    SInt16 ch_18_eq_1_db;
    SInt16 ch_18_eq_2_db;
    SInt16 ch_18_eq_3_db;
    SInt16 ch_18_eq_4_db;
    SInt16 ch_18_eq_1_q;
    SInt16 ch_18_eq_2_q;
    SInt16 ch_18_eq_3_q;
    SInt16 ch_18_eq_4_q;
    SInt16 ch_18_eq_1_freq;
    SInt16 ch_18_eq_2_freq;
    SInt16 ch_18_eq_3_freq;
    SInt16 ch_18_eq_4_freq;
    SInt16 ch_17_gate_range;
    SInt16 ch_17_gate_threshold;
    SInt16 ch_17_gate_attack;
    SInt16 ch_17_gate_release;
    SInt16 ch_17_gate_hold;
    SInt16 ch_17_expand_ratio;
    SInt16 ch_17_expand_threshold;
    SInt16 ch_17_expand_attack;
    SInt16 ch_17_expand_release;
    SInt16 ch_17_compressor_ratio;
    SInt16 ch_17_compressor_threshold;
    SInt16 ch_17_compressor_attack;
    SInt16 ch_17_compressor_release;
    SInt16 ch_17_compressor_out_gain;
    SInt16 ch_17_limiter_threshold;
    SInt16 ch_17_limiter_attack;
    SInt16 ch_17_limiter_release;
    SInt16 ch_18_gate_range;
    SInt16 ch_18_gate_threshold;
    SInt16 ch_18_gate_attack;
    SInt16 ch_18_gate_release;
    SInt16 ch_18_gate_hold;
    SInt16 ch_18_expand_ratio;
    SInt16 ch_18_expand_threshold;
    SInt16 ch_18_expand_attack;
    SInt16 ch_18_expand_release;
    SInt16 ch_18_compressor_ratio;
    SInt16 ch_18_compressor_threshold;
    SInt16 ch_18_compressor_attack;
    SInt16 ch_18_compressor_release;
    SInt16 ch_18_compressor_out_gain;
    SInt16 ch_18_limiter_threshold;
    SInt16 ch_18_limiter_attack;
    SInt16 ch_18_limiter_release;
    unsigned char crc;
} ViewPageUsbAudioPacket;

typedef struct {
    unsigned int seq_num;
    unsigned int command;
    unsigned int page_type;
    unsigned int reserve_2;
    SInt16 multi_1_ctrl;
    SInt16 multi_2_ctrl;
    SInt16 multi_3_ctrl;
    SInt16 multi_4_ctrl;
    SInt16 multi_1_delay_time;
    SInt16 multi_2_delay_time;
    SInt16 multi_3_delay_time;
    SInt16 multi_4_delay_time;
    SInt16 multi_1_meter_eq_out;
    SInt16 multi_2_meter_eq_out;
    SInt16 multi_3_meter_eq_out;
    SInt16 multi_4_meter_eq_out;
    SInt16 multi_1_meter_dyn_out;
    SInt16 multi_2_meter_dyn_out;
    SInt16 multi_3_meter_dyn_out;
    SInt16 multi_4_meter_dyn_out;
    SInt16 multi_1_4_dyn_status;
    SInt16 effect_control;
    SInt16 multi_1_4_meter_pre_post;
    SInt16 multi_1_fader;
    SInt16 multi_2_fader;
    SInt16 multi_3_fader;
    SInt16 multi_4_fader;
    SInt16 multi_1_source;
    SInt16 multi_2_source;
    SInt16 multi_3_source;
    SInt16 multi_4_source;
    SInt16 multi_1_eq_1_db;
    SInt16 multi_1_eq_2_db;
    SInt16 multi_1_eq_3_db;
    SInt16 multi_1_eq_4_db;
    SInt16 multi_2_eq_1_db;
    SInt16 multi_2_eq_2_db;
    SInt16 multi_2_eq_3_db;
    SInt16 multi_2_eq_4_db;
    SInt16 multi_3_eq_1_db;
    SInt16 multi_3_eq_2_db;
    SInt16 multi_3_eq_3_db;
    SInt16 multi_3_eq_4_db;
    SInt16 multi_4_eq_1_db;
    SInt16 multi_4_eq_2_db;
    SInt16 multi_4_eq_3_db;
    SInt16 multi_4_eq_4_db;
    SInt16 multi_1_eq_1_q;
    SInt16 multi_1_eq_2_q;
    SInt16 multi_1_eq_3_q;
    SInt16 multi_1_eq_4_q;
    SInt16 multi_2_eq_1_q;
    SInt16 multi_2_eq_2_q;
    SInt16 multi_2_eq_3_q;
    SInt16 multi_2_eq_4_q;
    SInt16 multi_3_eq_1_q;
    SInt16 multi_3_eq_2_q;
    SInt16 multi_3_eq_3_q;
    SInt16 multi_3_eq_4_q;
    SInt16 multi_4_eq_1_q;
    SInt16 multi_4_eq_2_q;
    SInt16 multi_4_eq_3_q;
    SInt16 multi_4_eq_4_q;
    SInt16 multi_1_eq_1_freq;
    SInt16 multi_1_eq_2_freq;
    SInt16 multi_1_eq_3_freq;
    SInt16 multi_1_eq_4_freq;
    SInt16 multi_2_eq_1_freq;
    SInt16 multi_2_eq_2_freq;
    SInt16 multi_2_eq_3_freq;
    SInt16 multi_2_eq_4_freq;
    SInt16 multi_3_eq_1_freq;
    SInt16 multi_3_eq_2_freq;
    SInt16 multi_3_eq_3_freq;
    SInt16 multi_3_eq_4_freq;
    SInt16 multi_4_eq_1_freq;
    SInt16 multi_4_eq_2_freq;
    SInt16 multi_4_eq_3_freq;
    SInt16 multi_4_eq_4_freq;
    SInt16 multi_1_gate_range;
    SInt16 multi_2_gate_range;
    SInt16 multi_3_gate_range;
    SInt16 multi_4_gate_range;
    SInt16 multi_1_gate_threshold;
    SInt16 multi_2_gate_threshold;
    SInt16 multi_3_gate_threshold;
    SInt16 multi_4_gate_threshold;
    SInt16 multi_1_gate_attack;
    SInt16 multi_2_gate_attack;
    SInt16 multi_3_gate_attack;
    SInt16 multi_4_gate_attack;
    SInt16 multi_1_gate_release;
    SInt16 multi_2_gate_release;
    SInt16 multi_3_gate_release;
    SInt16 multi_4_gate_release;
    SInt16 multi_1_gate_hold;
    SInt16 multi_2_gate_hold;
    SInt16 multi_3_gate_hold;
    SInt16 multi_4_gate_hold;
    SInt16 multi_1_expand_ratio;
    SInt16 multi_2_expand_ratio;
    SInt16 multi_3_expand_ratio;
    SInt16 multi_4_expand_ratio;
    SInt16 multi_1_expand_threshold;
    SInt16 multi_2_expand_threshold;
    SInt16 multi_3_expand_threshold;
    SInt16 multi_4_expand_threshold;
    SInt16 multi_1_expand_attack;
    SInt16 multi_2_expand_attack;
    SInt16 multi_3_expand_attack;
    SInt16 multi_4_expand_attack;
    SInt16 multi_1_expand_release;
    SInt16 multi_2_expand_release;
    SInt16 multi_3_expand_release;
    SInt16 multi_4_expand_release;
    SInt16 multi_1_compressor_ratio;
    SInt16 multi_2_compressor_ratio;
    SInt16 multi_3_compressor_ratio;
    SInt16 multi_4_compressor_ratio;
    SInt16 multi_1_compressor_threshold;
    SInt16 multi_2_compressor_threshold;
    SInt16 multi_3_compressor_threshold;
    SInt16 multi_4_compressor_threshold;
    SInt16 multi_1_compressor_attack;
    SInt16 multi_2_compressor_attack;
    SInt16 multi_3_compressor_attack;
    SInt16 multi_4_compressor_attack;
    SInt16 multi_1_compressor_release;
    SInt16 multi_2_compressor_release;
    SInt16 multi_3_compressor_release;
    SInt16 multi_4_compressor_release;
    SInt16 multi_1_compressor_out_gain;
    SInt16 multi_2_compressor_out_gain;
    SInt16 multi_3_compressor_out_gain;
    SInt16 multi_4_compressor_out_gain;
    SInt16 multi_1_limiter_threshold;
    SInt16 multi_2_limiter_threshold;
    SInt16 multi_3_limiter_threshold;
    SInt16 multi_4_limiter_threshold;
    SInt16 multi_1_limiter_attack;
    SInt16 multi_2_limiter_attack;
    SInt16 multi_3_limiter_attack;
    SInt16 multi_4_limiter_attack;
    SInt16 multi_1_limiter_release;
    SInt16 multi_2_limiter_release;
    SInt16 multi_3_limiter_release;
    SInt16 multi_4_limiter_release;
    unsigned char crc;
} ViewPageMultiPacket;

typedef struct {
    unsigned int seq_num;
    unsigned int command;
    unsigned int page_type;
    unsigned int reserve_2;
    SInt16 main_l_r_ctrl;
    SInt16 main_l_r_delay_time;
    SInt16 main_l_meter_eq_out;
    SInt16 main_r_meter_eq_out;
    SInt16 main_l_meter_dyn_out;
    SInt16 main_r_meter_dyn_out;
    SInt16 main_l_r_ch_17_18_dyn_status;
    unsigned char effect_on_off;    // bit 0=EFX1 ,bit 1=EFX2
    unsigned char main_meter_pre_post;  // bit0=Meter 0:pre 1:post, bit 7=Main 0:Stereo 1:Mono
    SInt16 main_fader;
    SInt16 ch_to_main_ctrl;
    SInt16 gp_to_main_ctrl;
    SInt16 ch_17_audio_setting;
    SInt16 ch_18_audio_setting;
    SInt16 main_l_r_eq_1_db;
    SInt16 main_l_r_eq_2_db;
    SInt16 main_l_r_eq_3_db;
    SInt16 main_l_r_eq_4_db;
    SInt16 main_l_r_eq_1_q;
    SInt16 main_l_r_eq_2_q;
    SInt16 main_l_r_eq_3_q;
    SInt16 main_l_r_eq_4_q;
    SInt16 main_l_r_eq_1_freq;
    SInt16 main_l_r_eq_2_freq;
    SInt16 main_l_r_eq_3_freq;
    SInt16 main_l_r_eq_4_freq;
    SInt16 main_l_r_gate_range;
    SInt16 main_l_r_gate_threshold;
    SInt16 main_l_r_gate_attack;
    SInt16 main_l_r_gate_release;
    SInt16 main_l_r_gate_hold;
    SInt16 main_l_r_expand_ratio;
    SInt16 main_l_r_expand_threshold;
    SInt16 main_l_r_expand_attack;
    SInt16 main_l_r_expand_release;
    SInt16 main_l_r_compressor_ratio;
    SInt16 main_l_r_compressor_threshold;
    SInt16 main_l_r_compressor_attack;
    SInt16 main_l_r_compressor_release;
    SInt16 main_l_r_compressor_out_gain;
    SInt16 main_l_r_limiter_threshold;
    SInt16 main_l_r_limiter_attack;
    SInt16 main_l_r_limiter_release;
    SInt16 geq_20_hz_db;
    SInt16 geq_25_hz_db;
    SInt16 geq_31_5_hz_db;
    SInt16 geq_40_hz_db;
    SInt16 geq_50_hz_db;
    SInt16 geq_63_hz_db;
    SInt16 geq_80_hz_db;
    SInt16 geq_100_hz_db;
    SInt16 geq_125_hz_db;
    SInt16 geq_160_hz_db;
    SInt16 geq_200_hz_db;
    SInt16 geq_250_hz_db;
    SInt16 geq_315_hz_db;
    SInt16 geq_400_hz_db;
    SInt16 geq_500_hz_db;
    SInt16 geq_630_hz_db;
    SInt16 geq_800_hz_db;
    SInt16 geq_1_khz_db;
    SInt16 geq_1_25_khz_db;
    SInt16 geq_1_6_khz_db;
    SInt16 geq_2_khz_db;
    SInt16 geq_2_5_khz_db;
    SInt16 geq_3_15_khz_db;
    SInt16 geq_4_khz_db;
    SInt16 geq_5_khz_db;
    SInt16 geq_6_3_khz_db;
    SInt16 geq_8_khz_db;
    SInt16 geq_10_khz_db;
    SInt16 geq_12_5_khz_db;
    SInt16 geq_16_khz_db;
    SInt16 geq_20_khz_db;
    unsigned char crc;
} ViewPageMainPacket;

typedef struct {
    unsigned int seq_num;
    unsigned int command;
    unsigned int reserve_1;
    unsigned int reserve_2;
    SInt16 channel_1_delay_time;
    SInt16 channel_2_delay_time;
    SInt16 channel_3_delay_time;
    SInt16 channel_4_delay_time;
    SInt16 channel_5_delay_time;
    SInt16 channel_6_delay_time;
    SInt16 channel_7_delay_time;
    SInt16 channel_8_delay_time;
    SInt16 channel_9_delay_time;
    SInt16 channel_10_delay_time;
    SInt16 channel_11_delay_time;
    SInt16 channel_12_delay_time;
    SInt16 channel_13_delay_time;
    SInt16 channel_14_delay_time;
    SInt16 channel_15_delay_time;
    SInt16 channel_16_delay_time;
    SInt16 main_delay_time;
    SInt16 channel_17_delay_time;
    SInt16 channel_18_delay_time;
    SInt16 multi_1_delay_time;
    SInt16 multi_2_delay_time;
    SInt16 multi_3_delay_time;
    SInt16 multi_4_delay_time;
    SInt16 channel_1_ctrl;
    SInt16 channel_2_ctrl;
    SInt16 channel_3_ctrl;
    SInt16 channel_4_ctrl;
    SInt16 channel_5_ctrl;
    SInt16 channel_6_ctrl;
    SInt16 channel_7_ctrl;
    SInt16 channel_8_ctrl;
    SInt16 channel_9_ctrl;
    SInt16 channel_10_ctrl;
    SInt16 channel_11_ctrl;
    SInt16 channel_12_ctrl;
    SInt16 channel_13_ctrl;
    SInt16 channel_14_ctrl;
    SInt16 channel_15_ctrl;
    SInt16 channel_16_ctrl;
    SInt16 main_ctrl;
    SInt16 channel_17_ctrl;
    SInt16 channel_18_ctrl;
    SInt16 multi_1_ctrl;
    SInt16 multi_2_ctrl;
    SInt16 multi_3_ctrl;
    SInt16 multi_4_ctrl;
    SInt16 ch_48v_ctrl;
    unsigned char crc;
} Delay48VOrderPagePacket;

typedef struct {
    unsigned int seq_num;
    unsigned int command;
    unsigned int reserve_1;
    unsigned int reserve_2;
    SInt16 ch_1_gate_range;
    SInt16 ch_2_gate_range;
    SInt16 ch_3_gate_range;
    SInt16 ch_4_gate_range;
    SInt16 ch_5_gate_range;
    SInt16 ch_6_gate_range;
    SInt16 ch_7_gate_range;
    SInt16 ch_8_gate_range;
    SInt16 ch_9_gate_range;
    SInt16 ch_10_gate_range;
    SInt16 ch_11_gate_range;
    SInt16 ch_12_gate_range;
    SInt16 ch_13_gate_range;
    SInt16 ch_14_gate_range;
    SInt16 ch_15_gate_range;
    SInt16 ch_16_gate_range;
    SInt16 ch_1_gate_threshold;
    SInt16 ch_2_gate_threshold;
    SInt16 ch_3_gate_threshold;
    SInt16 ch_4_gate_threshold;
    SInt16 ch_5_gate_threshold;
    SInt16 ch_6_gate_threshold;
    SInt16 ch_7_gate_threshold;
    SInt16 ch_8_gate_threshold;
    SInt16 ch_9_gate_threshold;
    SInt16 ch_10_gate_threshold;
    SInt16 ch_11_gate_threshold;
    SInt16 ch_12_gate_threshold;
    SInt16 ch_13_gate_threshold;
    SInt16 ch_14_gate_threshold;
    SInt16 ch_15_gate_threshold;
    SInt16 ch_16_gate_threshold;
    SInt16 ch_1_gate_attack;
    SInt16 ch_2_gate_attack;
    SInt16 ch_3_gate_attack;
    SInt16 ch_4_gate_attack;
    SInt16 ch_5_gate_attack;
    SInt16 ch_6_gate_attack;
    SInt16 ch_7_gate_attack;
    SInt16 ch_8_gate_attack;
    SInt16 ch_9_gate_attack;
    SInt16 ch_10_gate_attack;
    SInt16 ch_11_gate_attack;
    SInt16 ch_12_gate_attack;
    SInt16 ch_13_gate_attack;
    SInt16 ch_14_gate_attack;
    SInt16 ch_15_gate_attack;
    SInt16 ch_16_gate_attack;
    SInt16 ch_1_gate_release;
    SInt16 ch_2_gate_release;
    SInt16 ch_3_gate_release;
    SInt16 ch_4_gate_release;
    SInt16 ch_5_gate_release;
    SInt16 ch_6_gate_release;
    SInt16 ch_7_gate_release;
    SInt16 ch_8_gate_release;
    SInt16 ch_9_gate_release;
    SInt16 ch_10_gate_release;
    SInt16 ch_11_gate_release;
    SInt16 ch_12_gate_release;
    SInt16 ch_13_gate_release;
    SInt16 ch_14_gate_release;
    SInt16 ch_15_gate_release;
    SInt16 ch_16_gate_release;
    SInt16 ch_1_gate_hold;
    SInt16 ch_2_gate_hold;
    SInt16 ch_3_gate_hold;
    SInt16 ch_4_gate_hold;
    SInt16 ch_5_gate_hold;
    SInt16 ch_6_gate_hold;
    SInt16 ch_7_gate_hold;
    SInt16 ch_8_gate_hold;
    SInt16 ch_9_gate_hold;
    SInt16 ch_10_gate_hold;
    SInt16 ch_11_gate_hold;
    SInt16 ch_12_gate_hold;
    SInt16 ch_13_gate_hold;
    SInt16 ch_14_gate_hold;
    SInt16 ch_15_gate_hold;
    SInt16 ch_16_gate_hold;
    SInt16 ch_1_expand_ratio;
    SInt16 ch_2_expand_ratio;
    SInt16 ch_3_expand_ratio;
    SInt16 ch_4_expand_ratio;
    SInt16 ch_5_expand_ratio;
    SInt16 ch_6_expand_ratio;
    SInt16 ch_7_expand_ratio;
    SInt16 ch_8_expand_ratio;
    SInt16 ch_9_expand_ratio;
    SInt16 ch_10_expand_ratio;
    SInt16 ch_11_expand_ratio;
    SInt16 ch_12_expand_ratio;
    SInt16 ch_13_expand_ratio;
    SInt16 ch_14_expand_ratio;
    SInt16 ch_15_expand_ratio;
    SInt16 ch_16_expand_ratio;
    SInt16 ch_1_expand_threshold;
    SInt16 ch_2_expand_threshold;
    SInt16 ch_3_expand_threshold;
    SInt16 ch_4_expand_threshold;
    SInt16 ch_5_expand_threshold;
    SInt16 ch_6_expand_threshold;
    SInt16 ch_7_expand_threshold;
    SInt16 ch_8_expand_threshold;
    SInt16 ch_9_expand_threshold;
    SInt16 ch_10_expand_threshold;
    SInt16 ch_11_expand_threshold;
    SInt16 ch_12_expand_threshold;
    SInt16 ch_13_expand_threshold;
    SInt16 ch_14_expand_threshold;
    SInt16 ch_15_expand_threshold;
    SInt16 ch_16_expand_threshold;
    SInt16 ch_1_expand_attack;
    SInt16 ch_2_expand_attack;
    SInt16 ch_3_expand_attack;
    SInt16 ch_4_expand_attack;
    SInt16 ch_5_expand_attack;
    SInt16 ch_6_expand_attack;
    SInt16 ch_7_expand_attack;
    SInt16 ch_8_expand_attack;
    SInt16 ch_9_expand_attack;
    SInt16 ch_10_expand_attack;
    SInt16 ch_11_expand_attack;
    SInt16 ch_12_expand_attack;
    SInt16 ch_13_expand_attack;
    SInt16 ch_14_expand_attack;
    SInt16 ch_15_expand_attack;
    SInt16 ch_16_expand_attack;
    SInt16 ch_1_expand_release;
    SInt16 ch_2_expand_release;
    SInt16 ch_3_expand_release;
    SInt16 ch_4_expand_release;
    SInt16 ch_5_expand_release;
    SInt16 ch_6_expand_release;
    SInt16 ch_7_expand_release;
    SInt16 ch_8_expand_release;
    SInt16 ch_9_expand_release;
    SInt16 ch_10_expand_release;
    SInt16 ch_11_expand_release;
    SInt16 ch_12_expand_release;
    SInt16 ch_13_expand_release;
    SInt16 ch_14_expand_release;
    SInt16 ch_15_expand_release;
    SInt16 ch_16_expand_release;
    SInt16 ch_1_compressor_ratio;
    SInt16 ch_2_compressor_ratio;
    SInt16 ch_3_compressor_ratio;
    SInt16 ch_4_compressor_ratio;
    SInt16 ch_5_compressor_ratio;
    SInt16 ch_6_compressor_ratio;
    SInt16 ch_7_compressor_ratio;
    SInt16 ch_8_compressor_ratio;
    SInt16 ch_9_compressor_ratio;
    SInt16 ch_10_compressor_ratio;
    SInt16 ch_11_compressor_ratio;
    SInt16 ch_12_compressor_ratio;
    SInt16 ch_13_compressor_ratio;
    SInt16 ch_14_compressor_ratio;
    SInt16 ch_15_compressor_ratio;
    SInt16 ch_16_compressor_ratio;
    SInt16 ch_1_compressor_threshold;
    SInt16 ch_2_compressor_threshold;
    SInt16 ch_3_compressor_threshold;
    SInt16 ch_4_compressor_threshold;
    SInt16 ch_5_compressor_threshold;
    SInt16 ch_6_compressor_threshold;
    SInt16 ch_7_compressor_threshold;
    SInt16 ch_8_compressor_threshold;
    SInt16 ch_9_compressor_threshold;
    SInt16 ch_10_compressor_threshold;
    SInt16 ch_11_compressor_threshold;
    SInt16 ch_12_compressor_threshold;
    SInt16 ch_13_compressor_threshold;
    SInt16 ch_14_compressor_threshold;
    SInt16 ch_15_compressor_threshold;
    SInt16 ch_16_compressor_threshold;
    SInt16 ch_1_compressor_attack;
    SInt16 ch_2_compressor_attack;
    SInt16 ch_3_compressor_attack;
    SInt16 ch_4_compressor_attack;
    SInt16 ch_5_compressor_attack;
    SInt16 ch_6_compressor_attack;
    SInt16 ch_7_compressor_attack;
    SInt16 ch_8_compressor_attack;
    SInt16 ch_9_compressor_attack;
    SInt16 ch_10_compressor_attack;
    SInt16 ch_11_compressor_attack;
    SInt16 ch_12_compressor_attack;
    SInt16 ch_13_compressor_attack;
    SInt16 ch_14_compressor_attack;
    SInt16 ch_15_compressor_attack;
    SInt16 ch_16_compressor_attack;
    SInt16 ch_1_compressor_release;
    SInt16 ch_2_compressor_release;
    SInt16 ch_3_compressor_release;
    SInt16 ch_4_compressor_release;
    SInt16 ch_5_compressor_release;
    SInt16 ch_6_compressor_release;
    SInt16 ch_7_compressor_release;
    SInt16 ch_8_compressor_release;
    SInt16 ch_9_compressor_release;
    SInt16 ch_10_compressor_release;
    SInt16 ch_11_compressor_release;
    SInt16 ch_12_compressor_release;
    SInt16 ch_13_compressor_release;
    SInt16 ch_14_compressor_release;
    SInt16 ch_15_compressor_release;
    SInt16 ch_16_compressor_release;
    SInt16 ch_1_compressor_out_gain;
    SInt16 ch_2_compressor_out_gain;
    SInt16 ch_3_compressor_out_gain;
    SInt16 ch_4_compressor_out_gain;
    SInt16 ch_5_compressor_out_gain;
    SInt16 ch_6_compressor_out_gain;
    SInt16 ch_7_compressor_out_gain;
    SInt16 ch_8_compressor_out_gain;
    SInt16 ch_9_compressor_out_gain;
    SInt16 ch_10_compressor_out_gain;
    SInt16 ch_11_compressor_out_gain;
    SInt16 ch_12_compressor_out_gain;
    SInt16 ch_13_compressor_out_gain;
    SInt16 ch_14_compressor_out_gain;
    SInt16 ch_15_compressor_out_gain;
    SInt16 ch_16_compressor_out_gain;
    SInt16 ch_1_limiter_threshold;
    SInt16 ch_2_limiter_threshold;
    SInt16 ch_3_limiter_threshold;
    SInt16 ch_4_limiter_threshold;
    SInt16 ch_5_limiter_threshold;
    SInt16 ch_6_limiter_threshold;
    SInt16 ch_7_limiter_threshold;
    SInt16 ch_8_limiter_threshold;
    SInt16 ch_9_limiter_threshold;
    SInt16 ch_10_limiter_threshold;
    SInt16 ch_11_limiter_threshold;
    SInt16 ch_12_limiter_threshold;
    SInt16 ch_13_limiter_threshold;
    SInt16 ch_14_limiter_threshold;
    SInt16 ch_15_limiter_threshold;
    SInt16 ch_16_limiter_threshold;
    SInt16 ch_1_limiter_attack;
    SInt16 ch_2_limiter_attack;
    SInt16 ch_3_limiter_attack;
    SInt16 ch_4_limiter_attack;
    SInt16 ch_5_limiter_attack;
    SInt16 ch_6_limiter_attack;
    SInt16 ch_7_limiter_attack;
    SInt16 ch_8_limiter_attack;
    SInt16 ch_9_limiter_attack;
    SInt16 ch_10_limiter_attack;
    SInt16 ch_11_limiter_attack;
    SInt16 ch_12_limiter_attack;
    SInt16 ch_13_limiter_attack;
    SInt16 ch_14_limiter_attack;
    SInt16 ch_15_limiter_attack;
    SInt16 ch_16_limiter_attack;
    SInt16 ch_1_limiter_release;
    SInt16 ch_2_limiter_release;
    SInt16 ch_3_limiter_release;
    SInt16 ch_4_limiter_release;
    SInt16 ch_5_limiter_release;
    SInt16 ch_6_limiter_release;
    SInt16 ch_7_limiter_release;
    SInt16 ch_8_limiter_release;
    SInt16 ch_9_limiter_release;
    SInt16 ch_10_limiter_release;
    SInt16 ch_11_limiter_release;
    SInt16 ch_12_limiter_release;
    SInt16 ch_13_limiter_release;
    SInt16 ch_14_limiter_release;
    SInt16 ch_15_limiter_release;
    SInt16 ch_16_limiter_release;
    SInt16 main_l_r_gate_range;
    SInt16 ch_17_gate_range;
    SInt16 ch_18_gate_range;
    SInt16 main_l_r_gate_threshold;
    SInt16 ch_17_gate_threshold;
    SInt16 ch_18_gate_threshold;
    SInt16 main_l_r_gate_attack;
    SInt16 ch_17_gate_attack;
    SInt16 ch_18_gate_attack;
    SInt16 main_l_r_gate_release;
    SInt16 ch_17_gate_release;
    SInt16 ch_18_gate_release;
    SInt16 main_l_r_gate_hold;
    SInt16 ch_17_gate_hold;
    SInt16 ch_18_gate_hold;
    SInt16 main_l_r_expand_ratio;
    SInt16 ch_17_expand_ratio;
    SInt16 ch_18_expand_ratio;
    SInt16 main_l_r_expand_threshold;
    SInt16 ch_17_expand_threshold;
    SInt16 ch_18_expand_threshold;
    SInt16 main_l_r_expand_attack;
    SInt16 ch_17_expand_attack;
    SInt16 ch_18_expand_attack;
    SInt16 main_l_r_expand_release;
    SInt16 ch_17_expand_release;
    SInt16 ch_18_expand_release;
    SInt16 main_l_r_compressor_ratio;
    SInt16 ch_17_compressor_ratio;
    SInt16 ch_18_compressor_ratio;
    SInt16 main_l_r_compressor_threshold;
    SInt16 ch_17_compressor_threshold;
    SInt16 ch_18_compressor_threshold;
    SInt16 main_l_r_compressor_attack;
    SInt16 ch_17_compressor_attack;
    SInt16 ch_18_compressor_attack;
    SInt16 main_l_r_compressor_release;
    SInt16 ch_17_compressor_release;
    SInt16 ch_18_compressor_release;
    SInt16 main_l_r_compressor_out_gain;
    SInt16 ch_17_compressor_out_gain;
    SInt16 ch_18_compressor_out_gain;
    SInt16 main_l_r_limiter_threshold;
    SInt16 ch_17_limiter_threshold;
    SInt16 ch_18_limiter_threshold;
    SInt16 main_l_r_limiter_attack;
    SInt16 ch_17_limiter_attack;
    SInt16 ch_18_limiter_attack;
    SInt16 main_l_r_limiter_release;
    SInt16 ch_17_limiter_release;
    SInt16 ch_18_limiter_release;
    SInt16 multi_1_gate_range;
    SInt16 multi_2_gate_range;
    SInt16 multi_3_gate_range;
    SInt16 multi_4_gate_range;
    SInt16 multi_1_gate_threshold;
    SInt16 multi_2_gate_threshold;
    SInt16 multi_3_gate_threshold;
    SInt16 multi_4_gate_threshold;
    SInt16 multi_1_gate_attack;
    SInt16 multi_2_gate_attack;
    SInt16 multi_3_gate_attack;
    SInt16 multi_4_gate_attack;
    SInt16 multi_1_gate_release;
    SInt16 multi_2_gate_release;
    SInt16 multi_3_gate_release;
    SInt16 multi_4_gate_release;
    SInt16 multi_1_gate_hold;
    SInt16 multi_2_gate_hold;
    SInt16 multi_3_gate_hold;
    SInt16 multi_4_gate_hold;
    SInt16 multi_1_expand_ratio;
    SInt16 multi_2_expand_ratio;
    SInt16 multi_3_expand_ratio;
    SInt16 multi_4_expand_ratio;
    SInt16 multi_1_expand_threshold;
    SInt16 multi_2_expand_threshold;
    SInt16 multi_3_expand_threshold;
    SInt16 multi_4_expand_threshold;
    SInt16 multi_1_expand_attack;
    SInt16 multi_2_expand_attack;
    SInt16 multi_3_expand_attack;
    SInt16 multi_4_expand_attack;
    SInt16 multi_1_expand_release;
    SInt16 multi_2_expand_release;
    SInt16 multi_3_expand_release;
    SInt16 multi_4_expand_release;
    SInt16 multi_1_compressor_ratio;
    SInt16 multi_2_compressor_ratio;
    SInt16 multi_3_compressor_ratio;
    SInt16 multi_4_compressor_ratio;
    SInt16 multi_1_compressor_threshold;
    SInt16 multi_2_compressor_threshold;
    SInt16 multi_3_compressor_threshold;
    SInt16 multi_4_compressor_threshold;
    SInt16 multi_1_compressor_attack;
    SInt16 multi_2_compressor_attack;
    SInt16 multi_3_compressor_attack;
    SInt16 multi_4_compressor_attack;
    SInt16 multi_1_compressor_release;
    SInt16 multi_2_compressor_release;
    SInt16 multi_3_compressor_release;
    SInt16 multi_4_compressor_release;
    SInt16 multi_1_compressor_out_gain;
    SInt16 multi_2_compressor_out_gain;
    SInt16 multi_3_compressor_out_gain;
    SInt16 multi_4_compressor_out_gain;
    SInt16 multi_1_limiter_threshold;
    SInt16 multi_2_limiter_threshold;
    SInt16 multi_3_limiter_threshold;
    SInt16 multi_4_limiter_threshold;
    SInt16 multi_1_limiter_attack;
    SInt16 multi_2_limiter_attack;
    SInt16 multi_3_limiter_attack;
    SInt16 multi_4_limiter_attack;
    SInt16 multi_1_limiter_release;
    SInt16 multi_2_limiter_release;
    SInt16 multi_3_limiter_release;
    SInt16 multi_4_limiter_release;
    SInt16 ch_1_ctrl;
    SInt16 ch_2_ctrl;
    SInt16 ch_3_ctrl;
    SInt16 ch_4_ctrl;
    SInt16 ch_5_ctrl;
    SInt16 ch_6_ctrl;
    SInt16 ch_7_ctrl;
    SInt16 ch_8_ctrl;
    SInt16 ch_9_ctrl;
    SInt16 ch_10_ctrl;
    SInt16 ch_11_ctrl;
    SInt16 ch_12_ctrl;
    SInt16 ch_13_ctrl;
    SInt16 ch_14_ctrl;
    SInt16 ch_15_ctrl;
    SInt16 ch_16_ctrl;
    SInt16 main_l_r_ctrl;
    SInt16 ch_17_ctrl;
    SInt16 ch_18_ctrl;
    SInt16 multi_1_ctrl;
    SInt16 multi_2_ctrl;
    SInt16 multi_3_ctrl;
    SInt16 multi_4_ctrl;
    SInt16 ch_1_meter_noise_out;
    SInt16 ch_2_meter_noise_out;
    SInt16 ch_3_meter_noise_out;
    SInt16 ch_4_meter_noise_out;
    SInt16 ch_5_meter_noise_out;
    SInt16 ch_6_meter_noise_out;
    SInt16 ch_7_meter_noise_out;
    SInt16 ch_8_meter_noise_out;
    SInt16 ch_9_meter_noise_out;
    SInt16 ch_10_meter_noise_out;
    SInt16 ch_11_meter_noise_out;
    SInt16 ch_12_meter_noise_out;
    SInt16 ch_13_meter_noise_out;
    SInt16 ch_14_meter_noise_out;
    SInt16 ch_15_meter_noise_out;
    SInt16 ch_16_meter_noise_out;
    SInt16 ch_1_noise_gain;
    SInt16 ch_2_noise_gain;
    SInt16 ch_3_noise_gain;
    SInt16 ch_4_noise_gain;
    SInt16 ch_5_noise_gain;
    SInt16 ch_6_noise_gain;
    SInt16 ch_7_noise_gain;
    SInt16 ch_8_noise_gain;
    SInt16 ch_9_noise_gain;
    SInt16 ch_10_noise_gain;
    SInt16 ch_11_noise_gain;
    SInt16 ch_12_noise_gain;
    SInt16 ch_13_noise_gain;
    SInt16 ch_14_noise_gain;
    SInt16 ch_15_noise_gain;
    SInt16 ch_16_noise_gain;
    SInt16 ch_1_meter_expand_out;
    SInt16 ch_2_meter_expand_out;
    SInt16 ch_3_meter_expand_out;
    SInt16 ch_4_meter_expand_out;
    SInt16 ch_5_meter_expand_out;
    SInt16 ch_6_meter_expand_out;
    SInt16 ch_7_meter_expand_out;
    SInt16 ch_8_meter_expand_out;
    SInt16 ch_9_meter_expand_out;
    SInt16 ch_10_meter_expand_out;
    SInt16 ch_11_meter_expand_out;
    SInt16 ch_12_meter_expand_out;
    SInt16 ch_13_meter_expand_out;
    SInt16 ch_14_meter_expand_out;
    SInt16 ch_15_meter_expand_out;
    SInt16 ch_16_meter_expand_out;
    SInt16 ch_1_meter_expand_gain_reduce;
    SInt16 ch_2_meter_expand_gain_reduce;
    SInt16 ch_3_meter_expand_gain_reduce;
    SInt16 ch_4_meter_expand_gain_reduce;
    SInt16 ch_5_meter_expand_gain_reduce;
    SInt16 ch_6_meter_expand_gain_reduce;
    SInt16 ch_7_meter_expand_gain_reduce;
    SInt16 ch_8_meter_expand_gain_reduce;
    SInt16 ch_9_meter_expand_gain_reduce;
    SInt16 ch_10_meter_expand_gain_reduce;
    SInt16 ch_11_meter_expand_gain_reduce;
    SInt16 ch_12_meter_expand_gain_reduce;
    SInt16 ch_13_meter_expand_gain_reduce;
    SInt16 ch_14_meter_expand_gain_reduce;
    SInt16 ch_15_meter_expand_gain_reduce;
    SInt16 ch_16_meter_expand_gain_reduce;
    SInt16 ch_1_meter_compressor_out;
    SInt16 ch_2_meter_compressor_out;
    SInt16 ch_3_meter_compressor_out;
    SInt16 ch_4_meter_compressor_out;
    SInt16 ch_5_meter_compressor_out;
    SInt16 ch_6_meter_compressor_out;
    SInt16 ch_7_meter_compressor_out;
    SInt16 ch_8_meter_compressor_out;
    SInt16 ch_9_meter_compressor_out;
    SInt16 ch_10_meter_compressor_out;
    SInt16 ch_11_meter_compressor_out;
    SInt16 ch_12_meter_compressor_out;
    SInt16 ch_13_meter_compressor_out;
    SInt16 ch_14_meter_compressor_out;
    SInt16 ch_15_meter_compressor_out;
    SInt16 ch_16_meter_compressor_out;
    SInt16 ch_1_meter_compressor_gain_reduce;
    SInt16 ch_2_meter_compressor_gain_reduce;
    SInt16 ch_3_meter_compressor_gain_reduce;
    SInt16 ch_4_meter_compressor_gain_reduce;
    SInt16 ch_5_meter_compressor_gain_reduce;
    SInt16 ch_6_meter_compressor_gain_reduce;
    SInt16 ch_7_meter_compressor_gain_reduce;
    SInt16 ch_8_meter_compressor_gain_reduce;
    SInt16 ch_9_meter_compressor_gain_reduce;
    SInt16 ch_10_meter_compressor_gain_reduce;
    SInt16 ch_11_meter_compressor_gain_reduce;
    SInt16 ch_12_meter_compressor_gain_reduce;
    SInt16 ch_13_meter_compressor_gain_reduce;
    SInt16 ch_14_meter_compressor_gain_reduce;
    SInt16 ch_15_meter_compressor_gain_reduce;
    SInt16 ch_16_meter_compressor_gain_reduce;
    SInt16 ch_1_meter_limit_out;
    SInt16 ch_2_meter_limit_out;
    SInt16 ch_3_meter_limit_out;
    SInt16 ch_4_meter_limit_out;
    SInt16 ch_5_meter_limit_out;
    SInt16 ch_6_meter_limit_out;
    SInt16 ch_7_meter_limit_out;
    SInt16 ch_8_meter_limit_out;
    SInt16 ch_9_meter_limit_out;
    SInt16 ch_10_meter_limit_out;
    SInt16 ch_11_meter_limit_out;
    SInt16 ch_12_meter_limit_out;
    SInt16 ch_13_meter_limit_out;
    SInt16 ch_14_meter_limit_out;
    SInt16 ch_15_meter_limit_out;
    SInt16 ch_16_meter_limit_out;
    SInt16 ch_1_meter_limit_gain_reduce;
    SInt16 ch_2_meter_limit_gain_reduce;
    SInt16 ch_3_meter_limit_gain_reduce;
    SInt16 ch_4_meter_limit_gain_reduce;
    SInt16 ch_5_meter_limit_gain_reduce;
    SInt16 ch_6_meter_limit_gain_reduce;
    SInt16 ch_7_meter_limit_gain_reduce;
    SInt16 ch_8_meter_limit_gain_reduce;
    SInt16 ch_9_meter_limit_gain_reduce;
    SInt16 ch_10_meter_limit_gain_reduce;
    SInt16 ch_11_meter_limit_gain_reduce;
    SInt16 ch_12_meter_limit_gain_reduce;
    SInt16 ch_13_meter_limit_gain_reduce;
    SInt16 ch_14_meter_limit_gain_reduce;
    SInt16 ch_15_meter_limit_gain_reduce;
    SInt16 ch_16_meter_limit_gain_reduce;
    SInt16 main_l_meter_noise_out;
    SInt16 main_r_meter_noise_out;
    SInt16 ch_17_meter_noise_out;
    SInt16 ch_18_meter_noise_out;
    SInt16 main_l_noise_gain_reduce;
    SInt16 main_r_noise_gain_reduce;
    SInt16 ch_17_noise_gain_reduce;
    SInt16 ch_18_noise_gain_reduce;
    SInt16 main_l_meter_expand_out;
    SInt16 main_r_meter_expand_out;
    SInt16 ch_17_meter_expand_out;
    SInt16 ch_18_meter_expand_out;
    SInt16 main_l_meter_expand_gain_reduce;
    SInt16 main_r_meter_expand_gain_reduce;
    SInt16 ch_17_meter_expand_gain_reduce;
    SInt16 ch_18_meter_expand_gain_reduce;
    SInt16 main_l_meter_compressor_out;
    SInt16 main_r_meter_compressor_out;
    SInt16 ch_17_meter_compressor_out;
    SInt16 ch_18_meter_compressor_out;
    SInt16 main_l_meter_compressor_gain_reduce;
    SInt16 main_r_meter_compressor_gain_reduce;
    SInt16 ch_17_meter_compressor_gain_reduce;
    SInt16 ch_18_meter_compressor_gain_reduce;
    SInt16 main_l_meter_limit_out;
    SInt16 main_r_meter_limit_out;
    SInt16 ch_17_meter_limit_out;
    SInt16 ch_18_meter_limit_out;
    SInt16 main_l_meter_limit_gain_reduce;
    SInt16 main_r_meter_limit_gain_reduce;
    SInt16 ch_17_meter_limit_gain_reduce;
    SInt16 ch_18_meter_limit_gain_reduce;
    SInt16 multi_1_meter_noise_out;
    SInt16 multi_2_meter_noise_out;
    SInt16 multi_3_meter_noise_out;
    SInt16 multi_4_meter_noise_out;
    SInt16 multi_1_noise_gain_reduce;
    SInt16 multi_2_noise_gain_reduce;
    SInt16 multi_3_noise_gain_reduce;
    SInt16 multi_4_noise_gain_reduce;
    SInt16 multi_1_meter_expand_out;
    SInt16 multi_2_meter_expand_out;
    SInt16 multi_3_meter_expand_out;
    SInt16 multi_4_meter_expand_out;
    SInt16 multi_1_meter_expand_gain_reduce;
    SInt16 multi_2_meter_expand_gain_reduce;
    SInt16 multi_3_meter_expand_gain_reduce;
    SInt16 multi_4_meter_expand_gain_reduce;
    SInt16 multi_1_meter_compressor_out;
    SInt16 multi_2_meter_compressor_out;
    SInt16 multi_3_meter_compressor_out;
    SInt16 multi_4_meter_compressor_out;
    SInt16 multi_1_meter_compressor_gain_reduce;
    SInt16 multi_2_meter_compressor_gain_reduce;
    SInt16 multi_3_meter_compressor_gain_reduce;
    SInt16 multi_4_meter_compressor_gain_reduce;
    SInt16 multi_1_meter_limit_out;
    SInt16 multi_2_meter_limit_out;
    SInt16 multi_3_meter_limit_out;
    SInt16 multi_4_meter_limit_out;
    SInt16 multi_1_meter_limit_gain_reduce;
    SInt16 multi_2_meter_limit_gain_reduce;
    SInt16 multi_3_meter_limit_gain_reduce;
    SInt16 multi_4_meter_limit_gain_reduce;
    SInt16 ch_on_off;
    SInt16 solo_ctrl;
    SInt16 ch_meter_pre_post;
    SInt16 ch_1_fader;
    SInt16 ch_2_fader;
    SInt16 ch_3_fader;
    SInt16 ch_4_fader;
    SInt16 ch_5_fader;
    SInt16 ch_6_fader;
    SInt16 ch_7_fader;
    SInt16 ch_8_fader;
    SInt16 ch_9_fader;
    SInt16 ch_10_fader;
    SInt16 ch_11_fader;
    SInt16 ch_12_fader;
    SInt16 ch_13_fader;
    SInt16 ch_14_fader;
    SInt16 ch_15_fader;
    SInt16 ch_16_fader;
    SInt16 ch_17_setting;
    SInt16 ch_18_setting;
    SInt16 ch_17_fader;
    SInt16 ch_18_fader;
    SInt16 aux_1_ch_17_level;
    SInt16 aux_1_ch_18_level;
    SInt16 main_meter_pre_post;
    SInt16 main_fader;
    SInt16 multi_meter_pre_post;
    SInt16 multi_1_fader;
    SInt16 multi_2_fader;
    SInt16 multi_3_fader;
    SInt16 multi_4_fader;
    unsigned char crc;
} DynPagePacket;

typedef struct {
    unsigned int seq_num;
    unsigned int command;
    unsigned int reserve_1;
    unsigned int reserve_2;
    SInt16 ch_1_ctrl;
    SInt16 ch_2_ctrl;
    SInt16 ch_3_ctrl;
    SInt16 ch_4_ctrl;
    SInt16 ch_5_ctrl;
    SInt16 ch_6_ctrl;
    SInt16 ch_7_ctrl;
    SInt16 ch_8_ctrl;
    SInt16 ch_9_ctrl;
    SInt16 ch_10_ctrl;
    SInt16 ch_11_ctrl;
    SInt16 ch_12_ctrl;
    SInt16 ch_13_ctrl;
    SInt16 ch_14_ctrl;
    SInt16 ch_15_ctrl;
    SInt16 ch_16_ctrl;
    SInt16 main_ctrl;
    SInt16 ch_17_ctrl;
    SInt16 ch_18_ctrl;
    SInt16 multi_1_ctrl;
    SInt16 multi_2_ctrl;
    SInt16 multi_3_ctrl;
    SInt16 multi_4_ctrl;
    unsigned char crc;
} OrderPagePacket;

typedef struct {
    unsigned int seq_num;
    unsigned int command;
    unsigned int channel_page;
    unsigned int reserve_2;
    SInt16 solo_ctrl;
    SInt16 solo_safe_ctrl;
    SInt16 main_meter_pre_post;
    SInt16 ch_on_off;
    SInt16 main_l_r_ctrl;
    SInt16 ch_1_fader;
    SInt16 ch_2_fader;
    SInt16 ch_3_fader;
    SInt16 ch_4_fader;
    SInt16 ch_5_fader;
    SInt16 ch_6_fader;
    SInt16 ch_7_fader;
    SInt16 ch_8_fader;
    SInt16 main_fader;
    SInt16 ch_1_pan;
    SInt16 ch_2_pan;
    SInt16 ch_3_pan;
    SInt16 ch_4_pan;
    SInt16 ch_5_pan;
    SInt16 ch_6_pan;
    SInt16 ch_7_pan;
    SInt16 ch_8_pan;
    SInt16 ch_1_inv;
    SInt16 ch_2_inv;
    SInt16 ch_3_inv;
    SInt16 ch_4_inv;
    SInt16 ch_5_inv;
    SInt16 ch_6_inv;
    SInt16 ch_7_inv;
    SInt16 ch_8_inv;
    SInt16 ch_meter_pre_post;
    SInt16 ch_1_delay_time;
    SInt16 ch_2_delay_time;
    SInt16 ch_3_delay_time;
    SInt16 ch_4_delay_time;
    SInt16 ch_5_delay_time;
    SInt16 ch_6_delay_time;
    SInt16 ch_7_delay_time;
    SInt16 ch_8_delay_time;
    SInt16 main_l_r_delay_time;
    SInt16 ch_1_ctrl;
    SInt16 ch_2_ctrl;
    SInt16 ch_3_ctrl;
    SInt16 ch_4_ctrl;
    SInt16 ch_5_ctrl;
    SInt16 ch_6_ctrl;
    SInt16 ch_7_ctrl;
    SInt16 ch_8_ctrl;
    SInt16 ch_48v_ctrl;
    SInt16 ch_1_gate_range;
    SInt16 ch_2_gate_range;
    SInt16 ch_3_gate_range;
    SInt16 ch_4_gate_range;
    SInt16 ch_5_gate_range;
    SInt16 ch_6_gate_range;
    SInt16 ch_7_gate_range;
    SInt16 ch_8_gate_range;
    SInt16 ch_1_gate_threshold;
    SInt16 ch_2_gate_threshold;
    SInt16 ch_3_gate_threshold;
    SInt16 ch_4_gate_threshold;
    SInt16 ch_5_gate_threshold;
    SInt16 ch_6_gate_threshold;
    SInt16 ch_7_gate_threshold;
    SInt16 ch_8_gate_threshold;
    SInt16 ch_1_gate_attack;
    SInt16 ch_2_gate_attack;
    SInt16 ch_3_gate_attack;
    SInt16 ch_4_gate_attack;
    SInt16 ch_5_gate_attack;
    SInt16 ch_6_gate_attack;
    SInt16 ch_7_gate_attack;
    SInt16 ch_8_gate_attack;
    SInt16 ch_1_gate_release;
    SInt16 ch_2_gate_release;
    SInt16 ch_3_gate_release;
    SInt16 ch_4_gate_release;
    SInt16 ch_5_gate_release;
    SInt16 ch_6_gate_release;
    SInt16 ch_7_gate_release;
    SInt16 ch_8_gate_release;
    SInt16 ch_1_gate_hold;
    SInt16 ch_2_gate_hold;
    SInt16 ch_3_gate_hold;
    SInt16 ch_4_gate_hold;
    SInt16 ch_5_gate_hold;
    SInt16 ch_6_gate_hold;
    SInt16 ch_7_gate_hold;
    SInt16 ch_8_gate_hold;
    SInt16 ch_1_expand_ratio;
    SInt16 ch_2_expand_ratio;
    SInt16 ch_3_expand_ratio;
    SInt16 ch_4_expand_ratio;
    SInt16 ch_5_expand_ratio;
    SInt16 ch_6_expand_ratio;
    SInt16 ch_7_expand_ratio;
    SInt16 ch_8_expand_ratio;
    SInt16 ch_1_expand_threshold;
    SInt16 ch_2_expand_threshold;
    SInt16 ch_3_expand_threshold;
    SInt16 ch_4_expand_threshold;
    SInt16 ch_5_expand_threshold;
    SInt16 ch_6_expand_threshold;
    SInt16 ch_7_expand_threshold;
    SInt16 ch_8_expand_threshold;
    SInt16 ch_1_expand_attack;
    SInt16 ch_2_expand_attack;
    SInt16 ch_3_expand_attack;
    SInt16 ch_4_expand_attack;
    SInt16 ch_5_expand_attack;
    SInt16 ch_6_expand_attack;
    SInt16 ch_7_expand_attack;
    SInt16 ch_8_expand_attack;
    SInt16 ch_1_expand_release;
    SInt16 ch_2_expand_release;
    SInt16 ch_3_expand_release;
    SInt16 ch_4_expand_release;
    SInt16 ch_5_expand_release;
    SInt16 ch_6_expand_release;
    SInt16 ch_7_expand_release;
    SInt16 ch_8_expand_release;
    SInt16 ch_1_compressor_ratio;
    SInt16 ch_2_compressor_ratio;
    SInt16 ch_3_compressor_ratio;
    SInt16 ch_4_compressor_ratio;
    SInt16 ch_5_compressor_ratio;
    SInt16 ch_6_compressor_ratio;
    SInt16 ch_7_compressor_ratio;
    SInt16 ch_8_compressor_ratio;
    SInt16 ch_1_compressor_threshold;
    SInt16 ch_2_compressor_threshold;
    SInt16 ch_3_compressor_threshold;
    SInt16 ch_4_compressor_threshold;
    SInt16 ch_5_compressor_threshold;
    SInt16 ch_6_compressor_threshold;
    SInt16 ch_7_compressor_threshold;
    SInt16 ch_8_compressor_threshold;
    SInt16 ch_1_compressor_attack;
    SInt16 ch_2_compressor_attack;
    SInt16 ch_3_compressor_attack;
    SInt16 ch_4_compressor_attack;
    SInt16 ch_5_compressor_attack;
    SInt16 ch_6_compressor_attack;
    SInt16 ch_7_compressor_attack;
    SInt16 ch_8_compressor_attack;
    SInt16 ch_1_compressor_release;
    SInt16 ch_2_compressor_release;
    SInt16 ch_3_compressor_release;
    SInt16 ch_4_compressor_release;
    SInt16 ch_5_compressor_release;
    SInt16 ch_6_compressor_release;
    SInt16 ch_7_compressor_release;
    SInt16 ch_8_compressor_release;
    SInt16 ch_1_compressor_out_gain;
    SInt16 ch_2_compressor_out_gain;
    SInt16 ch_3_compressor_out_gain;
    SInt16 ch_4_compressor_out_gain;
    SInt16 ch_5_compressor_out_gain;
    SInt16 ch_6_compressor_out_gain;
    SInt16 ch_7_compressor_out_gain;
    SInt16 ch_8_compressor_out_gain;
    SInt16 ch_1_limiter_threshold;
    SInt16 ch_2_limiter_threshold;
    SInt16 ch_3_limiter_threshold;
    SInt16 ch_4_limiter_threshold;
    SInt16 ch_5_limiter_threshold;
    SInt16 ch_6_limiter_threshold;
    SInt16 ch_7_limiter_threshold;
    SInt16 ch_8_limiter_threshold;
    SInt16 ch_1_limiter_attack;
    SInt16 ch_2_limiter_attack;
    SInt16 ch_3_limiter_attack;
    SInt16 ch_4_limiter_attack;
    SInt16 ch_5_limiter_attack;
    SInt16 ch_6_limiter_attack;
    SInt16 ch_7_limiter_attack;
    SInt16 ch_8_limiter_attack;
    SInt16 ch_1_limiter_release;
    SInt16 ch_2_limiter_release;
    SInt16 ch_3_limiter_release;
    SInt16 ch_4_limiter_release;
    SInt16 ch_5_limiter_release;
    SInt16 ch_6_limiter_release;
    SInt16 ch_7_limiter_release;
    SInt16 ch_8_limiter_release;
    SInt16 main_l_r_gate_range;
    SInt16 main_l_r_gate_threshold;
    SInt16 main_l_r_gate_attack;
    SInt16 main_l_r_gate_release;
    SInt16 main_l_r_gate_hold;
    SInt16 main_l_r_expand_ratio;
    SInt16 main_l_r_expand_threshold;
    SInt16 main_l_r_expand_attack;
    SInt16 main_l_r_expand_release;
    SInt16 main_l_r_compressor_ratio;
    SInt16 main_l_r_compressor_threshold;
    SInt16 main_l_r_compressor_attack;
    SInt16 main_l_r_compressor_release;
    SInt16 main_l_r_compressor_out_gain;
    SInt16 main_l_r_limiter_threshold;
    SInt16 main_l_r_limiter_attack;
    SInt16 main_l_r_limiter_release;
    SInt16 ch_1_eq_1_db;
    SInt16 ch_1_eq_2_db;
    SInt16 ch_1_eq_3_db;
    SInt16 ch_1_eq_4_db;
    SInt16 ch_2_eq_1_db;
    SInt16 ch_2_eq_2_db;
    SInt16 ch_2_eq_3_db;
    SInt16 ch_2_eq_4_db;
    SInt16 ch_3_eq_1_db;
    SInt16 ch_3_eq_2_db;
    SInt16 ch_3_eq_3_db;
    SInt16 ch_3_eq_4_db;
    SInt16 ch_4_eq_1_db;
    SInt16 ch_4_eq_2_db;
    SInt16 ch_4_eq_3_db;
    SInt16 ch_4_eq_4_db;
    SInt16 ch_5_eq_1_db;
    SInt16 ch_5_eq_2_db;
    SInt16 ch_5_eq_3_db;
    SInt16 ch_5_eq_4_db;
    SInt16 ch_6_eq_1_db;
    SInt16 ch_6_eq_2_db;
    SInt16 ch_6_eq_3_db;
    SInt16 ch_6_eq_4_db;
    SInt16 ch_7_eq_1_db;
    SInt16 ch_7_eq_2_db;
    SInt16 ch_7_eq_3_db;
    SInt16 ch_7_eq_4_db;
    SInt16 ch_8_eq_1_db;
    SInt16 ch_8_eq_2_db;
    SInt16 ch_8_eq_3_db;
    SInt16 ch_8_eq_4_db;
    SInt16 ch_1_eq_1_q;
    SInt16 ch_1_eq_2_q;
    SInt16 ch_1_eq_3_q;
    SInt16 ch_1_eq_4_q;
    SInt16 ch_2_eq_1_q;
    SInt16 ch_2_eq_2_q;
    SInt16 ch_2_eq_3_q;
    SInt16 ch_2_eq_4_q;
    SInt16 ch_3_eq_1_q;
    SInt16 ch_3_eq_2_q;
    SInt16 ch_3_eq_3_q;
    SInt16 ch_3_eq_4_q;
    SInt16 ch_4_eq_1_q;
    SInt16 ch_4_eq_2_q;
    SInt16 ch_4_eq_3_q;
    SInt16 ch_4_eq_4_q;
    SInt16 ch_5_eq_1_q;
    SInt16 ch_5_eq_2_q;
    SInt16 ch_5_eq_3_q;
    SInt16 ch_5_eq_4_q;
    SInt16 ch_6_eq_1_q;
    SInt16 ch_6_eq_2_q;
    SInt16 ch_6_eq_3_q;
    SInt16 ch_6_eq_4_q;
    SInt16 ch_7_eq_1_q;
    SInt16 ch_7_eq_2_q;
    SInt16 ch_7_eq_3_q;
    SInt16 ch_7_eq_4_q;
    SInt16 ch_8_eq_1_q;
    SInt16 ch_8_eq_2_q;
    SInt16 ch_8_eq_3_q;
    SInt16 ch_8_eq_4_q;
    SInt16 ch_1_eq_1_freq;
    SInt16 ch_1_eq_2_freq;
    SInt16 ch_1_eq_3_freq;
    SInt16 ch_1_eq_4_freq;
    SInt16 ch_2_eq_1_freq;
    SInt16 ch_2_eq_2_freq;
    SInt16 ch_2_eq_3_freq;
    SInt16 ch_2_eq_4_freq;
    SInt16 ch_3_eq_1_freq;
    SInt16 ch_3_eq_2_freq;
    SInt16 ch_3_eq_3_freq;
    SInt16 ch_3_eq_4_freq;
    SInt16 ch_4_eq_1_freq;
    SInt16 ch_4_eq_2_freq;
    SInt16 ch_4_eq_3_freq;
    SInt16 ch_4_eq_4_freq;
    SInt16 ch_5_eq_1_freq;
    SInt16 ch_5_eq_2_freq;
    SInt16 ch_5_eq_3_freq;
    SInt16 ch_5_eq_4_freq;
    SInt16 ch_6_eq_1_freq;
    SInt16 ch_6_eq_2_freq;
    SInt16 ch_6_eq_3_freq;
    SInt16 ch_6_eq_4_freq;
    SInt16 ch_7_eq_1_freq;
    SInt16 ch_7_eq_2_freq;
    SInt16 ch_7_eq_3_freq;
    SInt16 ch_7_eq_4_freq;
    SInt16 ch_8_eq_1_freq;
    SInt16 ch_8_eq_2_freq;
    SInt16 ch_8_eq_3_freq;
    SInt16 ch_8_eq_4_freq;
    SInt16 main_l_r_eq_1_db;
    SInt16 main_l_r_eq_2_db;
    SInt16 main_l_r_eq_3_db;
    SInt16 main_l_r_eq_4_db;
    SInt16 main_l_r_eq_1_q;
    SInt16 main_l_r_eq_2_q;
    SInt16 main_l_r_eq_3_q;
    SInt16 main_l_r_eq_4_q;
    SInt16 main_l_r_eq_1_freq;
    SInt16 main_l_r_eq_2_freq;
    SInt16 main_l_r_eq_3_freq;
    SInt16 main_l_r_eq_4_freq;
    SInt16 geq_20_hz_db;
    SInt16 geq_25_hz_db;
    SInt16 geq_31_5_hz_db;
    SInt16 geq_40_hz_db;
    SInt16 geq_50_hz_db;
    SInt16 geq_63_hz_db;
    SInt16 geq_80_hz_db;
    SInt16 geq_100_hz_db;
    SInt16 geq_125_hz_db;
    SInt16 geq_160_hz_db;
    SInt16 geq_200_hz_db;
    SInt16 geq_250_hz_db;
    SInt16 geq_315_hz_db;
    SInt16 geq_400_hz_db;
    SInt16 geq_500_hz_db;
    SInt16 geq_630_hz_db;
    SInt16 geq_800_hz_db;
    SInt16 geq_1_khz_db;
    SInt16 geq_1_25_khz_db;
    SInt16 geq_1_6_khz_db;
    SInt16 geq_2_khz_db;
    SInt16 geq_2_5_khz_db;
    SInt16 geq_3_15_khz_db;
    SInt16 geq_4_khz_db;
    SInt16 geq_5_khz_db;
    SInt16 geq_6_3_khz_db;
    SInt16 geq_8_khz_db;
    SInt16 geq_10_khz_db;
    SInt16 geq_12_5_khz_db;
    SInt16 geq_16_khz_db;
    SInt16 geq_20_khz_db;
    unsigned char crc;
} ChannelCh1_8PagePacket;

typedef struct {
    unsigned int seq_num;
    unsigned int command;
    unsigned int channel_page;
    unsigned int reserve_2;
    SInt16 solo_ctrl;
    SInt16 solo_safe_ctrl;
    SInt16 main_meter_pre_post;
    SInt16 ch_on_off;
    SInt16 main_l_r_ctrl;
    SInt16 ch_9_fader;
    SInt16 ch_10_fader;
    SInt16 ch_11_fader;
    SInt16 ch_12_fader;
    SInt16 ch_13_fader;
    SInt16 ch_14_fader;
    SInt16 ch_15_fader;
    SInt16 ch_16_fader;
    SInt16 main_fader;
    SInt16 ch_9_pan;
    SInt16 ch_10_pan;
    SInt16 ch_11_pan;
    SInt16 ch_12_pan;
    SInt16 ch_13_pan;
    SInt16 ch_14_pan;
    SInt16 ch_15_pan;
    SInt16 ch_16_pan;
    SInt16 ch_9_inv;
    SInt16 ch_10_inv;
    SInt16 ch_11_inv;
    SInt16 ch_12_inv;
    SInt16 ch_13_inv;
    SInt16 ch_14_inv;
    SInt16 ch_15_inv;
    SInt16 ch_16_inv;
    SInt16 ch_meter_pre_post;
    SInt16 ch_9_delay_time;
    SInt16 ch_10_delay_time;
    SInt16 ch_11_delay_time;
    SInt16 ch_12_delay_time;
    SInt16 ch_13_delay_time;
    SInt16 ch_14_delay_time;
    SInt16 ch_15_delay_time;
    SInt16 ch_16_delay_time;
    SInt16 main_l_r_delay_time;
    SInt16 ch_9_ctrl;
    SInt16 ch_10_ctrl;
    SInt16 ch_11_ctrl;
    SInt16 ch_12_ctrl;
    SInt16 ch_13_ctrl;
    SInt16 ch_14_ctrl;
    SInt16 ch_15_ctrl;
    SInt16 ch_16_ctrl;
    SInt16 ch_48v_ctrl;
    SInt16 ch_9_gate_range;
    SInt16 ch_10_gate_range;
    SInt16 ch_11_gate_range;
    SInt16 ch_12_gate_range;
    SInt16 ch_13_gate_range;
    SInt16 ch_14_gate_range;
    SInt16 ch_15_gate_range;
    SInt16 ch_16_gate_range;
    SInt16 ch_9_gate_threshold;
    SInt16 ch_10_gate_threshold;
    SInt16 ch_11_gate_threshold;
    SInt16 ch_12_gate_threshold;
    SInt16 ch_13_gate_threshold;
    SInt16 ch_14_gate_threshold;
    SInt16 ch_15_gate_threshold;
    SInt16 ch_16_gate_threshold;
    SInt16 ch_9_gate_attack;
    SInt16 ch_10_gate_attack;
    SInt16 ch_11_gate_attack;
    SInt16 ch_12_gate_attack;
    SInt16 ch_13_gate_attack;
    SInt16 ch_14_gate_attack;
    SInt16 ch_15_gate_attack;
    SInt16 ch_16_gate_attack;
    SInt16 ch_9_gate_release;
    SInt16 ch_10_gate_release;
    SInt16 ch_11_gate_release;
    SInt16 ch_12_gate_release;
    SInt16 ch_13_gate_release;
    SInt16 ch_14_gate_release;
    SInt16 ch_15_gate_release;
    SInt16 ch_16_gate_release;
    SInt16 ch_9_gate_hold;
    SInt16 ch_10_gate_hold;
    SInt16 ch_11_gate_hold;
    SInt16 ch_12_gate_hold;
    SInt16 ch_13_gate_hold;
    SInt16 ch_14_gate_hold;
    SInt16 ch_15_gate_hold;
    SInt16 ch_16_gate_hold;
    SInt16 ch_9_expand_ratio;
    SInt16 ch_10_expand_ratio;
    SInt16 ch_11_expand_ratio;
    SInt16 ch_12_expand_ratio;
    SInt16 ch_13_expand_ratio;
    SInt16 ch_14_expand_ratio;
    SInt16 ch_15_expand_ratio;
    SInt16 ch_16_expand_ratio;
    SInt16 ch_9_expand_threshold;
    SInt16 ch_10_expand_threshold;
    SInt16 ch_11_expand_threshold;
    SInt16 ch_12_expand_threshold;
    SInt16 ch_13_expand_threshold;
    SInt16 ch_14_expand_threshold;
    SInt16 ch_15_expand_threshold;
    SInt16 ch_16_expand_threshold;
    SInt16 ch_9_expand_attack;
    SInt16 ch_10_expand_attack;
    SInt16 ch_11_expand_attack;
    SInt16 ch_12_expand_attack;
    SInt16 ch_13_expand_attack;
    SInt16 ch_14_expand_attack;
    SInt16 ch_15_expand_attack;
    SInt16 ch_16_expand_attack;
    SInt16 ch_9_expand_release;
    SInt16 ch_10_expand_release;
    SInt16 ch_11_expand_release;
    SInt16 ch_12_expand_release;
    SInt16 ch_13_expand_release;
    SInt16 ch_14_expand_release;
    SInt16 ch_15_expand_release;
    SInt16 ch_16_expand_release;
    SInt16 ch_9_compressor_ratio;
    SInt16 ch_10_compressor_ratio;
    SInt16 ch_11_compressor_ratio;
    SInt16 ch_12_compressor_ratio;
    SInt16 ch_13_compressor_ratio;
    SInt16 ch_14_compressor_ratio;
    SInt16 ch_15_compressor_ratio;
    SInt16 ch_16_compressor_ratio;
    SInt16 ch_9_compressor_threshold;
    SInt16 ch_10_compressor_threshold;
    SInt16 ch_11_compressor_threshold;
    SInt16 ch_12_compressor_threshold;
    SInt16 ch_13_compressor_threshold;
    SInt16 ch_14_compressor_threshold;
    SInt16 ch_15_compressor_threshold;
    SInt16 ch_16_compressor_threshold;
    SInt16 ch_9_compressor_attack;
    SInt16 ch_10_compressor_attack;
    SInt16 ch_11_compressor_attack;
    SInt16 ch_12_compressor_attack;
    SInt16 ch_13_compressor_attack;
    SInt16 ch_14_compressor_attack;
    SInt16 ch_15_compressor_attack;
    SInt16 ch_16_compressor_attack;
    SInt16 ch_9_compressor_release;
    SInt16 ch_10_compressor_release;
    SInt16 ch_11_compressor_release;
    SInt16 ch_12_compressor_release;
    SInt16 ch_13_compressor_release;
    SInt16 ch_14_compressor_release;
    SInt16 ch_15_compressor_release;
    SInt16 ch_16_compressor_release;
    SInt16 ch_9_compressor_out_gain;
    SInt16 ch_10_compressor_out_gain;
    SInt16 ch_11_compressor_out_gain;
    SInt16 ch_12_compressor_out_gain;
    SInt16 ch_13_compressor_out_gain;
    SInt16 ch_14_compressor_out_gain;
    SInt16 ch_15_compressor_out_gain;
    SInt16 ch_16_compressor_out_gain;
    SInt16 ch_9_limiter_threshold;
    SInt16 ch_10_limiter_threshold;
    SInt16 ch_11_limiter_threshold;
    SInt16 ch_12_limiter_threshold;
    SInt16 ch_13_limiter_threshold;
    SInt16 ch_14_limiter_threshold;
    SInt16 ch_15_limiter_threshold;
    SInt16 ch_16_limiter_threshold;
    SInt16 ch_9_limiter_attack;
    SInt16 ch_10_limiter_attack;
    SInt16 ch_11_limiter_attack;
    SInt16 ch_12_limiter_attack;
    SInt16 ch_13_limiter_attack;
    SInt16 ch_14_limiter_attack;
    SInt16 ch_15_limiter_attack;
    SInt16 ch_16_limiter_attack;
    SInt16 ch_9_limiter_release;
    SInt16 ch_10_limiter_release;
    SInt16 ch_11_limiter_release;
    SInt16 ch_12_limiter_release;
    SInt16 ch_13_limiter_release;
    SInt16 ch_14_limiter_release;
    SInt16 ch_15_limiter_release;
    SInt16 ch_16_limiter_release;
    SInt16 main_l_r_gate_range;
    SInt16 main_l_r_gate_threshold;
    SInt16 main_l_r_gate_attack;
    SInt16 main_l_r_gate_release;
    SInt16 main_l_r_gate_hold;
    SInt16 main_l_r_expand_ratio;
    SInt16 main_l_r_expand_threshold;
    SInt16 main_l_r_expand_attack;
    SInt16 main_l_r_expand_release;
    SInt16 main_l_r_compressor_ratio;
    SInt16 main_l_r_compressor_threshold;
    SInt16 main_l_r_compressor_attack;
    SInt16 main_l_r_compressor_release;
    SInt16 main_l_r_compressor_out_gain;
    SInt16 main_l_r_limiter_threshold;
    SInt16 main_l_r_limiter_attack;
    SInt16 main_l_r_limiter_release;
    SInt16 ch_9_eq_1_db;
    SInt16 ch_9_eq_2_db;
    SInt16 ch_9_eq_3_db;
    SInt16 ch_9_eq_4_db;
    SInt16 ch_10_eq_1_db;
    SInt16 ch_10_eq_2_db;
    SInt16 ch_10_eq_3_db;
    SInt16 ch_10_eq_4_db;
    SInt16 ch_11_eq_1_db;
    SInt16 ch_11_eq_2_db;
    SInt16 ch_11_eq_3_db;
    SInt16 ch_11_eq_4_db;
    SInt16 ch_12_eq_1_db;
    SInt16 ch_12_eq_2_db;
    SInt16 ch_12_eq_3_db;
    SInt16 ch_12_eq_4_db;
    SInt16 ch_13_eq_1_db;
    SInt16 ch_13_eq_2_db;
    SInt16 ch_13_eq_3_db;
    SInt16 ch_13_eq_4_db;
    SInt16 ch_14_eq_1_db;
    SInt16 ch_14_eq_2_db;
    SInt16 ch_14_eq_3_db;
    SInt16 ch_14_eq_4_db;
    SInt16 ch_15_eq_1_db;
    SInt16 ch_15_eq_2_db;
    SInt16 ch_15_eq_3_db;
    SInt16 ch_15_eq_4_db;
    SInt16 ch_16_eq_1_db;
    SInt16 ch_16_eq_2_db;
    SInt16 ch_16_eq_3_db;
    SInt16 ch_16_eq_4_db;
    SInt16 ch_9_eq_1_q;
    SInt16 ch_9_eq_2_q;
    SInt16 ch_9_eq_3_q;
    SInt16 ch_9_eq_4_q;
    SInt16 ch_10_eq_1_q;
    SInt16 ch_10_eq_2_q;
    SInt16 ch_10_eq_3_q;
    SInt16 ch_10_eq_4_q;
    SInt16 ch_11_eq_1_q;
    SInt16 ch_11_eq_2_q;
    SInt16 ch_11_eq_3_q;
    SInt16 ch_11_eq_4_q;
    SInt16 ch_12_eq_1_q;
    SInt16 ch_12_eq_2_q;
    SInt16 ch_12_eq_3_q;
    SInt16 ch_12_eq_4_q;
    SInt16 ch_13_eq_1_q;
    SInt16 ch_13_eq_2_q;
    SInt16 ch_13_eq_3_q;
    SInt16 ch_13_eq_4_q;
    SInt16 ch_14_eq_1_q;
    SInt16 ch_14_eq_2_q;
    SInt16 ch_14_eq_3_q;
    SInt16 ch_14_eq_4_q;
    SInt16 ch_15_eq_1_q;
    SInt16 ch_15_eq_2_q;
    SInt16 ch_15_eq_3_q;
    SInt16 ch_15_eq_4_q;
    SInt16 ch_16_eq_1_q;
    SInt16 ch_16_eq_2_q;
    SInt16 ch_16_eq_3_q;
    SInt16 ch_16_eq_4_q;
    SInt16 ch_9_eq_1_freq;
    SInt16 ch_9_eq_2_freq;
    SInt16 ch_9_eq_3_freq;
    SInt16 ch_9_eq_4_freq;
    SInt16 ch_10_eq_1_freq;
    SInt16 ch_10_eq_2_freq;
    SInt16 ch_10_eq_3_freq;
    SInt16 ch_10_eq_4_freq;
    SInt16 ch_11_eq_1_freq;
    SInt16 ch_11_eq_2_freq;
    SInt16 ch_11_eq_3_freq;
    SInt16 ch_11_eq_4_freq;
    SInt16 ch_12_eq_1_freq;
    SInt16 ch_12_eq_2_freq;
    SInt16 ch_12_eq_3_freq;
    SInt16 ch_12_eq_4_freq;
    SInt16 ch_13_eq_1_freq;
    SInt16 ch_13_eq_2_freq;
    SInt16 ch_13_eq_3_freq;
    SInt16 ch_13_eq_4_freq;
    SInt16 ch_14_eq_1_freq;
    SInt16 ch_14_eq_2_freq;
    SInt16 ch_14_eq_3_freq;
    SInt16 ch_14_eq_4_freq;
    SInt16 ch_15_eq_1_freq;
    SInt16 ch_15_eq_2_freq;
    SInt16 ch_15_eq_3_freq;
    SInt16 ch_15_eq_4_freq;
    SInt16 ch_16_eq_1_freq;
    SInt16 ch_16_eq_2_freq;
    SInt16 ch_16_eq_3_freq;
    SInt16 ch_16_eq_4_freq;
    SInt16 main_l_r_eq_1_db;
    SInt16 main_l_r_eq_2_db;
    SInt16 main_l_r_eq_3_db;
    SInt16 main_l_r_eq_4_db;
    SInt16 main_l_r_eq_1_q;
    SInt16 main_l_r_eq_2_q;
    SInt16 main_l_r_eq_3_q;
    SInt16 main_l_r_eq_4_q;
    SInt16 main_l_r_eq_1_freq;
    SInt16 main_l_r_eq_2_freq;
    SInt16 main_l_r_eq_3_freq;
    SInt16 main_l_r_eq_4_freq;
    SInt16 geq_20_hz_db;
    SInt16 geq_25_hz_db;
    SInt16 geq_31_5_hz_db;
    SInt16 geq_40_hz_db;
    SInt16 geq_50_hz_db;
    SInt16 geq_63_hz_db;
    SInt16 geq_80_hz_db;
    SInt16 geq_100_hz_db;
    SInt16 geq_125_hz_db;
    SInt16 geq_160_hz_db;
    SInt16 geq_200_hz_db;
    SInt16 geq_250_hz_db;
    SInt16 geq_315_hz_db;
    SInt16 geq_400_hz_db;
    SInt16 geq_500_hz_db;
    SInt16 geq_630_hz_db;
    SInt16 geq_800_hz_db;
    SInt16 geq_1_khz_db;
    SInt16 geq_1_25_khz_db;
    SInt16 geq_1_6_khz_db;
    SInt16 geq_2_khz_db;
    SInt16 geq_2_5_khz_db;
    SInt16 geq_3_15_khz_db;
    SInt16 geq_4_khz_db;
    SInt16 geq_5_khz_db;
    SInt16 geq_6_3_khz_db;
    SInt16 geq_8_khz_db;
    SInt16 geq_10_khz_db;
    SInt16 geq_12_5_khz_db;
    SInt16 geq_16_khz_db;
    SInt16 geq_20_khz_db;
    unsigned char crc;
} ChannelCh9_16PagePacket;

typedef struct {
    unsigned int seq_num;
    unsigned int command;
    unsigned int channel_page;
    unsigned int reserve_2;
    SInt16 ch_17_audio_setting;
    SInt16 ch_18_audio_setting;
    SInt16 main_meter_pre_post;
    SInt16 main_l_r_ctrl;
    SInt16 ch_17_fader;
    SInt16 ch_18_fader;
    SInt16 main_fader;
    SInt16 ch_17_pan;
    SInt16 ch_18_pan;
    SInt16 ch_17_ctrl;
    SInt16 ch_18_ctrl;
    SInt16 ch_17_delay_time;
    SInt16 ch_18_delay_time;
    SInt16 main_l_r_delay_time;
    SInt16 ch_17_gate_range;
    SInt16 ch_17_gate_threshold;
    SInt16 ch_17_gate_attack;
    SInt16 ch_17_gate_release;
    SInt16 ch_17_gate_hold;
    SInt16 ch_17_expand_ratio;
    SInt16 ch_17_expand_threshold;
    SInt16 ch_17_expand_attack;
    SInt16 ch_17_expand_release;
    SInt16 ch_17_compressor_ratio;
    SInt16 ch_17_compressor_threshold;
    SInt16 ch_17_compressor_attack;
    SInt16 ch_17_compressor_release;
    SInt16 ch_17_compressor_out_gain;
    SInt16 ch_17_limiter_threshold;
    SInt16 ch_17_limiter_attack;
    SInt16 ch_17_limiter_release;
    SInt16 ch_18_gate_range;
    SInt16 ch_18_gate_threshold;
    SInt16 ch_18_gate_attack;
    SInt16 ch_18_gate_release;
    SInt16 ch_18_gate_hold;
    SInt16 ch_18_expand_ratio;
    SInt16 ch_18_expand_threshold;
    SInt16 ch_18_expand_attack;
    SInt16 ch_18_expand_release;
    SInt16 ch_18_compressor_ratio;
    SInt16 ch_18_compressor_threshold;
    SInt16 ch_18_compressor_attack;
    SInt16 ch_18_compressor_release;
    SInt16 ch_18_compressor_out_gain;
    SInt16 ch_18_limiter_threshold;
    SInt16 ch_18_limiter_attack;
    SInt16 ch_18_limiter_release;
    SInt16 main_l_r_gate_range;
    SInt16 main_l_r_gate_threshold;
    SInt16 main_l_r_gate_attack;
    SInt16 main_l_r_gate_release;
    SInt16 main_l_r_gate_hold;
    SInt16 main_l_r_expand_ratio;
    SInt16 main_l_r_expand_threshold;
    SInt16 main_l_r_expand_attack;
    SInt16 main_l_r_expand_release;
    SInt16 main_l_r_compressor_ratio;
    SInt16 main_l_r_compressor_threshold;
    SInt16 main_l_r_compressor_attack;
    SInt16 main_l_r_compressor_release;
    SInt16 main_l_r_compressor_out_gain;
    SInt16 main_l_r_limiter_threshold;
    SInt16 main_l_r_limiter_attack;
    SInt16 main_l_r_limiter_release;
    SInt16 ch_17_eq_1_db;
    SInt16 ch_17_eq_2_db;
    SInt16 ch_17_eq_3_db;
    SInt16 ch_17_eq_4_db;
    SInt16 ch_17_eq_1_q;
    SInt16 ch_17_eq_2_q;
    SInt16 ch_17_eq_3_q;
    SInt16 ch_17_eq_4_q;
    SInt16 ch_17_eq_1_freq;
    SInt16 ch_17_eq_2_freq;
    SInt16 ch_17_eq_3_freq;
    SInt16 ch_17_eq_4_freq;
    SInt16 ch_18_eq_1_db;
    SInt16 ch_18_eq_2_db;
    SInt16 ch_18_eq_3_db;
    SInt16 ch_18_eq_4_db;
    SInt16 ch_18_eq_1_q;
    SInt16 ch_18_eq_2_q;
    SInt16 ch_18_eq_3_q;
    SInt16 ch_18_eq_4_q;
    SInt16 ch_18_eq_1_freq;
    SInt16 ch_18_eq_2_freq;
    SInt16 ch_18_eq_3_freq;
    SInt16 ch_18_eq_4_freq;
    SInt16 main_l_r_eq_1_db;
    SInt16 main_l_r_eq_2_db;
    SInt16 main_l_r_eq_3_db;
    SInt16 main_l_r_eq_4_db;
    SInt16 main_l_r_eq_1_q;
    SInt16 main_l_r_eq_2_q;
    SInt16 main_l_r_eq_3_q;
    SInt16 main_l_r_eq_4_q;
    SInt16 main_l_r_eq_1_freq;
    SInt16 main_l_r_eq_2_freq;
    SInt16 main_l_r_eq_3_freq;
    SInt16 main_l_r_eq_4_freq;
    SInt16 geq_20_hz_db;
    SInt16 geq_25_hz_db;
    SInt16 geq_31_5_hz_db;
    SInt16 geq_40_hz_db;
    SInt16 geq_50_hz_db;
    SInt16 geq_63_hz_db;
    SInt16 geq_80_hz_db;
    SInt16 geq_100_hz_db;
    SInt16 geq_125_hz_db;
    SInt16 geq_160_hz_db;
    SInt16 geq_200_hz_db;
    SInt16 geq_250_hz_db;
    SInt16 geq_315_hz_db;
    SInt16 geq_400_hz_db;
    SInt16 geq_500_hz_db;
    SInt16 geq_630_hz_db;
    SInt16 geq_800_hz_db;
    SInt16 geq_1_khz_db;
    SInt16 geq_1_25_khz_db;
    SInt16 geq_1_6_khz_db;
    SInt16 geq_2_khz_db;
    SInt16 geq_2_5_khz_db;
    SInt16 geq_3_15_khz_db;
    SInt16 geq_4_khz_db;
    SInt16 geq_5_khz_db;
    SInt16 geq_6_3_khz_db;
    SInt16 geq_8_khz_db;
    SInt16 geq_10_khz_db;
    SInt16 geq_12_5_khz_db;
    SInt16 geq_16_khz_db;
    SInt16 geq_20_khz_db;
    unsigned char crc;
} ChannelCh17_18PagePacket;

typedef struct {
    unsigned int seq_num;
    unsigned int command;
    unsigned int channel_page;
    unsigned int reserve_2;
    SInt16 solo_ctrl;
    SInt16 solo_safe_ctrl;
    SInt16 main_meter_pre_post;
    SInt16 aux_gp_master_on_off;
    SInt16 main_l_r_ctrl;
    SInt16 aux_1_fader;
    SInt16 aux_2_fader;
    SInt16 aux_3_fader;
    SInt16 aux_4_fader;
    SInt16 main_fader;
    SInt16 aux_gp_meter_pre_post;
    SInt16 main_l_r_delay_time;
    SInt16 main_l_r_gate_range;
    SInt16 main_l_r_gate_threshold;
    SInt16 main_l_r_gate_attack;
    SInt16 main_l_r_gate_release;
    SInt16 main_l_r_gate_hold;
    SInt16 main_l_r_expand_ratio;
    SInt16 main_l_r_expand_threshold;
    SInt16 main_l_r_expand_attack;
    SInt16 main_l_r_expand_release;
    SInt16 main_l_r_compressor_ratio;
    SInt16 main_l_r_compressor_threshold;
    SInt16 main_l_r_compressor_attack;
    SInt16 main_l_r_compressor_release;
    SInt16 main_l_r_compressor_out_gain;
    SInt16 main_l_r_limiter_threshold;
    SInt16 main_l_r_limiter_attack;
    SInt16 main_l_r_limiter_release;
    SInt16 main_l_r_eq_1_db;
    SInt16 main_l_r_eq_2_db;
    SInt16 main_l_r_eq_3_db;
    SInt16 main_l_r_eq_4_db;
    SInt16 main_l_r_eq_1_q;
    SInt16 main_l_r_eq_2_q;
    SInt16 main_l_r_eq_3_q;
    SInt16 main_l_r_eq_4_q;
    SInt16 main_l_r_eq_1_freq;
    SInt16 main_l_r_eq_2_freq;
    SInt16 main_l_r_eq_3_freq;
    SInt16 main_l_r_eq_4_freq;
    SInt16 geq_20_hz_db;
    SInt16 geq_25_hz_db;
    SInt16 geq_31_5_hz_db;
    SInt16 geq_40_hz_db;
    SInt16 geq_50_hz_db;
    SInt16 geq_63_hz_db;
    SInt16 geq_80_hz_db;
    SInt16 geq_100_hz_db;
    SInt16 geq_125_hz_db;
    SInt16 geq_160_hz_db;
    SInt16 geq_200_hz_db;
    SInt16 geq_250_hz_db;
    SInt16 geq_315_hz_db;
    SInt16 geq_400_hz_db;
    SInt16 geq_500_hz_db;
    SInt16 geq_630_hz_db;
    SInt16 geq_800_hz_db;
    SInt16 geq_1_khz_db;
    SInt16 geq_1_25_khz_db;
    SInt16 geq_1_6_khz_db;
    SInt16 geq_2_khz_db;
    SInt16 geq_2_5_khz_db;
    SInt16 geq_3_15_khz_db;
    SInt16 geq_4_khz_db;
    SInt16 geq_5_khz_db;
    SInt16 geq_6_3_khz_db;
    SInt16 geq_8_khz_db;
    SInt16 geq_10_khz_db;
    SInt16 geq_12_5_khz_db;
    SInt16 geq_16_khz_db;
    SInt16 geq_20_khz_db;
    unsigned char crc;
} ChannelAux1_4PagePacket;

typedef struct {
    unsigned int seq_num;
    unsigned int command;
    unsigned int channel_page;
    unsigned int reserve_2;    
    SInt16 solo_ctrl;
    SInt16 solo_safe_ctrl;
    SInt16 main_meter_pre_post;
    SInt16 aux_gp_master_on_off;
    SInt16 main_l_r_ctrl;
    SInt16 gp_1_fader;
    SInt16 gp_2_fader;
    SInt16 gp_3_fader;
    SInt16 gp_4_fader;
    SInt16 main_fader;
    SInt16 gp_1_pan;
    SInt16 gp_2_pan;
    SInt16 gp_3_pan;
    SInt16 gp_4_pan;
    SInt16 aux_gp_meter_pre_post;
    SInt16 main_l_r_delay_time;
    SInt16 main_l_r_gate_range;
    SInt16 main_l_r_gate_threshold;
    SInt16 main_l_r_gate_attack;
    SInt16 main_l_r_gate_release;
    SInt16 main_l_r_gate_hold;
    SInt16 main_l_r_expand_ratio;
    SInt16 main_l_r_expand_threshold;
    SInt16 main_l_r_expand_attack;
    SInt16 main_l_r_expand_release;
    SInt16 main_l_r_compressor_ratio;
    SInt16 main_l_r_compressor_threshold;
    SInt16 main_l_r_compressor_attack;
    SInt16 main_l_r_compressor_release;
    SInt16 main_l_r_compressor_out_gain;
    SInt16 main_l_r_limiter_threshold;
    SInt16 main_l_r_limiter_attack;
    SInt16 main_l_r_limiter_release;
    SInt16 main_l_r_eq_1_db;
    SInt16 main_l_r_eq_2_db;
    SInt16 main_l_r_eq_3_db;
    SInt16 main_l_r_eq_4_db;
    SInt16 main_l_r_eq_1_q;
    SInt16 main_l_r_eq_2_q;
    SInt16 main_l_r_eq_3_q;
    SInt16 main_l_r_eq_4_q;
    SInt16 main_l_r_eq_1_freq;
    SInt16 main_l_r_eq_2_freq;
    SInt16 main_l_r_eq_3_freq;
    SInt16 main_l_r_eq_4_freq;
    SInt16 geq_20_hz_db;
    SInt16 geq_25_hz_db;
    SInt16 geq_31_5_hz_db;
    SInt16 geq_40_hz_db;
    SInt16 geq_50_hz_db;
    SInt16 geq_63_hz_db;
    SInt16 geq_80_hz_db;
    SInt16 geq_100_hz_db;
    SInt16 geq_125_hz_db;
    SInt16 geq_160_hz_db;
    SInt16 geq_200_hz_db;
    SInt16 geq_250_hz_db;
    SInt16 geq_315_hz_db;
    SInt16 geq_400_hz_db;
    SInt16 geq_500_hz_db;
    SInt16 geq_630_hz_db;
    SInt16 geq_800_hz_db;
    SInt16 geq_1_khz_db;
    SInt16 geq_1_25_khz_db;
    SInt16 geq_1_6_khz_db;
    SInt16 geq_2_khz_db;
    SInt16 geq_2_5_khz_db;
    SInt16 geq_3_15_khz_db;
    SInt16 geq_4_khz_db;
    SInt16 geq_5_khz_db;
    SInt16 geq_6_3_khz_db;
    SInt16 geq_8_khz_db;
    SInt16 geq_10_khz_db;
    SInt16 geq_12_5_khz_db;
    SInt16 geq_16_khz_db;
    SInt16 geq_20_khz_db;
    unsigned char crc;
} ChannelGp1_4PagePacket;

typedef struct {
    unsigned int seq_num;
    unsigned int command;
    unsigned int channel_page;
    unsigned int reserve_2;
    SInt16 main_meter_pre_post;
    SInt16 multi_1_ctrl;
    SInt16 multi_2_ctrl;
    SInt16 multi_3_ctrl;
    SInt16 multi_4_ctrl;
    SInt16 main_l_r_ctrl;
    SInt16 multi_1_fader;
    SInt16 multi_2_fader;
    SInt16 multi_3_fader;
    SInt16 multi_4_fader;
    SInt16 main_fader;
    SInt16 multi_1_4_meter_pre_post;
    SInt16 multi_1_delay_time;
    SInt16 multi_2_delay_time;
    SInt16 multi_3_delay_time;
    SInt16 multi_4_delay_time;
    SInt16 main_l_r_delay_time;
    SInt16 multi_1_gate_range;
    SInt16 multi_2_gate_range;
    SInt16 multi_3_gate_range;
    SInt16 multi_4_gate_range;
    SInt16 multi_1_gate_threshold;
    SInt16 multi_2_gate_threshold;
    SInt16 multi_3_gate_threshold;
    SInt16 multi_4_gate_threshold;
    SInt16 multi_1_gate_attack;
    SInt16 multi_2_gate_attack;
    SInt16 multi_3_gate_attack;
    SInt16 multi_4_gate_attack;
    SInt16 multi_1_gate_release;
    SInt16 multi_2_gate_release;
    SInt16 multi_3_gate_release;
    SInt16 multi_4_gate_release;
    SInt16 multi_1_gate_hold;
    SInt16 multi_2_gate_hold;
    SInt16 multi_3_gate_hold;
    SInt16 multi_4_gate_hold;
    SInt16 multi_1_expand_ratio;
    SInt16 multi_2_expand_ratio;
    SInt16 multi_3_expand_ratio;
    SInt16 multi_4_expand_ratio;
    SInt16 multi_1_expand_threshold;
    SInt16 multi_2_expand_threshold;
    SInt16 multi_3_expand_threshold;
    SInt16 multi_4_expand_threshold;
    SInt16 multi_1_expand_attack;
    SInt16 multi_2_expand_attack;
    SInt16 multi_3_expand_attack;
    SInt16 multi_4_expand_attack;
    SInt16 multi_1_expand_release;
    SInt16 multi_2_expand_release;
    SInt16 multi_3_expand_release;
    SInt16 multi_4_expand_release;
    SInt16 multi_1_compressor_ratio;
    SInt16 multi_2_compressor_ratio;
    SInt16 multi_3_compressor_ratio;
    SInt16 multi_4_compressor_ratio;
    SInt16 multi_1_compressor_threshold;
    SInt16 multi_2_compressor_threshold;
    SInt16 multi_3_compressor_threshold;
    SInt16 multi_4_compressor_threshold;
    SInt16 multi_1_compressor_attack;
    SInt16 multi_2_compressor_attack;
    SInt16 multi_3_compressor_attack;
    SInt16 multi_4_compressor_attack;
    SInt16 multi_1_compressor_release;
    SInt16 multi_2_compressor_release;
    SInt16 multi_3_compressor_release;
    SInt16 multi_4_compressor_release;
    SInt16 multi_1_compressor_out_gain;
    SInt16 multi_2_compressor_out_gain;
    SInt16 multi_3_compressor_out_gain;
    SInt16 multi_4_compressor_out_gain;
    SInt16 multi_1_limiter_threshold;
    SInt16 multi_2_limiter_threshold;
    SInt16 multi_3_limiter_threshold;
    SInt16 multi_4_limiter_threshold;
    SInt16 multi_1_limiter_attack;
    SInt16 multi_2_limiter_attack;
    SInt16 multi_3_limiter_attack;
    SInt16 multi_4_limiter_attack;
    SInt16 multi_1_limiter_release;
    SInt16 multi_2_limiter_release;
    SInt16 multi_3_limiter_release;
    SInt16 multi_4_limiter_release;
    SInt16 main_l_r_gate_range;
    SInt16 main_l_r_gate_threshold;
    SInt16 main_l_r_gate_attack;
    SInt16 main_l_r_gate_release;
    SInt16 main_l_r_gate_hold;
    SInt16 main_l_r_expand_ratio;
    SInt16 main_l_r_expand_threshold;
    SInt16 main_l_r_expand_attack;
    SInt16 main_l_r_expand_release;
    SInt16 main_l_r_compressor_ratio;
    SInt16 main_l_r_compressor_threshold;
    SInt16 main_l_r_compressor_attack;
    SInt16 main_l_r_compressor_release;
    SInt16 main_l_r_compressor_out_gain;
    SInt16 main_l_r_limiter_threshold;
    SInt16 main_l_r_limiter_attack;
    SInt16 main_l_r_limiter_release;    
    SInt16 multi_1_eq_1_db;
    SInt16 multi_1_eq_2_db;
    SInt16 multi_1_eq_3_db;
    SInt16 multi_1_eq_4_db;
    SInt16 multi_2_eq_1_db;
    SInt16 multi_2_eq_2_db;
    SInt16 multi_2_eq_3_db;
    SInt16 multi_2_eq_4_db;
    SInt16 multi_3_eq_1_db;
    SInt16 multi_3_eq_2_db;
    SInt16 multi_3_eq_3_db;
    SInt16 multi_3_eq_4_db;
    SInt16 multi_4_eq_1_db;
    SInt16 multi_4_eq_2_db;
    SInt16 multi_4_eq_3_db;
    SInt16 multi_4_eq_4_db;
    SInt16 multi_1_eq_1_q;
    SInt16 multi_1_eq_2_q;
    SInt16 multi_1_eq_3_q;
    SInt16 multi_1_eq_4_q;
    SInt16 multi_2_eq_1_q;
    SInt16 multi_2_eq_2_q;
    SInt16 multi_2_eq_3_q;
    SInt16 multi_2_eq_4_q;
    SInt16 multi_3_eq_1_q;
    SInt16 multi_3_eq_2_q;
    SInt16 multi_3_eq_3_q;
    SInt16 multi_3_eq_4_q;
    SInt16 multi_4_eq_1_q;
    SInt16 multi_4_eq_2_q;
    SInt16 multi_4_eq_3_q;
    SInt16 multi_4_eq_4_q;
    SInt16 multi_1_eq_1_freq;
    SInt16 multi_1_eq_2_freq;
    SInt16 multi_1_eq_3_freq;
    SInt16 multi_1_eq_4_freq;
    SInt16 multi_2_eq_1_freq;
    SInt16 multi_2_eq_2_freq;
    SInt16 multi_2_eq_3_freq;
    SInt16 multi_2_eq_4_freq;
    SInt16 multi_3_eq_1_freq;
    SInt16 multi_3_eq_2_freq;
    SInt16 multi_3_eq_3_freq;
    SInt16 multi_3_eq_4_freq;
    SInt16 multi_4_eq_1_freq;
    SInt16 multi_4_eq_2_freq;
    SInt16 multi_4_eq_3_freq;
    SInt16 multi_4_eq_4_freq;
    SInt16 main_l_r_eq_1_db;
    SInt16 main_l_r_eq_2_db;
    SInt16 main_l_r_eq_3_db;
    SInt16 main_l_r_eq_4_db;
    SInt16 main_l_r_eq_1_q;
    SInt16 main_l_r_eq_2_q;
    SInt16 main_l_r_eq_3_q;
    SInt16 main_l_r_eq_4_q;
    SInt16 main_l_r_eq_1_freq;
    SInt16 main_l_r_eq_2_freq;
    SInt16 main_l_r_eq_3_freq;
    SInt16 main_l_r_eq_4_freq;
    SInt16 geq_20_hz_db;
    SInt16 geq_25_hz_db;
    SInt16 geq_31_5_hz_db;
    SInt16 geq_40_hz_db;
    SInt16 geq_50_hz_db;
    SInt16 geq_63_hz_db;
    SInt16 geq_80_hz_db;
    SInt16 geq_100_hz_db;
    SInt16 geq_125_hz_db;
    SInt16 geq_160_hz_db;
    SInt16 geq_200_hz_db;
    SInt16 geq_250_hz_db;
    SInt16 geq_315_hz_db;
    SInt16 geq_400_hz_db;
    SInt16 geq_500_hz_db;
    SInt16 geq_630_hz_db;
    SInt16 geq_800_hz_db;
    SInt16 geq_1_khz_db;
    SInt16 geq_1_25_khz_db;
    SInt16 geq_1_6_khz_db;
    SInt16 geq_2_khz_db;
    SInt16 geq_2_5_khz_db;
    SInt16 geq_3_15_khz_db;
    SInt16 geq_4_khz_db;
    SInt16 geq_5_khz_db;
    SInt16 geq_6_3_khz_db;
    SInt16 geq_8_khz_db;
    SInt16 geq_10_khz_db;
    SInt16 geq_12_5_khz_db;
    SInt16 geq_16_khz_db;
    SInt16 geq_20_khz_db;
    unsigned char crc;
} ChannelMulti1_4PagePacket;

typedef struct {
    unsigned int seq_num;
    unsigned int command;
    unsigned int channel_page;
    unsigned int reserve_2;
    SInt16 digital_in_out_ctrl;
    SInt16 main_meter_pre_post;
    SInt16 main_l_r_ctrl;
    SInt16 control_room_trim_level;
    SInt16 main_fader;
    SInt16 control_room_l_meter_level;
    SInt16 control_room_r_meter_level;
    SInt16 main_l_r_delay_time;
    SInt16 main_l_r_gate_range;
    SInt16 main_l_r_gate_threshold;
    SInt16 main_l_r_gate_attack;
    SInt16 main_l_r_gate_release;
    SInt16 main_l_r_gate_hold;
    SInt16 main_l_r_expand_ratio;
    SInt16 main_l_r_expand_threshold;
    SInt16 main_l_r_expand_attack;
    SInt16 main_l_r_expand_release;
    SInt16 main_l_r_compressor_ratio;
    SInt16 main_l_r_compressor_threshold;
    SInt16 main_l_r_compressor_attack;
    SInt16 main_l_r_compressor_release;
    SInt16 main_l_r_compressor_out_gain;
    SInt16 main_l_r_limiter_threshold;
    SInt16 main_l_r_limiter_attack;
    SInt16 main_l_r_limiter_release;
    SInt16 main_l_r_eq_1_db;
    SInt16 main_l_r_eq_2_db;
    SInt16 main_l_r_eq_3_db;
    SInt16 main_l_r_eq_4_db;
    SInt16 main_l_r_eq_1_q;
    SInt16 main_l_r_eq_2_q;
    SInt16 main_l_r_eq_3_q;
    SInt16 main_l_r_eq_4_q;
    SInt16 main_l_r_eq_1_freq;
    SInt16 main_l_r_eq_2_freq;
    SInt16 main_l_r_eq_3_freq;
    SInt16 main_l_r_eq_4_freq;
    SInt16 geq_20_hz_db;
    SInt16 geq_25_hz_db;
    SInt16 geq_31_5_hz_db;
    SInt16 geq_40_hz_db;
    SInt16 geq_50_hz_db;
    SInt16 geq_63_hz_db;
    SInt16 geq_80_hz_db;
    SInt16 geq_100_hz_db;
    SInt16 geq_125_hz_db;
    SInt16 geq_160_hz_db;
    SInt16 geq_200_hz_db;
    SInt16 geq_250_hz_db;
    SInt16 geq_315_hz_db;
    SInt16 geq_400_hz_db;
    SInt16 geq_500_hz_db;
    SInt16 geq_630_hz_db;
    SInt16 geq_800_hz_db;
    SInt16 geq_1_khz_db;
    SInt16 geq_1_25_khz_db;
    SInt16 geq_1_6_khz_db;
    SInt16 geq_2_khz_db;
    SInt16 geq_2_5_khz_db;
    SInt16 geq_3_15_khz_db;
    SInt16 geq_4_khz_db;
    SInt16 geq_5_khz_db;
    SInt16 geq_6_3_khz_db;
    SInt16 geq_8_khz_db;
    SInt16 geq_10_khz_db;
    SInt16 geq_12_5_khz_db;
    SInt16 geq_16_khz_db;
    SInt16 geq_20_khz_db;
    unsigned char crc;
} ChannelCtrlPagePacket;

typedef struct {
    unsigned int seq_num;
    unsigned int command;
    unsigned int reserve_1;
    unsigned int reserve_2;
    SInt16 effect_1_1_in_source;
    SInt16 effect_1_2_in_source;
    SInt16 effect_2_1_in_source;
    SInt16 effect_2_2_in_source;
    SInt16 effect_1_1_out_source;
    SInt16 effect_1_2_out_source;
    SInt16 effect_2_1_out_source;
    SInt16 effect_2_2_out_source;
    SInt16 effect_1_program;
    SInt16 effect_1_flanger_lfo_freq;
    SInt16 effect_1_flanger_lfo_phase;
    SInt16 effect_1_flanger_lfo_type;
    SInt16 effect_1_flanger_depth;
    SInt16 effect_1_flanger_pre_delay;
    SInt16 effect_1_flanger_lpf_freq;
    SInt16 effect_1_flanger_fb;
    SInt16 effect_1_chorus_lfo_freq;
    SInt16 effect_1_chorus_lfo_phase;
    SInt16 effect_1_chorus_lfo_type;
    SInt16 effect_1_chorus_depth;
    SInt16 effect_1_chorus_pre_delay;
    SInt16 effect_1_chorus_lpf_freq;
    SInt16 effect_1_vibrato_lfo_freq;
    SInt16 effect_1_vibrato_lfo_type;
    SInt16 effect_1_vibrato_depth;
    SInt16 effect_1_vibrato_freq;
    SInt16 effect_1_phaser_lfo_freq;
    SInt16 effect_1_phaser_lfo_type;
    SInt16 effect_1_phaser_depth;
    SInt16 effect_1_phaser_freq;
    SInt16 effect_1_phaser_stage_no;
    SInt16 effect_1_tremolo_lfo_freq;
    SInt16 effect_1_tremolo_lfo_type;
    SInt16 effect_1_tremolo_depth;
    SInt16 effect_1_autopan_lfo_freq;
    SInt16 effect_1_autopan_lfo_type;
    SInt16 effect_1_autopan_depth;
    SInt16 effect_1_autopan_way;
    SInt16 effect_1_tap_key;
    SInt16 effect_1_delay_time;
    SInt16 effect_1_tap_delay_fb;
    SInt16 effect_1_tap_delay_status;
    SInt16 effect_1_tap_delay_fb_hpf;
    SInt16 effect_1_tap_delay_fb_lpf;
    SInt16 effect_1_echo_delay1_time;
    SInt16 effect_1_echo_delay2_time;
    SInt16 effect_1_echo_delay_fb1;
    SInt16 effect_1_echo_delay_fb2;
    SInt16 effect_1_echo_delay_fb_hpf;
    SInt16 effect_1_echo_delay_fb_lpf;
    SInt16 effect_1_reverb_type;
    SInt16 effect_1_reverb_lpf_freq;
    SInt16 effect_1_reverb_hpf_freq;
    SInt16 effect_1_reverb_time;
    SInt16 effect_1_pre_delay;
    SInt16 effect_1_reverb_early_delay_out;
    SInt16 effect_1_reverb_hi_ratio;
    SInt16 effect_1_reverb_density;
    SInt16 effect_1_reverb_level;
    SInt16 effect_1_reverb_gate;
    SInt16 effect_1_reverb_gate_threshold;
    SInt16 effect_1_reverb_gate_hold_time;
    SInt16 effect_1_dry_wet_mix;
    SInt16 effect_2_program;
    SInt16 effect_control;
    SInt16 effect_2_dry_wet_mix;
    SInt16 effect_2_flanger_lfo_freq;
    SInt16 effect_2_flanger_lfo_phase;
    SInt16 effect_2_flanger_lfo_type;
    SInt16 effect_2_flanger_depth;
    SInt16 effect_2_flanger_pre_delay;
    SInt16 effect_2_flanger_lpf_freq;
    SInt16 effect_2_flanger_fb;
    SInt16 effect_2_chorus_lfo_freq;
    SInt16 effect_2_chorus_lfo_phase;
    SInt16 effect_2_chorus_lfo_type;
    SInt16 effect_2_chorus_depth;
    SInt16 effect_2_chorus_pre_delay;
    SInt16 effect_2_chorus_lpf_freq;
    SInt16 effect_2_vibrato_lfo_freq;
    SInt16 effect_2_vibrato_lfo_type;
    SInt16 effect_2_vibrato_depth;
    SInt16 effect_2_vibrato_freq;
    SInt16 effect_2_phaser_lfo_freq;
    SInt16 effect_2_phaser_lfo_type;
    SInt16 effect_2_phaser_depth;
    SInt16 effect_2_phaser_freq;
    SInt16 effect_2_phaser_stage_no;
    SInt16 effect_2_tremolo_lfo_freq;
    SInt16 effect_2_tremolo_lfo_type;
    SInt16 effect_2_tremolo_depth;
    SInt16 effect_2_autopan_lfo_freq;
    SInt16 effect_2_autopan_lfo_type;
    SInt16 effect_2_autopan_depth;
    SInt16 effect_2_autopan_way;
    SInt16 effect_2_tap_key;
    SInt16 effect_2_delay_time;
    SInt16 effect_2_tap_delay_fb;
    SInt16 effect_2_tap_delay_status;
    SInt16 effect_2_tap_delay_fb_hpf;
    SInt16 effect_2_tap_delay_fb_lpf;
    SInt16 effect_2_echo_delay1_time;
    SInt16 effect_2_echo_delay2_time;
    SInt16 effect_2_echo_delay_fb1;
    SInt16 effect_2_echo_delay_fb2;
    SInt16 effect_2_echo_delay_fb_hpf;
    SInt16 effect_2_echo_delay_fb_lpf;
    SInt16 effect_1_1_out_meter;
    SInt16 effect_1_2_out_meter;
    SInt16 effect_1_1_in_meter;
    SInt16 effect_1_2_in_meter;
    SInt16 effect_2_1_out_meter;
    SInt16 effect_2_2_out_meter;
    SInt16 effect_2_1_in_meter;
    SInt16 effect_2_2_in_meter;
    SInt16 gp_to_main;
    SInt16 effect_control_2;
    SInt16 efx_1_level;
    SInt16 efx_2_level;
    // GEQ31
    SInt16 l_geq31_20_hz_db;
    SInt16 l_geq31_25_hz_db;
    SInt16 l_geq31_31_5_hz_db;
    SInt16 l_geq31_40_hz_db;
    SInt16 l_geq31_50_hz_db;
    SInt16 l_geq31_63_hz_db;
    SInt16 l_geq31_80_hz_db;
    SInt16 l_geq31_100_hz_db;
    SInt16 l_geq31_125_hz_db;
    SInt16 l_geq31_160_hz_db;
    SInt16 l_geq31_200_hz_db;
    SInt16 l_geq31_250_hz_db;
    SInt16 l_geq31_315_hz_db;
    SInt16 l_geq31_400_hz_db;
    SInt16 l_geq31_500_hz_db;
    SInt16 l_geq31_630_hz_db;
    SInt16 l_geq31_800_hz_db;
    SInt16 l_geq31_1_khz_db;
    SInt16 l_geq31_1_25_khz_db;
    SInt16 l_geq31_1_6_khz_db;
    SInt16 l_geq31_2_khz_db;
    SInt16 l_geq31_2_5_khz_db;
    SInt16 l_geq31_3_15_khz_db;
    SInt16 l_geq31_4_khz_db;
    SInt16 l_geq31_5_khz_db;
    SInt16 l_geq31_6_3_khz_db;
    SInt16 l_geq31_8_khz_db;
    SInt16 l_geq31_10_khz_db;
    SInt16 l_geq31_12_5_khz_db;
    SInt16 l_geq31_16_khz_db;
    SInt16 l_geq31_20_khz_db;
    SInt16 r_geq31_20_hz_db;
    SInt16 r_geq31_25_hz_db;
    SInt16 r_geq31_31_5_hz_db;
    SInt16 r_geq31_40_hz_db;
    SInt16 r_geq31_50_hz_db;
    SInt16 r_geq31_63_hz_db;
    SInt16 r_geq31_80_hz_db;
    SInt16 r_geq31_100_hz_db;
    SInt16 r_geq31_125_hz_db;
    SInt16 r_geq31_160_hz_db;
    SInt16 r_geq31_200_hz_db;
    SInt16 r_geq31_250_hz_db;
    SInt16 r_geq31_315_hz_db;
    SInt16 r_geq31_400_hz_db;
    SInt16 r_geq31_500_hz_db;
    SInt16 r_geq31_630_hz_db;
    SInt16 r_geq31_800_hz_db;
    SInt16 r_geq31_1_khz_db;
    SInt16 r_geq31_1_25_khz_db;
    SInt16 r_geq31_1_6_khz_db;
    SInt16 r_geq31_2_khz_db;
    SInt16 r_geq31_2_5_khz_db;
    SInt16 r_geq31_3_15_khz_db;
    SInt16 r_geq31_4_khz_db;
    SInt16 r_geq31_5_khz_db;
    SInt16 r_geq31_6_3_khz_db;
    SInt16 r_geq31_8_khz_db;
    SInt16 r_geq31_10_khz_db;
    SInt16 r_geq31_12_5_khz_db;
    SInt16 r_geq31_16_khz_db;
    SInt16 r_geq31_20_khz_db;
    // GEQ15
    SInt16 l_geq15_25_hz_db;
    SInt16 l_geq15_40_hz_db;
    SInt16 l_geq15_63_hz_db;
    SInt16 l_geq15_100_hz_db;
    SInt16 l_geq15_160_hz_db;
    SInt16 l_geq15_250_hz_db;
    SInt16 l_geq15_400_hz_db;
    SInt16 l_geq15_630_hz_db;
    SInt16 l_geq15_1_khz_db;
    SInt16 l_geq15_1_6_khz_db;
    SInt16 l_geq15_2_5_khz_db;
    SInt16 l_geq15_4_khz_db;
    SInt16 l_geq15_6_3_khz_db;
    SInt16 l_geq15_10_khz_db;
    SInt16 l_geq15_16_khz_db;
    SInt16 r_geq15_25_hz_db;
    SInt16 r_geq15_40_hz_db;
    SInt16 r_geq15_63_hz_db;
    SInt16 r_geq15_100_hz_db;
    SInt16 r_geq15_160_hz_db;
    SInt16 r_geq15_250_hz_db;
    SInt16 r_geq15_400_hz_db;
    SInt16 r_geq15_630_hz_db;
    SInt16 r_geq15_1_khz_db;
    SInt16 r_geq15_1_6_khz_db;
    SInt16 r_geq15_2_5_khz_db;
    SInt16 r_geq15_4_khz_db;
    SInt16 r_geq15_6_3_khz_db;
    SInt16 r_geq15_10_khz_db;
    SInt16 r_geq15_16_khz_db;
    unsigned char crc;
} EffectPagePacket;

typedef struct {
    unsigned int seq_num;
    unsigned int command;
    unsigned int aux_group; // 0 = Aux, 1 = group
    unsigned int reserve_2;
    SInt16 view_aux_1_ch_1_meter;
    SInt16 view_aux_1_ch_2_meter;
    SInt16 view_aux_1_ch_3_meter;
    SInt16 view_aux_1_ch_4_meter;
    SInt16 view_aux_1_ch_5_meter;
    SInt16 view_aux_1_ch_6_meter;
    SInt16 view_aux_1_ch_7_meter;
    SInt16 view_aux_1_ch_8_meter;
    SInt16 view_aux_1_ch_9_meter;
    SInt16 view_aux_1_ch_10_meter;
    SInt16 view_aux_1_ch_11_meter;
    SInt16 view_aux_1_ch_12_meter;
    SInt16 view_aux_1_ch_13_meter;
    SInt16 view_aux_1_ch_14_meter;
    SInt16 view_aux_1_ch_15_meter;
    SInt16 view_aux_1_ch_16_meter;
    SInt16 view_aux_2_ch_1_meter;
    SInt16 view_aux_2_ch_2_meter;
    SInt16 view_aux_2_ch_3_meter;
    SInt16 view_aux_2_ch_4_meter;
    SInt16 view_aux_2_ch_5_meter;
    SInt16 view_aux_2_ch_6_meter;
    SInt16 view_aux_2_ch_7_meter;
    SInt16 view_aux_2_ch_8_meter;
    SInt16 view_aux_2_ch_9_meter;
    SInt16 view_aux_2_ch_10_meter;
    SInt16 view_aux_2_ch_11_meter;
    SInt16 view_aux_2_ch_12_meter;
    SInt16 view_aux_2_ch_13_meter;
    SInt16 view_aux_2_ch_14_meter;
    SInt16 view_aux_2_ch_15_meter;
    SInt16 view_aux_2_ch_16_meter;
    SInt16 view_aux_3_ch_1_meter;
    SInt16 view_aux_3_ch_2_meter;
    SInt16 view_aux_3_ch_3_meter;
    SInt16 view_aux_3_ch_4_meter;
    SInt16 view_aux_3_ch_5_meter;
    SInt16 view_aux_3_ch_6_meter;
    SInt16 view_aux_3_ch_7_meter;
    SInt16 view_aux_3_ch_8_meter;
    SInt16 view_aux_3_ch_9_meter;
    SInt16 view_aux_3_ch_10_meter;
    SInt16 view_aux_3_ch_11_meter;
    SInt16 view_aux_3_ch_12_meter;
    SInt16 view_aux_3_ch_13_meter;
    SInt16 view_aux_3_ch_14_meter;
    SInt16 view_aux_3_ch_15_meter;
    SInt16 view_aux_3_ch_16_meter;
    SInt16 view_aux_4_ch_1_meter;
    SInt16 view_aux_4_ch_2_meter;
    SInt16 view_aux_4_ch_3_meter;
    SInt16 view_aux_4_ch_4_meter;
    SInt16 view_aux_4_ch_5_meter;
    SInt16 view_aux_4_ch_6_meter;
    SInt16 view_aux_4_ch_7_meter;
    SInt16 view_aux_4_ch_8_meter;
    SInt16 view_aux_4_ch_9_meter;
    SInt16 view_aux_4_ch_10_meter;
    SInt16 view_aux_4_ch_11_meter;
    SInt16 view_aux_4_ch_12_meter;
    SInt16 view_aux_4_ch_13_meter;
    SInt16 view_aux_4_ch_14_meter;
    SInt16 view_aux_4_ch_15_meter;
    SInt16 view_aux_4_ch_16_meter;
    SInt16 view_aux_1_ch_17_meter;
    SInt16 view_aux_1_ch_18_meter;
    SInt16 view_aux_2_ch_17_meter;
    SInt16 view_aux_2_ch_18_meter;
    SInt16 view_aux_3_ch_17_meter;
    SInt16 view_aux_3_ch_18_meter;
    SInt16 view_aux_4_ch_17_meter;
    SInt16 view_aux_4_ch_18_meter;
    SInt16 aux_1_ch_1_level;
    SInt16 aux_1_ch_2_level;
    SInt16 aux_1_ch_3_level;
    SInt16 aux_1_ch_4_level;
    SInt16 aux_1_ch_5_level;
    SInt16 aux_1_ch_6_level;
    SInt16 aux_1_ch_7_level;
    SInt16 aux_1_ch_8_level;
    SInt16 aux_1_ch_9_level;
    SInt16 aux_1_ch_10_level;
    SInt16 aux_1_ch_11_level;
    SInt16 aux_1_ch_12_level;
    SInt16 aux_1_ch_13_level;
    SInt16 aux_1_ch_14_level;
    SInt16 aux_1_ch_15_level;
    SInt16 aux_1_ch_16_level;
    SInt16 aux_2_ch_1_level;
    SInt16 aux_2_ch_2_level;
    SInt16 aux_2_ch_3_level;
    SInt16 aux_2_ch_4_level;
    SInt16 aux_2_ch_5_level;
    SInt16 aux_2_ch_6_level;
    SInt16 aux_2_ch_7_level;
    SInt16 aux_2_ch_8_level;
    SInt16 aux_2_ch_9_level;
    SInt16 aux_2_ch_10_level;
    SInt16 aux_2_ch_11_level;
    SInt16 aux_2_ch_12_level;
    SInt16 aux_2_ch_13_level;
    SInt16 aux_2_ch_14_level;
    SInt16 aux_2_ch_15_level;
    SInt16 aux_2_ch_16_level;
    SInt16 aux_3_ch_1_level;
    SInt16 aux_3_ch_2_level;
    SInt16 aux_3_ch_3_level;
    SInt16 aux_3_ch_4_level;
    SInt16 aux_3_ch_5_level;
    SInt16 aux_3_ch_6_level;
    SInt16 aux_3_ch_7_level;
    SInt16 aux_3_ch_8_level;
    SInt16 aux_3_ch_9_level;
    SInt16 aux_3_ch_10_level;
    SInt16 aux_3_ch_11_level;
    SInt16 aux_3_ch_12_level;
    SInt16 aux_3_ch_13_level;
    SInt16 aux_3_ch_14_level;
    SInt16 aux_3_ch_15_level;
    SInt16 aux_3_ch_16_level;
    SInt16 aux_4_ch_1_level;
    SInt16 aux_4_ch_2_level;
    SInt16 aux_4_ch_3_level;
    SInt16 aux_4_ch_4_level;
    SInt16 aux_4_ch_5_level;
    SInt16 aux_4_ch_6_level;
    SInt16 aux_4_ch_7_level;
    SInt16 aux_4_ch_8_level;
    SInt16 aux_4_ch_9_level;
    SInt16 aux_4_ch_10_level;
    SInt16 aux_4_ch_11_level;
    SInt16 aux_4_ch_12_level;
    SInt16 aux_4_ch_13_level;
    SInt16 aux_4_ch_14_level;
    SInt16 aux_4_ch_15_level;
    SInt16 aux_4_ch_16_level;
    SInt16 aux_1_ch_17_level;
    SInt16 aux_1_ch_18_level;
    SInt16 aux_2_ch_17_level;
    SInt16 aux_2_ch_18_level;
    SInt16 aux_3_ch_17_level;
    SInt16 aux_3_ch_18_level;
    SInt16 aux_4_ch_17_level;
    SInt16 aux_4_ch_18_level;
    SInt16 aux_1_send;
    SInt16 aux_2_send;
    SInt16 aux_3_send;
    SInt16 aux_4_send;
    SInt16 ch_17_audio_setting;
    SInt16 ch_18_audio_setting;
    SInt16 view_meter_control;
    SInt16 solo_ctrl;
    SInt16 solo_safe_ctrl;
    SInt16 aux_gp_master;
    SInt16 aux_1_master_fader;
    SInt16 aux_2_master_fader;
    SInt16 aux_3_master_fader;
    SInt16 aux_4_master_fader;
    SInt16 aux_gp_meter_master;
    SInt16 multi_1_source;
    SInt16 multi_2_source;
    SInt16 multi_3_source;
    SInt16 multi_4_source;
    unsigned char crc;
} AuxPagePacket;

typedef struct {
    unsigned int seq_num;
    unsigned int command;
    unsigned int aux_group; // 0 = Aux, 1 = group
    unsigned int reserve_2;
    SInt16 view_gp_1_ch_1_meter;
    SInt16 view_gp_1_ch_2_meter;
    SInt16 view_gp_1_ch_3_meter;
    SInt16 view_gp_1_ch_4_meter;
    SInt16 view_gp_1_ch_5_meter;
    SInt16 view_gp_1_ch_6_meter;
    SInt16 view_gp_1_ch_7_meter;
    SInt16 view_gp_1_ch_8_meter;
    SInt16 view_gp_1_ch_9_meter;
    SInt16 view_gp_1_ch_10_meter;
    SInt16 view_gp_1_ch_11_meter;
    SInt16 view_gp_1_ch_12_meter;
    SInt16 view_gp_1_ch_13_meter;
    SInt16 view_gp_1_ch_14_meter;
    SInt16 view_gp_1_ch_15_meter;
    SInt16 view_gp_1_ch_16_meter;
    SInt16 view_gp_2_ch_1_meter;
    SInt16 view_gp_2_ch_2_meter;
    SInt16 view_gp_2_ch_3_meter;
    SInt16 view_gp_2_ch_4_meter;
    SInt16 view_gp_2_ch_5_meter;
    SInt16 view_gp_2_ch_6_meter;
    SInt16 view_gp_2_ch_7_meter;
    SInt16 view_gp_2_ch_8_meter;
    SInt16 view_gp_2_ch_9_meter;
    SInt16 view_gp_2_ch_10_meter;
    SInt16 view_gp_2_ch_11_meter;
    SInt16 view_gp_2_ch_12_meter;
    SInt16 view_gp_2_ch_13_meter;
    SInt16 view_gp_2_ch_14_meter;
    SInt16 view_gp_2_ch_15_meter;
    SInt16 view_gp_2_ch_16_meter;
    SInt16 view_gp_3_ch_1_meter;
    SInt16 view_gp_3_ch_2_meter;
    SInt16 view_gp_3_ch_3_meter;
    SInt16 view_gp_3_ch_4_meter;
    SInt16 view_gp_3_ch_5_meter;
    SInt16 view_gp_3_ch_6_meter;
    SInt16 view_gp_3_ch_7_meter;
    SInt16 view_gp_3_ch_8_meter;
    SInt16 view_gp_3_ch_9_meter;
    SInt16 view_gp_3_ch_10_meter;
    SInt16 view_gp_3_ch_11_meter;
    SInt16 view_gp_3_ch_12_meter;
    SInt16 view_gp_3_ch_13_meter;
    SInt16 view_gp_3_ch_14_meter;
    SInt16 view_gp_3_ch_15_meter;
    SInt16 view_gp_3_ch_16_meter;
    SInt16 view_gp_4_ch_1_meter;
    SInt16 view_gp_4_ch_2_meter;
    SInt16 view_gp_4_ch_3_meter;
    SInt16 view_gp_4_ch_4_meter;
    SInt16 view_gp_4_ch_5_meter;
    SInt16 view_gp_4_ch_6_meter;
    SInt16 view_gp_4_ch_7_meter;
    SInt16 view_gp_4_ch_8_meter;
    SInt16 view_gp_4_ch_9_meter;
    SInt16 view_gp_4_ch_10_meter;
    SInt16 view_gp_4_ch_11_meter;
    SInt16 view_gp_4_ch_12_meter;
    SInt16 view_gp_4_ch_13_meter;
    SInt16 view_gp_4_ch_14_meter;
    SInt16 view_gp_4_ch_15_meter;
    SInt16 view_gp_4_ch_16_meter;
    SInt16 view_gp_1_ch_17_meter;
    SInt16 view_gp_1_ch_18_meter;
    SInt16 view_gp_2_ch_17_meter;
    SInt16 view_gp_2_ch_18_meter;
    SInt16 view_gp_3_ch_17_meter;
    SInt16 view_gp_3_ch_18_meter;
    SInt16 view_gp_4_ch_17_meter;
    SInt16 view_gp_4_ch_18_meter;
    SInt16 gp_1_assign;
    SInt16 gp_2_assign;
    SInt16 gp_3_assign;
    SInt16 gp_4_assign;
    SInt16 ch_17_audio_setting;
    SInt16 ch_18_audio_setting;
    SInt16 view_meter_control;
    SInt16 gp_1_master_pan;
    SInt16 gp_2_master_pan;
    SInt16 gp_3_master_pan;
    SInt16 gp_4_master_pan;
    SInt16 solo_ctrl;
    SInt16 solo_safe_ctrl;
    SInt16 aux_gp_master;
    SInt16 gp_1_master_fader;
    SInt16 gp_2_master_fader;
    SInt16 gp_3_master_fader;
    SInt16 gp_4_master_fader;
    SInt16 aux_gp_meter_master;
    SInt16 multi_1_source;
    SInt16 multi_2_source;
    SInt16 multi_3_source;
    SInt16 multi_4_source;
    SInt16 effect_control;
    SInt16 gp_to_main;
    SInt16 effect_1_1_out_source;
    SInt16 effect_1_2_out_source;
    SInt16 effect_2_1_out_source;
    SInt16 effect_2_2_out_source;
    unsigned char crc;
} GpPagePacket;

typedef struct {
    unsigned int seq_num;
    unsigned int command;
    unsigned int page_type; // 0 = channel, 1 = multi , 2 = main
    unsigned int reserve_2;
    SInt16 ch_1_ctrl;
    SInt16 ch_2_ctrl;
    SInt16 ch_3_ctrl;
    SInt16 ch_4_ctrl;
    SInt16 ch_5_ctrl;
    SInt16 ch_6_ctrl;
    SInt16 ch_7_ctrl;
    SInt16 ch_8_ctrl;
    SInt16 ch_9_ctrl;
    SInt16 ch_10_ctrl;
    SInt16 ch_11_ctrl;
    SInt16 ch_12_ctrl;
    SInt16 ch_13_ctrl;
    SInt16 ch_14_ctrl;
    SInt16 ch_15_ctrl;
    SInt16 ch_16_ctrl;
    SInt16 ch_17_ctrl;
    SInt16 ch_18_ctrl;
    SInt16 ch_1_eq_1_db;
    SInt16 ch_1_eq_2_db;
    SInt16 ch_1_eq_3_db;
    SInt16 ch_1_eq_4_db;
    SInt16 ch_2_eq_1_db;
    SInt16 ch_2_eq_2_db;
    SInt16 ch_2_eq_3_db;
    SInt16 ch_2_eq_4_db;
    SInt16 ch_3_eq_1_db;
    SInt16 ch_3_eq_2_db;
    SInt16 ch_3_eq_3_db;
    SInt16 ch_3_eq_4_db;
    SInt16 ch_4_eq_1_db;
    SInt16 ch_4_eq_2_db;
    SInt16 ch_4_eq_3_db;
    SInt16 ch_4_eq_4_db;
    SInt16 ch_5_eq_1_db;
    SInt16 ch_5_eq_2_db;
    SInt16 ch_5_eq_3_db;
    SInt16 ch_5_eq_4_db;
    SInt16 ch_6_eq_1_db;
    SInt16 ch_6_eq_2_db;
    SInt16 ch_6_eq_3_db;
    SInt16 ch_6_eq_4_db;
    SInt16 ch_7_eq_1_db;
    SInt16 ch_7_eq_2_db;
    SInt16 ch_7_eq_3_db;
    SInt16 ch_7_eq_4_db;
    SInt16 ch_8_eq_1_db;
    SInt16 ch_8_eq_2_db;
    SInt16 ch_8_eq_3_db;
    SInt16 ch_8_eq_4_db;
    SInt16 ch_9_eq_1_db;
    SInt16 ch_9_eq_2_db;
    SInt16 ch_9_eq_3_db;
    SInt16 ch_9_eq_4_db;
    SInt16 ch_10_eq_1_db;
    SInt16 ch_10_eq_2_db;
    SInt16 ch_10_eq_3_db;
    SInt16 ch_10_eq_4_db;
    SInt16 ch_11_eq_1_db;
    SInt16 ch_11_eq_2_db;
    SInt16 ch_11_eq_3_db;
    SInt16 ch_11_eq_4_db;
    SInt16 ch_12_eq_1_db;
    SInt16 ch_12_eq_2_db;
    SInt16 ch_12_eq_3_db;
    SInt16 ch_12_eq_4_db;
    SInt16 ch_13_eq_1_db;
    SInt16 ch_13_eq_2_db;
    SInt16 ch_13_eq_3_db;
    SInt16 ch_13_eq_4_db;
    SInt16 ch_14_eq_1_db;
    SInt16 ch_14_eq_2_db;
    SInt16 ch_14_eq_3_db;
    SInt16 ch_14_eq_4_db;
    SInt16 ch_15_eq_1_db;
    SInt16 ch_15_eq_2_db;
    SInt16 ch_15_eq_3_db;
    SInt16 ch_15_eq_4_db;
    SInt16 ch_16_eq_1_db;
    SInt16 ch_16_eq_2_db;
    SInt16 ch_16_eq_3_db;
    SInt16 ch_16_eq_4_db;
    SInt16 ch_1_eq_1_q;
    SInt16 ch_1_eq_2_q;
    SInt16 ch_1_eq_3_q;
    SInt16 ch_1_eq_4_q;
    SInt16 ch_2_eq_1_q;
    SInt16 ch_2_eq_2_q;
    SInt16 ch_2_eq_3_q;
    SInt16 ch_2_eq_4_q;
    SInt16 ch_3_eq_1_q;
    SInt16 ch_3_eq_2_q;
    SInt16 ch_3_eq_3_q;
    SInt16 ch_3_eq_4_q;
    SInt16 ch_4_eq_1_q;
    SInt16 ch_4_eq_2_q;
    SInt16 ch_4_eq_3_q;
    SInt16 ch_4_eq_4_q;
    SInt16 ch_5_eq_1_q;
    SInt16 ch_5_eq_2_q;
    SInt16 ch_5_eq_3_q;
    SInt16 ch_5_eq_4_q;
    SInt16 ch_6_eq_1_q;
    SInt16 ch_6_eq_2_q;
    SInt16 ch_6_eq_3_q;
    SInt16 ch_6_eq_4_q;
    SInt16 ch_7_eq_1_q;
    SInt16 ch_7_eq_2_q;
    SInt16 ch_7_eq_3_q;
    SInt16 ch_7_eq_4_q;
    SInt16 ch_8_eq_1_q;
    SInt16 ch_8_eq_2_q;
    SInt16 ch_8_eq_3_q;
    SInt16 ch_8_eq_4_q;
    SInt16 ch_9_eq_1_q;
    SInt16 ch_9_eq_2_q;
    SInt16 ch_9_eq_3_q;
    SInt16 ch_9_eq_4_q;
    SInt16 ch_10_eq_1_q;
    SInt16 ch_10_eq_2_q;
    SInt16 ch_10_eq_3_q;
    SInt16 ch_10_eq_4_q;
    SInt16 ch_11_eq_1_q;
    SInt16 ch_11_eq_2_q;
    SInt16 ch_11_eq_3_q;
    SInt16 ch_11_eq_4_q;
    SInt16 ch_12_eq_1_q;
    SInt16 ch_12_eq_2_q;
    SInt16 ch_12_eq_3_q;
    SInt16 ch_12_eq_4_q;
    SInt16 ch_13_eq_1_q;
    SInt16 ch_13_eq_2_q;
    SInt16 ch_13_eq_3_q;
    SInt16 ch_13_eq_4_q;
    SInt16 ch_14_eq_1_q;
    SInt16 ch_14_eq_2_q;
    SInt16 ch_14_eq_3_q;
    SInt16 ch_14_eq_4_q;
    SInt16 ch_15_eq_1_q;
    SInt16 ch_15_eq_2_q;
    SInt16 ch_15_eq_3_q;
    SInt16 ch_15_eq_4_q;
    SInt16 ch_16_eq_1_q;
    SInt16 ch_16_eq_2_q;
    SInt16 ch_16_eq_3_q;
    SInt16 ch_16_eq_4_q;
    SInt16 ch_1_eq_1_freq;
    SInt16 ch_1_eq_2_freq;
    SInt16 ch_1_eq_3_freq;
    SInt16 ch_1_eq_4_freq;
    SInt16 ch_2_eq_1_freq;
    SInt16 ch_2_eq_2_freq;
    SInt16 ch_2_eq_3_freq;
    SInt16 ch_2_eq_4_freq;
    SInt16 ch_3_eq_1_freq;
    SInt16 ch_3_eq_2_freq;
    SInt16 ch_3_eq_3_freq;
    SInt16 ch_3_eq_4_freq;
    SInt16 ch_4_eq_1_freq;
    SInt16 ch_4_eq_2_freq;
    SInt16 ch_4_eq_3_freq;
    SInt16 ch_4_eq_4_freq;
    SInt16 ch_5_eq_1_freq;
    SInt16 ch_5_eq_2_freq;
    SInt16 ch_5_eq_3_freq;
    SInt16 ch_5_eq_4_freq;
    SInt16 ch_6_eq_1_freq;
    SInt16 ch_6_eq_2_freq;
    SInt16 ch_6_eq_3_freq;
    SInt16 ch_6_eq_4_freq;
    SInt16 ch_7_eq_1_freq;
    SInt16 ch_7_eq_2_freq;
    SInt16 ch_7_eq_3_freq;
    SInt16 ch_7_eq_4_freq;
    SInt16 ch_8_eq_1_freq;
    SInt16 ch_8_eq_2_freq;
    SInt16 ch_8_eq_3_freq;
    SInt16 ch_8_eq_4_freq;
    SInt16 ch_9_eq_1_freq;
    SInt16 ch_9_eq_2_freq;
    SInt16 ch_9_eq_3_freq;
    SInt16 ch_9_eq_4_freq;
    SInt16 ch_10_eq_1_freq;
    SInt16 ch_10_eq_2_freq;
    SInt16 ch_10_eq_3_freq;
    SInt16 ch_10_eq_4_freq;
    SInt16 ch_11_eq_1_freq;
    SInt16 ch_11_eq_2_freq;
    SInt16 ch_11_eq_3_freq;
    SInt16 ch_11_eq_4_freq;
    SInt16 ch_12_eq_1_freq;
    SInt16 ch_12_eq_2_freq;
    SInt16 ch_12_eq_3_freq;
    SInt16 ch_12_eq_4_freq;
    SInt16 ch_13_eq_1_freq;
    SInt16 ch_13_eq_2_freq;
    SInt16 ch_13_eq_3_freq;
    SInt16 ch_13_eq_4_freq;
    SInt16 ch_14_eq_1_freq;
    SInt16 ch_14_eq_2_freq;
    SInt16 ch_14_eq_3_freq;
    SInt16 ch_14_eq_4_freq;
    SInt16 ch_15_eq_1_freq;
    SInt16 ch_15_eq_2_freq;
    SInt16 ch_15_eq_3_freq;
    SInt16 ch_15_eq_4_freq;
    SInt16 ch_16_eq_1_freq;
    SInt16 ch_16_eq_2_freq;
    SInt16 ch_16_eq_3_freq;
    SInt16 ch_16_eq_4_freq;
    SInt16 ch_17_eq_1_db;
    SInt16 ch_17_eq_2_db;
    SInt16 ch_17_eq_3_db;
    SInt16 ch_17_eq_4_db;
    SInt16 ch_17_eq_1_q;
    SInt16 ch_17_eq_2_q;
    SInt16 ch_17_eq_3_q;
    SInt16 ch_17_eq_4_q;
    SInt16 ch_17_eq_1_freq;
    SInt16 ch_17_eq_2_freq;
    SInt16 ch_17_eq_3_freq;
    SInt16 ch_17_eq_4_freq;
    SInt16 ch_18_eq_1_db;
    SInt16 ch_18_eq_2_db;
    SInt16 ch_18_eq_3_db;
    SInt16 ch_18_eq_4_db;
    SInt16 ch_18_eq_1_q;
    SInt16 ch_18_eq_2_q;
    SInt16 ch_18_eq_3_q;
    SInt16 ch_18_eq_4_q;
    SInt16 ch_18_eq_1_freq;
    SInt16 ch_18_eq_2_freq;
    SInt16 ch_18_eq_3_freq;
    SInt16 ch_18_eq_4_freq;
    SInt16 ch_1_meter_eq_in;
    SInt16 ch_2_meter_eq_in;
    SInt16 ch_3_meter_eq_in;
    SInt16 ch_4_meter_eq_in;
    SInt16 ch_5_meter_eq_in;
    SInt16 ch_6_meter_eq_in;
    SInt16 ch_7_meter_eq_in;
    SInt16 ch_8_meter_eq_in;
    SInt16 ch_9_meter_eq_in;
    SInt16 ch_10_meter_eq_in;
    SInt16 ch_11_meter_eq_in;
    SInt16 ch_12_meter_eq_in;
    SInt16 ch_13_meter_eq_in;
    SInt16 ch_14_meter_eq_in;
    SInt16 ch_15_meter_eq_in;
    SInt16 ch_16_meter_eq_in;
    SInt16 ch_1_meter_eq_out;
    SInt16 ch_2_meter_eq_out;
    SInt16 ch_3_meter_eq_out;
    SInt16 ch_4_meter_eq_out;
    SInt16 ch_5_meter_eq_out;
    SInt16 ch_6_meter_eq_out;
    SInt16 ch_7_meter_eq_out;
    SInt16 ch_8_meter_eq_out;
    SInt16 ch_9_meter_eq_out;
    SInt16 ch_10_meter_eq_out;
    SInt16 ch_11_meter_eq_out;
    SInt16 ch_12_meter_eq_out;
    SInt16 ch_13_meter_eq_out;
    SInt16 ch_14_meter_eq_out;
    SInt16 ch_15_meter_eq_out;
    SInt16 ch_16_meter_eq_out;
    SInt16 ch_17_meter_eq_in;
    SInt16 ch_18_meter_eq_in;
    SInt16 ch_17_meter_eq_out;
    SInt16 ch_18_meter_eq_out;
    SInt16 solo_ctrl;
    SInt16 solo_safe_ctrl;
    SInt16 ch_17_audio_setting;
    SInt16 ch_18_audio_setting;
    SInt16 ch_on_off;
    SInt16 ch_1_fader;
    SInt16 ch_2_fader;
    SInt16 ch_3_fader;
    SInt16 ch_4_fader;
    SInt16 ch_5_fader;
    SInt16 ch_6_fader;
    SInt16 ch_7_fader;
    SInt16 ch_8_fader;
    SInt16 ch_9_fader;
    SInt16 ch_10_fader;
    SInt16 ch_11_fader;
    SInt16 ch_12_fader;
    SInt16 ch_13_fader;
    SInt16 ch_14_fader;
    SInt16 ch_15_fader;
    SInt16 ch_16_fader;
    SInt16 ch_17_fader;
    SInt16 ch_18_fader;
    SInt16 ch_meter_pre_post;
    unsigned char crc;
} EqChannelPagePacket;

typedef struct {
    unsigned int seq_num;
    unsigned int command;
    unsigned int page_type; // 0 = channel, 1 = multi , 2 = main
    unsigned int reserve_2;
    SInt16 multi_1_ctrl;
    SInt16 multi_2_ctrl;
    SInt16 multi_3_ctrl;
    SInt16 multi_4_ctrl;
    SInt16 multi_1_eq_1_db;
    SInt16 multi_1_eq_2_db;
    SInt16 multi_1_eq_3_db;
    SInt16 multi_1_eq_4_db;
    SInt16 multi_2_eq_1_db;
    SInt16 multi_2_eq_2_db;
    SInt16 multi_2_eq_3_db;
    SInt16 multi_2_eq_4_db;
    SInt16 multi_3_eq_1_db;
    SInt16 multi_3_eq_2_db;
    SInt16 multi_3_eq_3_db;
    SInt16 multi_3_eq_4_db;
    SInt16 multi_4_eq_1_db;
    SInt16 multi_4_eq_2_db;
    SInt16 multi_4_eq_3_db;
    SInt16 multi_4_eq_4_db;
    SInt16 multi_1_eq_1_q;
    SInt16 multi_1_eq_2_q;
    SInt16 multi_1_eq_3_q;
    SInt16 multi_1_eq_4_q;
    SInt16 multi_2_eq_1_q;
    SInt16 multi_2_eq_2_q;
    SInt16 multi_2_eq_3_q;
    SInt16 multi_2_eq_4_q;
    SInt16 multi_3_eq_1_q;
    SInt16 multi_3_eq_2_q;
    SInt16 multi_3_eq_3_q;
    SInt16 multi_3_eq_4_q;
    SInt16 multi_4_eq_1_q;
    SInt16 multi_4_eq_2_q;
    SInt16 multi_4_eq_3_q;
    SInt16 multi_4_eq_4_q;
    SInt16 multi_1_eq_1_freq;
    SInt16 multi_1_eq_2_freq;
    SInt16 multi_1_eq_3_freq;
    SInt16 multi_1_eq_4_freq;
    SInt16 multi_2_eq_1_freq;
    SInt16 multi_2_eq_2_freq;
    SInt16 multi_2_eq_3_freq;
    SInt16 multi_2_eq_4_freq;
    SInt16 multi_3_eq_1_freq;
    SInt16 multi_3_eq_2_freq;
    SInt16 multi_3_eq_3_freq;
    SInt16 multi_3_eq_4_freq;
    SInt16 multi_4_eq_1_freq;
    SInt16 multi_4_eq_2_freq;
    SInt16 multi_4_eq_3_freq;
    SInt16 multi_4_eq_4_freq;
    SInt16 multi_1_meter_eq_in;
    SInt16 multi_2_meter_eq_in;
    SInt16 multi_3_meter_eq_in;
    SInt16 multi_4_meter_eq_in;
    SInt16 multi_1_meter_eq_out;
    SInt16 multi_2_meter_eq_out;
    SInt16 multi_3_meter_eq_out;
    SInt16 multi_4_meter_eq_out;
    SInt16 multi_1_fader;
    SInt16 multi_2_fader;
    SInt16 multi_3_fader;
    SInt16 multi_4_fader;
    SInt16 multi_meter_pre_post;
    unsigned char crc;
} EqMultiPagePacket;

typedef struct {
    unsigned int seq_num;
    unsigned int command;
    unsigned int page_type; // 0 = channel, 1 = multi , 2 = main
    unsigned int reserve_2;
    SInt16 main_l_r_ctrl;
    SInt16 main_l_r_eq_1_db;
    SInt16 main_l_r_eq_2_db;
    SInt16 main_l_r_eq_3_db;
    SInt16 main_l_r_eq_4_db;
    SInt16 main_l_r_eq_1_q;
    SInt16 main_l_r_eq_2_q;
    SInt16 main_l_r_eq_3_q;
    SInt16 main_l_r_eq_4_q;
    SInt16 main_l_r_eq_1_freq;
    SInt16 main_l_r_eq_2_freq;
    SInt16 main_l_r_eq_3_freq;
    SInt16 main_l_r_eq_4_freq;
    SInt16 main_l_meter_eq_in;
    SInt16 main_r_meter_eq_in;
    SInt16 main_l_meter_eq_out;
    SInt16 main_r_meter_eq_out;
    SInt16 geq_20_hz_db;
    SInt16 geq_25_hz_db;
    SInt16 geq_31_5_hz_db;
    SInt16 geq_40_hz_db;
    SInt16 geq_50_hz_db;
    SInt16 geq_63_hz_db;
    SInt16 geq_80_hz_db;
    SInt16 geq_100_hz_db;
    SInt16 geq_125_hz_db;
    SInt16 geq_160_hz_db;
    SInt16 geq_200_hz_db;
    SInt16 geq_250_hz_db;
    SInt16 geq_315_hz_db;
    SInt16 geq_400_hz_db;
    SInt16 geq_500_hz_db;
    SInt16 geq_630_hz_db;
    SInt16 geq_800_hz_db;
    SInt16 geq_1_khz_db;
    SInt16 geq_1_25_khz_db;
    SInt16 geq_1_6_khz_db;
    SInt16 geq_2_khz_db;
    SInt16 geq_2_5_khz_db;
    SInt16 geq_3_15_khz_db;
    SInt16 geq_4_khz_db;
    SInt16 geq_5_khz_db;
    SInt16 geq_6_3_khz_db;
    SInt16 geq_8_khz_db;
    SInt16 geq_10_khz_db;
    SInt16 geq_12_5_khz_db;
    SInt16 geq_16_khz_db;
    SInt16 geq_20_khz_db;
    SInt16 main_fader;
    SInt16 main_meter_pre_post;  // bit0=Meter 0:pre 1:post, bit 7=Main 0:Stereo 1:Mono
    unsigned char crc;
} EqMainPagePacket;

typedef struct {
    unsigned int seq_num;
    unsigned int command;
    unsigned int reserve_1;
    unsigned int reserve_2;
    SInt16 dsp_sets_clock_source;
    SInt16 dsp_status_3;
    SInt16 usb_sampling_rate;
    SInt16 dsp_sampling_rate;
    SInt16 sg_setting_ctrl;
    SInt16 sg_level;
    SInt16 sg_assign;
    SInt16 gp_to_main;
    SInt16 control_room_channel;
    SInt16 control_room_aux_gp;
    SInt16 ch_17_audio_setting;
    SInt16 ch_18_audio_setting;
    SInt16 main_to_control_room;
    SInt16 solo_ctrl_channel;
    SInt16 solo_safe_ctrl_channel;
    SInt16 solo_ctrl_aux_gp;
    SInt16 solo_safe_ctrl_aux_gp;
    SInt16 digital_in_out_ctrl;
    SInt16 control_room_trim_level;
    SInt16 control_room_l_meter_level;
    SInt16 control_room_r_meter_level;
    SInt16 usb_audio_l_out_assign;
    SInt16 usb_audio_r_out_assign;
    unsigned char crc;
} SetupCtrlRmUsbAssignPagePacket;

#pragma pack(pop)

// DSP IDs
#define DSP_1                               0
#define DSP_2                               1
#define DSP_3                               2
#define DSP_4                               3
#define DSP_5                               4
#define DSP_6                               5
#define DSP_7                               6
#define DSP_8                               7

// EQ, DYN, GATE, EXPAND, COMPRESSOR, LIMIT, DELAY, INV ON/OFF, ORDER (W/R)
#define CMD_CH1_CTRL                        0x3072      // DSP_1
#define CMD_CH2_CTRL                        0x3073      // DSP_1
#define CMD_CH3_CTRL                        0x3074      // DSP_1
#define CMD_CH4_CTRL                        0x3075      // DSP_1
#define CMD_CH5_CTRL                        0x3072      // DSP_2
#define CMD_CH6_CTRL                        0x3073      // DSP_2
#define CMD_CH7_CTRL                        0x3074      // DSP_2
#define CMD_CH8_CTRL                        0x3075      // DSP_2
#define CMD_CH9_CTRL                        0x3072      // DSP_3
#define CMD_CH10_CTRL                       0x3073      // DSP_3
#define CMD_CH11_CTRL                       0x3074      // DSP_3
#define CMD_CH12_CTRL                       0x3075      // DSP_3
#define CMD_CH13_CTRL                       0x3072      // DSP_4
#define CMD_CH14_CTRL                       0x3073      // DSP_4
#define CMD_CH15_CTRL                       0x3074      // DSP_4
#define CMD_CH16_CTRL                       0x3075      // DSP_4
#define CMD_CH17_CTRL                       0x3074      // DSP_6
#define CMD_CH18_CTRL                       0x3075      // DSP_6
#define CMD_MULTI_1_CTRL                    0x3072      // DSP_7
#define CMD_MULTI_2_CTRL                    0x3073
#define CMD_MULTI_3_CTRL                    0x3074
#define CMD_MULTI_4_CTRL                    0x3075
#define CMD_MAIN_LR_CTRL                    0x3072      // DSP_6

// CH1-16 GATE, EXPAND, COMPRESSOR, LIMIT (W/R)
#define CMD_CH1_GATE_RANGE                  0x3076      // DSP_1
#define CMD_CH2_GATE_RANGE                  0x3077      // DSP_2
#define CMD_CH3_GATE_RANGE                  0x3078      // DSP_3 
#define CMD_CH4_GATE_RANGE                  0x3079      // DSP_4
#define CMD_CH1_GATE_THRESHOLD              0x307A
#define CMD_CH2_GATE_THRESHOLD              0x307B
#define CMD_CH3_GATE_THRESHOLD              0x307C
#define CMD_CH4_GATE_THRESHOLD              0x307D
#define CMD_CH1_GATE_ATTACK                 0x307E
#define CMD_CH2_GATE_ATTACK                 0x307F
#define CMD_CH3_GATE_ATTACK                 0x3080
#define CMD_CH4_GATE_ATTACK                 0x3081
#define CMD_CH1_GATE_RELEASE                0x3082
#define CMD_CH2_GATE_RELEASE                0x3083
#define CMD_CH3_GATE_RELEASE                0x3084
#define CMD_CH4_GATE_RELEASE                0x3085
#define CMD_CH1_GATE_HOLD                   0x3086
#define CMD_CH2_GATE_HOLD                   0x3087
#define CMD_CH3_GATE_HOLD                   0x3088
#define CMD_CH4_GATE_HOLD                   0x3089
#define CMD_CH1_EXPAND_RATIO                0x308A
#define CMD_CH2_EXPAND_RATIO                0x308B
#define CMD_CH3_EXPAND_RATIO                0x308C
#define CMD_CH4_EXPAND_RATIO                0x308D
#define CMD_CH1_EXPAND_THRESHOLD            0x308E
#define CMD_CH2_EXPAND_THRESHOLD            0x308F
#define CMD_CH3_EXPAND_THRESHOLD            0x3090
#define CMD_CH4_EXPAND_THRESHOLD            0x3091
#define CMD_CH1_EXPAND_ATTACK               0x3092
#define CMD_CH2_EXPAND_ATTACK               0x3093
#define CMD_CH3_EXPAND_ATTACK               0x3094
#define CMD_CH4_EXPAND_ATTACK               0x3095
#define CMD_CH1_EXPAND_RELEASE              0x3096
#define CMD_CH2_EXPAND_RELEASE              0x3097
#define CMD_CH3_EXPAND_RELEASE              0x3098
#define CMD_CH4_EXPAND_RELEASE              0x3099
#define CMD_CH1_COMPRESSOR_RATIO            0x309A
#define CMD_CH2_COMPRESSOR_RATIO            0x309B
#define CMD_CH3_COMPRESSOR_RATIO            0x309C
#define CMD_CH4_COMPRESSOR_RATIO            0x309D
#define CMD_CH1_COMPRESSOR_THRESHOLD        0x309E
#define CMD_CH2_COMPRESSOR_THRESHOLD        0x309F
#define CMD_CH3_COMPRESSOR_THRESHOLD        0x30A0
#define CMD_CH4_COMPRESSOR_THRESHOLD        0x30A1
#define CMD_CH1_COMPRESSOR_ATTACK           0x30A2
#define CMD_CH2_COMPRESSOR_ATTACK           0x30A3
#define CMD_CH3_COMPRESSOR_ATTACK           0x30A4
#define CMD_CH4_COMPRESSOR_ATTACK           0x30A5
#define CMD_CH1_COMPRESSOR_RELEASE          0x30A6
#define CMD_CH2_COMPRESSOR_RELEASE          0x30A7
#define CMD_CH3_COMPRESSOR_RELEASE          0x30A8
#define CMD_CH4_COMPRESSOR_RELEASE          0x30A9
#define CMD_CH1_COMPRESSOR_OUT_GAIN         0x30AA
#define CMD_CH2_COMPRESSOR_OUT_GAIN         0x30AB
#define CMD_CH3_COMPRESSOR_OUT_GAIN         0x30AC
#define CMD_CH4_COMPRESSOR_OUT_GAIN         0x30AD
#define CMD_CH1_LIMITER_THRESHOLD           0x30AE
#define CMD_CH2_LIMITER_THRESHOLD           0x30AF
#define CMD_CH3_LIMITER_THRESHOLD           0x30B0
#define CMD_CH4_LIMITER_THRESHOLD           0x30B1
#define CMD_CH1_LIMITER_ATTACK              0x30B2
#define CMD_CH2_LIMITER_ATTACK              0x30B3
#define CMD_CH3_LIMITER_ATTACK              0x30B4
#define CMD_CH4_LIMITER_ATTACK              0x30B5
#define CMD_CH1_LIMITER_RELEASE             0x30B6
#define CMD_CH2_LIMITER_RELEASE             0x30B7
#define CMD_CH3_LIMITER_RELEASE             0x30B8
#define CMD_CH4_LIMITER_RELEASE             0x30B9

// CH17-18, MAIN GATE, EXPAND, COMPRESSOR, LIMIT (W/R)
#define CMD_MAIN_LR_GATE_RANGE              0x3076      // DSP_6
#define CMD_CH17_GATE_RANGE                 0x3078      // DSP_6
#define CMD_CH18_GATE_RANGE                 0x3079
#define CMD_MAIN_LR_GATE_THRESHOLD          0x307A
#define CMD_CH17_GATE_THRESHOLD             0x307C
#define CMD_CH18_GATE_THRESHOLD             0x307D
#define CMD_MAIN_LR_GATE_ATTACK             0x307E
#define CMD_CH17_GATE_ATTACK                0x3080
#define CMD_CH18_GATE_ATTACK                0x3081
#define CMD_MAIN_LR_GATE_RELEASE            0x3082
#define CMD_CH17_GATE_RELEASE               0x3084
#define CMD_CH18_GATE_RELEASE               0x3085
#define CMD_MAIN_LR_GATE_HOLD               0x3086
#define CMD_CH17_GATE_HOLD                  0x3088
#define CMD_CH18_GATE_HOLD                  0x3089
#define CMD_MAIN_LR_EXPAND_RATIO            0x308A
#define CMD_CH17_EXPAND_RATIO               0x308C
#define CMD_CH18_EXPAND_RATIO               0x308D
#define CMD_MAIN_LR_EXPAND_THRESHOLD        0x308E
#define CMD_CH17_EXPAND_THRESHOLD           0x3090
#define CMD_CH18_EXPAND_THRESHOLD           0x3091
#define CMD_MAIN_LR_EXPAND_ATTACK           0x3092
#define CMD_CH17_EXPAND_ATTACK              0x3094
#define CMD_CH18_EXPAND_ATTACK              0x3095
#define CMD_MAIN_LR_EXPAND_RELEASE          0x3096
#define CMD_CH17_EXPAND_RELEASE             0x3098
#define CMD_CH18_EXPAND_RELEASE             0x3099
#define CMD_MAIN_LR_COMPRESSOR_RATIO        0x309A
#define CMD_CH17_COMPRESSOR_RATIO           0x309C
#define CMD_CH18_COMPRESSOR_RATIO           0x309D
#define CMD_MAIN_LR_COMPRESSOR_THRESHOLD    0x309E
#define CMD_CH17_COMPRESSOR_THRESHOLD       0x30A0
#define CMD_CH18_COMPRESSOR_THRESHOLD       0x30A1
#define CMD_MAIN_LR_COMPRESSOR_ATTACK       0x30A2
#define CMD_CH17_COMPRESSOR_ATTACK          0x30A4
#define CMD_CH18_COMPRESSOR_ATTACK          0x30A5
#define CMD_MAIN_LR_COMPRESSOR_RELEASE      0x30A6
#define CMD_CH17_COMPRESSOR_RELEASE         0x30A8
#define CMD_CH18_COMPRESSOR_RELEASE         0x30A9
#define CMD_MAIN_LR_COMPRESSOR_OUT_GAIN     0x30AA
#define CMD_CH17_COMPRESSOR_OUT_GAIN        0x30AC
#define CMD_CH18_COMPRESSOR_OUT_GAIN        0x30AD
#define CMD_MAIN_LR_LIMITER_THRESHOLD       0x30AE
#define CMD_CH17_LIMITER_THRESHOLD          0x30B0
#define CMD_CH18_LIMITER_THRESHOLD          0x30B1
#define CMD_MAIN_LR_LIMITER_ATTACK          0x30B2
#define CMD_CH17_LIMITER_ATTACK             0x30B4
#define CMD_CH18_LIMITER_ATTACK             0x30B5
#define CMD_MAIN_LR_LIMITER_RELEASE         0x30B6
#define CMD_CH17_LIMITER_RELEASE            0x30B8
#define CMD_CH18_LIMITER_RELEASE            0x30B9

// MULTI GATE, EXPAND, COMPRESSOR, LIMIT (W/R)
#define CMD_MULTI1_GATE_RANGE               0x3076      // DSP_7
#define CMD_MULTI2_GATE_RANGE               0x3077
#define CMD_MULTI3_GATE_RANGE               0x3078
#define CMD_MULTI4_GATE_RANGE               0x3079
#define CMD_MULTI1_GATE_THRESHOLD           0x307A
#define CMD_MULTI2_GATE_THRESHOLD           0x307B
#define CMD_MULTI3_GATE_THRESHOLD           0x307C
#define CMD_MULTI4_GATE_THRESHOLD           0x307D
#define CMD_MULTI1_GATE_ATTACK              0x307E
#define CMD_MULTI2_GATE_ATTACK              0x307F
#define CMD_MULTI3_GATE_ATTACK              0x3080
#define CMD_MULTI4_GATE_ATTACK              0x3081
#define CMD_MULTI1_GATE_RELEASE             0x3082
#define CMD_MULTI2_GATE_RELEASE             0x3083
#define CMD_MULTI3_GATE_RELEASE             0x3084
#define CMD_MULTI4_GATE_RELEASE             0x3085
#define CMD_MULTI1_GATE_HOLD                0x3086
#define CMD_MULTI2_GATE_HOLD                0x3087
#define CMD_MULTI3_GATE_HOLD                0x3088
#define CMD_MULTI4_GATE_HOLD                0x3089
#define CMD_MULTI1_EXPAND_RATIO             0x308A
#define CMD_MULTI2_EXPAND_RATIO             0x308B
#define CMD_MULTI3_EXPAND_RATIO             0x308C
#define CMD_MULTI4_EXPAND_RATIO             0x308D
#define CMD_MULTI1_EXPAND_THRESHOLD         0x308E
#define CMD_MULTI2_EXPAND_THRESHOLD         0x308F
#define CMD_MULTI3_EXPAND_THRESHOLD         0x3090
#define CMD_MULTI4_EXPAND_THRESHOLD         0x3091
#define CMD_MULTI1_EXPAND_ATTACK            0x3092
#define CMD_MULTI2_EXPAND_ATTACK            0x3093
#define CMD_MULTI3_EXPAND_ATTACK            0x3094
#define CMD_MULTI4_EXPAND_ATTACK            0x3095
#define CMD_MULTI1_EXPAND_RELEASE           0x3096
#define CMD_MULTI2_EXPAND_RELEASE           0x3097
#define CMD_MULTI3_EXPAND_RELEASE           0x3098
#define CMD_MULTI4_EXPAND_RELEASE           0x3099
#define CMD_MULTI1_COMPRESSOR_RATIO         0x309A
#define CMD_MULTI2_COMPRESSOR_RATIO         0x309B
#define CMD_MULTI3_COMPRESSOR_RATIO         0x309C
#define CMD_MULTI4_COMPRESSOR_RATIO         0x309D
#define CMD_MULTI1_COMPRESSOR_THRESHOLD     0x309E
#define CMD_MULTI2_COMPRESSOR_THRESHOLD     0x309F
#define CMD_MULTI3_COMPRESSOR_THRESHOLD     0x30A0
#define CMD_MULTI4_COMPRESSOR_THRESHOLD     0x30A1
#define CMD_MULTI1_COMPRESSOR_ATTACK        0x30A2
#define CMD_MULTI2_COMPRESSOR_ATTACK        0x30A3
#define CMD_MULTI3_COMPRESSOR_ATTACK        0x30A4
#define CMD_MULTI4_COMPRESSOR_ATTACK        0x30A5
#define CMD_MULTI1_COMPRESSOR_RELEASE       0x30A6
#define CMD_MULTI2_COMPRESSOR_RELEASE       0x30A7
#define CMD_MULTI3_COMPRESSOR_RELEASE       0x30A8
#define CMD_MULTI4_COMPRESSOR_RELEASE       0x30A9
#define CMD_MULTI1_COMPRESSOR_OUT_GAIN      0x30AA
#define CMD_MULTI2_COMPRESSOR_OUT_GAIN      0x30AB
#define CMD_MULTI3_COMPRESSOR_OUT_GAIN      0x30AC
#define CMD_MULTI4_COMPRESSOR_OUT_GAIN      0x30AD
#define CMD_MULTI1_LIMITER_THRESHOLD        0x30AE
#define CMD_MULTI2_LIMITER_THRESHOLD        0x30AF
#define CMD_MULTI3_LIMITER_THRESHOLD        0x30B0
#define CMD_MULTI4_LIMITER_THRESHOLD        0x30B1
#define CMD_MULTI1_LIMITER_ATTACK           0x30B2
#define CMD_MULTI2_LIMITER_ATTACK           0x30B3
#define CMD_MULTI3_LIMITER_ATTACK           0x30B4
#define CMD_MULTI4_LIMITER_ATTACK           0x30B5
#define CMD_MULTI1_LIMITER_RELEASE          0x30B6
#define CMD_MULTI2_LIMITER_RELEASE          0x30B7
#define CMD_MULTI3_LIMITER_RELEASE          0x30B8
#define CMD_MULTI4_LIMITER_RELEASE          0x30B9 

// CH1-16 GATE METER OUT, EXPAND METER OUT, COMPRESSOR METER OUT, LIMIT METER OUT (R)
#define CMD_CH1_METER_NOISE_OUT                 0x30F6
#define CMD_CH2_METER_NOISE_OUT                 0x30F7
#define CMD_CH3_METER_NOISE_OUT                 0x30F8
#define CMD_CH4_METER_NOISE_OUT                 0x30F9
#define CMD_CH1_METER_NOISE_GAIN_REDUCE         0x30FA
#define CMD_CH2_METER_NOISE_GAIN_REDUCE         0x30FB
#define CMD_CH3_METER_NOISE_GAIN_REDUCE         0x30FC
#define CMD_CH4_METER_NOISE_GAIN_REDUCE         0x30FD
#define CMD_CH1_METER_EXPAND_OUT                0x30FE
#define CMD_CH2_METER_EXPAND_OUT                0x30FF
#define CMD_CH3_METER_EXPAND_OUT                0x3100
#define CMD_CH4_METER_EXPAND_OUT                0x3101
#define CMD_CH1_METER_EXPAND_GAIN_REDUCE        0x3102
#define CMD_CH2_METER_EXPAND_GAIN_REDUCE        0x3103
#define CMD_CH3_METER_EXPAND_GAIN_REDUCE        0x3104
#define CMD_CH4_METER_EXPAND_GAIN_REDUCE        0x3105
#define CMD_CH1_METER_COMPRESSOR_OUT            0x3016
#define CMD_CH2_METER_COMPRESSOR_OUT            0x3017
#define CMD_CH3_METER_COMPRESSOR_OUT            0x3018
#define CMD_CH4_METER_COMPRESSOR_OUT            0x3019
#define CMD_CH1_METER_COMPRESSOR_GAIN_REDUCE    0x310A
#define CMD_CH2_METER_COMPRESSOR_GAIN_REDUCE    0x310B
#define CMD_CH3_METER_COMPRESSOR_GAIN_REDUCE    0x310C
#define CMD_CH4_METER_COMPRESSOR_GAIN_REDUCE    0x310D
#define CMD_CH1_METER_LIMIT_OUT                 0x310E
#define CMD_CH2_METER_LIMIT_OUT                 0x310F
#define CMD_CH3_METER_LIMIT_OUT                 0x3110
#define CMD_CH4_METER_LIMIT_OUT                 0x3111
#define CMD_CH1_METER_LIMIT_GAIN_REDUCE         0x3112
#define CMD_CH2_METER_LIMIT_GAIN_REDUCE         0x3113
#define CMD_CH3_METER_LIMIT_GAIN_REDUCE         0x3114
#define CMD_CH4_METER_LIMIT_GAIN_REDUCE         0x3115

// CH17-18, MAIN GATE METER OUT, EXPAND METER OUT, COMPRESSOR METER OUT, LIMIT METER OUT (R)
#define CMD_MAIN_L_METER_NOISE_OUT              0x30F6
#define CMD_MAIN_R_METER_NOISE_OUT              0x30F7
#define CMD_CH17_METER_NOISE_OUT                0x30F8
#define CMD_CH18_METER_NOISE_OUT                0x30F9
#define CMD_MAIN_L_METER_NOISE_GAIN_REDUCE      0x30FA
#define CMD_MAIN_R_METER_NOISE_GAIN_REDUCE      0x30FB
#define CMD_CH17_METER_NOISE_GAIN_REDUCE        0x30FC
#define CMD_CH18_METER_NOISE_GAIN_REDUCE        0x30FD
#define CMD_MAIN_L_METER_EXPAND_OUT             0x30FE
#define CMD_MAIN_R_METER_EXPAND_OUT             0x30FF
#define CMD_CH17_METER_EXPAND_OUT               0x3100
#define CMD_CH18_METER_EXPAND_OUT               0x3101
#define CMD_MAIN_L_METER_EXPAND_GAIN_REDUCE     0x3102
#define CMD_MAIN_R_METER_EXPAND_GAIN_REDUCE     0x3103
#define CMD_CH17_METER_EXPAND_GAIN_REDUCE       0x3104
#define CMD_CH18_METER_EXPAND_GAIN_REDUCE       0x3105
#define CMD_MAIN_L_METER_COMPRESSOR_OUT         0x3016
#define CMD_MAIN_R_METER_COMPRESSOR_OUT         0x3017
#define CMD_CH17_METER_COMPRESSOR_OUT           0x3018
#define CMD_CH18_METER_COMPRESSOR_OUT           0x3019
#define CMD_MAIN_L_METER_COMPRESSOR_GAIN_REDUCE 0x310A
#define CMD_MAIN_R_METER_COMPRESSOR_GAIN_REDUCE 0x310B
#define CMD_CH17_METER_COMPRESSOR_GAIN_REDUCE   0x310C
#define CMD_CH18_METER_COMPRESSOR_GAIN_REDUCE   0x310D
#define CMD_MAIN_L_METER_LIMIT_OUT              0x310E
#define CMD_MAIN_R_METER_LIMIT_OUT              0x310F
#define CMD_CH17_METER_LIMIT_OUT                0x3110
#define CMD_CH18_METER_LIMIT_OUT                0x3111
#define CMD_MAIN_L_METER_LIMIT_GAIN_REDUCE      0x3112
#define CMD_MAIN_R_METER_LIMIT_GAIN_REDUCE      0x3113
#define CMD_CH17_METER_LIMIT_GAIN_REDUCE        0x3114
#define CMD_CH18_METER_LIMIT_GAIN_REDUCE        0x3115

// MULTI GATE METER OUT, EXPAND METER OUT, COMPRESSOR METER OUT, LIMIT METER OUT (R)
#define CMD_MULTI1_METER_NOISE_OUT              0x30F6
#define CMD_MULTI2_METER_NOISE_OUT              0x30F7
#define CMD_MULTI3_METER_NOISE_OUT              0x30F8
#define CMD_MULTI4_METER_NOISE_OUT              0x30F9
#define CMD_MULTI1_METER_NOISE_GAIN_REDUCE      0x30FA
#define CMD_MULTI2_METER_NOISE_GAIN_REDUCE      0x30FB
#define CMD_MULTI3_METER_NOISE_GAIN_REDUCE      0x30FC
#define CMD_MULTI4_METER_NOISE_GAIN_REDUCE      0x30FD
#define CMD_MULTI1_METER_EXPAND_OUT             0x30FE
#define CMD_MULTI2_METER_EXPAND_OUT             0x30FF
#define CMD_MULTI3_METER_EXPAND_OUT             0x3100
#define CMD_MULTI4_METER_EXPAND_OUT             0x3101
#define CMD_MULTI1_METER_EXPAND_GAIN_REDUCE     0x3102
#define CMD_MULTI2_METER_EXPAND_GAIN_REDUCE     0x3103
#define CMD_MULTI3_METER_EXPAND_GAIN_REDUCE     0x3104
#define CMD_MULTI4_METER_EXPAND_GAIN_REDUCE     0x3105
#define CMD_MULTI1_METER_COMPRESSOR_OUT         0x3016
#define CMD_MULTI2_METER_COMPRESSOR_OUT         0x3017
#define CMD_MULTI3_METER_COMPRESSOR_OUT         0x3018
#define CMD_MULTI4_METER_COMPRESSOR_OUT         0x3019
#define CMD_MULTI1_METER_COMPRESSOR_GAIN_REDUCE 0x310A
#define CMD_MULTI2_METER_COMPRESSOR_GAIN_REDUCE 0x310B
#define CMD_MULTI3_METER_COMPRESSOR_GAIN_REDUCE 0x310C
#define CMD_MULTI4_METER_COMPRESSOR_GAIN_REDUCE 0x310D
#define CMD_MULTI1_METER_LIMIT_OUT              0x310E
#define CMD_MULTI2_METER_LIMIT_OUT              0x310F
#define CMD_MULTI3_METER_LIMIT_OUT              0x3110
#define CMD_MULTI4_METER_LIMIT_OUT              0x3111
#define CMD_MULTI1_METER_LIMIT_GAIN_REDUCE      0x3112
#define CMD_MULTI2_METER_LIMIT_GAIN_REDUCE      0x3113
#define CMD_MULTI3_METER_LIMIT_GAIN_REDUCE      0x3114
#define CMD_MULTI4_METER_LIMIT_GAIN_REDUCE      0x3115

// CH1-16 48V ON/OFF
#define CMD_CH1_16_48V_ONOFF                0x315B      // DSP_6

// DELAY TIME (W/R)
#define CMD_CH1_DELAY_TIME                  0x30EA      // DSP_1
#define CMD_CH2_DELAY_TIME                  0x30EB      // DSP_1
#define CMD_CH3_DELAY_TIME                  0x30EC      // DSP_1
#define CMD_CH4_DELAY_TIME                  0x30ED      // DSP_1
#define CMD_CH5_DELAY_TIME                  0x30EA      // DSP_2
#define CMD_CH6_DELAY_TIME                  0x30EB      // DSP_2
#define CMD_CH7_DELAY_TIME                  0x30EC      // DSP_2
#define CMD_CH8_DELAY_TIME                  0x30ED      // DSP_2
#define CMD_CH9_DELAY_TIME                  0x30EA      // DSP_3
#define CMD_CH10_DELAY_TIME                 0x30EB      // DSP_3
#define CMD_CH11_DELAY_TIME                 0x30EC      // DSP_3
#define CMD_CH12_DELAY_TIME                 0x30ED      // DSP_3
#define CMD_CH13_DELAY_TIME                 0x30EA      // DSP_4
#define CMD_CH14_DELAY_TIME                 0x30EB      // DSP_4
#define CMD_CH15_DELAY_TIME                 0x30EC      // DSP_4
#define CMD_CH16_DELAY_TIME                 0x30ED      // DSP_4
#define CMD_CH17_DELAY_TIME                 0x30EC      // DSP_6
#define CMD_CH18_DELAY_TIME                 0x30ED      // DSP_6
#define CMD_MULTI1_DELAY_TIME               0x30EA      // DSP_7
#define CMD_MULTI2_DELAY_TIME               0x30EB
#define CMD_MULTI3_DELAY_TIME               0x30EC
#define CMD_MULTI4_DELAY_TIME               0x30ED
#define CMD_MAIN_LR_DELAY_TIME              0x30EA      // DSP_6

// EQ METER OUT (R)
#define CMD_CH1_METER_EQ_OUT                0x311A
#define CMD_CH2_METER_EQ_OUT                0x311B
#define CMD_CH3_METER_EQ_OUT                0x311C
#define CMD_CH4_METER_EQ_OUT                0x311D
#define CMD_CH17_METER_EQ_OUT               0x311C
#define CMD_CH18_METER_EQ_OUT               0x311D
#define CMD_MULTI1_METER_EQ_OUT             0x311A
#define CMD_MULTI2_METER_EQ_OUT             0x311B
#define CMD_MULTI3_METER_EQ_OUT             0x311C
#define CMD_MULTI4_METER_EQ_OUT             0x311D
#define CMD_MAIN_L_METER_EQ_OUT             0x311A
#define CMD_MAIN_R_METER_EQ_OUT             0x311B

// DYN METER OUT (R)
#define CMD_CH1_METER_DYN_OUT               0x311E
#define CMD_CH2_METER_DYN_OUT               0x311F
#define CMD_CH3_METER_DYN_OUT               0x3120
#define CMD_CH4_METER_DYN_OUT               0x3121
#define CMD_CH17_METER_DYN_OUT              0x315C
#define CMD_CH18_METER_DYN_OUT              0x315D
#define CMD_MULTI1_METER_DYN_OUT            0x312B
#define CMD_MULTI2_METER_DYN_OUT            0x312C
#define CMD_MULTI3_METER_DYN_OUT            0x312D
#define CMD_MULTI4_METER_DYN_OUT            0x312E
#define CMD_MAIN_L_METER_DYN_OUT            0x3134
#define CMD_MAIN_R_METER_DYN_OUT            0x3135

// DYN ACTIVE (R)
#define CMD_CH_1234_DYN_STATUS              0x3122
#define CMD_CH17_18_DYN_STATUS              0x313A      // DSP_5
#define CMD_MULTI1234_DYN_STATUS            0x3133
#define CMD_MAIN_LR_DYN_STATUS              0x313A      // DSP_6

// PAN LEVEL (W/R)  
#define CMD_CH1_PAN_LEVEL                   0x309F      // DSP_5
#define CMD_CH2_PAN_LEVEL                   0x30A0
#define CMD_CH3_PAN_LEVEL                   0x30A1
#define CMD_CH4_PAN_LEVEL                   0x30A2
#define CMD_CH5_PAN_LEVEL                   0x30A3
#define CMD_CH6_PAN_LEVEL                   0x30A4
#define CMD_CH7_PAN_LEVEL                   0x30A5
#define CMD_CH8_PAN_LEVEL                   0x30A6
#define CMD_CH9_PAN_LEVEL                   0x30A7
#define CMD_CH10_PAN_LEVEL                  0x30A8
#define CMD_CH11_PAN_LEVEL                  0x30A9
#define CMD_CH12_PAN_LEVEL                  0x30AA
#define CMD_CH13_PAN_LEVEL                  0x30AB
#define CMD_CH14_PAN_LEVEL                  0x30AC
#define CMD_CH15_PAN_LEVEL                  0x30AD
#define CMD_CH16_PAN_LEVEL                  0x30AE
#define CMD_CH17_PAN_LEVEL                  0x3312
#define CMD_CH18_PAN_LEVEL                  0x3313
#define CMD_GP1_PAN_LEVEL                   0x30AF
#define CMD_GP2_PAN_LEVEL                   0x30B0
#define CMD_GP3_PAN_LEVEL                   0x30B1
#define CMD_GP4_PAN_LEVEL                   0x30B2

// AUX SEND PRE/POST (W/R)
#define CMD_AUX1_SEND                       0x30D3      // DSP_5
#define CMD_AUX2_SEND                       0x30D4
#define CMD_AUX3_SEND                       0x30D5
#define CMD_AUX4_SEND                       0x30D6
#define CMD_USB_SEND                        0x3310
#define CMD_VIEW_METER_CONTROL              0x318B

// CH1-16 ~ AUX1-4 FADER LEVEL (W/R)
#define CMD_AUX1_CH1_LEVEL                  0x30DB
#define CMD_AUX1_CH2_LEVEL                  0x30DC
#define CMD_AUX1_CH3_LEVEL                  0x30DD
#define CMD_AUX1_CH4_LEVEL                  0x30DE
#define CMD_AUX1_CH5_LEVEL                  0x30DF
#define CMD_AUX1_CH6_LEVEL                  0x30E0
#define CMD_AUX1_CH7_LEVEL                  0x30E1
#define CMD_AUX1_CH8_LEVEL                  0x30E2
#define CMD_AUX1_CH9_LEVEL                  0x30E3
#define CMD_AUX1_CH10_LEVEL                 0x30E4
#define CMD_AUX1_CH11_LEVEL                 0x30E5
#define CMD_AUX1_CH12_LEVEL                 0x30E6
#define CMD_AUX1_CH13_LEVEL                 0x30E7
#define CMD_AUX1_CH14_LEVEL                 0x30E8
#define CMD_AUX1_CH15_LEVEL                 0x30E9
#define CMD_AUX1_CH16_LEVEL                 0x30EA
#define CMD_AUX1_CH17_USB_LEVEL             0x3316
#define CMD_AUX1_CH18_USB_LEVEL             0x3317
#define CMD_AUX2_CH1_LEVEL                  0x30EB
#define CMD_AUX2_CH2_LEVEL                  0x30EC
#define CMD_AUX2_CH3_LEVEL                  0x30ED
#define CMD_AUX2_CH4_LEVEL                  0x30EE
#define CMD_AUX2_CH5_LEVEL                  0x30EF
#define CMD_AUX2_CH6_LEVEL                  0x30F0
#define CMD_AUX2_CH7_LEVEL                  0x30F1
#define CMD_AUX2_CH8_LEVEL                  0x30F2
#define CMD_AUX2_CH9_LEVEL                  0x30F3
#define CMD_AUX2_CH10_LEVEL                 0x30F4
#define CMD_AUX2_CH11_LEVEL                 0x30F5
#define CMD_AUX2_CH12_LEVEL                 0x30F6
#define CMD_AUX2_CH13_LEVEL                 0x30F7
#define CMD_AUX2_CH14_LEVEL                 0x30F8
#define CMD_AUX2_CH15_LEVEL                 0x30F9
#define CMD_AUX2_CH16_LEVEL                 0x30FA
#define CMD_AUX2_CH17_USB_LEVEL             0x3318
#define CMD_AUX2_CH18_USB_LEVEL             0x3319
#define CMD_AUX3_CH1_LEVEL                  0x30FB
#define CMD_AUX3_CH2_LEVEL                  0x30FC
#define CMD_AUX3_CH3_LEVEL                  0x30FD
#define CMD_AUX3_CH4_LEVEL                  0x30FE
#define CMD_AUX3_CH5_LEVEL                  0x30FF
#define CMD_AUX3_CH6_LEVEL                  0x3100
#define CMD_AUX3_CH7_LEVEL                  0x3101
#define CMD_AUX3_CH8_LEVEL                  0x3102
#define CMD_AUX3_CH9_LEVEL                  0x3103
#define CMD_AUX3_CH10_LEVEL                 0x3104
#define CMD_AUX3_CH11_LEVEL                 0x3105
#define CMD_AUX3_CH12_LEVEL                 0x3106
#define CMD_AUX3_CH13_LEVEL                 0x3107
#define CMD_AUX3_CH14_LEVEL                 0x3108
#define CMD_AUX3_CH15_LEVEL                 0x3109
#define CMD_AUX3_CH16_LEVEL                 0x310A
#define CMD_AUX3_CH17_USB_LEVEL             0x331A
#define CMD_AUX3_CH18_USB_LEVEL             0x331B
#define CMD_AUX4_CH1_LEVEL                  0x310B
#define CMD_AUX4_CH2_LEVEL                  0x310C
#define CMD_AUX4_CH3_LEVEL                  0x310D
#define CMD_AUX4_CH4_LEVEL                  0x310E
#define CMD_AUX4_CH5_LEVEL                  0x310F
#define CMD_AUX4_CH6_LEVEL                  0x3110
#define CMD_AUX4_CH7_LEVEL                  0x3111
#define CMD_AUX4_CH8_LEVEL                  0x3112
#define CMD_AUX4_CH9_LEVEL                  0x3113
#define CMD_AUX4_CH10_LEVEL                 0x3114
#define CMD_AUX4_CH11_LEVEL                 0x3115
#define CMD_AUX4_CH12_LEVEL                 0x3116
#define CMD_AUX4_CH13_LEVEL                 0x3117
#define CMD_AUX4_CH14_LEVEL                 0x3118
#define CMD_AUX4_CH15_LEVEL                 0x3119
#define CMD_AUX4_CH16_LEVEL                 0x311A
#define CMD_AUX4_CH17_USB_LEVEL             0x331C
#define CMD_AUX4_CH18_USB_LEVEL             0x331D

// CH1-16 ASSIGN GP1-4 (W/R)
#define CMD_GP1_ASSIGN                      0x3076
#define CMD_GP2_ASSIGN                      0x3077
#define CMD_GP3_ASSIGN                      0x3078
#define CMD_GP4_ASSIGN                      0x3079
#define CMD_USB_ASSIGN                      0x3310

// FADER LEVEL (W/R)
#define CMD_CH1_FADER_LEVEL                 0x307F      // DSP_5
#define CMD_CH2_FADER_LEVEL                 0x3080
#define CMD_CH3_FADER_LEVEL                 0x3081
#define CMD_CH4_FADER_LEVEL                 0x3082
#define CMD_CH5_FADER_LEVEL                 0x3083
#define CMD_CH6_FADER_LEVEL                 0x3084
#define CMD_CH7_FADER_LEVEL                 0x3085
#define CMD_CH8_FADER_LEVEL                 0x3086
#define CMD_CH9_FADER_LEVEL                 0x3087
#define CMD_CH10_FADER_LEVEL                0x3088
#define CMD_CH11_FADER_LEVEL                0x3089
#define CMD_CH12_FADER_LEVEL                0x308A
#define CMD_CH13_FADER_LEVEL                0x308B
#define CMD_CH14_FADER_LEVEL                0x308C
#define CMD_CH15_FADER_LEVEL                0x308D
#define CMD_CH16_FADER_LEVEL                0x308E
#define CMD_CH17_USB_AUDIO_FADER            0x3314
#define CMD_CH18_USB_AUDIO_FADER            0x3315
#define CMD_AUX1_FADER_LEVEL                0x3097
#define CMD_AUX2_FADER_LEVEL                0x3098
#define CMD_AUX3_FADER_LEVEL                0x3099
#define CMD_AUX4_FADER_LEVEL                0x309A
#define CMD_GP1_FADER_LEVEL                 0x308F
#define CMD_GP2_FADER_LEVEL                 0x3090
#define CMD_GP3_FADER_LEVEL                 0x3091
#define CMD_GP4_FADER_LEVEL                 0x3092
#define CMD_EFX1_FADER_LEVEL                0x30B3      // DSP_8
#define CMD_EFX2_FADER_LEVEL                0x32F1      // DSP_5
#define CMD_MULTI1_FADER_LEVEL              0x311E      // DSP_7
#define CMD_MULTI2_FADER_LEVEL              0x311F
#define CMD_MULTI3_FADER_LEVEL              0x3120
#define CMD_MULTI4_FADER_LEVEL              0x3121
#define CMD_CTRL_RM_TRIM_LEVEL              0x3136      // DSP_6
#define CMD_MAIN_FADER_LEVEL                0x311E
#define CMD_AUX1_CH1_FADER_LEVEL            0x30DB      // DSP_5
#define CMD_AUX1_CH2_FADER_LEVEL            0x30DC
#define CMD_AUX1_CH3_FADER_LEVEL            0x30DD
#define CMD_AUX1_CH4_FADER_LEVEL            0x30DE
#define CMD_AUX1_CH5_FADER_LEVEL            0x30DF
#define CMD_AUX1_CH6_FADER_LEVEL            0x30E0
#define CMD_AUX1_CH7_FADER_LEVEL            0x30E1
#define CMD_AUX1_CH8_FADER_LEVEL            0x30E2
#define CMD_AUX1_CH9_FADER_LEVEL            0x30E3
#define CMD_AUX1_CH10_FADER_LEVEL           0x30E4
#define CMD_AUX1_CH11_FADER_LEVEL           0x30E5
#define CMD_AUX1_CH12_FADER_LEVEL           0x30E6
#define CMD_AUX1_CH13_FADER_LEVEL           0x30E7
#define CMD_AUX1_CH14_FADER_LEVEL           0x30E8
#define CMD_AUX1_CH15_FADER_LEVEL           0x30E9
#define CMD_AUX1_CH16_FADER_LEVEL           0x30EA
#define CMD_AUX2_CH1_FADER_LEVEL            0x30EB
#define CMD_AUX2_CH2_FADER_LEVEL            0x30EC
#define CMD_AUX2_CH3_FADER_LEVEL            0x30ED
#define CMD_AUX2_CH4_FADER_LEVEL            0x30EE
#define CMD_AUX2_CH5_FADER_LEVEL            0x30EF
#define CMD_AUX2_CH6_FADER_LEVEL            0x30F0
#define CMD_AUX2_CH7_FADER_LEVEL            0x30F1
#define CMD_AUX2_CH8_FADER_LEVEL            0x30F2
#define CMD_AUX2_CH9_FADER_LEVEL            0x30F3
#define CMD_AUX2_CH10_FADER_LEVEL           0x30F4
#define CMD_AUX2_CH11_FADER_LEVEL           0x30F5
#define CMD_AUX2_CH12_FADER_LEVEL           0x30F6
#define CMD_AUX2_CH13_FADER_LEVEL           0x30F7
#define CMD_AUX2_CH14_FADER_LEVEL           0x30F8
#define CMD_AUX2_CH15_FADER_LEVEL           0x30F9
#define CMD_AUX2_CH16_FADER_LEVEL           0x30FA
#define CMD_AUX3_CH1_FADER_LEVEL            0x30FB
#define CMD_AUX3_CH2_FADER_LEVEL            0x30FC
#define CMD_AUX3_CH3_FADER_LEVEL            0x30FD
#define CMD_AUX3_CH4_FADER_LEVEL            0x30FE
#define CMD_AUX3_CH5_FADER_LEVEL            0x30FF
#define CMD_AUX3_CH6_FADER_LEVEL            0x3100
#define CMD_AUX3_CH7_FADER_LEVEL            0x3101
#define CMD_AUX3_CH8_FADER_LEVEL            0x3102
#define CMD_AUX3_CH9_FADER_LEVEL            0x3103
#define CMD_AUX3_CH10_FADER_LEVEL           0x3104
#define CMD_AUX3_CH11_FADER_LEVEL           0x3105
#define CMD_AUX3_CH12_FADER_LEVEL           0x3106
#define CMD_AUX3_CH13_FADER_LEVEL           0x3107
#define CMD_AUX3_CH14_FADER_LEVEL           0x3108
#define CMD_AUX3_CH15_FADER_LEVEL           0x3109
#define CMD_AUX3_CH16_FADER_LEVEL           0x310A
#define CMD_AUX4_CH1_FADER_LEVEL            0x310B
#define CMD_AUX4_CH2_FADER_LEVEL            0x310C
#define CMD_AUX4_CH3_FADER_LEVEL            0x310D
#define CMD_AUX4_CH4_FADER_LEVEL            0x310E
#define CMD_AUX4_CH5_FADER_LEVEL            0x310F
#define CMD_AUX4_CH6_FADER_LEVEL            0x3110
#define CMD_AUX4_CH7_FADER_LEVEL            0x3111
#define CMD_AUX4_CH8_FADER_LEVEL            0x3112
#define CMD_AUX4_CH9_FADER_LEVEL            0x3113
#define CMD_AUX4_CH10_FADER_LEVEL           0x3114
#define CMD_AUX4_CH11_FADER_LEVEL           0x3115
#define CMD_AUX4_CH12_FADER_LEVEL           0x3116
#define CMD_AUX4_CH13_FADER_LEVEL           0x3117
#define CMD_AUX4_CH14_FADER_LEVEL           0x3118
#define CMD_AUX4_CH15_FADER_LEVEL           0x3119
#define CMD_AUX4_CH16_FADER_LEVEL           0x311A
#define CMD_AUX1_CH17_USB_FADER_LEVEL       0x3316
#define CMD_AUX1_CH18_USB_FADER_LEVEL       0x3317
#define CMD_AUX2_CH17_USB_FADER_LEVEL       0x3318
#define CMD_AUX2_CH18_USB_FADER_LEVEL       0x3319
#define CMD_AUX3_CH17_USB_FADER_LEVEL       0x331A
#define CMD_AUX3_CH18_USB_FADER_LEVEL       0x331B
#define CMD_AUX4_CH17_USB_FADER_LEVEL       0x331C
#define CMD_AUX4_CH18_USB_FADER_LEVEL       0x331D

// ON/OFF (W/R)
#define CMD_CH_ON_OFF                       0x315B      // DSP_5
#define CMD_CH17_USB_AUDIO_SETTING          0x3310
#define CMD_CH18_USB_AUDIO_SETTING          0x3311
#define CMD_AUXGP_ON_OFF                    0x307E      
#define CMD_MULTI1_ON_OFF                   0x3072      // DSP_7
#define CMD_MULTI2_ON_OFF                   0x3073
#define CMD_MULTI3_ON_OFF                   0x3074
#define CMD_MULTI4_ON_OFF                   0x3075
#define CMD_MAIN_ON_OFF                     0x3072      // DSP_6
#define CMD_CTRL_RM_ON_OFF_CH               0x315E      // DSP_5 
#define CMD_CTRL_RM_ON_OFF_AUXGP            0x315F      // DSP_5
#define CMD_MAIN_TO_CTRL_RM                 0x3137      // DSP_6

// SOLO ON/OFF (W/R)
#define CMD_SOLO_CTRL                       0x3072      // DSP_5
#define CMD_SOLO_SAFE_CTRL                  0x3073      
#define CMD_USB_SOLO_CTRL                   0x3310      
#define CMD_AUXGP_SOLO_CTRL                 0x30C8      
#define CMD_AUXGP_SOLO_SAFE_CTRL            0x30C9      
#define CMD_DIGITAL_INOUT_CTRL              0x312F      // DSP_6

// METER PRE/POST (W/R)
#define CMD_CH_METER                        0x3160      // DSP_5
#define CMD_USB_METER                       0x3310
#define CMD_AUXGP_METER                     0x3161
#define CMD_MULTI_METER                     0x3126      // DSP_7
#define CMD_MAIN_METER                      0x3126      // DSP_6

// METER (R)
#define CMD_CH1_METER_LEVEL                 0x3162      // DSP_5    
#define CMD_CH2_METER_LEVEL                 0x3163
#define CMD_CH3_METER_LEVEL                 0x3164
#define CMD_CH4_METER_LEVEL                 0x3165
#define CMD_CH5_METER_LEVEL                 0x3166
#define CMD_CH6_METER_LEVEL                 0x3167
#define CMD_CH7_METER_LEVEL                 0x3168
#define CMD_CH8_METER_LEVEL                 0x3169
#define CMD_CH9_METER_LEVEL                 0x316A
#define CMD_CH10_METER_LEVEL                0x316B
#define CMD_CH11_METER_LEVEL                0x316C
#define CMD_CH12_METER_LEVEL                0x316D
#define CMD_CH13_METER_LEVEL                0x316E
#define CMD_CH14_METER_LEVEL                0x316F
#define CMD_CH15_METER_LEVEL                0x3170
#define CMD_CH16_METER_LEVEL                0x3171
#define CMD_CH17_USB_METER_LEVEL            0x331E
#define CMD_CH18_USB_METER_LEVEL            0x331F
#define CMD_AUX1_METER_LEVEL                0x317A
#define CMD_AUX2_METER_LEVEL                0x317B
#define CMD_AUX3_METER_LEVEL                0x317C
#define CMD_AUX4_METER_LEVEL                0x317D
#define CMD_GP1_METER_LEVEL                 0x3172
#define CMD_GP2_METER_LEVEL                 0x3173
#define CMD_GP3_METER_LEVEL                 0x3174
#define CMD_GP4_METER_LEVEL                 0x3175
#define CMD_EFFECT_1_1_OUT_METER            0x32AC
#define CMD_EFFECT_1_2_OUT_METER            0x32AD
#define CMD_EFFECT_1_1_IN_METER             0x32AE
#define CMD_EFFECT_1_2_IN_METER             0x32AF
#define CMD_EFFECT_2_1_OUT_METER            0x32B0
#define CMD_EFFECT_2_2_OUT_METER            0x32B1
#define CMD_EFFECT_2_1_IN_METER             0x32B2
#define CMD_EFFECT_2_2_IN_METER             0x32B3
#define CMD_USB_AUDIO_METER_L_OUT           0x3162      // DSP_6
#define CMD_USB_AUDIO_METER_R_OUT           0x3163
#define CMD_MULTI1_METER_LEVEL              0x3127      // DSP_7
#define CMD_MULTI2_METER_LEVEL              0x3128
#define CMD_MULTI3_METER_LEVEL              0x3129
#define CMD_MULTI4_METER_LEVEL              0x312A
#define CMD_CTRL_RM_L_METER_LEVEL           0x3129      // DSP_6
#define CMD_CTRL_RM_R_METER_LEVEL           0x312A
#define CMD_MAIN_L_METER_LEVEL              0x3127
#define CMD_MAIN_R_METER_LEVEL              0x3128
#define CMD_AUX1_CH1_METER_LEVEL            0x322C      // DSP_5
#define CMD_AUX1_CH2_METER_LEVEL            0x322D
#define CMD_AUX1_CH3_METER_LEVEL            0x322E
#define CMD_AUX1_CH4_METER_LEVEL            0x322F
#define CMD_AUX1_CH5_METER_LEVEL            0x3230
#define CMD_AUX1_CH6_METER_LEVEL            0x3231
#define CMD_AUX1_CH7_METER_LEVEL            0x3232
#define CMD_AUX1_CH8_METER_LEVEL            0x3233
#define CMD_AUX1_CH9_METER_LEVEL            0x3234
#define CMD_AUX1_CH10_METER_LEVEL           0x3235
#define CMD_AUX1_CH11_METER_LEVEL           0x3236
#define CMD_AUX1_CH12_METER_LEVEL           0x3237
#define CMD_AUX1_CH13_METER_LEVEL           0x3238
#define CMD_AUX1_CH14_METER_LEVEL           0x3239
#define CMD_AUX1_CH15_METER_LEVEL           0x323A
#define CMD_AUX1_CH16_METER_LEVEL           0x323B
#define CMD_AUX2_CH1_METER_LEVEL            0x323C
#define CMD_AUX2_CH2_METER_LEVEL            0x323D
#define CMD_AUX2_CH3_METER_LEVEL            0x323E
#define CMD_AUX2_CH4_METER_LEVEL            0x323F
#define CMD_AUX2_CH5_METER_LEVEL            0x3240
#define CMD_AUX2_CH6_METER_LEVEL            0x3241
#define CMD_AUX2_CH7_METER_LEVEL            0x3242
#define CMD_AUX2_CH8_METER_LEVEL            0x3243
#define CMD_AUX2_CH9_METER_LEVEL            0x3244
#define CMD_AUX2_CH10_METER_LEVEL           0x3245
#define CMD_AUX2_CH11_METER_LEVEL           0x3246
#define CMD_AUX2_CH12_METER_LEVEL           0x3247
#define CMD_AUX2_CH13_METER_LEVEL           0x3248
#define CMD_AUX2_CH14_METER_LEVEL           0x3249
#define CMD_AUX2_CH15_METER_LEVEL           0x324A
#define CMD_AUX2_CH16_METER_LEVEL           0x324B
#define CMD_AUX3_CH1_METER_LEVEL            0x324C
#define CMD_AUX3_CH2_METER_LEVEL            0x324D
#define CMD_AUX3_CH3_METER_LEVEL            0x324E
#define CMD_AUX3_CH4_METER_LEVEL            0x324F
#define CMD_AUX3_CH5_METER_LEVEL            0x3250
#define CMD_AUX3_CH6_METER_LEVEL            0x3251
#define CMD_AUX3_CH7_METER_LEVEL            0x3252
#define CMD_AUX3_CH8_METER_LEVEL            0x3253
#define CMD_AUX3_CH9_METER_LEVEL            0x3254
#define CMD_AUX3_CH10_METER_LEVEL           0x3255
#define CMD_AUX3_CH11_METER_LEVEL           0x3256
#define CMD_AUX3_CH12_METER_LEVEL           0x3257
#define CMD_AUX3_CH13_METER_LEVEL           0x3258
#define CMD_AUX3_CH14_METER_LEVEL           0x3259
#define CMD_AUX3_CH15_METER_LEVEL           0x325A
#define CMD_AUX3_CH16_METER_LEVEL           0x325B
#define CMD_AUX4_CH1_METER_LEVEL            0x325C
#define CMD_AUX4_CH2_METER_LEVEL            0x325D
#define CMD_AUX4_CH3_METER_LEVEL            0x325E
#define CMD_AUX4_CH4_METER_LEVEL            0x325F
#define CMD_AUX4_CH5_METER_LEVEL            0x3260
#define CMD_AUX4_CH6_METER_LEVEL            0x3261
#define CMD_AUX4_CH7_METER_LEVEL            0x3262
#define CMD_AUX4_CH8_METER_LEVEL            0x3263
#define CMD_AUX4_CH9_METER_LEVEL            0x3264
#define CMD_AUX4_CH10_METER_LEVEL           0x3265
#define CMD_AUX4_CH11_METER_LEVEL           0x3266
#define CMD_AUX4_CH12_METER_LEVEL           0x3267
#define CMD_AUX4_CH13_METER_LEVEL           0x3268
#define CMD_AUX4_CH14_METER_LEVEL           0x3269
#define CMD_AUX4_CH15_METER_LEVEL           0x326A
#define CMD_AUX4_CH16_METER_LEVEL           0x326B
#define CMD_AUX1_USB_METER_LEVEL            0x331D
#define CMD_AUX2_USB_METER_LEVEL            0x331E
#define CMD_AUX3_USB_METER_LEVEL            0x331F
#define CMD_AUX4_USB_METER_LEVEL            0x3320
#define CMD_GP1_CH1_METER_LEVEL             0x31AC      // DSP_5
#define CMD_GP1_CH2_METER_LEVEL             0x31AD
#define CMD_GP1_CH3_METER_LEVEL             0x31AE
#define CMD_GP1_CH4_METER_LEVEL             0x31AF
#define CMD_GP1_CH5_METER_LEVEL             0x31B0
#define CMD_GP1_CH6_METER_LEVEL             0x31B1
#define CMD_GP1_CH7_METER_LEVEL             0x31B2
#define CMD_GP1_CH8_METER_LEVEL             0x31B3
#define CMD_GP1_CH9_METER_LEVEL             0x31B4
#define CMD_GP1_CH10_METER_LEVEL            0x31B5
#define CMD_GP1_CH11_METER_LEVEL            0x31B6
#define CMD_GP1_CH12_METER_LEVEL            0x31B7
#define CMD_GP1_CH13_METER_LEVEL            0x31B8
#define CMD_GP1_CH14_METER_LEVEL            0x31B9
#define CMD_GP1_CH15_METER_LEVEL            0x31BA
#define CMD_GP1_CH16_METER_LEVEL            0x31BB
#define CMD_GP2_CH1_METER_LEVEL             0x31BC
#define CMD_GP2_CH2_METER_LEVEL             0x31BD
#define CMD_GP2_CH3_METER_LEVEL             0x31BE
#define CMD_GP2_CH4_METER_LEVEL             0x31BF
#define CMD_GP2_CH5_METER_LEVEL             0x31C0
#define CMD_GP2_CH6_METER_LEVEL             0x31C1
#define CMD_GP2_CH7_METER_LEVEL             0x31C2
#define CMD_GP2_CH8_METER_LEVEL             0x31C3
#define CMD_GP2_CH9_METER_LEVEL             0x31C4
#define CMD_GP2_CH10_METER_LEVEL            0x31C5
#define CMD_GP2_CH11_METER_LEVEL            0x31C6
#define CMD_GP2_CH12_METER_LEVEL            0x31C7
#define CMD_GP2_CH13_METER_LEVEL            0x31C8
#define CMD_GP2_CH14_METER_LEVEL            0x31C9
#define CMD_GP2_CH15_METER_LEVEL            0x31CA
#define CMD_GP2_CH16_METER_LEVEL            0x31CB
#define CMD_GP3_CH1_METER_LEVEL             0x31CC
#define CMD_GP3_CH2_METER_LEVEL             0x31CD
#define CMD_GP3_CH3_METER_LEVEL             0x31CE
#define CMD_GP3_CH4_METER_LEVEL             0x31CF
#define CMD_GP3_CH5_METER_LEVEL             0x31D0
#define CMD_GP3_CH6_METER_LEVEL             0x31D1
#define CMD_GP3_CH7_METER_LEVEL             0x31D2
#define CMD_GP3_CH8_METER_LEVEL             0x31D3
#define CMD_GP3_CH9_METER_LEVEL             0x31D4
#define CMD_GP3_CH10_METER_LEVEL            0x31D5
#define CMD_GP3_CH11_METER_LEVEL            0x31D6
#define CMD_GP3_CH12_METER_LEVEL            0x31D7
#define CMD_GP3_CH13_METER_LEVEL            0x31D8
#define CMD_GP3_CH14_METER_LEVEL            0x31D9
#define CMD_GP3_CH15_METER_LEVEL            0x31DA
#define CMD_GP3_CH16_METER_LEVEL            0x31DB
#define CMD_GP4_CH1_METER_LEVEL             0x31DC
#define CMD_GP4_CH2_METER_LEVEL             0x31DD
#define CMD_GP4_CH3_METER_LEVEL             0x31DE
#define CMD_GP4_CH4_METER_LEVEL             0x31DF
#define CMD_GP4_CH5_METER_LEVEL             0x31E0
#define CMD_GP4_CH6_METER_LEVEL             0x31E1
#define CMD_GP4_CH7_METER_LEVEL             0x31E2
#define CMD_GP4_CH8_METER_LEVEL             0x31E3
#define CMD_GP4_CH9_METER_LEVEL             0x31E4
#define CMD_GP4_CH10_METER_LEVEL            0x31E5
#define CMD_GP4_CH11_METER_LEVEL            0x31E6
#define CMD_GP4_CH12_METER_LEVEL            0x31E7
#define CMD_GP4_CH13_METER_LEVEL            0x31E8
#define CMD_GP4_CH14_METER_LEVEL            0x31E9
#define CMD_GP4_CH15_METER_LEVEL            0x31EA
#define CMD_GP4_CH16_METER_LEVEL            0x31EB
#define CMD_GP1_USB_METER_LEVEL             0x3319
#define CMD_GP2_USB_METER_LEVEL             0x331A
#define CMD_GP3_USB_METER_LEVEL             0x331B
#define CMD_GP4_USB_METER_LEVEL             0x331C
#define CMD_CH1_METER_EQ_IN                 0x3116      // DSP_1~DSP_4
#define CMD_CH2_METER_EQ_IN                 0x3117      // DSP_1~DSP_4
#define CMD_CH3_METER_EQ_IN                 0x3118      // DSP_1~DSP_4
#define CMD_CH4_METER_EQ_IN                 0x3119      // DSP_1~DSP_4
#define CMD_CH1_METER_EQ_OUT                0x311A      // DSP_1~DSP_4
#define CMD_CH2_METER_EQ_OUT                0x311B      // DSP_1~DSP_4
#define CMD_CH3_METER_EQ_OUT                0x311C      // DSP_1~DSP_4
#define CMD_CH4_METER_EQ_OUT                0x311D      // DSP_1~DSP_4
#define CMD_CH17_METER_EQ_IN                0x3118      // DSP_6
#define CMD_CH18_METER_EQ_IN                0x3119
#define CMD_CH17_METER_EQ_OUT               0x311C
#define CMD_CH18_METER_EQ_OUT               0x311D
#define CMD_MULTI1_METER_EQ_IN              0x3116      // DSP_7
#define CMD_MULTI2_METER_EQ_IN              0x3117
#define CMD_MULTI3_METER_EQ_IN              0x3118
#define CMD_MULTI4_METER_EQ_IN              0x3119
#define CMD_MULTI1_METER_EQ_OUT             0x311A
#define CMD_MULTI2_METER_EQ_OUT             0x311B
#define CMD_MULTI3_METER_EQ_OUT             0x311C
#define CMD_MULTI4_METER_EQ_OUT             0x311D
#define CMD_MAIN_L_METER_EQ_IN              0x3116      // DSP_6
#define CMD_MAIN_R_METER_EQ_IN              0x3117
#define CMD_MAIN_L_METER_EQ_OUT             0x311A
#define CMD_MAIN_R_METER_EQ_OUT             0x311B

// CH1-16 TO MAIN, GP TO MAIN (W/R)
#define CMD_CH_TO_MAIN_CTRL                 0x3074      // DSP_5
#define CMD_USB_TO_MAIN_CTRL                0x3310
#define CMD_GP_TO_MAIN_CTRL                 0x3075

// MULTI SOURCE
#define CMD_MULTI1_SOURCE                   0x30CA      // DSP_5
#define CMD_MULTI2_SOURCE                   0x30CB
#define CMD_MULTI3_SOURCE                   0x30CC
#define CMD_MULTI4_SOURCE                   0x30CD

// CLOCK
#define CMD_DSP_SET_CLOCK_SOURCE            0x3131      // DSP_6 (W/R)
#define CMD_DSP_STATUS_3                    0x3132      // DSP_6 (R)
#define CMD_DSP_SAMPLING_RATE               0x3133      // DSP_6 (R)

// SG (W/R)
#define CMD_SG_SETTING_CTRL                 0x30B4      // DSP_8
#define CMD_SG_LEVEL                        0x30B5      // DSP_5
#define CMD_SG_ASSIGN                       0x30D2      // DSP_5    

// EFFECT SOURCE (W/R)
#define CMD_EFFECT_1_1_IN_SOURCE            0x32B4      // DSP_5
#define CMD_EFFECT_1_2_IN_SOURCE            0x32B5
#define CMD_EFFECT_2_1_IN_SOURCE            0x32B6
#define CMD_EFFECT_2_2_IN_SOURCE            0x32B7
#define CMD_EFFECT_1_1_OUT_SOURCE           0x32B8
#define CMD_EFFECT_1_2_OUT_SOURCE           0x32B9
#define CMD_EFFECT_2_1_OUT_SOURCE           0x32BA
#define CMD_EFFECT_2_2_OUT_SOURCE           0x32BB

// EFFECT (W/R)
#define CMD_EFFECT_CONTROL                  0x32BE      // DSP_5 (bit0: EFX1, bit1: EFX2)
#define CMD_EFFECT_1_PROGRAM                0x3072      // DSP_8
#define CMD_EFFECT_1_DRY_WET                0x3074
#define CMD_EFFECT_1_FLANGER_LFO_FREQ       0x3075
#define CMD_EFFECT_1_FLANGER_LFO_PHASE      0x3076
#define CMD_EFFECT_1_FLANGER_LFO_TYPE       0x3077
#define CMD_EFFECT_1_FLANGER_DEPTH          0x3078
#define CMD_EFFECT_1_FLANGER_PRE_DELAY      0x3079
#define CMD_EFFECT_1_FLANGER_LPF_FREQ       0x307A
#define CMD_EFFECT_1_FLANGER_FB             0x307B
#define CMD_EFFECT_1_CHORUS_LFO_FREQ        0x307D
#define CMD_EFFECT_1_CHORUS_LFO_PHASE       0x307E
#define CMD_EFFECT_1_CHORUS_LFO_TYPE        0x307F
#define CMD_EFFECT_1_CHORUS_DEPTH           0x3080
#define CMD_EFFECT_1_CHORUS_PRE_DELAY       0x3081
#define CMD_EFFECT_1_CHORUS_LPF_FREQ        0x3082
#define CMD_EFFECT_1_VIBRATO_LFO_FREQ       0x3084
#define CMD_EFFECT_1_VIBRATO_LFO_TYPE       0x3085
#define CMD_EFFECT_1_VIBRATO_DEPTH          0x3086
#define CMD_EFFECT_1_VIBRATO_FREQ           0x3087
#define CMD_EFFECT_1_PHASER_LFO_FREQ        0x3089
#define CMD_EFFECT_1_PHASER_LFO_TYPE        0x308A
#define CMD_EFFECT_1_PHASER_DEPTH           0x308B
#define CMD_EFFECT_1_PHASER_FREQ            0x308C
#define CMD_EFFECT_1_PHASER_STAGE_NO        0x308D
#define CMD_EFFECT_1_TREMOLO_LFO_FREQ       0x308F
#define CMD_EFFECT_1_TREMOLO_LFO_TYPE       0x3090
#define CMD_EFFECT_1_TREMOLO_DEPTH          0x3091
#define CMD_EFFECT_1_AUTOPAN_LFO_FREQ       0x3093
#define CMD_EFFECT_1_AUTOPAN_LFO_TYPE       0x3094
#define CMD_EFFECT_1_AUTOPAN_DEPTH          0x3095
#define CMD_EFFECT_1_AUTOPAN_WAY            0x3096
#define CMD_EFFECT_1_TAP_KEY                0x3098
#define CMD_EFFECT_1_TAP_DELAY_TIME         0x3099
#define CMD_EFFECT_1_TAP_DELAY_FB           0x309A
#define CMD_EFFECT_1_TAP_DELAY_STATUS       0x309B      // (R)
#define CMD_EFFECT_1_TAP_DELAY_FB_HPF       0x309C
#define CMD_EFFECT_1_TAP_DELAY_FB_LPF       0x309D
#define CMD_EFFECT_1_ECHO_DELAY_1_TIME      0x309F
#define CMD_EFFECT_1_ECHO_DELAY_2_TIME      0x30A0
#define CMD_EFFECT_1_ECHO_DELAY_FB_1        0x30A1
#define CMD_EFFECT_1_ECHO_DELAY_FB_2        0x30A2
#define CMD_EFFECT_1_ECHO_DELAY_FB_HPF      0x30A3
#define CMD_EFFECT_1_ECHO_DELAY_FB_LPF      0x30A4
#define CMD_EFFECT_1_REVERB_TYPE            0x30A6
#define CMD_EFFECT_1_REVERB_LPF_FREQ        0x30A7
#define CMD_EFFECT_1_REVERB_HPF_FREQ        0x30A8
#define CMD_EFFECT_1_REVERB_TIME            0x30A9
#define CMD_EFFECT_1_REVERB_PRE_DELAY       0x30AA
#define CMD_EFFECT_1_REVERB_EARLY_DELAY_OUT 0x30AB
#define CMD_EFFECT_1_REVERB_HI_RATIO        0x30AC
#define CMD_EFFECT_1_REVERB_DENSITY         0x30AD
#define CMD_EFFECT_1_REVERB_LEVEL           0x30AE
#define CMD_EFFECT_1_REVERB_GATE            0x30AF
#define CMD_EFFECT_1_REVERB_GATE_THRESHOLD  0x30B0
#define CMD_EFFECT_1_REVERB_GATE_HOLD_TIME  0x30B1
#define CMD_EFFECT_1_DRY_WET_MIX            0x32BC
#define CMD_EFFECT_2_PROGRAM                0x32BD      // DSP_5
#define CMD_EFFECT_2_DRY_WET_MIX            0x32BF
#define CMD_EFFECT_2_FLANGER_LFO_FREQ       0x32C0
#define CMD_EFFECT_2_FLANGER_LFO_PHASE      0x32C1
#define CMD_EFFECT_2_FLANGER_LFO_TYPE       0x32C2
#define CMD_EFFECT_2_FLANGER_DEPTH          0x32C3
#define CMD_EFFECT_2_FLANGER_PRE_DELAY      0x32C4
#define CMD_EFFECT_2_FLANGER_LPF_FREQ       0x32C5
#define CMD_EFFECT_2_FLANGER_FB             0x32C6
#define CMD_EFFECT_2_CHORUS_LFO_FREQ        0x32C8
#define CMD_EFFECT_2_CHORUS_LFO_PHASE       0x32C9
#define CMD_EFFECT_2_CHORUS_LFO_TYPE        0x32CA
#define CMD_EFFECT_2_CHORUS_DEPTH           0x32CB
#define CMD_EFFECT_2_CHORUS_PRE_DELAY       0x32CC
#define CMD_EFFECT_2_CHORUS_LPF_FREQ        0x32CD
#define CMD_EFFECT_2_VIBRATO_LFO_FREQ       0x32CF
#define CMD_EFFECT_2_VIBRATO_LFO_TYPE       0x32D0
#define CMD_EFFECT_2_VIBRATO_DEPTH          0x32D1
#define CMD_EFFECT_2_VIBRATO_FREQ           0x32D2
#define CMD_EFFECT_2_PHASER_LFO_FREQ        0x32D4
#define CMD_EFFECT_2_PHASER_LFO_TYPE        0x32D5
#define CMD_EFFECT_2_PHASER_DEPTH           0x32D6
#define CMD_EFFECT_2_PHASER_FREQ            0x32D7
#define CMD_EFFECT_2_PHASER_STAGE_NO        0x32D8
#define CMD_EFFECT_2_TREMOLO_LFO_FREQ       0x32DA
#define CMD_EFFECT_2_TREMOLO_LFO_TYPE       0x32DB
#define CMD_EFFECT_2_TREMOLO_DEPTH          0x32DC
#define CMD_EFFECT_2_AUTOPAN_LFO_FREQ       0x32DE
#define CMD_EFFECT_2_AUTOPAN_LFO_TYPE       0x32DF
#define CMD_EFFECT_2_AUTOPAN_DEPTH          0x32E0
#define CMD_EFFECT_2_AUTOPAN_WAY            0x32E1
#define CMD_EFFECT_2_TAP_KEY                0x32E3
#define CMD_EFFECT_2_TAP_DELAY_TIME         0x32E4
#define CMD_EFFECT_2_TAP_DELAY_FB           0x32E5
#define CMD_EFFECT_2_TAP_DELAY_STATUS       0x32E6      // (R)
#define CMD_EFFECT_2_TAP_DELAY_FB_HPF       0x32E7
#define CMD_EFFECT_2_TAP_DELAY_FB_LPF       0x32E8
#define CMD_EFFECT_2_ECHO_DELAY_1_TIME      0x32EA
#define CMD_EFFECT_2_ECHO_DELAY_2_TIME      0x32EB
#define CMD_EFFECT_2_ECHO_DELAY_FB_1        0x32EC
#define CMD_EFFECT_2_ECHO_DELAY_FB_2        0x32ED
#define CMD_EFFECT_2_ECHO_DELAY_FB_HPF      0x32EE
#define CMD_EFFECT_2_ECHO_DELAY_FB_LPF      0x32EF

// GEQ31 (W/R)
#define CMD_L_GEQ_20HZ_DB                   0x30B6      // DSP_8
#define CMD_L_GEQ_25HZ_DB                   0x30B7 
#define CMD_L_GEQ_31_5HZ_DB                 0x30B8
#define CMD_L_GEQ_40HZ_DB                   0x30B9
#define CMD_L_GEQ_50HZ_DB                   0x30BA
#define CMD_L_GEQ_63HZ_DB                   0x30BB
#define CMD_L_GEQ_80HZ_DB                   0x30BC
#define CMD_L_GEQ_100HZ_DB                  0x30BD
#define CMD_L_GEQ_125HZ_DB                  0x30BE
#define CMD_L_GEQ_160HZ_DB                  0x30BF
#define CMD_L_GEQ_200HZ_DB                  0x30C0
#define CMD_L_GEQ_250HZ_DB                  0x30C1
#define CMD_L_GEQ_315HZ_DB                  0x30C2
#define CMD_L_GEQ_400HZ_DB                  0x30C3
#define CMD_L_GEQ_500HZ_DB                  0x30C4 
#define CMD_L_GEQ_630HZ_DB                  0x30C5
#define CMD_L_GEQ_800HZ_DB                  0x30C6
#define CMD_L_GEQ_1KHZ_DB                   0x30C7
#define CMD_L_GEQ_1_25KHZ_DB                0x30C8
#define CMD_L_GEQ_1_6KHZ_DB                 0x30C9
#define CMD_L_GEQ_2KHZ_DB                   0x30CA
#define CMD_L_GEQ_2_5KHZ_DB                 0x30CB
#define CMD_L_GEQ_3_15KHZ_DB                0x30CC
#define CMD_L_GEQ_4KHZ_DB                   0x30CD
#define CMD_L_GEQ_5KHZ_DB                   0x30CE
#define CMD_L_GEQ_6_3KHZ_DB                 0x30CF
#define CMD_L_GEQ_8KHZ_DB                   0x30D0
#define CMD_L_GEQ_10KHZ_DB                  0x30D1
#define CMD_L_GEQ_12_5KHZ_DB                0x30D2
#define CMD_L_GEQ_16KHZ_DB                  0x30D3
#define CMD_L_GEQ_20KHZ_DB                  0x30D4
#define CMD_R_GEQ_20HZ_DB                   0x30D5      // DSP_8
#define CMD_R_GEQ_25HZ_DB                   0x30D6
#define CMD_R_GEQ_31_5HZ_DB                 0x30D7
#define CMD_R_GEQ_40HZ_DB                   0x30D8
#define CMD_R_GEQ_50HZ_DB                   0x30D9
#define CMD_R_GEQ_63HZ_DB                   0x30DA
#define CMD_R_GEQ_80HZ_DB                   0x30DB
#define CMD_R_GEQ_100HZ_DB                  0x30DC
#define CMD_R_GEQ_125HZ_DB                  0x30DD
#define CMD_R_GEQ_160HZ_DB                  0x30DE
#define CMD_R_GEQ_200HZ_DB                  0x30DF
#define CMD_R_GEQ_250HZ_DB                  0x30E0
#define CMD_R_GEQ_315HZ_DB                  0x30E1
#define CMD_R_GEQ_400HZ_DB                  0x30E2
#define CMD_R_GEQ_500HZ_DB                  0x30E3
#define CMD_R_GEQ_630HZ_DB                  0x30E4
#define CMD_R_GEQ_800HZ_DB                  0x30E5
#define CMD_R_GEQ_1KHZ_DB                   0x30E6
#define CMD_R_GEQ_1_25KHZ_DB                0x30E7
#define CMD_R_GEQ_1_6KHZ_DB                 0x30E8
#define CMD_R_GEQ_2KHZ_DB                   0x30E9
#define CMD_R_GEQ_2_5KHZ_DB                 0x30EA
#define CMD_R_GEQ_3_15KHZ_DB                0x30EB
#define CMD_R_GEQ_4KHZ_DB                   0x30EC
#define CMD_R_GEQ_5KHZ_DB                   0x30ED
#define CMD_R_GEQ_6_3KHZ_DB                 0x30EE
#define CMD_R_GEQ_8KHZ_DB                   0x30EF
#define CMD_R_GEQ_10KHZ_DB                  0x30F0
#define CMD_R_GEQ_12_5KHZ_DB                0x30F1
#define CMD_R_GEQ_16KHZ_DB                  0x30F2
#define CMD_R_GEQ_20KHZ_DB                  0x30F3

// MAIN GEQ31 (W/R)
#define CMD_MAIN_GEQ_20HZ_DB                0x313C      // DSP_6
#define CMD_MAIN_GEQ_25HZ_DB                0x313D
#define CMD_MAIN_GEQ_31_5HZ_DB              0x313E
#define CMD_MAIN_GEQ_40HZ_DB                0x313F
#define CMD_MAIN_GEQ_50HZ_DB                0x3140
#define CMD_MAIN_GEQ_63HZ_DB                0x3141
#define CMD_MAIN_GEQ_80HZ_DB                0x3142
#define CMD_MAIN_GEQ_100HZ_DB               0x3143
#define CMD_MAIN_GEQ_125HZ_DB               0x3144
#define CMD_MAIN_GEQ_160HZ_DB               0x3145
#define CMD_MAIN_GEQ_200HZ_DB               0x3146
#define CMD_MAIN_GEQ_250HZ_DB               0x3147
#define CMD_MAIN_GEQ_315HZ_DB               0x3148
#define CMD_MAIN_GEQ_400HZ_DB               0x3149
#define CMD_MAIN_GEQ_500HZ_DB               0x314A
#define CMD_MAIN_GEQ_630HZ_DB               0x314B
#define CMD_MAIN_GEQ_800HZ_DB               0x314C
#define CMD_MAIN_GEQ_1KHZ_DB                0x314D
#define CMD_MAIN_GEQ_1_25KHZ_DB             0x314E
#define CMD_MAIN_GEQ_1_6KHZ_DB              0x314F
#define CMD_MAIN_GEQ_2KHZ_DB                0x3150
#define CMD_MAIN_GEQ_2_5KHZ_DB              0x3151
#define CMD_MAIN_GEQ_3_15KHZ_DB             0x3152
#define CMD_MAIN_GEQ_4KHZ_DB                0x3153
#define CMD_MAIN_GEQ_5KHZ_DB                0x3154
#define CMD_MAIN_GEQ_6_3KHZ_DB              0x3155
#define CMD_MAIN_GEQ_8KHZ_DB                0x3156
#define CMD_MAIN_GEQ_10KHZ_DB               0x3157
#define CMD_MAIN_GEQ_12_5KHZ_DB             0x3158
#define CMD_MAIN_GEQ_16KHZ_DB               0x3159
#define CMD_MAIN_GEQ_20KHZ_DB               0x315A

// GEQ15 (W/R)
#define CMD_L_GEQ15_25HZ_DB                 0x32F2      // DSP_5
#define CMD_L_GEQ15_40HZ_DB                 0x32F3
#define CMD_L_GEQ15_63HZ_DB                 0x32F4
#define CMD_L_GEQ15_100HZ_DB                0x32F5
#define CMD_L_GEQ15_160HZ_DB                0x32F6
#define CMD_L_GEQ15_250HZ_DB                0x32F7
#define CMD_L_GEQ15_400HZ_DB                0x32F8
#define CMD_L_GEQ15_630HZ_DB                0x32F9
#define CMD_L_GEQ15_1KHZ_DB                 0x32FA
#define CMD_L_GEQ15_1_6KHZ_DB               0x32FB
#define CMD_L_GEQ15_2_5KHZ_DB               0x32FC
#define CMD_L_GEQ15_4KHZ_DB                 0x32FD
#define CMD_L_GEQ15_6_3KHZ_DB               0x32FE
#define CMD_L_GEQ15_10KHZ_DB                0x32FF
#define CMD_L_GEQ15_16KHZ_DB                0x3300
#define CMD_R_GEQ15_25HZ_DB                 0x3301      // DSP_5
#define CMD_R_GEQ15_40HZ_DB                 0x3302
#define CMD_R_GEQ15_63HZ_DB                 0x3303
#define CMD_R_GEQ15_100HZ_DB                0x3304
#define CMD_R_GEQ15_160HZ_DB                0x3305
#define CMD_R_GEQ15_250HZ_DB                0x3306
#define CMD_R_GEQ15_400HZ_DB                0x3307
#define CMD_R_GEQ15_630HZ_DB                0x3308
#define CMD_R_GEQ15_1KHZ_DB                 0x3309
#define CMD_R_GEQ15_1_6KHZ_DB               0x330A
#define CMD_R_GEQ15_2_5KHZ_DB               0x330B
#define CMD_R_GEQ15_4KHZ_DB                 0x330C
#define CMD_R_GEQ15_6_3KHZ_DB               0x330D
#define CMD_R_GEQ15_10KHZ_DB                0x330E
#define CMD_R_GEQ15_16KHZ_DB                0x330F

// EQ GFQ
#define CMD_CH1_EQ1_DB                      0x30BA
#define CMD_CH1_EQ2_DB                      0x30BB
#define CMD_CH1_EQ3_DB                      0x30BC
#define CMD_CH1_EQ4_DB                      0x30BD
#define CMD_CH2_EQ1_DB                      0x30BE
#define CMD_CH2_EQ2_DB                      0x30BF
#define CMD_CH2_EQ3_DB                      0x30C0
#define CMD_CH2_EQ4_DB                      0x30C1
#define CMD_CH3_EQ1_DB                      0x30C2
#define CMD_CH3_EQ2_DB                      0x30C3
#define CMD_CH3_EQ3_DB                      0x30C4
#define CMD_CH3_EQ4_DB                      0x30C5
#define CMD_CH4_EQ1_DB                      0x30C6
#define CMD_CH4_EQ2_DB                      0x30C7
#define CMD_CH4_EQ3_DB                      0x30C8
#define CMD_CH4_EQ4_DB                      0x30C9
#define CMD_CH1_EQ1_Q                       0x30CA
#define CMD_CH1_EQ2_Q                       0x30CB
#define CMD_CH1_EQ3_Q                       0x30CC
#define CMD_CH1_EQ4_Q                       0x30CD
#define CMD_CH2_EQ1_Q                       0x30CE
#define CMD_CH2_EQ2_Q                       0x30CF
#define CMD_CH2_EQ3_Q                       0x30D0
#define CMD_CH2_EQ4_Q                       0x30D1
#define CMD_CH3_EQ1_Q                       0x30D2
#define CMD_CH3_EQ2_Q                       0x30D3
#define CMD_CH3_EQ3_Q                       0x30D4
#define CMD_CH3_EQ4_Q                       0x30D5
#define CMD_CH4_EQ1_Q                       0x30D6
#define CMD_CH4_EQ2_Q                       0x30D7
#define CMD_CH4_EQ3_Q                       0x30D8
#define CMD_CH4_EQ4_Q                       0x30D9
#define CMD_CH1_EQ1_FREQ                    0x30DA
#define CMD_CH1_EQ2_FREQ                    0x30DB
#define CMD_CH1_EQ3_FREQ                    0x30DC
#define CMD_CH1_EQ4_FREQ                    0x30DD
#define CMD_CH2_EQ1_FREQ                    0x30DE
#define CMD_CH2_EQ2_FREQ                    0x30DF
#define CMD_CH2_EQ3_FREQ                    0x30E0
#define CMD_CH2_EQ4_FREQ                    0x30E1
#define CMD_CH3_EQ1_FREQ                    0x30E2
#define CMD_CH3_EQ2_FREQ                    0x30E3
#define CMD_CH3_EQ3_FREQ                    0x30E4
#define CMD_CH3_EQ4_FREQ                    0x30E5
#define CMD_CH4_EQ1_FREQ                    0x30E6
#define CMD_CH4_EQ2_FREQ                    0x30E7
#define CMD_CH4_EQ3_FREQ                    0x30E8
#define CMD_CH4_EQ4_FREQ                    0x30E9
#define CMD_CH17_EQ1_DB                     0x30C2      // DSP_6
#define CMD_CH17_EQ2_DB                     0x30C3
#define CMD_CH17_EQ3_DB                     0x30C4
#define CMD_CH17_EQ4_DB                     0x30C5
#define CMD_CH17_EQ1_Q                      0x30D2
#define CMD_CH17_EQ2_Q                      0x30D3
#define CMD_CH17_EQ3_Q                      0x30D4
#define CMD_CH17_EQ4_Q                      0x30D5
#define CMD_CH17_EQ1_FREQ                   0x30E2
#define CMD_CH17_EQ2_FREQ                   0x30E3
#define CMD_CH17_EQ3_FREQ                   0x30E4
#define CMD_CH17_EQ4_FREQ                   0x30E5
#define CMD_CH18_EQ1_DB                     0x30C6
#define CMD_CH18_EQ2_DB                     0x30C7
#define CMD_CH18_EQ3_DB                     0x30C8
#define CMD_CH18_EQ4_DB                     0x30C9
#define CMD_CH18_EQ1_Q                      0x30D6
#define CMD_CH18_EQ2_Q                      0x30D7
#define CMD_CH18_EQ3_Q                      0x30D8
#define CMD_CH18_EQ4_Q                      0x30D9
#define CMD_CH18_EQ1_FREQ                   0x30E6
#define CMD_CH18_EQ2_FREQ                   0x30E7
#define CMD_CH18_EQ3_FREQ                   0x30E8
#define CMD_CH18_EQ4_FREQ                   0x30E9
#define CMD_MULTI1_EQ1_DB                   0x30BA      // DSP_7
#define CMD_MULTI1_EQ2_DB                   0x30BB
#define CMD_MULTI1_EQ3_DB                   0x30BC
#define CMD_MULTI1_EQ4_DB                   0x30BD
#define CMD_MULTI2_EQ1_DB                   0x30BE
#define CMD_MULTI2_EQ2_DB                   0x30BF
#define CMD_MULTI2_EQ3_DB                   0x30C0
#define CMD_MULTI2_EQ4_DB                   0x30C1
#define CMD_MULTI3_EQ1_DB                   0x30C2
#define CMD_MULTI3_EQ2_DB                   0x30C3
#define CMD_MULTI3_EQ3_DB                   0x30C4
#define CMD_MULTI3_EQ4_DB                   0x30C5
#define CMD_MULTI4_EQ1_DB                   0x30C6
#define CMD_MULTI4_EQ2_DB                   0x30C7
#define CMD_MULTI4_EQ3_DB                   0x30C8
#define CMD_MULTI4_EQ4_DB                   0x30C9
#define CMD_MULTI1_EQ1_Q                    0x30CA
#define CMD_MULTI1_EQ2_Q                    0x30CB
#define CMD_MULTI1_EQ3_Q                    0x30CC
#define CMD_MULTI1_EQ4_Q                    0x30CD
#define CMD_MULTI2_EQ1_Q                    0x30CE
#define CMD_MULTI2_EQ2_Q                    0x30CF
#define CMD_MULTI2_EQ3_Q                    0x30D0
#define CMD_MULTI2_EQ4_Q                    0x30D1
#define CMD_MULTI3_EQ1_Q                    0x30D2
#define CMD_MULTI3_EQ2_Q                    0x30D3
#define CMD_MULTI3_EQ3_Q                    0x30D4
#define CMD_MULTI3_EQ4_Q                    0x30D5
#define CMD_MULTI4_EQ1_Q                    0x30D6
#define CMD_MULTI4_EQ2_Q                    0x30D7
#define CMD_MULTI4_EQ3_Q                    0x30D8
#define CMD_MULTI4_EQ4_Q                    0x30D9
#define CMD_MULTI1_EQ1_FREQ                 0x30DA
#define CMD_MULTI1_EQ2_FREQ                 0x30DB
#define CMD_MULTI1_EQ3_FREQ                 0x30DC
#define CMD_MULTI1_EQ4_FREQ                 0x30DD
#define CMD_MULTI2_EQ1_FREQ                 0x30DE
#define CMD_MULTI2_EQ2_FREQ                 0x30DF
#define CMD_MULTI2_EQ3_FREQ                 0x30E0
#define CMD_MULTI2_EQ4_FREQ                 0x30E1
#define CMD_MULTI3_EQ1_FREQ                 0x30E2
#define CMD_MULTI3_EQ2_FREQ                 0x30E3
#define CMD_MULTI3_EQ3_FREQ                 0x30E4
#define CMD_MULTI3_EQ4_FREQ                 0x30E5
#define CMD_MULTI4_EQ1_FREQ                 0x30E6
#define CMD_MULTI4_EQ2_FREQ                 0x30E7
#define CMD_MULTI4_EQ3_FREQ                 0x30E8
#define CMD_MULTI4_EQ4_FREQ                 0x30E9
#define CMD_MAIN_LR_EQ1_DB                  0x30BA      // DSP_6
#define CMD_MAIN_LR_EQ2_DB                  0x30BB
#define CMD_MAIN_LR_EQ3_DB                  0x30BC
#define CMD_MAIN_LR_EQ4_DB                  0x30BD
#define CMD_MAIN_LR_EQ1_Q                   0x30CA
#define CMD_MAIN_LR_EQ2_Q                   0x30CB
#define CMD_MAIN_LR_EQ3_Q                   0x30CC
#define CMD_MAIN_LR_EQ4_Q                   0x30CD
#define CMD_MAIN_LR_EQ1_FREQ                0x30DA
#define CMD_MAIN_LR_EQ2_FREQ                0x30DB
#define CMD_MAIN_LR_EQ3_FREQ                0x30DC
#define CMD_MAIN_LR_EQ4_FREQ                0x30DD

// USB AUDIO OUT
#define CMD_USB_AUDIO_LOUT_ASSIGN           0x3160      // DSP_6
#define CMD_USB_AUDIO_ROUT_ASSIGN           0x3161

#endif
