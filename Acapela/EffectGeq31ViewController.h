//
//  EffectGeq31Controller.h
//  Acapela
//
//  Created by Kevin on 13/8/25.
//  Copyright (c) 2013年 Kevin Phua. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "acapela.h"
#import "JEProgressView.h"

@class EffectGeq31Controller;
@protocol Geq31PreviewDelegate <NSObject>
@required
- (void)didFinishEditingGeq31;
@end

@interface EffectGeq31ViewController : UIViewController

// Delegate callback to parent controller
@property (nonatomic, assign) id <Geq31PreviewDelegate> delegate;
@property (strong, nonatomic) UIImage *imgEqL;
@property (strong, nonatomic) UIImage *imgEqR;

// GEQ panel
@property (weak, nonatomic) IBOutlet UIButton *btnGeqLink;
@property (weak, nonatomic) IBOutlet UIButton *btnGeqDraw;
@property (weak, nonatomic) IBOutlet UIButton *btnFile;
@property (weak, nonatomic) IBOutlet UIView *viewGeqFrameL;
@property (weak, nonatomic) IBOutlet UIView *viewGeqDrawL;
@property (weak, nonatomic) IBOutlet UIView *viewGeqFrameR;
@property (weak, nonatomic) IBOutlet UIView *viewGeqDrawR;

// Sliders
@property (nonatomic,retain) UISlider *sliderLGeq1;
@property (nonatomic,retain) UISlider *sliderLGeq2;
@property (nonatomic,retain) UISlider *sliderLGeq3;
@property (nonatomic,retain) UISlider *sliderLGeq4;
@property (nonatomic,retain) UISlider *sliderLGeq5;
@property (nonatomic,retain) UISlider *sliderLGeq6;
@property (nonatomic,retain) UISlider *sliderLGeq7;
@property (nonatomic,retain) UISlider *sliderLGeq8;
@property (nonatomic,retain) UISlider *sliderLGeq9;
@property (nonatomic,retain) UISlider *sliderLGeq10;
@property (nonatomic,retain) UISlider *sliderLGeq11;
@property (nonatomic,retain) UISlider *sliderLGeq12;
@property (nonatomic,retain) UISlider *sliderLGeq13;
@property (nonatomic,retain) UISlider *sliderLGeq14;
@property (nonatomic,retain) UISlider *sliderLGeq15;
@property (nonatomic,retain) UISlider *sliderLGeq16;
@property (nonatomic,retain) UISlider *sliderLGeq17;
@property (nonatomic,retain) UISlider *sliderLGeq18;
@property (nonatomic,retain) UISlider *sliderLGeq19;
@property (nonatomic,retain) UISlider *sliderLGeq20;
@property (nonatomic,retain) UISlider *sliderLGeq21;
@property (nonatomic,retain) UISlider *sliderLGeq22;
@property (nonatomic,retain) UISlider *sliderLGeq23;
@property (nonatomic,retain) UISlider *sliderLGeq24;
@property (nonatomic,retain) UISlider *sliderLGeq25;
@property (nonatomic,retain) UISlider *sliderLGeq26;
@property (nonatomic,retain) UISlider *sliderLGeq27;
@property (nonatomic,retain) UISlider *sliderLGeq28;
@property (nonatomic,retain) UISlider *sliderLGeq29;
@property (nonatomic,retain) UISlider *sliderLGeq30;
@property (nonatomic,retain) UISlider *sliderLGeq31;

@property (nonatomic,retain) UISlider *sliderRGeq1;
@property (nonatomic,retain) UISlider *sliderRGeq2;
@property (nonatomic,retain) UISlider *sliderRGeq3;
@property (nonatomic,retain) UISlider *sliderRGeq4;
@property (nonatomic,retain) UISlider *sliderRGeq5;
@property (nonatomic,retain) UISlider *sliderRGeq6;
@property (nonatomic,retain) UISlider *sliderRGeq7;
@property (nonatomic,retain) UISlider *sliderRGeq8;
@property (nonatomic,retain) UISlider *sliderRGeq9;
@property (nonatomic,retain) UISlider *sliderRGeq10;
@property (nonatomic,retain) UISlider *sliderRGeq11;
@property (nonatomic,retain) UISlider *sliderRGeq12;
@property (nonatomic,retain) UISlider *sliderRGeq13;
@property (nonatomic,retain) UISlider *sliderRGeq14;
@property (nonatomic,retain) UISlider *sliderRGeq15;
@property (nonatomic,retain) UISlider *sliderRGeq16;
@property (nonatomic,retain) UISlider *sliderRGeq17;
@property (nonatomic,retain) UISlider *sliderRGeq18;
@property (nonatomic,retain) UISlider *sliderRGeq19;
@property (nonatomic,retain) UISlider *sliderRGeq20;
@property (nonatomic,retain) UISlider *sliderRGeq21;
@property (nonatomic,retain) UISlider *sliderRGeq22;
@property (nonatomic,retain) UISlider *sliderRGeq23;
@property (nonatomic,retain) UISlider *sliderRGeq24;
@property (nonatomic,retain) UISlider *sliderRGeq25;
@property (nonatomic,retain) UISlider *sliderRGeq26;
@property (nonatomic,retain) UISlider *sliderRGeq27;
@property (nonatomic,retain) UISlider *sliderRGeq28;
@property (nonatomic,retain) UISlider *sliderRGeq29;
@property (nonatomic,retain) UISlider *sliderRGeq30;
@property (nonatomic,retain) UISlider *sliderRGeq31;

// Slider Meters
@property (nonatomic,retain) JEProgressView *meterL1Top;
@property (nonatomic,retain) JEProgressView *meterL1Btm;
@property (nonatomic,retain) JEProgressView *meterL2Top;
@property (nonatomic,retain) JEProgressView *meterL2Btm;
@property (nonatomic,retain) JEProgressView *meterL3Top;
@property (nonatomic,retain) JEProgressView *meterL3Btm;
@property (nonatomic,retain) JEProgressView *meterL4Top;
@property (nonatomic,retain) JEProgressView *meterL4Btm;
@property (nonatomic,retain) JEProgressView *meterL5Top;
@property (nonatomic,retain) JEProgressView *meterL5Btm;
@property (nonatomic,retain) JEProgressView *meterL6Top;
@property (nonatomic,retain) JEProgressView *meterL6Btm;
@property (nonatomic,retain) JEProgressView *meterL7Top;
@property (nonatomic,retain) JEProgressView *meterL7Btm;
@property (nonatomic,retain) JEProgressView *meterL8Top;
@property (nonatomic,retain) JEProgressView *meterL8Btm;
@property (nonatomic,retain) JEProgressView *meterL9Top;
@property (nonatomic,retain) JEProgressView *meterL9Btm;
@property (nonatomic,retain) JEProgressView *meterL10Top;
@property (nonatomic,retain) JEProgressView *meterL10Btm;
@property (nonatomic,retain) JEProgressView *meterL11Top;
@property (nonatomic,retain) JEProgressView *meterL11Btm;
@property (nonatomic,retain) JEProgressView *meterL12Top;
@property (nonatomic,retain) JEProgressView *meterL12Btm;
@property (nonatomic,retain) JEProgressView *meterL13Top;
@property (nonatomic,retain) JEProgressView *meterL13Btm;
@property (nonatomic,retain) JEProgressView *meterL14Top;
@property (nonatomic,retain) JEProgressView *meterL14Btm;
@property (nonatomic,retain) JEProgressView *meterL15Top;
@property (nonatomic,retain) JEProgressView *meterL15Btm;
@property (nonatomic,retain) JEProgressView *meterL16Top;
@property (nonatomic,retain) JEProgressView *meterL16Btm;
@property (nonatomic,retain) JEProgressView *meterL17Top;
@property (nonatomic,retain) JEProgressView *meterL17Btm;
@property (nonatomic,retain) JEProgressView *meterL18Top;
@property (nonatomic,retain) JEProgressView *meterL18Btm;
@property (nonatomic,retain) JEProgressView *meterL19Top;
@property (nonatomic,retain) JEProgressView *meterL19Btm;
@property (nonatomic,retain) JEProgressView *meterL20Top;
@property (nonatomic,retain) JEProgressView *meterL20Btm;
@property (nonatomic,retain) JEProgressView *meterL21Top;
@property (nonatomic,retain) JEProgressView *meterL21Btm;
@property (nonatomic,retain) JEProgressView *meterL22Top;
@property (nonatomic,retain) JEProgressView *meterL22Btm;
@property (nonatomic,retain) JEProgressView *meterL23Top;
@property (nonatomic,retain) JEProgressView *meterL23Btm;
@property (nonatomic,retain) JEProgressView *meterL24Top;
@property (nonatomic,retain) JEProgressView *meterL24Btm;
@property (nonatomic,retain) JEProgressView *meterL25Top;
@property (nonatomic,retain) JEProgressView *meterL25Btm;
@property (nonatomic,retain) JEProgressView *meterL26Top;
@property (nonatomic,retain) JEProgressView *meterL26Btm;
@property (nonatomic,retain) JEProgressView *meterL27Top;
@property (nonatomic,retain) JEProgressView *meterL27Btm;
@property (nonatomic,retain) JEProgressView *meterL28Top;
@property (nonatomic,retain) JEProgressView *meterL28Btm;
@property (nonatomic,retain) JEProgressView *meterL29Top;
@property (nonatomic,retain) JEProgressView *meterL29Btm;
@property (nonatomic,retain) JEProgressView *meterL30Top;
@property (nonatomic,retain) JEProgressView *meterL30Btm;
@property (nonatomic,retain) JEProgressView *meterL31Top;
@property (nonatomic,retain) JEProgressView *meterL31Btm;

@property (nonatomic,retain) JEProgressView *meterR1Top;
@property (nonatomic,retain) JEProgressView *meterR1Btm;
@property (nonatomic,retain) JEProgressView *meterR2Top;
@property (nonatomic,retain) JEProgressView *meterR2Btm;
@property (nonatomic,retain) JEProgressView *meterR3Top;
@property (nonatomic,retain) JEProgressView *meterR3Btm;
@property (nonatomic,retain) JEProgressView *meterR4Top;
@property (nonatomic,retain) JEProgressView *meterR4Btm;
@property (nonatomic,retain) JEProgressView *meterR5Top;
@property (nonatomic,retain) JEProgressView *meterR5Btm;
@property (nonatomic,retain) JEProgressView *meterR6Top;
@property (nonatomic,retain) JEProgressView *meterR6Btm;
@property (nonatomic,retain) JEProgressView *meterR7Top;
@property (nonatomic,retain) JEProgressView *meterR7Btm;
@property (nonatomic,retain) JEProgressView *meterR8Top;
@property (nonatomic,retain) JEProgressView *meterR8Btm;
@property (nonatomic,retain) JEProgressView *meterR9Top;
@property (nonatomic,retain) JEProgressView *meterR9Btm;
@property (nonatomic,retain) JEProgressView *meterR10Top;
@property (nonatomic,retain) JEProgressView *meterR10Btm;
@property (nonatomic,retain) JEProgressView *meterR11Top;
@property (nonatomic,retain) JEProgressView *meterR11Btm;
@property (nonatomic,retain) JEProgressView *meterR12Top;
@property (nonatomic,retain) JEProgressView *meterR12Btm;
@property (nonatomic,retain) JEProgressView *meterR13Top;
@property (nonatomic,retain) JEProgressView *meterR13Btm;
@property (nonatomic,retain) JEProgressView *meterR14Top;
@property (nonatomic,retain) JEProgressView *meterR14Btm;
@property (nonatomic,retain) JEProgressView *meterR15Top;
@property (nonatomic,retain) JEProgressView *meterR15Btm;
@property (nonatomic,retain) JEProgressView *meterR16Top;
@property (nonatomic,retain) JEProgressView *meterR16Btm;
@property (nonatomic,retain) JEProgressView *meterR17Top;
@property (nonatomic,retain) JEProgressView *meterR17Btm;
@property (nonatomic,retain) JEProgressView *meterR18Top;
@property (nonatomic,retain) JEProgressView *meterR18Btm;
@property (nonatomic,retain) JEProgressView *meterR19Top;
@property (nonatomic,retain) JEProgressView *meterR19Btm;
@property (nonatomic,retain) JEProgressView *meterR20Top;
@property (nonatomic,retain) JEProgressView *meterR20Btm;
@property (nonatomic,retain) JEProgressView *meterR21Top;
@property (nonatomic,retain) JEProgressView *meterR21Btm;
@property (nonatomic,retain) JEProgressView *meterR22Top;
@property (nonatomic,retain) JEProgressView *meterR22Btm;
@property (nonatomic,retain) JEProgressView *meterR23Top;
@property (nonatomic,retain) JEProgressView *meterR23Btm;
@property (nonatomic,retain) JEProgressView *meterR24Top;
@property (nonatomic,retain) JEProgressView *meterR24Btm;
@property (nonatomic,retain) JEProgressView *meterR25Top;
@property (nonatomic,retain) JEProgressView *meterR25Btm;
@property (nonatomic,retain) JEProgressView *meterR26Top;
@property (nonatomic,retain) JEProgressView *meterR26Btm;
@property (nonatomic,retain) JEProgressView *meterR27Top;
@property (nonatomic,retain) JEProgressView *meterR27Btm;
@property (nonatomic,retain) JEProgressView *meterR28Top;
@property (nonatomic,retain) JEProgressView *meterR28Btm;
@property (nonatomic,retain) JEProgressView *meterR29Top;
@property (nonatomic,retain) JEProgressView *meterR29Btm;
@property (nonatomic,retain) JEProgressView *meterR30Top;
@property (nonatomic,retain) JEProgressView *meterR30Btm;
@property (nonatomic,retain) JEProgressView *meterR31Top;
@property (nonatomic,retain) JEProgressView *meterR31Btm;

- (void)processReply:(EffectPagePacket *)pkt;
- (void)setGeqSoloOnOff:(BOOL)value;
- (void)setGeqOnOff:(BOOL)value;
//- (void)setLinkMode:(BOOL)value;
- (void)reset;

@end
